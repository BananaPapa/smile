#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2015 Smile
#test
# Этот файл часть ПО Улыбка (smile), консольного анализатора кода.
# Лицензия: GNU GPL version 3, смотрите файл "AUTHORS" для подробностей.

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

import smile

if __name__ == '__main__':
    setup(
        name='smile',
        version=smile.__version__,
        author=smile.__author__,
        author_email=smile.__email__,
        description='Console code analyzer on RD FSTEK of Russia NDV',
        long_description=smile.__doc__,
        license=smile.__license__,
        classifiers=[
            "Development Status :: 1 - Planning",
            "Environment :: Console",
            "Intended Audience :: Information Technology",
            "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
            "Natural Language :: Russian",
            "Operating System :: POSIX :: Linux",
            "Operating System :: Microsoft :: Windows",
            "Topic :: Software Development :: Testing"
        ],
        keywords='smile development security analyze code',
        scripts=['scripts/smile'],
        data_files=[
            ('share/doc/smile',
             ['README.md',
              'CHANGELOG.md']),
        ],
        package_data={'smile': ['config/logging.json']},
        packages=('smile',
                  'smile.static'),
        ext_package=['peewee'],
        requires=['peewee']
    )

#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2015 Smile

# Этот файл часть ПО Улыбка (smile), консольного анализатора кода.
# Лицензия: GNU GPL version 3, смотрите файл "AUTHORS" для подробностей.
"""
Модуль для вставки маркеров в лабораторные данные.
"""

# Стандартные модули
import logging
import pathlib

# TODO Обдумать местоположение данного модуля и наименование
# Пользовательские модули и переменные
from smile.utils import OverLoadStr
from smile import LANGUAGES
from smile.utils import MarkerError
from smile.marker import markers

# Включить логирование
log = logging.getLogger(__name__)


class Marker(OverLoadStr):
    """
    Класс для вставки маркеров.
    Вставка маркеров происходит посредством вспомогательных функций вставки, соответствующих языкам программирования:
    insert_static_marker_c_plus_plus
    insert_static_marker_java
    insert_static_marker_c_sharp
    insert_dynamic_marker_c_plus_plus
    insert_dynamic_marker_java
    insert_dynamic_marker_c_sharp
    Все спецсимволы заменяются словами и/или знаками подчёркивания: '+':        '_plus',
                                                                    '#':        '_sharp'
                                                                    '-', ' ':   '_'
    """

    def __init__(self, id_num, file, analyze, language=None):
        """
        Инициализация объекта.
        :param id_num: Идентификационный номер маркера
        :type id_num: int
        :param file: Файл для вставки маркера.
        :type file: pathlib.Path
        :param analyze: Тип проводимого анализа (static, dynamic)
        :type analyze: str
        :param language: Язык программирования, которые используется в файле (нужен для определения типа маркера)
        :type language: str
        """
        self.id_num = id_num

        if not file.exists():
            raise MarkerError("Файл {} не существует".format(file))
        self.file = file

        self.analyze = analyze

        if not language:
            self.language = self._get_language_from_ext(self.file)
        else:
            if language not in LANGUAGES:
                raise MarkerError("Язык программирования {} отсутствует в списке доступных языков".format(language))
            self.language = language

    def insert(self):
        """
        Непосредственная вставка маркеров.
        :return:
        """
        log.debug("Вставляем {} маркер № {}, язык программирования {}, в файл {}".format(self.analyze,
                                                                                         self.id_num,
                                                                                         self.language,
                                                                                         self.file))
        self._get_insert_function()(self.id_num, self.file)

    def check(self):
        """
        Проверка файла на наличие маркера.
        :return:
        """
        log.debug("Проверка наличия маркера в исходном файле")
        return self._get_check_function()(self.file)

    def _get_insert_function(self):
        """
        Из данных инициализации формирует объект-функцию вставки маркеров.
        """
        log.debug("Получаем объект-функцию вставки маркера")
        if self.analyze not in ("static", "dynamic"):
            raise MarkerError("{} - нет такого типа анализа".format(self.analyze))

        name_of_language = self._replace_in_language(self.language)
        name_of_function = "insert_" + self.analyze + "_" + name_of_language
        log.debug(name_of_function)
        try:
            return getattr(markers, name_of_function)
        except AttributeError:
            raise MarkerError("Такой функции вставки маркеров не существует {}".format(name_of_function))

    def _get_check_function(self):
        """
        Из данных инициализации формирует объект-функцию проверки маркеров в исходных файлах.
        """
        log.debug("Получаем объект-функцию проверки маркера в исходных файлах")
        if self.analyze not in ("static", "dynamic"):
            raise MarkerError("{} - нет такого типа анализа".format(self.analyze))

        name_of_language = self._replace_in_language(self.language)
        name_of_function = "check_" + self.analyze + "_" + name_of_language
        log.debug(name_of_function)
        try:
            return getattr(markers, name_of_function)
        except AttributeError:
            raise MarkerError("Такой функции проверки маркеров в исходных файлах не существует {}"
                              .format(name_of_function))

    @staticmethod
    def _replace_in_language(language):
        """
        Заменяет символы в языке на строку:
        '+' - '_plus'
        '#' - '_sharp'
        '-', ' ' - '_'
        :param language: Язык для замены спецсимволов
        :type language: str
        :return: Форматированный язык
        :rtype: str
        """
        language = language.replace("+", "_plus")
        language = language.replace("#", "_sharp")
        language = language.replace("-", "_")
        language = language.replace(" ", "_")

        return language.lower()

    @staticmethod
    def _get_language_from_ext(file):
        """
        Получает название языка в соответствии с расширением файла.
        :param file: Файл для вставки маркера
        :type file: pathlib.Path
        :return: Название языка из списка языков
        :rtype: str
        """
        extension = file.suffix
        language = None
        for language in LANGUAGES:
            if extension in LANGUAGES[language]["EXT"]:
                break
        if not language:
            raise MarkerError("Расширение файла - {}, не удалось определить язык программирования".format(extension))
        return language


if __name__ == "__main__":
    try:
        from smile.logger import setup_logging

        setup_logging()
        log = logging.getLogger(__name__)
    except ImportError:
        logging.basicConfig(level="DEBUG")

    insert_python = Marker(1337, pathlib.Path("test/test.py"), "static")
    insert_python.insert()

    insert_cpp = Marker(1337, pathlib.Path("test/test.cpp"), "static")
    insert_cpp.insert()

    insert_java = Marker(1337, pathlib.Path("test/test.java"), "static")
    insert_java.insert()

    insert_c_sharp = Marker(1337, pathlib.Path("test/test.cs"), "static")
    insert_c_sharp.insert()

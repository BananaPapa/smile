#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2015 Smile

# Этот файл часть ПО Улыбка (smile), консольного анализатора кода.
# Лицензия: GNU GPL version 3, смотрите файл "AUTHORS" для подробностей.
"""
Функции вставки маркеров, соответствующих языкам программирования.
"""


# Стандартные модули
import logging
import pathlib

# Пользовательские модули и переменные

# Сторонние модули

# Включение логов
log = logging.getLogger(__name__)


def insert_static_c_plus_plus(id_num, file):
    """
    Функция вставки статического маркера в исходный файл языка программирования C, C++
    :param id_num: Идентификационный номер маркера
    :type id_num: int
    :param file: Файл для вставки
    :type file: pathlib.Path
    """
    marker = ("\n\n"
              "#ifndef NUM_MARKER_{id_num:08d}\n"
              "#define NUM_MARKER_{id_num:08d}\n"
              "\tstatic int tmp_marker_{id_num:08d}[]={{0xDEADBEAF, 0x{id_num:08x}}};\n"
              "#endif\n".format(id_num=id_num)).encode("ascii")

    with file.open("br+") as fd:
        # В конец файла
        fd.seek(0, 2)
        fd.write(marker)


def check_static_c_plus_plus(file):
    """
    Функция проверки исходного файла языка программирования C, C++ на наличие маркера.
    :param file: Файл для проверки
    :type file: pathlib.Path
    :return: True - если маркер присутсвует, False - если маркера нет в файле
    :rtype: bool
    """
    with file.open("r", errors="surrogateescape") as fd:
        if "DEADBEAF" in fd.read():
            return True
        else:
            return False


def insert_static_python(id_num, file):
    """
    Функция вставки статического маркера в исходный файл языка программирования Python
    :param id_num: Идентификационный номер маркера
    :type id_num: int
    :param file: Файл для вставки
    :type file: pathlib.Path
    """
    id_num = "{:08x}".format(id_num)
    marker = ("\n\n"
              "marker = b'\\xDE\\xAD\\xBE\\xAF"
              "\\x{}\\x{}\\x{}\\x{}'\n".format(id_num[0:2], id_num[2:4], id_num[4:6], id_num[6:8])).encode("ascii")

    with file.open("br+") as fd:
        # В конец файла
        fd.seek(0, 2)
        fd.write(marker)


def check_static_python(file):
    """
    Функция проверки исходного файла языка программирования Python на наличие маркера.
    :param file: Файл для проверки
    :type file: pathlib.Path
    :return: True - если маркер присутсвует, False - если маркера нет в файле
    :rtype: bool
    """
    with file.open("r", errors="surrogateescape") as fd:
        if """b'\xDE\xAD\xBE\xAF'""" in fd.read():
            return True
        else:
            return False


def insert_static_java(id_num, file):
    """
    Функция вставки статического маркера в исходный файл языка программирования Java
    :param id_num: Идентификационный номер маркера
    :type id_num: int
    :param file: Файл для вставки
    :type file: pathlib.Path
    """
    marker = ("\n\n"
              "final class 0x{:08x}DEADBEAF{{}}\n".format(id_num)).encode('ascii')

    with file.open("br+") as fd:
        # В конец файла
        fd.seek(0, 2)
        fd.write(marker)


def check_static_java(file):
    """
    Функция проверки исходного файла языка программирования Java на наличие маркера.
    :param file: Файл для проверки
    :type file: pathlib.Path
    :return: True - если маркер присутсвует, False - если маркера нет в файле
    :rtype: bool
    """
    with file.open("r", errors="surrogateescape") as fd:
        if "DEADBEAF" in fd.read():
            return True
        else:
            return False


def insert_static_c_sharp(id_num, file):
    """
    Функция вставки статического маркера в исходный файл языка программирования C#
    :param id_num: Идентификационный номер маркера
    :type id_num: int
    :param file: Файл для вставки
    :type file: pathlib.Path
    """
    marker = ("\n\n"
              "#region MyClass definition\n"
              "  public class tmp_{id_num:08d}{{\n"
              "    static void tmp_{id_num:08d}(){{\n"
              "      ulong[] tmp_{id_num:08d} = {{0x{id_num:08x}DEADBEAF}};\n"
              "    }}\n"
              "  }}\n"
              "#endregion\n".format(id_num=id_num)).encode("ascii")

    with file.open("br+") as fd:
        # В конец файла
        # TODO Обратить внимание!
        # fd.write(b"using System;")
        fd.seek(0, 2)
        fd.write(marker)


def check_static_c_sharp(file):
    """
    Функция проверки исходного файла языка программирования C# на наличие маркера.
    :param file: Файл для проверки
    :type file: pathlib.Path
    :return: True - если маркер присутсвует, False - если маркера нет в файле
    :rtype: bool
    """
    with file.open("r", errors="surrogateescape") as fd:
        if "DEADBEAF" in fd.read():
            return True
        else:
            return False


if __name__ == "__main__":
    try:
        from smile.logger import setup_logging

        setup_logging()
        log = logging.getLogger(__name__)
    except ImportError:
        logging.basicConfig(level="DEBUG")

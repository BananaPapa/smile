#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2015 Smile

# Этот файл часть ПО Улыбка (smile), консольного анализатора кода.
# Лицензия: GNU GPL version 3, смотрите файл "AUTHORS" для подробностей.
"""Smile
ПО Улыбка - программа для статического и динамического анализа в соответствии с РД ФСТЭК России по НДВ,
 а также создания отчётов.
"""

import pathlib

# Информация
__license__ = 'GPLv3'
__version__ = '1.0a1.dev1'
__author__ = __maintainer__ = 'Alexander Rumyantsev'
__email__ = 'rum.274.4@gmail.com'

# TODO Анализатор расширений файлов. Файлы без расширений - magic numbers
# Заполнять БД расширений, опрашивать пользователя о новом расширении (magic)

# TODO Классификация данных: бинарные, исходные, архивы, ресурсы: video, image, music
# TODO Поиск эквивалентных файлов

# TODO !!! Смена БД в run-time с одной и той же моделью данных!

# TODO перенести в отдельный файл для удобства дополнения
# TODO "GOST34_94", "STREEBOG256", "STREEBOG512"
CHECKSUM_TYPE = ["MD5", "SHA256", "SHA1"]
LEVELS = ['2', '3', '4']
# Языки с определённым параметром ASTYLE_OPT будут перекодированы и реформатированы AStyle'ом
#  (если пустая строка, то только перекодированы), если None - не перекодированы, не реформатированы
LANGUAGES = {
    # Будет удалён ввиду того, что он является дочерним элементом языка C++
    # "C": {
    #     "EXT": [".c", ".h"],
    #     "ASTYLE_OPT": "-A1"},
    "C++": {
        "EXT": [".cpp", ".hpp", ".cc", ".cxx", ".c++", ".c", ".h", ".hh", ".hpp", ".hxx", ".h++"],
        "ASTYLE_OPT": ("-A1",)},
    "OBJECTIVE-C": {
        "EXT": [".m", ".mm"],
        "ASTYLE_OPT": ("-A1",)},
    "JAVA": {
        "EXT": [".java", ".class"],
        "ASTYLE_OPT": ("-A1",)},
    "C#": {
        "EXT": [".cs"],
        "ASTYLE_OPT": ("-A1",)},
    "PYTHON": {
        "EXT": [".py"],
        "ASTYLE_OPT": ""},
    "PHP": {
        "EXT": [".php", ".phtml,", ".php3", ".php4", ".php5", ".phps"],
        "ASTYLE_OPT": ("-A1",)},
    "JAVASCRIPT": {
        "EXT": [".js"],
        "ASTYLE_OPT": ("-A1",)},
    "PASCAL": {
        "EXT": [".p", ".pp", ".pas", ".dpr"],
        "ASTYLE_OPT": ""},
    "ASM": {
        "EXT": [".asm"],
        "ASTYLE_OPT": ""},
    "UNIX SHELL": {
        "EXT": [".sh", ".bash", ".zsh", ".ksh", ".bsh", ".csh", ".tcsh"],
        "ASTYLE_OPT": ""},
    "WINDOWS SHELL": {
        "EXT": [".ps1", ".psm1", ".psd1", ".bat", ".cmd"],
        "ASTYLE_OPT": ""},
    "PERL": {
        "EXT": [".pl", ".pm", ".t", ".pod"],
        "ASTYLE_OPT": ""},
    "HTML": {
        "EXT": [".html", ".htm"],
        "ASTYLE_OPT": None},
    "CGI": {
        "EXT": [".cgi"],
        "ASTYLE_OPT": None},
    "QT": {
        "EXT": [".qt", ".pro", ".ui", ".qrc", ".pri"],
        "ASTYLE_OPT": None},
    "VISUAL STUDIO": {
        "EXT": [".vcproj", ".vbproj", ".vdproj", ".sln", ".vsproj", ".vsxproj", ".inc", ".dbp", ".inl", ".mc6", ".mdp",
                ".mrb", ".msvc", ".odl", ".pbo", ".vc1", ".vc2", ".vc4", ".vc5", ".vc6", ".vc7", ".vcw"],
        "ASTYLE_OPT": None},
    "INTELLIJ": {
        "EXT": [".iml", ".ipr", ".iws"],
        "ASTYLE_OPT": None},
    "XCODE": {
        "EXT": [".pbproj", ".xcode", ".iws"],
        "ASTYLE_OPT": None},
    "KEY CERTIFICATION": {
        "EXT": [".key", ".cer"],
        "ASTYLE_OPT": None},
    "MAKEFILE": {
        "EXT": [".mak", ".make"],
        "ASTYLE_OPT": None},
    "SWIG": {
        "EXT": [".i"],
        "ASTYLE_OPT": None},
    "FLASH": {
        "EXT": [".swf", ".fla", ".flv", ".jsfl", ".clr"],
        "ASTYLE_OPT": None},
    "R": {
        "EXT": [".r"],
        "ASTYLE_OPT": None},
    "GO": {
        "EXT": [".go"],
        "ASTYLE_OPT": None},
    "RUST": {
        "EXT": [".rs", ".rlib"],
        "ASTYLE_OPT": None},
    "VHDL": {
        "EXT": [".vhd"],
        "ASTYLE_OPT": None},
    "SQL": {
        "EXT": [".sql"],
        "ASTYLE_OPT": None},
    "SQLITE": {
        "EXT": [".sqlite", ".db-shm", ".db-wal", ".db3", ".mddata", ".sqlite3", ".sqlitedb"],
        "ASTYLE_OPT": None},
    "DATABASE": {
        "EXT": [".db"],
        "ASTYLE_OPT": None},
    "SCALA": {
        "EXT": [".scala"],
        "ASTYLE_OPT": None},
    "SCHEME": {
        "EXT": [".scm"],
        "ASTYLE_OPT": None},
    "SJS": {
        "EXT": [".sjs"],
        "ASTYLE_OPT": None},
    "TYPESCRIPT": {
        "EXT": [".ts"],
        "ASTYLE_OPT": None},
    "VALA": {
        "EXT": [".vala"],
        "ASTYLE_OPT": None},
    "VELOCITY": {
        "EXT": [".vm"],
        "ASTYLE_OPT": None},
    "VERILOG": {
        "EXT": [".v"],
        "ASTYLE_OPT": None},
    "CLOJURE": {
        "EXT": [".clj", ".cljs", ".cljc", ".edn"],
        "ASTYLE_OPT": None},
    "VBSCRIPT": {
        "EXT": [".vbs", ".wsf", ".wsc"],
        "ASTYLE_OPT": None},
    "BRAINFUCK": {
        "EXT": [".b", ".bf"],
        "ASTYLE_OPT": None},
    "OPENCL": {
        "EXT": [".cl"],
        "ASTYLE_OPT": None},
    "TCL": {
        "EXT": [".tcl"],
        "ASTYLE_OPT": None},
    "ACTIONSCRIPT": {
        "EXT": [".as", ".actionscript", ".aso", ".asr"],
        "ASTYLE_OPT": None},
    "ECMASCRIPT": {
        "EXT": [".es"],
        "ASTYLE_OPT": None},
    "HASKELL": {
        "EXT": [".hs", ".lhs"],
        "ASTYLE_OPT": None},
    "ERLANG": {
        "EXT": [".erl", ".hrl"],
        "ASTYLE_OPT": None},
    "RUBY": {
        "EXT": [".rb", ".rbw"],
        "ASTYLE_OPT": None},
    "APPLESCRIPT": {
        "EXT": [".scpt", ".AppleScript"],
        "ASTYLE_OPT": None},
    "MATLAB": {
        "EXT": [".matlab"],
        "ASTYLE_OPT": None},
    "LISP": {
        "EXT": [".lisp", ".lsp", ".l", ".cl", ".fasl"],
        "ASTYLE_OPT": None},
    "POSTSCRIPT": {
        "EXT": [".ps"],
        "ASTYLE_OPT": None},
    "ABAP": {
        "EXT": [".abap"],
        "ASTYLE_OPT": None},
    "ADA": {
        "EXT": [".ada"],
        "ASTYLE_OPT": None},
    "COBOL": {
        "EXT": [".cbl"],
        "ASTYLE_OPT": None},
    "COFFEE": {
        "EXT": [".coffee"],
        "ASTYLE_OPT": None},
    "COLDFUSION": {
        "EXT": [".cfm"],
        "ASTYLE_OPT": None},
    "CURLY": {
        "EXT": [".curly"],
        "ASTYLE_OPT": None},
    "D": {
        "EXT": [".d"],
        "ASTYLE_OPT": None},
    "DART": {
        "EXT": [".dart"],
        "ASTYLE_OPT": None},
    "EIFFEL": {
        "EXT": [".e"],
        "ASTYLE_OPT": None},
    "ELM": {
        "EXT": [".elm"],
        "ASTYLE_OPT": None},
    "GROOVY": {
        "EXT": [".groovy"],
        "ASTYLE_OPT": None},
    "HAML": {
        "EXT": [".haml"],
        "ASTYLE_OPT": None},
    "HAXE": {
        "EXT": [".hx"],
        "ASTYLE_OPT": None},
    "RUBY HTML": {
        "EXT": [".erb"],
        "ASTYLE_OPT": None},
    "IO": {
        "EXT": [".io"],
        "ASTYLE_OPT": None},
    "JACK": {
        "EXT": [".jack"],
        "ASTYLE_OPT": None},
    "JADE": {
        "EXT": [".jade"],
        "ASTYLE_OPT": None},
    "JSP": {
        "EXT": [".jsp"],
        "ASTYLE_OPT": None},
    "JSX": {
        "EXT": [".jsx"],
        "ASTYLE_OPT": None},
    "JULIA": {
        "EXT": [".jl"],
        "ASTYLE_OPT": None},
    "LATEX": {
        "EXT": [".tex", ".latex"],
        "ASTYLE_OPT": None},
    "LOGIQL": {
        "EXT": [".logic"],
        "ASTYLE_OPT": None},
    "LUA": {
        "EXT": [".lua"],
        "ASTYLE_OPT": None},
    "LUAPAGE": {
        "EXT": [".lp"],
        "ASTYLE_OPT": None},
    "LUCENE": {
        "EXT": [".lucene"],
        "ASTYLE_OPT": None},
    "MARKDOWN": {
        "EXT": [".md"],
        "ASTYLE_OPT": None},
    "MYSQL": {
        "EXT": [".mysql"],
        "ASTYLE_OPT": None},
    "NIX": {
        "EXT": [".nix"],
        "ASTYLE_OPT": None},
    "OCAML": {
        "EXT": [".ml"],
        "ASTYLE_OPT": None},
    "PGSQL": {
        "EXT": [".pgsql"],
        "ASTYLE_OPT": None},
    "PROLOG": {
        "EXT": [".plg"],
        "ASTYLE_OPT": None},
    "SASS": {
        "EXT": [".sass"],
        "ASTYLE_OPT": None},
    "SCAD": {
        "EXT": [".scad"],
        "ASTYLE_OPT": None},
    # Данный список языков под сомнением
    "XML": {
        "EXT": [".xml"],
        "ASTYLE_OPT": None},
    "XQUERY": {
        "EXT": [".xq"],
        "ASTYLE_OPT": None},
    "YAML": {
        "EXT": [".yaml"],
        "ASTYLE_OPT": None},
    "SCSS": {
        "EXT": [".scss"],
        "ASTYLE_OPT": None},
    "CSS": {
        "EXT": [".css"],
        "ASTYLE_OPT": None},
    "JSON": {
        "EXT": [".json"],
        "ASTYLE_OPT": None},
}

ARCHIVE = ['.7z', '.ace', '.afa', '.alz', '.ar', '.arc', '.arj', '.b1', '.ba', '.bh', '.bz2', '.cab',
           '.cfs', '.cpio', '.cpt', '.dar', '.dd', '.dgc', '.dmg', '.ear', '.ecc', '.f', '.gca', '.gz', '.ha',
           '.hki', '.ice', '.iso', '.kgb', '.lbr', '.lha', '.lz', '.lzh', '.lzma', '.lzo', '.lzx', '.mar',
           '.pak', '.paq6', '.paq7', '.paq8', '.par', '.par2', '.partimg', '.pea', '.pim', '.pit', '.qda',
           '.rar', '.rk', '.rz', '.s7z', '.sda', '.sea', '.sen', '.sfark', '.sfx', '.shar', '.sit', '.sqx',
           '.sz', '.tar', '.tar.bz2', '.tar.gz', '.tar.lzma', '.tar.z', '.tbz2', '.tgz', '.tlz', '.uc', '.uc0', '.uc2',
           '.uca', '.ucn', '.ue2', '.uha', '.ur2', '.war', '.wim', '.xar', '.xp3', '.xz', '.yz1', '.z', '.zip', '.zipx',
           '.zoo', '.zpaq', '.zz', '.sitx']

# TODO по мере исследования добавлять новые расширения файлов
BINARY = ['.a', '.air', '.apk', '.app', '.appx', '.bas', '.bin', '.com', '.deb', '.dex', '.dll', '.ejb',
          '.elf', '.exe', '.exe1', '.jar', '.jse', '.ipa', '.lib', '.lnk', '.msi', '.msp', '.nexe', '.nsi', '.o',
          '.odex', '.osx', '.paf.exe', '.pe', '.pif', '.pyc', '.pyd', '.pyo', '.pyw', '.pyz', '.rpm', '.shs', '.so',
          '.vb', '.vbe', '.ws']

PATH_SMILE = str(pathlib.Path(__file__).parent)
# TODO Внедрить смену папки конфигурации
PATH_CONF = str(pathlib.Path(PATH_SMILE) / "config")

from smile.main import main

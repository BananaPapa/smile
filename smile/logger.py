#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2015 Smile

# Этот файл часть ПО Улыбка (smile), консольного анализатора кода.
# Лицензия: GNU GPL version 3, смотрите файл "AUTHORS" для подробностей.
"""
Модуль для логирования ошибок, предупреждений, информировании, отладки и т.д.
"""

# Стандартные модули
import logging
import logging.config
import pathlib
import json

# Пользовательские модули
from smile import PATH_CONF

LOG_PATH = str(pathlib.Path().cwd() / "logs")
LOG_INFO_PATH = str(pathlib.Path(LOG_PATH) / "info.log")
LOG_ERRORS_PATH = str(pathlib.Path(LOG_PATH) / "errors.log")
LOG_DEBUG_PATH = str(pathlib.Path(LOG_PATH) / "debug.log")


def setup_logging(
        default_path=PATH_CONF + '/logging.json',
        default_level=logging.INFO
):
    """Setup logging configuration

    """
    path = pathlib.Path(default_path)
    if path.exists():
        with open(str(path)) as f:
            config = json.loads(f.read())
        if not pathlib.Path(LOG_PATH).exists():
            pathlib.Path(LOG_PATH).mkdir()
        logging.config.dictConfig(config)
    else:
        print("Be using basic logging!")
        logging.basicConfig(level=default_level)


class ColorizingStreamHandler(logging.StreamHandler):
    """Special handler for colorizing log messages
    """

    BLACK = '\033[0;30m'
    RED = '\033[0;31m'
    GREEN = '\033[0;32m'
    BROWN = '\033[0;33m'
    BLUE = '\033[0;34m'
    PURPLE = '\033[0;35m'
    CYAN = '\033[0;36m'
    GREY = '\033[0;37m'

    DARK_GREY = '\033[1;30m'
    LIGHT_RED = '\033[1;31m'
    LIGHT_GREEN = '\033[1;32m'
    YELLOW = '\033[1;33m'
    LIGHT_BLUE = '\033[1;34m'
    LIGHT_PURPLE = '\033[1;35m'
    LIGHT_CYAN = '\033[1;36m'
    WHITE = '\033[1;37m'

    RESET = "\033[0m"

    def __init__(self, *args, **kwargs):
        self._colors = {logging.DEBUG: self.LIGHT_BLUE,
                        logging.INFO: self.RESET,
                        logging.WARNING: self.BROWN,
                        logging.ERROR: self.RED,
                        logging.CRITICAL: self.LIGHT_RED}
        super(ColorizingStreamHandler, self).__init__(*args, **kwargs)

    @property
    def is_tty(self):
        isatty = getattr(self.stream, 'isatty', None)
        return isatty and isatty()

    def emit(self, record):
        # noinspection PyBroadException
        try:
            message = self.format(record)
            stream = self.stream
            if not self.is_tty:
                stream.write(message)
            else:
                message = self._colors[record.levelno] + message + self.RESET
                stream.write(message)
            stream.write(getattr(self, 'terminator', '\n'))
            self.flush()
        except (KeyboardInterrupt, SystemExit):
            raise
        except:
            self.handleError(record)

    # noinspection PyPep8Naming
    def setLevelColor(self, logging_level, escaped_ansi_code):
        self._colors[logging_level] = escaped_ansi_code

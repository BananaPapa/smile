#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2015 Smile

# Этот файл часть ПО Улыбка (smile), консольного анализатора кода.
# Лицензия: GNU GPL version 3, смотрите файл "AUTHORS" для подробностей.
"""
Парсер аргументов
"""

# Стандартные модули
import argparse
import logging
import sys

# Пользовательские модули
from smile import LANGUAGES, LEVELS, __version__

# Сторонние модули

# Включение логов
log = logging.getLogger(__name__)


# TODO Добавить аргументы!
def parser():
    """
    Функция парсинга аргументов.
    :return:
    """
    # Главный парсер
    main_parser = argparse.ArgumentParser(prog="smile",
                                          description="Анализатор кода."
                                                      " Выполняет снятие контрольных сумм,"
                                                      " статический и динамический анализ кода в соответствии"
                                                      " с РД НДВ и формирует отчёт."
                                                      " Для упрощённой работы с программой используйте интерактивный"
                                                      " режим (ключ -I, --interactive).",
                                          add_help=False)

    # Русскоязычный help
    main_parser.add_argument("--help", "-h", action="help",
                             help="Показывает это сообщение и выходит")

    main_parser.add_argument("--version", "-v", action='version', version="ПО Smile (Улыбка) v. {}".format(__version__),
                             help="Вывести версию программы")

    main_parser.add_argument("--interactive", "-I", action="store_true",
                             help="Запускает интерактивный режим")

    main_parser.add_argument("--print-all", "-P", action="store_true",
                             help="Отладка. Выводит всю информацию из БД конфигурации")

    # Подпарсеры для главного парсера
    subparsers = main_parser.add_subparsers(title="Стандартные команды",
                                            dest="subparser_name_main",
                                            help="Для получения справки запустите: %(prog)s 'КОМАНДА' -h/--help ")

    # Парсер для опции main -> apk
    parser_apk = subparsers.add_parser("apk", help="Операции с АПК",
                                       add_help=False)
    parser_apk.add_argument("--help", "-h", action="help",
                            help="Показывает это сообщение и выходит")

    # Подпарсеры для apk парсера
    subparsers_apk = parser_apk.add_subparsers(title="Команды для работы с АПК",
                                               dest="subparser_name_apk",
                                               help="Для получения справки запустите: %(prog)s 'КОМАНДА' -h/--help ")

    # Парсер для опции main -> apk -> create
    parser_apk_create = subparsers_apk.add_parser('create', help="Создание АПК",
                                                  add_help=False)
    parser_apk_create.add_argument("--help", "-h", action="help",
                                   help="Показывает это сообщение и выходит")
    parser_apk_create.add_argument("name", default="", type=str,
                                   help="Наименование АПК", metavar="ИМЯ_АПК")
    parser_apk_create.add_argument("--path", "-p", default="",
                                   help="Путь до АПК")

    # Парсер для опции main -> apk -> delete
    parser_apk_delete = subparsers_apk.add_parser('delete', help="Удаление АПК",
                                                  add_help=False)
    parser_apk_delete.add_argument("--help", "-h", action="help",
                                   help="Показывает это сообщение и выходит")
    parser_apk_delete.add_argument("name", default="", type=str,
                                   help="Наименование АПК", metavar="ИМЯ_АПК")

    # Парсер для опции main -> apk -> info
    parser_apk_info = subparsers_apk.add_parser('info', help="Информация об АПК",
                                                add_help=False)
    parser_apk_info.add_argument("--help", "-h", action="help",
                                 help="Показывает это сообщение и выходит")
    parser_apk_info.add_argument("name", default="", type=str,
                                 help="Наименование АПК", metavar="ИМЯ_АПК")

    # Парсер для опции main -> spo
    parser_spo = subparsers.add_parser('spo', help="Операции с СПО",
                                       add_help=False)
    parser_spo.add_argument("--help", "-h", action="help",
                            help="Показывает это сообщение и выходит")

    # Подпарсеры для spo парсера
    subparsers_spo = parser_spo.add_subparsers(title="Команды для работы с СПО",
                                               dest="subparser_name_spo",
                                               help="Для получения справки запустите: %(prog)s 'КОМАНДА' -h/--help ")

    # Парсер для опции main -> spo -> create
    parser_spo_create = subparsers_spo.add_parser('create', help="Создание СПО",
                                                  add_help=False)
    parser_spo_create.add_argument("--help", "-h", action="help",
                                   help="Показывает это сообщение и выходит")
    parser_spo_create.add_argument("name", default="", type=str,
                                   help="Наименование СПО", metavar="ИМЯ_СПО")
    parser_spo_create.add_argument("parent_apk", default="", type=str,
                                   help="Наименование родительского АПК", metavar="ИМЯ_АПК")
    parser_spo_create.add_argument("level", type=str, choices=LEVELS,
                                   help="Уровень контроля СПО")
    parser_spo_create.add_argument("languages", type=str, default=[], nargs='+', metavar="ЯЗЫК",
                                   choices=LANGUAGES,
                                   help="Языки программирования, используемые в СПО")
    parser_spo_create.add_argument("--path", "-p", default="",
                                   help="Путь до СПО")
    parser_spo_create.add_argument("--udb-path", "-u", default="",
                                   help="Путь до Understand БД")
    parser_spo_create.add_argument("--sdb-path", "-s", default="",
                                   help="Путь до SQLite БД")
    parser_spo_create.add_argument("--source-path", "-S", default="",
                                   help="Путь до исходных данных")
    parser_spo_create.add_argument("--lab-path", "-l", default="",
                                   help="Путь до лабораторных данных")

    # Парсер для опции main -> spo -> delete
    parser_spo_delete = subparsers_spo.add_parser('delete', help="Удаление СПО",
                                                  add_help=False)
    parser_spo_delete.add_argument("--help", "-h", action="help",
                                   help="Показывает это сообщение и выходит")
    parser_spo_delete.add_argument("name", default="", type=str,
                                   help="Наименование СПО", metavar="ИМЯ_СПО")
    parser_spo_delete.add_argument("parent_apk", default="", type=str,
                                   help="Наименование родительского АПК", metavar="ИМЯ_АПК")

    # Парсер для опции main -> spo -> info
    parser_spo_info = subparsers_spo.add_parser('info', help="Информация об СПО",
                                                add_help=False)
    parser_spo_info.add_argument("--help", "-h", action="help",
                                 help="Показывает это сообщение и выходит")
    parser_spo_info.add_argument("name", default="", type=str,
                                 help="Наименование СПО", metavar="ИМЯ_СПО")
    parser_spo_info.add_argument("parent_apk", default="", type=str,
                                 help="Наименование родительского АПК", metavar="ИМЯ_АПК")

    # Парсер для опции main -> spo -> reformat
    parser_spo_reformat = subparsers_spo.add_parser('reformat', help="Реформатирование СПО",
                                                    description="При вызове без параметров произведёт"
                                                                " копирование исходных данных,"
                                                                " перекодировку в UTF-8 и реформатирование AStyle'ом.",
                                                    add_help=False)
    parser_spo_reformat.add_argument("--help", "-h", action="help",
                                     help="Показывает это сообщение и выходит")
    parser_spo_reformat.add_argument("name", default="", type=str,
                                     help="Наименование СПО", metavar="ИМЯ_СПО")
    parser_spo_reformat.add_argument("parent_apk", default="", type=str,
                                     help="Наименование родительского АПК", metavar="ИМЯ_АПК")

    parser_spo_reformat_group_lab = parser_spo_reformat.add_mutually_exclusive_group()
    parser_spo_reformat_group_lab.add_argument("--copy-src", "-c", action="store_true",
                                               help="Скопировать исходные данные в лабораторные")
    parser_spo_reformat_group_lab.add_argument("--delete-lab", "-d", action="store_true",
                                               help="Удалить лабораторные данные")
    """
    parser_spo_reformat.add_argument("--to-utf8", "-u", action="store_true",
                                     help="Перекодировать исходные данные в UTF-8")
    """
    parser_spo_reformat.add_argument("--astyle", "-R", action="store_true",
                                     help="Реформатировать исходные данные (AStyle)")
    parser_spo_reformat_group_language = parser_spo_reformat.add_mutually_exclusive_group()
    parser_spo_reformat_group_language.add_argument("--add-language", "-A", action="store_true",
                                                    help="Добавить новый язык программирования в СПО")
    parser_spo_reformat_group_language.add_argument("--del-language", "-D", action="store_true",
                                                    help="Удалить язык программирования из СПО")
    parser_spo_reformat.add_argument("--search-language", "-S", action="store_true",
                                     help="Поиск языков в СПО (исходные данные)")
    parser_spo_reformat.add_argument("--get-size", "-s", action="store_true",
                                     help="Подсчёт размера файлов исходных данных")
    parser_spo_reformat.add_argument("--language", default="", type=str,
                                     help="Наименование языка программирования", metavar="ИМЯ_ЯЗЫКА")

    # Парсер для опции static
    parser_static = subparsers.add_parser('static', help="Статический анализ",
                                          add_help=False)
    parser_static.add_argument("--help", "-h", action="help",
                               help="Показывает это сообщение и выходит")
    # TODO Аргументы для статики
    parser_static.add_argument('--name', '-n')

    # Парсер для опции dynamic
    parser_dynamic = subparsers.add_parser('dynamic', help="Динамический анализ",
                                           add_help=False)
    parser_dynamic.add_argument("--help", "-h", action="help",
                                help="Показывает это сообщение и выходит")
    # TODO Аргументы для динамики
    parser_dynamic.add_argument('--name', '-n')

    if len(sys.argv) < 2:
        main_parser.print_help()
        main_parser.exit()

    return main_parser


if __name__ == '__main__':
    arg = parser()
    print(arg.parse_args())

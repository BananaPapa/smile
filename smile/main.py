#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2015 Smile

# Этот файл часть ПО Улыбка (smile), консольного анализатора кода.
# Лицензия: GNU GPL version 3, смотрите файл "AUTHORS" для подробностей.
"""Smile
ПО Улыбка - программа для статического и динамического анализа, а также создания отчётов.
Главный исполняемый файл.
"""
# TODO Удалить "капитанские" комментарии
# TODO Сделать установку, а также сделать относительный импорт (относительно пакета)
# TODO Сделать unittest
# TODO Перехватывать исключения прерывания клавиатуры во время процесса обработки проекта

# Стандартные модули
import sys
import logging


# Пользовательские модули
from smile import logger
from smile.menu import menu
from smile import project
from smile import arg_parser
from smile.utils import APKError, SPOError, Error
from smile.static import prepare

# Включаем логер
logger.setup_logging()
log = logging.getLogger(__name__)


# Сторонние модули


def main():
    """
    Главная функция
    :return: 0 - Успех, 1 - Ошибка, 2 - Ошибка аргумента
    :rtype: int
    """

    # Парсим аргументы, поданные программе
    # arg = opt_parser.Arguments(sys.argv[1:])
    arg = arg_parser.parser()
    run_arg(arg)

    # Запускаем то или иное действие в зависимости от аргументов
    # return run_operation(arg)


def run_arg(arg):
    log.debug("Парсим аргументы")
    args = arg.parse_args()
    log.debug(args)
    if args.interactive:
        log.debug("Интерактивное меню")
        menu()
    if args.print_all:
        log.debug("Отладка. Вывод БД конфигурации.")
        project.APK().debug_print_all()
    if args.subparser_name_main == "apk":
        log.debug("АПК")
        if args.subparser_name_apk == "create":
            log.debug("Создание АПК")
            try:
                project.APK(args.name, args.path).create()
            except APKError as err:
                log.error(err)
                sys.exit(err)
        elif args.subparser_name_apk == "delete":
            log.debug("Удаление АПК")
            try:
                project.APK(args.name).delete()
            except APKError as err:
                log.error(err)
                sys.exit(err)
        elif args.subparser_name_apk == "info":
            log.debug("Вывод информации об АПК")
            try:
                project.APK(args.name).print()
            except APKError as err:
                log.error(err)
                sys.exit(err)
        else:
            arg.print_help()
    elif args.subparser_name_main == "spo":
        log.debug("СПО")
        if args.subparser_name_spo == "create":
            log.debug("Создание СПО")
            try:
                project.SPO(args.name, args.parent_apk, args.level, args.languages,
                            args.path, args.udb_path, args.sdb_path, args.source_path,
                            args.lab_path).create()
            except SPOError as err:
                log.error(err)
                sys.exit(err)
        elif args.subparser_name_spo == "delete":
            log.debug("Удаление СПО")
            try:
                project.SPO(args.name, args.parent_apk).delete()
            except SPOError as err:
                log.error(err)
                sys.exit(err)
        if args.subparser_name_spo == "reformat":
            log.debug("Реформатирование СПО")
            try:
                spo = project.SPO(args.name, args.parent_apk).open()
            except SPOError as err:
                log.error(err)
                sys.exit(err)
            try:
                if (not args.copy_src and
                        not args.delete_lab and
                        not args.to_utf8 and
                        not args.astyle and
                        not args.add_language and
                        not args.del_language and
                        not args.search_language and
                        not args.get_size):
                    arg.error("Один из параметров должен использоваться.")
                if args.copy_src:
                    prepare.Prepare(spo).copy_source_to_lab()
                if args.delete_lab:
                    prepare.Prepare(spo).delete_lab(save_dir=True)
                if args.astyle:
                    prepare.Prepare(spo).reformat_astyle_src()
                if args.add_language:
                    if not args.language:
                        arg.error("Параметр --language должен быть определён")
                    spo.add_language(args.language)
                if args.del_language:
                    if not args.language:
                        arg.error("Параметр --language должен быть определён")
                    spo.delete_language(args.language)
                if args.search_language:
                    for language in prepare.Prepare(spo).search_languages():
                        log.info(language)
                if args.get_size:
                    size_of_source_files = prepare.Prepare(spo).get_size_of_source()
                    for language in sorted(size_of_source_files, key=lambda x: size_of_source_files[x][0]):
                        if language != "ALL":
                            log.info("Размер исходных файлов языка {} равен: {}".format(language,
                                                                                        size_of_source_files[language][1]))
                        else:
                            log.info("Размер ВСЕХ исходных данных равен: {}".format(size_of_source_files["ALL"][1]))
            except Error as err:
                log.error(err)
                sys.exit(err)
        elif args.subparser_name_spo == "delete":
            log.debug("Удаление СПО")
            try:
                project.SPO(args.name, args.parent_apk).delete()
            except SPOError as err:
                log.error(err)
                sys.exit(err)
        elif args.subparser_name_spo == "info":
            log.debug("Вывод информации об СПО")
            try:
                project.SPO(args.name, args.parent_apk).print()
            except SPOError as err:
                log.error(err)
                sys.exit(err)
        else:
            arg.print_help()
    elif args.subparser_name_main == "static":
        log.debug("Статический анализ")
        arg.print_help()
    elif args.subparser_name_main == "dynamic":
        log.debug("Динамический анализ")
        arg.print_help()

#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2015 Smile

# Этот файл часть ПО Улыбка (smile), консольного анализатора кода.
# Лицензия: GNU GPL version 3, смотрите файл "AUTHORS" для подробностей.
"""
Модуль для подсчёта контрольных сумм и создания отчётного файла.
"""

# Стандартные модули
import logging
import pathlib
import hashlib
import datetime

# Пользовательские модули и переменные
from smile import LANGUAGES, ARCHIVE, BINARY, CHECKSUM_TYPE
from smile.utils import OverLoadStr
from smile.utils import StaticError
from smile.static import prepare
from smile.project import SPO
from smile import database

# Сторонние модули

# Включение логов
log = logging.getLogger(__name__)


# TODO Записать всю информацию в БД. Вывести в файл? Создать HTML документ.

class CollectInfo(OverLoadStr):
    """
    Класс для подсчёта контрольных сумм исходных данных. Полученную информацию записывает в БД.
    """

    def __init__(self, spo_object, type_of_checksum='md5'):
        """
        Инициализация класса.
        :param spo_object: Объект СПО
        :type spo_object: SPO
        """
        log.debug("Начата инициализация объекта сбора информации о файлах")
        self.spo_object = spo_object
        self.prepare_object = prepare.Prepare(self.spo_object)
        self.type_of_checksum = type_of_checksum

        self.source_path = pathlib.Path(self.spo_object.source_path)

        self.sdb_path = self.spo_object.sdb_path

    def get_files(self):
        """
        Получает список всех файлов в папке исходных данных проекта.
        :return: Возвращает генератор, в котором содержится информация о файле
        :rtype: generator
        """
        for file in self.prepare_object.walk_in_dir(self.source_path):
            yield FileInfo(file, self.type_of_checksum)()

        # TODO обновление данных

    def save_db(self):
        """
        Сохраняет информацию о файлах в БД проекта.
        """
        log.debug("Сохранение в БД")
        database.init_database(self.sdb_path)
        db = database.SqliteDatabase(self.sdb_path)
        with database.Using(db, [database.Binary, database.Source, database.Files]):
            binary_data = []
            source_data = []
            etc_data = []
            files = self.get_files()
            for file in files:
                if file.type == "binary":
                    binary_data.append({"full_name": file.full_name,
                                        "checksum": file.checksum,
                                        "size": file.size,
                                        "ctime": datetime.datetime.fromtimestamp(file.ctime)})
                elif file.type == "source":
                    source_data.append({"full_name": file.full_name,
                                        "language": file.language,
                                        "checksum": file.checksum,
                                        "size": file.size,
                                        "lines": file.lines,
                                        "ctime": datetime.datetime.fromtimestamp(file.ctime),
                                        "is_marked": False,
                                        "is_used": False})
                else:
                    etc_data.append({"full_name": file.full_name,
                                     "type": file.type,
                                     "checksum": file.checksum,
                                     "size": file.size,
                                     "lines": file.lines,
                                     "ctime": datetime.datetime.fromtimestamp(file.ctime)})

            log.debug("Очистка БД перед записью")
            query_delete = database.Binary.delete()
            query_delete.execute()
            query_delete = database.Source.delete()
            query_delete.execute()
            query_delete = database.Files.delete()
            query_delete.execute()

            with database.database.atomic():
                for idx in range(0, len(binary_data), 100):
                    database.Binary.insert_many(binary_data[idx:idx + 100]).execute()
            with database.database.atomic():
                for idx in range(0, len(source_data), 100):
                    database.Source.insert_many(source_data[idx:idx + 100]).execute()
            with database.database.atomic():
                for idx in range(0, len(etc_data), 100):
                    database.Files.insert_many(etc_data[idx:idx + 100]).execute()


class FileInfo(OverLoadStr):
    """
    Класс, содержащий информацию о файле, а также сам её добывающий из полного имени файла.
    """

    def __init__(self, full_name, type_of_checksum):
        """
        Инициализация класса.
        Назначение атрибутов:
            type - тип файла (source, binary, resource, archive)
            language - язык программирования
            checksum - контрольная сумма
            type_of_checksum - тип контрольной суммы (md5, sha1, gost, etc)
            size - размер файла в байтах
            lines - количество строк в файле, если тот source файл
            ctime - время последней модификации метаданных (Unix), время создания (Windows)
        :param full_name: Полное имя файла (включая абсолютный путь до файла)
        :type full_name: str
        :param type_of_checksum: Тип контрольной суммы
        :type type_of_checksum: str
        """
        log.debug("Инициализация объекта сбора информации о файле")
        self.full_name = pathlib.Path(full_name)
        if type_of_checksum.upper() not in CHECKSUM_TYPE:
            raise StaticError("Неизвестный тип контрольной суммы {}".format(type_of_checksum))
        self.type_of_checksum = type_of_checksum.upper()

        # Ранее объявление атрибутов класса для наглядности
        # При добавлении новых атрибутов - также обновлять функцию сбора информации (функция __call__)
        self.type = str()
        self.language = str()
        self.checksum = str()
        self.size = int()
        self.lines = int()
        self.ctime = int()

    def __call__(self):
        """
        Функция вызова для экземпляра или класса.
        :return: Объект класса
        :rtype: FileInfo
        """
        log.debug("Создание объекта")
        self.type = self.determinate_type()
        self.language = self.determinate_language()
        self.checksum = self.get_checksum()
        self.size = self.full_name.stat().st_size
        self.lines = self.get_count_lines()
        self.ctime = self.full_name.stat().st_ctime

        return self

    def determinate_type(self):
        """
        Определяет тип файла.
        :return: source, binary, resource, archive
        :rtype: str
        """
        log.debug("Определение типа файла")

        # Определение возможных расширений исходных файлов
        src_extensions = [ext for language in LANGUAGES for ext in LANGUAGES[language]["EXT"]]

        if self.full_name.suffix.lower() in src_extensions:
            return "source"
        elif self.full_name.suffix.lower() in ARCHIVE:
            return "archive"
        # TODO определение исполняемых файлов не доработано, не находит файлы без расширения
        # TODO сделать поиск по magic bytes in header
        # После поиска и занесения в базу - отредактировать
        elif self.full_name.suffix.lower() in BINARY:
            return "binary"
        else:
            return "resource"

    def determinate_language(self):
        """
        Определяет язык программирования, если source файл.
        :return: Язык программирования
        :rtype: str
        """
        log.debug("Определение языка программирования")

        for language in LANGUAGES:
            if self.full_name.suffix.lower() in LANGUAGES[language]["EXT"]:
                return language

    def get_checksum(self):
        """
        Получает контрольную сумму файла.
        :return:
        """
        log.debug("Получение контрольной суммы {} файла {}".format(self.type_of_checksum, self.full_name))

        if self.type_of_checksum == "MD5":
            return self._md5_sum()
        if self.type_of_checksum == "SHA256":
            return self._sha256_sum()
        if self.type_of_checksum == "SHA1":
            return self._sha1_sum()
        # TODO реализовать
        if self.type_of_checksum == "GOST34_94":
            return "GOST34_94"
        if self.type_of_checksum == "STREEBOG256":
            return "STREEBOG256"
        if self.type_of_checksum == "STREEBOG512":
            return "STREEBOG512"

    def get_count_lines(self):
        """
        Получает количество строк в файле, если это возможно.
        :return:
        """
        log.debug("Получение количества строк в файле")
        try:
            with self.full_name.open() as file:
                return len(file.readline())
        except UnicodeDecodeError:
            return None

    def _md5_sum(self):
        """
        Генерация md5 суммы из содержимого файла.
        :return: md5 сумма
        :rtype: str
        """
        log.debug("Получение md5 суммы файла")
        with self.full_name.open(mode='rb') as file:
            content = file.read()
        return hashlib.md5(content).hexdigest()

    def _sha256_sum(self):
        """
        Генерация sha256 суммы из содержимого файла.
        :return: sha256 сумма
        :rtype: str
        """
        log.debug("Получение sha256 суммы файла")
        with self.full_name.open(mode='rb') as file:
            content = file.read()
        return hashlib.sha256(content).hexdigest()

    def _sha1_sum(self):
        """
        Генерация sha1 суммы из содержимого файла.
        :return: sha1 сумма
        :rtype: str
        """
        log.debug("Получение sha1 суммы файла")
        with self.full_name.open(mode='rb') as file:
            content = file.read()
        return hashlib.sha1(content).hexdigest()


if __name__ == "__main__":
    spo = SPO("Тест", "Тест").open()
    collect_info = CollectInfo(spo, 'md5')
    collect_info.save_db()

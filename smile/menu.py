#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2015 Smile

# Этот файл часть ПО Улыбка (smile), консольного анализатора кода.
# Лицензия: GNU GPL version 3, смотрите файл "AUTHORS" для подробностей.
"""
Модуль меню для отрисовки вариантов (меню) и пользовательского ввода
"""

# Стандартные модули
import os
import logging
import sys
import itertools

# Пользовательские модули
from smile import LANGUAGES, LEVELS, CHECKSUM_TYPE
from smile.utils import OverLoadStr
from smile.project import clear_string
from smile.project import APK, SPO
from smile.static.prepare import Prepare
from smile.utils import (APKError, DatabaseConfigError,
                         MenuError, MenuInputAbort, Error, MenuYesAnswer)
from smile.static.static import Static
from smile.static.collectinfo import CollectInfo
from smile import database

# Включить логирование
log = logging.getLogger(__name__)


# TODO Дописать строки документации к функциям, также сделать рефакторинг
# TODO Придумать идею организации меню получше... Графика? ncurses?
# TODO реорганизовать классы диалогов?
class Menu(OverLoadStr):
    """
    Базовый класс меню.
    """

    def __init__(self, name, items=None, title="",
                 go_main_menu=False,
                 go_quit=True,
                 go_back=True,
                 return_res=False,
                 main_menu=None,
                 debug_menu=None):
        """
        :param name: Наименование меню.
        :type name: str
        :param items: Список пунктов меню: [("Наименование пункта_1", функция_запуска_1, kwargs),
                                            ("Наименование пункта_2", функция_запуска_2), kwargs,
                                            ("Наименование пункта_3", функция_запуска_3)]
                    Наименование пункта меню - строка,
                    функция запуска - объект вызываемой функции (метода),
                    kwargs - аргументы для функции в виде словаря (могут отсутствовать)
        :type items: list
        :param title: Заголовок меню.
        :type title: str
        :param go_main_menu: Присутствует пункт меню - ^ Главное меню
        :type go_main_menu: bool
        :param go_quit: Присутствует пункт меню - X Выход
        :type go_quit: bool
        :param go_back: Присутствует пункт меню - < Назад
        :type go_back: bool
        :param return_res: Меню будет возвращать результат вместо того, чтобы вызывать функцию
        :type return_res: bool
        :param main_menu: Объект главного меню
        :type main_menu: Menu
        :param debug_menu: Объект меню отладки (name должен быть debug_menu)
        :type debug_menu: Menu
        """

        self.name = name
        # Если kwargs нет, то kwargs=dict()
        self.items = dict()
        # TODO Разобраться со списками при опции return_res
        if items:
            for key, item in enumerate(items):
                if len(item) < 3:
                    self.items[str(key + 1)] = (item[0], item[1], dict(), list())
                else:
                    self.items[str(key + 1)] = item

        self.title = title
        self.go_main_menu = go_main_menu
        self.go_quit = go_quit
        self.main_menu = main_menu
        self.return_res = return_res
        self.go_back = go_back

        self.choice = None
        self.debug = None
        self.res = None
        self.default_items = dict()
        self.debug_menu = debug_menu

        # Дополняем пунктами меню << Назад и ^ Главное меню
        if self.go_back:
            self.default_items['b'] = ("< Назад", None)
        if self.go_main_menu:
            self.default_items['m'] = ("^ Главное меню", self.main_menu)
        if self.go_quit:
            self.default_items['q'] = ("X Выход", self._quit)

    def __call__(self):
        """
        Запускает меню (печатает пункты, ждёт пользовательского ввода, запускает функцию)
        """
        log.debug("Запуск меню %s", self.name)
        while True:
            self._show()
            self.choice = None
            try:
                self._user_input()
            except KeyboardInterrupt:
                self._quit()
                continue

            # Если выбрано < Назад
            if self.go_back and self.choice == 'b':
                return

            # Если требуется вернуть выбранный пункт меню
            if self.return_res and self.choice not in self.default_items:
                return self.items[self.choice]

            self._run_function()

    def _show(self):
        """
        Печать пунктов меню.
        """
        MenuUtils.clear_terminal()

        log.debug("Вывод меню %s", self.name)

        # Меню отладки, если логер настроен на DEBUG и меню не debug
        self.debug = True if log.getEffectiveLevel() < 20 else False
        if self.debug and self.name != "debug_menu":
            self.default_items['d'] = ("# Отладка", self.debug_menu)

        # Вывод меню
        print(self.title)
        print('-' * 80)
        for key in sorted(self.items, key=lambda x: int(x)):
            if not self.return_res:
                print("[{}] {}".format(key, self.items[key][0]))
            else:
                print("[{}] {}".format(key, self.items[key]))
        print('-' * 80)
        for key in sorted(self.default_items):
            print("[{}] {}".format(key, self.default_items[key][0]))

    def _user_input(self):
        """
        Пользовательский ввод.
        """
        log.debug("Ожидание пользовательского ввода")
        self.choice = input(">> ")
        while self.choice not in dict(self.items, **self.default_items):
            log.error("Пункта меню с ключом %s не существует!", self.choice)
            self.choice = input(">> ")

    def _run_function(self):
        """
        Запускает функцию, определённую для пункта меню.
        """
        log.debug("Запуск функции, соответствующей выбранному пункту меню")

        if self.choice in self.items:
            log.debug("Исполнение действия функции [%s, %s(%s)]",
                      self.items[self.choice][0],
                      self.items[self.choice][1],
                      self.items[self.choice][2])
            # Если kwargs был подан, то вызываем с доп. аргументами
            if self.items[self.choice][3]:
                self.items[self.choice][1](*self.items[self.choice][3], **self.items[self.choice][2])
            elif self.items[self.choice][2]:
                self.items[self.choice][1](**self.items[self.choice][2])
            else:
                self.items[self.choice][1]()
        # Если выбранный пункт - назад, отладка, главное меню
        elif self.choice in self.default_items:
            log.debug("Исполнение действия стандартной функции [%s, %s()]",
                      self.default_items[self.choice][0],
                      self.default_items[self.choice][1])
            self.default_items[self.choice][1]()

    def add_item(self, title, func=None, kwargs=None, *args):
        """
        Добавляет новый пункт меню.
        """
        log.debug("Добавление пункта меню")
        log.debug("title: {}, func: {}, kwargs: {}, args: {}".format(title,
                                                                     func,
                                                                     kwargs,
                                                                     args))

        # Если не подан kwargs
        kwargs = dict() if not kwargs else kwargs

        # Если в title подан list или tuple
        if isinstance(title, list) or isinstance(title, tuple):
            title = " | ".join(title)
        self.items[str(len(self.items) + 1)] = [title, func, kwargs, args] if func else title
        return self.items[str(len(self.items))]

    def delete_item(self, key):
        """
        Удаляет пункт меню по ключу.
        :param key: Ключ меню (номер, буква пункта)
        :return:
        """
        log.debug("Удаление пункта меню")
        del self.items[key]

    def add_list_with_function(self, items_list, function, list_of_kwargs, *args):
        """
        Добавляет пункты меню из списка, каждый пункт ассоциирован с одной функцией вызова, но с разными аргументами.
        Принимает только именованные аргументы (словарь).
        Пример:
            ("АПК0", action_apk, {"parent_apk": "APK0", "exist": False})
        :param items_list: Список, из которого создаются пункты меню
        :type items_list: list, tuple
        :param function: Функция вызова, единая для всех пунктов меню
        :param list_of_kwargs: Список аргументов(словарей) для функции (очерёдность должна соответствовать items_list
        """
        # list нужен для того, чтобы map запустил все итерации
        list(map(self.add_item,
                 items_list,
                 itertools.repeat(function, len(items_list)),
                 list_of_kwargs,
                 *args))

    def add_list_with_instance(self, items_list, instance, method, list_of_kwargs, *args):
        """
        Добавляет пункты меню из списка, каждый пункт ассоциирован с одной функцией вызова, но с разными аргументами.
        Принимает только именованные аргументы (словарь).
        Пример:
            ("АПК0", action_apk, {"parent_apk": "APK0", "exist": False})
        :param items_list: Список, из которого создаются пункты меню
        :type items_list: list, tuple
        :param function: Функция вызова, единая для всех пунктов меню
        :param list_of_kwargs: Список аргументов(словарей) для функции (очерёдность должна соответствовать items_list
        """
        log.debug("items_list: {}, instance: {}, method: {}, list_of_kwargs: {}".format(items_list,
                                                                                        instance,
                                                                                        method,
                                                                                        list_of_kwargs))
        for num, item in enumerate(items_list):
            ins = instance(**list_of_kwargs[num])
            self.add_item(item, getattr(ins, method))

    @staticmethod
    def _quit():
        """
        Функция выхода из программы.
        """
        yes_or_no = None
        while not yes_or_no:
            MenuUtils.clear_terminal()

            try:
                yes_or_no = input("Вы уверены, что хотите выйти? [Y/N, Д/Н] ")
            except KeyboardInterrupt:
                continue

        if yes_or_no[0].lower() == 'y' or yes_or_no[0].lower() == 'д':
            MenuUtils.clear_terminal()
            sys.exit()


class DialogsAPK(OverLoadStr):
    """
    Пространство имён диалогов, меню и действий с АПК.
    Суффикс action_ - подразумевает исполнение действий без какого-либо вмешательства пользователя.
    Суффикс dialog_ - прежде, чем произойдёт какое-либо действие, требуется ввод пользователя (согласие, вопрос и т.д.)
    Суффикс menu_ - выводит дополнительное меню
    """

    def __init__(self, apk_name=None):
        self.apk_name = apk_name

    @staticmethod
    def dialog_create_apk():
        """
        Диалог для создания АПК.
        """
        log.debug("Диалог")
        try:
            apk_name = MenuUtils.input_apk_name()
        except MenuError as err:
            log.error(err)
            MenuUtils.output_pause()
            return
        except MenuInputAbort:
            return

        log.debug("Аргументы создания АПК. apk_name: %s", apk_name)
        apk = APK(name=apk_name).create()

        # Обновляем меню после создания
        menu.create_spo_menu.add_item(apk_name, DialogsSPO(parent_apk=apk_name).dialog_create_spo)
        menu.delete_apk.add_item(apk_name, DialogsAPK(apk_name).dialog_delete_apk)
        menu.open_apk_menu.add_item(apk_name, DialogsAPK(apk_name).dialog_open_apk)
        log.debug("Объект АПК: %s", apk)

        log.info("АПК {} успешно создано.".format(apk.name))
        MenuUtils.output_pause()

    def dialog_open_apk(self):
        """
        Диалог открытия АПК.
        """
        menu.action_apk_menu.items = dict()
        actions = [("Переименовать АПК(в разработке)", self.dialog_rename_apk),
                   ("Переместить АПК(в разработке)", self.dialog_replace_apk),
                   ("Вывести информацию", self.action_info_apk),
                   ("Создать отчёт(в разработке)", self.dialog_generate_report_apk)]

        for act in actions:
            menu.action_apk_menu.add_item(act[0], act[1])

        menu.action_apk_menu()

    @staticmethod
    def dialog_rename_apk():
        """
        Диалог для переименовывания АПК.
        """
        log.debug("Диалог")
        ...

    @staticmethod
    def dialog_replace_apk():
        """
        Диалог для переименовывания АПК.
        """
        log.debug("Диалог")
        ...

    @staticmethod
    def dialog_generate_report_apk():
        """
        Диалог для переименовывания АПК.
        """
        log.debug("Диалог")
        ...

    def action_info_apk(self):
        """
        Действие для переименовывания АПК.
        """
        log.debug("Действие")
        APK(self.apk_name).print()
        MenuUtils.output_pause()

    def dialog_delete_apk(self):
        """
        Диалог удаления АПК.
        """
        log.debug("Диалог")

        try:
            MenuUtils.question_delete_apk()
        except MenuInputAbort:
            return
        except MenuYesAnswer:
            try:
                APK(self.apk_name).delete()
            except APKError as err:
                log.error(err)
                MenuUtils.output_pause()
                return

            # Удаление пунктов меню
            key = [key for key, value in menu.create_spo_menu.items.items() if value[0] == self.apk_name]
            menu.create_spo_menu.delete_item(key[0])

            key = [key for key, value in menu.delete_apk.items.items() if value[0] == self.apk_name]
            menu.delete_apk.delete_item(key[0])

            key = [key for key, value in menu.open_apk_menu.items.items() if value[0] == self.apk_name]
            menu.open_apk_menu.delete_item(key[0])

            key = [key for key, value in menu.open_spo_menu.items.items() if value[0].split(' | ')[1] == self.apk_name]
            for k in key:
                menu.open_spo_menu.delete_item(k)

            key = [key for key, value in menu.delete_spo.items.items() if value[0].split(' | ')[1] == self.apk_name]
            for k in key:
                menu.delete_spo.delete_item(k)

            log.info("АПК {} успешно удалено.".format(self.apk_name))
            MenuUtils.output_pause()


class DialogsSPO(OverLoadStr):
    """
    Пространство имён диалогов, меню и действий с СПО.
    """
    warning_extra_menu = False

    def __init__(self, spo_name=None, parent_apk=None):
        self.parent_apk = parent_apk
        self.spo_name = spo_name

    def dialog_create_spo(self):
        """
        Диалог для создания СПО.
        """
        log.debug("Диалог")
        try:
            spo_name = MenuUtils.input_spo_name()
            level = MenuUtils.input_level()
            languages = MenuUtils.input_languages()
        except MenuError as err:
            log.error(err)
            MenuUtils.output_pause()
            return
        except MenuInputAbort:
            return

        log.debug("Аргументы создания СПО. spo_name: %s, parent_apk: %s, level: %s, languages: %s",
                  spo_name, self.parent_apk, level, languages)
        spo = SPO(name=spo_name,
                  parent_apk=self.parent_apk,
                  level=level,
                  languages=languages).create()

        # Обновляем меню после создания
        menu.open_spo_menu.add_item((spo_name, self.parent_apk), DialogsSPO(spo_name, self.parent_apk).menu_open_spo)
        menu.delete_spo.add_item((spo_name, self.parent_apk), DialogsSPO(spo_name, self.parent_apk).dialog_delete_spo)
        log.debug("Объект СПО: %s", spo)

        log.info("СПО {} с родительским АПК {} успешно создано.".format(spo.name, spo.parent_apk))
        MenuUtils.output_pause()

    def menu_open_spo(self):
        """
        Меню открытия СПО.
        :return:
        """
        log.debug("Меню")
        menu.action_spo_menu.items = dict()
        actions = [("Статический анализ", self.action_static_analyze_spo),
                   ("Динамический анализ(в разработке)", self.action_dynamic_analyze_spo),
                   ("Вывести информацию", self.action_info_spo),
                   ("Переименовать(в разработке)", self.dialog_rename_spo),
                   ("Переместить(в разработке)", self.dialog_replace_spo),
                   ("Сгенерировать отчёт(в разработке)", self.dialog_generate_report_spo),
                   ("Дополнительно...", self.menu_extra_actions_spo)]

        for act in actions:
            menu.action_spo_menu.add_item(act[0], act[1])

        menu.action_spo_menu()

    @staticmethod
    def dialog_rename_spo():
        """
        Диалог для переименовывания АПК.
        """
        log.debug("Диалог")
        ...

    @staticmethod
    def dialog_replace_spo():
        """
        Диалог для переименовывания АПК.
        """
        log.debug("Диалог")
        ...

    @staticmethod
    def dialog_generate_report_spo():
        """
        Диалог для переименовывания АПК.
        """
        log.debug("Диалог")

        ...

    def action_info_spo(self):
        """
        Действие для переименовывания АПК.
        """
        log.debug("Действие")
        SPO(self.spo_name, self.parent_apk).print()
        MenuUtils.output_pause()

    def action_static_analyze_spo(self):
        """
        Действие для статического анализа.
        """
        log.debug("Действие")
        MenuUtils.clear_terminal()
        try:
            spo = SPO(self.spo_name, self.parent_apk).open()
            spo_static = Static(spo)
            spo_static.run()
        except Error as err:
            log.error(err)
            log.info("Следует исправить ошибки и повторить запуск.")
            MenuUtils.output_pause()
            return
        except KeyboardInterrupt:
            log.error("Статический анализ был прерван пользователем!")
            MenuUtils.output_pause()
            return
        else:
            log.info("Статический анализ успешно завершён.")
            MenuUtils.output_pause()

    @staticmethod
    def action_dynamic_analyze_spo():
        """
        Диалог для динамического анализа.
        """
        log.debug("Диалог")
        ...

    def menu_extra_actions_spo(self):
        """
        Меню с дополнительными действиями для СПО.
        """
        log.debug("Меню")
        if not self.warning_extra_menu:
            print("Запуская этот пункт меню, Вы уверены в том, что знаете, что делаете!")
            MenuUtils.output_pause()
            self.warning_extra_menu = True

        menu.extra_action_spo_menu.items = dict()
        actions = [("Скопировать исходные данные в лабораторные", self.dialog_copy_src_to_lab_spo),
                   ("Удалить лабораторные данные", self.dialog_delete_lab_spo),
                   ("Вставить статические маркеры", self.action_insert_markers_spo),
                   ("Проверить вставку статических маркеров", self.action_check_markers_spo),
                   # ("Перекодировать исходные данные в UTF-8", self.src_to_utf8_spo),
                   ("Реформатировать исходные данные (AStyle)", self.action_reformat_source_spo),
                   ("Поиск языков программирования в проекте", self.dialog_search_languages_spo),
                   ("Добавить язык программирования", self.dialog_add_language_spo),
                   ("Удалить язык программирования", self.menu_delete_language_spo),
                   ("Подсчитать размер исходных данных", self.action_size_of_source_files),
                   ("Собрать информацию об исходных данных", self.menu_collect_info_spo)]

        for act in actions:
            menu.extra_action_spo_menu.add_item(act[0], act[1])

        menu.extra_action_spo_menu()

    def action_reformat_source_spo(self):
        """
        Действие реформатирования исходных данных и копирование их в оригинальные.
        """
        log.debug("Действие")
        spo = SPO(self.spo_name, self.parent_apk).open()
        spo_prepare = Prepare(spo)

        try:
            spo_prepare.reformat_astyle_src()
        except Error as err:
            log.error(err)
            MenuUtils.output_pause()
            return
        except KeyboardInterrupt:
            log.error("Реформатирование исходных данных было прервано пользователем!")
            MenuUtils.output_pause()
            return
        else:
            log.info("Реформатирование исходных данных успешно завершено.")
            MenuUtils.output_pause()

    def action_insert_markers_spo(self):
        """
        Действие для вставки маркеров.
        """
        log.debug("Действие")
        MenuUtils.clear_terminal()
        try:
            spo = SPO(self.spo_name, self.parent_apk).open()
            spo_static = Static(spo)
            spo_static.insert_markers()
        except Error as err:
            log.error(err)
            MenuUtils.output_pause()
            return
        except KeyboardInterrupt:
            log.error("Статический анализ был прерван пользователем!")
            MenuUtils.output_pause()
            return
        else:
            log.info("Подготовка исходных данных успешно завершена.")
            MenuUtils.output_pause()

    def action_check_markers_spo(self):
        """
        Действие для проверки вставки маркеров.
        """
        log.debug("Действие")
        MenuUtils.clear_terminal()
        try:
            spo = SPO(self.spo_name, self.parent_apk).open()
            spo_static = Static(spo)
            spo_static.check_markers()
        except Error as err:
            log.error(err)
            MenuUtils.output_pause()
            return
        except KeyboardInterrupt:
            log.error("Статический анализ был прерван пользователем!")
            MenuUtils.output_pause()
            return
        else:
            log.info("Подготовка исходных данных успешно завершена.")
            MenuUtils.output_pause()

    # TODO реализовывать журналирование или обновлять информацию полностью?
    def dialog_collect_info(self, type_of_checksum):
        """
        Диалог сбора информации об исходных данных и записи её в БД.
        """
        log.debug("Диалог")
        try:
            MenuUtils.question_save_db_spo()
        except MenuInputAbort:
            return
        except MenuYesAnswer:
            spo = SPO(self.spo_name, self.parent_apk).open()
            spo_info = CollectInfo(spo, type_of_checksum)

            try:
                spo_info.save_db()
            except Error as err:
                log.error(err)
                MenuUtils.output_pause()
                return
            except database.IntegrityError as err:
                log.error(err)
                MenuUtils.output_pause()
                return
            except KeyboardInterrupt:
                log.error("Получение информации об исходных данных было прервано пользователем!")
                MenuUtils.output_pause()
                return
            else:
                log.info("Получение информации об исходных данных успешно завершено.")
                MenuUtils.output_pause()

    def dialog_delete_lab_spo(self):
        """
        Диалог удаления лабораторных данных.
        """
        log.debug("Диалог")
        try:
            MenuUtils.question_delete_lab_directory()
        except MenuInputAbort:
            return
        except MenuYesAnswer:
            spo = SPO(self.spo_name, self.parent_apk).open()
            spo_prepare = Prepare(spo)

            try:
                spo_prepare.delete_lab(save_dir=True)
            except Error as err:
                log.error(err)
                MenuUtils.output_pause()
                return
            except KeyboardInterrupt:
                log.error("Удаление лабораторных данных было прервано пользователем!")
                MenuUtils.output_pause()
                return
            else:
                log.info("Удаление лабораторных данных успешно завершено.")
                MenuUtils.output_pause()

    def dialog_copy_src_to_lab_spo(self):
        """
        Диалог копирования исходных данных в лабораторные.
        """
        log.debug("Диалог")
        try:
            MenuUtils.question_delete_lab_directory()
        except MenuInputAbort:
            return
        except MenuYesAnswer:
            spo = SPO(self.spo_name, self.parent_apk).open()
            spo_prepare = Prepare(spo)

            try:
                spo_prepare.copy_source_to_lab()
            except Error as err:
                log.error(err)
                MenuUtils.output_pause()
                return
            except KeyboardInterrupt:
                log.error("Копирование данных было прервано пользователем!")
                MenuUtils.output_pause()
                return
            else:
                log.info("Копирование исходных данных успешно завершено.")
                MenuUtils.output_pause()

    def action_src_to_utf8_spo(self):
        """
        Действие перекодирования исходных данных в кодировку utf-8.
        """
        log.debug("Действие")
        spo = SPO(self.spo_name, self.parent_apk).open()
        spo_prepare = Prepare(spo)

        try:
            spo_prepare.src_to_utf8()
        except Error as err:
            log.error(err)
            MenuUtils.output_pause()
            return
        except KeyboardInterrupt:
            log.error("Перекодирование данных было прервано пользователем!")
            MenuUtils.output_pause()
            return
        else:
            log.info("Перекодирование исходных данных успешно завершено.")
            MenuUtils.output_pause()

    def dialog_delete_spo(self):
        """
        Диалог удаления СПО.
        """
        log.debug("Диалог")

        try:
            MenuUtils.question_delete_spo()
        except MenuInputAbort:
            return
        except MenuYesAnswer:
            try:
                SPO(self.spo_name, self.parent_apk).delete()
            except Error as err:
                log.error(err)
                MenuUtils.output_pause()
                return

            # Удаление пунктов меню
            key = [key for key, value in menu.open_spo_menu.items.items()
                   if value[0].split(' | ')[0] == self.spo_name and value[0].split(' | ')[1] == self.parent_apk]
            menu.open_spo_menu.delete_item(key[0])

            key = [key for key, value in menu.delete_spo.items.items()
                   if value[0].split(' | ')[0] == self.spo_name and value[0].split(' | ')[1] == self.parent_apk]
            menu.delete_spo.delete_item(key[0])

            log.info("Удаление СПО успешно завершено")
            MenuUtils.output_pause()

    def dialog_search_languages_spo(self):
        """
        Диалог поиска языков программирования в проекте.
        """
        log.debug("Диалог")
        spo = SPO(self.spo_name, self.parent_apk).open()
        spo_prepare = Prepare(spo)

        try:
            files_languages = spo_prepare.search_languages()
        except Error as err:
            log.error(err)
            MenuUtils.output_pause()
            return
        except KeyboardInterrupt:
            log.error("Поиск языков был прерван пользователем!")
            MenuUtils.output_pause()
            return

        # Диалог добавления нового языка
        for language in files_languages:
            if language not in spo.languages:
                MenuUtils.clear_terminal()
                print("Были найдены следующие файлы с незарегистрированным языком {}:".format(language))
                self._output_text_while_not_interrupt(files_languages[language])
                try:
                    MenuUtils.question_add_language()
                except MenuYesAnswer:
                    log.debug("Пользователь согласился")
                    self.dialog_add_language_spo(language)
                except MenuInputAbort:
                    return

        # Диалог удаления лишнего языка
        extra_languages = list()
        for language in spo.languages:
            if language not in files_languages:
                extra_languages.append(language)

        if extra_languages:
            MenuUtils.clear_terminal()
            print("Были найдены следующие языки в СПО, которые не найдены в исходных данных")
            self._output_text_while_not_interrupt(extra_languages)
            try:
                MenuUtils.question_delete_language()
            except MenuYesAnswer:
                log.debug("Пользователь согласился")
                for language in extra_languages:
                    self.dialog_delete_language_spo(language)
            except MenuInputAbort:
                return

        log.debug(spo.languages)

    def action_size_of_source_files(self):
        """
        Действие получения размера исходных данных.
        """
        log.debug("Действие")
        spo = SPO(self.spo_name, self.parent_apk).open()
        spo_prepare = Prepare(spo)

        try:
            size_of_source_files = spo_prepare.get_size_of_source()
            for language in sorted(size_of_source_files, key=lambda x: size_of_source_files[x][0]):
                if language != "ALL":
                    log.info("Размер исходных файлов языка {} равен: {}".format(language,
                                                                                size_of_source_files[language][1]))
                else:
                    log.info("Размер ВСЕХ исходных данных равен: {}".format(size_of_source_files["ALL"][1]))
            MenuUtils.output_pause()
        except Error as err:
            log.error(err)
            MenuUtils.output_pause()
            return
        except KeyboardInterrupt:
            log.error("Подсчёт размера исходных данных был прерван пользователем!")
            MenuUtils.output_pause()
            return

    @staticmethod
    def _output_text_while_not_interrupt(list_text):
        """
        Вспомогательный метод, выводит строчку за строчкой по нажатию на <Enter>, выходит по прерыванию с клавиатуры.
        :param list_text: Список текста, который требуется вывести
        """
        print("Для выхода нажмите ^C (Ctrl+C)")
        print("Для вывода следующей строки нажмите <Enter>")
        for line in list_text:
            print(line)
            try:
                input()
            except KeyboardInterrupt:
                return

    def dialog_add_language_spo(self, language=None):
        """
        Диалог добавления языка программирования в СПО.
        :return:
        """
        log.debug("Диалог")

        if not language:
            try:
                language = MenuUtils.input_language()
            except MenuInputAbort:
                return

        spo = SPO(self.spo_name, self.parent_apk).open()

        try:
            spo.add_language(language)
        except Error as err:
            log.error(err)
            MenuUtils.output_pause()
            return
        except KeyboardInterrupt:
            log.error("Добавление языка в СПО было прервано пользователем!")
            MenuUtils.output_pause()
            return
        else:
            log.info("Язык {} успешно добавлен".format(language))
            MenuUtils.output_pause()

    def dialog_delete_language_spo(self, language=None):
        """
        Диалог удаления языка программирования из СПО.
        """
        log.debug("Диалог")

        spo = SPO(self.spo_name, self.parent_apk).open()

        try:
            spo.delete_language(language)
            key = [key for key, value in menu.delete_language_spo_menu.items.items() if value[0] == language]
            if key:
                menu.delete_language_spo_menu.delete_item(key[0])
        except Error as err:
            log.error(err)
            MenuUtils.output_pause()
            return
        except KeyboardInterrupt:
            log.error("Удаление языка в СПО было прервано пользователем!")
            MenuUtils.output_pause()
            return
        else:
            log.info("Язык {} успешно удалён".format(language))
            MenuUtils.output_pause()

    def menu_delete_language_spo(self):
        """
        Меню удаления языка программирования из СПО.
        """
        log.debug("Меню")

        spo = SPO(self.spo_name, self.parent_apk).open()

        menu.delete_language_spo_menu.items = dict()
        menu.delete_language_spo_menu.add_list_with_function(spo.languages,
                                                             self.dialog_delete_language_spo,
                                                             [{"language": lang} for lang in spo.languages])

        menu.delete_language_spo_menu()

    def menu_collect_info_spo(self):
        """
        Меню сбора информации об исходных данных. Выбор типа хэш-функции.
        """
        log.debug("Меню")

        menu.collect_info_spo_menu.items = dict()
        menu.collect_info_spo_menu.add_list_with_function(CHECKSUM_TYPE,
                                                          self.dialog_collect_info,
                                                          [{"type_of_checksum": chksum} for chksum in CHECKSUM_TYPE])

        menu.collect_info_spo_menu()


def users_input(message):
    """
    Функция-декоратор пользовательского ввода.
    :param message: Сообщение, выводимое при пользовательском вводе
    :return: Функция
    """

    def inner_generator(func):

        def inner(*args, **kwargs):

            MenuUtils.clear_terminal()

            result = None
            while not result:
                try:
                    answer = input(message)
                except KeyboardInterrupt:
                    raise MenuInputAbort("Операция была прервана пользователем.")

                while not answer:
                    print("Введена пустая строка! Пожалуйста повторите ввод.")
                    try:
                        answer = input(message)
                    except KeyboardInterrupt:
                        raise MenuInputAbort("Операция была прервана пользователем.")

                log.debug("Ввод: %s", answer)

                args_list = list(args)
                args_list.append(answer)

                result = func(*args_list, **kwargs)
            return result

        return inner

    return inner_generator


class MenuUtils:
    """
    Пространство имён вспомогательных функций для работы с меню.
    """

    @staticmethod
    @users_input("Введите наименование АПК: ")
    def input_apk_name(apk_name, exist=True):
        """
        Функция ввода наименования АПК.
        :param apk_name: Имя АПК
        :type apk_name: str
        :param exist: Если True, то проверка на присутствие, если False - на отсутствие.
        :type exist: bool
        :return: None - если такой АПК уже есть в БД конфигурации, иначе apk_name
        """
        if APK(apk_name).exist() and exist:
            raise MenuError("АПК {} уже существует в БД конфигурации.".format(apk_name))
        elif not APK(apk_name).exist() and not exist:
            raise MenuError("АПК {} не существует в БД конфигурации.".format(apk_name))
        return apk_name

    @staticmethod
    @users_input("Введите наименование СПО: ")
    def input_spo_name(spo_name, parent_apk=None):
        """
        Функция ввода наименования СПО.
        :param spo_name: Имя СПО
        :param parent_apk: Родительский АПК
        :return:
        """
        if parent_apk:
            if SPO(spo_name, parent_apk).exist():
                raise MenuError("СПО {} в родительском АПК {} уже"
                                " существует в БД конфигурации.".format(spo_name, parent_apk))
            return spo_name, parent_apk
        else:
            return spo_name

    @staticmethod
    @users_input("Введите список СПО (через запятую): ")
    def input_list_spo(list_spo):
        """
        Функция ввода списка СПО.
        :param list_spo: Имя СПО
        :return:
        """
        return list_spo

    @staticmethod
    @users_input("Введите уровень контроля ({}): ".format(", ".join(LEVELS)))
    def input_level(level):
        """
        Функция ввода уровня контроля СПО.
        :param level: Уровень контроля
        :return:
        """
        if level not in LEVELS:
            print("Неверный ввод. Повторите.")
            return
        return level

    @staticmethod
    @users_input("Введите список языков (запятая - разделитель)\n{}\n: ".format(", ".join(sorted(LANGUAGES))))
    def input_languages(languages):
        """
        Функция ввода списка языков СПО.
        :param languages: Список языков
        :return:
        """
        bad_languages = True in [True for lang in languages.split(',') if
                                 clear_string(lang).upper() not in LANGUAGES]
        if bad_languages:
            print("Неверный ввод. Повторите.")
            return
        return languages.split(",")

    @staticmethod
    @users_input("Введите язык программирования\n{}\n: ".format(", ".join(sorted(LANGUAGES))))
    def input_language(language):
        """
        Функция ввода языка программирования СПО.
        :param language: Язык программирования
        :type language: str
        :return:
        """
        bad_language = False if language.upper() in LANGUAGES else True
        if bad_language:
            print("Неверный ввод. Повторите.")
            return
        return language

    @staticmethod
    @users_input("Вы уверены, что хотите полностью удалить СПО? [Y/N, Д/Н] ")
    def question_delete_spo(yes_or_no):
        """
        Функция ввода согласия для удаления СПО.
        :param yes_or_no: Пользовательский ввод
        :type yes_or_no: str
        """
        if yes_or_no[0].lower() == 'y' or yes_or_no[0].lower() == 'д':
            raise MenuYesAnswer
        return "any_string"

    @staticmethod
    @users_input("Был обнаружен язык, который отсутствует в СПО, хотите его добавить? [Y/N, Д/Н] ")
    def question_add_language(yes_or_no):
        """
        Функция ввода согласия для добавления отсутствующего языка СПО.
        :param yes_or_no: Пользовательский ввод
        :type yes_or_no: str
        """
        if yes_or_no[0].lower() == 'y' or yes_or_no[0].lower() == 'д':
            raise MenuYesAnswer
        return "any_string"

    @staticmethod
    @users_input("В СПО были обнаружены языки, которые отсутствуют в исходных данных, хотите их удалить? [Y/N, Д/Н] ")
    def question_delete_language(yes_or_no):
        """
        Функция ввода согласия для удаления лишнего языка СПО.
        :param yes_or_no: Пользовательский ввод
        :type yes_or_no: str
        """
        if yes_or_no[0].lower() == 'y' or yes_or_no[0].lower() == 'д':
            raise MenuYesAnswer
        return "any_string"

    @staticmethod
    @users_input("Вы уверены, что хотите полностью удалить АПК со всеми дочерними СПО? [Y/N, Д/Н] ")
    def question_delete_apk(yes_or_no):
        """
        Функция ввода согласия для удаления АПК.
        :param yes_or_no: Пользовательский ввод
        :type yes_or_no: str
        """
        if yes_or_no[0].lower() == 'y' or yes_or_no[0].lower() == 'д':
            raise MenuYesAnswer
        return "any_string"

    @staticmethod
    @users_input("Папка с лабораторными данными может быть не пуста, все данные будут УДАЛЕНЫ, Вы уверены? [Y/N, Д/Н] ")
    def question_delete_lab_directory(yes_or_no):
        """
        Функция ввода согласия для копирования исходных данных в лабораторные.
        :param yes_or_no: Пользовательский ввод
        :type yes_or_no: str
        """
        if yes_or_no[0].lower() == 'y' or yes_or_no[0].lower() == 'д':
            raise MenuYesAnswer
        return "any_string"

    @staticmethod
    @users_input("Все имеющиеся данные из БД будут УДАЛЕНЫ, Вы уверены? [Y/N, Д/Н] ")
    def question_save_db_spo(yes_or_no):
        """
        Функция ввода согласия для обновления БД исходных данных.
        :param yes_or_no: Пользовательский ввод
        :type yes_or_no: str
        """
        if yes_or_no[0].lower() == 'y' or yes_or_no[0].lower() == 'д':
            raise MenuYesAnswer
        return "any_string"

    @staticmethod
    def stub():
        """
        Функция-заглушка для меню.
        """
        pass

    @staticmethod
    def get_list_apk():
        """
        Получить список АПК из БД конфигурации.
        """
        try:
            list_apk = APK.get_list_apk()
        except DatabaseConfigError as err:
            log.warning(err)
            return list()
        else:
            return list_apk

    @staticmethod
    def get_list_spo(parent_apk=None):
        """
        Получить список СПО из БД конфигурации.
        """
        try:
            if parent_apk:
                list_spo = SPO.get_list_spo(parent_apk)
            else:
                list_spo = SPO.get_list_spo()
        except DatabaseConfigError as err:
            log.warning(err)
            return list()
        else:
            log.debug("Получили СПО %s", list_spo)
            return list_spo

    @staticmethod
    def clear_terminal():
        """
        Функция очистки терминала, если уровень логера > INFO.
        :return:
        """

        if log.getEffectiveLevel() > 10:
            os.system(['clear', 'cls'][os.name == 'nt'])
        else:
            log.debug("Экран очищен.")

    @staticmethod
    def output_pause():
        """
        Останавливает вывод терминала до тех пор, пока пользователь не нажмёт на <Enter>
        """
        try:
            input("Нажмите <Enter> для продолжения...")
        except KeyboardInterrupt:
            return


# TODO Правильно ли сделано? Перенести в отдельный модуль и вызывать оттуда?

class MenuItems:
    """
    Класс создан для создания пространств имён меню.
    При создании нового меню, убедитесь, что все пункты меню расположены в правильном порядке.
    Если пункты меню зависят друг от друга при создании, то сначала создаём меню,
    а позже добавляем в него пункты с помощью метода Menu.add_item
    Будьте внимательны при прикреплении функций к пунктам меню:
    - Если функция затрагивает элементы меню, то следует добавить в функцию обновление меню (создание, удаление)
    - Если требуется добавить несколько пунктов меню с одной функцией,
    используйте метод - Menu.add_list_with_function
    """

    def __init__(self):
        self.list_apk = MenuUtils.get_list_apk()
        self.list_spo = MenuUtils.get_list_spo()

        log.debug("Инициализация меню")
        # Отладочное меню
        self.debug_menu = Menu(name="debug_menu",
                               title="Отладочное меню",
                               items=[("Печать БД конфигурации", APK.debug_print_all),
                                      ("Создание тестового проекта с исходными данными", APK.debug_create_test)])

        # Главное меню
        self.main_menu = Menu(name="main_menu",
                              title="Главное меню",
                              go_back=False,
                              debug_menu=self.debug_menu)

        # Меню "Создать..."
        self.create_menu = Menu(name="create_menu",
                                title="Создать...",
                                debug_menu=self.debug_menu)

        # Меню "Создать..." -> "СПО"
        self.create_spo_menu = Menu(name="create_spo_menu",
                                    title="Выберите родительский АПК",
                                    go_main_menu=True,
                                    main_menu=self.main_menu,
                                    debug_menu=self.debug_menu)
        self.create_spo_menu.add_list_with_instance(self.list_apk, DialogsSPO, "dialog_create_spo",
                                                    [{"parent_apk": apk} for apk in self.list_apk])

        # Пункты меню "Создать..."
        self.create_menu.add_item("АПК", DialogsAPK().dialog_create_apk)
        self.create_menu.add_item("СПО", self.create_spo_menu)

        # Меню "Открыть..."
        self.open_menu = Menu(name="open_menu",
                              title="Открыть...",
                              debug_menu=self.debug_menu)

        # Меню "Открыть..." -> "АПК"
        self.open_apk_menu = Menu(name="open_apk_menu",
                                  title="Выберите АПК для открытия",
                                  go_main_menu=True,
                                  main_menu=self.main_menu,
                                  debug_menu=self.debug_menu)

        # Меню "Открыть..." -> "АПК" -> ИМЯ_АПК
        self.action_apk_menu = Menu(name="action_apk_menu",
                                    title="Выберите действие для АПК",
                                    go_main_menu=True,
                                    main_menu=self.main_menu,
                                    debug_menu=self.debug_menu)
        self.open_apk_menu.add_list_with_instance(self.list_apk,
                                                  DialogsAPK, "dialog_open_apk",
                                                  [{"apk_name": apk} for apk in self.list_apk])

        # Меню "Открыть..." -> "СПО"
        self.open_spo_menu = Menu(name="open_spo_menu",
                                  title="Выберите СПО для открытия",
                                  go_main_menu=True,
                                  main_menu=self.main_menu,
                                  debug_menu=self.debug_menu)

        # Меню "Открыть..." -> "СПО" -> ИМЯ_СПО
        self.action_spo_menu = Menu(name="action_spo_menu",
                                    title="Выберите действие для СПО",
                                    go_main_menu=True,
                                    main_menu=self.main_menu,
                                    debug_menu=self.debug_menu)
        self.open_spo_menu.add_list_with_instance(self.list_spo,
                                                  DialogsSPO, "menu_open_spo",
                                                  [{"parent_apk": apk, "spo_name": spo} for spo, apk in
                                                   self.list_spo])

        # Меню "Открыть..." -> "СПО" -> ИМЯ_СПО -> "Дополнительно..."
        self.extra_action_spo_menu = Menu(name="extra_action_spo_menu",
                                          title="Выберите дополнительное действие для СПО",
                                          go_main_menu=True,
                                          main_menu=self.main_menu,
                                          debug_menu=self.debug_menu)

        # Меню "Открыть..." -> "СПО" -> ИМЯ_СПО -> "Дополнительно..." -> "Удалить язык программирования"
        self.delete_language_spo_menu = Menu(name="delete_language_spo_menu",
                                             title="Выберите язык для удаления",
                                             go_main_menu=True,
                                             main_menu=self.main_menu,
                                             debug_menu=self.debug_menu)

        # Меню "Открыть..." -> "СПО" -> ИМЯ_СПО -> "Дополнительно..." -> "Собрать информацию о файлах"
        self.collect_info_spo_menu = Menu(name="collect_info_spo_menu",
                                          title="Выберите тип хэш-функции",
                                          go_main_menu=True,
                                          main_menu=self.main_menu,
                                          debug_menu=self.debug_menu)

        # Пункты меню "Открыть..."
        self.open_menu.add_item("АПК", self.open_apk_menu)
        self.open_menu.add_item("СПО", self.open_spo_menu)

        # Меню "Удалить..."
        self.delete_menu = Menu(name="delete_menu",
                                title="Удалить...",
                                debug_menu=self.debug_menu)

        # Меню "Удалить..." -> "АПК"
        self.delete_apk = Menu(name="delete_apk_menu",
                               title="Выберите АПК для удаления",
                               go_main_menu=True,
                               main_menu=self.main_menu,
                               debug_menu=self.debug_menu)
        self.delete_apk.add_list_with_instance(self.list_apk, DialogsAPK, "dialog_delete_apk",
                                               [{"apk_name": apk} for apk in self.list_apk])

        # Меню "Удалить..." -> "СПО"
        self.delete_spo = Menu(name="delete_spo_menu",
                               title="Выберите СПО для удаления",
                               go_main_menu=True,
                               main_menu=self.main_menu,
                               debug_menu=self.debug_menu)
        self.delete_spo.add_list_with_instance(self.list_spo,
                                               DialogsSPO, "dialog_delete_spo",
                                               [{"parent_apk": apk, "spo_name": spo} for spo, apk in
                                                self.list_spo])
        # Пункты меню "Удалить..."
        self.delete_menu.add_item("АПК", self.delete_apk)
        self.delete_menu.add_item("СПО", self.delete_spo)

        # Пункты главного меню
        self.main_menu.add_item("Создать...", self.create_menu)
        self.main_menu.add_item("Открыть...", self.open_menu)
        self.main_menu.add_item("Удалить...", self.delete_menu)

        log.debug("Отладочное меню: %s", self.debug_menu)
        log.debug("Главное меню: %s", self.main_menu)
        log.debug("Меню 'Создать...': %s", self.create_menu)
        log.debug("Меню 'Открыть...': %s", self.open_menu)
        log.debug("Меню 'Удалить...': %s", self.delete_menu)
        log.debug("Меню 'Удалить...' -> 'АПК': %s", self.delete_apk)
        log.debug("Меню 'Удалить...' -> 'СПО': %s", self.delete_spo)
        log.debug("Меню 'Открыть...' -> 'АПК': %s", self.open_apk_menu)
        log.debug("Меню 'Открыть...' -> 'АПК' -> ИМЯ АПК: %s", self.action_apk_menu)
        log.debug("Меню 'Открыть...' -> 'СПО': %s", self.open_spo_menu)
        log.debug("Меню 'Открыть...' -> 'СПО' -> ИМЯ СПО: %s", self.action_spo_menu)

    def __call__(self):
        self.main_menu()


menu = MenuItems()

if __name__ == "__main__":
    try:
        import logger

        logger.setup_logging()
        log = logging.getLogger(__name__)
    except ImportError:
        logging.basicConfig(level="DEBUG")

    menu()

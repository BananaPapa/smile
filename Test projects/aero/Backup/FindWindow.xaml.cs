﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AeroNotes
{
    /// <summary>
    /// Interaction logic for FindWindow.xaml
    /// </summary>
    public partial class FindWindow : Window
    {
        MainUI MainWindow;

        public MainUI MainForm
        {
            set { MainWindow = value; }
        }

        public FindWindow()
        {
            InitializeComponent();

            FindCanvas.MouseDown += new MouseButtonEventHandler(FindCanvas_MouseDown);
            ReplaceCanvas.MouseDown += new MouseButtonEventHandler(ReplaceCanvas_MouseDown);
        }

        //apply aero effect to entire window
        protected override void OnSourceInitialized(EventArgs e)
        {
            base.OnSourceInitialized(e);
            AeroAccess.ExtendGlassFrame(this, new Thickness(-1));
        }

        void ReplaceCanvas_MouseDown(object sender, MouseButtonEventArgs e)
        {
            MainWindow.tbEditor.Text = MainWindow.tbEditor.Text.Replace(FindText.Text, ReplaceText.Text);

            this.Close();
        }

        void FindCanvas_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (MainWindow.tbEditor.Text.IndexOf(FindText.Text) != -1)
            {
                MainWindow.tbEditor.SelectionStart = MainWindow.tbEditor.Text.IndexOf(FindText.Text);
                MainWindow.tbEditor.SelectionLength = FindText.Text.Length;
            }
            else
            {
                MessageBox.Show("Cannot Find: \"" + FindText.Text + "\"");
            }//if

            this.Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            FindText.Focus();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Forms;
using System.IO;
using MessageBox=System.Windows.MessageBox;
using TextBox=System.Windows.Controls.TextBox;
using Clipboard = System.Windows.Clipboard;

namespace AeroNotes
{

    public partial class MainUI : Window
    {
        FileDialog objSave = new SaveFileDialog();
        FileDialog objOpen = new OpenFileDialog();
        ApplicationSettings Config;
        MainUI MainWindow;
        string strPath = string.Empty;
        string strOriginal = string.Empty;
        bool bModified = false;

        public string FilePath
        {
            get { return strPath; }
            set { strPath = value; }
        }
        
        public MainUI()
        {
            InitializeComponent();

            InitializeWindow();
        }

        //apply aero effect to entire window
        protected override void OnSourceInitialized(EventArgs e)
        {
            base.OnSourceInitialized(e);
            AeroAccess.ExtendGlassFrame(this, new Thickness(-1));
        }

        private void InitializeWindow()
        {
            tbEditor.Focus();

            MainWindow = this;
            Config = new ApplicationSettings();

            Config.InitializeSettings(ref MainWindow, true);

            tbEditor.KeyUp += new System.Windows.Input.KeyEventHandler(tbEditor_KeyUp);
            objOpen.FileOk += new System.ComponentModel.CancelEventHandler(objOpen_FileOk);
            objSave.FileOk += new System.ComponentModel.CancelEventHandler(objSave_FileOk);
            this.Closing += new System.ComponentModel.CancelEventHandler(MainUI_Closing);

            //lousy hack for hibernation issue - need to research resume more...
            this.Activated += new EventHandler(MainUI_Activated);

            tbEditor.Focus();
            this.Activate();
        }


        #region event handlers

        void MainUI_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Modified();
            
            try
            {
                UpdateState();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        void MainUI_Activated(object sender, EventArgs e)
        {
            AeroAccess.ExtendGlassFrame(this, new Thickness(-1));
        }
        
        void PreferencesMenuItem_Click(object sender, RoutedEventArgs e)
        {
            MainUI MainWindow = this;
            ConfWindow Config = new ConfWindow(ref MainWindow);

            Config.Show();
        }

        void OpenMenuItem_Click(object sender, RoutedEventArgs e)
        {
            objOpen.ShowDialog();
            OpenFile();
        }

        void NewMenuItem_Click(object sender, RoutedEventArgs e)
        {
            strPath = string.Empty;
            tbEditor.Text = string.Empty;
        }

        void SaveMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (strPath != string.Empty)
            {
                SaveFile();
            }
            else
            {
                objSave.ShowDialog();
                SaveFile();
            }
        }

        void SaveAsMenuItem_Click(object sender, RoutedEventArgs e)
        {
            objSave.ShowDialog();
            SaveFile();
        }

        void FindMenuItem_Click(object sender, RoutedEventArgs e)
        {
            FindWindow objFind = new FindWindow();
            objFind.MainForm = this;
            objFind.Show();
        }

        void tbEditor_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            KeyEvents(e);
        }

        void objOpen_FileOk(object sender, System.ComponentModel.CancelEventArgs e)
        {
            strPath = objOpen.FileName;
        }

        void objSave_FileOk(object sender, System.ComponentModel.CancelEventArgs e)
        {
            strPath = objSave.FileName;
        }

        private void CutMenuItem_Click(object sender, RoutedEventArgs e)
        {
            tbEditor.Cut();
        }

        private void CopyMenuItem_Click(object sender, RoutedEventArgs e)
        {
            tbEditor.Copy();
        }

        private void PasteMenuItem_Click(object sender, RoutedEventArgs e)
        {
            tbEditor.Paste();
        }
        #endregion

        private void UpdateState()
        {
            MainUI MainWindow = this;
            ApplicationSettings WindowConfiguration = new ApplicationSettings();

            WindowConfiguration.InitializeSettings(ref this.MainWindow, false);

            WindowConfiguration.UpdateSettings(false, null);
        }                                                           

        public void OpenFile()
        {
            if (strPath != string.Empty)
            {
                StreamReader objReader = new StreamReader(strPath);
                tbEditor.Text = objReader.ReadToEnd();
                tbEditor.SelectionStart = 0;
                tbEditor.SelectionLength = 0;
                objReader.Close();

                strOriginal = tbEditor.Text;
            }//if
        }

        private void KeyEvents(System.Windows.Input.KeyEventArgs e)
        {
            if (Keyboard.Modifiers == ModifierKeys.Control && e.Key == System.Windows.Input.Key.O)
            {
                objOpen.ShowDialog();
                OpenFile();
            }//if

            if (Keyboard.Modifiers == ModifierKeys.Control && e.Key == System.Windows.Input.Key.S)
            {
                if (strPath != string.Empty)
                {
                    SaveFile();
                }
                else
                {
                    objSave.ShowDialog();
                    SaveFile();
                }
            }//if

            if (Keyboard.Modifiers == ModifierKeys.Control && e.Key == System.Windows.Input.Key.F)
            {
                FindWindow objFind = new FindWindow();
                objFind.MainForm = this;
                objFind.Show();
            }//if

            if (e.Key == System.Windows.Input.Key.Escape)
            {
                Modified();
            }//if
        }

        public void SaveFile()
        {
            if (strPath != string.Empty)
            {
                StreamWriter objWriter = new StreamWriter(strPath);
                objWriter.Write(tbEditor.Text);
                objWriter.Close();

                strOriginal = tbEditor.Text;
            }//if
        }

        void Modified()
        {
            if (tbEditor.Text == strOriginal)
            {
                System.Windows.Forms.Application.Exit();
            }
            else
            {
                if (System.Windows.Forms.MessageBox.Show("File has changed. Would you like to save?", "Save File?", MessageBoxButtons.OKCancel) == System.Windows.Forms.DialogResult.OK)
                {
                    if (strPath == string.Empty)
                    {
                        objSave.ShowDialog();
                        SaveFile();
                    }
                    else
                    {
                        SaveFile();
                    }//if
                }//if
            }//if
        }
    }
}

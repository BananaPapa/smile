﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AeroNotes
{
    /// <summary>
    /// Interaction logic for ConfWindow.xaml
    /// </summary>
    public partial class ConfWindow : Window
    {
        private MainUI MainWindow;

        public ConfWindow(ref MainUI MainWindow)
        {
            this.MainWindow = MainWindow;

            InitializeComponent();

            this.Loaded += new RoutedEventHandler(ConfWindow_Loaded);
            SaveCanvas.MouseDown += new MouseButtonEventHandler(SaveCanvas_MouseDown);
        }

        //apply aero effect to entire window
        protected override void OnSourceInitialized(EventArgs e)
        {
            base.OnSourceInitialized(e);
            AeroAccess.ExtendGlassFrame(this, new Thickness(-1));
        }

        void ConfWindow_Loaded(object sender, RoutedEventArgs e)
        {
            FontSelector.ItemsSource = Fonts.SystemFontFamilies;

            FontSelector.Focus();
        }

        void SaveCanvas_MouseDown(object sender, MouseButtonEventArgs e)
        {
            ApplicationSettings Config = new ApplicationSettings();
            ConfWindow CurrentWindow = this;

            Config.InitializeSettings(ref this.MainWindow, false);

            try
            {
                Config.UpdateSettings(true, CurrentWindow);
            }
            catch
            {
                MessageBox.Show("Error occured while applying settings." + Environment.NewLine + "Make sure all options has a value.");
            }

            this.Close();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;
using System.Xml;
using System.Reflection;
using System.Configuration;

namespace AeroNotes
{
    /// <summary>
    /// This class saves and updates control & form properties.
    /// </summary>
    public class ApplicationSettings
    {
        private string SettingsFilePath = string.Empty;
        private MainUI MainWindow;

        public ApplicationSettings()
        {
            //default settings for local user app data folder
            string XML = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>";
            XML = XML + "<configuration>";
	        XML = XML + "    <appSettings>";
		    XML = XML + "        <add key=\"FontName\" value=\"Arial\"/>";
		    XML = XML + "        <add key=\"FontColor\" value=\"Black\"/>";
		    XML = XML + "        <add key=\"FontSize\" value=\"12\"/>";
		    XML = XML + "        <add key=\"WindowLeft\" value=\"320\"/>";
		    XML = XML + "        <add key=\"WindowTop\" value=\"240\"/>";
		    XML = XML + "        <add key=\"Width\" value=\"320\"/>";
		    XML = XML + "        <add key=\"Height\" value=\"240\"/>";
		    XML = XML + "        <add key=\"FirstRun\" value=\"True\"/>";
	        XML = XML + "    </appSettings>";
            XML = XML + "</configuration>";

            if(!System.IO.Directory.Exists(System.Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\AeroNotes"))
            {
                System.IO.Directory.CreateDirectory(System.Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\AeroNotes");
            }

            if(!System.IO.File.Exists(System.Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\AeroNotes\\settings.xml"))
            {
                System.IO.StreamWriter sw = System.IO.File.CreateText(System.Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\AeroNotes\\settings.xml");

                sw.Write(XML);

                sw.Close();
            }

            SettingsFilePath = System.Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\AeroNotes\\settings.xml";
        }

        public void InitializeSettings(ref MainUI MainWindow, bool ApplySettings)
        {
            this.MainWindow = MainWindow;

            if (ApplySettings)
            {
                SetSettings();
            }
        }

        /// <summary>
        /// Assign window and control properties.
        /// </summary>
        private void SetSettings()
        {
            try
            {
                BrushConverter Converter = new BrushConverter();
                
                MainWindow.tbEditor.FontFamily = new FontFamily(ReadSetting("FontName"));                
                MainWindow.tbEditor.Foreground = (Brush)Converter.ConvertFromString(ReadSetting("FontColor"));
                MainWindow.tbEditor.FontSize = double.Parse(ReadSetting("FontSize"));

                MainWindow.FileMenu.FontFamily = new FontFamily(ReadSetting("FontName"));
                MainWindow.FileMenu.Foreground = (Brush)Converter.ConvertFromString(ReadSetting("FontColor"));
                MainWindow.FileMenu.FontSize = double.Parse(ReadSetting("FontSize"));

                MainWindow.EditMenu.FontFamily = new FontFamily(ReadSetting("FontName"));
                MainWindow.EditMenu.Foreground = (Brush)Converter.ConvertFromString(ReadSetting("FontColor"));
                MainWindow.EditMenu.FontSize = double.Parse(ReadSetting("FontSize"));

                MainWindow.Width = double.Parse(ReadSetting("Width"));
                MainWindow.Height = double.Parse(ReadSetting("Height"));

                //center scrren on first run
                if (ReadSetting("FirstRun") == "True")
                {
                    MainWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;

                    WriteSetting("FirstRun", "False");
                }
                else
                {
                    MainWindow.Left = double.Parse(ReadSetting("WindowLeft"));
                    MainWindow.Top = double.Parse(ReadSetting("WindowTop"));
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message + Environment.NewLine + e.StackTrace);

                MessageBox.Show("Error loading settings. Please ensure the settings have the correct values. A possible uninstall/reinstall may be needed");
            }
        }


        /// <summary>
        /// Update settings upon change or appllication exit.
        /// </summary>
        /// <param name="UpdateFont"></param>
        /// <param name="ConfWindow"></param>
        public void UpdateSettings(bool UpdateFont, ConfWindow ConfWindow)
        {
            try
            {
                //new font settings?
                if (UpdateFont)
                {
                    WriteSetting("FontName", ConfWindow.FontSelector.Text);
                    WriteSetting("FontColor", ConfWindow.ColorSelector.Text);
                    WriteSetting("FontSize", ConfWindow.SizeSelector.Text);

                    BrushConverter Converter = new BrushConverter();

                    MainWindow.tbEditor.FontFamily = new FontFamily(ConfWindow.FontSelector.Text);
                    MainWindow.tbEditor.Foreground = (Brush)Converter.ConvertFromString(ConfWindow.ColorSelector.Text);
                    MainWindow.tbEditor.FontSize = double.Parse(ConfWindow.SizeSelector.Text);

                    MainWindow.FileMenu.FontFamily = new FontFamily(ConfWindow.FontSelector.Text);
                    MainWindow.FileMenu.Foreground = (Brush)Converter.ConvertFromString(ConfWindow.ColorSelector.Text);
                    MainWindow.FileMenu.FontSize = double.Parse(ConfWindow.SizeSelector.Text);

                    MainWindow.EditMenu.FontFamily = new FontFamily(ConfWindow.FontSelector.Text);
                    MainWindow.EditMenu.Foreground = (Brush)Converter.ConvertFromString(ConfWindow.ColorSelector.Text);
                    MainWindow.EditMenu.FontSize = double.Parse(ConfWindow.SizeSelector.Text);
                }

                WriteSetting("WindowLeft", MainWindow.Left.ToString());
                WriteSetting("WindowTop", MainWindow.Top.ToString());
                WriteSetting("Width", MainWindow.Width.ToString());
                WriteSetting("Height", MainWindow.Height.ToString());
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message + Environment.NewLine + e.StackTrace);

                throw new Exception("Error writing settings. Possible unistall/reinstall may be needed.", e);
            }
        }

        private string ReadSetting(string key)
        {
            XmlDocument doc = SettingsDocument();

            XmlNode node = doc.SelectSingleNode("//appSettings");

            XmlElement elem = (XmlElement)node.SelectSingleNode(string.Format("//add[@key='{0}']", key));
           
            return elem.GetAttribute("value");
        }

        private void WriteSetting(string key, string value)
        {
            XmlDocument doc = SettingsDocument();

            XmlNode node = doc.SelectSingleNode("//appSettings");

            XmlElement elem = (XmlElement)node.SelectSingleNode(string.Format("//add[@key='{0}']", key));

            elem.SetAttribute("value", value);

            doc.Save(SettingsFilePath);
        }

        private XmlDocument SettingsDocument()
        {
            XmlDocument doc = null;

            doc = new XmlDocument();
            
            doc.Load(SettingsFilePath);

            return doc;
        }
    }
}

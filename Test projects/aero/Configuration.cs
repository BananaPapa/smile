﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;
using System.Xml;
using System.Reflection;
using System.Configuration;

namespace AeroNotes
{
    class Configuration
    {
        private static string ApplicationPath = System.Reflection.Assembly.GetExecutingAssembly().Location.Replace("AeroNotes.exe", string.Empty);
        private MainUI MainWindow;

        public Configuration(ref MainUI MainWindow)
        {
            Initialize(ref MainWindow);
        }

        public Configuration()
        {
            //default constructor
        }

        private void Initialize(ref MainUI MainWindow)
        {
            this.MainWindow = MainWindow;

            GetConfiguration();
        }

        private void GetConfiguration()
        {
            try
            {
                XmlTextReader objXmlTextReader = new XmlTextReader(ApplicationPath + "settings.xml");
                string Name = string.Empty;
                bool bFirst = false;

                MainWindow.tbEditor.FontFamily = new FontFamily(ConfigurationManager.AppSettings["FontName"]);

                BrushConverter Converter = new BrushConverter();
                
                MainWindow.tbEditor.Foreground = (Brush)Converter.ConvertFromString(ConfigurationManager.AppSettings["FontColor"]);
                
                MainWindow.tbEditor.FontSize = double.Parse(ConfigurationManager.AppSettings["FontSize"]);                
                
                MainWindow.Width = double.Parse(ConfigurationManager.AppSettings["WindowWidth"]);
                
                MainWindow.Height = double.Parse(ConfigurationManager.AppSettings["WindowHeight"]);

                if (ConfigurationManager.AppSettings["FirstRun"] == "True")
                {
                    MainWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                }
                else
                {
                    MainWindow.Left = double.Parse(ConfigurationManager.AppSettings["WindowLeft"]);

                    MainWindow.Top = double.Parse(ConfigurationManager.AppSettings["WindowTop"]);
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message + Environment.NewLine + e.StackTrace);

                MessageBox.Show("Error loading settings. Please ensure the settings have the correct values.");
            }
        }

        public static void WriteSetting(string key, string value)
        {
            // load config document for current assembly
            XmlDocument doc = loadConfigDocument();

            // retrieve appSettings node
            XmlNode node = doc.SelectSingleNode("//appSettings");

            if (node == null)
                throw new InvalidOperationException("appSettings section not found in config file.");

            try
            {
                // select the 'add' element that contains the key
                XmlElement elem = (XmlElement)node.SelectSingleNode(string.Format("//add[@key='{0}']", key));

                if (elem != null)
                {
                    // add value for key
                    elem.SetAttribute("value", value);
                }
                else
                {
                    // key was not found so create the 'add' element 
                    // and set it's key/value attributes 
                    elem = doc.CreateElement("add");
                    elem.SetAttribute("key", key);
                    elem.SetAttribute("value", value);
                    node.AppendChild(elem);
                }
                doc.Save(getConfigFilePath());
            }
            catch(Exception e)
            {
                throw new Exception("Error saving settings.", e);
            }
        }

        private static XmlDocument loadConfigDocument()
        {
            XmlDocument doc = null;
            try
            {
                doc = new XmlDocument();
                doc.Load(getConfigFilePath());
                return doc;
            }
            catch (System.IO.FileNotFoundException e)
            {
                throw new Exception("No configuration file found.", e);
            }
        }

        private static string getConfigFilePath()
        {
            return Assembly.GetExecutingAssembly().Location + ".config";
        }



        public void Update(bool UpdateFont, ConfWindow ConfWindow)
        {
            try
            {  
                #region xml start
                    XmlTextWriter objXmlTextWriter = new XmlTextWriter(ApplicationPath +  "settings.xml",null);
                    
                    objXmlTextWriter.Formatting = Formatting.Indented;
                    
                    objXmlTextWriter.WriteStartDocument();
                    objXmlTextWriter.WriteStartElement("AeroNotes");
                #endregion
                
                #region font
                    //new font settings?
                    if(UpdateFont)
                    {
                        objXmlTextWriter.WriteStartElement("FontName");
                        objXmlTextWriter.WriteString(ConfWindow.FontSelector.Text);
                        objXmlTextWriter.WriteEndElement();
                        objXmlTextWriter.WriteStartElement("FontColor");
                        objXmlTextWriter.WriteString(ConfWindow.ColorSelector.Text);
                        objXmlTextWriter.WriteEndElement();
                        objXmlTextWriter.WriteStartElement("FontSize");
                        objXmlTextWriter.WriteString(ConfWindow.SizeSelector.Text); 
                        objXmlTextWriter.WriteEndElement();

                        BrushConverter Converter = new BrushConverter();

                        MainWindow.tbEditor.FontFamily = new FontFamily(ConfWindow.FontSelector.Text);
                        MainWindow.tbEditor.Foreground = (Brush)Converter.ConvertFromString(ConfWindow.ColorSelector.Text);
                        MainWindow.tbEditor.FontSize = double.Parse(ConfWindow.SizeSelector.Text);
                    }
                    else
                    {
                        objXmlTextWriter.WriteStartElement("FontName");
                        objXmlTextWriter.WriteString(MainWindow.tbEditor.FontFamily.ToString());
                        objXmlTextWriter.WriteEndElement();
                        objXmlTextWriter.WriteStartElement("FontColor");
                        objXmlTextWriter.WriteString(MainWindow.tbEditor.Foreground.ToString());
                        objXmlTextWriter.WriteEndElement();
                        objXmlTextWriter.WriteStartElement("FontSize");
                        objXmlTextWriter.WriteString(MainWindow.tbEditor.FontSize.ToString()); 
                        objXmlTextWriter.WriteEndElement();
                    }
                #endregion

                #region window size and location
                    objXmlTextWriter.WriteStartElement("WindowLeft");
                    objXmlTextWriter.WriteString(MainWindow.Left.ToString());
                    objXmlTextWriter.WriteEndElement();
                    objXmlTextWriter.WriteStartElement("WindowsTop");
                    objXmlTextWriter.WriteString(MainWindow.Top.ToString());
                    objXmlTextWriter.WriteEndElement();
                    objXmlTextWriter.WriteStartElement("WindowWidth");
                    objXmlTextWriter.WriteString(MainWindow.Width.ToString()); 
                    objXmlTextWriter.WriteEndElement();
                    objXmlTextWriter.WriteStartElement("WindowHeight");
                    objXmlTextWriter.WriteString(MainWindow.Height.ToString()); 
                    objXmlTextWriter.WriteEndElement();
                #endregion

                #region xml end
                    objXmlTextWriter.WriteEndElement();
                    objXmlTextWriter.WriteEndDocument();
                    objXmlTextWriter.Flush();
                    objXmlTextWriter.Close();
                #endregion
            }
            catch(Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message + Environment.NewLine + e.StackTrace);

                MessageBox.Show("Application Failure. This application will close. If you continue to see this message please reinstall it.");
            }
        }
    }
}

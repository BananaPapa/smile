AeroNotes By Tensai Labs

With the release of Microsoft's latest operating system, Vista, 
applications interfaces may take a completely new approach 
leveraging the famed DirectX platform through the use of
Windows Presentation Foundation.

AeroNotes is a simple notepad alternative using the new Aero 
Glass technology over the entire form using the new interface 
approach mentioned.

http://www.tensailabs.com/



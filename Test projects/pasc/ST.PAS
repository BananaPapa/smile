{  ��ᬮ�� ��ॢ� ��⠫����, ��稭�� � ⥪�饣� }
{  (C) �.���फ��, 1992 }
{  �������, ⥫. (817-22) 2-01-62, (817-22) 4-61-77 }

program ShowTree;

uses
   Dos;

const
   Step  = 3;  { ����� �� �뢮�� }

var
   Level       : integer;        { �஢��� �����⠫��� }
   DirCount    : word;           { ���稪 ������⢠ ��⠫���� }
   ContLevels  : set of 0..31;   { ������⢮ �������襭��� �஢��� }

procedure ShowCurrDir(  var DirCount : word );
   { �����ᨢ�� ��ᬮ�� ⥪�饣� ��⠫��� }

var
   Info           : SearchRec;
   TotalDir, DC   : integer;
   i              : integer;

begin
   with Info do begin

   { C�⠥� �����⠫��� }

      DC := 0;
      FindFirst( '*.*', Directory, Info );
      while DosError = 0 do begin
         if ( Attr = Directory ) and (  Name <> '.' ) and ( Name <> '..' )
         then
            DC := DC + 1;
         FindNext( Info );
      end;
      TotalDir := DC;
      DirCount := DirCount + DC;

   { ������ �뢮��� ��ॢ� }

      DC := 0;
      FindFirst( '*.*', Directory, Info );
      while DosError = 0 do begin
         if ( Attr = Directory ) and (  Name <> '.' ) and ( Name <> '..' )
         then begin
            DC := DC + 1;
            if  DC = TotalDir then
               ContLevels := ContLevels - [Level]
            else
               ContLevels := ContLevels + [Level];
            for i := 1 to Level-1 do
               if i in ContLevels then
                  Write('�  ')
               else
                  Write('   ');
            if Level in ContLevels then
               WriteLn( '���',Name )
            else
               WriteLn( '���',Name );
            ChDir( Name );
            Level := Level + 1;
            { ����� ४���� }
               ShowCurrDir( DirCount );
            Level := Level - 1;
            ChDir('..');
         end;
         FindNext( Info );
      end;
   end;
end;

begin
   WriteLn;
   WriteLn;
   WriteLn('ShowTree: ��ᬮ�� ��ॢ�  ��⠫����, ��稭�� � ⥪�饣�.');
   Writeln('(C) �.���फ��, 1991');
   Level := 1;
   ContLevels:= [];
   DirCount  := 0;
   ShowCurrDir( DirCount );
   Writeln;
   WriteLn('�����⠫���� ', DirCount : 10 );
end.




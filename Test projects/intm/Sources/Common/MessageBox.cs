﻿using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Eureca.Utility.Extensions;

namespace Eureca.Integrator.Utility.Forms
{
    /// <summary>
    ///     Represents an alernate message box.
    /// </summary>
    public static class MessageBox
    {
        public static void ShowInfo(string text)
        {
            ShowInfo("Информация", text);
        }

        /// <summary>
        ///     Shows message box.
        /// </summary>
        /// <param name="text">Message text.</param>
        /// <param name="caption">Message box caption.</param>
        /// <param name="owner">Message box owner.</param>
        /// <param name="icon">Message box icon.</param>
        /// <param name="buttons">Message box buttons.</param>
        /// <param name="defaultButton">Message box default button.</param>
        /// <returns>
        ///     An instance of <see cref="T:System.Windows.Standarts.DialogResult" />.
        /// </returns>
        public static DialogResult Show(string text, string caption, IWin32Window owner = null,
            MessageBoxIcon icon = MessageBoxIcon.None,
            MessageBoxButtons buttons = MessageBoxButtons.OK,
            MessageBoxDefaultButton defaultButton = MessageBoxDefaultButton.Button1)
        {
            var control = owner as Control;

            if (control != null && control.InvokeRequired)
            {
                return
                    (DialogResult)
                        control.Invoke(
                            new Func
                                <string, string, IWin32Window, MessageBoxIcon, MessageBoxButtons,
                                    MessageBoxDefaultButton,
                                    DialogResult>(Show), text, caption, owner, icon, buttons, defaultButton);
            }

            return XtraMessageBox.Show(owner, text, caption, buttons, icon, defaultButton);
        }

        /// <summary>
        ///     Shows validate error message.
        /// </summary>
        /// <param name="text">Error message.</param>
        /// <param name="owner">Message box owner.</param>
        public static void ShowValidationError(string text, IWin32Window owner = null)
        {
            ShowExclamation("Ошибка валидации", text, owner);
        }

        /// <summary>
        ///     Shows error.
        /// </summary>
        /// <param name="text">Error text.</param>
        /// <param name="owner">Message box owner.</param>
        /// <param name="exception">
        ///     Instance of <see cref="T:System.Exception" /> to get error text from.
        /// </param>
        /// <param name="detailed">Whether to show detailed information about the exception.</param>
        public static void ShowError(string text, IWin32Window owner = null, Exception exception = null,
            bool detailed = false)
        {
            if (exception != null)
            {
                text += Environment.NewLine + exception.GetErrorText(detailed);
            }

            Show(text, "Ошибка", owner, MessageBoxIcon.Error);
        }

        /// <summary>
        ///     Shows unhandled application error.
        /// </summary>
        /// <param name="e">Thrown exception.</param>
        /// <param name="owner">Message box owner.</param>
        public static void ShowUnhandledError(Exception e, IWin32Window owner = null)
        {
            string message =
                "В приложении возникла необработанная ошибка, которая может привести к его нестабильной работе, поэтому рекомендуется перезапустить приложение.";

#if DEBUG
            string error = e.GetErrorText(true);
#else
            string error = e.GetErrorText();
#endif

            if (e != null)
            {
                message += String.Format("{0}{0}Детали:{0}{1}", Environment.NewLine, error);
            }

            message += String.Format("{0}{0}Перезапустить приложение сейчас?", Environment.NewLine);

            if (Show(message, "Необработанная ошибка", owner, MessageBoxIcon.Question, MessageBoxButtons.YesNo,
                MessageBoxDefaultButton.Button2)
                == DialogResult.Yes)
            {
                try
                {
                    Application.Restart();
                }
                catch
                {
                    ShowError("Не удалось перезапустить приложение!");
                }
            }
        }

        /// <summary>
        ///     Shows exclamation message.
        /// </summary>
        /// <param name="caption">Message caption.</param>
        /// <param name="text">Message text.</param>
        /// <param name="owner">Message box owner.</param>
        public static void ShowExclamation(string caption, string text, IWin32Window owner = null)
        {
            Show(text, caption, owner, MessageBoxIcon.Exclamation);
        }

        /// <summary>
        ///     Shows info message.
        /// </summary>
        /// <param name="caption">Message caption.</param>
        /// <param name="text">Message text.</param>
        /// <param name="owner">Message box owner.</param>
        public static void ShowInfo(string caption, string text, IWin32Window owner = null)
        {
            Show(text, caption, owner, MessageBoxIcon.Information);
        }

        /// <summary>
        ///     Shows confirmation message.
        /// </summary>
        /// <param name="question">Question to ask.</param>
        /// <param name="owner">Message box owner.</param>
        /// <returns>Whether user confirms action.</returns>
        public static bool Confirm(string question, IWin32Window owner = null)
        {
            return Show(question, "Подтверждение", owner, MessageBoxIcon.Question, MessageBoxButtons.YesNo,
                MessageBoxDefaultButton.Button2)
                   == DialogResult.Yes;
        }

        /// <summary>
        ///     Shows question message.
        /// </summary>
        /// <param name="question">Question to ask.</param>
        /// <param name="owner">Message box owner.</param>
        /// <returns>Whether user confirms action.</returns>
        public static bool? ConfirmOrCancel(string question, IWin32Window owner = null)
        {
            DialogResult result = Show(question, "Подтверждение", owner, MessageBoxIcon.Question,
                MessageBoxButtons.YesNoCancel,
                MessageBoxDefaultButton.Button3);

            switch (result)
            {
                case DialogResult.Yes:
                    return true;

                case DialogResult.No:
                    return false;
                default:
                    return null;
            }
        }
    }
}
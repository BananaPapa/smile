﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eureca.Updater.Common;

namespace Eureca.Integrator.Utility
{
    public class AppVersion
    {
        public const string ApplicationName = "Eureca.Integrator";

        public const string ApplicationRuName = "Интегратор-М";

        public const int TargetDbVersion = 32;

        public const string ApplicationVersionString = "1.0.1.2";//без релиза
        
        public static ModuleVersion ApplicationVersion = new ModuleVersion( ApplicationVersionString );
    }
}

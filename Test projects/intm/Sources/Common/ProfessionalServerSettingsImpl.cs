﻿using System;
using System.Collections.Generic;
using System.Linq;
using Eureca.Updater.Common;
using Eureca.Updater.UpdaterClient;
using Eureca.Professional.ActiveRecords;
using Eureca.Utility.Extensions;
using log4net;

namespace Eureca.Integrator.Utility
{

    public class ProfessionalServerSettingsImpl : IUpdateSettingStorage
    {
        private ILog Log = LogManager.GetLogger(typeof (ProfessionalServerSettingsImpl));

        const string updateSettingsTemplateString = @"IF NOT EXISTS(SELECT [Value] FROM [SystemProperties] WHERE EnumId = {0})
    BEGIN
        INSERT INTO [SystemProperties](EnumID, Value)
        VALUES({0}, '{1}')
    END
    ELSE
    BEGIN
        UPDATE [SystemProperties]
        SET Value = '{1}'
        WHERE EnumId={0}
    END";

        const string selectSettingTemplateString = "Select [Value] from SystemProperties where EnumID={0}";

        public event EventHandler<OnSettingChangedArgs> SettingsChanged;
    
        int currentVersion = -1;
        string _connectionString;
        public ProfessionalServerSettingsImpl( string connectionString )
        {
            _connectionString = connectionString;
        }

        public string CurriculumConnectionString
        {
            get
            {
                string curConnString = null;
                try
                {
                    curConnString = (string)DatabaseHelper.ExecScalar( "Select [Value] from SystemProperties where EnumID={0}".FormatString( (int)SysPropEnum.CurriculumConnString), _connectionString );
                }
                catch { }
                return curConnString;
            }
            set
            {
                if(value == null)
                    return;
                try
                {
                    DatabaseHelper.ExecScript(
                        updateSettingsTemplateString.FormatString((int) SysPropEnum.CurriculumConnString, value),
                        _connectionString);
                }
                catch (Exception ex)
                {
                    Log.Error( "On set curriculum connection string to professional fail with connstring {0}".FormatString(_connectionString), ex );
                }
            }
        }


        public IList<Eureca.Updater.Common.RepositoryConnectionSetting> RepositorySettings
        {
            get
            {
                string ver = null;
                try
                {
                    ver = (string)DatabaseHelper.ExecScalar( "Select [Value] from SystemProperties where EnumID={0}".FormatString( (int)SysPropEnum.RepositorySettings ), _connectionString );
                }
                catch{}

                if (String.IsNullOrEmpty(ver))
                    return new List<RepositoryConnectionSetting>( );

                Exception ex;
                var settings =
                    (List<Eureca.Updater.Common.RepositoryConnectionSetting>)XmlSerializationHelper.Deserialize( typeof( List<Eureca.Updater.Common.RepositoryConnectionSetting> ), ver, out ex );
                
                if (ex != null)
                    throw ex;
                
                return settings;
            }
            set 
            {
                Exception ex;
                var settingsList = value.ToList();
                var settings = XmlSerializationHelper.Serialize( typeof( List<Eureca.Updater.Common.RepositoryConnectionSetting> ), settingsList, out ex );
                var oldSettings = RepositorySettings;
                
                if (ex != null)
                    throw ex;

                try
                {
                    DatabaseHelper.ExecScript( updateSettingsTemplateString.FormatString( (int)SysPropEnum.RepositorySettings, settings ), _connectionString );
                }
                catch  {return;}

                if (SettingsChanged!=null)
                    SettingsChanged( this, new OnSettingChangedArgs(value, oldSettings) );
               
            }
        }

        public int StorageVersion
        {
            get
            {
                var ver = DatabaseHelper.ExecScalar( selectSettingTemplateString.FormatString( (int)SysPropEnum.DbVersion ), _connectionString );
                currentVersion = ver == null? 0 : int.Parse((string)ver);
                return currentVersion;
            }
            set 
            {
                if(currentVersion == value)
                    return; 
                DatabaseHelper.ExecScript( updateSettingsTemplateString.FormatString( (int)SysPropEnum.DbVersion, value ), _connectionString );
            }
        }

        
    }
}

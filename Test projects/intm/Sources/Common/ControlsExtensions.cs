﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraNavBar;
using DevExpress.XtraRichEdit;

namespace Eureca.Integrator.Extensions
{
    public static class RichTextBoxExtensions
    {
        public static void LoadRtf(this RichTextBox rtb, MemoryStream ms)
        {
            if (rtb.InvokeRequired)
            {
                rtb.Invoke((Action) (() => { rtb.LoadFile(ms, RichTextBoxStreamType.RichText); })
                    );
            }
            else
                rtb.LoadFile(ms, RichTextBoxStreamType.RichText);
        }

        public static void LoadRtf(this RichEditControl rtb, MemoryStream ms)
        {
            if (rtb.InvokeRequired)
            {
                rtb.Invoke((Action) (() => rtb.LoadDocument(ms, DocumentFormat.Rtf)));
            }
            else
            {
                rtb.LoadDocument(ms, DocumentFormat.Rtf);
            }
        }


        public static MemoryStream SaveRtf(this RichEditControl rtb)
        {
            MemoryStream ms = new MemoryStream();
            if (rtb.InvokeRequired)
            {
                rtb.Invoke((Action) (() => rtb.SaveDocument(ms, DocumentFormat.Rtf)));
            }
            else
                rtb.SaveDocument(ms, DocumentFormat.Rtf);
            return ms;
        }

        public static MemoryStream SaveRtf(this RichTextBox rtb)
        {
            MemoryStream ms = new MemoryStream( );
            if (rtb.InvokeRequired)
            {
                rtb.Invoke((Action) (() => { rtb.SaveFile(ms, RichTextBoxStreamType.RichText); })
                    );
            }
            else
                rtb.SaveFile(ms, RichTextBoxStreamType.RichText);
            return ms;
        }

        public static List<T> GetItems<T>(this CheckedListBoxControl oneOfTheVeryStupidDevExpressControls,int itemsCount)
        {
            List<T> items = new List<T>();
            for (int i = 0; i < itemsCount; i++)
            {
               items.Add((T)oneOfTheVeryStupidDevExpressControls.GetItemValue(i));  
            }
            return items;
        }

        public static object GetSelectedItem(this GridView view)
        {
            var selectedIndexes = view.GetSelectedRows();
            if (selectedIndexes.Length == 0)
                return null;
            else
                return view.GetRow(selectedIndexes[0]);
        }

        public static Form FindPlacementForm(this Control control)
        {
            return GetParentForm(control);
        }

        private static Form GetParentForm( Control parent )
        {
            Form form = parent as Form;
            if (form != null)
            {
                return form;
            }
            if (parent != null)
            {
                // Walk up the control hierarchy
                return GetParentForm( parent.Parent );
            }
            return null; // Control is not on a Form
        }

        public static string GetNavBarMenuPath(this DevExpress.XtraNavBar.NavBarItem item )
        {
            var links = item.Links.Cast<DevExpress.XtraNavBar.NavBarItemLink>( ).FirstOrDefault( );
            if (links != null)
            {
                var groupName = links.Group.Tag as string;
                if (groupName != null)
                {
                    return groupName;
                }
            }
            return null;
        }

    }
}

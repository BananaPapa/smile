﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;

namespace Eureca.Integrator.Utility
{
    public static class ServiceExtensions
    {

        public static void StopAndWait( this ServiceController controller )
        {
            controller.Stop( );
            controller.WaitForStatus(
                ServiceControllerStatus.Stopped,
                TimeSpan.FromMinutes( 1 ) );
        }

        public static void StartAndWait( this ServiceController controller )
        {
            controller.Start( );
            controller.WaitForStatus(
                ServiceControllerStatus.Running,
                TimeSpan.FromMinutes( 1 ) );
        }


        #region GetRegistryPath

        private const string SystemRootRegistryPath = @"Software\Microsoft\Windows NT\CurrentVersion\";
        private const string ServicesRegistyPath = @"SYSTEM\CurrentControlSet\Services\";
        private const string ImagePathKey = "ImagePath";
        private const string SystemRootKey = "SystemRoot";

        public static string GetImagePath( this ServiceController controller )
        {
            string registryPath = ServicesRegistyPath + controller.ServiceName;
            RegistryKey keyHklm = Registry.LocalMachine;

            RegistryKey key;
            if (!String.IsNullOrEmpty( controller.MachineName ) && controller.MachineName != ".")
            {
                key = RegistryKey.OpenRemoteBaseKey
                    ( RegistryHive.LocalMachine, controller.MachineName ).OpenSubKey( registryPath );
            }
            else
            {
                key = keyHklm.OpenSubKey( registryPath );
            }

            if (key == null)
            {
                return null;
            }

            string value = key.GetValue( ImagePathKey ).ToString( );
            key.Close( );

            string imagePath = controller.ExpandEnvironmentVariables( value );
            if (imagePath != null)
            {
                imagePath = imagePath.Replace( "\"", String.Empty );
            }

            return imagePath;
        }

        private static string ExpandEnvironmentVariables( this ServiceController controller, string path )
        {
            if (!String.IsNullOrEmpty( controller.MachineName ) && controller.MachineName != ".")
            {
                RegistryKey key = RegistryKey.OpenRemoteBaseKey
                    ( RegistryHive.LocalMachine, controller.MachineName ).OpenSubKey( SystemRootRegistryPath );

                if (key != null)
                {
                    string expandedSystemRoot = key.GetValue( SystemRootKey ).ToString( );
                    key.Close( );

                    path = path.Replace( "%" + SystemRootKey + "%", expandedSystemRoot );
                }

                return path;
            }

            return Environment.ExpandEnvironmentVariables( path );
        }

        #endregion
    }
}

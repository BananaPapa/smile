﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DevExpress.XtraEditors;

namespace Eureca.Integrator.Common
{
    public class PreloadMarqueeForm : XtraForm, IDisposable
    {
        private AutoResetEvent _waitLongTaskEvent = new AutoResetEvent(false);

        protected void StartMarquee(string message)
        {
            Task.Factory.StartNew(() => Eureca.Integrator.Utility.Forms.MarqueeProgressForm.Show(message,
                (form) => { _waitLongTaskEvent.WaitOne(); }, this.Owner));
        }

        protected void CloseMarquee()
        {
            _waitLongTaskEvent.Set();
        }

        #region IDisposable Members

        void IDisposable.Dispose()
        {
            if (_waitLongTaskEvent != null)
            {
                _waitLongTaskEvent.Reset();
                _waitLongTaskEvent.Dispose();
                _waitLongTaskEvent = null;
            }
        }

        #endregion
    }
}
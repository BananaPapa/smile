﻿using Eureca.Integrator.Utility.Forms;
using Eureca.Integrator.Utility.Settings;
using System;
using System.Data;
using System.Data.EntityClient;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Eureca.Utility.Extensions;
using log4net;

namespace Eureca.Integrator.Utility
{
    public static class DatabaseHelper
    {
        private static ILog Log = LogManager.GetLogger(typeof (DatabaseHelper));
       //const string checkConnectionQuery= "Select GETDATE();";
       const string checkCurriculumConnectionQuery = "Select top 1 * from [Settings]";
       const string checkProfessionalConnectionQuery = "Select top 1 * from [SystemProperties]";

       //static string curriculumConnectionString;
       //static string professionalConnectionString;

       //public static string LastSuccessfulConnectionString(IntegratorDb dbName)
       //{
       //    switch (dbName)
       //    {
       //        case IntegratorDb.Curriculum:
       //            return curriculumConnectionString;
       //        case IntegratorDb.Professional:
       //            return professionalConnectionString;
       //        default:
       //            throw new InvalidOperationException("Unknown db name.");
       //    }
       //}

       public static bool CheckConnection(string connectionString,string query)
       {
           SqlConnection conn = null;
           try
           {
               conn = new SqlConnection( connectionString );
               conn.Open();
               var command = conn.CreateCommand();
               command.CommandText = query;
               return command.ExecuteScalar() != null;
           }
           catch
           {
               return false;
           }
           finally
           {
               if (conn != null && conn.State != ConnectionState.Closed)
                   conn.Close();
           }
       }


       /// <summary>
       ///     метод выполнения запроса из файла
       /// </summary>
       /// <param name="fileName">путь к файлу</param>
       /// <returns>результат выполнения</returns>
       public static bool ExecFile(string fileName, string connectionString , out Exception ex)
       {
           ex = null;
           string sqlStr = string.Empty;
           try
           {
               //FileStream cryptedStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
               //						RijndaelManaged RMCrypto = new RijndaelManaged();
               //						CryptoStream csDecrypted = new CryptoStream(cryptedStream, RMCrypto.CreateDecryptor(Key, IV),CryptoStreamMode.Read);
               //						StreamReader sr = new StreamReader(csDecrypted,Encoding.UTF8);

               //открываем  и читаем файл
               string line = null;
               using (var sr = new StreamReader(fileName, Encoding.Default))
               {
                   line = sr.ReadToEnd();
                   sr.Close();
               }
               return ExecScript(line, connectionString);
           }
           catch (Exception exx)
           {
               ex = exx;
               //MessageBox.Show(@"Ошибка чтения файла: ");
               //FileLog.Error(exx);
               return false;
           }
           
           
       }

       public static bool ExecScript(string sqlStr, string connectionString)
       {
           sqlStr = String.Join(" \n", sqlStr.Split(new[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries).Select(item => item.Trim()));
           //для каждого блока делаем выполнение
           int pos = GetNextCommandPos(sqlStr);
           bool successful = true;           
           AggregateException aggrException = new AggregateException();
           using (SqlConnection conn = new SqlConnection(connectionString))
           {
               conn.Open();
               while (pos >= 0 || sqlStr.Length > 0)
               {
                   if (pos < 0)
                   {
                       pos = sqlStr.Length;
                   }
                   string str = sqlStr.Substring(0, pos);
                   sqlStr = sqlStr.Remove(0, Math.Min(pos + 3, sqlStr.Length));
                   pos = GetNextCommandPos(sqlStr);
                   
                    int ret = ExecNonQuery(str, conn);
                    if (ret < -1)
                    {
                        successful = false;
                        //FileLog.Error("sql-script " + fileName + " error");
                    }
               }
           }
           return successful;
       }

       private static int GetNextCommandPos(string sqlStr)
       {
           int pos = -1; //sqlStr.ToLower().IndexOf(" go ");
           if (pos < 0)
           {
               pos = sqlStr.ToLower().IndexOf("\ngo");
           }
           if (sqlStr.ToLower().IndexOf("go ") == 0)
           {
               pos = 0;
           }
           return pos;
       }


       /// <summary>
       ///     метод выполнение запроса
       /// </summary>
       /// <param name="aStringToExecute">строка с запросом</param>
       /// <returns>число затрагиваемых строк</returns>
       public static int ExecNonQuery(String aStringToExecute, SqlConnection conn)
       {
           int ret;
           var cmd = new SqlCommand(aStringToExecute, conn == null ? conn : conn);
           cmd.CommandTimeout = 500;
           ret = cmd.ExecuteNonQuery();
           return ret;
       }

       public static int ExecNonQuery(string query, string connectionString)
       {
           SqlConnection conn = null;
           try
           {
               conn = new SqlConnection(connectionString);
               conn.Open();
               return ExecNonQuery(query, conn);
           }
           catch 
           {
               throw;
           }
           finally
           {
               if(conn != null && conn.State != ConnectionState.Closed)
                   conn.Close();
           }
       }


       public static object ExecScalar( string aStringToExecute , string connectionSting )
       {
           object ret;
           SqlConnection conection = null;
           try
           {
               conection = new SqlConnection( connectionSting );
               conection.Open( );
               var cmd = new SqlCommand( aStringToExecute, conection );
               cmd.CommandTimeout = 500;
               ret = cmd.ExecuteScalar( );
           }
           finally
           {
               if (conection != null && conection.State != ConnectionState.Closed)
               {
                   conection.Close( );
                   conection = null;
               }
           }
           return ret;
       }

       public static DialogResult ChangeConnection( IntegratorDb integratorDb, out string resultConnectionString, Func<string, bool> databaseCheckCallBack = null,
           bool saveConnectionString = true)
       {
           resultConnectionString = null;
           try
           {
                EntityConnectionStringBuilder builder = new EntityConnectionStringBuilder(ConnectionStringManager.GetLocalConnectionString(integratorDb));
                string connectionString = builder.ProviderConnectionString;
               if (String.IsNullOrEmpty( connectionString ))
                   connectionString = String.Format( "Server={0}; Database={1};Trusted_Connection={2};", ".\\sqlexpress2012", integratorDb.GetEnumDescription( ), "True" );
               
               
               var form = new SqlServerConnectionForm(connectionString , databaseCheckCallBack);
               
               DialogResult res = form.ShowDialog();
               
               if (res == DialogResult.OK)
               {
                   resultConnectionString = form.ConnectionString;
                   if (builder.ProviderConnectionString != form.ConnectionString)
                   {
                       builder.ProviderConnectionString = form.ConnectionString;
                       ConnectionStringManager.SaveConnectionString( builder.ConnectionString, integratorDb, saveConnectionString );
                   }
               }
               return res;
           }
           catch (Exception ex)
           {
                Eureca.Integrator.Utility.Forms.MessageBox.ShowError("Ошибка при обращении к конфигурационным файлам.", null,
                   ex);
               return DialogResult.Abort;
           }
       }

       //private static void SetLastConnString( IntegratorDb integratorDb, string connectionString)
       //{
       //    switch (integratorDb)
       //    {
       //        case IntegratorDb.Curriculum:
       //            curriculumConnectionString = connectionString;
       //            break;
       //        case IntegratorDb.Professional:
       //            professionalConnectionString = connectionString;
       //            break;
       //    }
       //}

       public static bool CheckConnection(IntegratorDb integratorDb)
       {
           //try
           //{
               var connectionString = ConnectionStringManager.GetLocalConnectionString( integratorDb );
               if (String.IsNullOrEmpty( connectionString ))
                   return false;
               var builder = new EntityConnectionStringBuilder( connectionString );
               bool checkRes = CheckConnection( builder.ProviderConnectionString, integratorDb == IntegratorDb.Curriculum ? checkCurriculumConnectionQuery : checkProfessionalConnectionQuery );
               //if (checkRes)
               //{
               //    SetLastConnString( integratorDb, builder.ProviderConnectionString );

               //}
               return checkRes;
           //}
           //catch (Exception ex)
           //{
           //    Log.Error(ex.ToString());
           //    return false;
           //}

         
       }
    }
}    


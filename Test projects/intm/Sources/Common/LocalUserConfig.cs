﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Eureca.Utility;
using Eureca.Utility.Synchronization;

namespace Eureca.Integrator.Utility.Settings
{
    [Serializable]
    [XmlRoot]
    public class LocalUserConfig
    {
        const string MutexName = "Eureca.Integrator.Utility.Settings.LocalUserConfig";


        static string _file;

        [XmlIgnore]
        static string _tmpFile;

        [XmlIgnore]
        static string _backupFile;

        [XmlElement] 
        public XmlSerializableDictionary<string, int> PreviousUsers
        {
            get;
            set;
        }

        public LocalUserConfig()
        {
            PreviousUsers = new XmlSerializableDictionary<string, int>();
        }

        static LocalUserConfig( )
        {
            _file = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile), @"AppData\Eureca\Integrator\config.xml");
            _tmpFile = _file + ".tmp";
        }

        public static LocalUserConfig Load( )
        {
            using (new MutexLocker(MutexName))
            {
                try
                {
                    string file;

                    if (!File.Exists(_file))
                    {
                        if (File.Exists(_backupFile))
                            file = _backupFile;
                        else
                            return new LocalUserConfig( );
                    }
                    else
                        file = _file;

                    using (var fileStream = File.OpenRead(file))
                    {
                        XmlSerializer xs = new XmlSerializer( typeof( LocalUserConfig ) );

                        var config = (LocalUserConfig)xs.Deserialize( fileStream );

                        return config;
                    }
                }
                catch (Exception ex)
                {
                    return new LocalUserConfig( );
                }
            }
        }

        public void Save()
        {
            using (new MutexLocker(MutexName))
            {
                var dir = Path.GetDirectoryName(_tmpFile);
                if (!Directory.Exists(dir))
                    Directory.CreateDirectory(dir);
                using (var fileStream = new FileStream(_tmpFile, FileMode.Create, FileAccess.ReadWrite))
                {
                    XmlSerializer xs = new XmlSerializer( typeof( LocalUserConfig ) );

                    xs.Serialize(fileStream, this);
                }

                File.Copy(_tmpFile, _file, true);
                File.Delete(_tmpFile);
            }
        }

    }
}

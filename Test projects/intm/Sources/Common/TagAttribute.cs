﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eureca.Integrator.Common
{

    [AttributeUsage( AttributeTargets.Field, Inherited = false, AllowMultiple = false )]
    public sealed class TagAttribute : Attribute
    {
        private string _tag;

        public string Tag
        {
            get { return _tag; }
        }

        public TagAttribute( string tag )
        {
            _tag = tag;
        }
    }
}

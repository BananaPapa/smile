﻿using System;
using System.Configuration;
using System.Data.Common;
using System.Data.EntityClient;
using System.Data.SqlClient;
using System.Diagnostics;
using Eureca.Integrator.Utility.Forms;

namespace Eureca.Integrator.Utility.Settings
{
    public class ConnectionStringManager
    {
        public const string CurriculumConfigKey = "Curriculum.Properties.Settings.CurriculumConnectionString";
        public const string ProfessionalConfigKey = "Curriculum.Properties.Settings.ProfessionalConnectionString";
        private const string ConnectionStringsSectionName = "connectionStrings";

        public static string GetLocalConnectionString(IntegratorDb integratorDb)
        {
            ConnectionStringSettings connString =
                ConfigurationManager.ConnectionStrings[
                    integratorDb == IntegratorDb.Curriculum ? CurriculumConfigKey : ProfessionalConfigKey];
            return connString == null ? null : connString.ConnectionString;
        }

        //public static CommonConfig GetGlobalConfig( )
        //{
        //    CommonConfig config = CommonConfig.Load( );
        //    return config;
        //}

        public static void SynchronizeConnectionString(IntegratorDb integratorDb)
        {
            string localConnStr = GetLocalConnectionString(integratorDb);

            CommonConfig config = CommonConfig.Load();
            string globalConnString = config.GetConnectionString(integratorDb);
            if (string.IsNullOrWhiteSpace(globalConnString))
            {
                config.SetConnectionString(integratorDb, localConnStr);
                config.Save();
            }
            else if (localConnStr != globalConnString)
            {
                ReplaceLocalConnectionString(globalConnString, integratorDb);
            }
        }

        public static void ReplaceLocalConnectionString(string connectionString, IntegratorDb integratorDb,
            string exePath = null)
        {
            Configuration configuration = exePath == null
                // Current application configuration.
                ? ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None)
                // Another application configuration.
                : ConfigurationManager.OpenExeConfiguration(exePath);
            ConnectionStringSettings connectionStringConfiguration =
                configuration.ConnectionStrings.ConnectionStrings[
                    integratorDb == IntegratorDb.Curriculum ? CurriculumConfigKey : ProfessionalConfigKey];

            connectionStringConfiguration.ConnectionString = connectionString;
            configuration.Save(ConfigurationSaveMode.Modified, true);

            // Current application configuration.
            if (exePath == null)
            {
                ConfigurationManager.RefreshSection(ConnectionStringsSectionName);
                ConfigurationManager.RefreshSection(integratorDb == IntegratorDb.Curriculum
                    ? CurriculumConfigKey
                    : ProfessionalConfigKey);
            }
        }

        public static void SaveConnectionString(string connectionString, IntegratorDb integratorDb,
            bool saveServiceConnectionString = true)
        {
            MarqueeProgressForm.Show(
                "Сохранение настроек...",
                progressForm =>
                {
                    try
                    {
                        CommonConfig config = CommonConfig.Load();
                        config.SetConnectionString(integratorDb, connectionString);
                        config.Save();

                        ReplaceLocalConnectionString(connectionString, integratorDb);
                    }
                    catch (Exception ex)
                    {
                        progressForm.StopAnimation();
                        MessageBox.ShowError("Не удалось сохранить настройки.", null, ex);
                    }
                });
        }

        public static string GetProviderConnectionString(string connectionString)
        {
            return (new  EntityConnectionStringBuilder(connectionString)).ProviderConnectionString;
        }
    }
}
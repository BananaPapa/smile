﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Eureca.Integrator.Common.Controls
{
    public class AutoWidthLayoutPanel:FlowLayoutPanel
    {
        public AutoWidthLayoutPanel()
        {
            this.Layout += AutoWidthLayoutPanel_Layout;
        }

        protected virtual void BeforeLayoutEvent()
        {

        }

        void AutoWidthLayoutPanel_Layout( object sender, LayoutEventArgs e )
        {
            BeforeLayoutEvent();
            var controls = this.Controls;
            if(controls.Count == 0)
                return;

            controls[0].Dock = DockStyle.None;
            controls[0].Width = this.DisplayRectangle.Width - controls[0].Margin.Horizontal;

            for (int i = 1; i < controls.Count; i++)
            {
                controls[i].Dock = DockStyle.Fill;
            } 
        }
    }
}

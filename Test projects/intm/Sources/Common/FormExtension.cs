﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Castle.ActiveRecord;
using DevExpress.XtraBars.Docking2010.Views;

namespace Eureca.Integrator.Extensions
{
    public static class ActiveRecordHelper
    {
        public static bool ShowInTransaction(Func<Form> CreateFormAction , Action<Form> OnSuccess = null,bool isInherit = true,Form owner = null)
        {
            bool res = false;
            try
            {
                using (var tran = isInherit ? new TransactionScope( TransactionMode.Inherits, OnDispose.Commit ) : new TransactionScope( TransactionMode.Inherits))
                {
                    var form = CreateFormAction( );
                    var dialogResult = owner == null ? form.ShowDialog( ) : form.ShowDialog(owner );
                    if (dialogResult == DialogResult.OK)
                    {
                        if (OnSuccess != null)
                            OnSuccess( form );
                        res = true;
                    }
                    else
                    {
                        tran.VoteRollBack( );
                    }
                }
                return res;
            }
            catch (Exception ex)
            {
                return false;
            }
           
        }

        public static bool ShowInSessionScope( Func<Form> CreateFormAction, Action<Form> OnSuccess = null, Form owner = null )
        {
            bool res = false;
            try
            {
                using (var scope = new SessionScope())
                {
                    var form = CreateFormAction( );
                    var dialogResult = owner == null ? form.ShowDialog( ) : form.ShowDialog( owner );
                    if (dialogResult == DialogResult.OK)
                    {
                        if (OnSuccess != null)
                            OnSuccess(form);
                        res = true;
                        scope.Flush();
                    }
                }
                return res;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        //public static bool ShowInTransaction( Func<Form> CreateFormAction, Func<Form,bool> OnSuccess, bool isInherit = true )
        //{
        //    bool res = false;
        //    try
        //    {
        //        using (var tran = isInherit ? new TransactionScope( TransactionMode.Inherits, OnDispose.Commit ) : new TransactionScope( TransactionMode.Inherits ))
        //        {
        //            var form = CreateFormAction( );
        //            var dialogResult = form.ShowDialog( );
        //            if (dialogResult == DialogResult.OK)
        //            {
        //                if (OnSuccess != null)
        //                {
        //                    if (!OnSuccess(form))
        //                    {
        //                        res = false;
        //                        tran.VoteRollBack();
        //                    }
        //                }
                           
        //                //tran.VoteCommit();
        //                res = true;
        //            }
        //            else
        //            {
        //                tran.VoteRollBack( );
        //            }
        //        }
        //        return res;
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }

        //}
    }
}

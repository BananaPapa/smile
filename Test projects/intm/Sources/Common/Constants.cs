﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eureca.Integrator.Common
{
    [Flags]
    public enum PermissionType
    {
        [Tag( "" )]
        None = 0,
        [Tag( "r" )]
        ReadOnly = 1 << 0,
        [Tag( "a" )]
        Add = 1 << 1,
        [Tag( "c" )]
        Change = 1 << 2,
        [Tag( "d" )]
        Delete = 1 << 3
    }

}

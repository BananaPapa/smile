﻿using System;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Eureca.Integrator.Utility.Forms;
using Eureca.Professional.ConnectDBLib;

namespace Eureca.Integrator.Utility.Forms
{
    public partial class SqlServerConnectionForm : XtraForm
    {
        private readonly Func<string, bool> _databaseCheckCallback;
        private readonly connDB _databaseConnectControl;

        public SqlServerConnectionForm(string connectionString, Func<string, bool> databaseCheckCallback)
        {

            _databaseCheckCallback = databaseCheckCallback;
            ConnectionString = connectionString;

            InitializeComponent();

            _databaseConnectControl = new connDB()
            {
                //_okSimpleButton.BackColor
                //BackGroundColor = Color.Empty,
                ConnectionString = connectionString,
                Dock = DockStyle.Top,
                Font =
                    new Font("Tahoma", 9.75F, FontStyle.Regular, GraphicsUnit.Point,
                        ((204))),
                Location = new Point(0, 0),
                Margin = new Padding(4, 4, 4, 4),
                Name = "_databaseConnectControl",
                Padding = new Padding(5),
                Size = new Size(563, 574),
                TabIndex = 0
            };

            SuspendLayout();
            Controls.Add(_databaseConnectControl);
            ResumeLayout(false);
        }

        public string ConnectionString { get; private set; }

        private void SqlServerConnectionFormLoad(object sender, EventArgs e)
        {
            _databaseConnectControl.Initialize(ConnectionString);
        }

        private void OkSimpleButtonClick(object sender, EventArgs e)
        {
            bool success = false;

            MarqueeProgressForm.Show("Проверка соединения...",
                progressForm => { success = _databaseConnectControl.TestConnection; }, this);
            if (success)
            {
                if (_databaseCheckCallback == null || _databaseCheckCallback(_databaseConnectControl.ConnectionString))
                {
                    ConnectionString = _databaseConnectControl.ConnectionString;
                    DialogResult = DialogResult.OK;
                    Close();
                }
                else
                {
                    XtraMessageBox.Show(this, "Выбрана некорректная база данных!", "Ошибка выбора",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                XtraMessageBox.Show(this, "Не удалось подключиться к базе данных!", "Ошибка подключения",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }
}
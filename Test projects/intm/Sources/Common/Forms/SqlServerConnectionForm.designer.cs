﻿namespace Eureca.Integrator.Utility.Forms
{
    partial class SqlServerConnectionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SqlServerConnectionForm));
            this._okSimpleButton = new DevExpress.XtraEditors.SimpleButton();
            this._buttonsPanelControl = new DevExpress.XtraEditors.PanelControl();
            this._cancelSimpleButton = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this._buttonsPanelControl)).BeginInit();
            this._buttonsPanelControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // _okSimpleButton
            // 
            this._okSimpleButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this._okSimpleButton.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._okSimpleButton.Appearance.Options.UseFont = true;
            this._okSimpleButton.Location = new System.Drawing.Point(144, 5);
            this._okSimpleButton.Name = "_okSimpleButton";
            this._okSimpleButton.Size = new System.Drawing.Size(100, 27);
            this._okSimpleButton.TabIndex = 0;
            this._okSimpleButton.Text = "OK";
            this._okSimpleButton.Click += new System.EventHandler(this.OkSimpleButtonClick);
            // 
            // _buttonsPanelControl
            // 
            this._buttonsPanelControl.Controls.Add(this._cancelSimpleButton);
            this._buttonsPanelControl.Controls.Add(this._okSimpleButton);
            this._buttonsPanelControl.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._buttonsPanelControl.Location = new System.Drawing.Point(0, 513);
            this._buttonsPanelControl.Name = "_buttonsPanelControl";
            this._buttonsPanelControl.Size = new System.Drawing.Size(494, 39);
            this._buttonsPanelControl.TabIndex = 1;
            // 
            // _cancelSimpleButton
            // 
            this._cancelSimpleButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this._cancelSimpleButton.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._cancelSimpleButton.Appearance.Options.UseFont = true;
            this._cancelSimpleButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._cancelSimpleButton.Location = new System.Drawing.Point(250, 5);
            this._cancelSimpleButton.Name = "_cancelSimpleButton";
            this._cancelSimpleButton.Size = new System.Drawing.Size(100, 27);
            this._cancelSimpleButton.TabIndex = 0;
            this._cancelSimpleButton.Text = "Отмена";
            // 
            // SqlServerConnectionForm
            // 
            this.AcceptButton = this._okSimpleButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._cancelSimpleButton;
            this.ClientSize = new System.Drawing.Size(494, 552);
            this.Controls.Add(this._buttonsPanelControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SqlServerConnectionForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Параметры подключения к базе данных";
            this.Load += new System.EventHandler(this.SqlServerConnectionFormLoad);
            ((System.ComponentModel.ISupportInitialize)(this._buttonsPanelControl)).EndInit();
            this._buttonsPanelControl.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl _buttonsPanelControl;
        private DevExpress.XtraEditors.SimpleButton _cancelSimpleButton;
        private DevExpress.XtraEditors.SimpleButton _okSimpleButton;
    }
}
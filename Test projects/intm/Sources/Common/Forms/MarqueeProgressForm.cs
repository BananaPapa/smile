﻿using System;
using System.Drawing;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace Eureca.Integrator.Utility.Forms
{
    /// <summary>
    ///     Represents simple marquee progress form with description text.
    /// </summary>
    public partial class MarqueeProgressForm : XtraForm
    {
        /// <summary>
        ///     Timeout of freeze result effect.
        /// </summary>
        private const int FreezeTimeout = 1000;

        /// <summary>
        ///     Encapsulates method that represents action to perform.
        /// </summary>
        private Action<MarqueeProgressForm> _action;

        public Exception Exception { get; set; }

        /// <summary>
        ///     Initializes new instance of marquee progress form.
        /// </summary>
        /// <param name="action">Action to perform.</param>
        /// <param name="description">Action description.</param>
        /// <param name="hidden">Whether form is hidden.</param>
        private MarqueeProgressForm(Action<MarqueeProgressForm> action, string description, bool hidden = false)
        {
            InitializeComponent();

            _action = action;

            Description = description;

            if (hidden)
            {
                StartPosition = FormStartPosition.Manual;
                Location = new Point(-2000, -2000);
            }
        }

        /// <summary>
        ///     Gets or sets progress description.
        /// </summary>
        public string Description
        {
            get { return GetDescription(); }
            set { SetDescription(value ?? String.Empty); }
        }

        /// <summary>
        ///     Stops progress animation.
        /// </summary>
        public void StopAnimation()
        {
            if (InvokeRequired)
            {
                Invoke(new Action(StopAnimation));
            }
            else
            {
                _marqueeProgressBarControl.Properties.Stopped = true;
            }
        }

        /// <summary>
        ///     Starts progress animation.
        /// </summary>
        public void StartAnimation()
        {
            if (InvokeRequired)
            {
                Invoke(new Action(StartAnimation));
            }
            else
            {
                _marqueeProgressBarControl.Properties.Stopped = false;
            }
        }

        /// <summary>
        ///     Stops animation and shows result for a moment.
        /// </summary>
        /// <param name="description">Result description.</param>
        public void FreezeResult(string description)
        {
            StopAnimation();
            Description = description;
            Thread.Sleep(FreezeTimeout);
        }

        /// <summary>
        ///     Gets description.
        /// </summary>
        /// <returns></returns>
        private string GetDescription()
        {
            if (InvokeRequired)
            {
                return (string) Invoke(new Func<string>(GetDescription));
            }

            return _marqueeProgressBarControl.Text;
        }

        /// <summary>
        ///     Sets description.
        /// </summary>
        /// <param name="description">Progress description.</param>
        private void SetDescription(string description)
        {
            if (InvokeRequired)
            {
                Invoke(new Action<string>(SetDescription), description);
            }
            else
            {
                _marqueeProgressBarControl.Text = description;
            }
        }

        /// <summary>
        ///     Closes form.
        /// </summary>
        private void CloseForm()
        {
            if (InvokeRequired)
            {
                Invoke(new Action(CloseForm));
            }
            else
            {
                if (!IsDisposed)
                {
                    try
                    {
                        Close();
                    }
                    catch (InvalidOperationException)
                    {
                    }
                }
            }
        }

        /// <summary>
        ///     Invokes action.
        /// </summary>
        private void InvokeAction()
        {
            try
            {
                if (_action != null)
                {
                    _action.Invoke(this);
                }
            }
            catch (Exception ex)
            {
                Exception = ex;
            }
            finally
            {
                CloseForm();
            }
        }

        /// <summary>
        ///     Handles marquee progress form load event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void MarqueeProgressFormLoad(object sender, EventArgs e)
        {
            var task = new Task(InvokeAction);
            task.Start();
        }

        /// <summary>
        ///     Shows marquee progress form.
        /// </summary>
        /// <param name="description">Action description.</param>
        /// <param name="action">Action to perform.</param>
        /// <param name="owner">Form owner.</param>
        /// <param name="hidden">Whether form is hidden.</param>
        public static void Show(string description, Action<MarqueeProgressForm> action, IWin32Window owner = null,
            bool hidden = false)
        {
            MarqueeProgressForm form = null;
            try
            {
                form = new MarqueeProgressForm(action, description, hidden);

                if (owner != null)
                {
                    form.ShowDialog(owner);
                }
                else
                {
                    form.ShowDialog();
                }

                if (form.Exception != null)
                    throw new MarqueeProgressFormException("MarqueeProgressForm Action invocation exception", form.Exception);
            }
            finally
            {
                if (form != null)
                    form._action = null;                        
            }
        }
    }

    public class MarqueeProgressFormException : Exception
    {
        public MarqueeProgressFormException(string message, Exception innerException)
            : base(message, innerException)
        {

        }
    }
}
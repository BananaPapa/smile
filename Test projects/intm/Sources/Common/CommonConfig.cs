﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Eureca.Utility.Synchronization;

namespace Eureca.Integrator.Utility.Settings
{
    [Serializable]
    [XmlRoot]
    public class CommonConfig
    {
        const string MutexName = "Eureca.Integrator.Utility.Settings.CommonConfig";

        public const string DefaultConnectionString = null;

        public const string DefaultUpdateServerUrl = @"ftp://dimadiv-server/update/";
        public const string DefaultUpdateServerLogin = "dimadiv";
        public const string DefaultUpdateServerPassword = "1";
        public const int DefaultUpdateCheckInterval = 10;

        [XmlIgnore]
        static string _file;

        [XmlIgnore]
        static string _tmpFile;

        [XmlIgnore]
        static string _backupFile;

        [XmlElement]
        public string CurriculumConnectionString { get; set; }

        [XmlElement]
        public string ProfessionalConnectionString { get; set; }

        public CommonConfig()
        {
            CurriculumConnectionString = DefaultConnectionString;
            ProfessionalConnectionString = DefaultConnectionString;
        }

        static CommonConfig()
        {
            _file = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), @"Eureca\Integrator\config.xml");
            _tmpFile = _file + ".tmp";
            _backupFile = _file + ".bak";
        }

        public static CommonConfig Load()
        {
            using (new MutexLocker(MutexName))
            {
                try
                {
                    string file;

                    if (!File.Exists(_file))
                    {
                        if (File.Exists(_backupFile))
                            file = _backupFile;
                        else
                            return new CommonConfig();
                    }
                    else
                        file = _file;

                    using (var fileStream = File.OpenRead(file))
                    {
                        XmlSerializer xs = new XmlSerializer(typeof(CommonConfig));

                        var config = (CommonConfig)xs.Deserialize(fileStream);

                        return config;
                    }
                }
                catch (Exception ex)
                {
                    return new CommonConfig();
                }
            }
        }

        public void Save()
        {
            using (new MutexLocker(MutexName))
            {
                var dir = Path.GetDirectoryName(_tmpFile);
                if (!Directory.Exists(dir))
                    Directory.CreateDirectory(dir);
                using (var fileStream = new FileStream(_tmpFile, FileMode.Create, FileAccess.ReadWrite))
                {
                    XmlSerializer xs = new XmlSerializer(typeof(CommonConfig));

                    xs.Serialize(fileStream, this);
                }

                if (File.Exists(_file))
                    File.Copy(_file, _backupFile, true);

                File.Copy(_tmpFile, _file, true);
                File.Delete(_tmpFile);
            }
        }

        public string GetConnectionString(IntegratorDb integratorDb)
        {
            switch (integratorDb)
            {
                case IntegratorDb.Curriculum:
                    return CurriculumConnectionString;
                case IntegratorDb.Professional:
                    return ProfessionalConnectionString;
                default:
                    return null;
            }
        }

        public void SetConnectionString(IntegratorDb integratorDb,string connectionString)
        {
            switch (integratorDb)
            {
                case IntegratorDb.Curriculum:
                    CurriculumConnectionString = connectionString;
                    break;
                case IntegratorDb.Professional:
                    ProfessionalConnectionString = connectionString;
                    break;
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Dispatcher;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using Eureca.Updater.Common;
using Eureca.Updater.Downloader;
using System.ServiceModel.Channels;
using System.Net;
using System.IO;
using Eureca.Updater.Repository;


namespace Eureca.Updater.Service
{
    public partial class UpdaterService : IUpdaterService, IErrorHandler
    {
        public void Subscribe(string appName, ModuleVersion version)
        {
            var context = OperationContext.Current;
            var currentClient = new AppClientDescriptor( appName, context.GetCallbackChannel<IUpdaterNotifier>( ), version, context.SessionId );
            lock (_clientListLock)
            {
                Log.Info( String.Format( "New user subscribed: {0} Ip address {1}" , appName,context.Channel.RemoteAddress.ToString() ) );
                _clients.Add(currentClient );
            }
            _updateDownloader.AddClient( appName);

            VersionSourcePair vsPair;
            if (_updateDownloader.AvailableVersions.TryGetValue( appName, out vsPair ) && vsPair != null && version < vsPair.UpdateInfo.Version)
            {
                string updateFileFullName = UpdateDownloader.GenerateUpdatePath(appName,vsPair.UpdateInfo.Version);
                if(File.Exists(updateFileFullName) && vsPair.Source.CheckUpdateDistrib(appName,new FileInfo(updateFileFullName),vsPair.UpdateInfo))
                {
                    NotifyUpdateReady( appName, updateFileFullName, new[] { currentClient } );
                }
            }
        }

     
        public void AddRepository(RepositoryConnectionSetting repositoryConnectionSettings)
        {
            if (!_repositories.Any(repo=>repo.IsAccessory(repositoryConnectionSettings)))
            {
                Log.Info( String.Format( "Add repository: {0} ", repositoryConnectionSettings ) );
                IApplicationRepository newRepo = CreateRepo( repositoryConnectionSettings );
                lock (_reposListLock)
                {
                    _repositories.Add( newRepo );
                }
                Exception ex;
                List<string> serialisedSettings = _config.Repositories.Select(item=>item.Serialize(out ex)).ToList();
                string serialisedSetting = repositoryConnectionSettings.Serialize(out ex);
                if (ex == null && !serialisedSettings.Contains( serialisedSetting))
                {
                    _config.Repositories.Add( repositoryConnectionSettings );
                    _config.Save( );
                }
            }
        }

        public UpdateInfo GetAvailableVersion(string applicationName)
        {
            VersionSourcePair version = null;
            _updateDownloader.AvailableVersions.TryGetValue( applicationName, out version );
            return version.UpdateInfo;
        }

        public bool CheckConnection()
        {
 	        return true;
        }

        public  void ConfirmUpdating( string appName , bool isAgree = true )
        {
            if (!_waitingForConfirmation.ContainsKey( appName ))
                return;
            
            string updateFileFullName = _waitingForConfirmation[appName];
            _waitingForConfirmation.Remove( appName );
            var context = OperationContext.Current;
            string sessionid = context.SessionId;
            if (isAgree)
            {
                var task =
                    Task.Factory.StartNew(
                        () =>
                            NotifyConfirmationResult(
                                _clients.Where(clnt => clnt.AppName == appName && clnt.SessionID != sessionid), true));
                task.ContinueWith(
                    (t) => { 
                var currentClient = _clients.SingleOrDefault(item => item.SessionID == sessionid);
                currentClient.CallbackChannel.ConfirmationResult(true, updateFileFullName);
                    }).Wait();
        }
            else
            {
                Task.Factory.StartNew( ( ) => NotifyConfirmationResult( _clients.Where( clnt => clnt.AppName == appName && clnt.SessionID != sessionid ),false ) ).Wait();
            }
        }

        #region IErrorHandler Members

        public bool HandleError( Exception error )
        {
            Log.Error(error.ToString());
            return true;
        }

        public void ProvideFault( Exception error, MessageVersion version, ref Message fault )
        {

        }

        #endregion
    }
}

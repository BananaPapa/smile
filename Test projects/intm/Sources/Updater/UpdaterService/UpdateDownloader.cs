﻿using System.Security.Policy;
using Eureca.Updater.Service;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Eureca.Updater.Common;
using Eureca.Updater.Repository;
using Eureca.Utility.Extensions;
using log4net;

namespace Eureca.Updater.Downloader
{
    public class NewUpdatesReadyArgs:EventArgs
    {
        public string AppName { get; private set; }

        public string UpdateFilePath { get; private set; }

        public NewUpdatesReadyArgs(string appName, string UpdateFilePath )
        {
            this.AppName = appName;
            this.UpdateFilePath = UpdateFilePath;
        }
    }

    public class UpdateDownloader : PeriodicInvoker
    {
        readonly static ILog Log = LogManager.GetLogger( typeof( UpdateDownloader ) );
        static Regex _updateDirRegex = new Regex( @"^\d+\.\d+\.\d+\.\d+$", RegexOptions.Compiled );
        //List<IApplicationRepository> _repositories;
        List<string> _newCLients = new List<string>();
        Dictionary<string, VersionSourcePair> _availableVersions = new Dictionary<string, VersionSourcePair>( );
        Dictionary<string, VersionSourcePair> _previousVersions = new Dictionary<string, VersionSourcePair>( );
        object _newClientListLock = new object( );
        Func<string,UpdateInfo, bool> _updateFilter;
        private Func<List<IApplicationRepository>> _getReposFunc;
        public event EventHandler<NewUpdatesReadyArgs> NewUpdatesReady;

        public Dictionary<string, VersionSourcePair> AvailableVersions
        {
            get { return _availableVersions; }
            set { _availableVersions = value; }
        }

        public UpdateDownloader( Func<List<IApplicationRepository>> getReposFunc, Func<string, UpdateInfo, bool> updateFilter = null )
        {
            this.Timeout = TimeSpan.FromSeconds(20);
            this._updateFilter = updateFilter;
            _getReposFunc = getReposFunc;
            //_repositories = repositories;

        }

        public void AddClient( string appName)
        {
            lock (_newClientListLock)
            {
                if (!_availableVersions.Keys.Contains( appName ))
                {
                    _newCLients.Add( appName );
                }
            }
        }

        protected override void PeriodicActionMethod( )
        {
            try
            {
                lock (_newClientListLock)
                {
                    foreach (var client in _newCLients)
                    {
                        _availableVersions[client] = null;
                    }
                    _newCLients.Clear( );
                }


                List<string> outdatedApps = new List<string>( );
                foreach (var repo in _getReposFunc())
                {
                    if (repo.CheckConnection( ))
                    {
                        var clients = _availableVersions.Keys.ToList( );
                        foreach (var client in clients)
                        {
                            try
                            {
                                var updatesInfos = repo.GetVersionHistory( client );
                                if (updatesInfos == null || updatesInfos.Count == 0)
                                    continue;
                                updatesInfos = updatesInfos.OrderByDescending( item => item.Version ).ToList( );

                                UpdateInfo lastVersion;

                                if (_updateFilter != null)
                                {
                                    updatesInfos = FilterUpdates( client, updatesInfos );
                                    lastVersion = updatesInfos.Count > 0 ? updatesInfos[0] : null;
                                }
                                else
                                    lastVersion = updatesInfos[0];

                                if (lastVersion != null && ( _availableVersions[client] == null || _availableVersions[client].UpdateInfo.Version < lastVersion.Version ))
                                {
                                    if (!outdatedApps.Contains( client ))
                                        outdatedApps.Add( client );
                                    _previousVersions[client] = _availableVersions[client];
                                    _availableVersions[client] = new VersionSourcePair( lastVersion, repo );
                                }
                            }
                            catch (Exception ex)
                            {
                                Log.Error("On repo search for {0}:\n\t{1}".FormatString(client,ex.ToString( )));
                            }
                        }
                    }
                }

                foreach (var outdatedApp in outdatedApps)
                {
                    var vsPair = _availableVersions[outdatedApp];
                    var repo = vsPair.Source;
                    var updateInfo = vsPair.UpdateInfo;
                    var updateFileFullName = GenerateUpdatePath( outdatedApp, updateInfo.Version);

                    if (File.Exists( updateFileFullName ) && repo.CheckUpdateDistrib( outdatedApp, new FileInfo( updateFileFullName ), updateInfo ))
                        NotifyNewUpdateReady( outdatedApp, updateFileFullName );
                    else
                    {
                        Task.Factory.StartNew( ( ) => DownloadUpdate( repo, outdatedApp, updateInfo, updateFileFullName ) );
                        //Task.Run( ( ) => DownloadUpdate( repo, outdatedApp, updateInfo, updateFileFullName ) );
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error( ex.ToString( ) );
            }
        }

        private void DownloadUpdate(IApplicationRepository repo,string outdatedApp,UpdateInfo updateInfo,string updateFilePath)
        {
            try
            {
                repo.DownloadUpdate(outdatedApp, updateInfo, updateFilePath, null, NotifyNewUpdateReady);
            }
            catch (Exception e)
            {
                _availableVersions[outdatedApp] = _previousVersions[outdatedApp];
                Log.Warn( String.Format( "Failed to download update file {0} , available version set to to version {1}",updateInfo.Version,_previousVersions[outdatedApp].UpdateInfo.Version ));
            }
        }

        private List<UpdateInfo> FilterUpdates(string appName, List<UpdateInfo> updatesInfos )
        {
            return updatesInfos.Where( item => _updateFilter(appName, item ) ).ToList( );
        }

        public void NotifyNewUpdateReady( string appName,string updateFilePath)
        {
            DeleteOutdatedDistributives(Path.GetDirectoryName(updateFilePath));
            NewUpdatesReady.RaiseEvent(this,new NewUpdatesReadyArgs(appName,updateFilePath));
        }

        public static string GenerateUpdatePath( string appName, ModuleVersion version )
        {
            string updatesDirectory = GetDownloadUpdateDirectory( appName );

            string updatePath = Path.Combine( updatesDirectory, string.Format( "setup-{0}.exe", version ) );

            return updatePath;
        }

        private static void DeleteOutdatedDistributives(string directoryPath)
        {
            DirectoryInfo info = new DirectoryInfo(directoryPath);
            var files = info.GetFiles().Where(file=> file.Extension.ToLower() == ".exe").OrderByDescending(p => p.CreationTime).Skip(4);
            foreach (var file in files)
            {
                try
                {
                    File.Delete(file.FullName);
                }
                catch { }
            }
        }

        static string GetDownloadUpdateDirectory( string appName )
        {
            string dir = Path.Combine(Environment.GetFolderPath( Environment.SpecialFolder.CommonApplicationData ),"Eureca", appName, "updates" );

            if (!Directory.Exists( dir ))
                Directory.CreateDirectory( dir );

            return dir;
        }
    }

}
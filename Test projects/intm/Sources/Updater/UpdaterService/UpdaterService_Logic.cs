﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.ServiceModel;
using System.Threading;
using System.Threading.Tasks;
using Eureca.Updater.Common;
using Eureca.Updater.Downloader;
using Eureca.Updater.Repository;
using log4net;
using log4net.Repository.Hierarchy;

namespace Eureca.Updater.Service
{
    public class VersionSourcePair
    {
        public UpdateInfo UpdateInfo { get; private set; }
        public IApplicationRepository Source { get; private set; }

        public VersionSourcePair( UpdateInfo Version, IApplicationRepository Source )
	    {
            this.UpdateInfo = Version;
            this.Source = Source;
	    }
    }

    public class AppClientDescriptor
    {
        public string AppName { get;private set; }
        
        public IUpdaterNotifier CallbackChannel { get; private set; }
        
        public ModuleVersion CurrentAppVersion { get;private set; }

        public string SessionID { get; private set; }
        //public IPAddress ClientIpAddress { get; private set; }

        public AppClientDescriptor (string appName,IUpdaterNotifier appChannel,ModuleVersion currentAppVersion, string sessionID)
	    {
            this.AppName =appName;
            this.CallbackChannel = appChannel;
            this.CurrentAppVersion  =currentAppVersion;
            this.SessionID = sessionID;
	    }
    }

    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerSession)]
    public partial class UpdaterService : IUpdaterService,IDisposable
    {
        private static ILog Log = LogManager.GetLogger(typeof (UpdaterService));

        static readonly object _clientListLock = new object( );
        static object _reposListLock = new object();
        static readonly object _waitingForConfirmationListLock = new object( );
        static readonly CommonUpdaterConfig _config;
        static readonly List<IApplicationRepository> _repositories;
        static readonly List<AppClientDescriptor> _clients;
        static readonly UpdateDownloader _updateDownloader;
        static readonly SortedList<string,string> _waitingForConfirmation;
        static IEnumerable<IPAddress> _machineIPAddresses;
        static readonly Timer _checkNetworkInterfacesTimer = new Timer( _timerCallback );
        static readonly ReaderWriterLock _netInterfacesUpdateLock = new ReaderWriterLock( );
        static UpdaterService()
        {
#if DEBUG
            _checkNetworkInterfacesTimer.Change( 0, 1000 );
#else      
            _checkNetworkInterfacesTimer.Change( 0, 10000 );
#endif
            //machineIPAddresses = GetHostIP( );
            _config = CommonUpdaterConfig.Load();
            _repositories = new List<IApplicationRepository>();
            _clients = new List<AppClientDescriptor>( );
            _waitingForConfirmation = new SortedList<string, string>( );
            _updateDownloader = new UpdateDownloader(() =>
            {
                lock (_reposListLock)
                {
                    return _repositories.ToList();
                }
            }, _updateFilter );
            _updateDownloader.NewUpdatesReady += _updateDownloader_NewUpdatesReady;
            _updateDownloader.Start( );
            foreach (var repoSettings in _config.Repositories)
            {
                var repo = CreateRepo( repoSettings );
                _repositories.Add( repo );
            }
            _updateDownloader.Start( );
        }


        static bool _updateFilter( string appName,UpdateInfo updateInfo )
        {
            if (_clients.Any( clnt => clnt.AppName == appName && clnt.CurrentAppVersion >= updateInfo.Version ))
                return false;

            if (updateInfo.TargetIpRange == null)
                return true;
            else
            {
                try
                {
                    _netInterfacesUpdateLock.AcquireReaderLock( 1000 );
                    bool res = _machineIPAddresses.Any( ipAdr => updateInfo.TargetIpRange.IsInRange( ipAdr ) );
                    _netInterfacesUpdateLock.ReleaseReaderLock( );
                    return res;
                }
                catch (ApplicationException ex) { }
                catch (Exception ex) {_netInterfacesUpdateLock.ReleaseReaderLock( );}
            }
            return false;
        }

        static void _timerCallback(object state)
        {
            try
            {
                _netInterfacesUpdateLock.AcquireWriterLock( 1000 );
                _machineIPAddresses = GetHostIP( );
                _netInterfacesUpdateLock.ReleaseWriterLock( );
            }
            catch (ApplicationException ex) { }
            catch (Exception ex){ _netInterfacesUpdateLock.ReleaseWriterLock( ); }
        }

        static IEnumerable<IPAddress> GetHostIP( )
        {
            List<IPAddress> addresses = new List<IPAddress>();
            foreach (NetworkInterface netif in NetworkInterface.GetAllNetworkInterfaces( ))
                foreach (IPAddressInformation unicast in netif.GetIPProperties().UnicastAddresses)
                    addresses.Add(unicast.Address);
            
            return addresses;
        }

        static void _updateDownloader_NewUpdatesReady( object sender, NewUpdatesReadyArgs e )
        {
            NotifyUpdateReady( e.AppName,e.UpdateFilePath );
        }

        private static void NotifyUpdateReady( string appName , string updateFilePath , IEnumerable<AppClientDescriptor> clients = null )
        {
            lock(_waitingForConfirmationListLock)
            {
            if (!_waitingForConfirmation.ContainsKey( appName ))
                _waitingForConfirmation.Add( appName, updateFilePath );
            }
           
            var updateInfo = _updateDownloader.AvailableVersions[appName].UpdateInfo;
            List<AppClientDescriptor> brokenConnections = Task.Factory.StartNew( 
                ( ) =>
                    {
                        List<AppClientDescriptor> brokenConn = new List<AppClientDescriptor>( );
                        var recipients = clients == null ?  _clients.Where( clt => clt.AppName == appName && clt.CurrentAppVersion < updateInfo.Version ) : clients;
                        foreach (var client in recipients)
                        {
                            try
                            {
                                client.CallbackChannel.OnNewUpdatesReady( updateInfo );
                            }
                            catch
                            {
                                brokenConn.Add( client );
                            }
                        }
                        return brokenConn;
                    }).Result;
            lock (_clientListLock)
                _clients.RemoveAll( clnt => brokenConnections.Contains( clnt ) );
        }

        private static void NotifyConfirmationResult( IEnumerable<AppClientDescriptor> condemnedList, bool result )
        {
            foreach (var clientDescriptor in condemnedList)
            {
                try
                {
                    clientDescriptor.CallbackChannel.ConfirmationResult( result );
                    Log.Info( String.Format( "Notfy client {0} , confirm result {1}", clientDescriptor.AppName , result ) );
                }
                catch { }
            }
            lock (_clientListLock)
            {
                _clients.RemoveAll( item => condemnedList.Contains( item ) );
            }
        }

        private static IApplicationRepository CreateRepo( RepositoryConnectionSetting repositoryConnectionSettings )
        {
            if (repositoryConnectionSettings == null)
                return null;
            IApplicationRepository res = null;
            switch (repositoryConnectionSettings.GetRepositoryType())
            {
                case RepositoryType.FtpServer:
                    res = new FtpAppRepository( (FtpConnectionSettings)repositoryConnectionSettings );
                    break;
                default:
                    throw new NotImplementedException( "Unknown RepositoryType" );
            }
            return res;
        }

        #region IDisposable Members

        public void Dispose( )
        {
            _checkNetworkInterfacesTimer.Change( Timeout.Infinite, Timeout.Infinite );
            _netInterfacesUpdateLock.ReleaseLock( );
        }

        #endregion
    }
}

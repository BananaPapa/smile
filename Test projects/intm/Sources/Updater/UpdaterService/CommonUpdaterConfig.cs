﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
 
using System.Security.AccessControl;
using Eureca.Updater.Common;
using Eureca.Updater.UpdaterClient;

namespace Eureca.Updater.Service
{

    [Serializable]
    [XmlRoot]
    public class CommonUpdaterConfig : IUpdateSettingStorage
    {
        const string MutexName = "Eureca.Updater.Settings.CommonConfig";

        public const int DefaultUpdateCheckInterval = 10;

        [XmlIgnore]
        static string _file;

        [XmlIgnore]
        static string _tmpFile;

        [XmlIgnore]
        static string _backupFile;


        [XmlElement]
        public int UpdateCheckIntervalSeconds { get; set; }

        List<RepositoryConnectionSetting> _repositories = new List<RepositoryConnectionSetting>( );

        [XmlArray( ElementName = "Repositories" ),XmlArrayItem(ElementName="Repositiries")]
        public List<RepositoryConnectionSetting> Repositories
        {
            get { return _repositories; }
            set { _repositories = value; }
        }

        public CommonUpdaterConfig()
        {
            UpdateCheckIntervalSeconds = DefaultUpdateCheckInterval;
        }

        static CommonUpdaterConfig()
        {
            _file = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), @"Eureca\Updater\config.xml");
            _tmpFile = _file + ".tmp";
            _backupFile = _file + ".bak";
        }

        public static CommonUpdaterConfig Load()
        {
            //using (new MutexLocker(MutexName))
            //{
                try
                {
                    string file;

                    if (!File.Exists(_file))
                    {
                        if (File.Exists(_backupFile))
                            file = _backupFile;
                        else
                            return new CommonUpdaterConfig();
                    }
                    else
                        file = _file;

                    using (var fileStream = File.OpenRead(file))
                    {
                        XmlSerializer xs = new XmlSerializer(typeof(CommonUpdaterConfig));

                        var config = (CommonUpdaterConfig)xs.Deserialize(fileStream);

                        return config;
                    }
                }
                catch (Exception ex)
                {
                    return new CommonUpdaterConfig();
                }
            //}
        }

        public void Save()
        {
            //using (new MutexLocker(MutexName))
            string dir= Path.GetDirectoryName(_tmpFile);
            if (!Directory.Exists( dir ))
            {
                Directory.CreateDirectory( dir);
               var sec = Directory.GetAccessControl( dir );
            }
            using (var fileStream = new FileStream(_tmpFile, FileMode.Create, FileAccess.ReadWrite))
            {
                XmlSerializer xs = new XmlSerializer(typeof(CommonUpdaterConfig));
                xs.Serialize(fileStream, this);
            }

            if (File.Exists(_file))
                File.Copy(_file, _backupFile, true);

            File.Copy(_tmpFile, _file, true);
            File.Delete(_tmpFile);
            //}
        }


        #region IUpdateSettingStorage Members

        event EventHandler<OnSettingChangedArgs> IUpdateSettingStorage.SettingsChanged
        {
            add { throw new NotImplementedException( ); }
            remove { throw new NotImplementedException( ); }
        }

        IList<RepositoryConnectionSetting> IUpdateSettingStorage.RepositorySettings
        {
            get
            {
                return _repositories; 
            }
            set
            {
                this._repositories = value.ToList();
                Save();
            }
        }

        int IUpdateSettingStorage.StorageVersion
        {
            get
            {
                throw new NotImplementedException( );
            }
            set
            {
                throw new NotImplementedException( );
            }
        }

        #endregion
    }

    //public class ConfigFileStorageImplementation : IUpdateSettingStorage
    //{
    //    private CommonUpdaterConfig _commonUpdaterConfig;
    //    private BindingList<RepositoryConnectionSetting> _repoSettigs; 


    //    public ConfigFileStorageImplementation(CommonUpdaterConfig commonUpdaterConfig)
    //    {
    //        _commonUpdaterConfig = commonUpdaterConfig;
    //        InitSettingsCollection();
    //    }

    //    private void InitSettingsCollection()
    //    {
    //        _repoSettigs = new BindingList<RepositoryConnectionSetting>(_commonUpdaterConfig.Repositories);
    //        _repoSettigs.ListChanged += _repoSettigs_ListChanged;
    //    }

    //    void _repoSettigs_ListChanged( object sender, ListChangedEventArgs e )
    //    {
    //        if(e.ListChangedType != ListChangedType.ItemAdded && e.ListChangedType !=ListChangedType.ItemChanged && e.ListChangedType != ListChangedType.ItemDeleted )
    //            return;
    //        if(SettingsChanged != null)
    //            SettingsChanged(this,new OnSettingChangedArgs(_repoSettigs));
    //        _commonUpdaterConfig.Save();
    //    }

    //    #region IUpdateSettingStorage Members

    //    public event EventHandler<OnSettingChangedArgs> SettingsChanged;

    //    public IList<RepositoryConnectionSetting> RepositorySettings
    //    {
    //        get
    //        {
    //            return  _repoSettigs;
    //        }
    //        set
    //        {
    //            _commonUpdaterConfig.Repositories = value.ToList();
    //            InitSettingsCollection();
    //        }
    //    }

    //    public int StorageVersion
    //    {
    //        get
    //        {
    //            return 1 ;
    //        }
    //        set
    //        {
    //            throw new NotImplementedException( );
    //        }
    //    }

    //    #endregion
    //}


}

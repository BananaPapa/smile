﻿using Eureca.Updater.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
  
using System.IO;

namespace Eureca.Updater.Repository
{
    public sealed class DownloadUpdateCompleteEventArgs:EventArgs
    {
        public string AppName { get; private set; }
        public ModuleVersion Version { get; private set; }

        public DownloadUpdateCompleteEventArgs( string appName, ModuleVersion version )
        {
            this.AppName = appName;
            this.Version = version;
        }
    }

    public interface IApplicationRepository
    {
        event EventHandler<DownloadUpdateCompleteEventArgs> DownloadUpdateComplete;

        bool IsAccessory( RepositoryConnectionSetting settings );

        List<UpdateInfo> GetVersionHistory( string applicationbGuid );

        void DownloadUpdate( string applicationbGuid, UpdateInfo updateInfo, string outputFilePath, Action<int> ProgressCallback, Action<string,string> DownloadComplete );

        bool CheckConnection( );

        bool CheckUpdateDistrib(string applicationName, FileInfo file, UpdateInfo updateInfo );
    }
}

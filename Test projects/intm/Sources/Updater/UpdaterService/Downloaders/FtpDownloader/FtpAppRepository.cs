﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.FtpClient;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Eureca.Updater.Common;
using Eureca.Updater.Downloader;
using Eureca.Updater.Service;
using log4net;

namespace Eureca.Updater.Repository
{

    public class FtpAppRepository : IApplicationRepository, IDisposable
    {
        static ILog Log = LogManager.GetLogger( typeof( FtpAppRepository ) );
        static Regex _updateDirRegex = new Regex( @"^\d+\.\d+\.\d+\.\d+$", RegexOptions.Compiled );
        const string setupFileName = "setup.exe",setupInfoFileName = "update.xml";

        FtpClient _client;
        FtpConnectionSettings _settings;


        public event EventHandler<DownloadUpdateCompleteEventArgs> DownloadUpdateComplete;

        public FtpAppRepository( FtpConnectionSettings settings )
        {
            _settings = settings;
            RecreateFtpClient( );
        }

        private void RecreateFtpClient( )
        {
            _client = CreateFtpClient( _settings.GetRepositoryUri( ), _settings.Login, _settings.Password );
        }

        FtpClient CreateFtpClient( Uri repositoryUri, string login, string password )
        {
            FtpClient client = new FtpClient( );

            client.Host = repositoryUri.Host;
            client.Port = repositoryUri.IsDefaultPort ? 21 : repositoryUri.Port;
            client.Credentials = new NetworkCredential(
                login,
                password );

            return client;
        }

        #region IApplicationRepository Members

        public Func<UpdateInfo, bool> filterFunc
        {
            set { throw new NotImplementedException( ); }
        }

        public bool IsAccessory( RepositoryConnectionSetting settings )
        {
            var ftpSettings = settings as FtpConnectionSettings;

            //There is no reason for cheking password if all other props are same.
            return ftpSettings != null &&
                ( _settings.Host == ftpSettings.Host && _settings.Port == ftpSettings.Port && _settings.UrlPath == ftpSettings.UrlPath && _settings.Login == ftpSettings.Login );
        }

        public List<UpdateInfo> GetVersionHistory( string appName )
        {

            List<UpdateInfo> history = new List<UpdateInfo>( );
            try
            {
                var versions = GetApplicationUpdateFolders( appName );
                if (versions == null || versions.Length == 0)
                    return history;

                foreach (var versionFolder in versions.Where( item => _updateDirRegex.IsMatch( item.Name ) ))
                {
                    _client.SetWorkingDirectory( versionFolder.FullName );
                    if (!_client.FileExists( setupFileName ))
                        continue;
                    if (!_client.FileExists( setupInfoFileName ))
                        history.Add( new UpdateInfo( ) { Version = new ModuleVersion( versionFolder.Name ), IsCritical = false } );
                    else
                        try
                        {
                            using (var stream = _client.OpenRead( setupInfoFileName ))
                            {
                                var updateInfo = UpdateInfo.Deserialize(stream);
                                updateInfo.Version = new ModuleVersion(versionFolder.Name);
                                history.Add(updateInfo);
                            }
                        }
                        catch (Exception ex)
                        {
                            history.Add( new UpdateInfo( ) { Version = new ModuleVersion( versionFolder.Name ), IsCritical = false } );
                        }
                }

            }
            catch (Exception ex)
            {
                CheckAndRecreate( );
                Log.Error( ex.ToString( ) );
            }
            return history;
        }

        private FtpListItem[] GetApplicationUpdateFolders( string appName )
        {
            FtpListItem[] versions;

            string directory = Path.Combine( _settings.GetRepositoryUri( ).PathAndQuery, appName );

            _client.SetWorkingDirectory( directory );

            versions = _client.GetListing( );

            return versions;
        }

        public void DownloadUpdate( string appName, Common.UpdateInfo updateInfo, string dstFilePath, Action<int> progressCallback, Action<string, string> downloadComplete )
        {
            var client = CreateFtpClient( _settings.GetRepositoryUri( ), _settings.Login, _settings.Password );
            string tmpFilePath = dstFilePath + ".tmp";
            string directory = Path.Combine( _settings.GetRepositoryUri( ).PathAndQuery, appName, updateInfo.Version.ToString( ) );
            client.SetWorkingDirectory( directory );
            try
            {
                var size = GetFileSize(client);

                byte[] buffer = new byte[1024];
                int totalBytesReceived = 0;

                using (var srcStream = client.OpenRead( setupFileName ))
                {
                    string dir = Path.GetDirectoryName( tmpFilePath );
                    if (!Directory.Exists( dir ))
                    {
                        Directory.CreateDirectory( dir );
                        var sec = Directory.GetAccessControl( dir );
                    }
                    using (var dstStream = new FileStream( tmpFilePath, FileMode.Create, FileAccess.Write ))
                    {
                        while (true)
                        {
                            int bytesReceived = srcStream.Read( buffer, 0, buffer.Length );

                            if (bytesReceived == 0)
                                break;

                            dstStream.Write( buffer, 0, bytesReceived );

                            totalBytesReceived += bytesReceived;

                            int progress = (int)( (float)totalBytesReceived / (float)size * 100 );

                            if (progressCallback != null)
                                progressCallback( progress );

                            DownloadUpdateComplete.RaiseEvent( this, new DownloadUpdateCompleteEventArgs( appName, updateInfo.Version ) );
                        }
                    }
                }

                if (File.Exists( dstFilePath ))
                    File.Delete( dstFilePath );

                File.Move( tmpFilePath, dstFilePath );
                if (downloadComplete != null)
                    downloadComplete( appName, dstFilePath );
            }
            catch (Exception ex)
            {
                Log.Error( ex.ToString( ) );
                if(File.Exists(tmpFilePath))
                    File.Delete( tmpFilePath );
                throw;
            }
        }

        private static long GetFileSize(FtpClient client)
        {
            long size = 0;
            int attemptCount = 5;
            do
            {
                size = 0;
                try
                {
                   size = client.GetFileSize(setupFileName);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
                if (size == 0)
                {
                    if (--attemptCount == 0)
                        break;
                    Thread.Sleep(5000);
                }
                else
                {
                    break;
                }
            } while (true);

            if (attemptCount == 0 && size == 0)
            {
                throw new Exception("Некорректный размер установочного файла");
            }
            return size;
        }

        public bool CheckConnection( )
        {
            try
            {
                _client.GetListing( );
                return true;
            }
            catch
            {
                return false;
            }
        }

        private void CheckAndRecreate()
        {
            try
            {
                if(!_client.IsConnected)
                    _client.Connect( );
            }
            catch (Exception ex)
            {
                Log.Warn( ex.ToString( ) );
                RecreateFtpClient( );
            }
        }

        public bool CheckUpdateDistrib(string applicationName, FileInfo file, UpdateInfo updateInfo )
        {
            if(!file.Exists)
                return false;
            try
            {
                var client = CreateFtpClient( _settings.GetRepositoryUri( ), _settings.Login, _settings.Password );
                string folderName = Path.Combine( _settings.UrlPath, applicationName, updateInfo.Version.ToString( ) );
                //var res = client.GetListing( );
                client.SetWorkingDirectory( folderName );
                long fileSize = client.GetFileSize( setupFileName );
                return file.Length == fileSize;
            }
            catch (Exception ex)
            {
                CheckAndRecreate( );
                Log.Error( ex.ToString( ) );
                return false;
            }
        }

        #endregion

        #region IDisposable Members

        public void Dispose( )
        {
            if (_client != null )
            {
                _client.Dispose( );
                _client = null;
            }
        }

        #endregion

    }
}

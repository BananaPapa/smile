﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Eureca.Updater.Common;
using Eureca.Updater.UpdaterClient;


namespace Eureca.Updater.Service
{
    public partial class ServiceSettingWindow : Form
    {
        private IUpdateSettingStorage _settingStorage;
        public ServiceSettingWindow(IUpdateSettingStorage settingStorage )
        {
            InitializeComponent( );
            _updatesSettingsControl.RepositorySettings = settingStorage;
            Closing += ServiceSettingWindow_Closing;
        }

        void ServiceSettingWindow_Closing( object sender, CancelEventArgs e )
        {
            _updatesSettingsControl.OnLoseFocus();
        }

    }
}


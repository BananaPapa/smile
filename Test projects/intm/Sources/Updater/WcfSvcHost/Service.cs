﻿using System;
using System.ServiceProcess;
using Eureca.Updater.Service;
using Eureca.Updater.Service.Hosting.Properties;
using Eureca.Updater.Wcf;
using System.Diagnostics;


namespace Eureca.Updater.Service.Hosting
{
    /// <summary>
    ///     Represents transport service.
    /// </summary>
    public partial class WcfServiceHost : ServiceBase
    {
        public Hoster<UpdaterService> _hoster = null;
        //private readonly UpdaterService _sm;

        /// <summary>
        ///     Initializes new instance of <see cref="T:Eureca.Rumble.Service.TransportService.Service" />.
        /// </summary>
        public WcfServiceHost()
        {
            //EventLog.WriteEntry( "Updater Service", "Call Init");
            InitializeComponent();
            //_sm = new UpdaterService( );
            _hoster = new Hoster<UpdaterService>(typeof(UpdaterService),typeof(IUpdaterService),BindingSource.InitNetTcp( ), Uri.UriSchemeNetTcp,
                    "UpdateService", Settings.Default.HosingPort );
            //EventLog.WriteEntry( "Updater Service", "Call successful" );
            
        }

        /// <summary>
        ///     Executes when a Start command is sent to the service by the Service Control Manager (SCM) or when the operating
        ///     system starts (for a service that starts automatically). Specifies actions to take when the service starts.
        /// </summary>
        /// <param name="args">Data passed by the start command. </param>
        protected override void OnStart(string[] args)
        {
            //EventLog.WriteEntry( "Updater Service", "Call Start" );
            if (_hoster != null)
                _hoster.Start( );
        }

        /// <summary>
        ///     Executes when a Stop command is sent to the service by the Service Control Manager (SCM). Specifies actions to take
        ///     when a service stops running.
        /// </summary>
        protected override void OnStop()
        {
            //EventLog.WriteEntry( "Updater Service", "Call Stop" );
            if (_hoster != null)
                _hoster.Stop( );
        }
    }
}
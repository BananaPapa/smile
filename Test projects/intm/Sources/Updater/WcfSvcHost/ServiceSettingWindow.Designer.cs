﻿namespace Eureca.Updater.Service
{
    partial class ServiceSettingWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if (disposing && ( components != null ))
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            this._updatesSettingsControl = new Eureca.Common.Settings.UpdatesSettingsControl();
            this.SuspendLayout();
            // 
            // _updatesSettingsControl
            // 
            this._updatesSettingsControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._updatesSettingsControl.Location = new System.Drawing.Point(0, 0);
            this._updatesSettingsControl.Name = "_updatesSettingsControl";
            this._updatesSettingsControl.RepositorySettings = null;
            this._updatesSettingsControl.Size = new System.Drawing.Size(841, 572);
            this._updatesSettingsControl.TabIndex = 0;
            // 
            // ServiceSettingWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(841, 572);
            this.Controls.Add(this._updatesSettingsControl);
            this.Name = "ServiceSettingWindow";
            this.Text = "Настройки репозиториев";
            this.ResumeLayout(false);

        }

        #endregion

        private Eureca.Common.Settings.UpdatesSettingsControl _updatesSettingsControl;
    }
}
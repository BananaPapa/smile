﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Eureca.Updater.Common;
using Eureca.Updater.Wcf;
using log4net;
using WcfSvcHost;

namespace Eureca.Updater.Service.Hosting
{
    internal static class Program
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof (Program));
        private static string _serviceName = "EurecaUpdateService";

        //static UpdaterService _sm;
//#if(Debug)
        private static Hoster<UpdaterService> _hoster;
//#endif
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        private static void Main()
        {
            bool isServiceWasStopped = false;

            try
            {
                AppDomain.CurrentDomain.UnhandledException += (CurrentDomain_UnhandledException);

                if (!Environment.UserInteractive)
                {
                    ServiceBase[] ServicesToRun;
                    ServicesToRun = new ServiceBase[] {new WcfServiceHost()};
                    ServiceBase.Run(ServicesToRun);
                }
                else
                {
//#if(Debug)
                    if (WinServiceHelper.IsInstalled( _serviceName ))
                    {

                        var sc = new ServiceController( _serviceName );
                        if (sc.Status == ServiceControllerStatus.Running)
                        {
                            WinServiceHelper.StopService( _serviceName );
                            isServiceWasStopped = true;
                        }
                    }
                    
                    _hoster = new Hoster<UpdaterService>( typeof( UpdaterService ), typeof( IUpdaterService ),
                        Eureca.Updater.Wcf.BindingSource.InitNetTcp( ), Uri.UriSchemeNetTcp, "UpdateService", 34567 );
                    _hoster.Start( );
//#else
                    var settings = CommonUpdaterConfig.Load( );
                    var settingForm = new ServiceSettingWindow( settings );
                    Application.Run( settingForm );
//#endif
                }
            }
            finally
            {
                if ( isServiceWasStopped && Environment.UserInteractive)
                {
                    if (!WinServiceHelper.StartService(_serviceName))
                        throw new Exception("Failed to start service.");
                }
            }
        }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            //Log.Error( "Необработанное исключение" );
            //Log.Error( e.ExceptionObject );
            Log.Error(String.Format("Unhandled exception: {0}", e.ToString()));
            //if (
            //    MessageBox.Show(
            //        "Выполненное действие вызвало ошибку.\n За информацией обратитесь в службу технической поддержки. Продолжить работу?", "Произошла ошибка", MessageBoxButtons.YesNo, MessageBoxIcon.Error ) !=
            //    DialogResult.Yes)
            //    Application.Exit( );
        }
    }
}

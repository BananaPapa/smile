﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Mime;
using System.Threading;
using System.Threading.Tasks;
using Eureca.Updater.Common;
using Eureca.Updater.Common.Wcf;
using Eureca.Updater.Service;

namespace Eureca.Updater.UpdaterClient
{
    public class UpdatesReadyEventArgs : EventArgs
    {
        public UpdateInfo UpdateInfo { get; private set; }

        public UpdatesReadyEventArgs(UpdateInfo updateInfo)
        {
            UpdateInfo = updateInfo;
        }
    }

    /// <summary>
    ///     Клиентская сторона системы обновления, переодически проверият подключеник к серверу.
    /// </summary>
    public class UpdaterClient : PeriodicInvoker, IUpdaterNotifier
    {
        //private static TimeSpan _checkUpdatePeriod = TimeSpan.FromSeconds(10),
        //    activeCheckPeriod = TimeSpan.FromSeconds(3);

        //static CancellationTokenSource _updateConfirmationcancelationSource = null;
        private Task _requestTask;
        private int _executionThreadId = -1;

        public int RequestExecutionThreadId
        {
            get { return _executionThreadId; }
        }

        public event EventHandler<UpdatesReadyEventArgs> UpdatesReady;
        public event EventHandler RequestShutdown;
        public event EventHandler UpdatingCanceled;

        private readonly object _lockUpdateRequest = new object();

        private readonly object _settingsLock = new object();

        private readonly UpdaterServiceProxy _updaterProxy;
        private readonly string _applicationName;
        private readonly ModuleVersion _applicationVersion;

        private readonly List<RepositoryConnectionSetting> _newRepoSettings = new List<RepositoryConnectionSetting>();

        /// <summary>
        ///     Метод регистрации репозиторрия на сервисе.
        /// </summary>
        /// <param name="repo">Конфигурация подключения к репозиторию.</param>
        public void AddRepository(RepositoryConnectionSetting repo)
        {
            if (repo == null)
                return;

            //Task.Run(
            Task.Factory.StartNew(() =>
            {
                if (!_updaterProxy.PerformServiceAction(s => s.AddRepository(repo)))
                {
                    lock (_settingsLock)
                    {
                        _newRepoSettings.Add(repo);
                    }
                }
            }
                );
        }

        public UpdaterClient(string appName, ModuleVersion applicationVersion)
        {
            _applicationName = appName;
            _applicationVersion = applicationVersion;
            _updaterProxy = new UpdaterServiceProxy(this);
            _updaterProxy.DiscoveryCompleted += _updaterProxy_DiscoveryCompleted;
            //this.Timeout = checkUpdatePeriod;
        }

        /// <summary>
        ///     Метод подтверждения готовности установки обновления.
        /// </summary>
        /// <param name="info">На данный момент не ипользуется</param>
        /// <param name="isAgree">True в случае готовности.</param>
        public void ConfirmUpdate(UpdateInfo info, bool isAgree)
        {
            _requestTask = null;

            Task.Factory.StartNew( ( ) => _updaterProxy.PerformServiceAction( s => s.ConfirmUpdating( _applicationName, isAgree ) ) ); 
            //Task.Run( ( ) => _updaterProxy.PerformServiceAction( s => s.ConfirmUpdating( _applicationName, isAgree ) ) );
        }

        protected override void PeriodicActionMethod()
        {
            //bool res = false;
            _updaterProxy.PerformServiceAction(s => s.CheckConnection());
            //if(res)
            //    Trace.WriteLine(Environment.CurrentDirectory.ToString() + " Connected successfuly.");
        }

        private void _updaterProxy_DiscoveryCompleted(object sender, ServiceInstanceGuidsEventArgs e)
        {
            SubscribeOnUpdate(_applicationName, _applicationVersion);
            var commitedSettings = new List<RepositoryConnectionSetting>();
            lock (_settingsLock)
            {
                foreach (RepositoryConnectionSetting repoSetting in _newRepoSettings)
                {
                    if (_updaterProxy.PerformServiceAction(s => s.AddRepository(repoSetting)))
                        commitedSettings.Add(repoSetting);
                    else
                        break;
                }
                foreach (RepositoryConnectionSetting repoSetting in commitedSettings)
                {
                    _newRepoSettings.Remove(repoSetting);
                }
            }
            _newRepoSettings.Clear();
            //Timeout = checkUpdatePeriod;
            //this.Nudge( );
        }

        private void SubscribeOnUpdate(string appName, ModuleVersion version)
        {
            //var settings = new FtpConnectionSettings( ) { Login = "dimadiv", Password = "1", UriString = "ftp://localhost:21/Updates" };
            //_updaterProxy.PerformServiceAction( s => s.AddRepository( settings ) );
            Task.Factory.StartNew( ( ) => _updaterProxy.PerformServiceAction( s => s.Subscribe( appName, version ) ) );
            //Task.Run(() => _updaterProxy.PerformServiceAction(s => s.Subscribe(appName, version)));
        }

        #region IUpdaterNotifier Members

        /// <summary>
        ///     Метод обратного вызова сервера. Оповещение о готовности нового обновления
        /// </summary>
        /// <param name="updateInfo">Описание обновления</param>
        void IUpdaterNotifier.OnNewUpdatesReady(UpdateInfo updateInfo)
        {
            var confirmArgs = new UpdatesReadyEventArgs(updateInfo);
            lock (_lockUpdateRequest)
            {
                CancelRequest();
            }
            _requestTask = Task.Factory.StartNew( ( ) =>
            {
                _executionThreadId = Thread.CurrentThread.ManagedThreadId;
                UpdatesReady.RaiseEvent(this, confirmArgs);
            });
            //_requestTask = Task.Run(() =>
            //{
            //    _executionThreadId = Thread.CurrentThread.ManagedThreadId;
            //    UpdatesReady.RaiseEvent(this, confirmArgs);
            //});
        }

        private void CancelRequest()
        {
            if (_requestTask != null)
            {
                UpdatingCanceled.RaiseEvent(this, EventArgs.Empty);
                _requestTask = null;
                //_executionThreadId = -1;
            }
        }

        /// <summary>
        ///     Метод обратного вызова сервера. Оповещение о результате опроса готовности.
        /// </summary>
        /// <param name="isAgree">Результат опроса. Если результат положительный то следует немедленно завершить работу приложения.</param>
        /// <param name="updateFile"> Если имя дестрибутива не равно null то после завершения следует запустить этот файл.</param>
        void IUpdaterNotifier.ConfirmationResult(bool isAgree, string updateFile = null)
        {
            //CancelConfirmationTask( );
            if (isAgree)
            {
                Stop( );
                RequestShutdown.RaiseEvent(this, EventArgs.Empty);
                if (updateFile != null)
                {
                    Process.Start(updateFile);
                }
            }
            else
            {
                CancelRequest();
            }
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Remoting.Messaging;
using System.Threading;
using System.Threading.Tasks;
using Eureca.Updater.Common;
using Eureca.Updater.UpdaterClient;
using log4net;

namespace Eureca.Updater.DbUpdater
{
    public class DbDescriptor
    {
        public string ConnectionString { get; set; }

        public string DbName { get; set; }

    }

    public class DbUpdateHelper
    {
        public static DbUpdateScripts GetNextUpdate( string updatesDirectory, int currentVersion )
        {
            int nextVersion = currentVersion + 1;
            string nextUpdatePath = Path.Combine( updatesDirectory, nextVersion.ToString( ) );

            if (!Directory.Exists( nextUpdatePath ))
                return null;

            DbUpdateScripts update = new DbUpdateScripts( );
            update.Version = nextVersion;

            var fInfos = Directory.GetFiles( nextUpdatePath ).Where( item => Path.GetExtension( item ).ToLower( ) == ".sql" );
            update.Scripts = fInfos;
            return update;
        }
    }

    public class DbUpdateScripts
    {
        public int Version { get; set; }
        public IEnumerable<string> Scripts { get; set; }
    }

    public class DbUpdater
    {
        //public event EventHandler<DbUpdateLogMessage> OnMessage;
        private ILog Log = LogManager.GetLogger(typeof (DbUpdater));
        string _mutexKey = "Eureca.Integrator.Updater.DbUpdate",
            _updatesFolder;
        static List<int> _versions = new List<int>( );

        List<DbDescriptor> _dataBases;
        IUpdateSettingStorage _storage;
        IWorkerListener _listener;
        public DbUpdater( string applicationKey, IEnumerable<DbDescriptor> dataBases, string updatesFolder, IUpdateSettingStorage storage )
        {
            _mutexKey = applicationKey;
            _dataBases = new List<DbDescriptor>( dataBases );
            _storage = storage;
            _updatesFolder = updatesFolder;
        }

        public bool Update( Func<IWorkerListener> createLogFunc = null, int targetVersion =-1)
        {
            int currentDBVersion = _storage.StorageVersion;
            if (currentDBVersion == targetVersion)
                return true;
            //_listener = listener;
            if (!Directory.Exists( _updatesFolder ))
                return false;
            //List<int> versions = new List<int>();
            var updates = Directory.GetDirectories( _updatesFolder );
            foreach (var update in updates)
            {
                int version;
                if (int.TryParse( Path.GetFileName( update ), out version ))
                {
                    _versions.Add( version );
                }
            }
            
            if (_versions.Any( item => item > currentDBVersion ))
            {
                Mutex mut = null;
                try
                {
                    bool createdNew;
                    mut = new Mutex(false, _mutexKey, out createdNew);
                    if (!createdNew)
                    {
                        return false;
                    }
                    currentDBVersion = _storage.StorageVersion;
                    if (_versions.Any(item => item > currentDBVersion))
                    {
                        if (createLogFunc != null)
                        {
                            _listener = createLogFunc();
                            _listener.Run(
                                () =>
                                {
                                    BeginUpdate(targetVersion);
                                }
                                );
                        }
                        else
                            BeginUpdate(targetVersion);
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex.ToString());
                    return false;                     
                }
                finally
                {
                    if (mut != null)
                    {
                        mut.WaitOne( );
                        mut.ReleaseMutex();
                        mut.Dispose();
                    }
                }
            }
            return targetVersion == _storage.StorageVersion;
        }

        private void BeginUpdate( int targetVersion )
        {
            DbUpdateScripts upInfo = null;
            while (( upInfo = DbUpdateHelper.GetNextUpdate( _updatesFolder, _storage.StorageVersion ) ) != null)
            {
                if (targetVersion != -1 && upInfo.Version > targetVersion)
                    break;
                WriteLine( String.Format( "Начало обновления до версии {0}...", upInfo.Version.ToString( ) ) );
                TransactExecute( upInfo, _dataBases );
            }
            WriteLine( String.Format( "Обновление базы данных завершено текущая версия {0}.", _storage.StorageVersion.ToString( ) ), MessageType.Completed );
        }

        void TransactExecute( DbUpdateScripts updateInfos, IEnumerable<DbDescriptor> dataBases )
        {
            List<SqlTransaction> transactions = new List<SqlTransaction>( updateInfos.Scripts.Count( ) );
            List<SqlConnection> connections = new List<SqlConnection>( transactions.Count );
            try
            {
                foreach (var dbDescriptor in dataBases)
                {
                    var script = updateInfos.Scripts.SingleOrDefault( item => Path.GetFileNameWithoutExtension( item ).ToLower( ) == dbDescriptor.DbName.ToLower( ) );
                    if (script == null)
                        continue;
                    WriteLine( string.Format( "Обновление базы '{0}'...", dbDescriptor.DbName) );
                    SqlConnection connection = new SqlConnection( dbDescriptor.ConnectionString );
                    connections.Add( connection );
                    connection.Open( );
                    SqlTransaction transaction = connection.BeginTransaction( );
                    transactions.Add( transaction );
                    Exception ex;
                    DbHelper.ExecFile( script, connection, out ex, transaction );
                                    
                }

                transactions.ForEach( item => item.Commit( ) );
                connections.ForEach( item => { if (item != null && item.State != ConnectionState.Closed) item.Close( ); } );
                _storage.StorageVersion = updateInfos.Version;

                WriteLine( string.Format( "Обновление базы данных установлено -> {0} ", updateInfos.Version ) );
                WriteLine( );
            }
            catch (Exception ex)
            {
                transactions.ForEach( item =>
                {
                    try
                    {
                        item.Rollback();
                    }
                    catch(Exception innerException)
                    {
                    }
                } );
                connections.ForEach( item => { if (item != null && item.State != ConnectionState.Closed) item.Close( ); } );
                WriteLine( String.Format( "Обновление базы завершилось ошибкой:\n{0}", ex.Message ), MessageType.Error );
                Log.Error(ex.ToString());
                throw ex;
            }
        }

        void WriteLine( string message = "\n", MessageType messageType = MessageType.Information )
        {
            switch (messageType)
            {
                    case MessageType.Error:
                    Log.Error(message);
                    break;
                    case MessageType.Information:
                    Log.Info(message);
                    break;
                    case MessageType.Completed:
                    Log.Info("Success: " + message);
                    break;
            }
            if (_listener != null)
                _listener.WriteLine( message, messageType );
        }

        Task UpdateDatabaseAsync( string sqlScriptPath, SqlConnection connection, SqlTransaction transaction )
        {
            return Task.Factory.StartNew( ( ) =>
            {
                Exception ex;
                DbHelper.ExecFile( sqlScriptPath, connection, out ex, transaction );
            } );
        }

        Task UpdateVersionAsync( int version, IUpdateSettingStorage storage )
        {
            return Task.Factory.StartNew( ( ) =>
            {
                storage.StorageVersion = version;
            } );
        }

        public void CloseLogger()
        {
            if (_listener != null)
            {
                _listener.Dispose();
                _listener = null;
            }

        }

    }
}

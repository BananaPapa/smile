﻿using System.Runtime.Remoting.Messaging;
using Eureca.Updater.Service;
using Eureca.Updater.Wcf;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using Eureca.Updater.Common;
using log4net;

namespace Eureca.Updater.Common.Wcf
{
    public class UpdaterServiceProxy : ServiceProxy<IUpdaterService>, IDisposable
    {
        private ILog Log = LogManager.GetLogger(typeof (UpdaterServiceProxy));

        private readonly ManualResetEvent _discoveryCompletedEvent = new ManualResetEvent(true);
        private Guid? _serviceInstanceGuid;

        public UpdaterServiceProxy( object callbackContext )
            : base( BindingSource.InitNetTcp, callbackContext )
        {
            DiscoveryProgressChanged += TransportServiceProxyDiscoveryProgressChanged;
            DiscoveryCompleted += TransportServiceProxyDiscoveryCompleted;
        }

        public bool Found
        {
            get { return _serviceInstanceGuid.HasValue; }
        }

        private void TransportServiceProxyDiscoveryCompleted(object sender, ServiceInstanceGuidsEventArgs e)
        {
            try
            {
                _discoveryCompletedEvent.Set();
            }
            catch (Exception ex) {}
        }

        private void TransportServiceProxyDiscoveryProgressChanged(object sender, ServiceInstanceGuidsEventArgs e)
        {
            try
            {
                if (e.Guids != null && e.Guids.Length > 0)
                {
                    _serviceInstanceGuid = e.Guids[0];
                    CancelDiscovering();
                }
            }
            catch (Exception exception)
            {
                Log.Error(exception);
            }
        }


        public bool PerformServiceAction(Action<IUpdaterService> action)
        {
            if (!_serviceInstanceGuid.HasValue)
            {
                _discoveryCompletedEvent.Reset();

                StartDiscovering();

                if (!_discoveryCompletedEvent.WaitOne( 5000 ))
                    return false;
            }

            return _serviceInstanceGuid.HasValue && PerformServiceAction(_serviceInstanceGuid.Value, action);
        }

        #region Implementation of IDisposable

        /// <summary>
        ///     Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <filterpriority>2</filterpriority>
        public void Dispose()
        {
            Dispose(true);
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                _discoveryCompletedEvent.WaitOne();
                _discoveryCompletedEvent.Dispose();
            }
        }

        ~UpdaterServiceProxy()
        {
            Dispose(false);
        }

        #endregion
    }
}
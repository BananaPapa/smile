﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Eureca.Updater.Common;
using Eureca.Updater.UpdaterClient;

namespace Eureca.Integrator.Common
{
    public partial class UpdateForm : Form ,IWorkerListener
    {
        bool _delayClosePlanned = false;
        Action _workTask = null;
        public UpdateForm()
        {
            InitializeComponent();
            Shown += UpdateForm_Shown;
        }

        private void UpdateForm_Shown(object sender, EventArgs e)
        {
            var workTask = Task.Factory.StartNew(() => _workTask());
            workTask.ContinueWith((t) => Thread.Sleep(200)).ContinueWith(
                (t) =>
                {
                    if (!_delayClosePlanned)
                        this.Invoke((Action)(() => DialogResult = DialogResult.OK));
                }
                );
        ;

    }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);
        }

        public void WriteLine( string message = null, MessageType messageType = MessageType.Information )
        {
            if (_logMemoEdit.InvokeRequired)
            {
                _logMemoEdit.Invoke( (Action<string, MessageType>)WriteLine, new object[] { message, messageType } );
            }
            else
            {
                if (message != null)
                    _logMemoEdit.Text += string.Format( "{0} {1}\r\n", DateTime.Now, message );
                else
                    _logMemoEdit.Text += "\r\n";
                _logMemoEdit.SelectionStart = _logMemoEdit.Text.Length;
                _logMemoEdit.ScrollToCaret( );
            }
            if(messageType == MessageType.Error)
            {
                _delayClosePlanned = true; 
            }
            if (!_delayClosePlanned && messageType == MessageType.Completed)
            {
                repositoryItemMarqueeProgressBar1.Stopped = true;
                //await Task.Delay( 1000 );
                //this.Invoke( (Action)delegate( ) { this.Close( ); } );
            }
        }

        public void Run( Action work )
        {
            _workTask = work;
            this.ShowDialog( );
        }

    }
}

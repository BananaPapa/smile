﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Eureca.Updater.Downloader;
using Eureca.Updater.Common;
using System.Collections.ObjectModel;
using Eureca.Updater.Service;
using DevExpress.XtraGrid.Columns;
using Eureca.Rumble.Utility.Settings;
using Eureca.Updater.UpdaterClient;

namespace Eureca.Common.Settings
{
    public interface IApplySettings
    {
        /// <summary>
        /// Handle hide/close/lost focus event
        /// </summary>
        /// <returns>Return true if closing allowed, otherwise false</returns>
        bool OnLoseFocus();
    }

    public partial class UpdatesSettingsControl : XtraUserControl, IApplySettings
    {
        BindingList<RepositorySettingsView> Repositories
        {
            get;
            set;
        }

        public bool SettingsChanged { get; set; }

        private IUpdateSettingStorage _repoSettingsStorage;

        public IUpdateSettingStorage RepositorySettings
        {
            get 
            { 
                return _repoSettingsStorage; 
            }
            set 
            {
                if (value == null)
                    return;
                
                _repoSettingsStorage = value;
                UpdateRepoSettings( value );
            }
        }
        
        public UpdatesSettingsControl( )
        {
            InitializeComponent( );
            Load += UpdatesSettingsControl_Load;
        }

        void UpdatesSettingsControl_Load( object sender, EventArgs e )
        {
            _repoEditGridLookUp.DataSource = Enum.GetValues( typeof( RepositoryType ) );
            _repoEditGridLookUp.CustomDisplayText += _repoEditGridLookUp_CustomDisplayText;
            _repoSettingPopup.QueryPopUp += _repoSettingPopup_QueryPopUp;
            _repoSettingPopup.QueryResultValue += _repoSettingPopup_QueryResultValue;
            _repoSettingPopup.QueryDisplayText += _repoSettingPopup_QueryDisplayText;
        }

        void _repoEditGridLookUp_CustomDisplayText( object sender, DevExpress.XtraEditors.Controls.CustomDisplayTextEventArgs e )
        {
            if (e.Value == null)
                return;
            e.DisplayText = ( (RepositoryType)e.Value ).GetEnumDescription( );
        }

        private void UpdateRepoSettings( IUpdateSettingStorage settingsStorage )
        {
            Repositories = new BindingList<RepositorySettingsView>( settingsStorage.RepositorySettings.Select( item => new RepositorySettingsView( ) { Settings = item } ).ToList( ) );
            _gridControl.DataSource = Repositories;
            Repositories.ListChanged+=Repositories_ListChanged;
        }

        void Repositories_ListChanged( object sender, ListChangedEventArgs e )
        {
            if (!SettingsChanged && ( e.ListChangedType == ListChangedType.ItemAdded || e.ListChangedType == ListChangedType.ItemDeleted || e.ListChangedType == ListChangedType.ItemChanged ))
            SettingsChanged = true;
        }

        void _repoSettingPopup_QueryDisplayText( object sender, DevExpress.XtraEditors.Controls.QueryDisplayTextEventArgs e )
        {
            //throw new NotImplementedException( );
            if(e.EditValue == null)
                return;
            GridColumn column = _gridView.FocusedColumn;
            if (column != _repoSettings)
                return;

            RepositorySettingsView repoSetting = e.EditValue as RepositorySettingsView;
            if (repoSetting == null || repoSetting.Settings == null) return;

            e.DisplayText = repoSetting.Settings.ToString( );
        }

        void _repoSettingPopup_QueryResultValue( object sender, DevExpress.XtraEditors.Controls.QueryResultValueEventArgs e )
        {
            e.Value = null;

            GridColumn column = _gridView.FocusedColumn;
            if (column != _repoSettings)
                return;

            RepositorySettingsView repoSetting = _gridView.GetFocusedRow( ) as RepositorySettingsView;
            if (repoSetting == null || repoSetting.Settings == null) return;
            Exception ex;
            e.Value = _settingsControl.Settings;
        }

        void _repoSettingPopup_QueryPopUp( object sender, CancelEventArgs e )
        {
            e.Cancel = true;


            GridColumn column = _gridView.FocusedColumn;
            if (column != _repoSettings)
                return;

            RepositorySettingsView repoSettingContainer = _gridView.GetFocusedRow( ) as RepositorySettingsView;
            if (repoSettingContainer == null || !repoSettingContainer.RepoType.HasValue) return;
            if (repoSettingContainer.Settings == null || repoSettingContainer.RepoType != repoSettingContainer.Settings.GetRepositoryType())
                repoSettingContainer.Settings = CreateSettings( repoSettingContainer.RepoType.Value ); 
            SettingsBase settings = repoSettingContainer.Settings;
            
            if (settings == null) return;

            _settingsControl.Settings = settings;
            e.Cancel = false;
        }

        RepositoryConnectionSetting CreateSettings( RepositoryType repositoryType )
        {
            switch (repositoryType)
            {
                case RepositoryType.FtpServer:
                    return new FtpConnectionSettings( );
                    break;
                default:
                    break;
            }
            return null;
        }

        private void _applyButton_Click( object sender, EventArgs e )
        {
            ApplyCurrentSettings( );
        }

        private void ApplyCurrentSettings( )
        {
            Apply();
        }

        public void Apply()
        {
            _repoSettingsStorage.RepositorySettings = Repositories.Select(item => item.Settings).ToList();
            SettingsChanged = false;
        }

        /// <summary>
        /// Handle hide/close/lost focus event
        /// </summary>
        /// <returns>Return true if closing allowed, otherwise false</returns>
        public bool OnLoseFocus()
        {
            if (SettingsChanged)
            {
                var res = MessageBox.Show( "Текущие настройки не применяны. Сохранить настройки перед выходом",
                    "Сохрвнить настройки?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question );
                if (res == DialogResult.OK)
                    Apply( );
                else if (res == DialogResult.Cancel)
                    return false;
            }
            return true;
        }

    }    
  
    public class RepositorySettingsView
    {
        private RepositoryConnectionSetting _model;

        private RepositoryType? repoType;

        public RepositoryType? RepoType
        {
            get { return repoType; }
            set { repoType = value; }
        }

        public RepositoryConnectionSetting Settings
        {
            get
            {
                return _model;
            }
            set
            {
                if (value != null && _model != value)
                {
                    _model = value;
                    repoType = value.GetRepositoryType();
                }
            }
        }

        public RepositorySettingsView( ) { }
    }
}

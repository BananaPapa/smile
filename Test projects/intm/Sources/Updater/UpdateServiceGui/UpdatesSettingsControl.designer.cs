﻿namespace Eureca.Common.Settings
{
    partial class UpdatesSettingsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if (disposing && ( components != null ))
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            this._repoEditGridLookUp = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.repositoryItemGridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._repoSettingPopup = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this._popupContainerControl = new DevExpress.XtraEditors.PopupContainerControl();
            this._settingsControl = new Eureca.Rumble.Utility.Settings.SettingsControl();
            this._gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._repoType = new DevExpress.XtraGrid.Columns.GridColumn();
            this._repoSettings = new DevExpress.XtraGrid.Columns.GridColumn();
            this._gridControl = new DevExpress.XtraGrid.GridControl();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this._applyButton = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this._repoEditGridLookUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._repoSettingPopup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._popupContainerControl)).BeginInit();
            this._popupContainerControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._gridControl)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _repoEditGridLookUp
            // 
            this._repoEditGridLookUp.AutoHeight = false;
            this._repoEditGridLookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._repoEditGridLookUp.Name = "_repoEditGridLookUp";
            this._repoEditGridLookUp.View = this.repositoryItemGridLookUpEdit1View;
            // 
            // repositoryItemGridLookUpEdit1View
            // 
            this.repositoryItemGridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.repositoryItemGridLookUpEdit1View.Name = "repositoryItemGridLookUpEdit1View";
            this.repositoryItemGridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // _repoSettingPopup
            // 
            this._repoSettingPopup.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._repoSettingPopup.CloseOnLostFocus = false;
            this._repoSettingPopup.CloseOnOuterMouseClick = false;
            this._repoSettingPopup.Name = "_repoSettingPopup";
            this._repoSettingPopup.PopupControl = this._popupContainerControl;
            // 
            // _popupContainerControl
            // 
            this._popupContainerControl.Controls.Add(this._settingsControl);
            this._popupContainerControl.Location = new System.Drawing.Point(31, 136);
            this._popupContainerControl.Name = "_popupContainerControl";
            this._popupContainerControl.Size = new System.Drawing.Size(320, 250);
            this._popupContainerControl.TabIndex = 4;
            // 
            // _settingsControl
            // 
            this._settingsControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._settingsControl.Location = new System.Drawing.Point(0, 0);
            this._settingsControl.Name = "_settingsControl";
            this._settingsControl.Size = new System.Drawing.Size(320, 250);
            this._settingsControl.TabIndex = 0;
            // 
            // _gridView
            // 
            this._gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._repoType,
            this._repoSettings});
            this._gridView.GridControl = this._gridControl;
            this._gridView.Name = "_gridView";
            this._gridView.OptionsView.ShowGroupPanel = false;
            // 
            // _repoType
            // 
            this._repoType.Caption = "Тип репозитория";
            this._repoType.ColumnEdit = this._repoEditGridLookUp;
            this._repoType.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this._repoType.FieldName = "RepoType";
            this._repoType.Name = "_repoType";
            this._repoType.Visible = true;
            this._repoType.VisibleIndex = 0;
            // 
            // _repoSettings
            // 
            this._repoSettings.Caption = "Настройки";
            this._repoSettings.ColumnEdit = this._repoSettingPopup;
            this._repoSettings.FieldName = "Settings";
            this._repoSettings.Name = "_repoSettings";
            this._repoSettings.Visible = true;
            this._repoSettings.VisibleIndex = 1;
            this._repoSettings.Width = 150;
            // 
            // _gridControl
            // 
            this._gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._gridControl.Location = new System.Drawing.Point(0, 0);
            this._gridControl.MainView = this._gridView;
            this._gridControl.Name = "_gridControl";
            this._gridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this._repoEditGridLookUp,
            this._repoSettingPopup});
            this._gridControl.Size = new System.Drawing.Size(709, 468);
            this._gridControl.TabIndex = 3;
            this._gridControl.UseEmbeddedNavigator = true;
            this._gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this._gridView});
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.Controls.Add(this._applyButton);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 468);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(709, 29);
            this.flowLayoutPanel1.TabIndex = 5;
            // 
            // _applyButton
            // 
            this._applyButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._applyButton.Location = new System.Drawing.Point(631, 3);
            this._applyButton.Name = "_applyButton";
            this._applyButton.Size = new System.Drawing.Size(75, 23);
            this._applyButton.TabIndex = 3;
            this._applyButton.Text = "Применить";
            this._applyButton.Click += new System.EventHandler(this._applyButton_Click);
            // 
            // UpdatesSettingsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._gridControl);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this._popupContainerControl);
            this.Name = "UpdatesSettingsControl";
            this.Size = new System.Drawing.Size(709, 497);
            ((System.ComponentModel.ISupportInitialize)(this._repoEditGridLookUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._repoSettingPopup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._popupContainerControl)).EndInit();
            this._popupContainerControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._gridControl)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit _repoEditGridLookUp;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit1View;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit _repoSettingPopup;
        private DevExpress.XtraEditors.PopupContainerControl _popupContainerControl;
        private Rumble.Utility.Settings.SettingsControl _settingsControl;
        private DevExpress.XtraGrid.Views.Grid.GridView _gridView;
        private DevExpress.XtraGrid.Columns.GridColumn _repoType;
        private DevExpress.XtraGrid.Columns.GridColumn _repoSettings;
        private DevExpress.XtraGrid.GridControl _gridControl;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton _applyButton;

    }
}

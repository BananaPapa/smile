﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.XtraEditors;
using Eureca.Updater.Common;

namespace Eureca.Rumble.Utility.Settings
{
    /// <summary>
    ///     Represents settings editor.
    /// </summary>
    public partial class SettingsControl : XtraUserControl
    {
        /// <summary>
        ///     Initializes new instance of <see cref="T:Eureca.Rumble.Utility.Settings.SettingsControl" />.
        /// </summary>
        public SettingsControl()
        {
            InitializeComponent();
        }

        /// <summary>
        ///     Gets or sets current settings object.
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public SettingsBase Settings
        {
            get { return _propertyGrid.SelectedObject as SettingsBase; }
            set { _propertyGrid.SelectedObject = value; }
        }
    }
}
﻿using System;

namespace Eureca.Common.Settings
{
    /// <summary>
    ///     Sets display order of a property.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class PropertyOrderAttribute : Attribute
    {
        /// <summary>
        ///     Initializes new instance of <see cref="T:Eureca.Rumble.Utility.Settings.PropertyOrderAttribute" />.
        /// </summary>
        /// <param name="order">Display order of a property.</param>
        public PropertyOrderAttribute(int order)
        {
            Order = order;
        }

        /// <summary>
        ///     Gets display order of a property.
        /// </summary>
        public int Order { get; private set; }
    }
}
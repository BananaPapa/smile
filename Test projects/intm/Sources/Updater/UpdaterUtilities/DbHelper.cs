﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eureca.Updater.Common
{
    public static class DbHelper
    {
        /// <summary>
        ///     метод выполнения запроса из файла
        /// </summary>
        /// <param name="fileName">путь к файлу</param>
        /// <returns>результат выполнения</returns>
        public static bool ExecFile( string fileName, SqlConnection sqlConnection, out Exception ex, SqlTransaction transaction = null )
        {
            ex = null;
            //string sqlStr = string.Empty;
            var script = ReadFile(fileName, ref ex);
            if(script == null)
                return false;
            return ExecScript( script, sqlConnection, transaction);
        }

        private static string ReadFile( string fileName, ref Exception ex )
        {
            StreamReader sr = null;
            try
            {
                string line = null;
                sr = new StreamReader( fileName, Encoding.Default );
                line = sr.ReadToEnd( );
                sr.Close( );
                return line;
            }
            catch (Exception exx)
            {
                ex = exx;
                return null;
            }
            finally
            {
                if (sr != null)
                {
                    sr.Close( );
                    sr.Dispose( );
                }
            }
        }

        //public static bool ExecScript( string sqlStr, string connectionString )
        //{
        //    return ExecSqript( sqlStr, new SqlConnection( connectionString ) );
        //}

        public static bool ExecScript(string sqlStr, SqlConnection conn , SqlTransaction transaction = null )
        {
            sqlStr = String.Join( " \n", sqlStr.Split( new[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries ).Select( item => item.Trim( ) ) );
            int pos = GetNextCommandPos( sqlStr );
            while (pos >= 0 || sqlStr.Length > 0)
            {
                if (pos < 0)
                {
                    pos = sqlStr.Length;
                }
                string str = sqlStr.Substring( 0, pos );
                sqlStr = sqlStr.Remove( 0, Math.Min( pos + 3, sqlStr.Length ) );
                pos = GetNextCommandPos( sqlStr );

                int ret = ExecNonQuery( str, conn , transaction );
                if (ret < -1)
                    return false;
            }
            return true;
        }

        /// <summary>
        ///     метод выполнение запроса
        /// </summary>
        /// <param name="aStringToExecute">строка с запросом</param>
        /// <returns>число затрагиваемых строк</returns>
        public static int ExecNonQuery( String aStringToExecute, SqlConnection conn, SqlTransaction transaction =null)
        {
            if (conn == null)
                throw new ArgumentNullException("conn");
            int ret;
            var cmd = new SqlCommand( aStringToExecute, conn, transaction );
            cmd.CommandTimeout = 500;
            ret = cmd.ExecuteNonQuery( );
            return ret;
        }

        private static int GetNextCommandPos( string sqlStr )
        {
            int pos = -1; 
            if (pos < 0)
            {
                pos = sqlStr.ToLower( ).IndexOf( "\ngo" );
            }
            if (sqlStr.ToLower( ).IndexOf( "go " ) == 0)
            {
                pos = 0;
            }
            return pos;
        }
    }
}

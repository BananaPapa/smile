﻿using System;
using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace Eureca.Updater.Common
{
    /// <summary>
    ///     Contains serializing to XML and deserializing from XML parametrized by type of object methods.
    /// </summary>
    public static class XmlSerializationHelper
    {
        /// <summary>
        ///     Instance of <see cref="T:System.Text.Encoding" /> used as default encoder/decoder.
        /// </summary>
        /// <remarks>
        ///     Unicode (UTF-8) is used as default for .NET.
        /// </remarks>
        private static readonly Encoding Encoding = Encoding.UTF8;

        /// <summary>
        ///     Serializes the specified object to <see cref="T:System.String" /> containing an XML document.
        /// </summary>
        /// <param name="type">Type of object to serialize.</param>
        /// <param name="obj">
        ///     An instance the specified <paramref name="type" /> that represent an object to serialize.
        /// </param>
        /// ///
        /// <param name="exception">
        ///     An instance of <see cref="T:System.Exception" /> if occured; otherwise, null.
        /// </param>
        /// <returns>
        ///     A <see cref="T:System.String" /> that contains XML document or null if any error occured.
        /// </returns>
        public static string Serialize(Type type, object obj, out Exception exception)
        {
            exception = null;
            MemoryStream memoryStream = null;

            try
            {
                memoryStream = new MemoryStream();
                (new XmlSerializer(type)).Serialize(memoryStream, obj);

                return Encoding.GetString(memoryStream.ToArray());
            }
            catch (Exception e)
            {
                exception = e;
                return null;
            }
            finally
            {
                if (memoryStream != null)
                {
                    memoryStream.Dispose();
                }
            }
        }

        /// <summary>
        ///     Deserializes the specified <see cref="T:System.String" /> containing an XML document to an instance of the
        ///     specified
        ///     <paramref
        ///         name="type" />
        ///     .
        /// </summary>
        /// <param name="type">Type of an object to deserialize.</param>
        /// <param name="xml">
        ///     A <see cref="T:System.String" /> that contains XML document.
        /// </param>
        /// <param name="exception">
        ///     An instance of <see cref="T:System.Exception" /> if occured; otherwise, null.
        /// </param>
        /// <returns>
        ///     An instance of the specified <paramref name="type" /> that represent deserialized object or null if any error
        ///     occured.
        /// </returns>
        public static object Deserialize(Type type, string xml, out Exception exception)
        {
            exception = null;

            MemoryStream memoryStream = null;

            try
            {
                memoryStream = new MemoryStream(Encoding.GetBytes(xml));

                return (new XmlSerializer(type)).Deserialize(memoryStream);
            }
            catch (Exception e)
            {
                exception = e;
                return null;
            }
            finally
            {
                if (memoryStream != null)
                {
                    memoryStream.Dispose();
                }
            }
        }
    }
}
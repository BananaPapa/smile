﻿using System;
namespace Eureca.Updater.UpdaterClient
{
    public interface IWorkerListener:IDisposable
    {
        void WriteLine(string messege  = null, MessageType messageType = MessageType.Information);

        void Run( Action work );

    }

    public enum MessageType
    {
        Information,
        Error,
        Completed
    }

}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Discovery;
using System.Threading.Tasks;
using log4net;

namespace Eureca.Updater.Common
{
    /// <summary>
    ///     Represents probationer service proxy.
    /// </summary>
    public class ServiceProxy<T>
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof (ServiceProxy<T>)); 
        /// <summary>
        ///     Channel instance life-time seconds.
        /// </summary>
        private const int ChannelLifeTimeSeconds = 30;

        /// <summary>
        ///     Number of unsuccessful service operations in a row to invoke <see cref="FaultOccured" /> event.
        /// </summary>
        private readonly int _numberOfAttempts;

        /// <summary>
        ///     Whether discovery process is started.
        /// </summary>
        private volatile bool _isDiscovering;

        #region Fields And Constructor

        /// <summary>
        ///     Dictionary to associate service instance GUID and its last access time.
        /// </summary>
        private readonly Dictionary<Guid, DateTime> _accessTimeByGuidDict = new Dictionary<Guid, DateTime>();

        /// <summary>
        ///     Dictionary to associate service instance GUID and its address.
        /// </summary>
        private readonly Dictionary<Guid, string> _addressByGuidDict = new Dictionary<Guid, string>();

        /// <summary>
        ///     Instance of channel factory.
        /// </summary>
        private readonly ChannelFactory<T> _channelFactory;

        /// <summary>
        ///     Instance of discovery client.
        /// </summary>
        private readonly DiscoveryClient _discoveryClient = new DiscoveryClient(new UdpDiscoveryEndpoint());

        /// <summary>
        ///     Dictionary to associate service instance GUID and its number of faults in a row.
        /// </summary>
        private readonly Dictionary<Guid, int> _faultsByGuidDict = new Dictionary<Guid, int>();

        /// <summary>
        ///     Dictionary to associate service instance GUID and its instance.
        /// </summary>
        private readonly Dictionary<Guid, T> _instanceByGuidDict =
            new Dictionary<Guid, T>();

        /// <summary>
        ///     Thread syncronization object.
        /// </summary>
        private readonly object _syncRoot = new object();

        /// <summary>
        ///     Discorvery process state object.
        /// </summary>
        private readonly object _userState = new object();

        /// <summary>
        ///     Initializes static fields.
        /// </summary>
        public ServiceProxy( Func<Binding> bindingInitializer, object callBackContext = null, int numberOfAttempts = 5 )
        {
            if (bindingInitializer == null)
            {
                throw new ArgumentNullException("bindingInitializer");
            }

            _numberOfAttempts = numberOfAttempts;

            if (callBackContext != null)
                _channelFactory = new DuplexChannelFactory<T>(callBackContext ,bindingInitializer.Invoke( ) );
            else
                _channelFactory = new ChannelFactory<T>( bindingInitializer.Invoke( ) );
            // Add event handlers for discovery client.

            _discoveryClient.FindProgressChanged += DiscoveryClientFindProgressChanged;
            _discoveryClient.FindCompleted += DiscoveryClientFindCompleted;
        }

        #endregion

        #region Events

        /// <summary>
        ///     Occurs when discovery client has found next endpoint.
        /// </summary>
        public event EventHandler<ServiceInstanceGuidsEventArgs> DiscoveryProgressChanged;

        /// <summary>
        ///     Occurs when discovery client has completed discovering.
        /// </summary>
        public event EventHandler<ServiceInstanceGuidsEventArgs> DiscoveryCompleted;

        /// <summary>
        ///     Occurs when some of service actions failed.
        /// </summary>
        public event EventHandler<ServiceInstanceGuidsEventArgs> FaultOccured;

        /// <summary>
        ///     Invokes discovery progress changed event.
        /// </summary>
        /// <param name="e">Event arguments.</param>
        private void InvokeDiscoveryProgressChanged(ServiceInstanceGuidsEventArgs e)
        {
            EventHandler<ServiceInstanceGuidsEventArgs> handler = DiscoveryProgressChanged;
            if (handler != null) handler(null, e);
        }

        /// <summary>
        ///     Invokes discovery completed event.
        /// </summary>
        /// <param name="e">Event arguments.</param>
        private void InvokeDiscoveryCompleted(ServiceInstanceGuidsEventArgs e)
        {
            EventHandler<ServiceInstanceGuidsEventArgs> handler = DiscoveryCompleted;
            if (handler != null) handler(null, e);
        }

        /// <summary>
        ///     Invokes fault occured event.
        /// </summary>
        /// <param name="e">Event arguments.</param>
        private void InvokeFaultOccured(ServiceInstanceGuidsEventArgs e)
        {
            EventHandler<ServiceInstanceGuidsEventArgs> handler = FaultOccured;
            if (handler != null) handler(null, e);
        }

        #endregion

        #region Public

        /// <summary>
        ///     Gets list of instance guids.
        /// </summary>
        public IEnumerable<Guid> InstanceGuids
        {
            get
            {
                lock (_syncRoot)
                {
                    return _instanceByGuidDict.Keys.AsEnumerable();
                }
            }
        }

        /// <summary>
        ///     Starts discovering of probationer services.
        /// </summary>
        public void StartDiscovering()
        {
            if (_isDiscovering) return;

            _isDiscovering = true;
            _discoveryClient.FindAsync(new FindCriteria(typeof (T)), _userState);
        }

        /// <summary>
        ///     Cancels discovering of probationer services.
        /// </summary>
        public void CancelDiscovering()
        {
            _discoveryClient.CancelAsync(_userState);
        }

        /// <summary>
        ///     Clears failed attempts counter for the service instance with the specified <paramref name="instanceGuid" />.
        /// </summary>
        /// <param name="instanceGuid">
        ///     <see cref="T:System.Guid" /> that defines the service instance.
        /// </param>
        public void ClearFailedAttempts(Guid instanceGuid)
        {
            lock (_syncRoot)
            {
                if (_faultsByGuidDict.ContainsKey(instanceGuid))
                    _faultsByGuidDict[instanceGuid] = 0;
            }
        }

        /// <summary>
        ///     Gets host of services instance address.
        /// </summary>
        /// <param name="guid">GUID of service instance.</param>
        /// <returns>Host part of service instance URI</returns>
        public string GetServiceInstaceHost(Guid guid)
        {
            lock (_syncRoot)
            {
                try
                {
                    return new Uri(_addressByGuidDict[guid]).Host;
                }
                catch
                {
                    return String.Empty;
                }
            }
        }

        /// <summary>
        ///     Closes probationer service proxy.
        /// </summary>
        public void Close()
        {
            lock (_syncRoot)
            {
                try
                {
                    _channelFactory.Close();
                    _discoveryClient.Close();
                }
                catch
                {
                    // TODO: log.
                }
            }
        }

        /// <summary>
        ///     Performs service action.
        /// </summary>
        /// <param name="instanceGuid">GUID of service instance.</param>
        /// <param name="action">Action to perform.</param>
        /// <returns>True if successfully; otherwise, false.</returns>
        public bool PerformServiceAction(Guid instanceGuid, Action<T> action)
        {
            try
            {
                action.Invoke(GetServiceInstance(instanceGuid));

                lock (_syncRoot)
                {
                    if (_faultsByGuidDict.ContainsKey(instanceGuid))
                    {
                        _faultsByGuidDict.Remove(instanceGuid);
                    }
                }

                return true;
            }
            catch(Exception ex)
            {
                //FileLog.Error(ex);

                ClearInstance(instanceGuid);

                int faults = 1;

                lock (_syncRoot)
                {
                    if (!_faultsByGuidDict.ContainsKey(instanceGuid))
                    {
                        _faultsByGuidDict[instanceGuid] = faults;
                    }
                    else
                    {
                        faults = ++_faultsByGuidDict[instanceGuid];
                    }
                }

                if (faults >= _numberOfAttempts)
                {
                    new Task(() => InvokeFaultOccured(new ServiceInstanceGuidsEventArgs(new[] {instanceGuid}))).Start();
                }

                StartDiscovering();

                return false;
            }
        }

        #endregion

        /// <summary>
        ///     Gets probationer service instance by its GUID.
        /// </summary>
        /// <param name="guid">GUID of service instance.</param>
        /// <returns>Probationer service instance.</returns>
        private T GetServiceInstance(Guid guid)
        {
            object instance = null;

            lock (_syncRoot)
            {
                if (_instanceByGuidDict.ContainsKey(guid) && _accessTimeByGuidDict.ContainsKey(guid))
                {
                    if (DateTime.Now - _accessTimeByGuidDict[guid] < TimeSpan.FromSeconds(ChannelLifeTimeSeconds))
                    {
                        instance = _instanceByGuidDict[guid];
                    }
                    else
                    {
                        ClearInstance(guid);
                    }
                }

                if (instance == null)
                {
                    string address = _addressByGuidDict[guid];
                    instance = _channelFactory.CreateChannel(new EndpointAddress(address));
                    _instanceByGuidDict[guid] = (T) instance;
                }

                _accessTimeByGuidDict[guid] = DateTime.Now;

                return (T) instance;
            }
        }

        /// <summary>
        ///     Clears service instance from dictionaries.
        /// </summary>
        /// <param name="guid">GUID of service instance.</param>
        private void ClearInstance(Guid guid)
        {
            lock (_syncRoot)
            {
                _instanceByGuidDict.Remove(guid);
                _accessTimeByGuidDict.Remove(guid);
            }
        }

        /// <summary>
        ///     Gets GUID of service instance by its address.
        /// </summary>
        /// <param name="address">Address of service.</param>
        /// <returns>GUID of service instace.</returns>
        private Guid GetGuidByAddress(string address)
        {
            return _addressByGuidDict.GetKeyOfValue(address);
        }

        #region Event Handlers

        /// <summary>
        ///     Handles discovery client find progress changed event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void DiscoveryClientFindProgressChanged(object sender, FindProgressChangedEventArgs e)
        {
            lock (_syncRoot)
            {
                EndpointDiscoveryMetadata endpoint = e.EndpointDiscoveryMetadata;
                Uri uri = endpoint.Address.Uri;
                string address = uri.ToString();

                if (!_addressByGuidDict.ContainsValue(address))
                {
                    Guid guid = Guid.NewGuid();

                    _addressByGuidDict.Add(guid, uri.ToString());

                    InvokeDiscoveryProgressChanged(new ServiceInstanceGuidsEventArgs(new[] {guid}));
                }
                else // Existing instance.
                {
                    InvokeDiscoveryProgressChanged(new ServiceInstanceGuidsEventArgs(new[] {GetGuidByAddress(address)}));
                }
            }
        }

        /// <summary>
        ///     Handles discovery client find completed event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void DiscoveryClientFindCompleted(object sender, FindCompletedEventArgs e)
        {
            try
            {
                lock (_syncRoot)
                {
                    if (!e.Cancelled)
                    {
                        Collection<EndpointDiscoveryMetadata> endpoints = e.Result.Endpoints;

                        string[] addresses = _addressByGuidDict.Values.ToArray();

                        // Clear channels of not existing anymore endpoints.
                        foreach (
                            Guid guid in
                                from a in addresses
                                where endpoints.All(p => p.Address.Uri.ToString() != a)
                                select GetGuidByAddress(a))
                        {
                            _addressByGuidDict.Remove(guid);
                            ClearInstance(guid);
                        }
                        
                    }

                    InvokeDiscoveryCompleted( new ServiceInstanceGuidsEventArgs( _addressByGuidDict.Keys.ToArray( ) ) );
                }

                _isDiscovering = false;
            }
            catch (Exception exception)
            {
                Log.Error(exception);
                //Console.WriteLine(exception);
            }
        }

        #endregion
    }
}
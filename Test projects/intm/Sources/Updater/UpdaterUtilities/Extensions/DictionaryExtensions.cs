﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Eureca.Updater.Common
{

    /// <summary>
    ///     <![CDATA[
    /// System.Collections.Generic.Dictionary<TKey, TValue> extensions.
    /// ]]>
    /// </summary>
    public static class DictionaryExtensions
    {
        /// <summary>
        ///     Gets unique key of the specified value in the specified dictionary.
        /// </summary>
        /// <typeparam name="TKey">Type of key.</typeparam>
        /// <typeparam name="TValue">Type of value.</typeparam>
        /// <param name="dictionary">Dictionary to look throw.</param>
        /// <param name="value">Value to look for.</param>
        /// <returns>Instance of unique key or default value of TKey type.</returns>
        public static TKey GetKeyOfValue<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, TValue value)
        {
            try
            {
                return dictionary.Keys.Single(k => dictionary[k].Equals(value));
            }
            catch (InvalidOperationException)
            {
                return default(TKey);
            }
        }

        /// <summary>
        ///     Gets keys of the specified value in the specified dictionary.
        /// </summary>
        /// <typeparam name="TKey">Type of key.</typeparam>
        /// <typeparam name="TValue">Type of value.</typeparam>
        /// <param name="dictionary">Dictionary to look throw.</param>
        /// <param name="value">Value to look for.</param>
        /// <returns>Enumerable collection of keys.</returns>
        public static IEnumerable<TKey> GetKeysOfValue<TKey, TValue>(this Dictionary<TKey, TValue> dictionary,
            TValue value)
        {
            return dictionary.Keys.Where(k => dictionary[k].Equals(value));
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace Eureca.Updater.Common
{
    public static class WinServiceHelper
    {
        public static bool StartService( string serviceName, int timeoutMilliseconds =5000 )
        {
            using (ServiceController service = new ServiceController( serviceName ))
            {
                try
                {
                    TimeSpan timeout = TimeSpan.FromMilliseconds( timeoutMilliseconds );

                    service.Start( );
                    service.WaitForStatus( ServiceControllerStatus.Running, timeout );
                    return true;
                }
                catch (Exception ex)
                {
                    throw ex;
                    return false;
                }
            }
        }

        public static bool StopService(string serviceName, int timeoutMilliseconds = 5000)
        {
          using( ServiceController service = new ServiceController(serviceName))
              try
              {
                TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);

                service.Stop();
                service.WaitForStatus(ServiceControllerStatus.Stopped, timeout);
                return true;
              }
              catch (Exception ex)
              {
                  throw ex;
                  return false;
              }
        }

        public static bool IsInstalled(string serviceName )
        {
            try
            {
                var key = "HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\services\\" + serviceName + "\\";
                var name = "ImagePath";

                object keyValue = Microsoft.Win32.Registry.GetValue( key, name, null );
                if (keyValue == null)
                {
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                //Log.Error( ex );
                return false;
            }
        }


    }
}

﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Eureca.Updater.Common
{
    /// <summary>
    ///     Performs periodic invoking of a method.
    /// </summary>
    public abstract class PeriodicInvoker : IDisposable
    {
        /// <summary>
        ///     Syncronization object.
        /// </summary>
        private readonly object _syncRoot = new object();

        /// <summary>
        ///     Cancellation token source.
        /// </summary>
        protected CancellationTokenSource CancellationTokenSource;

        /// <summary>
        ///     Invoking task.
        /// </summary>
        private Task _invokingTask;

        /// <summary>
        ///     Nundging event.
        /// </summary>
        private AutoResetEvent _nudgeEvent;

        /// <summary>
        ///     Invoking period.
        /// </summary>
        private TimeSpan? _timeout = TimeSpan.FromSeconds(1);

        /// <summary>
        ///     Gets or sets invoking period.
        /// </summary>
        public TimeSpan? Timeout
        {
            get { return _timeout; }
            set { _timeout = value; }
        }

        /// <summary>
        ///     Gets whether invoking is started.
        /// </summary>
        public bool IsStarted
        {
            get
            {
                lock (_syncRoot)
                {
                    return _invokingTask != null && !_invokingTask.IsCompleted && !_invokingTask.IsFaulted &&
                           !_invokingTask.IsCanceled;
                }
            }
        }

        /// <summary>
        ///     Represents periodic action method.
        /// </summary>
        protected abstract void PeriodicActionMethod();

        /// <summary>
        ///     Task that performs invoking.
        /// </summary>
        private void InvokingTask()
        {
            do
            {
                try
                {
                    PeriodicActionMethod();
                }
                catch (Exception ex)
                {
                    //FileLog.Error(ex);
                }

                // Wait for nudge event.
                if (_timeout.HasValue)
                {
                    _nudgeEvent.WaitOne(_timeout.Value);
                }
                else
                {
                    _nudgeEvent.WaitOne();
                }
            } while (!CancellationTokenSource.IsCancellationRequested);
        }

        /// <summary>
        ///     Starts periodic invoking.
        /// </summary>
        /// <returns>true, if the state of the invoker was changed; otherwise, false.</returns>
        public virtual bool Start()
        {
            lock (_syncRoot)
            {
                if (_invokingTask == null)
                {
                    _nudgeEvent = new AutoResetEvent(false);
                    CancellationTokenSource = new CancellationTokenSource();
                    _invokingTask = new Task(InvokingTask, TaskCreationOptions.LongRunning);
                    _invokingTask.Start();

                    return true;
                }
            }

            return false;
        }

        /// <summary>
        ///     Stops periodic invoking.
        /// </summary>
        /// <returns>true, if the state of the invoker was changed; otherwise, false.</returns>
        public virtual bool Stop()
        {
            lock (_syncRoot)
            {
                if (IsStarted)
                {
                    CancellationTokenSource.Cancel();
                    _nudgeEvent.Set();
                    _invokingTask.Wait();

                    _invokingTask.Dispose();
                    _invokingTask = null;
                    CancellationTokenSource.Dispose();
                    CancellationTokenSource = null;
                    _nudgeEvent.Dispose();
                    _nudgeEvent = null;

                    return true;
                }
            }

            return false;
        }

        /// <summary>
        ///     Makes invoker call periodic action method out-of-turn.
        /// </summary>
        public void Nudge()
        {
            try
            {
                _nudgeEvent.Set();
            }
            catch (Exception ex)
            {
                // TODO: log.
            }
        }

        #region Implementation of IDisposable

        /// <summary>
        ///     Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <filterpriority>2</filterpriority>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        ///     Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <param name="disposing">
        ///     If the <see cref="Dispose" /> method is called explicitly.
        /// </param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (IsStarted)
                {
                    Stop();
                }
            }
        }

        /// <summary>
        ///     Allows an <see cref="T:Eureca.Rumble.Utility.PeriodicInvoker" /> to attempt to free resources and perform other
        ///     cleanup operations before the
        ///     <see
        ///         cref="T:Eureca.Rumble.Utility.PeriodicInvoker" />
        ///     is reclaimed by garbage collection.
        /// </summary>
        ~PeriodicInvoker()
        {
            Dispose(false);
        }

        #endregion
    }
}
﻿using System;

namespace Eureca.Updater.Common
{
    /// <summary>
    ///     Represents service instance guids event arguments.
    /// </summary>
    public class ServiceInstanceGuidsEventArgs : EventArgs
    {
        /// <summary>
        ///     Initializes new intance of service instance discovery event arguments.
        /// </summary>
        /// <param name="guids">Array of instance GUIDs.</param>
        public ServiceInstanceGuidsEventArgs(Guid[] guids)
        {
            Guids = guids;
        }

        /// <summary>
        ///     Gets array of discovered instance GUIDs.
        /// </summary>
        public Guid[] Guids { get; private set; }
    }
}
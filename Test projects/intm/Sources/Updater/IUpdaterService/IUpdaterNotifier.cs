﻿using System.ServiceModel;
using Eureca.Updater.Common;

namespace Eureca.Updater.Service
{
    public interface IUpdaterNotifier
    {
        [OperationContract(IsOneWay=true)]
        void OnNewUpdatesReady(UpdateInfo updateInfo);
        
        [OperationContract( IsOneWay = true )]
        void ConfirmationResult( bool isAgree ,string updateFilePath = null);
    }
}

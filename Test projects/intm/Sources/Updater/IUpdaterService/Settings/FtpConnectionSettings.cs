﻿using System;
using System.ComponentModel;
using System.IO;
using System.Xml.Serialization;
using Eureca.Common.Settings;
using Eureca.Updater.Common;

namespace Eureca.Updater.Service
{
    [Serializable]
    public class FtpConnectionSettings : RepositoryConnectionSetting
    {
        string _relativePath;
        
        public Uri GetRepositoryUri()
        {
            return new Uri( String.Format( "{0}://{1}:{2}{3}", Uri.UriSchemeFtp, _host, _port, _relativePath ) ); 
        }
        
        string _host="localhost";
        /// <summary>
        /// Gets or sets host name of FTP server.
        /// </summary>
        [Category( "1. Сервер" )]
        [DisplayName( @"Хост" )]
        [Description( "Доменное имя или IP-адрес FTP-сервера без схемы ftp://." )]
        [PropertyOrder( 2 )]
        public string Host
        {
            get { return _host; }
            set
            {
                _host = value;
                InvokePropertyChanged( "Host" );
            }
        }

        int _port =21;

        /// <summary>
        /// Gets or sets network port FTP server listens to.
        /// </summary>
        [Category( "2. Сервер" )]
        [DisplayName( @"Порт" )]
        [Description( "Сетевой порт FTP-сервера." )]
        [PropertyOrder( 3 )]
        public int Port
        {
            get { return _port; }
            set
            {
                if(value > 0 && value < ushort.MaxValue && _port != value)
                    _port = value;
                InvokePropertyChanged( "Port" );
            }
        }

        /// <summary>
        /// Gets or sets network port FTP server listens to.
        /// </summary>
        [Category( "2. Сервер" )]
        [DisplayName( @"Относительный путь" )]
        [Description( "Путь до папки с обновлениями на сервере." )]
        [PropertyOrder( 4 )]
        public string UrlPath
        {
            get { return _relativePath; }
            set
            {
                if (Path.IsPathRooted( value ) && !value.StartsWith( "/" ))
                    return;
                if (!value.StartsWith( "\\" ) && !value.StartsWith( "/" ))
                    _relativePath = "/" + value;
                else
                    _relativePath = value;
            }
        }


        string _login;

        [Category( "3. Аутентификация" )]
        [DisplayName( @"Пользователь" )]
        [Description( "Имя пользователя для доступа к FTP-серверу." )]
        [PropertyOrder( 5 )]
        public string Login
        {
            get { return _login; }
            set { _login = value; }
        }

        string _password;

        [Category( "3. Аутентификация" )]
        [DisplayName( @"Пароль" )]
        [Description( "Пароль пользователя для доступа к FTP-серверу." )]
        [PropertyOrder( 6 )]
        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }

        public override Downloader.RepositoryType GetRepositoryType()
        {
            return Downloader.RepositoryType.FtpServer;
        }

        public override string ToString( )
        {
            return String.Format( "Хост: {0}, порт: {1}, путь: {2}, логин: {3} , пароль: {4}", _host, _port, _relativePath, _login, _password );
        }

        public override string Serialize( out Exception exception )
        {
            return Serialize( typeof( FtpConnectionSettings ), this, out exception );
        }

        public override bool AreValid( out System.Collections.Generic.IEnumerable<string> errors )
        {
            errors = null;
            try
            {
                var uri = GetRepositoryUri();
                return uri.IsWellFormedOriginalString( );
            }
            catch (Exception ex)
            {
                errors = new[] { ex.Message };
                return false;
            }
            //errors = null;
            //return RepositoryUri.Scheme == Uri.UriSchemeFtp;
        }
    }
}

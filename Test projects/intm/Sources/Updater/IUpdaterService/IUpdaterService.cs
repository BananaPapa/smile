﻿using System.ServiceModel;
using Eureca.Updater.Common;

namespace Eureca.Updater.Service
{
    [ServiceContract(CallbackContract = typeof(IUpdaterNotifier))]
    public interface IUpdaterService
    {
        [OperationContract]
        void Subscribe(string appName, ModuleVersion appVersion);
        
        [OperationContract]
        void AddRepository(RepositoryConnectionSetting repositoryConnectionSettings);

        [OperationContract]
        UpdateInfo GetAvailableVersion( string appName );


        [OperationContract]
        bool CheckConnection( );

        [OperationContract]
        void ConfirmUpdating(string appName,bool isAgree = true );
    }

    public enum UpdateState
    {
        Ready,
        No,
        Downloading,
        WaitingShutdown
    }

}

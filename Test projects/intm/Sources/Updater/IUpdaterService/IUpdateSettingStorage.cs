﻿using Eureca.Updater.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eureca.Updater.Common;

namespace Eureca.Updater.UpdaterClient
{

    public class OnSettingChangedArgs : EventArgs
    {
        public IList<RepositoryConnectionSetting> OldSettings { get; private set; }

        public IList<RepositoryConnectionSetting> NewSettings { get; private set; }

        public OnSettingChangedArgs(IList<RepositoryConnectionSetting> newSettings, IList<RepositoryConnectionSetting> oldSettings =null)
        {
            this.OldSettings = oldSettings;
            this.NewSettings = newSettings;
        }
    }

    /// <summary>
    /// Интерфейс для хранения настроек БД.
    /// </summary>
    public interface IUpdateSettingStorage
    {
        event EventHandler<OnSettingChangedArgs> SettingsChanged;

        IList<RepositoryConnectionSetting> RepositorySettings { get; set; }

        int StorageVersion { get; set; }
    }
}

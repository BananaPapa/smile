﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Xml.Serialization;

namespace Eureca.Updater.Common
{
    public class IPAddressRange : IXmlSerializable
    {
        private IPAddress startIP;
        AddressFamily addressFamily;
        byte[] lowerBytes;
        byte[] upperBytes;

        IPAddressRange( )
        {

        }

        IPAddress StartIP
        {
            get { return startIP; }
            set { startIP = value; lowerBytes = value.GetAddressBytes( ); }
        }

        private IPAddress endIP;

        IPAddress EndIP
        {
            get { return endIP; }
            set { endIP = value; upperBytes = value.GetAddressBytes( ); }
        }

        public IPAddressRange( IPAddress startIP, IPAddress endIP )
        {
            if (startIP.AddressFamily != endIP.AddressFamily)
                throw new Exception( "address famaly must be the same" );
            this.addressFamily = startIP.AddressFamily;
            StartIP = startIP;
            EndIP = endIP;
        }

        public bool IsInRange( IPAddress address )
        {
            if (address.AddressFamily != addressFamily)
            {
                return false;
            }

            byte[] addressBytes = address.GetAddressBytes( );

            bool lowerBoundary = true, upperBoundary = true;

            for (int i = 0; i < this.lowerBytes.Length &&
                ( lowerBoundary || upperBoundary ); i++)
            {
                if (( lowerBoundary && addressBytes[i] < lowerBytes[i] ) ||
                    ( upperBoundary && addressBytes[i] > upperBytes[i] ))
                {
                    return false;
                }

                lowerBoundary &= ( addressBytes[i] == lowerBytes[i] );
                upperBoundary &= ( addressBytes[i] == upperBytes[i] );
            }

            return true;
        }

        #region IXmlSerializable Members

        System.Xml.Schema.XmlSchema IXmlSerializable.GetSchema( )
        {
            throw new NotImplementedException( );
        }

        void IXmlSerializable.ReadXml( System.Xml.XmlReader reader )
        {
            this.StartIP = IPAddress.Parse( reader.GetAttribute( "StartIP" ) );
            this.EndIP = IPAddress.Parse( reader.GetAttribute( "EndIP" ) );
            this.addressFamily = this.startIP.AddressFamily;
        }

        void IXmlSerializable.WriteXml( System.Xml.XmlWriter writer )
        {
            writer.WriteAttributeString( "StartIP", StartIP.ToString( ) );
            writer.WriteAttributeString( "EndIP", EndIP.ToString( ) );

        }

        #endregion

    }

}

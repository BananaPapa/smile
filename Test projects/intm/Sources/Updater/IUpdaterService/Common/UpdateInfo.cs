﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace Eureca.Updater.Common
{
    [Serializable]
    [XmlRoot]
    public class UpdateInfo
    {
        [XmlElement]
        public ModuleVersion Version { get; set; }

        [XmlElement]
        public string Description { get; set; }

        [XmlElement]
        public bool IsCritical { get; set; }

        [XmlElement]
        public IPAddressRange TargetIpRange { get; set; }

        public UpdateInfo( )
        {
        }

        public static UpdateInfo Deserialize(Stream stream)
        {
            XmlSerializer xs = new XmlSerializer(typeof(UpdateInfo));

            return (UpdateInfo)xs.Deserialize(stream);
        }

        public override bool Equals(object obj)
        {
            UpdateInfo update = obj as UpdateInfo;

            if (update != null)
                return Version == update.Version 
                    && IsCritical == update.IsCritical
                    && Description == update.Description;
            else
                return false;
        }

        public override int GetHashCode()
        {
            return (Version + IsCritical.ToString() + Description).GetHashCode();
        }

    }
}

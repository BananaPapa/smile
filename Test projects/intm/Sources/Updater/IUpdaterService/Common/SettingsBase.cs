﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;
using Eureca.Updater.Common;


namespace Eureca.Updater.Common
{
    /// <summary>
    ///     Represets abstract base class for settings classes.
    /// </summary>
    [DataContract]
    public abstract class SettingsBase : INotifyPropertyChanged
    {
        #region Implementation of INotifyPropertyChanged

        /// <summary>
        ///     Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        /// <summary>
        ///     Invokes <see cref="PropertyChanged" /> event handlers.
        /// </summary>
        /// <param name="propertyName">Name of changed property.</param>
        protected void InvokePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        ///     Determines whether current settings are valid.
        /// </summary>
        /// <param name="errors">List of validation errors or null.</param>
        /// <returns>true if settings are valid; otherwise, false.</returns>
        public virtual bool AreValid(out IEnumerable<string> errors)
        {
            errors = null;
            return true;
        }

        /// <summary>
        ///     Serializes the current instance to XML.
        /// </summary>
        /// <param name="exception">
        ///     An instance of <see cref="T:System.Exception" /> if occured; otherwise, null.
        /// </param>
        /// <returns>
        ///     A <see cref="T:System.String" /> that contains XML document or null if any error occured.
        /// </returns>
        public abstract string Serialize(out Exception exception);

        /// <summary>
        ///     Serializes the specified object to <see cref="T:System.String" /> containing an XML document.
        /// </summary>
        /// <param name="type">Type of object to serialize.</param>
        /// <param name="obj">
        ///     An instance the specified <paramref name="type" /> that represent an object to serialize.
        /// </param>
        /// ///
        /// <param name="exception">
        ///     An instance of <see cref="T:System.Exception" /> if occured; otherwise, null.
        /// </param>
        /// <returns>
        ///     A <see cref="T:System.String" /> that contains XML document or null if any error occured.
        /// </returns>
        protected static string Serialize(Type type, object obj, out Exception exception)
        {
            return XmlSerializationHelper.Serialize(type, obj, out exception);
        }

        /// <summary>
        ///     Deserializes the specified <see cref="T:System.String" /> containing an XML document to an instance of the
        ///     specified
        ///     <paramref
        ///         name="type" />
        ///     .
        /// </summary>
        /// <param name="type">Type of an object to deserialize.</param>
        /// <param name="xml">
        ///     A <see cref="T:System.String" /> that contains XML document.
        /// </param>
        /// <param name="exception">
        ///     An instance of <see cref="T:System.Exception" /> if occured; otherwise, null.
        /// </param>
        /// <returns>
        ///     An instance of the specified <paramref name="type" /> that represent deserialized object or null if any error
        ///     occured.
        /// </returns>
        public static SettingsBase Deserialize(Type type, string xml, out Exception exception)
        {
            return XmlSerializationHelper.Deserialize(type, xml, out exception) as SettingsBase;
        }

        /// <summary>
        ///     Creates an instance of the specified <paramref name="type" /> using the <paramref name="type" />'s default
        ///     constructor.
        /// </summary>
        /// <param name="type">
        ///     The type that inherits from <see cref="T:Eureca.Rumble.Utility.Settings.SettingsBase" />.
        /// </param>
        /// <returns>
        ///     Instance of the <paramref name="type" /> or null.
        /// </returns>
        public static SettingsBase GetDefaultInstance(Type type)
        {
            try
            {
                return Activator.CreateInstance(type) as SettingsBase;
            }
            catch
            {
                return null;
            }
        }
    }
}

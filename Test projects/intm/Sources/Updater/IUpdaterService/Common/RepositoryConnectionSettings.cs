﻿using System.Runtime.Serialization;
using System.Xml.Serialization;
using Eureca.Updater.Downloader;
using Eureca.Updater.Service;

namespace Eureca.Updater.Common
{
    [DataContract]
    [KnownType( typeof( FtpConnectionSettings ) )]
    [XmlInclude( typeof( FtpConnectionSettings ) )]
    public abstract class RepositoryConnectionSetting : SettingsBase
    {
        public abstract RepositoryType GetRepositoryType( );
    }
}

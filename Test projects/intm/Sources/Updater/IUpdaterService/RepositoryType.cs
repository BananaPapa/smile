﻿using System.ComponentModel;

namespace Eureca.Updater.Downloader
{
    /// <summary>
    /// Enum of available storage types
    /// </summary>
    public enum RepositoryType
    {
        [Description("FTP сервер")]
        FtpServer = 1,
    }
}

﻿using System;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Discovery;
using System.Threading;
using System.Threading.Tasks;


namespace Eureca.Updater.Wcf
{
    /// <summary>
    ///     Represents hosting helper for WCF-service.
    /// </summary>
    /// <typeparam name="T">Service contract interface.</typeparam>
    public class Hoster<T>
    {
        /// <summary>
        ///     Service binding info.
        /// </summary>
        private readonly Binding _binding;

        /// <summary>
        ///     Hame of host.
        /// </summary>
        private readonly string _hostName;

        /// <summary>
        ///     Service instance.
        /// </summary>
        private readonly Type _serviceType;

        /// <summary>
        ///     Service listening port.
        /// </summary>
        private readonly int _port;

        /// <summary>
        ///     Name of service.
        /// </summary>
        private readonly string _serviceName;

         /// <summary>
        ///     Name of service.
        /// </summary>
        private readonly Type _interfaceType;

        /// <summary>
        ///     URI scheme.
        /// </summary>
        private readonly string _uriScheme;

        /// <summary>
        ///     Close event.
        /// </summary>
        private ManualResetEvent _closeHostEvent = new ManualResetEvent(false);

        /// <summary>
        ///     Instance of service host.
        /// </summary>
        private ServiceHost _host;

        /// <summary>
        ///     Hosting task.
        /// </summary>
        private Task _hostingTask;

        /// <summary>
        ///     Initializes new instance of <see cref="Eureca.Rumble.Utility.Wcf.Hoster{T}" />.
        /// </summary>
        /// <param name="interfaceType">Service implementation instance.</param>
        /// <param name="binding">Service binding info.</param>
        /// <param name="uriScheme">URI scheme.</param>
        /// <param name="serviceName">Name of service.</param>
        /// <param name="port">Port to listen to.</param>
        /// <param name="hostName">Hostname.</param>
        public Hoster( Type serviceType, Type interfaceType, Binding binding, string uriScheme, string serviceName, int port,
            string hostName = "localhost")
        {
            _serviceType = serviceType;
            _interfaceType = interfaceType;
            _uriScheme = uriScheme;
            _serviceName = serviceName;
            _port = port;
            _hostName = hostName;
            _binding = binding;
        }

        /// <summary>
        ///     Starts hosting.
        /// </summary>
        public void Start()
        {
            if (_hostingTask == null)
            {
                _hostingTask = new Task(HostingTask, TaskCreationOptions.LongRunning);
                _hostingTask.Start();
            }
        }

        /// <summary>
        ///     Stops hosting.
        /// </summary>
        public void Stop()
        {
            if (_hostingTask != null)
            {
                _closeHostEvent.Set();
                _hostingTask.Wait();
                _hostingTask.Dispose();
                _hostingTask = null;
                _closeHostEvent.Dispose();
                _closeHostEvent = null;
            }
        }

        /// <summary>
        ///     Hosting task.
        /// </summary>
        private void HostingTask()
        {
            try
            {

                var builder = new UriBuilder(_uriScheme, _hostName, _port, _serviceName);
                //_host = new ServiceHost( _serviceType);
                _host = new ServiceHost( _serviceType, builder.Uri );
                var endpoint = _host.AddServiceEndpoint( typeof( Eureca.Updater.Service.IUpdaterService), _binding, "" );
                //builder.Uri.ToString( )
                //var metadataBehavior = _host.Description.Behaviors.Find<ServiceMetadataBehavior>( );
                //if (metadataBehavior == null)
                //{
                //    metadataBehavior = new ServiceMetadataBehavior( );
                //    _host.Description.Behaviors.Add( metadataBehavior );
                //}
                //_host.AddServiceEndpoint(typeof( IMetadataExchange ), _binding, "MEX" );

                // Add discovery behavior.
                _host.Description.Behaviors.Add(new ServiceDiscoveryBehavior());
                _host.AddServiceEndpoint(new UdpDiscoveryEndpoint());

                EventLog.WriteEntry( "Updater Service", String.Join( "\n", _host.Description.Endpoints.Select( item => item.Address.ToString( ) ) ), EventLogEntryType.Information);
                _host.Open();

                _closeHostEvent.WaitOne();

                _host.Close();
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry( "Updater Service", ex.ToString( ),EventLogEntryType.Error );
            }
        }
    }
}
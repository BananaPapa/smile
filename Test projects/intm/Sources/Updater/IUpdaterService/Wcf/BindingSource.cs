﻿using System;
using System.ServiceModel;

namespace Eureca.Updater.Wcf
{
    /// <summary>
    ///     Contains <see cref="T:System.ServiceModel.Channels.Binding" /> factory methods.
    /// </summary>
    public static class BindingSource
    {
        /// <summary>
        ///     Initializes new instance of <see cref="T:System.ServiceModel.NetTcpBinding" />.
        /// </summary>
        /// <returns>
        ///     An instance of <see cref="T:System.ServiceModel.NetTcpBinding" />.
        /// </returns>
        public static NetTcpBinding InitNetTcp()
        {
            return new NetTcpBinding(SecurityMode.None)
            {
                MaxBufferPoolSize = Int32.MaxValue,
                MaxBufferSize = Int32.MaxValue,
                MaxReceivedMessageSize = Int32.MaxValue,
                ReaderQuotas =  
                {
                    MaxArrayLength = Int32.MaxValue,
                    MaxBytesPerRead = Int32.MaxValue,
                    MaxDepth = Int32.MaxValue,
                    MaxNameTableCharCount = Int32.MaxValue,
                    MaxStringContentLength = Int32.MaxValue
                }
            };
        }
    }
}
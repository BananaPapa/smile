﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Eureca.Updater.Common;
using Eureca.Updater.Wcf;
using WcfSvcHost;

namespace Eureca.Updater.Service.Hosting
{
    static class Program
    {
  
        static string _serviceName = "Updater Service";

        //static UpdaterService _sm;
//#if(Debug)
        static Hoster<UpdaterService> _hoster;
//#endif
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main( )
        {
            bool isServiceWasStopped = false;
                
            try
            {
                AppDomain.CurrentDomain.UnhandledException += ( CurrentDomain_UnhandledException );

                if (!Environment.UserInteractive)
                {
                    ServiceBase[] ServicesToRun;
                    ServicesToRun = new ServiceBase[] { new WcfServiceHost() };
                    ServiceBase.Run( ServicesToRun );
                }
                else
                {
//#if(Debug)
               _hoster = new Hoster<UpdaterService>( typeof( UpdaterService ), typeof( IUpdaterService ), Eureca.Updater.Wcf.BindingSource.InitNetTcp(), Uri.UriSchemeNetTcp, "UpdateService", 34567 );
                //Eureca.Updater.Wcf.BindingSource.InitNetTcp( )
                _hoster.Start();
                var settingForm = new ServiceSettingWindow( );
                Application.Run( settingForm );
                _hoster.Stop( );
//#else
                    //if (WinServiceHelper.IsInstalled( _serviceName ))
                    //{

                    //    var sc = new ServiceController( _serviceName );
                    //    if (sc.Status == ServiceControllerStatus.Running)
                    //    {
                    //        WinServiceHelper.StopService( _serviceName );
                    //        isServiceWasStopped = true;
                    //    }
                    //}
                    //Application.Run( new ServiceSettingWindow( ) );
//#endif
                }
            }
            finally
            {
                if (isServiceWasStopped && Environment.UserInteractive)
                {
                    if (!WinServiceHelper.StartService( _serviceName ))
                        throw new Exception( "Failed to start service." );
                }
            }
        }

        private static void CurrentDomain_UnhandledException( object sender, UnhandledExceptionEventArgs e )
        {
            //Log.Add( "Необработанное исключение" );
            //Log.Add( e.ExceptionObject );
            if (
                MessageBox.Show(
                    "Выполненное действие вызвало ошибку.\n За информацией обратитесь в службу технической поддержки. Продолжить работу?", "Произошла ошибка" , MessageBoxButtons.YesNo , MessageBoxIcon.Error ) !=
                DialogResult.Yes)
                Application.Exit( );
        }
    
    }
}

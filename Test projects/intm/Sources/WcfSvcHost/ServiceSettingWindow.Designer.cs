﻿namespace Eureca.Updater.Service
{
    partial class ServiceSettingWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if (disposing && ( components != null ))
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            this.updatesSettingsControl1 = new Eureca.Common.Settings.UpdatesSettingsControl();
            this.SuspendLayout();
            // 
            // updatesSettingsControl1
            // 
            this.updatesSettingsControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.updatesSettingsControl1.Location = new System.Drawing.Point(0, 0);
            this.updatesSettingsControl1.Name = "updatesSettingsControl1";
            this.updatesSettingsControl1.RepositorySettings = null;
            this.updatesSettingsControl1.Size = new System.Drawing.Size(841, 572);
            this.updatesSettingsControl1.TabIndex = 0;
            // 
            // ServiceSettingWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(841, 572);
            this.Controls.Add(this.updatesSettingsControl1);
            this.Name = "ServiceSettingWindow";
            this.Text = "ServiceSettingWindow";
            this.ResumeLayout(false);

        }

        #endregion

        private Eureca.Common.Settings.UpdatesSettingsControl updatesSettingsControl1;
    }
}
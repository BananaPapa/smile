﻿namespace WcfSvcHost
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public string ServiceName 
        {
            get
            {
                return UpdateServiceInstaller.ServiceName;
            }
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if (disposing && ( components != null ))
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            this.UpdateServiceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.UpdateServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // UpdateServiceProcessInstaller
            // 
            this.UpdateServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalService;
            this.UpdateServiceProcessInstaller.Password = null;
            this.UpdateServiceProcessInstaller.Username = null;
            // 
            // UpdateServiceInstaller
            // 
            this.UpdateServiceInstaller.Description = "Служба для обновления продуктов компании Эврика.";
            this.UpdateServiceInstaller.DisplayName = "Служба обновления продуктов Эврика";
            this.UpdateServiceInstaller.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.UpdateServiceProcessInstaller});
            this.UpdateServiceInstaller.ServiceName = "Update Service";
            this.UpdateServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.UpdateServiceInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller UpdateServiceProcessInstaller;
        private System.ServiceProcess.ServiceInstaller UpdateServiceInstaller;
    }
}
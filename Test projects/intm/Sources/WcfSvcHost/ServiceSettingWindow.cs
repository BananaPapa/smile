﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Eureca.Updater.Common;
using Eureca.Updater.UpdaterClient;


namespace Eureca.Updater.Service
{
    public partial class ServiceSettingWindow : Form
    {
        CommonUpdaterConfig _configFile;

        public CommonUpdaterConfig ConfigFile
        {
            get 
            { 
                return _configFile; 
            }
            set 
            { 
                _configFile = value;
                updatesSettingsControl1.RepositorySettings = (IUpdateSettingStorage)value;
            }
        }

        public ServiceSettingWindow( )
        {
            InitializeComponent( );
        }

    }
}


using System;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;

namespace Curriculum
{
    /// <summary>
    /// Interface for types with property "Name".
    /// </summary>
    public interface INameOwner
    {
        /// <summary>
        /// Name of object.
        /// </summary>
        string Name { get; set; }
    }

    /// <summary>
    /// Container of name owner tools.
    /// </summary>
    public static class NameOwnerTools
    {
        /// <summary>
        /// Compares name owner with another object.
        /// </summary>
        /// <param name="nameOwner">Name owner instance.</param>
        /// <param name="obj">Another object.</param>
        /// <returns>Standard -1, 0, 1.</returns>
        public static int CompareNameOwnerWithObject(INameOwner nameOwner, object obj)
        {
            if (nameOwner == null || obj == null || !(obj is INameOwner))
            {
                return 0;
            }

            return String.Compare(nameOwner.Name, ((INameOwner) obj).Name);
        }

        /// <summary>
        /// Fills repository item combo box with names.
        /// </summary>
        /// <typeparam name="T">CommentType of database table record.</typeparam>
        /// <param name="comboBox">Combo box instance.</param>
        public static void FillComboBox<T>(RepositoryItemComboBox comboBox) where T : class
        {
            comboBox.Items.Clear();

            Program.DbSingletone.RefreshDataSource(typeof (T));

            foreach (T item in Program.DbSingletone.GetDataSource(typeof (T)))
            {
                comboBox.Items.Add(item);
            }
        }

        /// <summary>
        /// Fills windows forms combo box with names.
        /// </summary>
        /// <typeparam name="T">CommentType of database table record.</typeparam>
        /// <param name="comboBox">Combo box instance.</param>
        public static void FillComboBox<T>(ComboBoxEdit comboBox) where T : class
        {
            comboBox.Properties.Items.Clear();

            foreach (T item in Program.DbSingletone.GetDataSource(typeof (T)))
            {
                comboBox.Properties.Items.Add(item);
            }
        }

        /// <summary>
        /// Updates repository item combo box with new value.
        /// </summary>
        /// <typeparam name="T">CommentType of value.</typeparam>
        /// <param name="value">Value as it is.</param>
        /// <param name="comboBox">Combo box instance.</param>
        public static void UpdateComboBox<T>(T value, RepositoryItemComboBox comboBox) where T : class
        {
            if (value != null && comboBox != null && !comboBox.Items.Contains(value))
            {
                comboBox.Items.Add(value);
            }
        }

        /// <summary>
        /// Gets part of name.
        /// </summary>
        /// <param name="name">Full name.</param>
        /// <param name="index">Index of part.</param>
        /// <returns></returns>
        public static string GetNamePart(string name, int index)
        {
            if (String.IsNullOrEmpty(name)) return null;

            string[] parts = name.Split(new[] {" "}, StringSplitOptions.RemoveEmptyEntries);

            if (parts.Length >= index + 1)
            {
                return parts[index];
            }

            return String.Empty;
        }

        /// <summary>
        /// Gets first name.
        /// </summary>
        /// <param name="name">Full name.</param>
        /// <returns>First name or null.</returns>
        public static string GetFirstName(string name)
        {
            return GetNamePart(name, 1);
        }

        /// <summary>
        /// Gets last name.
        /// </summary>
        /// <param name="name">Full name.</param>
        /// <returns>First last or null.</returns>
        public static string GetLastName(string name)
        {
            return GetNamePart(name, 0);
        }

        /// <summary>
        /// Gets middle name.
        /// </summary>
        /// <param name="name">Full name.</param>
        /// <returns>First middle or null.</returns>
        public static string GetMiddleName(string name)
        {
            return GetNamePart(name, 2);
        }
    }
}
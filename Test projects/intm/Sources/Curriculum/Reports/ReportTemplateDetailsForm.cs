﻿using System;
using System.Windows.Forms;
using DevExpress.XtraEditors.Controls;

namespace Curriculum.Reports
{
    public partial class ReportTemplateDetailsForm : Form
    {
        private readonly ReportTemplate _template;

        public ReportTemplateDetailsForm(ReportTemplate template)
        {
            InitializeComponent();
            _template = template;
        }

        public ReportTemplateDetailsForm() : this(null)
        {
        }

        public string TemplateName
        {
            get { return _nameMemoEdit.Text.Trim(); }
        }

        public string TemplateFile
        {
            get { return _fileButtonEdit.Text; }
        }

        private void ReportTemplateDetailsFormLoad(object sender, EventArgs e)
        {
            if (_template != null)
            {
                _nameMemoEdit.Text = _template.Name;
            }
        }

        private void FileButtonEditButtonClick(object sender, ButtonPressedEventArgs e)
        {
            var dialog = new OpenFileDialog
                             {
                                 Title = @"Выберите Файл с шаблоном",
                                 Filter = Reporter.ExtensionFilter,
                                 AddExtension = true,
                                 CheckFileExists = true,
                                 Multiselect = false
                             };

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                _fileButtonEdit.Text = dialog.FileName;
            }
        }

        private void EditorEditValueChanged(object sender, EventArgs e)
        {
            _okSimpleButton.Enabled = !String.IsNullOrEmpty(TemplateName) && !String.IsNullOrEmpty(TemplateFile);
        }

        private void OkSimpleButtonClick(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }
    }
}
namespace Curriculum.Reports
{
    partial class TestPassingControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupControlFIO = new DevExpress.XtraEditors.GroupControl();
            this.gridControlTestPassings = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnShip = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this._editPersonPanelControl = new DevExpress.XtraEditors.PanelControl();
            this._editPersonSimpleButton = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlFIO)).BeginInit();
            this.groupControlFIO.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlTestPassings)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._editPersonPanelControl)).BeginInit();
            this._editPersonPanelControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupControlFIO
            // 
            this.groupControlFIO.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControlFIO.Controls.Add(this.gridControlTestPassings);
            this.groupControlFIO.Location = new System.Drawing.Point(0, 0);
            this.groupControlFIO.Name = "groupControlFIO";
            this.groupControlFIO.Size = new System.Drawing.Size(706, 540);
            this.groupControlFIO.TabIndex = 23;
            this.groupControlFIO.Text = "����������� �����";
            // 
            // gridControlTestPassings
            // 
            this.gridControlTestPassings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlTestPassings.Location = new System.Drawing.Point(2, 21);
            this.gridControlTestPassings.MainView = this.gridView1;
            this.gridControlTestPassings.Name = "gridControlTestPassings";
            this.gridControlTestPassings.Size = new System.Drawing.Size(702, 517);
            this.gridControlTestPassings.TabIndex = 15;
            this.gridControlTestPassings.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn3,
            this.gridColumn19,
            this.gridColumn2,
            this.gridColumnShip,
            this.gridColumn1,
            this.gridColumn4,
            this.gridColumn5});
            this.gridView1.GridControl = this.gridControlTestPassings;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsDetail.EnableMasterViewMode = false;
            this.gridView1.OptionsView.ShowDetailButtons = false;
            this.gridView1.ViewCaption = "123";
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "����";
            this.gridColumn3.FieldName = "sTime";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 0;
            this.gridColumn3.Width = 84;
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "������������ �����";
            this.gridColumn19.FieldName = "Title";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.Visible = true;
            this.gridColumn19.VisibleIndex = 1;
            this.gridColumn19.Width = 220;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "���������  %";
            this.gridColumn2.FieldName = "PassingScore";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 2;
            this.gridColumn2.Width = 96;
            // 
            // gridColumnShip
            // 
            this.gridColumnShip.Caption = "��������� %";
            this.gridColumnShip.FieldName = "Perc";
            this.gridColumnShip.Name = "gridColumnShip";
            this.gridColumnShip.Visible = true;
            this.gridColumnShip.VisibleIndex = 3;
            this.gridColumnShip.Width = 96;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "������";
            this.gridColumn1.FieldName = "Valuation";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 4;
            this.gridColumn1.Width = 58;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "IsSupervise";
            this.gridColumn4.FieldName = "IsSupervise";
            this.gridColumn4.Name = "gridColumn4";
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "�����������";
            this.gridColumn5.FieldName = "Description";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 5;
            this.gridColumn5.Width = 127;
            // 
            // _editPersonPanelControl
            // 
            this._editPersonPanelControl.Controls.Add(this._editPersonSimpleButton);
            this._editPersonPanelControl.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._editPersonPanelControl.Location = new System.Drawing.Point(0, 544);
            this._editPersonPanelControl.Name = "_editPersonPanelControl";
            this._editPersonPanelControl.Size = new System.Drawing.Size(708, 30);
            this._editPersonPanelControl.TabIndex = 24;
            // 
            // _editPersonSimpleButton
            // 
            this._editPersonSimpleButton.Dock = System.Windows.Forms.DockStyle.Right;
            this._editPersonSimpleButton.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this._editPersonSimpleButton.Location = new System.Drawing.Point(588, 2);
            this._editPersonSimpleButton.Name = "_editPersonSimpleButton";
            this._editPersonSimpleButton.Size = new System.Drawing.Size(118, 26);
            this._editPersonSimpleButton.TabIndex = 2;
            this._editPersonSimpleButton.Text = "������������ �����";
            this._editPersonSimpleButton.ToolTip = "������������ ������";
            this._editPersonSimpleButton.Click += new System.EventHandler(this.EditPersonSimpleButtonClick);
            // 
            // TestPassingControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(708, 574);
            this.Controls.Add(this._editPersonPanelControl);
            this.Controls.Add(this.groupControlFIO);
            this.Name = "TestPassingControl";
            this.ShowIcon = false;
            this.Text = "�������� �����������";
            ((System.ComponentModel.ISupportInitialize)(this.groupControlFIO)).EndInit();
            this.groupControlFIO.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlTestPassings)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._editPersonPanelControl)).EndInit();
            this._editPersonPanelControl.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControlFIO;
        private DevExpress.XtraGrid.GridControl gridControlTestPassings;
        private DevExpress.XtraEditors.PanelControl _editPersonPanelControl;
        private DevExpress.XtraEditors.SimpleButton _editPersonSimpleButton;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnShip;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
    }
}

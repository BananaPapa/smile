﻿namespace Curriculum.Reports
{
    partial class ReportTemplatesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._templatesGridControl = new DevExpress.XtraGrid.GridControl();
            this._templatesGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._nameGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._nameMemoEdit = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this._lastModifiedGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._buttonsPanelControl = new DevExpress.XtraEditors.PanelControl();
            this._desciptionSimpleButton = new DevExpress.XtraEditors.SimpleButton();
            this._deleteSimpleButton = new DevExpress.XtraEditors.SimpleButton();
            this._replaceSimpleButton = new DevExpress.XtraEditors.SimpleButton();
            this._addSimpleButton = new DevExpress.XtraEditors.SimpleButton();
            this._saveSimpleButton = new DevExpress.XtraEditors.SimpleButton();
            this._okSimpleButton = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this._templatesGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._templatesGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._nameMemoEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._buttonsPanelControl)).BeginInit();
            this._buttonsPanelControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // _templatesGridControl
            // 
            this._templatesGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._templatesGridControl.Location = new System.Drawing.Point(0, 0);
            this._templatesGridControl.MainView = this._templatesGridView;
            this._templatesGridControl.Name = "_templatesGridControl";
            this._templatesGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this._nameMemoEdit});
            this._templatesGridControl.Size = new System.Drawing.Size(594, 342);
            this._templatesGridControl.TabIndex = 0;
            this._templatesGridControl.Tag = "Шаблоны отчёта#r";
            this._templatesGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this._templatesGridView});
            // 
            // _templatesGridView
            // 
            this._templatesGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._nameGridColumn,
            this._lastModifiedGridColumn});
            this._templatesGridView.GridControl = this._templatesGridControl;
            this._templatesGridView.Name = "_templatesGridView";
            this._templatesGridView.OptionsBehavior.Editable = false;
            this._templatesGridView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this._templatesGridView.OptionsSelection.EnableAppearanceHideSelection = false;
            this._templatesGridView.OptionsView.RowAutoHeight = true;
            this._templatesGridView.OptionsView.ShowGroupPanel = false;
            this._templatesGridView.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.TemplatesGridViewFocusedRowChanged);
            // 
            // _nameGridColumn
            // 
            this._nameGridColumn.Caption = "Название";
            this._nameGridColumn.ColumnEdit = this._nameMemoEdit;
            this._nameGridColumn.FieldName = "Name";
            this._nameGridColumn.Name = "_nameGridColumn";
            this._nameGridColumn.Visible = true;
            this._nameGridColumn.VisibleIndex = 0;
            // 
            // _nameMemoEdit
            // 
            this._nameMemoEdit.Name = "_nameMemoEdit";
            // 
            // _lastModifiedGridColumn
            // 
            this._lastModifiedGridColumn.Caption = "Последнее изменение";
            this._lastModifiedGridColumn.FieldName = "LastModified";
            this._lastModifiedGridColumn.Name = "_lastModifiedGridColumn";
            this._lastModifiedGridColumn.Visible = true;
            this._lastModifiedGridColumn.VisibleIndex = 1;
            // 
            // _buttonsPanelControl
            // 
            this._buttonsPanelControl.Controls.Add(this._desciptionSimpleButton);
            this._buttonsPanelControl.Controls.Add(this._deleteSimpleButton);
            this._buttonsPanelControl.Controls.Add(this._replaceSimpleButton);
            this._buttonsPanelControl.Controls.Add(this._addSimpleButton);
            this._buttonsPanelControl.Controls.Add(this._saveSimpleButton);
            this._buttonsPanelControl.Controls.Add(this._okSimpleButton);
            this._buttonsPanelControl.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._buttonsPanelControl.Location = new System.Drawing.Point(0, 342);
            this._buttonsPanelControl.Name = "_buttonsPanelControl";
            this._buttonsPanelControl.Size = new System.Drawing.Size(594, 30);
            this._buttonsPanelControl.TabIndex = 1;
            // 
            // _desciptionSimpleButton
            // 
            this._desciptionSimpleButton.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this._desciptionSimpleButton.Enabled = false;
            this._desciptionSimpleButton.Image = global::Curriculum.Properties.Resources.Document16;
            this._desciptionSimpleButton.Location = new System.Drawing.Point(323, 2);
            this._desciptionSimpleButton.Name = "_desciptionSimpleButton";
            this._desciptionSimpleButton.Size = new System.Drawing.Size(122, 26);
            this._desciptionSimpleButton.TabIndex = 2;
            this._desciptionSimpleButton.Tag = "Шаблоны отчёта#rc";
            this._desciptionSimpleButton.Text = "Описание отчета";
            this._desciptionSimpleButton.Click += new System.EventHandler(this.DesciptionSimpleButtonClick);
            // 
            // _deleteSimpleButton
            // 
            this._deleteSimpleButton.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this._deleteSimpleButton.Enabled = false;
            this._deleteSimpleButton.Image = global::Curriculum.Properties.Resources.Delete16;
            this._deleteSimpleButton.Location = new System.Drawing.Point(92, 4);
            this._deleteSimpleButton.Name = "_deleteSimpleButton";
            this._deleteSimpleButton.Size = new System.Drawing.Size(23, 23);
            this._deleteSimpleButton.TabIndex = 1;
            this._deleteSimpleButton.Tag = "Шаблоны отчёта#rd";
            this._deleteSimpleButton.ToolTip = "Удалить выделенный шаблон";
            this._deleteSimpleButton.Click += new System.EventHandler(this.DeleteSimpleButtonClick);
            // 
            // _replaceSimpleButton
            // 
            this._replaceSimpleButton.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this._replaceSimpleButton.Enabled = false;
            this._replaceSimpleButton.Image = global::Curriculum.Properties.Resources.Replace16;
            this._replaceSimpleButton.Location = new System.Drawing.Point(63, 4);
            this._replaceSimpleButton.Name = "_replaceSimpleButton";
            this._replaceSimpleButton.Size = new System.Drawing.Size(23, 23);
            this._replaceSimpleButton.TabIndex = 1;
            this._replaceSimpleButton.Tag = "Шаблоны отчёта#rc";
            this._replaceSimpleButton.ToolTip = "Заменить выделенный шаблон файлом";
            this._replaceSimpleButton.Click += new System.EventHandler(this.ReplaceSimpleButtonClick);
            // 
            // _addSimpleButton
            // 
            this._addSimpleButton.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this._addSimpleButton.Image = global::Curriculum.Properties.Resources.Add16;
            this._addSimpleButton.Location = new System.Drawing.Point(34, 4);
            this._addSimpleButton.Name = "_addSimpleButton";
            this._addSimpleButton.Size = new System.Drawing.Size(23, 23);
            this._addSimpleButton.TabIndex = 1;
            this._addSimpleButton.Tag = "Шаблоны отчёта#ra";
            this._addSimpleButton.ToolTip = "Добавить новый шаблон";
            this._addSimpleButton.Click += new System.EventHandler(this.AddSimpleButtonClick);
            // 
            // _saveSimpleButton
            // 
            this._saveSimpleButton.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this._saveSimpleButton.Enabled = false;
            this._saveSimpleButton.Image = global::Curriculum.Properties.Resources.Save16;
            this._saveSimpleButton.Location = new System.Drawing.Point(5, 4);
            this._saveSimpleButton.Name = "_saveSimpleButton";
            this._saveSimpleButton.Size = new System.Drawing.Size(23, 23);
            this._saveSimpleButton.TabIndex = 1;
            this._saveSimpleButton.Tag = "Шаблоны отчёта#r";
            this._saveSimpleButton.ToolTip = "Сохранить шаблон в файл";
            this._saveSimpleButton.Click += new System.EventHandler(this.SaveSimpleButtonClick);
            // 
            // _okSimpleButton
            // 
            this._okSimpleButton.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this._okSimpleButton.Enabled = false;
            this._okSimpleButton.Image = global::Curriculum.Properties.Resources.MSWordDocument;
            this._okSimpleButton.Location = new System.Drawing.Point(451, 2);
            this._okSimpleButton.Name = "_okSimpleButton";
            this._okSimpleButton.Size = new System.Drawing.Size(140, 26);
            this._okSimpleButton.TabIndex = 0;
            this._okSimpleButton.Tag = "Шаблоны отчёта#r";
            this._okSimpleButton.Text = "Сформировать отчёт";
            this._okSimpleButton.Click += new System.EventHandler(this.OkSimpleButtonClick);
            // 
            // ReportTemplatesForm
            // 
            this.AcceptButton = this._okSimpleButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(594, 372);
            this.Controls.Add(this._templatesGridControl);
            this.Controls.Add(this._buttonsPanelControl);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(500, 200);
            this.Name = "ReportTemplatesForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "Выбор шаблона отчёта#r";
            this.Text = "Выбор шаблона отчёта";
            this.Load += new System.EventHandler(this.ReportTemplatesFormLoad);
            ((System.ComponentModel.ISupportInitialize)(this._templatesGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._templatesGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._nameMemoEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._buttonsPanelControl)).EndInit();
            this._buttonsPanelControl.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl _templatesGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView _templatesGridView;
        private DevExpress.XtraGrid.Columns.GridColumn _nameGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _lastModifiedGridColumn;
        private DevExpress.XtraEditors.PanelControl _buttonsPanelControl;
        private DevExpress.XtraEditors.SimpleButton _okSimpleButton;
        private DevExpress.XtraEditors.SimpleButton _saveSimpleButton;
        private DevExpress.XtraEditors.SimpleButton _deleteSimpleButton;
        private DevExpress.XtraEditors.SimpleButton _replaceSimpleButton;
        private DevExpress.XtraEditors.SimpleButton _addSimpleButton;
        private DevExpress.XtraEditors.SimpleButton _desciptionSimpleButton;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit _nameMemoEdit;
    }
}
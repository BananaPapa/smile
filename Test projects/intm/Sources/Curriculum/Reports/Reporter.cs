﻿using System;
using System.Data.Common;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MessageBox = Eureca.Integrator.Utility.Forms.MessageBox;

namespace Curriculum.Reports
{
    internal static class Reporter
    {
        #region Delegates

        /// <summary>
        /// Save report delegate.
        /// </summary>
        /// <param name="fileName">File path to save as.</param>
        /// <returns>Exception if occurs; otherwise, null.</returns>
        public delegate Exception SaveReportDelegate(string fileName);

        #endregion

        /// <summary>
        /// Extension filter string for file open and save dialogs.
        /// </summary>
        public const string ExtensionFilter = "*.doc|*.doc|*.docx|*.docx|*.rtf|*.rtf";

        /// <summary>
        /// Saves template to file, offers to open it or shows it in folder.
        /// </summary>
        /// <param name="template">Report template entity.</param>
        public static void SaveAndOpenTemplate(ReportTemplate template)
        {
            try
            {
                if (template != null && template.Data != null)
                {
                    var dialog = new SaveFileDialog
                                     {
                                         Title = @"Укажите файл для сохранения шаблона",
                                         Filter = ExtensionFilter,
                                         AddExtension = true,
                                         FileName = "Шаблон",
                                         DefaultExt = "doc"
                                     };

                    if (dialog.ShowDialog() == DialogResult.OK)
                    {
                        System.IO.File.WriteAllBytes( dialog.FileName, template.Data.ToArray( ) );

                        OfferToOpenFileOrShowIt(dialog.FileName);
                    }
                }
            }
            catch (Exception e)
            {
                Program.HandleUnhandledException(e);
            }
        }

        /// <summary>
        /// Replace template stored in database with new one.
        /// </summary>
        /// <param name="template">Report template entity.</param>
        public static void ReplaceTemplate(ReportTemplate template, Control control)
        {
            if (XtraMessageBox.Show(control,
                                    "Выбранный шаблон будет перезаписан указанным вами шаблоном. Вы уверены?",
                                    "Подтверждение", MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                                    MessageBoxDefaultButton.Button2) ==
                DialogResult.Yes)
            {
                var dialog = new ReportTemplateDetailsForm(template);

                if (dialog.ShowDialog(control) == DialogResult.OK)
                {
                    try
                    {
                        template.Name = dialog.TemplateName;
                        template.Data = System.IO.File.ReadAllBytes( dialog.TemplateFile );
                        template.LastModified = DateTime.Now;

                        Program.DbSingletone.SubmitChanges();

                        XtraMessageBox.Show(
                            "Шаблон заменён успешно.",
                            String.Empty, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (DbException e)
                    {
                        Program.HandleDbException(e);
                    }
                    catch (Exception e)
                    {
                        Program.HandleUnhandledException(e);
                    }
                }
            }
        }

        public static void DeleteTemplate(ReportTemplate template, Control control)
        {
            if (XtraMessageBox.Show(control,
                                    "Выбранный шаблон будет удалён без возможности восстановления. Вы уверены?",
                                    "Подтверждение", MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                                    MessageBoxDefaultButton.Button2) ==
                DialogResult.Yes)
            {
                try
                {
                    Program.DbSingletone.ReportTemplates.DeleteOnSubmit(template);
                    Program.DbSingletone.SubmitChanges();

                    XtraMessageBox.Show(
                        "Шаблон удалён.",
                        String.Empty, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (DbException e)
                {
                    Program.HandleDbException(e);
                }
            }
        }

        public static void AddTemplate(Report report, Control control)
        {
            var dialog = new ReportTemplateDetailsForm();

            if (dialog.ShowDialog(control) == DialogResult.OK)
            {
                try
                {
                    var template = new ReportTemplate
                                       {
                                           Report = report,
                                           Name = dialog.TemplateName,
                                           Data = System.IO.File.ReadAllBytes( dialog.TemplateFile ),
                                           LastModified = DateTime.Now
                                       };

                    Program.DbSingletone.ReportTemplates.InsertOnSubmit(template);
                    Program.DbSingletone.SubmitChanges();

                    XtraMessageBox.Show(
                        "Шаблон успешно добавлен.",
                        String.Empty, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (DbException e)
                {
                    Program.HandleDbException(e);
                }
            }
        }

        public static string GetTemplateDocument(string reportKey, Control control)
        {
            ReportTemplate reportTemplate = null;

            var form = new ReportTemplatesForm(reportKey);
            var allowReport = Program.PermissionManager.CheckAllow(form);
            if (!allowReport)
            {
                MessageBox.ShowValidationError("Недостаточно прав для просмотра формы.");
                return null;
            }
            if (form.ShowDialog(control) == DialogResult.OK)
            {
                reportTemplate = form.SelectedTemplate;
            }

            if (reportTemplate != null)
            {
                byte[] data = reportTemplate.Data != null ? reportTemplate.Data.ToArray() : null;

                if (data != null)
                {
                    // Save template to temporary file.
                    string reportPath = Path.GetTempFileName();
                    FileStream fs = System.IO.File.Create( reportPath );
                    fs.Write(data, 0, data.Length);
                    fs.Close();
                    return reportPath;
                }
            }

            return null;
        }

        public static Control GetReportTemplateDummyForm()
        {
            return new ReportTemplatesForm(null);
        }

        /// <summary>
        /// Saves report to file, offers to open it or shows it in folder.
        /// </summary>
        /// <param name="saveDelegate">Save report delegate to perform external saving operations.</param>
        public static void SaveAndOpenReport(SaveReportDelegate saveDelegate)
        {
            var dialog = new SaveFileDialog
                             {
                                 Title = @"Укажите файл для сохранения отчёта",
                                 Filter = ExtensionFilter,
                                 AddExtension = true,
                                 FileName = "Отчёт",
                                 DefaultExt = "doc"
                             };

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                if (saveDelegate != null)
                {
                    Exception exception;

                    if ((exception = saveDelegate(dialog.FileName)) == null)
                    {
                        OfferToOpenFileOrShowIt(dialog.FileName);
                    }
                    else
                    {
                        XtraMessageBox.Show(
                            "При сохранении отчёта возникла ошибка:" + Environment.NewLine +
                            exception.Message,
                            "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        /// <summary>
        /// Offers to open file or shows it in folder.
        /// </summary>
        /// <param name="fileName">Path to file.</param>
        private static void OfferToOpenFileOrShowIt(string fileName)
        {
            if (XtraMessageBox.Show("Открыть файл?" + Environment.NewLine + fileName,
                                    "Предложение", MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==
                DialogResult.Yes)
            {
                Process.Start(fileName);
            }
            //else
            //{
            //    Process.Start("explorer.exe", String.Format("/select,\"{0}\"", fileName));
            //}
        }

        public static void ShowReportDescription(string reportKey, Control control)
        {
            new ReportDescriptionForm(reportKey).ShowDialog(control);
        }
    }
}
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Eureca.Integrator.Common;
using File = System.IO.File;

namespace Curriculum.Reports
{
    /// <summary>
    /// Work direction control.
    /// </summary>
    public partial class WorkDirectionControl : ControlWithAccessAreas, IAliasesHolder
    {
        /// <summary>
        /// Report key.
        /// </summary>
        private const string ReportKey = "WorkDirection";

        /// <summary>
        /// Default text of previous person name text edit.
        /// </summary>
        private readonly string _previousPersonNameDefaultText;

        /// <summary>
        /// Initializes new instance of work direction control.
        /// </summary>
        public WorkDirectionControl()
        {
            InitializeComponent();

            _previousPersonNameDefaultText = _previousPersonNameTextEdit.Text;
        }

        /// <summary>
        /// Gets selected person.
        /// </summary>
        private Person Person
        {
            get { return _personGridLookUpEdit.EditValue as Person; }
        }

        /// <summary>
        /// Gets selected previous person.
        /// </summary>
        private Person PreviousPerson
        {
            get { return _previousPersonGridLookUpEdit.EditValue as Person; }
        }

        /// <summary>
        /// Gets previous person name.
        /// </summary>
        private string PreviousPersonName
        {
            get
            {
                Person person = PreviousPerson;

                if (person != null)
                {
                    return person.Name;
                }

                return _previousPersonNameTextEdit.Text;
            }
        }

        #region IAliasesHolder Members

        /// <summary>
        /// Applies aliases.
        /// </summary>
        public void ApplyAliases()
        {
            try
            {
                Aliase.ApplyAlias( "Ship", a => _personShipGridColumn.Caption = a );
                Aliase.ApplyAlias( "Position",
                                 a => _personPositionGridColumn.Caption = _previousPersonPositionGridColumn.Caption = a);
                Aliase.ApplyAlias( "Period", a => _personPeriodGridColumn.Caption = a );
                Aliase.ApplyAlias( "TripCount", a => _personTripCountGridColumn.Caption = a );
            }
            catch (DbException e)
            {
                Program.HandleDbException(e);
            }
        }

        #endregion

        /// <summary>
        /// Handles work direction control load event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void WorkDirectionControlLoad(object sender, EventArgs e)
        {
            _personGridLookUpEdit.Properties.DataSource = from person in Program.DbSingletone.Persons
                                                          orderby person.DepartureDate descending
                                                          select person;

            _personGridLookUpEdit.EditValue = null;
        }

        /// <summary>
        /// Handles previous person name text edit properties enter event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void PreviousPersonNameTextEditPropertiesEnter(object sender, EventArgs e)
        {
            if (_previousPersonNameTextEdit.Text == _previousPersonNameDefaultText)
            {
                _previousPersonNameTextEdit.Text = String.Empty;
            }
        }

        /// <summary>
        /// Handles previous person name text edit properties leave event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void PreviousPersonNameTextEditPropertiesLeave(object sender, EventArgs e)
        {
            if (_previousPersonNameTextEdit.Text.Trim().Length == 0)
            {
                _previousPersonNameTextEdit.Text = _previousPersonNameDefaultText;
            }
        }

        /// <summary>
        /// Handles previous person name text edit value changed event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void PreviousPersonNameTextEditEditValueChanged(object sender, EventArgs e)
        {
            if (_previousPersonNameTextEdit.Text != _previousPersonNameDefaultText)
            {
                _previousPersonGridLookUpEdit.EditValue = null;

                _reportSimpleButton.Enabled = Person != null;
            }
            else
            {
                _reportSimpleButton.Enabled = Person != null && PreviousPerson != null;
            }
        }

        /// <summary>
        /// Handles previous person grid look up edit value changed event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void PreviousPersonGridLookUpEditEditValueChanged(object sender, EventArgs e)
        {
            if (_previousPersonGridLookUpEdit.EditValue != null)
            {
                _previousPersonNameTextEdit.Text = _previousPersonNameDefaultText;

                _reportSimpleButton.Enabled = Person != null;
            }
            else
            {
                _reportSimpleButton.Enabled = Person != null && PreviousPersonName != _previousPersonNameDefaultText;
            }
        }

        /// <summary>
        /// Handles person grid look up edit value changed event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void PersonGridLookUpEditEditValueChanged(object sender, EventArgs e)
        {
            var person = _personGridLookUpEdit.EditValue as Person;

            if (person != null)
            {
                _previousPersonGridLookUpEdit.Properties.DataSource = from p in Program.DbSingletone.Persons
                                                                      where
                                                                          p.ShipId == person.ShipId &&
                                                                          p.PersonId != person.PersonId &&
                                                                          (p.DepartureDate < DateTime.Today ||
                                                                           p.DepartureDate == null)
                                                                      orderby p.Position.Name
                                                                      select p;
                _previousPersonGridLookUpEdit.EditValue = null;

                _previousPersonGridLookUpEdit.Enabled =
                    _previousPersonNameTextEdit.Enabled = true;
            }
            else
            {
                _previousPersonGridLookUpEditView.GridControl.DataSource = null;

                _previousPersonGridLookUpEdit.Enabled =
                    _previousPersonNameTextEdit.Enabled = false;

                _previousPersonGridLookUpEdit.EditValue = null;
                _previousPersonNameTextEdit.Text = _previousPersonNameDefaultText;
            }
        }

        /// <summary>
        /// Handles report drop down button click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void ReportSimpleReportButtonClick(object sender, EventArgs e)
        {
            Person person = Person;

            try
            {
                if (person != null)
                {
                    Person previousPerson = PreviousPerson;
                    string previousPersonName = previousPerson == null ? PreviousPersonName : previousPerson.Name;

                    string reportPath = Reporter.GetTemplateDocument(ReportKey, this);
                    if (!string.IsNullOrEmpty(reportPath))
                    {
                        //�������� ������� ���������� �����
                        Dictionary<string, string> dict = WordManager.GetWorkDirectionFields(person, previousPersonName,
                                                                                             WordManager.GetCommonFields
                                                                                                 (person));
                        var wordManager = new WordManager();
                        try
                        {
                            wordManager.Open(reportPath);
                            //�������� 
                            wordManager.ReplaceTemplateCommonFields(dict);
                            wordManager.SaveAs(reportPath);
                        }
                        finally
                        {
                            wordManager.KillWinWord();
                        }


                        Application.DoEvents();
                        // ��������� � ��������� �����....
                        Reporter.SaveAndOpenReport(fileName =>
                                                       {
                                                           try
                                                           {
                                                               if (System.IO.File.Exists( fileName ))
                                                                   System.IO.File.Delete( fileName );
                                                               System.IO.File.Move( reportPath, fileName );
                                                               return null;
                                                           }
                                                           catch (Exception ex)
                                                           {
                                                               return ex;
                                                           }
                                                       });
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "����������");
            }
        }

        public override IEnumerable<ISKOCBCTDNBTIDED> GetHiddenChilds( bool withLazy )
        {
            var hidden = new List<ISKOCBCTDNBTIDED>( );

            if (withLazy)
            {
                var reportTemplateDummyForm = Reporter.GetReportTemplateDummyForm( );
                var supportAccessAreasForm = reportTemplateDummyForm as ISupportAccessAreas;
                supportAccessAreasForm.HierarchyParent = Program.StartForm;
                hidden.Add( new ControlInterfaceImplementation( reportTemplateDummyForm ) );
            }

            return hidden;
        }
    }
}
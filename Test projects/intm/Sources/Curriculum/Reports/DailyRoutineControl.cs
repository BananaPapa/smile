using System;
using System.Linq;
using DevExpress.XtraEditors;
using Eureca.Integrator.Common;

namespace Curriculum.Reports
{
    public partial class DailyRoutineControl : ControlWithAccessAreas
    {
        public DailyRoutineControl()
        {
            InitializeComponent();
            dateEdit1.DateTime = DateTime.Now;
        }

        private void dateEdit1_EditValueChanged(object sender, EventArgs e)
        {
            gridControl2.DataSource = from lesson in Program.DbSingletone.Lessons
                                      where lesson.Datetime.Date == dateEdit1.DateTime.Date
                                      select lesson;
        }

        private void _editPersonSimpleButton_Click(object sender, EventArgs e)
        {
            gridControl2.ShowPrintPreview();
        }
    }
}
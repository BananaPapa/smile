﻿namespace Curriculum.Reports
{
    partial class ReportTemplateDetailsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._nameLabelControl = new DevExpress.XtraEditors.LabelControl();
            this._nameMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this._fileLabelControl = new DevExpress.XtraEditors.LabelControl();
            this._fileButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this._okSimpleButton = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this._nameMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._fileButtonEdit.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // _nameLabelControl
            // 
            this._nameLabelControl.Location = new System.Drawing.Point(12, 12);
            this._nameLabelControl.Name = "_nameLabelControl";
            this._nameLabelControl.Size = new System.Drawing.Size(53, 13);
            this._nameLabelControl.TabIndex = 0;
            this._nameLabelControl.Text = "Описание:";
            // 
            // _nameMemoEdit
            // 
            this._nameMemoEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._nameMemoEdit.Location = new System.Drawing.Point(12, 31);
            this._nameMemoEdit.Name = "_nameMemoEdit";
            this._nameMemoEdit.Size = new System.Drawing.Size(320, 87);
            this._nameMemoEdit.TabIndex = 1;
            this._nameMemoEdit.UseOptimizedRendering = true;
            this._nameMemoEdit.EditValueChanged += new System.EventHandler(this.EditorEditValueChanged);
            // 
            // _fileLabelControl
            // 
            this._fileLabelControl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._fileLabelControl.Location = new System.Drawing.Point(12, 124);
            this._fileLabelControl.Name = "_fileLabelControl";
            this._fileLabelControl.Size = new System.Drawing.Size(134, 13);
            this._fileLabelControl.TabIndex = 2;
            this._fileLabelControl.Text = "Путь к файлу с шаблоном:";
            // 
            // _fileButtonEdit
            // 
            this._fileButtonEdit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._fileButtonEdit.Location = new System.Drawing.Point(12, 143);
            this._fileButtonEdit.Name = "_fileButtonEdit";
            this._fileButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this._fileButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this._fileButtonEdit.Size = new System.Drawing.Size(319, 20);
            this._fileButtonEdit.TabIndex = 3;
            this._fileButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.FileButtonEditButtonClick);
            this._fileButtonEdit.EditValueChanged += new System.EventHandler(this.EditorEditValueChanged);
            // 
            // _okSimpleButton
            // 
            this._okSimpleButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._okSimpleButton.Enabled = false;
            this._okSimpleButton.Location = new System.Drawing.Point(263, 176);
            this._okSimpleButton.Name = "_okSimpleButton";
            this._okSimpleButton.Size = new System.Drawing.Size(69, 23);
            this._okSimpleButton.TabIndex = 4;
            this._okSimpleButton.Text = "OK";
            this._okSimpleButton.Click += new System.EventHandler(this.OkSimpleButtonClick);
            // 
            // ReportTemplateDetailsForm
            // 
            this.AcceptButton = this._okSimpleButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(344, 211);
            this.Controls.Add(this._okSimpleButton);
            this.Controls.Add(this._fileButtonEdit);
            this.Controls.Add(this._fileLabelControl);
            this.Controls.Add(this._nameMemoEdit);
            this.Controls.Add(this._nameLabelControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ReportTemplateDetailsForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Параметры шаблона";
            this.Load += new System.EventHandler(this.ReportTemplateDetailsFormLoad);
            ((System.ComponentModel.ISupportInitialize)(this._nameMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._fileButtonEdit.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl _nameLabelControl;
        private DevExpress.XtraEditors.MemoEdit _nameMemoEdit;
        private DevExpress.XtraEditors.LabelControl _fileLabelControl;
        private DevExpress.XtraEditors.ButtonEdit _fileButtonEdit;
        private DevExpress.XtraEditors.SimpleButton _okSimpleButton;
    }
}
namespace Curriculum.Reports
{
    partial class WorkDirectionControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._panelControl = new DevExpress.XtraEditors.PanelControl();
            this._reportSimpleButton = new DevExpress.XtraEditors.SimpleButton();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this._layoutControl = new DevExpress.XtraLayout.LayoutControl();
            this._previousPersonNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this._previousPersonGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this._previousPersonGridLookUpEditView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._personGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this._personGridLookUpEditView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this._personLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this._previousPersonLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this._previousPersonNameLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this._previousPersonNameGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._previousPersonPositionGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._personNameGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._personShipGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._personPositionGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._personDepartureDateGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._personPeriodGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._personTripCountGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this._panelControl)).BeginInit();
            this._panelControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._layoutControl)).BeginInit();
            this._layoutControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._previousPersonNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._previousPersonGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._previousPersonGridLookUpEditView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._personGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._personGridLookUpEditView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._personLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._previousPersonLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._previousPersonNameLayoutControlItem)).BeginInit();
            this.SuspendLayout();
            // 
            // _panelControl
            // 
            this._panelControl.Controls.Add(this._reportSimpleButton);
            this._panelControl.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._panelControl.Location = new System.Drawing.Point(0, 589);
            this._panelControl.Name = "_panelControl";
            this._panelControl.Size = new System.Drawing.Size(754, 30);
            this._panelControl.TabIndex = 1;
            // 
            // _reportSimpleButton
            // 
            this._reportSimpleButton.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this._reportSimpleButton.Enabled = false;
            this._reportSimpleButton.Image = global::Curriculum.Properties.Resources.MSWordDocument;
            this._reportSimpleButton.Location = new System.Drawing.Point(676, 2);
            this._reportSimpleButton.Name = "_reportSimpleButton";
            this._reportSimpleButton.Size = new System.Drawing.Size(76, 26);
            this._reportSimpleButton.TabIndex = 1;
            this._reportSimpleButton.Tag = "..\\����������� �� ������#r";
            this._reportSimpleButton.Text = "�����";
            this._reportSimpleButton.Click += new System.EventHandler(this.ReportSimpleReportButtonClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(0, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 0);
            this.barDockControlBottom.Size = new System.Drawing.Size(0, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 0);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Location = new System.Drawing.Point(0, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 0);
            // 
            // _layoutControl
            // 
            this._layoutControl.Controls.Add(this._previousPersonNameTextEdit);
            this._layoutControl.Controls.Add(this._previousPersonGridLookUpEdit);
            this._layoutControl.Controls.Add(this._personGridLookUpEdit);
            this._layoutControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._layoutControl.Location = new System.Drawing.Point(0, 0);
            this._layoutControl.Name = "_layoutControl";
            this._layoutControl.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignMode.AutoSize;
            this._layoutControl.Root = this.layoutControlGroup1;
            this._layoutControl.Size = new System.Drawing.Size(754, 589);
            this._layoutControl.TabIndex = 0;
            this._layoutControl.Text = "layoutControl1";
            // 
            // _previousPersonNameTextEdit
            // 
            this._previousPersonNameTextEdit.EditValue = "������� ���";
            this._previousPersonNameTextEdit.Enabled = false;
            this._previousPersonNameTextEdit.Location = new System.Drawing.Point(460, 36);
            this._previousPersonNameTextEdit.Name = "_previousPersonNameTextEdit";
            this._previousPersonNameTextEdit.Properties.Enter += new System.EventHandler(this.PreviousPersonNameTextEditPropertiesEnter);
            this._previousPersonNameTextEdit.Properties.Leave += new System.EventHandler(this.PreviousPersonNameTextEditPropertiesLeave);
            this._previousPersonNameTextEdit.Size = new System.Drawing.Size(282, 20);
            this._previousPersonNameTextEdit.StyleController = this._layoutControl;
            this._previousPersonNameTextEdit.TabIndex = 6;
            this._previousPersonNameTextEdit.EditValueChanged += new System.EventHandler(this.PreviousPersonNameTextEditEditValueChanged);
            // 
            // _previousPersonGridLookUpEdit
            // 
            this._previousPersonGridLookUpEdit.EditValue = "�������� �����������";
            this._previousPersonGridLookUpEdit.Enabled = false;
            this._previousPersonGridLookUpEdit.Location = new System.Drawing.Point(138, 36);
            this._previousPersonGridLookUpEdit.Name = "_previousPersonGridLookUpEdit";
            this._previousPersonGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._previousPersonGridLookUpEdit.Properties.NullText = "�������� �����������";
            this._previousPersonGridLookUpEdit.Properties.PopupFormMinSize = new System.Drawing.Size(500, 400);
            this._previousPersonGridLookUpEdit.Properties.View = this._previousPersonGridLookUpEditView;
            this._previousPersonGridLookUpEdit.Size = new System.Drawing.Size(297, 20);
            this._previousPersonGridLookUpEdit.StyleController = this._layoutControl;
            this._previousPersonGridLookUpEdit.TabIndex = 5;
            this._previousPersonGridLookUpEdit.EditValueChanged += new System.EventHandler(this.PreviousPersonGridLookUpEditEditValueChanged);
            // 
            // _previousPersonGridLookUpEditView
            // 
            this._previousPersonGridLookUpEditView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._previousPersonNameGridColumn,
            this._previousPersonPositionGridColumn});
            this._previousPersonGridLookUpEditView.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this._previousPersonGridLookUpEditView.Name = "_previousPersonGridLookUpEditView";
            this._previousPersonGridLookUpEditView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this._previousPersonGridLookUpEditView.OptionsView.ShowGroupPanel = false;
            // 
            // _personGridLookUpEdit
            // 
            this._personGridLookUpEdit.EditValue = "";
            this._personGridLookUpEdit.Location = new System.Drawing.Point(73, 12);
            this._personGridLookUpEdit.Name = "_personGridLookUpEdit";
            this._personGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._personGridLookUpEdit.Properties.NullText = "�������� ����������";
            this._personGridLookUpEdit.Properties.PopupFormMinSize = new System.Drawing.Size(800, 400);
            this._personGridLookUpEdit.Properties.View = this._personGridLookUpEditView;
            this._personGridLookUpEdit.Size = new System.Drawing.Size(669, 20);
            this._personGridLookUpEdit.StyleController = this._layoutControl;
            this._personGridLookUpEdit.TabIndex = 4;
            this._personGridLookUpEdit.EditValueChanged += new System.EventHandler(this.PersonGridLookUpEditEditValueChanged);
            // 
            // _personGridLookUpEditView
            // 
            this._personGridLookUpEditView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._personNameGridColumn,
            this._personShipGridColumn,
            this._personPositionGridColumn,
            this._personDepartureDateGridColumn,
            this._personPeriodGridColumn,
            this._personTripCountGridColumn});
            this._personGridLookUpEditView.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this._personGridLookUpEditView.Name = "_personGridLookUpEditView";
            this._personGridLookUpEditView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this._personGridLookUpEditView.OptionsView.ShowGroupPanel = false;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this._personLayoutControlItem,
            this._previousPersonLayoutControlItem,
            this._previousPersonNameLayoutControlItem});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(754, 589);
            this.layoutControlGroup1.Tag = "..\\����������� �� ������#r";
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // _personLayoutControlItem
            // 
            this._personLayoutControlItem.Control = this._personGridLookUpEdit;
            this._personLayoutControlItem.CustomizationFormText = "���������";
            this._personLayoutControlItem.Location = new System.Drawing.Point(0, 0);
            this._personLayoutControlItem.Name = "_personLayoutControlItem";
            this._personLayoutControlItem.Size = new System.Drawing.Size(734, 24);
            this._personLayoutControlItem.Text = "���������";
            this._personLayoutControlItem.TextSize = new System.Drawing.Size(58, 13);
            // 
            // _previousPersonLayoutControlItem
            // 
            this._previousPersonLayoutControlItem.Control = this._previousPersonGridLookUpEdit;
            this._previousPersonLayoutControlItem.CustomizationFormText = "���������� ����������";
            this._previousPersonLayoutControlItem.Location = new System.Drawing.Point(0, 24);
            this._previousPersonLayoutControlItem.Name = "_previousPersonLayoutControlItem";
            this._previousPersonLayoutControlItem.Size = new System.Drawing.Size(427, 545);
            this._previousPersonLayoutControlItem.Text = "���������� ����������";
            this._previousPersonLayoutControlItem.TextSize = new System.Drawing.Size(123, 13);
            // 
            // _previousPersonNameLayoutControlItem
            // 
            this._previousPersonNameLayoutControlItem.AppearanceItemCaption.Options.UseTextOptions = true;
            this._previousPersonNameLayoutControlItem.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._previousPersonNameLayoutControlItem.Control = this._previousPersonNameTextEdit;
            this._previousPersonNameLayoutControlItem.CustomizationFormText = "���";
            this._previousPersonNameLayoutControlItem.Location = new System.Drawing.Point(427, 24);
            this._previousPersonNameLayoutControlItem.Name = "_previousPersonNameLayoutControlItem";
            this._previousPersonNameLayoutControlItem.Size = new System.Drawing.Size(307, 545);
            this._previousPersonNameLayoutControlItem.Text = "���";
            this._previousPersonNameLayoutControlItem.TextSize = new System.Drawing.Size(18, 13);
            // 
            // _previousPersonNameGridColumn
            // 
            this._previousPersonNameGridColumn.Caption = "���";
            this._previousPersonNameGridColumn.FieldName = "Name";
            this._previousPersonNameGridColumn.Name = "_previousPersonNameGridColumn";
            this._previousPersonNameGridColumn.Visible = true;
            this._previousPersonNameGridColumn.VisibleIndex = 0;
            // 
            // _previousPersonPositionGridColumn
            // 
            this._previousPersonPositionGridColumn.Caption = "���������";
            this._previousPersonPositionGridColumn.FieldName = "Position";
            this._previousPersonPositionGridColumn.Name = "_previousPersonPositionGridColumn";
            this._previousPersonPositionGridColumn.Visible = true;
            this._previousPersonPositionGridColumn.VisibleIndex = 1;
            // 
            // _personNameGridColumn
            // 
            this._personNameGridColumn.Caption = "���";
            this._personNameGridColumn.FieldName = "Name";
            this._personNameGridColumn.Name = "_personNameGridColumn";
            this._personNameGridColumn.Visible = true;
            this._personNameGridColumn.VisibleIndex = 0;
            this._personNameGridColumn.Width = 223;
            // 
            // _personShipGridColumn
            // 
            this._personShipGridColumn.Caption = "�������";
            this._personShipGridColumn.FieldName = "Ship";
            this._personShipGridColumn.Name = "_personShipGridColumn";
            this._personShipGridColumn.Visible = true;
            this._personShipGridColumn.VisibleIndex = 1;
            this._personShipGridColumn.Width = 172;
            // 
            // _personPositionGridColumn
            // 
            this._personPositionGridColumn.Caption = "���������";
            this._personPositionGridColumn.FieldName = "Position";
            this._personPositionGridColumn.Name = "_personPositionGridColumn";
            this._personPositionGridColumn.Visible = true;
            this._personPositionGridColumn.VisibleIndex = 3;
            this._personPositionGridColumn.Width = 172;
            // 
            // _personDepartureDateGridColumn
            // 
            this._personDepartureDateGridColumn.Caption = "���� ������";
            this._personDepartureDateGridColumn.FieldName = "DepartureDate";
            this._personDepartureDateGridColumn.Name = "_personDepartureDateGridColumn";
            this._personDepartureDateGridColumn.Visible = true;
            this._personDepartureDateGridColumn.VisibleIndex = 2;
            this._personDepartureDateGridColumn.Width = 172;
            // 
            // _personPeriodGridColumn
            // 
            this._personPeriodGridColumn.Caption = "����";
            this._personPeriodGridColumn.FieldName = "Period";
            this._personPeriodGridColumn.Name = "_personPeriodGridColumn";
            this._personPeriodGridColumn.Visible = true;
            this._personPeriodGridColumn.VisibleIndex = 4;
            this._personPeriodGridColumn.Width = 172;
            // 
            // _personTripCountGridColumn
            // 
            this._personTripCountGridColumn.Caption = "���������� �����";
            this._personTripCountGridColumn.FieldName = "TripCount";
            this._personTripCountGridColumn.Name = "_personTripCountGridColumn";
            this._personTripCountGridColumn.Visible = true;
            this._personTripCountGridColumn.VisibleIndex = 5;
            this._personTripCountGridColumn.Width = 177;
            // 
            // WorkDirectionControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._layoutControl);
            this.Controls.Add(this._panelControl);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "WorkDirectionControl";
            this.Size = new System.Drawing.Size(754, 619);
            this.Tag = "����������� �� ������#r";
            this.Load += new System.EventHandler(this.WorkDirectionControlLoad);
            ((System.ComponentModel.ISupportInitialize)(this._panelControl)).EndInit();
            this._panelControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._layoutControl)).EndInit();
            this._layoutControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._previousPersonNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._previousPersonGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._previousPersonGridLookUpEditView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._personGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._personGridLookUpEditView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._personLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._previousPersonLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._previousPersonNameLayoutControlItem)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl _panelControl;
        private DevExpress.XtraLayout.LayoutControl _layoutControl;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.GridLookUpEdit _previousPersonGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView _previousPersonGridLookUpEditView;
        private DevExpress.XtraEditors.GridLookUpEdit _personGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView _personGridLookUpEditView;
        private DevExpress.XtraLayout.LayoutControlItem _personLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem _previousPersonLayoutControlItem;
        private DevExpress.XtraEditors.TextEdit _previousPersonNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem _previousPersonNameLayoutControlItem;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraGrid.Columns.GridColumn _personNameGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _personShipGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _personPositionGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _personDepartureDateGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _personPeriodGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _personTripCountGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _previousPersonNameGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _previousPersonPositionGridColumn;
        private DevExpress.XtraEditors.SimpleButton _reportSimpleButton;
    }
}

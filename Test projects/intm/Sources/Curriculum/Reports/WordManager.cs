using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Microsoft.Office.Interop.Word;
using Application = Microsoft.Office.Interop.Word.Application;

namespace Curriculum.Reports
{
    /// <summary>
    /// ����� ��� ������ � word ����������
    /// </summary>
    internal class WordManager
    {
        #region ����������

        //������������ � ������ ��������
        private static object _objFalse = false;
        private static object _wdFormat = WdOriginalFormat.wdOriginalDocumentFormat;
        private static object _wdNotSave = WdSaveOptions.wdDoNotSaveChanges;
        private readonly List<CustomTag> _tags = new List<CustomTag>();
        private Application _wa;
        private Microsoft.Office.Interop.Word.Document _wordDoc;

        #endregion

        #region Constructor

        #endregion

        #region Work with document

        /// <summary>
        /// �������� ������������� ���������
        /// </summary>
        /// <param name="filePath"></param>
        public void Open(string filePath)
        {
            //������� �������� ����
            _wa = new Application();
            _wa.Visible = false;
            object fileName = filePath;
            object optional = Missing.Value;
            object visible = true;
            object readOnly = false;


            _wordDoc = _wa.Documents.Open(ref fileName, ref optional,
                                          ref readOnly, ref optional,
                                          ref optional, ref optional,
                                          ref optional, ref optional,
                                          ref optional, ref optional,
                                          ref optional, ref visible,
                                          ref optional, ref optional,
                                          ref optional, ref optional);


            //��������� ���� ����� 
            List<string> allTags = GetAllTagItems();
            foreach (string tag in allTags)
            {
                _tags.Add(new CustomTag(tag));
            }
        }

        ///// <summary>
        ///// �������� ������ ���������
        ///// </summary>
        //private static void New()
        //{
        //    //������� �������� ����
        //    wa = new Application();
        //    object visible = true;
        //    object missing = Missing.Value;

        //    wordDoc = wa.Documents.Add(ref missing, ref missing, ref missing, ref visible);
        //}

        ///// <summary>
        ///// �������� ��������� �� ������������ �������
        ///// </summary>
        ///// <param name="questinoCount"></param>
        ///// <param name="stenCount"></param>
        //private void CreateTemplateDocument(int questinoCount, int stenCount)
        //{
        //object oMissing = Missing.Value;
        //Object defaultTableBehavior =
        //    WdDefaultTableBehavior.wdWord9TableBehavior;
        //Object autoFitBehavior =
        //    WdAutoFitBehavior.wdAutoFitWindow;
        //object unit;
        //object extend;
        //unit = WdUnits.wdStory;
        //extend = WdMovementType.wdMove;


        //wordDoc.Select();
        //wa.Selection.Range.Text = "����� �������� � �����";

        //wa.Selection.EndKey(ref unit, ref extend);

        //wordDoc.Tables.Add(wa.Selection.Range, 12, 2, ref defaultTableBehavior, ref autoFitBehavior);

        ////���������� ���� � ����� ���������
        //wordDoc.Select();
        //wa.Selection.EndKey(ref unit, ref extend);
        //wordDoc.Paragraphs.Add(ref oMissing);

        //wordDoc.Select();
        //wa.Selection.EndKey(ref unit, ref extend);
        //wordDoc.Paragraphs.Add(ref oMissing);

        //wa.Selection.Range.Text = "������� ������ �����";
        ////wordDoc.Select();
        ////wordDoc.Paragraphs.Add(ref oMissing);
        //wordDoc.Select();
        //wa.Selection.EndKey(ref unit, ref extend);
        //wordDoc.Paragraphs.Add(ref oMissing);
        ////��������� ������� �� ����� �������
        //wordDoc.Tables.Add(wa.Selection.Range, stenCount + 1, 5, ref defaultTableBehavior,
        //                   ref autoFitBehavior);

        //wordDoc.Select();
        //wa.Selection.EndKey(ref unit, ref extend);
        //wordDoc.Paragraphs.Add(ref oMissing);

        //for (int i = 0; i < questinoCount; i++)
        //{
        //    Log.Error("�������� ������� ��� ������� " + (i + 1));
        //    wa.Selection.Range.Text = "������ � " + (i + 1);
        //    wordDoc.Select();
        //    wa.Selection.EndKey(ref unit, ref extend);

        //    wordDoc.Paragraphs.Add(ref oMissing);
        //    //��������� ������� �� ����� �������
        //    wordDoc.Tables.Add(wa.Selection.Range, 2, 2, ref defaultTableBehavior,
        //                       ref autoFitBehavior);

        //    wordDoc.Select();
        //    wa.Selection.EndKey(ref unit, ref extend);
        //    wordDoc.Paragraphs.Add(ref oMissing);
        //}
        //}

        /// <summary>
        /// ����� ���������� �����
        /// </summary>
        /// <param name="filePath"></param>
        public void SaveAs(object filePath)
        {
            object missing = Missing.Value;
            if (_wordDoc == null)
                throw new Exception("wordDoc = null");
            _wordDoc.SaveAs(ref filePath,
                            ref missing, ref missing, ref missing, ref missing, ref missing,
                            ref missing, ref missing, ref missing, ref missing, ref missing,
                            ref missing, ref missing, ref missing, ref missing, ref missing);
        }

        /// <summary>
        /// �������� ��������
        /// </summary>
        public void KillWinWord()
        {
            if (_wa != null)
                (_wa).Quit(ref _wdNotSave, ref _wdFormat, ref _objFalse); //����� ������� WINWORD.EXE
            _wordDoc = null;
            _wa = null;
        }


        /// <summary>
        /// ������ ����� �� �������
        /// </summary>
        /// <param name="replacedFields"></param>
        private void ReplaceFields(Dictionary<string, string> replacedFields)
        {
            // ������� ��������� � ������ ��������� 
            Object unit = WdUnits.wdStory;
            Object extend = WdMovementType.wdMove;
            _wordDoc.Select();
            foreach (string key in replacedFields.Keys)
            {
                _wa.Selection.HomeKey(ref unit, ref extend);

                Find fnd = _wa.Selection.Find;

                fnd.Text = key;
                fnd.Replacement.Text = replacedFields[key];

                ExecuteReplace(fnd);
            }
        }

        /// <summary>
        /// ������ �� ���� ���������
        /// </summary>
        /// <param name="find"></param>
        /// <returns></returns>
        private static void ExecuteReplace(Find find)
        {
            ExecuteReplace(find, WdReplace.wdReplaceAll);
        }

        /// <summary>
        /// ������� �������� Find.Execute: 
        /// </summary>
        /// <param name="find"></param>
        /// <param name="replaceOption"></param>
        /// <returns></returns>
        private static void ExecuteReplace(Find find, object replaceOption)
        {
            Object findText = Type.Missing;
            Object matchCase = Type.Missing;
            Object matchWholeWord = Type.Missing;
            Object matchWildcards = Type.Missing;
            Object matchSoundsLike = Type.Missing;
            Object matchAllWordForms = Type.Missing;
            Object forward = Type.Missing;
            Object wrap = Type.Missing;
            Object format = Type.Missing;
            Object replaceWith = Type.Missing;
            Object replace = replaceOption;
            Object matchKashida = Type.Missing;
            Object matchDiacritics = Type.Missing;
            Object matchAlefHamza = Type.Missing;
            Object matchControl = Type.Missing;

            find.Execute(ref findText, ref matchCase,
                         ref matchWholeWord, ref matchWildcards, ref matchSoundsLike,
                         ref matchAllWordForms, ref forward, ref wrap, ref format,
                         ref replaceWith, ref replace, ref matchKashida,
                         ref matchDiacritics, ref matchAlefHamza, ref matchControl);
        }

        private static string GetValueOrPlaceHolder(string value, string placeHolder)
        {
            return !String.IsNullOrEmpty(value) ? value : placeHolder;
        }


        /// <summary>
        /// go to end document
        /// </summary>
        public void GoToEnd()
        {
            object unit = WdUnits.wdStory;
            object extend = WdMovementType.wdMove;
            _wa.Selection.EndKey(ref unit, ref extend);
        }

        /// <summary>
        /// go to begin document
        /// </summary>
        public void GoToBegin()
        {
            object unit = WdUnits.wdStory;
            object extend = WdMovementType.wdMove;
            _wa.Selection.HomeKey(ref unit, ref extend);
        }

        /// <summary>
        /// copy selected 
        /// </summary>
        public void Copy()
        {
            _wa.Selection.Copy();
        }

        /// <summary>
        /// select all into document
        /// </summary>
        public void SelectAll()
        {
            // Ctrl-A
            _wa.Selection.WholeStory();
        }

        /// <summary>
        /// ����������� ����� ���������
        /// </summary>
        public void CopyAll()
        {
            SelectAll();
            Copy();
        }

        /// <summary>
        /// paste into document
        /// </summary>
        public void Paste()
        {
            int count = 0;
            while (count < 5)
            {
                try
                {
                    _wa.Selection.Paste();
                    return;
                }
                catch (Exception exception)
                {
                    count++;
                    if (count == 5)
                    {
                        _wa.Selection.Range.Text = "";
                    }
                }
            }
        }

        /// <summary>
        /// Get all tag item in document
        /// </summary>
        /// <returns></returns>
        public List<string> GetAllTagItems()
        {
            var res = new List<string>();
            var sb = new StringBuilder();
            bool write = false;

            string text = GetAllText();
            foreach (char c in text)
            {
                if (c == '[')
                {
                    write = true;
                    sb.Remove(0, sb.Length);
                }
                if (write)
                {
                    sb.Append(c);
                }
                if (c == ']')
                {
                    write = false;
                }
                if (!write && sb.Length > 0)
                {
                    try
                    {
                        res.Add(GetItemNameFromTag(sb.ToString()));
                    }
                    catch (Exception ex)
                    {
                    }
                    sb.Remove(0, sb.Length);
                }
            }


            return res;
        }

        /// <summary>
        /// ��������� ����� ������ � ���������
        /// </summary>
        /// <returns></returns>
        public string GetAllText()
        {
            SelectAll();
            return _wa.Selection.Range.Text;
        }

        /// <summary>
        /// Get name from tag
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static string GetItemNameFromTag(object item)
        {
            string itemStr = item.ToString();
            string itemStr1 = itemStr.Remove(0, 1);
            return itemStr1.Remove(itemStr1.Length - 1, 1);
        }

        /// <summary>
        /// ����������� �������
        /// </summary>
        /// <param name="table"></param>
        private void CopyTable(Table table)
        {
            table.Select();
            Thread.Sleep(100);
            Copy();
            Thread.Sleep(100);
        }

        public void CopyTable(int tableIndex)
        {
            CopyTable(_wordDoc.Tables[tableIndex]);
        }

        #endregion

        #region Custom methods for fill document

        /// <summary>
        /// ���������� ������� � ������ ����������
        /// </summary>
        /// <param name="person"></param>
        /// <returns></returns>
        public static Dictionary<string, string> GetCommonFields(Person person)
        {
            var res = new Dictionary<string, string>
                          {
                              {"[����.���]", person.FirstName},
                              {"[����.��������]", person.MiddleName},
                              {"[����.�������]", person.LastName}
                          };

            //���������� �� ����������
            if (person.Degree != null)
                res.Add("[����.������]", person.Degree.Name);
            if (person.Ship != null)
            {
                res.Add("[����.�����]", person.Ship.Name);

                if (person.Ship.River != null)
                {
                    res.Add("[����.�����.����]", person.Ship.River.Name);
                    res.Add("[����.�����.������]", person.Ship.River.Name);
                }

                if (person.Ship.ShipLine != null)
                {
                    res.Add("[����.�����.�����������]", person.Ship.ShipLine.Name);
                }
                if (person.Ship.Condition != null)
                {
                    res.Add("[����.�����.�������]", person.Ship.Condition.Name);
                }
            }
            if (person.Position != null)
                res.Add("[����.���������]", person.Position.Name);
            res.Add("[����.����.�����]", person.ChildNickName ?? string.Empty);

            //�����(�������) ����������
            if (person.Ship != null)


                //���� ������ ��������
                if (person.TeachingStartDate.HasValue)
                {
                    res.Add("[����.��.�����]", person.TeachingStartDate.Value.ToString("dd"));
                    res.Add("[����.��.�����.�]", person.TeachingStartDate.Value.ToString("MM"));
                    res.Add("[����.��.�����.�]",
                            NameOwnerTools.GetNamePart(person.TeachingStartDate.Value.ToString("D"), 1));
                }
            //���� ��������
            if (person.ExaminationDate.HasValue)
            {
                res.Add("[����.��.�����]", person.ExaminationDate.Value.ToString("dd"));
                res.Add("[����.��.�����.�]", person.ExaminationDate.Value.ToString("MM"));
                res.Add("[����.��.�����.�]", NameOwnerTools.GetNamePart(person.ExaminationDate.Value.ToString("D"), 1));
                res.Add("[����.��.���]", person.ExaminationDate.Value.Year.ToString());
            }

            //���� ������
            if (person.DepartureDate.HasValue)
            {
                res.Add("[����.��.�����]", person.DepartureDate.Value.ToString("dd"));
                res.Add("[����.��.�����.�]", person.DepartureDate.Value.ToString("MM"));
                res.Add("[����.��.�����.�]", NameOwnerTools.GetNamePart(person.DepartureDate.Value.ToString("D"), 1));
                res.Add("[����.��.���]", person.DepartureDate.Value.Year.ToString());
            }

            //��������
            res.Add("[����.�]", person.FirstName.Length > 0 ? person.FirstName[0].ToString() : "");
            res.Add("[����.�]", person.MiddleName.Length > 0 ? person.MiddleName[0].ToString() : "");


            //���� �������� ����������
            res.Add("[��.�����]", person.BirthDate.ToString("dd"));
            res.Add("[��.�����.�]", person.BirthDate.ToString("MM"));
            res.Add("[��.�����.�]", NameOwnerTools.GetNamePart(person.BirthDate.ToString("D"), 1));
            res.Add("[��.���]", person.BirthDate.Year.ToString());


            //������� ����
            res.Add("[��.�����]", DateTime.Now.ToString("dd"));
            res.Add("[��.�����.�]", DateTime.Now.ToString("MM"));
            res.Add("[��.�����.�]", NameOwnerTools.GetNamePart(DateTime.Now.ToString("D"), 1));
            res.Add("[��.���]", DateTime.Now.Year.ToString());
            return res;
        }

        /// <summary>
        /// ��������� ��� �� �������
        /// </summary>
        /// <param name="previousPerson"></param>
        /// <param name="declensionCase"></param>
        /// <param name="fioPart"></param>
        /// <returns></returns>
        public static string Declension(string previousPerson, DeclensionCase declensionCase, string fioPart)
        {
            try
            {
                string surname;
                string name;
                string patronimic;
                if (!string.IsNullOrEmpty(fioPart) && "���".Contains(fioPart.ToUpper()))
                {
                    string f = "������";
                    string i = "����";
                    string o = "��������";
                    switch (fioPart.ToUpper().Trim())
                    {
                        case "�":
                            previousPerson = NameOwnerTools.GetNamePart(
                                DeclensionBLL.GetSNPDeclension(previousPerson, i, o, declensionCase), 0);
                            break;
                        case "�":
                            previousPerson = NameOwnerTools.GetNamePart(
                                DeclensionBLL.GetSNPDeclension(f, previousPerson, o, declensionCase), 1);
                            break;
                        case "�":
                            previousPerson = NameOwnerTools.GetNamePart(
                                DeclensionBLL.GetSNPDeclension(f, i, previousPerson, declensionCase), 2);
                            break;
                    }
                }
                else
                {
                    //���� �� ���������� ����� ���, �� �������� ��� ��� ���
                    DeclensionBLL.GetSNM(previousPerson, out surname, out name, out patronimic);
                    previousPerson = DeclensionBLL.GetSNPDeclension(surname, name, patronimic, declensionCase);
                }
            }
            catch
            {
                XtraMessageBox.Show(
                    "�� ������� ����������� ��� ����������� �����������. ��� ���������� ����� ������� ������� � ������.",
                    "��������������", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            return previousPerson;
        }

        /// <summary>
        /// �������������� ���� ��� ������ ����������� �� ������
        /// </summary>
        /// <param name="person"></param>
        /// <param name="previousPerson"></param>
        /// <param name="res"></param>
        /// <returns></returns>
        public static Dictionary<string, string> GetWorkDirectionFields(Person person, string previousPerson,
                                                                        Dictionary<string, string> res)
        {
            res.Add("[����.�������]", NameOwnerTools.GetNamePart(previousPerson, 0));
            res.Add("[����.���]", NameOwnerTools.GetNamePart(previousPerson, 1));
            res.Add("[����.��������]", NameOwnerTools.GetNamePart(previousPerson, 2));
            return res;
        }

        /// <summary>
        /// ��������� ����� ��� ������ �� ����������� ��������
        /// </summary>
        /// <param name="res"></param>
        /// <param name="totalMinutes"></param>
        /// <param name="comissioner"></param>
        /// <param name="teacher"></param>
        /// <param name="executive"></param>
        /// <param name="testPassings"></param>
        /// <returns></returns>
        public static Dictionary<string, string> GetTeachingConclusionFields(Dictionary<string, string> res,
                                                                             int totalMinutes,
                                                                             TeachingConclusionControl.Member
                                                                                 comissioner,
                                                                             TeachingConclusionControl.Member teacher,
                                                                             TeachingConclusionControl.Member executive,
                                                                             IEnumerable<vMaxPassingTest> testPassings,
                                                                             IEnumerable
                                                                                 <
                                                                                 TeachingConclusionControl.
                                                                                 LessonsSummaryRow> exams)

        {
            res.Add("[����� ��������]",  (int)TimeSpan.FromMinutes(totalMinutes).TotalHours+":"+
                TimeSpan.FromMinutes(totalMinutes).Minutes);

            //������������ ��������
            res.Add("[�������.���������]", comissioner.Rank);
            res.Add("[�������.�������]", NameOwnerTools.GetLastName(comissioner.Name));
            if (NameOwnerTools.GetFirstName(comissioner.Name).Length > 0)
                res.Add("[�������.�]", NameOwnerTools.GetFirstName(comissioner.Name)[0].ToString());

            //������������� �������
            res.Add("[��������.���������]", teacher.Rank);
            res.Add("[��������.�������]", NameOwnerTools.GetLastName(teacher.Name));
            if (NameOwnerTools.GetFirstName(teacher.Name).Length > 0)
                res.Add("[��������.�]", NameOwnerTools.GetFirstName(teacher.Name)[0].ToString());

            //������������� �� ����������
            res.Add("[������.�������]", NameOwnerTools.GetLastName(executive.Name));
            if (NameOwnerTools.GetFirstName(executive.Name).Length > 0)
                res.Add("[������.�]", NameOwnerTools.GetFirstName(executive.Name)[0].ToString());

            //������� �������� �������� ��������� ���������
            decimal maxPerc = 0;
            int index = 0;
            foreach (TeachingConclusionControl.LessonsSummaryRow row in exams)
            {
                if (row.TestGuid != Guid.Empty && testPassings.Where(i => i.TestUniqueId == row.TestGuid).Count() > 0)
                {
                    TeachingConclusionControl.LessonsSummaryRow row1 = row;
                    vMaxPassingTest testPassing =
                        testPassings.Where(i => i.TestUniqueId == row1.TestGuid).SingleOrDefault();
                    if (testPassing != null)
                    {
                        //����������� ������������ ������� ��� ����������� ��������
                        if (testPassing.perc != null)
                        {
                            maxPerc += testPassing.perc.Value;
                            index++;
                        }
                    }
                }
            }
            decimal avgPerc = index == 0 ? 0 : maxPerc/index;
            res.Add("[�������.��������.�������]", avgPerc.ToString());
            return res;
        }

        /// <summary>
        /// ����� �������� ������ � ���������
        /// </summary>
        /// <param name="exams">��������</param>
        /// <param name="members">����� ��������</param>
        /// <param name="testPassings"></param>
        public void CreateTeachingConclusionTables(
            IEnumerable<TeachingConclusionControl.LessonsSummaryRow> exams,
            IEnumerable<TeachingConclusionControl.Member> members,
            IEnumerable<vMaxPassingTest> testPassings)
        {
            Object beforeRow = Type.Missing;

            ////������� � ������
            Table tbl = _wordDoc.Tables[1];
            int index = 2;
            decimal maxPerc = 0;
            //������� � ������������
            foreach (TeachingConclusionControl.LessonsSummaryRow row in exams)
            {
                //��������� ������ � ������� ��������� word
                tbl.Rows.Add(ref beforeRow);
                //tbl.Cell(index, 1).Range.Text = (index - 1).ToString();
                tbl.Cell(index, 1).Range.Text = row.Discipline;
                if (row.TestGuid != Guid.Empty && 
                    testPassings.Where(i => i.TestUniqueId == row.TestGuid).Count() > 0)
                {
                    var testPassing = testPassings.Where(i => i.TestUniqueId == row.TestGuid).FirstOrDefault();
                    if (testPassing != null)
                    {
                        //�������� �� ��� �����
                        if (testPassing.Speed != null)
                        {
                            tbl.Cell(index, 2).Range.Text = testPassing.Description ?? string.Empty;
                        }
                        else
                        {
                            tbl.Cell(index, 2).Range.Text = testPassing.perc != null
                                                                ? testPassing.perc.Value.ToString()
                                                                : string.Empty;
                        }
                        //����������� ������������ ������� ��� ����������� ��������
                        if (testPassing.perc != null)
                            maxPerc += testPassing.perc.Value;
                        //tbl.Cell(index, 3).Range.Text = testPassing.Valuation != null
                        //                                    ? testPassing.Valuation.Value.ToString()
                        //                                    : string.Empty;
                    }
                }
                //tbl.Cell(index, 3).Range.Text = row.Duration.ToString();
                index++;
            }
            //MessageBox.Show("�������� �������");
            //������� � ���������
            tbl = _wordDoc.Tables[2];
            index = 2;
            foreach (TeachingConclusionControl.Member row in members)
            {
                //  MessageBox.Show(row.Name);
                //��������� ������ � ������� ��������� word
                tbl.Rows.Add(ref beforeRow);
                //MessageBox.Show(tbl.Rows.Count.ToString());
                //MessageBox.Show(tbl.Columns.Count.ToString());
                tbl.Cell(index, 2).Range.Text = row.Rank;
                tbl.Cell(index, 3).Range.Text = "_______________";

                string str = string.Empty;
                try
                {
                    string firstName = NameOwnerTools.GetFirstName(row.Name);
                    if (!string.IsNullOrEmpty(firstName) && firstName.Length > 1)
                    {
                        str = string.Format("{0}.", firstName[0]);
                    }
                    str = string.Format("{0}", NameOwnerTools.GetLastName(row.Name));
                }
                catch (Exception ex)
                {
                    MessageBox.Show(@"������ " + ex.Message);
                }

                tbl.Cell(index, 4).Range.Text = str;
                index++;
            }
        }

        /// <summary>
        /// ����� ���������� ������ ������� �������
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="ship"></param>
        /// <param name="table"></param>
        /// <param name="index"></param>
        /// <param name="shipIndex"></param>
        private static void FillShipRow(Form owner, Ship ship, Table table, int index, int shipIndex)
        {
            var richTextBox = new RichTextBox();

            table.Cell(index, 1).Range.Text = (shipIndex).ToString();
            table.Cell(index, 2).Range.Text = ship.Name;
            table.Cell(index, 3).Range.Text = ship.Index;
            table.Cell(index, 4).Range.Text = ship.River != null ? ship.River.ToString() : String.Empty;
            table.Cell(index, 5).Range.Text = ship.ShipLine != null ? ship.ShipLine.ToString() : String.Empty;
            table.Cell(index, 6).Range.Text = ship.Condition != null ? ship.Condition.ToString() : String.Empty;

            table.Cell(index, 7).Range.Text = String.Join(", ",
                                                          (from shipTool in ship.ShipTools
                                                           select shipTool.Tool.ToString()).ToArray());

            #region Responsibility Zone

            ResponsibilityZone rz = ship.ResponsibilityZone;
            string rzText = String.Empty;

            if (rz != null)
            {
                if ((new[] {rz.Name, rz.N, rz.OY, rz.Room, rz.Building}).Any(
                    s => !String.IsNullOrEmpty(s)))
                {
                    rzText += String.Format("{0}, {1}/{2}, {3}/{4}",
                                            GetValueOrPlaceHolder(rz.Name, "���"),
                                            GetValueOrPlaceHolder(rz.N, "N"),
                                            GetValueOrPlaceHolder(rz.OY, "OY"),
                                            GetValueOrPlaceHolder(rz.Room, "�������"),
                                            GetValueOrPlaceHolder(rz.Building, "������"));
                }

                if (rz.ResponsibilityZonePhones.Count > 0)
                {
                    if (rzText.Length > 0)
                    {
                        rzText += Environment.NewLine;
                    }

                    rzText += String.Join(Environment.NewLine,
                                          (from phone in rz.ResponsibilityZonePhones
                                           select phone.PhoneType != null
                                                      ? String.Format("{0}/{1}", phone.Number,
                                                                      phone.PhoneType)
                                                      : phone.Number).ToArray());
                }
            }

            #endregion

            table.Cell(index, 8).Range.Text = rzText;

            table.Cell(index, 9).Range.Text = String.Join(Environment.NewLine,
                                                          (from reference in ship.References
                                                           select
                                                               String.Format("{0}{1}{2}", reference.Item,
                                                                             Char.ConvertFromUtf32(0x2014),
                                                                             reference.Value)).ToArray());

            #region Notes

            MethodInvoker method = delegate
                                       {
                                           Clipboard.Clear();
                                           richTextBox.Rtf = ship.Notes;
                                           richTextBox.SelectAll();
                                           richTextBox.Copy();
                                           try
                                           {
                                               float fontSize = table.Cell(2, 10).Range.Font.Size;
                                               table.Cell(index, 10).Range.Paste();
                                               table.Cell(index, 10).Range.Font.Size = fontSize;
                                           }
                                           catch (Exception)
                                           {
                                               table.Cell(index, 10).Range.Text = String.Empty;
                                           }
                                           Clipboard.Clear();
                                       };
            owner.Invoke(method);

            #endregion
        }

        /// <summary>
        /// ����� ���������� ������ ������� �������
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="ship"></param>
        /// <param name="table"></param>
        /// <param name="index"></param>
        /// <param name="shipIndex"></param>
        private static void FillPersonRow(Form owner, CoursesPassingPerson person, Table table, int index, int shipIndex)
        {
            //�����
            table.Cell(index, 1).Range.Text = (shipIndex).ToString();
            //������
            table.Cell(index, 2).Range.Text = person.Person.Degree != null ? person.Person.Degree.Name : string.Empty;
            //���
            table.Cell(index, 3).Range.Text = person.Person.Name;
            //���������
            table.Cell(index, 4).Range.Text = person.Person.Position != null ? person.Person.Position.Name : string.Empty;
            //�������������
            table.Cell(index, 5).Range.Text = person.Debts;

            //���������� �����
            table.Cell(index, 6).Range.Text = person.Person.TripCount.HasValue ? person.Person.TripCount.ToString() : string.Empty;
            //���� ������ ��������
            table.Cell(index, 7).Range.Text = person.Person.TeachingStartDate.HasValue
                                                  ? person.Person.TeachingStartDate.Value.ToShortDateString()
                                                  : string.Empty;
            //�������
            table.Cell(index, 8).Range.Text = person.Person.Ship != null ? person.Person.Ship.Name : string.Empty;

            //�������
            if(person.Person.Ship != null)
            table.Cell(index, 9).Range.Text = person.Person.Ship.Condition != null ? person.Person.Ship.Condition.Name : string.Empty;

            //��������
            table.Cell(index, 10).Range.Text = person.ShipTool;

            //���� ��������
            table.Cell(index, 11).Range.Text = person.Person.ExaminationDate.HasValue
                                                  ? person.Person.ExaminationDate.Value.ToShortDateString()
                                                  : string.Empty;
            //��������
            table.Cell(index, 12).Range.Text = person.One?"+":string.Empty;

            //������
            table.Cell(index, 13).Range.Text = person.Request;
            //�������
            table.Cell(index, 14).Range.Text = person.Person.AppointmentOrderDate.HasValue
                                                   ? person.Person.AppointmentOrderDate.Value.ToShortDateString()
                                                   : string.Empty;

            //�������������
            table.Cell(index, 15).Range.Text = person.Two ? "+" : string.Empty;
            //�������������
            table.Cell(index, 16).Range.Text = person.Three ? "+" : string.Empty;
            //�������������
            table.Cell(index, 17).Range.Text = person.Four;
            //�������������
            table.Cell(index, 18).Range.Text = person.Five ? "+" : string.Empty;
            //�������������
            table.Cell(index, 19).Range.Text = person.Six ? "+" : string.Empty;
            //�������������
            table.Cell(index, 20).Range.Text = person.Seven ? "+" : string.Empty;

            //������
            table.Cell(index, 21).Range.Text = person.HasLetters ? "+" : string.Empty;


            //���� ������
            table.Cell(index, 22).Range.Text = person.Person.DepartureDate.HasValue
                                                   ? person.Person.DepartureDate.Value.ToShortDateString()
                                                   : string.Empty;
            //������
            table.Cell(index, 23).Range.Text = person.Notes;
        }

        private static string FormatProgressMessage(string message, int index, int count)
        {
            return String.Format("{0} | {1}%", message, Math.Floor(100d*(index + 1)/count));
        }

        public void CreateCoursesPassing(MarqueeProgressForm owner, List<CoursesPassingPerson> first, List<CoursesPassingPerson> second)
        {
            Object beforeRow = Type.Missing;

            Table table = _wordDoc.Tables[1];
            int index = 2;
            string message = owner.Message;
            for (int i = 0; i < first.Count; ++i)
            {
                //��������� ������ � ������� ��������� word
                table.Rows.Add(ref beforeRow);
                //��������� ������
                FillPersonRow(owner, first[i], table, index, index - 1);
                index++;
                owner.Message = FormatProgressMessage(message, i, first.Count);
            }
            table = _wordDoc.Tables[2];
            index = 2;
            message = owner.Message;
            for (int i = 0; i < second.Count; ++i)
            {
                //��������� ������ � ������� ��������� word
                table.Rows.Add(ref beforeRow);
                //��������� ������
                FillPersonRow(owner, second[i], table, index, index - 1);
                index++;
                owner.Message = FormatProgressMessage(message, i, second.Count);
            }

            Thread.Sleep(300);
        }

        public void CreateShipsTable(MarqueeProgressForm owner, List<Ship> ships)
        {
            Object beforeRow = Type.Missing;
            Table table = _wordDoc.Tables[1];
            int index = 2;

            string message = owner.Message;

            for (int i = 0; i < ships.Count; ++i)
            {
                //��������� ������ � ������� ��������� word
                table.Rows.Add(ref beforeRow);
                //��������� ������
                FillShipRow(owner, ships[i], table, index, index - 1);
                index++;

                owner.Message = FormatProgressMessage(message, i, ships.Count);
            }

            Thread.Sleep(300);
        }

        public void CreateShipsPersonsTable(MarqueeProgressForm owner, List<Ship> ships)
        {
            //�������� ������� � ���������� �� ������
            _wordDoc.Select();
            Thread.Sleep(1000);
            GoToEnd();
            Thread.Sleep(1000);
            GoToEnd();
            Thread.Sleep(1000);
            Paste();
            Thread.Sleep(1000);
            //�������� �����
            Object beforeRow = Type.Missing;
            Table shipTable = _wordDoc.Tables[1];
            Table personTable = _wordDoc.Tables[2];
            int rowIndex = 2;
            //�������� ����� �������� ��� ���� ��������
            int personsShip = ships.Where(item => item.Persons.Count > 0).Count();

            //��������� �������
            for (int i = 0; i < ships.Count + personsShip; i++)
            {
                //��������� ������ � ������� ��������� word
                shipTable.Rows.Add(ref beforeRow);
            }

            string message = owner.Message;

            //������� �� �������� � �������� ������
            int shipIndex = 1;
            for (int i = 0; i < ships.Count; ++i)
            {
                //��������� ������ � ������ � �������
                FillShipRow(owner, ships[i], shipTable, rowIndex, shipIndex);

                #region ���������� � ������� ���������� � ����������

                if (ships[i].Persons.Count > 0)
                {
                    //�������� �� ����� ������
                    rowIndex++;
                    //��������� � ������ (�������������) ��� ������ ������
                    object begCell = shipTable.Cell(rowIndex, 1).Range.Start;
                    object endCell = shipTable.Cell(rowIndex, shipTable.Rows[rowIndex].Cells.Count).Range.End;
                    Range wordcellrange = _wordDoc.Range(ref begCell, ref endCell);
                    wordcellrange.Select();
                    _wa.Selection.Cells.Merge();

                    //������ ������� � ����������
                    foreach (Row row in personTable.Rows)
                    {
                        if (!row.IsFirst && row.Index > 2)
                            row.Delete();
                    }
                    //�������� ������� � ����������
                    CreatePersonsTable(owner, ships[i].Persons.ToList(), personTable);
                    CopyTable(personTable);
                    wordcellrange.PasteAsNestedTable();
                }

                #endregion

                rowIndex++;
                shipIndex++;

                owner.Message = FormatProgressMessage(message, i, ships.Count);
            }
            personTable.Delete();

            Thread.Sleep(300);
        }


        private static void FillPersonRow(Form owner, Person person, Table table, int index)
        {
            var richTextBox = new RichTextBox();

            table.Cell(index, 1).Range.Text = (index - 1).ToString();
            table.Cell(index, 2).Range.Text = person.Name;
            table.Cell(index, 3).Range.Text = person.BirthDate.ToShortDateString();
            table.Cell(index, 4).Range.Text = person.ChildNickName ?? String.Empty;
            table.Cell(index, 5).Range.Text = person.Address ?? String.Empty;
            table.Cell(index, 6).Range.Text = person.Qualification != null
                                                  ? person.Qualification.ToString()
                                                  : String.Empty;
            table.Cell(index, 7).Range.Text = person.Education != null ? person.Education.ToString() : String.Empty;
            table.Cell(index, 8).Range.Text = person.Degree != null ? person.Degree.ToString() : String.Empty;
            table.Cell(index, 9).Range.Text = person.Position != null ? person.Position.ToString() : String.Empty;
            table.Cell(index, 10).Range.Text = person.Department != null
                                                   ? person.Department.ToString()
                                                   : String.Empty;
            table.Cell(index, 11).Range.Text = person.Affiliation != null
                                                   ? person.Affiliation.ToString()
                                                   : String.Empty;
            table.Cell(index, 12).Range.Text = person.Ship != null ? person.Ship.Name : String.Empty;
            table.Cell(index, 13).Range.Text = person.Category != null ? person.Category.ToString() : String.Empty;
            table.Cell(index, 14).Range.Text = (person.AppointmentOrderCode ?? string.Empty) +
                                               (person.AppointmentOrderDate != null
                                                    ? (" �� " +
                                                       ((DateTime) person.AppointmentOrderDate).ToShortDateString())
                                                    : String.Empty);
            table.Cell(index, 15).Range.Text = person.TeachingStartDate != null
                                                   ? ((DateTime) person.TeachingStartDate).ToShortDateString()
                                                   : String.Empty;
            table.Cell(index, 16).Range.Text = person.ExaminationDate != null
                                                   ? ((DateTime) person.ExaminationDate).ToShortDateString()
                                                   : String.Empty;
            table.Cell(index, 17).Range.Text = person.DepartureDate != null
                                                   ? ((DateTime) person.DepartureDate).ToShortDateString()
                                                   : String.Empty;
            table.Cell(index, 18).Range.Text = person.Period ?? String.Empty;
            table.Cell(index, 19).Range.Text = person.TripCount != null
                                                   ? person.TripCount.ToString()
                                                   : String.Empty;
            table.Cell(index, 20).Range.Text = person.Statuse != null
                                                   ? person.Statuse.ToString()
                                                   : String.Empty;

            table.Cell(index, 21).Range.Text = String.Join(Environment.NewLine,
                                                           (from phone in person.PersonPhones
                                                            select phone.PhoneType != null
                                                                       ? String.Format("{0}/{1}", phone.Number,
                                                                                       phone.PhoneType)
                                                                       : phone.Number).ToArray());

            table.Cell(index, 22).Range.Text = String.Join(Environment.NewLine,
                                                           (from letter in person.Letters
                                                            select
                                                                String.Format("{0}-{1}-{2}", letter.Addressee,
                                                                              letter.Code,
                                                                              letter.Date.ToShortDateString())).
                                                               ToArray());

            table.Cell(index, 23).Range.Text = String.Format("�����: {0}{1}������: {2}", person.ProfessionalLogin,
                                                             Environment.NewLine, person.ProfessionalPassword);

            #region Notes

            MethodInvoker method = delegate
                                       {
                                           Clipboard.Clear();
                                           richTextBox.Rtf = person.Notes;
                                           richTextBox.SelectAll();
                                           richTextBox.Copy();

                                           try
                                           {
                                               float fontSize = table.Cell(2, 24).Range.Font.Size;
                                               table.Cell(index, 24).Range.Paste();
                                               table.Cell(index, 24).Range.Font.Size = fontSize;
                                           }
                                           catch (Exception)
                                           {
                                               table.Cell(index, 24).Range.Text = String.Empty;
                                           }
                                           Clipboard.Clear();
                                       };
            owner.Invoke(method);

            #endregion
        }

        public void CreatePersonsTable(MarqueeProgressForm owner, List<Person> persons, int tableIndex)
        {
            Table table = _wordDoc.Tables[tableIndex];
            CreatePersonsTable(owner, persons, table);
        }

        private static void CreatePersonsTable(MarqueeProgressForm owner, List<Person> persons, Table table)
        {
            Object beforeRow = Type.Missing;

            string message = owner.Message;

            int index = 2;
            for (int i = 0; i < persons.Count; ++i)
            {
                //��������� ������ � ������� ��������� word
                table.Rows.Add(ref beforeRow);
                FillPersonRow(owner, persons[i], table, index);
                index++;

                owner.Message = FormatProgressMessage(message, i, persons.Count);
            }

            Thread.Sleep(300);
        }


        /// <summary>
        /// ���������� ������� ��������� �� �������
        /// </summary>
        /// <param name="replacedFields"></param>
        public void ReplaceTemplateCommonFields(Dictionary<string, string> replacedFields)
        {
            //������ ���������
            try
            {
                replacedFields = ReplaceDeclination(replacedFields);
            }
            catch (Exception ex) { }

            try
            {
                //������ ���� �����
                ReplaceFields(replacedFields);
            }
            catch { KillWinWord(); }
        }

        private Dictionary<string, string> ReplaceDeclination(
            Dictionary<string, string> replacedFields)
        {
            try
            {
                //������ ����� � ��������
                foreach (CustomTag tag in _tags)
                {
                    //���� ��� �������� ��������
                    if (tag.IsFunction && tag.FuncName == "��������")
                    {
                        string field = "[" + tag.FieldName.Trim() + "]".ToUpper();

                        //������� �� ������� �������� � ������� � ��� ����
                        foreach (var replaceField in replacedFields)
                        {
                            if (field == replaceField.Key)
                            {
                                //������� �����
                                string decCase = tag.Parameters.ContainsKey("�����")
                                                     ? tag.Parameters["�����"]
                                                     : string.Empty;
                                if (string.IsNullOrEmpty(decCase))
                                {
                                    //�������� ���� ��� ������
                                    replacedFields.Add(tag.FieldText, replaceField.Value);
                                }
                                else
                                {
                                    //������� �� �������� ����� ���
                                    string fioPart = tag.Parameters.ContainsKey("����� ���")
                                                         ? tag.Parameters["����� ���"]
                                                         : string.Empty;
                                    if (string.IsNullOrEmpty(fioPart))
                                    {
                                        //�� ����� ��������� ����� ���
                                        replacedFields.Add(tag.FieldText,
                                                           Declension(replaceField.Value,
                                                                      (DeclensionCase) int.Parse(decCase), string.Empty));
                                    }
                                    else
                                    {
                                        //��������� ���������� ����� ���
                                        replacedFields.Add(tag.FieldText,
                                                           Declension(replaceField.Value,
                                                                      (DeclensionCase) int.Parse(decCase), fioPart));
                                    }
                                }
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return replacedFields;
        }

        #endregion
    }
}
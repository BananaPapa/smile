namespace Curriculum.Reports
{
    partial class TeachingConclusionControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._wizardControl = new DevExpress.XtraWizard.WizardControl();
            this._welcomeWizardPage = new DevExpress.XtraWizard.WelcomeWizardPage();
            this._welcomeLabelControl = new DevExpress.XtraEditors.LabelControl();
            this._personWizardPage = new DevExpress.XtraWizard.WizardPage();
            this._personGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this._personGridLookUpEditView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._personNameGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._personShipGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._personPositionGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._personTeachingStartDateGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._personExaminationDateGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._completionWizardPage = new DevExpress.XtraWizard.CompletionWizardPage();
            this._finishLabelControl = new DevExpress.XtraEditors.LabelControl();
            this._lessonsWizardPage = new DevExpress.XtraWizard.WizardPage();
            this._lessonsGridControl = new DevExpress.XtraGrid.GridControl();
            this._lessonsGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._disciplineGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._lessonTypeGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._durationGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._toExamsGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._toExamsCheckEdit = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this._toThemesCheckEdit = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this._commissionWizardPage = new DevExpress.XtraWizard.WizardPage();
            this._commissionGridControl = new DevExpress.XtraGrid.GridControl();
            this._commissionGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._memberRankGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._memberNameGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._isMemberGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._isMemberCheckEdit = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this._isCommissionerGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._isCommissionerCheckEdit = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this._isTeacherGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._isTeacherCheckEdit = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this._isExecutiveGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._isExecutiveCheckEdit = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.wizardPageReportType = new DevExpress.XtraWizard.WizardPage();
            this.radioGroupReportType = new DevExpress.XtraEditors.RadioGroup();
            ((System.ComponentModel.ISupportInitialize)(this._wizardControl)).BeginInit();
            this._wizardControl.SuspendLayout();
            this._welcomeWizardPage.SuspendLayout();
            this._personWizardPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._personGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._personGridLookUpEditView)).BeginInit();
            this._completionWizardPage.SuspendLayout();
            this._lessonsWizardPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lessonsGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._lessonsGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._toExamsCheckEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._toThemesCheckEdit)).BeginInit();
            this._commissionWizardPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._commissionGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._commissionGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._isMemberCheckEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._isCommissionerCheckEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._isTeacherCheckEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._isExecutiveCheckEdit)).BeginInit();
            this.wizardPageReportType.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupReportType.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // _wizardControl
            // 
            this._wizardControl.CancelText = "&������";
            this._wizardControl.Controls.Add(this._welcomeWizardPage);
            this._wizardControl.Controls.Add(this._personWizardPage);
            this._wizardControl.Controls.Add(this._completionWizardPage);
            this._wizardControl.Controls.Add(this._lessonsWizardPage);
            this._wizardControl.Controls.Add(this._commissionWizardPage);
            this._wizardControl.Controls.Add(this.wizardPageReportType);
            this._wizardControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._wizardControl.FinishText = "��&���";
            this._wizardControl.HeaderImage = global::Curriculum.Properties.Resources.AlignCenter16;
            this._wizardControl.ImageLayout = System.Windows.Forms.ImageLayout.Center;
            this._wizardControl.Location = new System.Drawing.Point(0, 0);
            this._wizardControl.Name = "_wizardControl";
            this._wizardControl.NextText = "&����� >";
            this._wizardControl.Pages.AddRange(new DevExpress.XtraWizard.BaseWizardPage[] {
            this._welcomeWizardPage,
            this.wizardPageReportType,
            this._personWizardPage,
            this._lessonsWizardPage,
            this._commissionWizardPage,
            this._completionWizardPage});
            this._wizardControl.PreviousText = "< &�����";
            this._wizardControl.Size = new System.Drawing.Size(743, 582);
            this._wizardControl.Tag = "..\\���������� �� ��������#r";
            this._wizardControl.Text = "���������� �� ��������";
            this._wizardControl.TitleImage = global::Curriculum.Properties.Resources.MSWordDocument;
            this._wizardControl.WizardStyle = DevExpress.XtraWizard.WizardStyle.WizardAero;
            this._wizardControl.CancelClick += new System.ComponentModel.CancelEventHandler(this.WizardControlCancelClick);
            this._wizardControl.FinishClick += new System.ComponentModel.CancelEventHandler(this.WizardControlFinishClick);
            this._wizardControl.NextClick += new DevExpress.XtraWizard.WizardCommandButtonClickEventHandler(this.WizardControlNextClick);
            // 
            // _welcomeWizardPage
            // 
            this._welcomeWizardPage.Controls.Add(this._welcomeLabelControl);
            this._welcomeWizardPage.IntroductionText = "���� ������ ������� ��� ������� ���������� �� ��������.";
            this._welcomeWizardPage.Name = "_welcomeWizardPage";
            this._welcomeWizardPage.ProceedText = "����� ����������, ������� �����";
            this._welcomeWizardPage.Size = new System.Drawing.Size(683, 420);
            this._welcomeWizardPage.Text = "������ �������� ������";
            // 
            // _welcomeLabelControl
            // 
            this._welcomeLabelControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._welcomeLabelControl.Location = new System.Drawing.Point(0, 0);
            this._welcomeLabelControl.Name = "_welcomeLabelControl";
            this._welcomeLabelControl.Size = new System.Drawing.Size(310, 13);
            this._welcomeLabelControl.TabIndex = 0;
            this._welcomeLabelControl.Text = "���� ������ ������� ��� ������� ���������� �� ��������.";
            // 
            // _personWizardPage
            // 
            this._personWizardPage.Controls.Add(this._personGridLookUpEdit);
            this._personWizardPage.DescriptionText = "������� ����������, ��� �������� ���������� ������� ���������� �� ��������.";
            this._personWizardPage.Name = "_personWizardPage";
            this._personWizardPage.Size = new System.Drawing.Size(683, 420);
            this._personWizardPage.Text = "���������";
            // 
            // _personGridLookUpEdit
            // 
            this._personGridLookUpEdit.Dock = System.Windows.Forms.DockStyle.Top;
            this._personGridLookUpEdit.EditValue = "";
            this._personGridLookUpEdit.Location = new System.Drawing.Point(0, 0);
            this._personGridLookUpEdit.Name = "_personGridLookUpEdit";
            this._personGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._personGridLookUpEdit.Properties.NullText = "�������� ����������";
            this._personGridLookUpEdit.Properties.PopupFormMinSize = new System.Drawing.Size(800, 400);
            this._personGridLookUpEdit.Properties.View = this._personGridLookUpEditView;
            this._personGridLookUpEdit.Size = new System.Drawing.Size(683, 20);
            this._personGridLookUpEdit.TabIndex = 5;
            this._personGridLookUpEdit.Popup += new System.EventHandler(this.PersonGridLookUpEditPopup);
            this._personGridLookUpEdit.EditValueChanged += new System.EventHandler(this.PersonGridLookUpEditEditValueChanged);
            // 
            // _personGridLookUpEditView
            // 
            this._personGridLookUpEditView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._personNameGridColumn,
            this._personShipGridColumn,
            this._personPositionGridColumn,
            this._personTeachingStartDateGridColumn,
            this._personExaminationDateGridColumn});
            this._personGridLookUpEditView.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this._personGridLookUpEditView.Name = "_personGridLookUpEditView";
            this._personGridLookUpEditView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this._personGridLookUpEditView.OptionsView.ShowGroupPanel = false;
            // 
            // _personNameGridColumn
            // 
            this._personNameGridColumn.Caption = "���";
            this._personNameGridColumn.FieldName = "Name";
            this._personNameGridColumn.Name = "_personNameGridColumn";
            this._personNameGridColumn.Visible = true;
            this._personNameGridColumn.VisibleIndex = 0;
            this._personNameGridColumn.Width = 223;
            // 
            // _personShipGridColumn
            // 
            this._personShipGridColumn.Caption = "�������";
            this._personShipGridColumn.FieldName = "Ship";
            this._personShipGridColumn.Name = "_personShipGridColumn";
            this._personShipGridColumn.Visible = true;
            this._personShipGridColumn.VisibleIndex = 1;
            this._personShipGridColumn.Width = 172;
            // 
            // _personPositionGridColumn
            // 
            this._personPositionGridColumn.Caption = "���������";
            this._personPositionGridColumn.FieldName = "Position";
            this._personPositionGridColumn.Name = "_personPositionGridColumn";
            this._personPositionGridColumn.Visible = true;
            this._personPositionGridColumn.VisibleIndex = 2;
            this._personPositionGridColumn.Width = 172;
            // 
            // _personTeachingStartDateGridColumn
            // 
            this._personTeachingStartDateGridColumn.Caption = "������ ��������";
            this._personTeachingStartDateGridColumn.FieldName = "TeachingStartDate";
            this._personTeachingStartDateGridColumn.Name = "_personTeachingStartDateGridColumn";
            this._personTeachingStartDateGridColumn.Visible = true;
            this._personTeachingStartDateGridColumn.VisibleIndex = 3;
            // 
            // _personExaminationDateGridColumn
            // 
            this._personExaminationDateGridColumn.Caption = "��������� ��������";
            this._personExaminationDateGridColumn.FieldName = "ExaminationDate";
            this._personExaminationDateGridColumn.Name = "_personExaminationDateGridColumn";
            this._personExaminationDateGridColumn.Visible = true;
            this._personExaminationDateGridColumn.VisibleIndex = 4;
            // 
            // _completionWizardPage
            // 
            this._completionWizardPage.Controls.Add(this._finishLabelControl);
            this._completionWizardPage.FinishText = "��� ������ ��� ���������� �� �������� ������.";
            this._completionWizardPage.Name = "_completionWizardPage";
            this._completionWizardPage.ProceedText = "����� ������������ �����, ������� ������ �����.";
            this._completionWizardPage.Size = new System.Drawing.Size(683, 420);
            this._completionWizardPage.Text = "���������� ������ �������";
            // 
            // _finishLabelControl
            // 
            this._finishLabelControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._finishLabelControl.Location = new System.Drawing.Point(0, 0);
            this._finishLabelControl.Name = "_finishLabelControl";
            this._finishLabelControl.Size = new System.Drawing.Size(164, 13);
            this._finishLabelControl.TabIndex = 1;
            this._finishLabelControl.Text = "��� ������ ��� ������ ������.";
            // 
            // _lessonsWizardPage
            // 
            this._lessonsWizardPage.Controls.Add(this._lessonsGridControl);
            this._lessonsWizardPage.DescriptionText = "������� � ������, ����� ���� � ����������� ������� �������� � ���������� �� �����" +
    "���.";
            this._lessonsWizardPage.Name = "_lessonsWizardPage";
            this._lessonsWizardPage.Size = new System.Drawing.Size(683, 420);
            this._lessonsWizardPage.Text = "������ ��� � ����������� �������";
            // 
            // _lessonsGridControl
            // 
            this._lessonsGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lessonsGridControl.Location = new System.Drawing.Point(0, 0);
            this._lessonsGridControl.MainView = this._lessonsGridView;
            this._lessonsGridControl.Name = "_lessonsGridControl";
            this._lessonsGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this._toThemesCheckEdit,
            this._toExamsCheckEdit});
            this._lessonsGridControl.Size = new System.Drawing.Size(683, 420);
            this._lessonsGridControl.TabIndex = 0;
            this._lessonsGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this._lessonsGridView});
            // 
            // _lessonsGridView
            // 
            this._lessonsGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._disciplineGridColumn,
            this._lessonTypeGridColumn,
            this._durationGridColumn,
            this._toExamsGridColumn});
            this._lessonsGridView.GridControl = this._lessonsGridControl;
            this._lessonsGridView.GroupCount = 1;
            this._lessonsGridView.Name = "_lessonsGridView";
            this._lessonsGridView.OptionsBehavior.AutoExpandAllGroups = true;
            this._lessonsGridView.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this._lessonTypeGridColumn, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this._disciplineGridColumn, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // _disciplineGridColumn
            // 
            this._disciplineGridColumn.Caption = "����";
            this._disciplineGridColumn.FieldName = "Discipline";
            this._disciplineGridColumn.Name = "_disciplineGridColumn";
            this._disciplineGridColumn.OptionsColumn.ReadOnly = true;
            this._disciplineGridColumn.Visible = true;
            this._disciplineGridColumn.VisibleIndex = 0;
            // 
            // _lessonTypeGridColumn
            // 
            this._lessonTypeGridColumn.Caption = "��� �������";
            this._lessonTypeGridColumn.FieldName = "LessonType";
            this._lessonTypeGridColumn.Name = "_lessonTypeGridColumn";
            this._lessonTypeGridColumn.OptionsColumn.ReadOnly = true;
            // 
            // _durationGridColumn
            // 
            this._durationGridColumn.Caption = "���������� �����";
            this._durationGridColumn.FieldName = "Duration";
            this._durationGridColumn.Name = "_durationGridColumn";
            this._durationGridColumn.OptionsColumn.ReadOnly = true;
            this._durationGridColumn.Visible = true;
            this._durationGridColumn.VisibleIndex = 1;
            // 
            // _toExamsGridColumn
            // 
            this._toExamsGridColumn.Caption = "� �����";
            this._toExamsGridColumn.ColumnEdit = this._toExamsCheckEdit;
            this._toExamsGridColumn.FieldName = "ToExams";
            this._toExamsGridColumn.Name = "_toExamsGridColumn";
            this._toExamsGridColumn.Visible = true;
            this._toExamsGridColumn.VisibleIndex = 2;
            // 
            // _toExamsCheckEdit
            // 
            this._toExamsCheckEdit.AutoHeight = false;
            this._toExamsCheckEdit.Caption = "Check";
            this._toExamsCheckEdit.Name = "_toExamsCheckEdit";
            // 
            // _toThemesCheckEdit
            // 
            this._toThemesCheckEdit.AutoHeight = false;
            this._toThemesCheckEdit.Caption = "Check";
            this._toThemesCheckEdit.Name = "_toThemesCheckEdit";
            // 
            // _commissionWizardPage
            // 
            this._commissionWizardPage.Controls.Add(this._commissionGridControl);
            this._commissionWizardPage.DescriptionText = "�������� � ������ ������ ��������, ������������ �������� � �������������� �������" +
    "������.";
            this._commissionWizardPage.Name = "_commissionWizardPage";
            this._commissionWizardPage.Size = new System.Drawing.Size(683, 420);
            this._commissionWizardPage.Text = "����� ��������";
            // 
            // _commissionGridControl
            // 
            this._commissionGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._commissionGridControl.Location = new System.Drawing.Point(0, 0);
            this._commissionGridControl.MainView = this._commissionGridView;
            this._commissionGridControl.Name = "_commissionGridControl";
            this._commissionGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this._isMemberCheckEdit,
            this._isTeacherCheckEdit,
            this._isCommissionerCheckEdit,
            this._isExecutiveCheckEdit});
            this._commissionGridControl.Size = new System.Drawing.Size(683, 420);
            this._commissionGridControl.TabIndex = 0;
            this._commissionGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this._commissionGridView});
            // 
            // _commissionGridView
            // 
            this._commissionGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._memberRankGridColumn,
            this._memberNameGridColumn,
            this._isMemberGridColumn,
            this._isCommissionerGridColumn,
            this._isTeacherGridColumn,
            this._isExecutiveGridColumn});
            this._commissionGridView.GridControl = this._commissionGridControl;
            this._commissionGridView.Name = "_commissionGridView";
            this._commissionGridView.OptionsView.ShowGroupPanel = false;
            this._commissionGridView.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.CommissionGridViewCellValueChanging);
            // 
            // _memberRankGridColumn
            // 
            this._memberRankGridColumn.Caption = "���������";
            this._memberRankGridColumn.FieldName = "Rank";
            this._memberRankGridColumn.Name = "_memberRankGridColumn";
            this._memberRankGridColumn.OptionsColumn.ReadOnly = true;
            this._memberRankGridColumn.Visible = true;
            this._memberRankGridColumn.VisibleIndex = 1;
            // 
            // _memberNameGridColumn
            // 
            this._memberNameGridColumn.Caption = "���";
            this._memberNameGridColumn.FieldName = "Name";
            this._memberNameGridColumn.Name = "_memberNameGridColumn";
            this._memberNameGridColumn.OptionsColumn.ReadOnly = true;
            this._memberNameGridColumn.Visible = true;
            this._memberNameGridColumn.VisibleIndex = 0;
            // 
            // _isMemberGridColumn
            // 
            this._isMemberGridColumn.Caption = "���� ��������";
            this._isMemberGridColumn.ColumnEdit = this._isMemberCheckEdit;
            this._isMemberGridColumn.FieldName = "IsMember";
            this._isMemberGridColumn.Name = "_isMemberGridColumn";
            this._isMemberGridColumn.Visible = true;
            this._isMemberGridColumn.VisibleIndex = 2;
            // 
            // _isMemberCheckEdit
            // 
            this._isMemberCheckEdit.AutoHeight = false;
            this._isMemberCheckEdit.Caption = "Check";
            this._isMemberCheckEdit.Name = "_isMemberCheckEdit";
            // 
            // _isCommissionerGridColumn
            // 
            this._isCommissionerGridColumn.Caption = "������������";
            this._isCommissionerGridColumn.ColumnEdit = this._isCommissionerCheckEdit;
            this._isCommissionerGridColumn.FieldName = "IsCommissioner";
            this._isCommissionerGridColumn.Name = "_isCommissionerGridColumn";
            this._isCommissionerGridColumn.Visible = true;
            this._isCommissionerGridColumn.VisibleIndex = 3;
            // 
            // _isCommissionerCheckEdit
            // 
            this._isCommissionerCheckEdit.AutoHeight = false;
            this._isCommissionerCheckEdit.Caption = "Check";
            this._isCommissionerCheckEdit.Name = "_isCommissionerCheckEdit";
            // 
            // _isTeacherGridColumn
            // 
            this._isTeacherGridColumn.Caption = "�������������";
            this._isTeacherGridColumn.ColumnEdit = this._isTeacherCheckEdit;
            this._isTeacherGridColumn.FieldName = "IsTeacher";
            this._isTeacherGridColumn.Name = "_isTeacherGridColumn";
            this._isTeacherGridColumn.Visible = true;
            this._isTeacherGridColumn.VisibleIndex = 4;
            // 
            // _isTeacherCheckEdit
            // 
            this._isTeacherCheckEdit.AutoHeight = false;
            this._isTeacherCheckEdit.Caption = "Check";
            this._isTeacherCheckEdit.Name = "_isTeacherCheckEdit";
            // 
            // _isExecutiveGridColumn
            // 
            this._isExecutiveGridColumn.Caption = "������������� �����������";
            this._isExecutiveGridColumn.ColumnEdit = this._isExecutiveCheckEdit;
            this._isExecutiveGridColumn.FieldName = "IsExecutive";
            this._isExecutiveGridColumn.Name = "_isExecutiveGridColumn";
            this._isExecutiveGridColumn.Visible = true;
            this._isExecutiveGridColumn.VisibleIndex = 5;
            // 
            // _isExecutiveCheckEdit
            // 
            this._isExecutiveCheckEdit.AutoHeight = false;
            this._isExecutiveCheckEdit.Caption = "Check";
            this._isExecutiveCheckEdit.Name = "_isExecutiveCheckEdit";
            // 
            // wizardPageReportType
            // 
            this.wizardPageReportType.Controls.Add(this.radioGroupReportType);
            this.wizardPageReportType.Name = "wizardPageReportType";
            this.wizardPageReportType.Size = new System.Drawing.Size(683, 420);
            this.wizardPageReportType.Tag = "..\\���������� �� ��������#r";
            this.wizardPageReportType.Text = "��� ������";
            // 
            // radioGroupReportType
            // 
            this.radioGroupReportType.EditValue = "TeachingConclusionShort";
            this.radioGroupReportType.Location = new System.Drawing.Point(3, 3);
            this.radioGroupReportType.Name = "radioGroupReportType";
            this.radioGroupReportType.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("TeachingConclusionShort", "�������"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("TeachingConclusion", "���������")});
            this.radioGroupReportType.Size = new System.Drawing.Size(677, 96);
            this.radioGroupReportType.TabIndex = 0;
            // 
            // TeachingConclusionControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._wizardControl);
            this.Name = "TeachingConclusionControl";
            this.Size = new System.Drawing.Size(743, 582);
            this.Tag = "���������� �� ��������#r";
            this.Load += new System.EventHandler(this.TeachingConclusionControlLoad);
            ((System.ComponentModel.ISupportInitialize)(this._wizardControl)).EndInit();
            this._wizardControl.ResumeLayout(false);
            this._welcomeWizardPage.ResumeLayout(false);
            this._welcomeWizardPage.PerformLayout();
            this._personWizardPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._personGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._personGridLookUpEditView)).EndInit();
            this._completionWizardPage.ResumeLayout(false);
            this._completionWizardPage.PerformLayout();
            this._lessonsWizardPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lessonsGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._lessonsGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._toExamsCheckEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._toThemesCheckEdit)).EndInit();
            this._commissionWizardPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._commissionGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._commissionGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._isMemberCheckEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._isCommissionerCheckEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._isTeacherCheckEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._isExecutiveCheckEdit)).EndInit();
            this.wizardPageReportType.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupReportType.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraWizard.WizardControl _wizardControl;
        private DevExpress.XtraWizard.WelcomeWizardPage _welcomeWizardPage;
        private DevExpress.XtraWizard.WizardPage _personWizardPage;
        private DevExpress.XtraWizard.CompletionWizardPage _completionWizardPage;
        private DevExpress.XtraEditors.GridLookUpEdit _personGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView _personGridLookUpEditView;
        private DevExpress.XtraGrid.Columns.GridColumn _personNameGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _personShipGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _personPositionGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _personTeachingStartDateGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _personExaminationDateGridColumn;
        private DevExpress.XtraWizard.WizardPage _lessonsWizardPage;
        private DevExpress.XtraWizard.WizardPage _commissionWizardPage;
        private DevExpress.XtraEditors.LabelControl _welcomeLabelControl;
        private DevExpress.XtraEditors.LabelControl _finishLabelControl;
        private DevExpress.XtraGrid.GridControl _lessonsGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView _lessonsGridView;
        private DevExpress.XtraGrid.Columns.GridColumn _disciplineGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _lessonTypeGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _durationGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _toExamsGridColumn;
        private DevExpress.XtraGrid.GridControl _commissionGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView _commissionGridView;
        private DevExpress.XtraGrid.Columns.GridColumn _memberNameGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _memberRankGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _isMemberGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _isCommissionerGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _isTeacherGridColumn;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit _isMemberCheckEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit _isTeacherCheckEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit _isCommissionerCheckEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit _toThemesCheckEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit _toExamsCheckEdit;
        private DevExpress.XtraGrid.Columns.GridColumn _isExecutiveGridColumn;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit _isExecutiveCheckEdit;
        private DevExpress.XtraWizard.WizardPage wizardPageReportType;
        private DevExpress.XtraEditors.RadioGroup radioGroupReportType;
    }
}

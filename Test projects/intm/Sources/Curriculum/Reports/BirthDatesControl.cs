using System;
using System.Collections.Generic;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using Eureca.Integrator.Common;
using File = System.IO.File;
namespace Curriculum.Reports
{
    /// <summary>
    /// Coming birthdays report control.
    /// </summary>
    public partial class BirthDatesControl : ControlWithAccessAreas, IAliasesHolder
    {
        /// <summary>
        /// Birthday report key.
        /// </summary>
        private const string ReportKey = "Birthday";

        /// <summary>
        /// Initializes new instance of birth dates control.
        /// </summary>
        public BirthDatesControl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Gets selected person.
        /// </summary>
        private Person SelectedPerson
        {
            get { return _gridView.GetFocusedRow() as Person; }
        }

        #region IAliasesHolder Members

        /// <summary>
        /// Applies aliases.
        /// </summary>
        public void ApplyAliases()
        {
            try
            {
                Aliase.ApplyAlias( "Ship", a => _shipGridColumn.Caption = a );
            }
            catch (DbException ex)
            {
                Program.HandleDbException(ex);
            }
        }

        #endregion

        /// <summary>
        /// Initilizes grid control's data source.
        /// </summary>
        private void InitDataSource()
        {
            _daysSpinEdit.Value = Properties.Settings.Default.LastSelectedDaysCountBirthdayReport;
            SelectFilteredPersons();
        }

        private void SelectFilteredPersons()
        {
            _gridControl.DataSource =
                Person.SelectByComingBirthdays((int) _daysSpinEdit.Value, Setting.GetExtclusionStatusId(),
                                               Setting.GetBirthAffiliationIds()).ToList();
        }

        /// <summary>
        /// Handles birth dates control load event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void BirthDatesControlLoad(object sender, EventArgs e)
        {
            InitDataSource();
        }

        /// <summary>
        /// Handles days spin edit value changed event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void DaysSpinEditEditValueChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.LastSelectedDaysCountBirthdayReport = (int) _daysSpinEdit.Value;
            Properties.Settings.Default.Save();
            SelectFilteredPersons();
        }

        /// <summary>
        /// Handles grid view focused row changed event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void GridViewFocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            _reportSimpleButton.Enabled = SelectedPerson != null;
        }

        /// <summary>
        /// Handles  drop down button click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void ReportSimpleButtonClick(object sender, EventArgs e)
        {
            ProduceBirthdayReport(ReportKey);
        }

        /// <summary>
        /// Produces birthday report.
        /// </summary>
        /// <param name="reportKey"></param>
        private void ProduceBirthdayReport(string reportKey)
        {
            Person person = SelectedPerson;
            try
            {
                if (person != null)
                {
                    string reportPath = Reporter.GetTemplateDocument(reportKey, this);
                    if (!string.IsNullOrEmpty(reportPath))
                    {
                        var wordManager = new WordManager();

                        try
                        {
                            wordManager.Open(reportPath);
                            wordManager.ReplaceTemplateCommonFields(WordManager.GetCommonFields(person));
                            wordManager.SaveAs(reportPath);
                        }
                        finally
                        {
                            wordManager.KillWinWord();
                        }


                        Application.DoEvents();
                        // ��������� � ��������� �����....
                        Reporter.SaveAndOpenReport(fileName =>
                                                       {
                                                           try
                                                           {
                                                               if (System.IO.File.Exists( fileName ))
                                                                   System.IO.File.Delete( fileName );
                                                               System.IO.File.Move( reportPath, fileName );

                                                               try
                                                               {
                                                                   Greeting.Save(person, person.GetNextBirthday());
                                                               }
                                                               catch (Exception ex)
                                                               {
                                                                   return ex;
                                                               }

                                                               return null;
                                                           }
                                                           catch (Exception e)
                                                           {
                                                               return e;
                                                           }
                                                       });
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "����������");
            }
        }

        public override IEnumerable<ISKOCBCTDNBTIDED> GetHiddenChilds(bool withLazy)
        {
            var hidden = new List<ISKOCBCTDNBTIDED>();

            if (withLazy)
            {
                var reportTemplateDummyForm = Reporter.GetReportTemplateDummyForm( );
                var supportAccessAreasForm = reportTemplateDummyForm as ISupportAccessAreas;
                supportAccessAreasForm.HierarchyParent = Program.StartForm;
                hidden.Add( new ControlInterfaceImplementation( reportTemplateDummyForm ) );
            }

            return hidden;
        }
    }
}
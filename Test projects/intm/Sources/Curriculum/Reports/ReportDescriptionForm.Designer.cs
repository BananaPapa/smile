﻿namespace Curriculum.Reports
{
    partial class ReportDescriptionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._descriptionMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            ((System.ComponentModel.ISupportInitialize)(this._descriptionMemoEdit.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // _descriptionMemoEdit
            // 
            this._descriptionMemoEdit.Dock = System.Windows.Forms.DockStyle.Fill;
            this._descriptionMemoEdit.Location = new System.Drawing.Point(0, 0);
            this._descriptionMemoEdit.Name = "_descriptionMemoEdit";
            this._descriptionMemoEdit.Properties.ReadOnly = true;
            this._descriptionMemoEdit.Size = new System.Drawing.Size(540, 284);
            this._descriptionMemoEdit.TabIndex = 0;
            // 
            // ReportDescriptionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(540, 284);
            this.Controls.Add(this._descriptionMemoEdit);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "ReportDescriptionForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Описание шаблона";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.ReportDescriptionFormLoad);
            ((System.ComponentModel.ISupportInitialize)(this._descriptionMemoEdit.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.MemoEdit _descriptionMemoEdit;
    }
}
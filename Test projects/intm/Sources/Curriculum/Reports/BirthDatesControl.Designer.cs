namespace Curriculum.Reports
{
    partial class BirthDatesControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._panelControl = new DevExpress.XtraEditors.PanelControl();
            this._reportSimpleButton = new DevExpress.XtraEditors.SimpleButton();
            this._gridControl = new DevExpress.XtraGrid.GridControl();
            this._gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._nameGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._birthDateGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._shipGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnPhonesString = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnPosition = new DevExpress.XtraGrid.Columns.GridColumn();
            this._greetinExistsGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._greetingExists�heckEdit = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this._textLabelControl = new DevExpress.XtraEditors.LabelControl();
            this._daysSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this._daysLabelControl = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this._panelControl)).BeginInit();
            this._panelControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._greetingExists�heckEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._daysSpinEdit.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // _panelControl
            // 
            this._panelControl.Controls.Add(this._reportSimpleButton);
            this._panelControl.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._panelControl.Location = new System.Drawing.Point(0, 512);
            this._panelControl.Name = "_panelControl";
            this._panelControl.Size = new System.Drawing.Size(798, 30);
            this._panelControl.TabIndex = 4;
            this._panelControl.Tag = "";
            // 
            // _reportSimpleButton
            // 
            this._reportSimpleButton.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this._reportSimpleButton.Enabled = false;
            this._reportSimpleButton.Image = global::Curriculum.Properties.Resources.MSWordDocument;
            this._reportSimpleButton.Location = new System.Drawing.Point(720, 2);
            this._reportSimpleButton.Name = "_reportSimpleButton";
            this._reportSimpleButton.Size = new System.Drawing.Size(76, 26);
            this._reportSimpleButton.TabIndex = 0;
            this._reportSimpleButton.Tag = "������� ��� ��������#r";
            this._reportSimpleButton.Text = "�����";
            this._reportSimpleButton.Click += new System.EventHandler(this.ReportSimpleButtonClick);
            // 
            // _gridControl
            // 
            this._gridControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._gridControl.Location = new System.Drawing.Point(0, 29);
            this._gridControl.MainView = this._gridView;
            this._gridControl.Name = "_gridControl";
            this._gridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this._greetingExists�heckEdit});
            this._gridControl.Size = new System.Drawing.Size(798, 480);
            this._gridControl.TabIndex = 3;
            this._gridControl.Tag = "������� ��� ��������#r";
            this._gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this._gridView});
            // 
            // _gridView
            // 
            this._gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._nameGridColumn,
            this._birthDateGridColumn,
            this._shipGridColumn,
            this.gridColumnPhonesString,
            this.gridColumnPosition,
            this._greetinExistsGridColumn});
            this._gridView.GridControl = this._gridControl;
            this._gridView.Name = "_gridView";
            this._gridView.OptionsBehavior.Editable = false;
            this._gridView.OptionsDetail.EnableMasterViewMode = false;
            this._gridView.OptionsView.ShowDetailButtons = false;
            this._gridView.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.GridViewFocusedRowChanged);
            // 
            // _nameGridColumn
            // 
            this._nameGridColumn.Caption = "���";
            this._nameGridColumn.FieldName = "Name";
            this._nameGridColumn.Name = "_nameGridColumn";
            this._nameGridColumn.Tag = "���#r";
            this._nameGridColumn.Visible = true;
            this._nameGridColumn.VisibleIndex = 0;
            // 
            // _birthDateGridColumn
            // 
            this._birthDateGridColumn.Caption = "���� ��������";
            this._birthDateGridColumn.FieldName = "BirthDate";
            this._birthDateGridColumn.Name = "_birthDateGridColumn";
            this._birthDateGridColumn.Tag = "���� ��������#r";
            this._birthDateGridColumn.Visible = true;
            this._birthDateGridColumn.VisibleIndex = 1;
            // 
            // _shipGridColumn
            // 
            this._shipGridColumn.Caption = "�������";
            this._shipGridColumn.FieldName = "Ship";
            this._shipGridColumn.Name = "_shipGridColumn";
            this._shipGridColumn.Tag = "�������#r";
            this._shipGridColumn.Visible = true;
            this._shipGridColumn.VisibleIndex = 2;
            // 
            // gridColumnPhonesString
            // 
            this.gridColumnPhonesString.Caption = "�������";
            this.gridColumnPhonesString.FieldName = "PhonesString";
            this.gridColumnPhonesString.Name = "gridColumnPhonesString";
            this.gridColumnPhonesString.Tag = "�������#r";
            this.gridColumnPhonesString.Visible = true;
            this.gridColumnPhonesString.VisibleIndex = 3;
            // 
            // gridColumnPosition
            // 
            this.gridColumnPosition.Caption = "���������";
            this.gridColumnPosition.FieldName = "Position";
            this.gridColumnPosition.Name = "gridColumnPosition";
            this.gridColumnPosition.Tag = "���������#r";
            this.gridColumnPosition.Visible = true;
            this.gridColumnPosition.VisibleIndex = 4;
            // 
            // _greetinExistsGridColumn
            // 
            this._greetinExistsGridColumn.Caption = "������������ ������������";
            this._greetinExistsGridColumn.ColumnEdit = this._greetingExists�heckEdit;
            this._greetinExistsGridColumn.FieldName = "GreetingExists";
            this._greetinExistsGridColumn.Name = "_greetinExistsGridColumn";
            this._greetinExistsGridColumn.Tag = "������������ ������������#r";
            this._greetinExistsGridColumn.Visible = true;
            this._greetinExistsGridColumn.VisibleIndex = 5;
            // 
            // _greetingExists�heckEdit
            // 
            this._greetingExists�heckEdit.AutoHeight = false;
            this._greetingExists�heckEdit.Caption = "Check";
            this._greetingExists�heckEdit.Name = "_greetingExists�heckEdit";
            // 
            // _textLabelControl
            // 
            this._textLabelControl.Location = new System.Drawing.Point(3, 6);
            this._textLabelControl.Name = "_textLabelControl";
            this._textLabelControl.Size = new System.Drawing.Size(188, 13);
            this._textLabelControl.TabIndex = 0;
            this._textLabelControl.Tag = "������� ��� ��������#r";
            this._textLabelControl.Text = "��� �������� ������� � ���������";
            // 
            // _daysSpinEdit
            // 
            this._daysSpinEdit.EditValue = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this._daysSpinEdit.Location = new System.Drawing.Point(197, 3);
            this._daysSpinEdit.Name = "_daysSpinEdit";
            this._daysSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this._daysSpinEdit.Properties.IsFloatValue = false;
            this._daysSpinEdit.Properties.Mask.EditMask = "N00";
            this._daysSpinEdit.Properties.MaxValue = new decimal(new int[] {
            365,
            0,
            0,
            0});
            this._daysSpinEdit.Properties.MinValue = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this._daysSpinEdit.Size = new System.Drawing.Size(45, 20);
            this._daysSpinEdit.TabIndex = 1;
            this._daysSpinEdit.Tag = "������� ��� ��������#r";
            this._daysSpinEdit.EditValueChanged += new System.EventHandler(this.DaysSpinEditEditValueChanged);
            // 
            // _daysLabelControl
            // 
            this._daysLabelControl.Location = new System.Drawing.Point(248, 6);
            this._daysLabelControl.Name = "_daysLabelControl";
            this._daysLabelControl.Size = new System.Drawing.Size(29, 13);
            this._daysLabelControl.TabIndex = 2;
            this._daysLabelControl.Tag = "������� ��� ��������#r";
            this._daysLabelControl.Text = "����.";
            // 
            // BirthDatesControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._daysLabelControl);
            this.Controls.Add(this._daysSpinEdit);
            this.Controls.Add(this._textLabelControl);
            this.Controls.Add(this._gridControl);
            this.Controls.Add(this._panelControl);
            this.Name = "BirthDatesControl";
            this.Size = new System.Drawing.Size(798, 542);
            this.Tag = "��� ��������#r";
            this.Load += new System.EventHandler(this.BirthDatesControlLoad);
            ((System.ComponentModel.ISupportInitialize)(this._panelControl)).EndInit();
            this._panelControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._greetingExists�heckEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._daysSpinEdit.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl _panelControl;
        private DevExpress.XtraGrid.GridControl _gridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView _gridView;
        private DevExpress.XtraGrid.Columns.GridColumn _nameGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _birthDateGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _shipGridColumn;
        private DevExpress.XtraEditors.LabelControl _textLabelControl;
        private DevExpress.XtraEditors.SpinEdit _daysSpinEdit;
        private DevExpress.XtraEditors.LabelControl _daysLabelControl;
        private DevExpress.XtraEditors.SimpleButton _reportSimpleButton;
        private DevExpress.XtraGrid.Columns.GridColumn _greetinExistsGridColumn;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit _greetingExists�heckEdit;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnPhonesString;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnPosition;
    }
}

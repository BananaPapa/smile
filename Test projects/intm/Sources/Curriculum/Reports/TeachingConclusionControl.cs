using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraWizard;
using Eureca.Integrator.Common;

namespace Curriculum.Reports
{
    /// <summary>
    /// Teaching conclusion report control.
    /// </summary>
    public partial class TeachingConclusionControl : ControlWithAccessAreas, IAliasesHolder
    {
        /// <summary>
        /// Initializes new instance of teaching conclusion report control.
        /// </summary>
        public TeachingConclusionControl()
        {
            InitializeComponent();
        }

        private string ReportKey
        {
            get
            {
                if (radioGroupReportType.SelectedIndex == -1)
                    return "TeachingConclusion";
                return radioGroupReportType.Properties.Items[radioGroupReportType.SelectedIndex].Value.ToString();
            }
        }

        /// <summary>
        /// Selected person.
        /// </summary>
        private Person Person
        {
            get { return _personGridLookUpEdit.EditValue as Person; }
        }

        /// <summary>
        /// Commission info.
        /// </summary>
        private IEnumerable<Member> Commission
        {
            get
            {
                try
                {
                    return (IEnumerable<Member>) _commissionGridView.DataSource;
                }
                catch (InvalidCastException)
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Lessons summary info.
        /// </summary>
        private IEnumerable<LessonsSummaryRow> LessonsSummary
        {
            get
            {
                try
                {
                    return (IEnumerable<LessonsSummaryRow>) _lessonsGridControl.DataSource;
                }
                catch (InvalidCastException)
                {
                    return null;
                }
            }
        }

        #region IAliasesHolder Members

        public void ApplyAliases()
        {
            try
            {
                Aliase.ApplyAlias( "Ship", a => _personShipGridColumn.Caption = a );
                Aliase.ApplyAlias( "Position", a => _personPositionGridColumn.Caption = a );
            }
            catch (DbException e)
            {
                Program.HandleDbException(e);
            }
        }

        #endregion

        /// <summary>
        /// Resets wizard to initial state.
        /// </summary>
        private void ResetWizard()
        {
            _personGridLookUpEdit.Properties.DataSource = from person in Program.DbSingletone.Persons
                                                          where person.Statuse.CreateProbationer
                                                          orderby person.ExaminationDate descending
                                                          select person;
            _personGridLookUpEdit.EditValue = null;

            _lessonsGridControl.DataSource = null;

            _commissionGridControl.DataSource = from member in Program.DbSingletone.CommissionMembers
                                                select new Member {Name = member.Name, Rank = member.Rank};

            _wizardControl.SelectedPage = _welcomeWizardPage;
        }

        /// <summary>
        /// Gets lessons summary grid control data source.
        /// </summary>
        /// <param name="person">Person to get lessons summary for.</param>
        /// <returns>Lessons summary enumerable data.</returns>
        private static List<LessonsSummaryRow> GetLessonsSummary(Person person)
        {
            if (person == null)
            {
                throw new ArgumentNullException("person");
            }

            IQueryable<LessonsSummaryRow> collectionSet = Program.DbSingletone.Lessons
                // Filter by person id.
                .Where(l => l.PersonId == person.PersonId)
                // Order by discipline name.
                .OrderBy(l => l.Discipline.Name)
                // Group by discipline.
                .GroupBy(l => l.Discipline)
                .Select(item =>
                        new LessonsSummaryRow
                            {
                                Discipline = item.Key.Name,
                                TestGuid = item.Key.TestGuid,
                                Duration = item.Sum(l => l.Duration),
                                ToExams = true
                            }
                );


            return collectionSet.ToList();
        }

        /// <summary>
        /// Handles teaching conclusion control load event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void TeachingConclusionControlLoad(object sender, EventArgs e)
        {
            ResetWizard();
        }

        /// <summary>
        /// Handles person grid look up edit value changed event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void PersonGridLookUpEditEditValueChanged(object sender, EventArgs e)
        {
            Person person = Person;

            if (person != null)
            {
                _lessonsGridControl.DataSource = GetLessonsSummary(person);
            }
        }

        /// <summary>
        /// Handles person grid look up edit popup event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void PersonGridLookUpEditPopup(object sender, EventArgs e)
        {
            _personGridLookUpEdit.Properties.View.BestFitColumns();
        }

        /// <summary>
        /// Handles wizard finish click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void WizardControlFinishClick(object sender, CancelEventArgs e)
        {
            ProduceReport();
        }

        /// <summary>
        /// Handles wizard cancel click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void WizardControlCancelClick(object sender, CancelEventArgs e)
        {
            ResetWizard();
        }

        /// <summary>
        /// Shows not enough data message.
        /// </summary>
        /// <param name="message">Message to show.</param>
        private static void ShowNotEnoughDataMessage(string message)
        {
            XtraMessageBox.Show(message, "������������ ������", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        /// <summary>
        /// Handles wizard next click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void WizardControlNextClick(object sender, WizardCommandButtonClickEventArgs e)
        {
            if (_wizardControl.SelectedPage == wizardPageReportType)
            {
                if (radioGroupReportType.Properties.Items[radioGroupReportType.SelectedIndex].Value.ToString() ==
                    "TeachingConclusionShort")
                {
                    //�������� �������� � ������� ����� �������� � ���������
                    _lessonsWizardPage.Visible = false;
                    _commissionWizardPage.Visible = false;
                }
                else
                {
                    //���������� ��������
                    _lessonsWizardPage.Visible = true;
                    _commissionWizardPage.Visible = true;
                }
            }
            if (_wizardControl.SelectedPage == _personWizardPage)
            {
                if (Person == null)
                {
                    ShowNotEnoughDataMessage("�������� ����������.");
                    e.Handled = true;
                }
            }
                //else if (_wizardControl.SelectedPage == _lessonsWizardPage)
                //{
                //    IEnumerable<LessonsSummaryRow> lessonsSummary = LessonsSummary;

                //}
            else if (_wizardControl.SelectedPage == _commissionWizardPage)
            {
                IEnumerable<Member> commission = Commission;

                if (commission != null)
                {
                    if (!commission.Any(m => m.IsMember))
                    {
                        ShowNotEnoughDataMessage("�������� ���� �� ������ ����� ��������.");
                        e.Handled = true;
                    }
                    else if (!commission.Any(m => m.IsCommissioner))
                    {
                        ShowNotEnoughDataMessage("�������� ������������ ��������.");
                        e.Handled = true;
                    }
                    else if (!commission.Any(m => m.IsTeacher))
                    {
                        ShowNotEnoughDataMessage("�������� �������������� �������������.");
                        e.Handled = true;
                    }
                    else if (!commission.Any(m => m.IsExecutive))
                    {
                        ShowNotEnoughDataMessage("�������� �������������� ����������� ����� ���������.");
                        e.Handled = true;
                    }
                }
            }
        }

        /// <summary>
        /// Handles commission grid view cell value changing event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void CommissionGridViewCellValueChanging(object sender, CellValueChangedEventArgs e)
        {
            var value = e.Value as bool?;
            var row = _commissionGridView.GetRow(e.RowHandle) as Member;

            if (value != null && row != null)
            {
                if (e.Column == _isMemberGridColumn)
                {
                    row.IsMember = (bool) value;
                }
                else if (e.Column == _isCommissionerGridColumn)
                {
                    if (value == true)
                    {
                        row.IsMember = false;

                        try
                        {
                            Commission.ForEach(member => { if (member != row) member.IsCommissioner = false; });
                        }
                        catch (ArgumentNullException)
                        {
                        }
                    }

                    row.IsCommissioner = (bool) value;

                    _commissionGridView.RefreshData();
                }
                else if (e.Column == _isTeacherGridColumn)
                {
                    if (value == true)
                    {
                        try
                        {
                            Commission.ForEach(member => { if (member != row) member.IsTeacher = false; });
                        }
                        catch (ArgumentNullException)
                        {
                        }
                    }

                    row.IsTeacher = (bool) value;

                    _commissionGridView.RefreshData();
                }
                else if (e.Column == _isExecutiveGridColumn)
                {
                    if (value == true)
                    {
                        try
                        {
                            Commission.ForEach(member => { if (member != row) member.IsExecutive = false; });
                        }
                        catch (ArgumentNullException)
                        {
                        }
                    }

                    row.IsExecutive = (bool) value;

                    _commissionGridView.RefreshData();
                }
            }
        }

        /// <summary>
        /// Produces teaching conclusion report.
        /// </summary>
        private void ProduceReport()
        {
            // ��� ������ ������� ��������� �� null ���� � � ��������� �� �� ������ ������!

            // ���������
            Person person = Person;
            if (person == null)
                throw new NullReferenceException("person");

            //�������� ������
            try
            {
                string reportPath = Reporter.GetTemplateDocument(ReportKey, this);
                if (!string.IsNullOrEmpty(reportPath))
                {
                    var wordManager = new WordManager();
                    try
                    {
                        wordManager.Open(reportPath);

                        Dictionary<string, string> dict;
                        if (ReportKey == "TeachingConclusion")
                        {
                            // ��� ������ �� ��������
                            IEnumerable<LessonsSummaryRow> lessonsSummary = LessonsSummary.OrderBy(l => l.Discipline);
                            // ��� ������ �� ��������
                            IEnumerable<Member> commission = Commission;

                            // ��������-�����������, ������� ���� �������� � �����
                            IEnumerable<LessonsSummaryRow> exams = lessonsSummary.Where(l => l.ToExams);


                            Guid userUniqueId = person.ProfessionalUserUniqueId.HasValue
                                                    ? person.ProfessionalUserUniqueId.Value
                                                    : Guid.Empty;
                            IEnumerable<vMaxPassingTest> testPassigns =
                                from items in Program.ProfessionalDb.vMaxPassingTests
                                where items.UserUniqueId == userUniqueId
                                select items;


                            // ����� ����� (� ��� ������� ���� ������� ������ ����, ���� ������� ��������, �� ���������������� ����� ������)
                            int totalMinutes = lessonsSummary.Where(l => l.ToExams).Sum(t => t.Duration);

                            // ������ �����, ���������� � �����
                            IEnumerable<Member> members = commission.Where(m => m.IsMember);
                            // ������������ ��������, ���������� � �����
                            Member commissioner = commission.First(m => m.IsCommissioner);
                            // ������������ �������������, ���������� � �����
                            Member teacher = commission.First(m => m.IsTeacher);
                            // ������������ �����������, ���������� � �����
                            Member executive = commission.First(m => m.IsExecutive);


                            //�������� ������� ���������� �����
                            dict = WordManager.GetTeachingConclusionFields(
                                WordManager.GetCommonFields(person), totalMinutes, commissioner, teacher, executive,
                                testPassigns, exams);

                            //��������� ������ � �������
                            try
                            {
                                wordManager.CreateTeachingConclusionTables(exams, members, testPassigns);
                            }
                            catch (Exception)
                            {
                                MessageBox.Show(@"������������ ������. ������ ������������ ������ � ������.");
                            }
                        }
                        else
                        {
                            dict = WordManager.GetCommonFields(person);
                        }

                        //�������� ����
                        wordManager.ReplaceTemplateCommonFields(dict);
                        wordManager.SaveAs(reportPath);
                    }
                    catch (Exception ex)
                    {
                        Program.HandleUnhandledException(ex);
                        MessageBox.Show(ex.Message);
                    }
                    finally
                    {
                        wordManager.KillWinWord();
                    }


                    Application.DoEvents();
                    // ��������� � ��������� �����....
                    Reporter.SaveAndOpenReport(fileName =>
                                                   {
                                                       try
                                                       {
                                                           if (System.IO.File.Exists( fileName ))
                                                               System.IO.File.Delete( fileName );
                                                           System.IO.File.Move( reportPath, fileName );
                                                           return null;
                                                       }
                                                       catch (Exception ex)
                                                       {
                                                           return ex;
                                                       }
                                                   });
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "����������");
            }
        }

        #region Nested type: LessonsSummaryRow

        /// <summary>
        /// Lesson summary row.
        /// </summary>
        public class LessonsSummaryRow
        {
            public string Discipline { get; set; }
            public int Duration { get; set; }
            public bool ToExams { get; set; }
            public Guid? TestGuid { get; set; }
        }

        #endregion

        #region Nested type: Member

        /// <summary>
        /// Commission member row.
        /// </summary>
        public class Member
        {
            public string Name { get; set; }
            public string Rank { get; set; }
            public bool IsMember { get; set; }
            public bool IsCommissioner { get; set; }
            public bool IsTeacher { get; set; }
            public bool IsExecutive { get; set; }
        }

        #endregion
    }
}
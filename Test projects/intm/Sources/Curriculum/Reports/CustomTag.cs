﻿using System;
using System.Collections.Generic;

namespace Curriculum.Reports
{
    /// <summary>
    /// Класс для инкапсуляции работы с тегами
    /// </summary>
    public class CustomTag : IComparable
    {
        #region Fields

        public Dictionary<string, string> Parameters = new Dictionary<string, string>();

        #endregion

        #region Properties

        /// <summary>
        /// Название поля
        /// </summary>
        public string FieldName { get; private set; }

        /// <summary>
        /// Значение поля которое нужно подставлять вместо тега 
        /// </summary>
        public string FieldValue { get; set; }


        /// <summary>
        /// Наименование таблицы из которой берется поле
        /// </summary>
        public string TableName { get; private set; }

        /// <summary>
        /// название функции
        /// </summary>
        public string FuncName { get; private set; }

        /// <summary>
        /// флаг что Тег есть табличное поле
        /// </summary>
        public bool IsTableField
        {
            get
            {
                if (string.IsNullOrEmpty(TableName))
                    return false;
                return true;
            }
        }

        public bool IsFunction { get; private set; }

        public string FieldText { get; private set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="text">Текст для парсинга и получения данных по тегу
        /// </param>
        public CustomTag(string text)
        {
            FieldText = "[" + text + "]";
            GetCustomTagAttributesFromFieldText(text);
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Парсинг текста поля и получение из него аттрибутов
        /// </summary>
        /// <param name="text"></param>
        private void GetCustomTagAttributesFromFieldText(string text)
        {
            text = text.Trim();

            //поле с функцией
            if (text.Contains("#"))
            {
                //Поиск по ключевому слову функции
                foreach (string func in text.Split('#'))
                {
                    if (string.IsNullOrEmpty(func))
                    {
                        //Пустой тег
                        continue;
                    }

                    //получаю название функции
                    FuncName = func.Substring(0, func.IndexOf('('));
                    IsFunction = true;
                    //Очищаю до скобки
                    string parseText = func.Remove(0, func.IndexOf('('));
                    //Если есть параметры, то перечисляю по параметрам
                    if (parseText.Contains(","))
                    {
                        string field = parseText.Substring(0, parseText.IndexOf(","));
                        ParseFieldName(field);
                        parseText = parseText.Remove(0, parseText.IndexOf(','));
                        ParseParams(parseText);
                    }
                    else
                    {
                        //Нет параметров
                        ParseFieldName(parseText);
                    }
                }
            }
            else
            {
                //Простое поле
                ParseFieldName(text);
            }
        }

        private void ParseFieldName(string text)
        {
            //Очистка служебных символов
            text = text.Replace(")", "").Replace("(", "");

            FieldName = text;
            ////Поиск имени таблицы
            //int pos1 = text.IndexOf(".");
            //if (pos1 <= 0)
            //{
            //    FieldName = text;
            //}
            //else
            //{
            //    TableName = text.Substring(0, pos1);
            //    FieldName = text.Substring(pos1 + 1, text.Length - pos1 - 1);
            //}
        }

        private void ParseParams(string text)
        {
            foreach (string parametr in text.Split(','))
            {
                string trimPar = parametr.Trim().Replace("(", "").Replace(")", "");
                if (!trimPar.Contains("="))
                    continue;
                string key = trimPar.Substring(0, trimPar.IndexOf("=")).Trim();
                string value = trimPar.Remove(0, key.Length + 2).Trim();
                Parameters.Add(key, value);
            }
            return;
        }

        #endregion

        #region Override methods

        /// <summary>
        /// Override method toString()
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return FieldName;
        }

        #endregion

        #region Implement methods

        /// <summary>
        /// сравнение с другим тегом
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int CompareTo(object obj)
        {
            var tag = obj as CustomTag;
            if (tag == null)
                return -1;

            if (IsTableField || tag.IsTableField)
            {
                if (TableName.CompareTo(tag.TableName) == 0)
                    return FieldName.CompareTo(tag.FieldName);
                return -1;
            }

            return FieldName.CompareTo(tag.FieldName);
        }

        #endregion
    }
}
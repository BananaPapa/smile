using System;
using System.Data.Common;
using System.Drawing;
using System.Linq;
using DevExpress.XtraEditors;

namespace Curriculum.Reports
{
    public partial class TestPassingControl : XtraForm, IAliasesHolder
    {
        private readonly Guid _testGuid;
        private readonly Guid _userGuid;

        public TestPassingControl(Guid testGuid, Guid userGuid)
        {
            _testGuid = testGuid;
            _userGuid = userGuid;
            InitializeComponent();
            InsertData();
        }

        #region IAliasesHolder Members

        public void ApplyAliases()
        {
            try
            {
                Aliase.ApplyAlias( "Ship", a => gridColumnShip.Caption = a );
            }
            catch (DbException)
            {
                //TODO: Handle DB errors.
                throw;
            }
        }

        #endregion

        private void InsertData()
        {
            gridControlTestPassings.Text = (from items in Program.DbSingletone.Persons
                                            where items.ProfessionalUserUniqueId == _userGuid
                                            select items).FirstOrDefault().ToString();

            if (_testGuid != Guid.Empty)
            {
                gridControlTestPassings.DataSource = from items in Program.ProfessionalDb.vStatistics
                                                     where
                                                         items.UserUniqueId == _userGuid &&
                                                         items.TestUniqueId == _testGuid &&
                                                         items.IsSupervise
                                                     select items;
            }
            else
            {
                gridControlTestPassings.DataSource = from items in Program.ProfessionalDb.vStatistics
                                                     where
                                                         items.UserUniqueId == _userGuid
                                                     select items;
            }
        }


        private void EditPersonSimpleButtonClick(object sender, EventArgs e)
        {
            gridControlTestPassings.ShowPrintPreview();
        }

       
    }
}
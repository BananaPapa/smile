﻿using System;
using System.Data.Common;
using System.Linq;
using System.Windows.Forms;

namespace Curriculum.Reports
{
    public partial class ReportDescriptionForm : Form
    {
        private readonly string _reportKey;

        public ReportDescriptionForm(string reportKey)
        {
            InitializeComponent();
            _reportKey = reportKey;
        }

        private void ReportDescriptionFormLoad(object sender, EventArgs e)
        {
            try
            {
                Report report = Program.DbSingletone.Reports.Single(t => t.ReportKey == _reportKey);

                _descriptionMemoEdit.Text = report.Description;
            }
            catch (DbException ex)
            {
                Program.HandleDbException(ex);
            }

            _descriptionMemoEdit.DeselectAll();
        }
    }
}
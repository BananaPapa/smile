﻿using System;
using System.Data.Common;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using Eureca.Integrator.Common;

namespace Curriculum.Reports
{
    public partial class ReportTemplatesForm : FormWithAccessAreas
    {
        private readonly string _reportKey;
        private Report _report;

        public ReportTemplatesForm(string reportKey)
        {
            InitializeComponent();
            this.HierarchyParent = Program.StartForm;
            _reportKey = reportKey;
        }

        public ReportTemplate SelectedTemplate
        {
            get { return _templatesGridView.GetFocusedRow() as ReportTemplate; }
        }

        private void ReportTemplatesFormLoad(object sender, EventArgs e)
        {
            try
            {
                _report = Program.DbSingletone.Reports.Single(r => r.ReportKey == _reportKey);

                if (_report != null && _report.Description != null)
                {
                    _desciptionSimpleButton.Enabled = true;
                }

                InitDataSource();
            }
            catch (DbException ex)
            {
                Program.HandleDbException(ex);

                Close();
                Dispose(true);
            }
            catch (Exception ex)
            {
                Program.HandleUnhandledException(ex);

                Close();
                Dispose(true);
            }
        }

        private void InitDataSource()
        {
            _templatesGridControl.DataSource = from template in Program.DbSingletone.ReportTemplates
                                               where template.ReportKey == _reportKey
                                               select template;
            _templatesGridView.BestFitColumns();
        }

        private void OkSimpleButtonClick(object sender, EventArgs e)
        {
            if (SelectedTemplate != null)
            {
                DialogResult = DialogResult.OK;
                //Close();
            }
            else
            {
                XtraMessageBox.Show(this, "Не выбран шаблон!", "Ошибка", MessageBoxButtons.OK,
                                    MessageBoxIcon.Exclamation);
            }
        }

        private void SaveSimpleButtonClick(object sender, EventArgs e)
        {
            ReportTemplate template = SelectedTemplate;

            if (template != null)
            {
                Reporter.SaveAndOpenTemplate(template);
            }
        }

        private void TemplatesGridViewFocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            _saveSimpleButton.Enabled =
                _replaceSimpleButton.Enabled =
                _deleteSimpleButton.Enabled = _okSimpleButton.Enabled = SelectedTemplate != null;
        }

        private void ReplaceSimpleButtonClick(object sender, EventArgs e)
        {
            ReportTemplate template = SelectedTemplate;

            if (template != null)
            {
                Reporter.ReplaceTemplate(template, this);
            }
        }

        private void DesciptionSimpleButtonClick(object sender, EventArgs e)
        {
            Reporter.ShowReportDescription(_reportKey, this);
        }

        private void AddSimpleButtonClick(object sender, EventArgs e)
        {
            Reporter.AddTemplate(_report, this);

            RefreshData();
        }

        private void DeleteSimpleButtonClick(object sender, EventArgs e)
        {
            ReportTemplate template = SelectedTemplate;

            if (template != null)
            {
                Reporter.DeleteTemplate(template, this);

                RefreshData();
            }
        }

        private void RefreshData()
        {
            Program.DbSingletone.RefreshDataSource(typeof (ReportTemplate));
            InitDataSource();
        }
    }
}
namespace Curriculum.Settings
{
    partial class DaySchedulesControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DaySchedulesControl));
            this._gridControl = new DevExpress.XtraGrid.GridControl();
            this._imageList = new System.Windows.Forms.ImageList();
            this._daySchedulesGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._dayOfWeekGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._scheduleGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._schedulesGridLookUpEdit = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this._shedulesGridLookUpEditView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._scheduleNameGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)(this._gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._daySchedulesGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._schedulesGridLookUpEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._shedulesGridLookUpEditView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // _gridControl
            // 
            this._gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._gridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this._gridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this._gridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this._gridControl.EmbeddedNavigator.Buttons.ImageList = this._imageList;
            this._gridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this._gridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "�������� ������", "Refresh")});
            this._gridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.GridControlEmbeddedNavigatorButtonClick);
            this._gridControl.Location = new System.Drawing.Point(0, 0);
            this._gridControl.MainView = this._daySchedulesGridView;
            this._gridControl.Name = "_gridControl";
            this._gridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this._schedulesGridLookUpEdit});
            this._gridControl.Size = new System.Drawing.Size(534, 467);
            this._gridControl.TabIndex = 0;
            this._gridControl.Tag = "������� ������� ����#r";
            this._gridControl.UseEmbeddedNavigator = true;
            this._gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this._daySchedulesGridView,
            this.gridView2});
            // 
            // _imageList
            // 
            this._imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("_imageList.ImageStream")));
            this._imageList.TransparentColor = System.Drawing.Color.White;
            this._imageList.Images.SetKeyName(0, "Edit16.png");
            this._imageList.Images.SetKeyName(1, "Refresh16.png");
            // 
            // _daySchedulesGridView
            // 
            this._daySchedulesGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._dayOfWeekGridColumn,
            this._scheduleGridColumn});
            this._daySchedulesGridView.GridControl = this._gridControl;
            this._daySchedulesGridView.Name = "_daySchedulesGridView";
            this._daySchedulesGridView.OptionsDetail.EnableMasterViewMode = false;
            this._daySchedulesGridView.OptionsView.ShowGroupPanel = false;
            this._daySchedulesGridView.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.DaySchedulesGridViewValidateRow);
            this._daySchedulesGridView.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.DaySchedulesGridViewRowUpdated);
            this._daySchedulesGridView.CustomColumnDisplayText += new DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventHandler(this.DaySchedulesGridViewCustomColumnDisplayText);
            // 
            // _dayOfWeekGridColumn
            // 
            this._dayOfWeekGridColumn.AppearanceCell.Options.UseTextOptions = true;
            this._dayOfWeekGridColumn.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this._dayOfWeekGridColumn.Caption = "���� ������";
            this._dayOfWeekGridColumn.FieldName = "DayOfWeek";
            this._dayOfWeekGridColumn.Name = "_dayOfWeekGridColumn";
            this._dayOfWeekGridColumn.OptionsColumn.AllowEdit = false;
            this._dayOfWeekGridColumn.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this._dayOfWeekGridColumn.OptionsColumn.FixedWidth = true;
            this._dayOfWeekGridColumn.OptionsColumn.ReadOnly = true;
            this._dayOfWeekGridColumn.Tag = "���� ������#r";
            this._dayOfWeekGridColumn.Visible = true;
            this._dayOfWeekGridColumn.VisibleIndex = 0;
            this._dayOfWeekGridColumn.Width = 111;
            // 
            // _scheduleGridColumn
            // 
            this._scheduleGridColumn.Caption = "����������";
            this._scheduleGridColumn.ColumnEdit = this._schedulesGridLookUpEdit;
            this._scheduleGridColumn.FieldName = "Schedule";
            this._scheduleGridColumn.Name = "_scheduleGridColumn";
            this._scheduleGridColumn.Tag = "����������#r";
            this._scheduleGridColumn.Visible = true;
            this._scheduleGridColumn.VisibleIndex = 1;
            this._scheduleGridColumn.Width = 402;
            // 
            // _schedulesGridLookUpEdit
            // 
            this._schedulesGridLookUpEdit.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this._schedulesGridLookUpEdit.AutoHeight = false;
            this._schedulesGridLookUpEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._schedulesGridLookUpEdit.Name = "_schedulesGridLookUpEdit";
            this._schedulesGridLookUpEdit.NullText = "�� ������";
            this._schedulesGridLookUpEdit.View = this._shedulesGridLookUpEditView;
            // 
            // _shedulesGridLookUpEditView
            // 
            this._shedulesGridLookUpEditView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._scheduleNameGridColumn});
            this._shedulesGridLookUpEditView.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this._shedulesGridLookUpEditView.Name = "_shedulesGridLookUpEditView";
            this._shedulesGridLookUpEditView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this._shedulesGridLookUpEditView.OptionsView.ShowColumnHeaders = false;
            this._shedulesGridLookUpEditView.OptionsView.ShowGroupPanel = false;
            this._shedulesGridLookUpEditView.OptionsView.ShowViewCaption = true;
            this._shedulesGridLookUpEditView.ViewCaption = "����������";
            // 
            // _scheduleNameGridColumn
            // 
            this._scheduleNameGridColumn.Caption = "��������";
            this._scheduleNameGridColumn.FieldName = "Name";
            this._scheduleNameGridColumn.Name = "_scheduleNameGridColumn";
            this._scheduleNameGridColumn.Visible = true;
            this._scheduleNameGridColumn.VisibleIndex = 0;
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this._gridControl;
            this.gridView2.Name = "gridView2";
            // 
            // DaySchedulesControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._gridControl);
            this.Name = "DaySchedulesControl";
            this.Size = new System.Drawing.Size(534, 467);
            this.Tag = "������� ����#r";
            this.Load += new System.EventHandler(this.DaySchedulesControlLoad);
            ((System.ComponentModel.ISupportInitialize)(this._gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._daySchedulesGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._schedulesGridLookUpEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._shedulesGridLookUpEditView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl _gridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView _daySchedulesGridView;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn _dayOfWeekGridColumn;
        private System.Windows.Forms.ImageList _imageList;
        private DevExpress.XtraGrid.Columns.GridColumn _scheduleGridColumn;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit _schedulesGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView _shedulesGridLookUpEditView;
        private DevExpress.XtraGrid.Columns.GridColumn _scheduleNameGridColumn;
    }
}

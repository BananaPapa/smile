﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors.Registrator;
using Eureca.Integrator.Common;
using Eureca.Utility.Extensions;
using MessageBox = Eureca.Integrator.Utility.Forms.MessageBox;

namespace Curriculum.Forms
{
    public partial class PeopleGroupSelectionForms : FormWithAccessAreas
    
    {
        private const string _formTitle = "Редактирование группы: {0}";
        private Binding binding;

        public IEnumerable<string> UnallowedNames { get; set; }

        public IEnumerable<User> SelectedUsers
        {
            get
            {
                return _usersSelectionControl.SelectedUsers;
            }
            set
            {
                _usersSelectionControl.SelectedUsers = value;
            }
        }

        private string _groupName;

        public string GroupName
        {
            get
            {
                return _groupName;
            }
            set
            {
                _groupName = value;
                Text = _formTitle.FormatString(_groupName);
            }
        }
        

        public PeopleGroupSelectionForms(IEnumerable<User> allUsers)
        {
            InitializeComponent( );
            _usersSelectionControl.Init(allUsers);
            binding = new Binding("Text", this, "GroupName",false,DataSourceUpdateMode.OnPropertyChanged);
            _groupNameText.DataBindings.Add( binding );
        }

        private void simpleButton1_Click( object sender, EventArgs e )
        {
            if(ValidateUsersGroup())
                DialogResult = DialogResult.OK;
        }

        private bool ValidateUsersGroup( )
        {
            if(UnallowedNames!= null && UnallowedNames.Contains(GroupName))
            {
                MessageBox.ShowValidationError("Группа с таким именем уже существует",this);
                return false;
            }
            return true;
        }

    }


}

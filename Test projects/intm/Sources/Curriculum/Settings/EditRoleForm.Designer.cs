﻿namespace Curriculum.Forms
{
    partial class EditRoleForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if (disposing && ( components != null ))
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this._saveBtn = new DevExpress.XtraEditors.SimpleButton();
            this._cancelBtn = new DevExpress.XtraEditors.SimpleButton();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this._roleNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this._parentRoleComboBox = new System.Windows.Forms.ComboBox();
            this._accessAreasTreeControl = new Curriculum.Controls.AccessAreasTreeControl();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._roleNameTextEdit.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.Controls.Add(this._saveBtn);
            this.flowLayoutPanel1.Controls.Add(this._cancelBtn);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 688);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(898, 29);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // _saveBtn
            // 
            this._saveBtn.Location = new System.Drawing.Point(813, 3);
            this._saveBtn.Margin = new System.Windows.Forms.Padding(10, 3, 10, 3);
            this._saveBtn.Name = "_saveBtn";
            this._saveBtn.Size = new System.Drawing.Size(75, 23);
            this._saveBtn.TabIndex = 0;
            this._saveBtn.Text = "Сохранить";
            this._saveBtn.Click += new System.EventHandler(this._saveBtn_Click);
            // 
            // _cancelBtn
            // 
            this._cancelBtn.Location = new System.Drawing.Point(718, 3);
            this._cancelBtn.Margin = new System.Windows.Forms.Padding(10, 3, 10, 3);
            this._cancelBtn.Name = "_cancelBtn";
            this._cancelBtn.Size = new System.Drawing.Size(75, 23);
            this._cancelBtn.TabIndex = 1;
            this._cancelBtn.Text = "Отмена";
            this._cancelBtn.Click += new System.EventHandler(this._cancelBtn_Click);
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.IsSplitterFixed = true;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.flowLayoutPanel2);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this._accessAreasTreeControl);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(898, 688);
            this.splitContainerControl1.SplitterPosition = 275;
            this.splitContainerControl1.TabIndex = 1;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.labelControl1);
            this.flowLayoutPanel2.Controls.Add(this._roleNameTextEdit);
            this.flowLayoutPanel2.Controls.Add(this.labelControl2);
            this.flowLayoutPanel2.Controls.Add(this._parentRoleComboBox);
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel2.Margin = new System.Windows.Forms.Padding(20);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Padding = new System.Windows.Forms.Padding(20);
            this.flowLayoutPanel2.Size = new System.Drawing.Size(275, 688);
            this.flowLayoutPanel2.TabIndex = 0;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(23, 23);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(52, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Название:";
            // 
            // _roleNameTextEdit
            // 
            this._roleNameTextEdit.Location = new System.Drawing.Point(23, 42);
            this._roleNameTextEdit.Name = "_roleNameTextEdit";
            this._roleNameTextEdit.Size = new System.Drawing.Size(225, 20);
            this._roleNameTextEdit.TabIndex = 1;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(23, 68);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(99, 13);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "Родительская роль";
            // 
            // _parentRoleComboBox
            // 
            this._parentRoleComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._parentRoleComboBox.FormattingEnabled = true;
            this._parentRoleComboBox.Location = new System.Drawing.Point(23, 87);
            this._parentRoleComboBox.Name = "_parentRoleComboBox";
            this._parentRoleComboBox.Size = new System.Drawing.Size(225, 21);
            this._parentRoleComboBox.TabIndex = 4;
            // 
            // _accessAreasTreeControl
            // 
            this._accessAreasTreeControl.Cursor = System.Windows.Forms.Cursors.Arrow;
            this._accessAreasTreeControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._accessAreasTreeControl.Location = new System.Drawing.Point(0, 0);
            this._accessAreasTreeControl.Name = "_accessAreasTreeControl";
            this._accessAreasTreeControl.Size = new System.Drawing.Size(617, 688);
            this._accessAreasTreeControl.TabIndex = 0;
            // 
            // EditRoleForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(898, 717);
            this.ControlBox = false;
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Name = "EditRoleForm";
            this.Text = "EditRoleForm";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._roleNameTextEdit.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton _saveBtn;
        private DevExpress.XtraEditors.SimpleButton _cancelBtn;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private Controls.AccessAreasTreeControl _accessAreasTreeControl;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit _roleNameTextEdit;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private System.Windows.Forms.ComboBox _parentRoleComboBox;

    }
}
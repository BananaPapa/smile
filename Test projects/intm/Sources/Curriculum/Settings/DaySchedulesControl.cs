using System;
using System.Data.Common;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using Eureca.Integrator.Common;

namespace Curriculum.Settings
{
    /// <summary>
    /// Represents day schedules control.
    /// </summary>
    public partial class DaySchedulesControl : ControlWithAccessAreas
    {
        /// <summary>
        /// Initializes new instance of day schedules control.
        /// </summary>
        public DaySchedulesControl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initializes grid control data source.
        /// </summary>
        private void InitDataSource()
        {
            _gridControl.DataSource = Program.DbSingletone.GetDataSource(typeof (DaySchedule));
            _schedulesGridLookUpEdit.DataSource = Program.DbSingletone.GetDataSource(typeof (Schedule));
        }

        /// <summary>
        /// Refreshes data source.
        /// </summary>
        private void RefreshDataSource()
        {
            Program.DbSingletone.RefreshDataSource(typeof (DaySchedule));
            Program.DbSingletone.RefreshDataSource(typeof (Schedule));

            _daySchedulesGridView.RefreshData();
            _schedulesGridLookUpEdit.View.RefreshData();
        }

        /// <summary>
        /// Handles day schedules control load event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void DaySchedulesControlLoad(object sender, EventArgs e)
        {
            InitDataSource();
        }

        /// <summary>
        /// Grid control embedded navigator button click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param> 
        private void GridControlEmbeddedNavigatorButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    {
                        var tag = e.Button.Tag as string;

                        if (tag != null)
                        {
                            if (tag == "Refresh")
                            {
                                try
                                {
                                    Program.DbSingletone.SubmitChanges();
                                }
                                catch (DbException ex)
                                {
                                    Program.HandleDbException(ex);
                                }
                                finally
                                {
                                    RefreshDataSource();
                                }
                            }
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// Handles day schedules grid view custom column display text event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param> 
        private void DaySchedulesGridViewCustomColumnDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
        {
            if (e.Value != null && e.Value is int)
            {
                e.DisplayText = DaySchedule.GetDayOfWeekName((int) e.Value);
            }
            else
            {
                if (e.Value != null)
                {
                    e.DisplayText = e.Value.ToString();
                }
            }
        }

        /// <summary>
        /// Handles day schedules grid view validate row event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param> 
        private void DaySchedulesGridViewValidateRow(object sender, ValidateRowEventArgs e)
        {
            var daySchedule = _daySchedulesGridView.GetFocusedRow() as DaySchedule;

            if (daySchedule != null && daySchedule.Schedule == null)
            {
                e.ErrorText = "���������� ������� ����������.";
                e.Valid = false;
            }
        }

        private void DaySchedulesGridViewRowUpdated(object sender, RowObjectEventArgs e)
        {
            if (e.Row != null)
            {
                try
                {
                    // Submit changes.
                    Program.DbSingletone.SubmitChanges();
                }
                catch (DbException ex)
                {
                    Program.HandleDbException(ex);
                }
            }
        }
    }
}
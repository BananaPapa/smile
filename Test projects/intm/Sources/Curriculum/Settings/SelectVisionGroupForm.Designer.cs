﻿namespace Curriculum.Settings
{
    partial class SelectVisionGroupForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if (disposing && ( components != null ))
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            this._groupsGridControl = new DevExpress.XtraGrid.GridControl();
            this._groupsView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._groupNameColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this._groupsGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._groupsView)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _groupsGridControl
            // 
            this._groupsGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._groupsGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this._groupsGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this._groupsGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this._groupsGridControl.Location = new System.Drawing.Point(0, 0);
            this._groupsGridControl.MainView = this._groupsView;
            this._groupsGridControl.Name = "_groupsGridControl";
            this._groupsGridControl.Size = new System.Drawing.Size(655, 465);
            this._groupsGridControl.TabIndex = 0;
            this._groupsGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this._groupsView});
            // 
            // _groupsView
            // 
            this._groupsView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._groupNameColumn});
            this._groupsView.GridControl = this._groupsGridControl;
            this._groupsView.Name = "_groupsView";
            this._groupsView.OptionsSelection.MultiSelect = true;
            this._groupsView.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this._groupsView.OptionsView.ShowDetailButtons = false;
            this._groupsView.OptionsView.ShowGroupPanel = false;
            // 
            // _groupNameColumn
            // 
            this._groupNameColumn.Caption = "Название";
            this._groupNameColumn.FieldName = "UserGroupName";
            this._groupNameColumn.Name = "_groupNameColumn";
            this._groupNameColumn.OptionsColumn.AllowEdit = false;
            this._groupNameColumn.OptionsColumn.ReadOnly = true;
            this._groupNameColumn.Visible = true;
            this._groupNameColumn.VisibleIndex = 1;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.Controls.Add(this.simpleButton1);
            this.flowLayoutPanel1.Controls.Add(this.simpleButton2);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 465);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(655, 29);
            this.flowLayoutPanel1.TabIndex = 1;
            // 
            // simpleButton1
            // 
            this.simpleButton1.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.simpleButton1.Location = new System.Drawing.Point(577, 3);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 0;
            this.simpleButton1.Text = "Применить";
            // 
            // simpleButton2
            // 
            this.simpleButton2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButton2.Location = new System.Drawing.Point(496, 3);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 1;
            this.simpleButton2.Text = "Отмена";
            // 
            // SelectVisionGroupForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(655, 494);
            this.ControlBox = false;
            this.Controls.Add(this._groupsGridControl);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Name = "SelectVisionGroupForm";
            this.Text = "Выберите группы пользователей";
            ((System.ComponentModel.ISupportInitialize)(this._groupsGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._groupsView)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl _groupsGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView _groupsView;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraGrid.Columns.GridColumn _groupNameColumn;
    }
}
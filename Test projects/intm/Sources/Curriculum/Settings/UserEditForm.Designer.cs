namespace Curriculum.Settings
{
    partial class UserEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserEditForm));
            this._layoutControl = new DevExpress.XtraLayout.LayoutControl();
            this._groupEditButton = new DevExpress.XtraEditors.ButtonEdit();
            this._positionComboBox = new System.Windows.Forms.ComboBox();
            this._departmentComboBox = new System.Windows.Forms.ComboBox();
            this._roleComboBox = new System.Windows.Forms.ComboBox();
            this._confirmPasswordTextEdit = new DevExpress.XtraEditors.TextEdit();
            this._passwordTextEdit = new DevExpress.XtraEditors.TextEdit();
            this._nameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this._layoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this._nameLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this._passwordLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this._confirmPasswordLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this._okSimpleButton = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this._layoutControl)).BeginInit();
            this._layoutControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._groupEditButton.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._confirmPasswordTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._passwordTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._nameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._layoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._nameLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._passwordLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._confirmPasswordLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // _layoutControl
            // 
            this._layoutControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._layoutControl.Controls.Add(this._groupEditButton);
            this._layoutControl.Controls.Add(this._positionComboBox);
            this._layoutControl.Controls.Add(this._departmentComboBox);
            this._layoutControl.Controls.Add(this._roleComboBox);
            this._layoutControl.Controls.Add(this._confirmPasswordTextEdit);
            this._layoutControl.Controls.Add(this._passwordTextEdit);
            this._layoutControl.Controls.Add(this._nameTextEdit);
            this._layoutControl.Location = new System.Drawing.Point(0, 0);
            this._layoutControl.Name = "_layoutControl";
            this._layoutControl.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(703, 243, 250, 350);
            this._layoutControl.Root = this._layoutControlGroup;
            this._layoutControl.Size = new System.Drawing.Size(375, 193);
            this._layoutControl.TabIndex = 0;
            this._layoutControl.Text = "layoutControl1";
            // 
            // _groupEditButton
            // 
            this._groupEditButton.Location = new System.Drawing.Point(94, 159);
            this._groupEditButton.Name = "_groupEditButton";
            this._groupEditButton.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this._groupEditButton.Properties.ReadOnly = true;
            this._groupEditButton.Size = new System.Drawing.Size(269, 20);
            this._groupEditButton.StyleController = this._layoutControl;
            this._groupEditButton.TabIndex = 11;
            this._groupEditButton.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.buttonEdit1_ButtonClick);
            // 
            // _positionComboBox
            // 
            this._positionComboBox.DisplayMember = "Name";
            this._positionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._positionComboBox.FormattingEnabled = true;
            this._positionComboBox.Location = new System.Drawing.Point(94, 134);
            this._positionComboBox.Name = "_positionComboBox";
            this._positionComboBox.Size = new System.Drawing.Size(269, 21);
            this._positionComboBox.TabIndex = 9;
            this._positionComboBox.ValueMember = "Value";
            // 
            // _departmentComboBox
            // 
            this._departmentComboBox.DisplayMember = "Name";
            this._departmentComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._departmentComboBox.FormattingEnabled = true;
            this._departmentComboBox.Location = new System.Drawing.Point(94, 109);
            this._departmentComboBox.Name = "_departmentComboBox";
            this._departmentComboBox.Size = new System.Drawing.Size(269, 21);
            this._departmentComboBox.TabIndex = 8;
            this._departmentComboBox.ValueMember = "Value";
            // 
            // _roleComboBox
            // 
            this._roleComboBox.DisplayMember = "Name";
            this._roleComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._roleComboBox.FormattingEnabled = true;
            this._roleComboBox.Location = new System.Drawing.Point(94, 84);
            this._roleComboBox.Name = "_roleComboBox";
            this._roleComboBox.Size = new System.Drawing.Size(269, 21);
            this._roleComboBox.TabIndex = 7;
            this._roleComboBox.ValueMember = "Value";
            // 
            // _confirmPasswordTextEdit
            // 
            this._confirmPasswordTextEdit.Location = new System.Drawing.Point(94, 60);
            this._confirmPasswordTextEdit.Name = "_confirmPasswordTextEdit";
            this._confirmPasswordTextEdit.Properties.UseSystemPasswordChar = true;
            this._confirmPasswordTextEdit.Size = new System.Drawing.Size(269, 20);
            this._confirmPasswordTextEdit.StyleController = this._layoutControl;
            this._confirmPasswordTextEdit.TabIndex = 6;
            // 
            // _passwordTextEdit
            // 
            this._passwordTextEdit.Location = new System.Drawing.Point(94, 36);
            this._passwordTextEdit.Name = "_passwordTextEdit";
            this._passwordTextEdit.Properties.UseSystemPasswordChar = true;
            this._passwordTextEdit.Size = new System.Drawing.Size(269, 20);
            this._passwordTextEdit.StyleController = this._layoutControl;
            this._passwordTextEdit.TabIndex = 5;
            // 
            // _nameTextEdit
            // 
            this._nameTextEdit.Location = new System.Drawing.Point(94, 12);
            this._nameTextEdit.Name = "_nameTextEdit";
            this._nameTextEdit.Size = new System.Drawing.Size(269, 20);
            this._nameTextEdit.StyleController = this._layoutControl;
            this._nameTextEdit.TabIndex = 4;
            // 
            // _layoutControlGroup
            // 
            this._layoutControlGroup.CustomizationFormText = "Root";
            this._layoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this._nameLayoutControlItem,
            this._passwordLayoutControlItem,
            this._confirmPasswordLayoutControlItem,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem6});
            this._layoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this._layoutControlGroup.Name = "Root";
            this._layoutControlGroup.OptionsItemText.TextToControlDistance = 6;
            this._layoutControlGroup.Size = new System.Drawing.Size(375, 193);
            this._layoutControlGroup.Text = "Root";
            this._layoutControlGroup.TextVisible = false;
            // 
            // _nameLayoutControlItem
            // 
            this._nameLayoutControlItem.Control = this._nameTextEdit;
            this._nameLayoutControlItem.CustomizationFormText = "���";
            this._nameLayoutControlItem.Location = new System.Drawing.Point(0, 0);
            this._nameLayoutControlItem.Name = "_nameLayoutControlItem";
            this._nameLayoutControlItem.Size = new System.Drawing.Size(355, 24);
            this._nameLayoutControlItem.Tag = "���#r";
            this._nameLayoutControlItem.Text = "���";
            this._nameLayoutControlItem.TextSize = new System.Drawing.Size(76, 13);
            this._nameLayoutControlItem.TextToControlDistance = 6;
            // 
            // _passwordLayoutControlItem
            // 
            this._passwordLayoutControlItem.Control = this._passwordTextEdit;
            this._passwordLayoutControlItem.CustomizationFormText = "������";
            this._passwordLayoutControlItem.Location = new System.Drawing.Point(0, 24);
            this._passwordLayoutControlItem.Name = "_passwordLayoutControlItem";
            this._passwordLayoutControlItem.Size = new System.Drawing.Size(355, 24);
            this._passwordLayoutControlItem.Tag = "������#r";
            this._passwordLayoutControlItem.Text = "������";
            this._passwordLayoutControlItem.TextSize = new System.Drawing.Size(76, 13);
            this._passwordLayoutControlItem.TextToControlDistance = 6;
            // 
            // _confirmPasswordLayoutControlItem
            // 
            this._confirmPasswordLayoutControlItem.Control = this._confirmPasswordTextEdit;
            this._confirmPasswordLayoutControlItem.CustomizationFormText = "������ ������";
            this._confirmPasswordLayoutControlItem.Location = new System.Drawing.Point(0, 48);
            this._confirmPasswordLayoutControlItem.Name = "_confirmPasswordLayoutControlItem";
            this._confirmPasswordLayoutControlItem.Size = new System.Drawing.Size(355, 24);
            this._confirmPasswordLayoutControlItem.Tag = "������ ������#r";
            this._confirmPasswordLayoutControlItem.Text = "������ ������";
            this._confirmPasswordLayoutControlItem.TextSize = new System.Drawing.Size(76, 13);
            this._confirmPasswordLayoutControlItem.TextToControlDistance = 6;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this._roleComboBox;
            this.layoutControlItem2.CustomizationFormText = "����";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(355, 25);
            this.layoutControlItem2.Text = "����";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this._departmentComboBox;
            this.layoutControlItem3.CustomizationFormText = "�����";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 97);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(355, 25);
            this.layoutControlItem3.Text = "�����";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this._positionComboBox;
            this.layoutControlItem4.CustomizationFormText = "��������";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 122);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(355, 25);
            this.layoutControlItem4.Text = "��������";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this._groupEditButton;
            this.layoutControlItem6.CustomizationFormText = "���������";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 147);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(355, 26);
            this.layoutControlItem6.Text = "���������";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this._passwordTextEdit;
            this.layoutControlItem1.CustomizationFormText = "������";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 31);
            this.layoutControlItem1.Name = "_passwordLayoutControlItem";
            this.layoutControlItem1.Size = new System.Drawing.Size(248, 156);
            this.layoutControlItem1.Text = "������";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(37, 20);
            this.layoutControlItem1.TextToControlDistance = 6;
            // 
            // _okSimpleButton
            // 
            this._okSimpleButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._okSimpleButton.Location = new System.Drawing.Point(294, 210);
            this._okSimpleButton.Name = "_okSimpleButton";
            this._okSimpleButton.Size = new System.Drawing.Size(75, 23);
            this._okSimpleButton.TabIndex = 1;
            this._okSimpleButton.Text = "OK";
            this._okSimpleButton.Click += new System.EventHandler(this.OkSimpleButton_Click);
            // 
            // UserEditForm
            // 
            this.AcceptButton = this._okSimpleButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(375, 239);
            this.Controls.Add(this._okSimpleButton);
            this.Controls.Add(this._layoutControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "UserEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "��������� ������������";
            this.Load += new System.EventHandler(this.UserEditForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this._layoutControl)).EndInit();
            this._layoutControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._groupEditButton.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._confirmPasswordTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._passwordTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._nameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._layoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._nameLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._passwordLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._confirmPasswordLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl _layoutControl;
        private DevExpress.XtraEditors.TextEdit _passwordTextEdit;
        private DevExpress.XtraEditors.TextEdit _nameTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup _layoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlItem _nameLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem _passwordLayoutControlItem;
        private DevExpress.XtraEditors.TextEdit _confirmPasswordTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem _confirmPasswordLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.SimpleButton _okSimpleButton;
        private System.Windows.Forms.ComboBox _roleComboBox;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private System.Windows.Forms.ComboBox _positionComboBox;
        private System.Windows.Forms.ComboBox _departmentComboBox;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.ButtonEdit _groupEditButton;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;

    }
}
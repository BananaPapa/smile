namespace Curriculum.Settings
{
    partial class NameOwnersControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode2 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode3 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode4 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode5 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode6 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode7 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode8 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode9 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode10 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode11 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode12 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode13 = new DevExpress.XtraGrid.GridLevelNode();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NameOwnersControl));
            this._shipsGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._shipNameGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._shipIndexGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._shipRiverGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._shipLineGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._shipConditionsGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._gridControl = new DevExpress.XtraGrid.GridControl();
            this._imageList = new System.Windows.Forms.ImageList(this.components);
            this._shipToolsGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._shipToolShipGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._shipToolGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._shipToolCategoryGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._shipToolSubCategoryGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._personsBandedGridView = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this._privateInfoGridBand = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this._personNameBandedGridColumn = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this._personBirthDateBandedGridColumn = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this._personQualificationInfoGridBand = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this._personQualificationBandedGridColumn = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this._personEducationBandedGridColumn = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this._personDegreeBandedGridColumn = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this._personCadreInfoGridBand = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this._personPositionBandedGridColumn = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this._personDepartmentBandedGridColumn = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this._personAffiliationBandedGridColumn = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this._personOtherInfoGridBand = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this._personCategoryBandedGridColumn = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this._personStatusBandedGridColumn = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this._lettersGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._letterAddresseeGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._letterCodeGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._letterDateGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._coursesGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._courseShipGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._courseCategoryGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._courseDisciplineGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._courseLessonTypeGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._courseDurationGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._lessonsGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._lessonPersonGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._lessonDisciplineGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._lessonLessonTypeGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._lessonDateTimeGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._lessonDurationGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._lessonCommentsGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._personPhonesGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._personPhonePersonGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._personPhoneNumberGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._personPhoneTypeGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._disciplinesGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._disciplineNameGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._disciplineUnitGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._disciplinePartGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._partsGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._partNameGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._partUnitGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._responsibilityZonePhonesGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._responsibilityZonePhoneZoneGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._responsibilityZonePhoneTypeGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._responsibilityZonePhoneNumberGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._templateCoursesGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._templateCoursesTemplateGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._templateCoursesDisciplineGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._templateCoursesLessonTypeGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._templateCoursesCategoryGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._templateCoursesDurationGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._userPositionGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._userPositionGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._userDepartmentsGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._userDepartmentGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._nameOwnersGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._nameGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._isTestGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._isTestCheckEdit = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this._createProbationerGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._createProbationerCheckEdit = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this._operationsGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._OperationName = new DevExpress.XtraGrid.Columns.GridColumn();
            this._stateGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this._shipsGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._shipToolsGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._personsBandedGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._lettersGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._coursesGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._lessonsGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._personPhonesGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._disciplinesGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._partsGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._responsibilityZonePhonesGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._templateCoursesGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._userPositionGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._userDepartmentsGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._nameOwnersGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._isTestCheckEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._createProbationerCheckEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._operationsGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._stateGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // _shipsGridView
            // 
            this._shipsGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._shipNameGridColumn,
            this._shipIndexGridColumn,
            this._shipRiverGridColumn,
            this._shipLineGridColumn,
            this._shipConditionsGridColumn});
            this._shipsGridView.GridControl = this._gridControl;
            this._shipsGridView.Name = "_shipsGridView";
            this._shipsGridView.OptionsBehavior.Editable = false;
            this._shipsGridView.OptionsDetail.EnableMasterViewMode = false;
            this._shipsGridView.OptionsView.ShowGroupPanel = false;
            this._shipsGridView.Tag = "�������#r";
            this._shipsGridView.ViewCaption = "�������";
            // 
            // _shipNameGridColumn
            // 
            this._shipNameGridColumn.Caption = "��������";
            this._shipNameGridColumn.FieldName = "Name";
            this._shipNameGridColumn.Name = "_shipNameGridColumn";
            this._shipNameGridColumn.Tag = "��������#r";
            this._shipNameGridColumn.Visible = true;
            this._shipNameGridColumn.VisibleIndex = 0;
            // 
            // _shipIndexGridColumn
            // 
            this._shipIndexGridColumn.Caption = "������";
            this._shipIndexGridColumn.FieldName = "Index";
            this._shipIndexGridColumn.Name = "_shipIndexGridColumn";
            this._shipIndexGridColumn.Tag = "������#r";
            this._shipIndexGridColumn.Visible = true;
            this._shipIndexGridColumn.VisibleIndex = 1;
            // 
            // _shipRiverGridColumn
            // 
            this._shipRiverGridColumn.Caption = "����";
            this._shipRiverGridColumn.FieldName = "River";
            this._shipRiverGridColumn.Name = "_shipRiverGridColumn";
            this._shipRiverGridColumn.Tag = "����#r";
            this._shipRiverGridColumn.Visible = true;
            this._shipRiverGridColumn.VisibleIndex = 2;
            // 
            // _shipLineGridColumn
            // 
            this._shipLineGridColumn.Caption = "�����������";
            this._shipLineGridColumn.FieldName = "ShipLine";
            this._shipLineGridColumn.Name = "_shipLineGridColumn";
            this._shipLineGridColumn.Tag = "�����������#r";
            this._shipLineGridColumn.Visible = true;
            this._shipLineGridColumn.VisibleIndex = 3;
            // 
            // _shipConditionsGridColumn
            // 
            this._shipConditionsGridColumn.Caption = "�������";
            this._shipConditionsGridColumn.FieldName = "Condition";
            this._shipConditionsGridColumn.Name = "_shipConditionsGridColumn";
            this._shipConditionsGridColumn.Tag = "�������#r";
            this._shipConditionsGridColumn.Visible = true;
            this._shipConditionsGridColumn.VisibleIndex = 4;
            // 
            // _gridControl
            // 
            this._gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._gridControl.EmbeddedNavigator.Buttons.Append.Tag = "������� ������������#ra";
            this._gridControl.EmbeddedNavigator.Buttons.CancelEdit.Tag = "������� ������������#rc";
            this._gridControl.EmbeddedNavigator.Buttons.Edit.Tag = "������� ������������#rc";
            this._gridControl.EmbeddedNavigator.Buttons.EndEdit.Tag = "������� ������������#rc";
            this._gridControl.EmbeddedNavigator.Buttons.ImageList = this._imageList;
            this._gridControl.EmbeddedNavigator.Buttons.Remove.Tag = "������� ������������#rd";
            this._gridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "�������� ������", "Refresh")});
            this._gridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.GridControl_EmbeddedNavigator_ButtonClick);
            gridLevelNode1.LevelTemplate = this._shipsGridView;
            gridLevelNode1.RelationName = "Ships";
            gridLevelNode2.LevelTemplate = this._shipToolsGridView;
            gridLevelNode2.RelationName = "ShipTools";
            gridLevelNode3.LevelTemplate = this._personsBandedGridView;
            gridLevelNode3.RelationName = "Persons";
            gridLevelNode4.LevelTemplate = this._lettersGridView;
            gridLevelNode4.RelationName = "Letters";
            gridLevelNode5.LevelTemplate = this._coursesGridView;
            gridLevelNode5.RelationName = "Courses";
            gridLevelNode6.LevelTemplate = this._lessonsGridView;
            gridLevelNode6.RelationName = "Lessons";
            gridLevelNode7.LevelTemplate = this._personPhonesGridView;
            gridLevelNode7.RelationName = "PersonPhones";
            gridLevelNode8.LevelTemplate = this._disciplinesGridView;
            gridLevelNode8.RelationName = "Disciplines";
            gridLevelNode9.LevelTemplate = this._partsGridView;
            gridLevelNode9.RelationName = "Parts";
            gridLevelNode10.LevelTemplate = this._responsibilityZonePhonesGridView;
            gridLevelNode10.RelationName = "ResponsibilityZonePhones";
            gridLevelNode11.LevelTemplate = this._templateCoursesGridView;
            gridLevelNode11.RelationName = "TemplateCourses";
            gridLevelNode12.LevelTemplate = this._userPositionGridView;
            gridLevelNode12.RelationName = "UserPositionsUsers";
            gridLevelNode13.LevelTemplate = this._userDepartmentsGridView;
            gridLevelNode13.RelationName = "UsersDepartmentsUsers";
            this._gridControl.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1,
            gridLevelNode2,
            gridLevelNode3,
            gridLevelNode4,
            gridLevelNode5,
            gridLevelNode6,
            gridLevelNode7,
            gridLevelNode8,
            gridLevelNode9,
            gridLevelNode10,
            gridLevelNode11,
            gridLevelNode12,
            gridLevelNode13});
            this._gridControl.Location = new System.Drawing.Point(0, 0);
            this._gridControl.MainView = this._nameOwnersGridView;
            this._gridControl.Name = "_gridControl";
            this._gridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this._isTestCheckEdit,
            this._createProbationerCheckEdit});
            this._gridControl.Size = new System.Drawing.Size(579, 523);
            this._gridControl.TabIndex = 1;
            this._gridControl.Tag = "������� ������������#r";
            this._gridControl.UseEmbeddedNavigator = true;
            this._gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this._shipToolsGridView,
            this._personsBandedGridView,
            this._lettersGridView,
            this._coursesGridView,
            this._lessonsGridView,
            this._personPhonesGridView,
            this._disciplinesGridView,
            this._partsGridView,
            this._responsibilityZonePhonesGridView,
            this._templateCoursesGridView,
            this._userPositionGridView,
            this._userDepartmentsGridView,
            this._nameOwnersGridView,
            this._operationsGridView,
            this._stateGridView,
            this._shipsGridView});
            this._gridControl.FocusedViewChanged += new DevExpress.XtraGrid.ViewFocusEventHandler(this.GridControl_FocusedViewChanged);
            // 
            // _imageList
            // 
            this._imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("_imageList.ImageStream")));
            this._imageList.TransparentColor = System.Drawing.Color.White;
            this._imageList.Images.SetKeyName(0, "Refresh16.png");
            this._imageList.Images.SetKeyName(1, "Ship16.png");
            this._imageList.Images.SetKeyName(2, "User16.png");
            this._imageList.Images.SetKeyName(3, "Date16.png");
            this._imageList.Images.SetKeyName(4, "Discipline16.png");
            this._imageList.Images.SetKeyName(5, "Templates16.png");
            // 
            // _shipToolsGridView
            // 
            this._shipToolsGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._shipToolShipGridColumn,
            this._shipToolGridColumn,
            this._shipToolCategoryGridColumn,
            this._shipToolSubCategoryGridColumn});
            this._shipToolsGridView.GridControl = this._gridControl;
            this._shipToolsGridView.Images = this._imageList;
            this._shipToolsGridView.Name = "_shipToolsGridView";
            this._shipToolsGridView.OptionsBehavior.Editable = false;
            this._shipToolsGridView.OptionsDetail.EnableMasterViewMode = false;
            this._shipToolsGridView.OptionsView.ShowGroupPanel = false;
            this._shipToolsGridView.Tag = "��������#r";
            this._shipToolsGridView.ViewCaption = "��������";
            // 
            // _shipToolShipGridColumn
            // 
            this._shipToolShipGridColumn.Caption = "�������";
            this._shipToolShipGridColumn.FieldName = "Ship";
            this._shipToolShipGridColumn.ImageIndex = 1;
            this._shipToolShipGridColumn.Name = "_shipToolShipGridColumn";
            this._shipToolShipGridColumn.Tag = "�������#r";
            this._shipToolShipGridColumn.Visible = true;
            this._shipToolShipGridColumn.VisibleIndex = 0;
            // 
            // _shipToolGridColumn
            // 
            this._shipToolGridColumn.Caption = "��������";
            this._shipToolGridColumn.FieldName = "Tool";
            this._shipToolGridColumn.Name = "_shipToolGridColumn";
            this._shipToolGridColumn.Tag = "��������#r";
            this._shipToolGridColumn.Visible = true;
            this._shipToolGridColumn.VisibleIndex = 1;
            // 
            // _shipToolCategoryGridColumn
            // 
            this._shipToolCategoryGridColumn.Caption = "���������";
            this._shipToolCategoryGridColumn.FieldName = "ToolCategory";
            this._shipToolCategoryGridColumn.Name = "_shipToolCategoryGridColumn";
            this._shipToolCategoryGridColumn.Tag = "���������#r";
            this._shipToolCategoryGridColumn.Visible = true;
            this._shipToolCategoryGridColumn.VisibleIndex = 2;
            // 
            // _shipToolSubCategoryGridColumn
            // 
            this._shipToolSubCategoryGridColumn.Caption = "������������";
            this._shipToolSubCategoryGridColumn.FieldName = "ToolSubCategory";
            this._shipToolSubCategoryGridColumn.Name = "_shipToolSubCategoryGridColumn";
            this._shipToolSubCategoryGridColumn.Tag = "������������#r";
            this._shipToolSubCategoryGridColumn.Visible = true;
            this._shipToolSubCategoryGridColumn.VisibleIndex = 3;
            // 
            // _personsBandedGridView
            // 
            this._personsBandedGridView.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this._privateInfoGridBand,
            this._personQualificationInfoGridBand,
            this._personCadreInfoGridBand,
            this._personOtherInfoGridBand});
            this._personsBandedGridView.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this._personNameBandedGridColumn,
            this._personBirthDateBandedGridColumn,
            this._personQualificationBandedGridColumn,
            this._personEducationBandedGridColumn,
            this._personDegreeBandedGridColumn,
            this._personPositionBandedGridColumn,
            this._personDepartmentBandedGridColumn,
            this._personAffiliationBandedGridColumn,
            this._personStatusBandedGridColumn,
            this._personCategoryBandedGridColumn});
            this._personsBandedGridView.CustomizationFormBounds = new System.Drawing.Rectangle(1053, 651, 217, 199);
            this._personsBandedGridView.GridControl = this._gridControl;
            this._personsBandedGridView.Images = this._imageList;
            this._personsBandedGridView.Name = "_personsBandedGridView";
            this._personsBandedGridView.OptionsBehavior.Editable = false;
            this._personsBandedGridView.OptionsDetail.EnableMasterViewMode = false;
            this._personsBandedGridView.OptionsView.ShowGroupPanel = false;
            this._personsBandedGridView.Tag = "��������#r";
            this._personsBandedGridView.ViewCaption = "��������";
            // 
            // _privateInfoGridBand
            // 
            this._privateInfoGridBand.Caption = "������ ������";
            this._privateInfoGridBand.Columns.Add(this._personNameBandedGridColumn);
            this._privateInfoGridBand.Columns.Add(this._personBirthDateBandedGridColumn);
            this._privateInfoGridBand.MinWidth = 20;
            this._privateInfoGridBand.Name = "_privateInfoGridBand";
            this._privateInfoGridBand.VisibleIndex = 0;
            this._privateInfoGridBand.Width = 282;
            // 
            // _personNameBandedGridColumn
            // 
            this._personNameBandedGridColumn.Caption = "���";
            this._personNameBandedGridColumn.FieldName = "Name";
            this._personNameBandedGridColumn.Name = "_personNameBandedGridColumn";
            this._personNameBandedGridColumn.Tag = "���#r";
            this._personNameBandedGridColumn.Visible = true;
            this._personNameBandedGridColumn.Width = 150;
            // 
            // _personBirthDateBandedGridColumn
            // 
            this._personBirthDateBandedGridColumn.Caption = "���� ��������";
            this._personBirthDateBandedGridColumn.FieldName = "BirthDate";
            this._personBirthDateBandedGridColumn.ImageIndex = 3;
            this._personBirthDateBandedGridColumn.Name = "_personBirthDateBandedGridColumn";
            this._personBirthDateBandedGridColumn.Tag = "���� ��������#r";
            this._personBirthDateBandedGridColumn.Visible = true;
            this._personBirthDateBandedGridColumn.Width = 132;
            // 
            // _personQualificationInfoGridBand
            // 
            this._personQualificationInfoGridBand.Caption = "���������������� ������";
            this._personQualificationInfoGridBand.Columns.Add(this._personQualificationBandedGridColumn);
            this._personQualificationInfoGridBand.Columns.Add(this._personEducationBandedGridColumn);
            this._personQualificationInfoGridBand.Columns.Add(this._personDegreeBandedGridColumn);
            this._personQualificationInfoGridBand.MinWidth = 20;
            this._personQualificationInfoGridBand.Name = "_personQualificationInfoGridBand";
            this._personQualificationInfoGridBand.Visible = false;
            this._personQualificationInfoGridBand.VisibleIndex = -1;
            this._personQualificationInfoGridBand.Width = 310;
            // 
            // _personQualificationBandedGridColumn
            // 
            this._personQualificationBandedGridColumn.Caption = "������������";
            this._personQualificationBandedGridColumn.FieldName = "Qualification";
            this._personQualificationBandedGridColumn.Name = "_personQualificationBandedGridColumn";
            this._personQualificationBandedGridColumn.Tag = "������������#r";
            this._personQualificationBandedGridColumn.Visible = true;
            this._personQualificationBandedGridColumn.Width = 100;
            // 
            // _personEducationBandedGridColumn
            // 
            this._personEducationBandedGridColumn.Caption = "�����������";
            this._personEducationBandedGridColumn.FieldName = "Education";
            this._personEducationBandedGridColumn.Name = "_personEducationBandedGridColumn";
            this._personEducationBandedGridColumn.Tag = "�����������#r";
            this._personEducationBandedGridColumn.Visible = true;
            this._personEducationBandedGridColumn.Width = 100;
            // 
            // _personDegreeBandedGridColumn
            // 
            this._personDegreeBandedGridColumn.Caption = "������ �������";
            this._personDegreeBandedGridColumn.FieldName = "Degree";
            this._personDegreeBandedGridColumn.Name = "_personDegreeBandedGridColumn";
            this._personDegreeBandedGridColumn.Tag = "������ �������#r";
            this._personDegreeBandedGridColumn.Visible = true;
            this._personDegreeBandedGridColumn.Width = 110;
            // 
            // _personCadreInfoGridBand
            // 
            this._personCadreInfoGridBand.Caption = "�������� ����������";
            this._personCadreInfoGridBand.Columns.Add(this._personPositionBandedGridColumn);
            this._personCadreInfoGridBand.Columns.Add(this._personDepartmentBandedGridColumn);
            this._personCadreInfoGridBand.Columns.Add(this._personAffiliationBandedGridColumn);
            this._personCadreInfoGridBand.MinWidth = 20;
            this._personCadreInfoGridBand.Name = "_personCadreInfoGridBand";
            this._personCadreInfoGridBand.Visible = false;
            this._personCadreInfoGridBand.VisibleIndex = -1;
            this._personCadreInfoGridBand.Width = 301;
            // 
            // _personPositionBandedGridColumn
            // 
            this._personPositionBandedGridColumn.Caption = "���������";
            this._personPositionBandedGridColumn.FieldName = "Position";
            this._personPositionBandedGridColumn.Name = "_personPositionBandedGridColumn";
            this._personPositionBandedGridColumn.Tag = "���������#r";
            this._personPositionBandedGridColumn.Visible = true;
            this._personPositionBandedGridColumn.Width = 99;
            // 
            // _personDepartmentBandedGridColumn
            // 
            this._personDepartmentBandedGridColumn.Caption = "���������";
            this._personDepartmentBandedGridColumn.FieldName = "Department";
            this._personDepartmentBandedGridColumn.Name = "_personDepartmentBandedGridColumn";
            this._personDepartmentBandedGridColumn.Tag = "���������#r";
            this._personDepartmentBandedGridColumn.Visible = true;
            this._personDepartmentBandedGridColumn.Width = 99;
            // 
            // _personAffiliationBandedGridColumn
            // 
            this._personAffiliationBandedGridColumn.Caption = "��������������";
            this._personAffiliationBandedGridColumn.FieldName = "Affiliation";
            this._personAffiliationBandedGridColumn.Name = "_personAffiliationBandedGridColumn";
            this._personAffiliationBandedGridColumn.Tag = "��������������#r";
            this._personAffiliationBandedGridColumn.Visible = true;
            this._personAffiliationBandedGridColumn.Width = 103;
            // 
            // _personOtherInfoGridBand
            // 
            this._personOtherInfoGridBand.Columns.Add(this._personCategoryBandedGridColumn);
            this._personOtherInfoGridBand.Columns.Add(this._personStatusBandedGridColumn);
            this._personOtherInfoGridBand.MinWidth = 20;
            this._personOtherInfoGridBand.Name = "_personOtherInfoGridBand";
            this._personOtherInfoGridBand.Visible = false;
            this._personOtherInfoGridBand.VisibleIndex = -1;
            this._personOtherInfoGridBand.Width = 195;
            // 
            // _personCategoryBandedGridColumn
            // 
            this._personCategoryBandedGridColumn.Caption = "���������";
            this._personCategoryBandedGridColumn.FieldName = "Category";
            this._personCategoryBandedGridColumn.Name = "_personCategoryBandedGridColumn";
            this._personCategoryBandedGridColumn.Width = 64;
            // 
            // _personStatusBandedGridColumn
            // 
            this._personStatusBandedGridColumn.Caption = "������";
            this._personStatusBandedGridColumn.FieldName = "Statuses";
            this._personStatusBandedGridColumn.Name = "_personStatusBandedGridColumn";
            this._personStatusBandedGridColumn.Width = 67;
            // 
            // _lettersGridView
            // 
            this._lettersGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._letterAddresseeGridColumn,
            this._letterCodeGridColumn,
            this._letterDateGridColumn});
            this._lettersGridView.GridControl = this._gridControl;
            this._lettersGridView.Images = this._imageList;
            this._lettersGridView.Name = "_lettersGridView";
            this._lettersGridView.OptionsBehavior.Editable = false;
            this._lettersGridView.OptionsDetail.EnableMasterViewMode = false;
            this._lettersGridView.OptionsView.ShowGroupPanel = false;
            this._lettersGridView.Tag = "������#r";
            this._lettersGridView.ViewCaption = "������";
            // 
            // _letterAddresseeGridColumn
            // 
            this._letterAddresseeGridColumn.Caption = "�������";
            this._letterAddresseeGridColumn.FieldName = "Addressee";
            this._letterAddresseeGridColumn.Name = "_letterAddresseeGridColumn";
            this._letterAddresseeGridColumn.Tag = "�������#r";
            this._letterAddresseeGridColumn.Visible = true;
            this._letterAddresseeGridColumn.VisibleIndex = 0;
            // 
            // _letterCodeGridColumn
            // 
            this._letterCodeGridColumn.Caption = "�";
            this._letterCodeGridColumn.FieldName = "Code";
            this._letterCodeGridColumn.Name = "_letterCodeGridColumn";
            this._letterCodeGridColumn.Tag = "�����#r";
            this._letterCodeGridColumn.Visible = true;
            this._letterCodeGridColumn.VisibleIndex = 1;
            // 
            // _letterDateGridColumn
            // 
            this._letterDateGridColumn.Caption = "����";
            this._letterDateGridColumn.FieldName = "Date";
            this._letterDateGridColumn.ImageIndex = 3;
            this._letterDateGridColumn.Name = "_letterDateGridColumn";
            this._letterDateGridColumn.Tag = "����#r";
            this._letterDateGridColumn.Visible = true;
            this._letterDateGridColumn.VisibleIndex = 2;
            // 
            // _coursesGridView
            // 
            this._coursesGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._courseShipGridColumn,
            this._courseCategoryGridColumn,
            this._courseDisciplineGridColumn,
            this._courseLessonTypeGridColumn,
            this._courseDurationGridColumn});
            this._coursesGridView.GridControl = this._gridControl;
            this._coursesGridView.GroupCount = 3;
            this._coursesGridView.Images = this._imageList;
            this._coursesGridView.Name = "_coursesGridView";
            this._coursesGridView.OptionsBehavior.Editable = false;
            this._coursesGridView.OptionsDetail.EnableMasterViewMode = false;
            this._coursesGridView.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this._courseShipGridColumn, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this._courseDisciplineGridColumn, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this._courseCategoryGridColumn, DevExpress.Data.ColumnSortOrder.Ascending)});
            this._coursesGridView.Tag = "�����#r";
            this._coursesGridView.ViewCaption = "�����";
            // 
            // _courseShipGridColumn
            // 
            this._courseShipGridColumn.Caption = "�������";
            this._courseShipGridColumn.FieldName = "Ship";
            this._courseShipGridColumn.ImageIndex = 1;
            this._courseShipGridColumn.Name = "_courseShipGridColumn";
            // 
            // _courseCategoryGridColumn
            // 
            this._courseCategoryGridColumn.Caption = "���������";
            this._courseCategoryGridColumn.FieldName = "Category";
            this._courseCategoryGridColumn.Name = "_courseCategoryGridColumn";
            this._courseCategoryGridColumn.Tag = "���������#r";
            this._courseCategoryGridColumn.Visible = true;
            this._courseCategoryGridColumn.VisibleIndex = 0;
            this._courseCategoryGridColumn.Width = 77;
            // 
            // _courseDisciplineGridColumn
            // 
            this._courseDisciplineGridColumn.Caption = "����";
            this._courseDisciplineGridColumn.FieldName = "Discipline";
            this._courseDisciplineGridColumn.ImageIndex = 4;
            this._courseDisciplineGridColumn.Name = "_courseDisciplineGridColumn";
            // 
            // _courseLessonTypeGridColumn
            // 
            this._courseLessonTypeGridColumn.Caption = "��� �������";
            this._courseLessonTypeGridColumn.FieldName = "LessonType";
            this._courseLessonTypeGridColumn.Name = "_courseLessonTypeGridColumn";
            this._courseLessonTypeGridColumn.Tag = "��� �������#r";
            this._courseLessonTypeGridColumn.Visible = true;
            this._courseLessonTypeGridColumn.VisibleIndex = 0;
            this._courseLessonTypeGridColumn.Width = 77;
            // 
            // _courseDurationGridColumn
            // 
            this._courseDurationGridColumn.Caption = "������������ (���)";
            this._courseDurationGridColumn.FieldName = "Duration";
            this._courseDurationGridColumn.Name = "_courseDurationGridColumn";
            this._courseDurationGridColumn.Tag = "������������#r";
            this._courseDurationGridColumn.Visible = true;
            this._courseDurationGridColumn.VisibleIndex = 1;
            // 
            // _lessonsGridView
            // 
            this._lessonsGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._lessonPersonGridColumn,
            this._lessonDisciplineGridColumn,
            this._lessonLessonTypeGridColumn,
            this._lessonDateTimeGridColumn,
            this._lessonDurationGridColumn,
            this._lessonCommentsGridColumn});
            this._lessonsGridView.GridControl = this._gridControl;
            this._lessonsGridView.GroupCount = 3;
            this._lessonsGridView.Images = this._imageList;
            this._lessonsGridView.Name = "_lessonsGridView";
            this._lessonsGridView.OptionsBehavior.Editable = false;
            this._lessonsGridView.OptionsDetail.EnableMasterViewMode = false;
            this._lessonsGridView.OptionsView.RowAutoHeight = true;
            this._lessonsGridView.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this._lessonPersonGridColumn, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this._lessonDisciplineGridColumn, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this._lessonLessonTypeGridColumn, DevExpress.Data.ColumnSortOrder.Ascending)});
            this._lessonsGridView.Tag = "�������#r";
            this._lessonsGridView.ViewCaption = "�������";
            // 
            // _lessonPersonGridColumn
            // 
            this._lessonPersonGridColumn.Caption = "���������";
            this._lessonPersonGridColumn.FieldName = "Person";
            this._lessonPersonGridColumn.ImageIndex = 2;
            this._lessonPersonGridColumn.Name = "_lessonPersonGridColumn";
            this._lessonPersonGridColumn.Tag = "���������#r";
            this._lessonPersonGridColumn.Visible = true;
            this._lessonPersonGridColumn.VisibleIndex = 0;
            // 
            // _lessonDisciplineGridColumn
            // 
            this._lessonDisciplineGridColumn.Caption = "����";
            this._lessonDisciplineGridColumn.FieldName = "Discipline";
            this._lessonDisciplineGridColumn.ImageIndex = 4;
            this._lessonDisciplineGridColumn.Name = "_lessonDisciplineGridColumn";
            this._lessonDisciplineGridColumn.Tag = "����#r";
            this._lessonDisciplineGridColumn.Visible = true;
            this._lessonDisciplineGridColumn.VisibleIndex = 0;
            // 
            // _lessonLessonTypeGridColumn
            // 
            this._lessonLessonTypeGridColumn.Caption = "��� �������";
            this._lessonLessonTypeGridColumn.FieldName = "LessonType";
            this._lessonLessonTypeGridColumn.Name = "_lessonLessonTypeGridColumn";
            this._lessonLessonTypeGridColumn.Tag = "��� �������#r";
            this._lessonLessonTypeGridColumn.Visible = true;
            this._lessonLessonTypeGridColumn.VisibleIndex = 0;
            // 
            // _lessonDateTimeGridColumn
            // 
            this._lessonDateTimeGridColumn.Caption = "����";
            this._lessonDateTimeGridColumn.FieldName = "Datetime";
            this._lessonDateTimeGridColumn.ImageIndex = 3;
            this._lessonDateTimeGridColumn.Name = "_lessonDateTimeGridColumn";
            this._lessonDateTimeGridColumn.Tag = "����#r";
            this._lessonDateTimeGridColumn.Visible = true;
            this._lessonDateTimeGridColumn.VisibleIndex = 0;
            this._lessonDateTimeGridColumn.Width = 77;
            // 
            // _lessonDurationGridColumn
            // 
            this._lessonDurationGridColumn.Caption = "������������ (���)";
            this._lessonDurationGridColumn.FieldName = "Duration";
            this._lessonDurationGridColumn.Name = "_lessonDurationGridColumn";
            this._lessonDurationGridColumn.Tag = "������������#r";
            this._lessonDurationGridColumn.Visible = true;
            this._lessonDurationGridColumn.VisibleIndex = 1;
            // 
            // _lessonCommentsGridColumn
            // 
            this._lessonCommentsGridColumn.Caption = "����������";
            this._lessonCommentsGridColumn.FieldName = "CommentText";
            this._lessonCommentsGridColumn.Name = "_lessonCommentsGridColumn";
            this._lessonCommentsGridColumn.Tag = "����������#r";
            this._lessonCommentsGridColumn.Visible = true;
            this._lessonCommentsGridColumn.VisibleIndex = 2;
            // 
            // _personPhonesGridView
            // 
            this._personPhonesGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._personPhonePersonGridColumn,
            this._personPhoneNumberGridColumn,
            this._personPhoneTypeGridColumn});
            this._personPhonesGridView.GridControl = this._gridControl;
            this._personPhonesGridView.Name = "_personPhonesGridView";
            this._personPhonesGridView.OptionsBehavior.Editable = false;
            this._personPhonesGridView.OptionsDetail.EnableMasterViewMode = false;
            this._personPhonesGridView.Tag = "�������� ���������#r";
            this._personPhonesGridView.ViewCaption = "�������� ���������";
            // 
            // _personPhonePersonGridColumn
            // 
            this._personPhonePersonGridColumn.Caption = "���";
            this._personPhonePersonGridColumn.FieldName = "Person";
            this._personPhonePersonGridColumn.Name = "_personPhonePersonGridColumn";
            this._personPhonePersonGridColumn.Tag = "���#r";
            this._personPhonePersonGridColumn.Visible = true;
            this._personPhonePersonGridColumn.VisibleIndex = 0;
            // 
            // _personPhoneNumberGridColumn
            // 
            this._personPhoneNumberGridColumn.Caption = "�����";
            this._personPhoneNumberGridColumn.FieldName = "Number";
            this._personPhoneNumberGridColumn.Name = "_personPhoneNumberGridColumn";
            this._personPhoneNumberGridColumn.Tag = "�����#r";
            this._personPhoneNumberGridColumn.Visible = true;
            this._personPhoneNumberGridColumn.VisibleIndex = 1;
            // 
            // _personPhoneTypeGridColumn
            // 
            this._personPhoneTypeGridColumn.Caption = "���";
            this._personPhoneTypeGridColumn.FieldName = "PhoneType";
            this._personPhoneTypeGridColumn.Name = "_personPhoneTypeGridColumn";
            this._personPhoneTypeGridColumn.Tag = "���#r";
            this._personPhoneTypeGridColumn.Visible = true;
            this._personPhoneTypeGridColumn.VisibleIndex = 2;
            // 
            // _disciplinesGridView
            // 
            this._disciplinesGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._disciplineNameGridColumn,
            this._disciplineUnitGridColumn,
            this._disciplinePartGridColumn});
            this._disciplinesGridView.GridControl = this._gridControl;
            this._disciplinesGridView.Name = "_disciplinesGridView";
            this._disciplinesGridView.OptionsBehavior.Editable = false;
            this._disciplinesGridView.OptionsDetail.EnableMasterViewMode = false;
            this._disciplinesGridView.Tag = "����#r";
            this._disciplinesGridView.ViewCaption = "����";
            // 
            // _disciplineNameGridColumn
            // 
            this._disciplineNameGridColumn.Caption = "��������";
            this._disciplineNameGridColumn.FieldName = "Name";
            this._disciplineNameGridColumn.Name = "_disciplineNameGridColumn";
            this._disciplineNameGridColumn.Tag = "����#r";
            this._disciplineNameGridColumn.Visible = true;
            this._disciplineNameGridColumn.VisibleIndex = 0;
            // 
            // _disciplineUnitGridColumn
            // 
            this._disciplineUnitGridColumn.Caption = "������";
            this._disciplineUnitGridColumn.FieldName = "Unit";
            this._disciplineUnitGridColumn.Name = "_disciplineUnitGridColumn";
            this._disciplineUnitGridColumn.Tag = "������#r";
            this._disciplineUnitGridColumn.Visible = true;
            this._disciplineUnitGridColumn.VisibleIndex = 2;
            // 
            // _disciplinePartGridColumn
            // 
            this._disciplinePartGridColumn.Caption = "�����";
            this._disciplinePartGridColumn.FieldName = "Part";
            this._disciplinePartGridColumn.Name = "_disciplinePartGridColumn";
            this._disciplinePartGridColumn.Tag = "�����#r";
            this._disciplinePartGridColumn.Visible = true;
            this._disciplinePartGridColumn.VisibleIndex = 1;
            // 
            // _partsGridView
            // 
            this._partsGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._partNameGridColumn,
            this._partUnitGridColumn});
            this._partsGridView.GridControl = this._gridControl;
            this._partsGridView.Name = "_partsGridView";
            this._partsGridView.OptionsBehavior.Editable = false;
            this._partsGridView.OptionsDetail.EnableMasterViewMode = false;
            this._partsGridView.Tag = "�����#r";
            this._partsGridView.ViewCaption = "�����";
            // 
            // _partNameGridColumn
            // 
            this._partNameGridColumn.Caption = "��������";
            this._partNameGridColumn.FieldName = "Name";
            this._partNameGridColumn.Name = "_partNameGridColumn";
            this._partNameGridColumn.Tag = "��������#r";
            this._partNameGridColumn.Visible = true;
            this._partNameGridColumn.VisibleIndex = 0;
            // 
            // _partUnitGridColumn
            // 
            this._partUnitGridColumn.Caption = "������";
            this._partUnitGridColumn.FieldName = "Unit";
            this._partUnitGridColumn.Name = "_partUnitGridColumn";
            this._partUnitGridColumn.Tag = "������#r";
            this._partUnitGridColumn.Visible = true;
            this._partUnitGridColumn.VisibleIndex = 1;
            // 
            // _responsibilityZonePhonesGridView
            // 
            this._responsibilityZonePhonesGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._responsibilityZonePhoneZoneGridColumn,
            this._responsibilityZonePhoneTypeGridColumn,
            this._responsibilityZonePhoneNumberGridColumn});
            this._responsibilityZonePhonesGridView.GridControl = this._gridControl;
            this._responsibilityZonePhonesGridView.Name = "_responsibilityZonePhonesGridView";
            this._responsibilityZonePhonesGridView.OptionsBehavior.Editable = false;
            this._responsibilityZonePhonesGridView.OptionsDetail.EnableMasterViewMode = false;
            this._responsibilityZonePhonesGridView.Tag = "�������� ��� ��������������#r";
            this._responsibilityZonePhonesGridView.ViewCaption = "�������� ��� ��������������";
            // 
            // _responsibilityZonePhoneZoneGridColumn
            // 
            this._responsibilityZonePhoneZoneGridColumn.Caption = "���� ��������������";
            this._responsibilityZonePhoneZoneGridColumn.FieldName = "ResponsibilityZone";
            this._responsibilityZonePhoneZoneGridColumn.Name = "_responsibilityZonePhoneZoneGridColumn";
            this._responsibilityZonePhoneZoneGridColumn.Tag = "���� ��������������#r";
            this._responsibilityZonePhoneZoneGridColumn.Visible = true;
            this._responsibilityZonePhoneZoneGridColumn.VisibleIndex = 0;
            // 
            // _responsibilityZonePhoneTypeGridColumn
            // 
            this._responsibilityZonePhoneTypeGridColumn.Caption = "���";
            this._responsibilityZonePhoneTypeGridColumn.FieldName = "PhoneType";
            this._responsibilityZonePhoneTypeGridColumn.Name = "_responsibilityZonePhoneTypeGridColumn";
            this._responsibilityZonePhoneTypeGridColumn.Tag = "���#r";
            this._responsibilityZonePhoneTypeGridColumn.Visible = true;
            this._responsibilityZonePhoneTypeGridColumn.VisibleIndex = 1;
            // 
            // _responsibilityZonePhoneNumberGridColumn
            // 
            this._responsibilityZonePhoneNumberGridColumn.Caption = "�����";
            this._responsibilityZonePhoneNumberGridColumn.Name = "_responsibilityZonePhoneNumberGridColumn";
            this._responsibilityZonePhoneNumberGridColumn.Tag = "�����#r";
            this._responsibilityZonePhoneNumberGridColumn.Visible = true;
            this._responsibilityZonePhoneNumberGridColumn.VisibleIndex = 2;
            // 
            // _templateCoursesGridView
            // 
            this._templateCoursesGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._templateCoursesTemplateGridColumn,
            this._templateCoursesDisciplineGridColumn,
            this._templateCoursesLessonTypeGridColumn,
            this._templateCoursesCategoryGridColumn,
            this._templateCoursesDurationGridColumn});
            this._templateCoursesGridView.GridControl = this._gridControl;
            this._templateCoursesGridView.GroupCount = 2;
            this._templateCoursesGridView.Images = this._imageList;
            this._templateCoursesGridView.Name = "_templateCoursesGridView";
            this._templateCoursesGridView.OptionsBehavior.Editable = false;
            this._templateCoursesGridView.OptionsDetail.EnableMasterViewMode = false;
            this._templateCoursesGridView.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this._templateCoursesDisciplineGridColumn, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this._templateCoursesCategoryGridColumn, DevExpress.Data.ColumnSortOrder.Ascending)});
            this._templateCoursesGridView.Tag = "���������� �������� ������#r";
            this._templateCoursesGridView.ViewCaption = "���������� �������� ������";
            // 
            // _templateCoursesTemplateGridColumn
            // 
            this._templateCoursesTemplateGridColumn.Caption = "������";
            this._templateCoursesTemplateGridColumn.FieldName = "Template";
            this._templateCoursesTemplateGridColumn.ImageIndex = 5;
            this._templateCoursesTemplateGridColumn.Name = "_templateCoursesTemplateGridColumn";
            this._templateCoursesTemplateGridColumn.Tag = "������#r";
            this._templateCoursesTemplateGridColumn.Visible = true;
            this._templateCoursesTemplateGridColumn.VisibleIndex = 0;
            // 
            // _templateCoursesDisciplineGridColumn
            // 
            this._templateCoursesDisciplineGridColumn.Caption = "����";
            this._templateCoursesDisciplineGridColumn.FieldName = "Discipline";
            this._templateCoursesDisciplineGridColumn.ImageIndex = 4;
            this._templateCoursesDisciplineGridColumn.Name = "_templateCoursesDisciplineGridColumn";
            this._templateCoursesDisciplineGridColumn.Tag = "����#r";
            this._templateCoursesDisciplineGridColumn.Visible = true;
            this._templateCoursesDisciplineGridColumn.VisibleIndex = 1;
            // 
            // _templateCoursesLessonTypeGridColumn
            // 
            this._templateCoursesLessonTypeGridColumn.Caption = "��� �������";
            this._templateCoursesLessonTypeGridColumn.FieldName = "LessonType";
            this._templateCoursesLessonTypeGridColumn.Name = "_templateCoursesLessonTypeGridColumn";
            this._templateCoursesLessonTypeGridColumn.Tag = "��� �������#r";
            this._templateCoursesLessonTypeGridColumn.Visible = true;
            this._templateCoursesLessonTypeGridColumn.VisibleIndex = 1;
            // 
            // _templateCoursesCategoryGridColumn
            // 
            this._templateCoursesCategoryGridColumn.Caption = "���������";
            this._templateCoursesCategoryGridColumn.FieldName = "Category";
            this._templateCoursesCategoryGridColumn.Name = "_templateCoursesCategoryGridColumn";
            this._templateCoursesCategoryGridColumn.Tag = "���������#r";
            this._templateCoursesCategoryGridColumn.Visible = true;
            this._templateCoursesCategoryGridColumn.VisibleIndex = 2;
            // 
            // _templateCoursesDurationGridColumn
            // 
            this._templateCoursesDurationGridColumn.Caption = "������������ (���)";
            this._templateCoursesDurationGridColumn.FieldName = "Duration";
            this._templateCoursesDurationGridColumn.Name = "_templateCoursesDurationGridColumn";
            this._templateCoursesDurationGridColumn.Tag = "������������#r";
            this._templateCoursesDurationGridColumn.Visible = true;
            this._templateCoursesDurationGridColumn.VisibleIndex = 2;
            // 
            // _userPositionGridView
            // 
            this._userPositionGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._userPositionGridColumn});
            this._userPositionGridView.GridControl = this._gridControl;
            this._userPositionGridView.Name = "_userPositionGridView";
            this._userPositionGridView.OptionsBehavior.Editable = false;
            this._userPositionGridView.OptionsDetail.EnableMasterViewMode = false;
            this._userPositionGridView.OptionsView.ShowGroupPanel = false;
            this._userPositionGridView.Tag = "���������#r";
            this._userPositionGridView.ViewCaption = "���������";
            // 
            // _userPositionGridColumn
            // 
            this._userPositionGridColumn.Caption = "�����";
            this._userPositionGridColumn.FieldName = "User.Name";
            this._userPositionGridColumn.Name = "_userPositionGridColumn";
            this._userPositionGridColumn.Tag = "..\\�����#r";
            this._userPositionGridColumn.Visible = true;
            this._userPositionGridColumn.VisibleIndex = 0;
            // 
            // _userDepartmentsGridView
            // 
            this._userDepartmentsGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._userDepartmentGridColumn});
            this._userDepartmentsGridView.GridControl = this._gridControl;
            this._userDepartmentsGridView.Name = "_userDepartmentsGridView";
            this._userDepartmentsGridView.OptionsBehavior.Editable = false;
            this._userDepartmentsGridView.OptionsDetail.EnableMasterViewMode = false;
            this._userDepartmentsGridView.OptionsView.ShowGroupPanel = false;
            this._userDepartmentsGridView.Tag = "������#r";
            this._userDepartmentsGridView.ViewCaption = "������";
            // 
            // _userDepartmentGridColumn
            // 
            this._userDepartmentGridColumn.Caption = "�����";
            this._userDepartmentGridColumn.FieldName = "User.Name";
            this._userDepartmentGridColumn.Name = "_userDepartmentGridColumn";
            this._userDepartmentGridColumn.Tag = "..\\�����#r";
            this._userDepartmentGridColumn.Visible = true;
            this._userDepartmentGridColumn.VisibleIndex = 0;
            // 
            // _nameOwnersGridView
            // 
            this._nameOwnersGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._nameGridColumn,
            this._isTestGridColumn,
            this._createProbationerGridColumn});
            this._nameOwnersGridView.GridControl = this._gridControl;
            this._nameOwnersGridView.Name = "_nameOwnersGridView";
            this._nameOwnersGridView.OptionsView.ShowGroupPanel = false;
            this._nameOwnersGridView.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this._nameGridColumn, DevExpress.Data.ColumnSortOrder.Ascending)});
            this._nameOwnersGridView.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.NameOwnersGridView_InitNewRow);
            this._nameOwnersGridView.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.NameOwnersGridView_FocusedRowChanged);
            this._nameOwnersGridView.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.NameOwnersGridView_ValidateRow);
            this._nameOwnersGridView.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.NameOwnersGridView_RowUpdated);
            this._nameOwnersGridView.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.NameOwnersGridView_ValidatingEditor);
            // 
            // _nameGridColumn
            // 
            this._nameGridColumn.Caption = "������������";
            this._nameGridColumn.FieldName = "Name";
            this._nameGridColumn.Name = "_nameGridColumn";
            this._nameGridColumn.Tag = "������������#r";
            this._nameGridColumn.Visible = true;
            this._nameGridColumn.VisibleIndex = 0;
            // 
            // _isTestGridColumn
            // 
            this._isTestGridColumn.Caption = "��������� ����";
            this._isTestGridColumn.ColumnEdit = this._isTestCheckEdit;
            this._isTestGridColumn.FieldName = "IsTest";
            this._isTestGridColumn.Name = "_isTestGridColumn";
            this._isTestGridColumn.OptionsColumn.AllowEdit = false;
            this._isTestGridColumn.OptionsColumn.AllowFocus = false;
            this._isTestGridColumn.OptionsColumn.ReadOnly = true;
            // 
            // _isTestCheckEdit
            // 
            this._isTestCheckEdit.AutoHeight = false;
            this._isTestCheckEdit.Caption = "Check";
            this._isTestCheckEdit.Name = "_isTestCheckEdit";
            // 
            // _createProbationerGridColumn
            // 
            this._createProbationerGridColumn.Caption = "��������� �����������";
            this._createProbationerGridColumn.ColumnEdit = this._createProbationerCheckEdit;
            this._createProbationerGridColumn.FieldName = "CreateProbationer";
            this._createProbationerGridColumn.Name = "_createProbationerGridColumn";
            this._createProbationerGridColumn.OptionsColumn.AllowEdit = false;
            this._createProbationerGridColumn.OptionsColumn.AllowFocus = false;
            this._createProbationerGridColumn.OptionsColumn.ReadOnly = true;
            // 
            // _createProbationerCheckEdit
            // 
            this._createProbationerCheckEdit.AutoHeight = false;
            this._createProbationerCheckEdit.Caption = "Check";
            this._createProbationerCheckEdit.Name = "_createProbationerCheckEdit";
            // 
            // _operationsGridView
            // 
            this._operationsGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._OperationName});
            this._operationsGridView.GridControl = this._gridControl;
            this._operationsGridView.Name = "_operationsGridView";
            // 
            // _OperationName
            // 
            this._OperationName.Caption = "_operations";
            this._OperationName.Name = "_OperationName";
            this._OperationName.Visible = true;
            this._OperationName.VisibleIndex = 0;
            // 
            // _stateGridView
            // 
            this._stateGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1});
            this._stateGridView.GridControl = this._gridControl;
            this._stateGridView.Name = "_stateGridView";
            // 
            // gridColumn1
            // 
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // NameOwnersControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._gridControl);
            this.Name = "NameOwnersControl";
            this.Size = new System.Drawing.Size(579, 523);
            this.Tag = "";
            this.Load += new System.EventHandler(this.NameOwnersControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this._shipsGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._shipToolsGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._personsBandedGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._lettersGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._coursesGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._lessonsGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._personPhonesGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._disciplinesGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._partsGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._responsibilityZonePhonesGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._templateCoursesGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._userPositionGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._userDepartmentsGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._nameOwnersGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._isTestCheckEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._createProbationerCheckEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._operationsGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._stateGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl _gridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView _nameOwnersGridView;
        private DevExpress.XtraGrid.Columns.GridColumn _nameGridColumn;
        private DevExpress.XtraGrid.Views.Grid.GridView _shipsGridView;
        private DevExpress.XtraGrid.Columns.GridColumn _shipNameGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _shipIndexGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _shipRiverGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _shipLineGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _shipConditionsGridColumn;
        private DevExpress.XtraGrid.Views.Grid.GridView _shipToolsGridView;
        private DevExpress.XtraGrid.Columns.GridColumn _shipToolShipGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _shipToolGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _shipToolCategoryGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _shipToolSubCategoryGridColumn;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView _personsBandedGridView;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand _privateInfoGridBand;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn _personNameBandedGridColumn;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn _personBirthDateBandedGridColumn;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand _personQualificationInfoGridBand;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand _personCadreInfoGridBand;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn _personQualificationBandedGridColumn;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn _personEducationBandedGridColumn;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn _personDegreeBandedGridColumn;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn _personPositionBandedGridColumn;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn _personDepartmentBandedGridColumn;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn _personAffiliationBandedGridColumn;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn _personStatusBandedGridColumn;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn _personCategoryBandedGridColumn;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand _personOtherInfoGridBand;
        private DevExpress.XtraGrid.Views.Grid.GridView _lettersGridView;
        private DevExpress.XtraGrid.Columns.GridColumn _letterAddresseeGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _letterCodeGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _letterDateGridColumn;
        private DevExpress.XtraGrid.Views.Grid.GridView _coursesGridView;
        private DevExpress.XtraGrid.Columns.GridColumn _courseShipGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _courseCategoryGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _courseDisciplineGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _courseLessonTypeGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _courseDurationGridColumn;
        private DevExpress.XtraGrid.Views.Grid.GridView _lessonsGridView;
        private DevExpress.XtraGrid.Columns.GridColumn _lessonPersonGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _lessonDisciplineGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _lessonLessonTypeGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _lessonDateTimeGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _lessonDurationGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _lessonCommentsGridColumn;
        private System.Windows.Forms.ImageList _imageList;
        private DevExpress.XtraGrid.Views.Grid.GridView _personPhonesGridView;
        private DevExpress.XtraGrid.Columns.GridColumn _personPhonePersonGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _personPhoneNumberGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _personPhoneTypeGridColumn;
        private DevExpress.XtraGrid.Views.Grid.GridView _disciplinesGridView;
        private DevExpress.XtraGrid.Views.Grid.GridView _partsGridView;
        private DevExpress.XtraGrid.Columns.GridColumn _disciplineNameGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _partNameGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _partUnitGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _disciplineUnitGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _disciplinePartGridColumn;
        private DevExpress.XtraGrid.Views.Grid.GridView _responsibilityZonePhonesGridView;
        private DevExpress.XtraGrid.Columns.GridColumn _responsibilityZonePhoneZoneGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _responsibilityZonePhoneTypeGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _responsibilityZonePhoneNumberGridColumn;
        private DevExpress.XtraGrid.Views.Grid.GridView _templateCoursesGridView;
        private DevExpress.XtraGrid.Columns.GridColumn _templateCoursesTemplateGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _templateCoursesDisciplineGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _templateCoursesLessonTypeGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _templateCoursesCategoryGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _templateCoursesDurationGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _isTestGridColumn;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit _isTestCheckEdit;
        private DevExpress.XtraGrid.Columns.GridColumn _createProbationerGridColumn;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit _createProbationerCheckEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView _operationsGridView;
        private DevExpress.XtraGrid.Views.Grid.GridView _stateGridView;
        private DevExpress.XtraGrid.Views.Grid.GridView _userPositionGridView;
        private DevExpress.XtraGrid.Views.Grid.GridView _userDepartmentsGridView;
        private DevExpress.XtraGrid.Columns.GridColumn _userPositionGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _userDepartmentGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _OperationName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
    }
}

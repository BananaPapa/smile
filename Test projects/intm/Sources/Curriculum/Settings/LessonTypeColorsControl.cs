using System;
using System.Data.Common;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using Eureca.Integrator.Common;

namespace Curriculum.Settings
{
    /// <summary>
    /// Represents lesson type colors control.
    /// </summary>
    public partial class LessonTypeColorsControl : ControlWithAccessAreas
    {
        /// <summary>
        /// Initializes new instance of lesson type colors control.
        /// </summary>
        public LessonTypeColorsControl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initializes grid control data source.
        /// </summary>
        private void InitDataSource()
        {
            _gridControl.DataSource = Program.DbSingletone.GetDataSource(typeof (LessonType));
        }

        /// <summary>
        /// Refreshes data source.
        /// </summary>
        private void RefreshDataSource()
        {
            Program.DbSingletone.RefreshDataSource(typeof (LessonType));
            _colorsGridView.RefreshData();
        }

        /// <summary>
        /// Handles lesson type colors control load event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void LessonTypeColorsControl_Load(object sender, EventArgs e)
        {
            InitDataSource();
        }

        /// <summary>
        /// Handles colors grid view row updated event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void ColorsGridView_RowUpdated(object sender, RowObjectEventArgs e)
        {
            try
            {
                Program.DbSingletone.SubmitChanges();
            }
            catch (DbException ex)
            {
                Program.HandleDbException(ex);
            }
        }

        /// <summary>
        /// Handles grid control embedded navigator button click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void GridControl_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:

                    if ((e.Button.Tag as string) == "Refresh")
                    {
                        try
                        {
                            Program.DbSingletone.SubmitChanges();
                        }
                        catch (DbException ex)
                        {
                            Program.HandleDbException(ex);
                        }
                        finally
                        {
                            RefreshDataSource();
                        }
                    }

                    break;
            }
        }
    }
}
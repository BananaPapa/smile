﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Curriculum.PersonPage;
using DevExpress.XtraEditors;
using Eureca.Integrator.Extensions;

namespace Curriculum.Settings
{
    public partial class RoutesListControl : UserControl
    {
        public RoutesListControl( )
        {
            InitializeComponent( );
        }

        private void gridControl1_EmbeddedNavigator_ButtonClick( object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e )
        {
            switch (e.Button.ButtonType)
            {
                    case NavigatorButtonType.Custom:
                        var tag = (e.Button.Tag as string).Split(new []{'#'}).First();
                        switch (tag)
                        {
                            case "Edit":
                                var selectedItem = _routesGridView.GetSelectedItem();
                                if(selectedItem == null)
                                    return;

                            break;
                        }
                        e.Handled = true;
                    break;
                    case NavigatorButtonType.Append:

                        e.Handled = true;
                    break;
            }
        }

        private bool EditRoute( Route updatedRoute = null)
        {
            bool isNew = updatedRoute == null;
            List<TargetView> targets = null;
            if (updatedRoute != null)
            {
                targets = new List<TargetView>(updatedRoute.RoutesSteps.Count);
                foreach (var routesStep in updatedRoute.RoutesSteps)
                {
                    var routeTarget =
                        Program.DbSingletone.Targets.Single(target => target.TargetId == routesStep.TargetId);
                    var receiver = Program.DbSingletone.Users.Single( user => user.UserId == routeTarget.TargetObjectId );
                    //targets.Add(new TargetView(routeTarget.TargetsOperations.Select(operation => operation.Operation),receiver,routesStep.Order){TargetId = routeTarget.TargetId});
                }
            }
            else
                targets = new List<TargetView>();

            var form = new RouteEditForm( targets );
            bool res = form.ShowDialog( ) == DialogResult.OK;
            if (res)
            {
                updatedRoute = new Route( );
                if (isNew)
                {
                    Program.DbSingletone.Routes.InsertOnSubmit( updatedRoute );
                    Program.DbSingletone.SubmitChanges( );
                }
                UpdateRoute( updatedRoute, form);
                Program.DbSingletone.SubmitChanges( );
            }
            return res;
        }

        private void UpdateRoute( Route updatedRoute, RouteEditForm form )
        {
            List<int> condemnedIndexes  = new List<int>();

            for (int i = 0; i < updatedRoute.RoutesSteps.Count; i++)
            {
                if(form.Targets.RemovedItems.Any(view => view.TargetId == updatedRoute.RoutesSteps[i].TargetId))
                    condemnedIndexes.Add(i);
            }
            
            foreach (var condemnedIndex in condemnedIndexes)
                updatedRoute.RoutesSteps.RemoveAt(condemnedIndex);

            foreach (var changedItem in form.Targets.ChangedItems)
            {
                var originalTarget =
                    Program.DbSingletone.Targets.Single(target => target.TargetId == changedItem.TargetId);
               // originalTarget.
            }
        }

    }
}

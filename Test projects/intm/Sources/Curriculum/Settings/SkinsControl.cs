using System;
using DevExpress.LookAndFeel;
using DevExpress.Skins;
using DevExpress.XtraEditors;
using Eureca.Integrator.Common;

namespace Curriculum.Settings
{
    /// <summary>
    /// Represents skins control.
    /// </summary>
    public partial class SkinsControl : ControlWithAccessAreas
    {
        /// <summary>
        /// Initializes new instance of skins control.
        /// </summary>
        public SkinsControl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Handles skins control load event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void SkinsControl_Load(object sender, EventArgs e)
        {
            // Fill all available skins.
            foreach (SkinContainer skinContainer in SkinManager.Default.Skins)
            {
                _skinsListBoxControl.Items.Add(skinContainer.SkinName);
            }

            // Select current skin.
            if (!String.IsNullOrEmpty(Program.Settings.SkinName))
            {
                _skinsListBoxControl.SelectedValue = Program.Settings.SkinName;
            }
        }

        /// <summary>
        /// Handles skins list box control selected index changed event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void SkinsListBoxControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Enable/disable apply button.
            _applySkinSimpleButton.Enabled =
                UserLookAndFeel.Default.SkinName !=
                (_skinsListBoxControl.SelectedValue as string);
        }

        /// <summary>
        /// Handles apply skin simple button event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void ApplySkinSimpleButton_Click(object sender, EventArgs e)
        {
            // Disable button.
            _applySkinSimpleButton.Enabled = false;

            var skinName = _skinsListBoxControl.SelectedValue as string;

            if (!String.IsNullOrEmpty(skinName))
            {
                // Apply skin.
                UserLookAndFeel.Default.SetSkinStyle(skinName);

                // Save skin to settings.
                Program.Settings.SkinName = skinName;
                Program.Settings.Save();
            }
        }
    }
}
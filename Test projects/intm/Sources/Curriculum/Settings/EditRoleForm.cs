﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Curriculum.AccessAreas;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using Eureca.Integrator.Common;
using Eureca.Utility;
using Eureca.Utility.Extensions;
using Eureca.Utility.Helper;
using Integrator.Curriculum.Constants;
using MessageBox = Eureca.Integrator.Utility.Forms.MessageBox;

namespace Curriculum.Forms
{
    public partial class EditRoleForm : FormWithAccessAreas
    {
        private const string _formTitleTemplate = "Редактирование роли: {0}";
        private readonly Role _role;
        private readonly IEnumerable<Role> _otherRoles;
        private readonly List<Role> _canBeParentRoles;
        public string RoleName
        {
            set
            {
                _role.Name = value;
                Text = _formTitleTemplate.FormatString(value);
            }
            get
            {
                return _role.Name;
            }
        }

        public BindingListWithRemoveEvent<Permission> Permissions
        {
            get
            {
                return _accessAreasTreeControl.Projection;
            }
        }

        private EditRoleForm()
        {
            InitializeComponent( );
        }

        public EditRoleForm( Role role , IEnumerable<Role> roles )
        {
            _role = role;
            //_roleName = role.Name;
            _otherRoles = roles.Where( otherRole => otherRole != role && !otherRole.Admin);
            _canBeParentRoles = FilterParentsRole( role,_otherRoles ).ToList();
            _canBeParentRoles.Insert(0,Constants.EmptyRole);
            InitializeComponent( );
            Load += EditRoleForm_Load;
        }

        //void Permissions_ListChanged( object sender, ListChangedEventArgs e )
        //{
        //    _a.Text = "Added:" + Permissions.AddedItems.Count;
        //    _c.Text = "Changed:" + Permissions.ChangedItems.Count; 
        //    _r.Text = "Removed:" + Permissions.RemovedItems.Count;
        //}

        private IEnumerable<Role> FilterParentsRole( Role role,IEnumerable<Role> roles  )
        {
            List<Role> filteredRoles = new List<Role>();
            foreach (var role1 in roles)
            {
                if(role == role1)
                    continue;
                var parent = role1;
                while (true)
                {
                    if(parent == null)
                    {
                        filteredRoles.Add(role1);
                        break;
                    }
                    if(parent == role)
                        break;
                    parent = parent.ParentRole;
                }
            }
            return filteredRoles;
        }

        private void InitAccessAreas(Role role)
        {
            _accessAreasTreeControl.DropProjection();
            if (role.Admin)
            {
                _accessAreasTreeControl.LoadPermissions(Program.DbSingletone.Tags.Select(tag => new Permission( tag ){PermissionType = PermissionType.ReadOnly| PermissionType.Delete|PermissionType.Change|PermissionType.Add}),true);
            }
            else
            {
                var parentPermissions = role.GetParentPermissions( ).Select( permission => new Permission( permission ) );
                var permissions = role.RolesPermissions.Select( permission => new Permission( permission ) );
                if (parentPermissions.Any( ) || permissions.Any( ))
                {
                    _accessAreasTreeControl.LoadPermissions(
                        parentPermissions, true );
                    _accessAreasTreeControl.LoadPermissions( permissions );
                }   
            }
        }

        private void InitComboBox()
        {
            _parentRoleComboBox.DisplayMember = "Name";
            _parentRoleComboBox.ValueMember = "Value";
            var binding = new Binding( "SelectedItem", _role, "ParentRole",false, DataSourceUpdateMode.OnPropertyChanged,Constants.EmptyRole);
            binding.Format += binding_Format;
            _parentRoleComboBox.DataSource = _canBeParentRoles.ToList( );
            _parentRoleComboBox.DataBindings.Add(binding);
            _parentRoleComboBox.SelectedValueChanged += _parentRoleComboBox_SelectedValueChanged;
        }

        void binding_Format( object sender, ConvertEventArgs e )
        {
            if (e.Value == null)
                e.Value = Constants.EmptyRole;
            else if (e.Value == Constants.EmptyRole)
                e.Value = null;
        }

        void _parentRoleComboBox_SelectedValueChanged( object sender, EventArgs e ) 
        {
            var selectedItem = _parentRoleComboBox.SelectedItem as Role;
            if (selectedItem == null)
            {
                _accessAreasTreeControl.DropProjection();
            }
            else
            {
                _accessAreasTreeControl.DropProjection( );
                _accessAreasTreeControl.LoadPermissions( selectedItem.GetParentPermissions( ).Select( permission => new Permission( permission ) ),true );
                _accessAreasTreeControl.LoadPermissions( selectedItem.RolesPermissions.Select( permission => new Permission( permission ) ), true );
            }
        }

        void EditRoleForm_Load( object sender, EventArgs e )
        {
            _accessAreasTreeControl.Init( Program.DbSingletone.Tags );
            InitComboBox( );
            InitAccessAreas( _role );
            RoleName = _role.Name;
            _roleNameTextEdit.DataBindings.Add(new Binding("Text", this, "RoleName",false,DataSourceUpdateMode.OnPropertyChanged));
            if (_role.Name == null)
                Text = "Редактирование новой роли";
            if (_role.Admin)
            {
                _parentRoleComboBox.Enabled = false;
                _saveBtn.Enabled = false;
                _roleNameTextEdit.Enabled = false;
            }

            //Permissions.ListChanged += Permissions_ListChanged;
        }

        private void _saveBtn_Click( object sender, EventArgs e )
        {
            if (_role.ParentRole == Constants.EmptyRole)
                _role.ParentRole = null;
            if(ValidateRole())
            {
                DialogResult = DialogResult.OK;
            }
        }

        private void _cancelBtn_Click( object sender, EventArgs e )
        {
            if(!_accessAreasTreeControl.Projection.CollectionChanged)
                DialogResult = DialogResult.Cancel;
            else if (MessageBox.Confirm( "Изменения не будут сохранены. Вы уверены?" ))
                DialogResult = DialogResult.Cancel;
        }


        public bool ValidateRole( )
        {
            StringBuilder sb = new StringBuilder( );
            Control shouldFocusControl = null;
            if (String.IsNullOrWhiteSpace( _roleNameTextEdit.Text ))
            {
                sb.Append( "Неодходимо заполнить название роли." );
                shouldFocusControl = _roleNameTextEdit;
            }
            if (_otherRoles.Any( role => role.Name == _role.Name ))
            {
                sb.Append( "Данное имя уже использовано." );
                shouldFocusControl = _roleNameTextEdit;
            }

            if (_role.ParentRole != null)
            {
                if (Permissions.Count == 0 && !Permissions.CollectionChanged)
                    sb.Append( "Роль должна отличаться от родительской.");
            }
            else
            {
                if (Permissions.Count == 0 )
                    sb.Append( "Роль должна содержать разрешения.");
            }

            if (sb.Length > 0)
            {
                sb.Insert(0, "Обнаружены следующие ошибки:\n" );
                MessageBox.ShowValidationError(sb.ToString());
                if (shouldFocusControl != null)
                    shouldFocusControl.Focus();
                return false;
            }
            return true;
        }

        public static EditRoleForm CreateDummy()
        {
            return new EditRoleForm();
        }

        private void button1_Click( object sender, EventArgs e )
        {
            Permissions.ClearHistory();
        }

    }
}

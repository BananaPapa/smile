﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using Eureca.Integrator.Common;

namespace Curriculum.Settings
{
    public partial class ParametersControl : ControlWithAccessAreas
    {
        public ParametersControl()
        {
            InitializeComponent();
        }

        private string Path
        {
            get { return _pathButtonEdit.Text.Trim(); }
            set { _pathButtonEdit.Text = value; }
        }

        private bool SaveButtonEnabled
        {
            set { _saveSimpleButton.Enabled = value; }
        }


        /// <summary>
        /// Initilizes grid control's data source.
        /// </summary>
        private void InitDataSource()
        {
            _exclusionStatusComboBox.Properties.Items.Clear();
            _exclusionStatusComboBox.Properties.Items.Add("Не задан");
            _exclusionStatusComboBox.Properties.Items.AddRange(Program.DbSingletone.Statuses.ToArray());
            _birthDayAffiliationsCheckedComboBoxEdit.Properties.Items.AddRange(Program.DbSingletone.Affiliations.ToArray());

            //Директория с прикрепленными материалами
            Setting attachmentsSettings = Setting.GetSetting(Properties.Settings.Default.AttachmentsBaseFolderSettingKey);
            if (attachmentsSettings != null)
            {
                Path = attachmentsSettings.Value;
            }

            // Принадлежности.
            int[] affiliationIds = Setting.GetBirthAffiliationIds();

            if (affiliationIds == null)
            {
                foreach (CheckedListBoxItem item in _birthDayAffiliationsCheckedComboBoxEdit.Properties.Items)
                {
                    item.CheckState = CheckState.Checked;
                }
            }
            else
            {
                foreach (CheckedListBoxItem item in _birthDayAffiliationsCheckedComboBoxEdit.Properties.Items)
                {
                    var affiliation = item.Value as Affiliation;

                    if (affiliation != null)
                    {
                        item.CheckState = affiliationIds.Contains(affiliation.AffiliationId)
                                              ? CheckState.Checked
                                              : CheckState.Unchecked;
                    }
                }
            }

            _birthDayAffiliationsCheckedComboBoxEdit.EditValueChanged +=
                BirthDayAffiliationsCheckedComboBoxEditEditValueChanged;

            //Статус
            int? exclusionStatusId = Setting.GetExtclusionStatusId();
            if (exclusionStatusId.HasValue)
            {
                //Проставляем последний статус из настроек
                _exclusionStatusComboBox.SelectedItem =
                    _exclusionStatusComboBox.Properties.Items.Cast<object>().Select(item => item as Statuse).
                        FirstOrDefault(status => status != null && status.StatusId == exclusionStatusId.Value);
            }
            //Если статус не проставлен ищем первый попавшийся
            if (_exclusionStatusComboBox.SelectedIndex == -1 && _exclusionStatusComboBox.Properties.Items.Count > 0)
                _exclusionStatusComboBox.SelectedIndex = 0;

            // Срок хранения
            Setting expirationPeriodSetting =
                Setting.GetSetting(Properties.Settings.Default.EventExpirationPeriodSettingKey);

            int result;
            if (expirationPeriodSetting != null && !String.IsNullOrEmpty(expirationPeriodSetting.Value) &&
                Int32.TryParse(expirationPeriodSetting.Value, out result))
            {
                _eventExpirationPeriodSpinEdit.Value = result;
            }
        }

        private void AttachmentsControlLoad(object sender, EventArgs e)
        {
            InitDataSource();

            SaveButtonEnabled = false;
        }

        private void SaveSimpleButtonClick(object sender, EventArgs e)
        {
            string path = Path;

            if (Directory.Exists(path))
            {
                try
                {
                    Setting setting = Setting.GetSetting(Properties.Settings.Default.AttachmentsBaseFolderSettingKey);

                    if (setting == null)
                    {
                        setting = new Setting
                                      {Key = Properties.Settings.Default.AttachmentsBaseFolderSettingKey, Value = path};
                        Program.DbSingletone.Settings.InsertOnSubmit(setting);
                    }
                    else
                    {
                        setting.Value = path;
                    }

                    Program.DbSingletone.SubmitChanges();
                }
                catch (DbException ex)
                {
                    Program.HandleDbException(ex);
                }

                SaveButtonEnabled = false;
            }
            else
            {
                XtraMessageBox.Show(this, "Введенный путь не существует!", "Ошибка", MessageBoxButtons.OK,
                                    MessageBoxIcon.Exclamation);
            }
        }

        private void PathButtonEditEditValueChanged(object sender, EventArgs e)
        {
            SaveButtonEnabled = true;
        }

        private void PathButtonEditButtonClick(object sender, ButtonPressedEventArgs e)
        {
            var dialog = new FolderBrowserDialog
                             {SelectedPath = Path, ShowNewFolderButton = false};


            if (dialog.ShowDialog(this) == DialogResult.OK)
            {
                Path = dialog.SelectedPath;
            }
        }

        private void ComboBoxEditStatuseSelectedIndexChanged(object sender, EventArgs e)
        {
            var status = _exclusionStatusComboBox.SelectedItem as Statuse;
            Setting setting = Setting.GetSetting(Properties.Settings.Default.ExclusionStatusSettingKey);

            if (setting == null)
            {
                setting = new Setting
                              {
                                  Key = Properties.Settings.Default.ExclusionStatusSettingKey,
                                  Value = status == null ? string.Empty : status.StatusId.ToString()
                              };
                Program.DbSingletone.Settings.InsertOnSubmit(setting);
            }
            else
            {
                setting.Value = status == null ? string.Empty : status.StatusId.ToString();
            }

            Program.DbSingletone.SubmitChanges();
        }

        private void _eventExpirationPeriodSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                Setting expirationPeriodSetting =
                    Setting.GetSetting(Properties.Settings.Default.EventExpirationPeriodSettingKey);

                if (expirationPeriodSetting == null)
                {
                    expirationPeriodSetting = new Setting
                                                  {Key = Properties.Settings.Default.EventExpirationPeriodSettingKey};
                    Program.DbSingletone.Settings.InsertOnSubmit(expirationPeriodSetting);
                }

                expirationPeriodSetting.Value = _eventExpirationPeriodSpinEdit.Value.ToString();

                Program.DbSingletone.SubmitChanges();
            }
            catch (DbException ex)
            {
                Program.HandleDbException(ex);
            }
        }

        private void BirthDayAffiliationsCheckedComboBoxEditEditValueChanged(object sender, EventArgs e)
        {
            if (_birthDayAffiliationsCheckedComboBoxEdit.Properties.Items.Count == 0) return;

            var ids =
                new List<int>((from CheckedListBoxItem item in _birthDayAffiliationsCheckedComboBoxEdit.Properties.Items
                               where item.CheckState == CheckState.Checked
                               select item.Value).OfType<Affiliation>().Select(affiliation => affiliation.AffiliationId));

            Setting affiliationsSetting = Setting.GetSetting(Properties.Settings.Default.BirthDayAffiliationSettingKey);
            string value = String.Join(",", ids.ConvertAll(i => i.ToString()).ToArray());

            if (affiliationsSetting == null)
            {
                affiliationsSetting = new Setting
                                          {
                                              Key = Properties.Settings.Default.BirthDayAffiliationSettingKey,
                                              Value = value
                                          };
                Program.DbSingletone.Settings.InsertOnSubmit(affiliationsSetting);
            }
            else
            {
                affiliationsSetting.Value = value;
            }

            Program.DbSingletone.SubmitChanges();
        }
    }
}
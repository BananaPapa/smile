using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid.Views.Base;
using Eureca.Integrator.Common;

namespace Curriculum.Settings
{
    /// <summary>
    /// Represents users control.
    /// </summary>
    public partial class SchedulesControl : ControlWithAccessAreas
    {
        /// <summary>
        /// Initializes new instance of schedules control.
        /// </summary>
        public SchedulesControl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Gets currently focused schedule.
        /// </summary>
        private Schedule FocusedSchedule
        {
            get { return _schedulesGridView.GetFocusedRow() as Schedule; }
        }

        /// <summary>
        /// Initializes grid control data source.
        /// </summary>
        private void InitDataSource()
        {
            _gridControl.DataSource = Program.DbSingletone.GetDataSource(typeof (Schedule));
        }

        /// <summary>
        /// Refreshes data source.
        /// </summary>
        private void RefreshDataSource()
        {
            Program.DbSingletone.RefreshDataSource(typeof (Schedule));
            _schedulesGridView.RefreshData();
        }

        /// <summary>
        /// Handles schedules control load event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void SchedulesControlLoad(object sender, EventArgs e)
        {
            InitDataSource();

            _dayScheduleGridColumn.AppearanceCell.BackColor = BackColor;
            _exceptionScheduleGridColumn.AppearanceCell.BackColor = BackColor;
        }

        /// <summary>
        /// Grid control embedded navigator button click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param> 
        private void GridControlEmbeddedNavigatorButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Remove:

                    e.Handled = true;

                    Schedule schedule = FocusedSchedule;

                    if (schedule != null)
                    {
                        if (schedule.DaySchedules.Count > 0 || schedule.ExceptionSchedules.Count > 0)
                        {
                            XtraMessageBox.Show(this,
                                                "�� �� ������ ������� ��� ����������, ��� ��� ��� ������������ ������� ����������!",
                                                "������",
                                                MessageBoxButtons.OK,
                                                MessageBoxIcon.Error);
                        }
                        else if (XtraMessageBox.Show(this,
                                                     "�� �������, ��� ������ ������� ���������� ����������?",
                                                     "������������� ��������",
                                                     MessageBoxButtons.YesNo,
                                                     MessageBoxIcon.Question,
                                                     MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                        {
                            try
                            {
                                Program.DbSingletone.Schedules.DeleteOnSubmit(schedule);
                                Program.DbSingletone.SubmitChanges();

                                e.Handled = false;
                            }
                            catch (DbException ex)
                            {
                                Program.HandleDbException(ex);
                            }
                        }
                    }

                    break;

                case NavigatorButtonType.Custom:

                    var tag = e.Button.Tag as string;

                    if (tag != null)
                    {
                        switch (tag)
                        {
                            case "Refresh":
                                try
                                {
                                    Program.DbSingletone.SubmitChanges();
                                }
                                catch (DbException ex)
                                {
                                    Program.HandleDbException(ex);
                                }
                                finally
                                {
                                    RefreshDataSource();
                                }
                                break;
                        }
                    }

                    break;
            }
        }

        private void TimeEditEditValueChanged(object sender, EventArgs e)
        {
            if (_schedulesGridView.ActiveEditor.EditValue != null)
            {
                _schedulesGridView.SetRowCellValue(_schedulesGridView.FocusedRowHandle, _isHolidayGridColumn, false);
            }
        }

        private void CheckEditCheckedChanged(object sender, EventArgs e)
        {
            object check = _schedulesGridView.ActiveEditor.EditValue;

            if (check is bool && (bool) check)
            {
                int rowHandle = _schedulesGridView.FocusedRowHandle;

                _schedulesGridView.SetRowCellValue(rowHandle, _startGridColumn, null);
                _schedulesGridView.SetRowCellValue(rowHandle, _breakStartGridColumn, null);
                _schedulesGridView.SetRowCellValue(rowHandle, _breakFinishGridColumn, null);
                _schedulesGridView.SetRowCellValue(rowHandle, _finishGridColumn, null);
            }
        }

        private void SchedulesGridViewValidatingEditor(object sender, BaseContainerValidateEditorEventArgs e)
        {
            if (e.Value is DateTime)
            {
                e.Value = ((DateTime) e.Value).TimeOfDay;
            }
        }

        private void SchedulesGridViewValidateRow(object sender, ValidateRowEventArgs e)
        {
            Schedule schedule = FocusedSchedule;

            if (schedule != null)
            {
                if (String.IsNullOrEmpty(schedule.Name))
                {
                    e.ErrorText = "�������� �� ������ ���� ������.";
                    e.Valid = false;
                }
                else if (
                    Program.DbSingletone.Schedules.Any(
                        s => s.Name.Equals(schedule.Name) && s.ScheduleId != schedule.ScheduleId))
                {
                    e.ErrorText = "����� �������� ��� ������������.";
                    e.Valid = false;
                }
                else if (!schedule.IsHoliday)
                {
                    if (schedule.Start == null || schedule.Finish == null)
                    {
                        e.ErrorText = "����������� ������� ����� ������ � ��������� �������� ���.";
                        e.Valid = false;
                    }
                    else if (schedule.BreakStart == null && schedule.BreakFinish != null ||
                             schedule.BreakStart != null && schedule.BreakFinish == null)
                    {
                        e.ErrorText = "������� ����� � ������, � ��������� �������� ��� �� ���������� �� ���.";
                        e.Valid = false;
                    }
                    else
                    {
                        var times = new List<TimeSpan?>();

                        times.AddRange(new[]
                                           {
                                               schedule.Start, schedule.BreakStart, schedule.BreakFinish,
                                               schedule.Finish
                                           });

                        if (
                            times.Any(
                                span =>
                                span != null &&
                                (times.Count(other => span == other) > 1
                                 || times.Any(other => span >= other && times.IndexOf(span) < times.IndexOf(other)))))
                        {
                            e.ErrorText = "������� ��������� ���������� ���������.";
                            e.Valid = false;
                        }
                    }
                }
            }
        }

        private void SchedulesGridViewCustomColumnDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
        {
            if (e.Value is TimeSpan)
            {
                var span = ((TimeSpan) e.Value);

                e.DisplayText = (DateTime.MinValue + span).ToShortTimeString();
            }
        }

        private void SchedulesGridViewRowUpdated(object sender, RowObjectEventArgs e)
        {
            if (e.Row != null)
            {
                try
                {
                    // Submit changes.
                    Program.DbSingletone.SubmitChanges();
                }
                catch (DbException ex)
                {
                    Program.HandleDbException(ex);
                }
            }
        }

        private void DaySchedulesGridViewCustomColumnDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
        {
            if (e.Value != null && e.Value is int)
            {
                e.DisplayText = DaySchedule.GetDayOfWeekName((int) e.Value);
            }
            else
            {
                if (e.Value != null)
                {
                    e.DisplayText = e.Value.ToString();
                }
            }
        }
    }
}
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Linq;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using Eureca.Integrator.Common;
using MessageBox = Eureca.Integrator.Utility.Forms.MessageBox;

namespace Curriculum.Settings
{
    /// <summary>
    /// Represents name owners control.
    /// </summary>
    internal partial class NameOwnersControl :  ControlWithAccessAreas, IAliasesHolder
    {
        /// <summary>
        /// CommentType of name owner.
        /// </summary>
        private readonly Type _entityType;

        /// <summary>
        /// Initializes new instance of name owners control.
        /// </summary>
        /// <param name="entityType">CommentType of name owner.</param>
        public NameOwnersControl(Type entityType)
        {
            InitializeComponent();
            ApplyAliases();

            _entityType = entityType;
        }

        /// <summary>
        /// Gets focused object.
        /// </summary>
        public object FocusedObject
        {
            get { return _nameOwnersGridView.GetFocusedRow(); }
        }

        #region IAliasesHolder Members

        /// <summary>
        /// Applies aliases to control.
        /// </summary>
        public void ApplyAliases()
        {
            try
            {
                Aliase.ApplyAlias( "Ships", a => _shipsGridView.ViewCaption = a );
                Aliase.ApplyAlias( "Ship", a => _shipToolShipGridColumn.Caption =
                                              _courseShipGridColumn.Caption = a);
                Aliase.ApplyAlias( "River", a => _shipRiverGridColumn.Caption = a );
                Aliase.ApplyAlias( "ShipLine", a => _shipLineGridColumn.Caption = a );
                Aliase.ApplyAlias( "Status", a => _personStatusBandedGridColumn.Caption = a );
                Aliase.ApplyAlias( "Qualification", a => _personQualificationBandedGridColumn.Caption = a );
                Aliase.ApplyAlias( "Education", a => _personEducationBandedGridColumn.Caption = a );
                Aliase.ApplyAlias( "Degree", a => _personDegreeBandedGridColumn.Caption = a );
                Aliase.ApplyAlias( "Position", a => _personPositionBandedGridColumn.Caption = a );
                Aliase.ApplyAlias( "Department", a => _personDepartmentBandedGridColumn.Caption = a );
                Aliase.ApplyAlias( "Affiliation", a => _personAffiliationBandedGridColumn.Caption = a );
                Aliase.ApplyAlias( "QualInfo", a => _personQualificationInfoGridBand.Caption = a );
                Aliase.ApplyAlias( "CadreInfo", a => _personCadreInfoGridBand.Caption = a );
            }
            catch (DbException ex)
            {
                Program.HandleDbException(ex);
            }
        }

        #endregion

        /// <summary>
        /// Focused object changed event.
        /// </summary>
        public event EventHandler FocusedObjectChanged;

        /// <summary>
        /// Initializes grid control data source.
        /// </summary>
        private void InitDataSource()
        {
            _gridControl.DataSource = Program.DbSingletone.GetDataSource(_entityType);
        }

        /// <summary>
        /// Refreshes data source.
        /// </summary>
        private void RefreshDataSource()
        {
            Program.DbSingletone.RefreshDataSource(_entityType);
            _nameOwnersGridView.RefreshData();
        }

        /// <summary>
        /// Handles name owners control load event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void NameOwnersControl_Load(object sender, EventArgs e)
        {
            #region Init Details Grids View

            // List of columns in details grids corresponding to the current name owner.
            var nameOwnerColumns = new List<GridColumn>();

            if (_entityType == typeof (River))
            {
                nameOwnerColumns.Add(_shipRiverGridColumn);
            }
            else if (_entityType == typeof (ShipLine))
            {
                nameOwnerColumns.Add(_shipLineGridColumn);
            }
            else if (_entityType == typeof (Condition))
            {
                nameOwnerColumns.Add(_shipConditionsGridColumn);
            }
            else if (_entityType == typeof (Tool))
            {
                nameOwnerColumns.Add(_shipToolGridColumn);
            }
            else if (_entityType == typeof (ToolCategory))
            {
                nameOwnerColumns.Add(_shipToolCategoryGridColumn);
            }
            else if (_entityType == typeof (ToolSubCategory))
            {
                nameOwnerColumns.Add(_shipToolSubCategoryGridColumn);
            }
            else if (_entityType == typeof (Qualification))
            {
                nameOwnerColumns.Add(_personQualificationBandedGridColumn);

                // Show qualification info grid band only.
                _personQualificationInfoGridBand.Visible = true;
            }
            else if (_entityType == typeof (Education))
            {
                nameOwnerColumns.Add(_personEducationBandedGridColumn);

                // Show qualification info grid band only.
                _personQualificationInfoGridBand.Visible = true;
            }
            else if (_entityType == typeof (Degree))
            {
                nameOwnerColumns.Add(_personDegreeBandedGridColumn);

                // Show qualification info grid band only.
                _personQualificationInfoGridBand.Visible = true;
            }
            else if (_entityType == typeof (Position))
            {
                nameOwnerColumns.Add(_personPositionBandedGridColumn);

                // Show cadre info grid band only.
                _personCadreInfoGridBand.Visible = true;
            }
            else if (_entityType == typeof (Department))
            {
                nameOwnerColumns.Add(_personDepartmentBandedGridColumn);

                // Show cadre info grid band only.
                _personCadreInfoGridBand.Visible = true;
            }
            else if (_entityType == typeof (Affiliation))
            {
                nameOwnerColumns.Add(_personAffiliationBandedGridColumn);

                // Show cadre info grid band only.
                _personCadreInfoGridBand.Visible = true;
            }
            else if (_entityType == typeof (PhoneType))
            {
                nameOwnerColumns.Add(_personPhoneTypeGridColumn);
                nameOwnerColumns.Add(_responsibilityZonePhoneTypeGridColumn);
            }
            else if (_entityType == typeof (Category))
            {
                nameOwnerColumns.Add(_personCategoryBandedGridColumn);
                nameOwnerColumns.Add(_courseCategoryGridColumn);
                nameOwnerColumns.Add(_templateCoursesCategoryGridColumn);

                // Show others info grid band and category column only.
                _personOtherInfoGridBand.Visible =
                    _personCategoryBandedGridColumn.Visible = true;
            }
            else if (_entityType == typeof (Statuse))
            {
                nameOwnerColumns.Add(_personStatusBandedGridColumn);

                // Show others info grid band and status column only.
                _personOtherInfoGridBand.Visible =
                    _personStatusBandedGridColumn.Visible = true;

                _createProbationerGridColumn.Visible = true;
                _createProbationerGridColumn.AppearanceCell.BackColor = BackColor;
            }
            else if (_entityType == typeof (Addressee))
            {
                nameOwnerColumns.Add(_letterAddresseeGridColumn);
            }
            else if (_entityType == typeof (Unit))
            {
                nameOwnerColumns.Add(_partUnitGridColumn);
                nameOwnerColumns.Add(_disciplineUnitGridColumn);
            }
            else if (_entityType == typeof (Part))
            {
                nameOwnerColumns.Add(_disciplinePartGridColumn);
            }
            else if (_entityType == typeof (LessonType))
            {
                nameOwnerColumns.Add(_courseLessonTypeGridColumn);
                nameOwnerColumns.Add(_lessonLessonTypeGridColumn);
                nameOwnerColumns.Add(_templateCoursesLessonTypeGridColumn);

                _isTestGridColumn.Visible = true;
                _isTestGridColumn.AppearanceCell.BackColor = BackColor;
            }
            else if (_entityType == typeof (Template))
            {
                nameOwnerColumns.Add(_templateCoursesTemplateGridColumn);
            }

            else if (_entityType == typeof( UserDepartment ))
            {
                nameOwnerColumns.Add( _userDepartmentGridColumn );
            }
            else if (_entityType == typeof( UserPosition ))
            {
                nameOwnerColumns.Add( _userPositionGridColumn );
            }
            // Highlight columns using back color.
            if (nameOwnerColumns.Count > 0)
            {
                nameOwnerColumns.ForEach(c => c.AppearanceCell.BackColor = BackColor);
            }

            #endregion

            InitDataSource();
        }

        /// <summary>
        /// Handles grid control focused view changed event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void GridControl_FocusedViewChanged(object sender, ViewFocusEventArgs e)
        {
            if (_gridControl.EmbeddedNavigator != null && e.View != null)
            {
                // Disable adding and removing rows in details grids.
                _gridControl.EmbeddedNavigator.Buttons.Append.Enabled =
                    _gridControl.EmbeddedNavigator.Buttons.Remove.Enabled = e.View == _nameOwnersGridView;
            }
        }

        /// <summary>
        /// Handles name owners grid view init new row event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void NameOwnersGridView_InitNewRow(object sender, InitNewRowEventArgs e)
        {
            // If new row is lesson type.
            var lessonType = _nameOwnersGridView.GetRow(e.RowHandle) as LessonType;
            if (lessonType != null)
            {
                // Init default fill and border colors for calendar.
                lessonType.FillColorArgb = Color.White.ToArgb();
                lessonType.BorderColorArgb = Color.Black.ToArgb();
            }

            _nameOwnersGridView.ShowEditor();
        }

        /// <summary>
        /// Validates name owner text.
        /// </summary>
        /// <param name="text">Name owner text.</param>
        /// <param name="errorText">Error, if occured.</param>
        /// <returns>true if text is valid; otherwise, false.</returns>
        private bool ValidateName(string text, out string errorText)
        {
            if (text == null || text.Trim().Length == 0)
            {
                errorText = "�������� �� ������ ���� ������";
                return false;
            }

            for (int i = 0; i < _nameOwnersGridView.RowCount; ++i)
            {
                int rowHandle = _nameOwnersGridView.GetRowHandle(i);

                if (rowHandle != _nameOwnersGridView.FocusedRowHandle)
                {
                    object value = _nameOwnersGridView.GetRowCellValue(rowHandle, _nameGridColumn);

                    if ((value as string) == text)
                    {
                        errorText = "����� �������� ��� ������������.";
                        return false;
                    }
                }
            }

            errorText = null;
            return true;
        }

        /// <summary>
        /// Handles name owners grid view validating editor event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void NameOwnersGridView_ValidatingEditor(object sender, BaseContainerValidateEditorEventArgs e)
        {
            var text = e.Value as string;

            if (text != null)
            {
                string errorText;

                // Validate name.
                if (!(e.Valid = ValidateName(text, out errorText)))
                {
                    e.ErrorText = errorText;
                }
            }
        }

        /// <summary>
        /// Handles name owners grid view validate row event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void NameOwnersGridView_ValidateRow(object sender, ValidateRowEventArgs e)
        {
            string text;

            if (e.Row is INameOwner)
            {
                text = ((INameOwner) e.Row).Name;
            }
            else
            {
                return;
            }

            string errorText;

            // Validate name.
            if (!(e.Valid = ValidateName(text, out errorText)))
            {
                e.ErrorText = errorText;
            }
        }

        /// <summary>
        /// Handles name owners grid view row updated event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void NameOwnersGridView_RowUpdated(object sender, RowObjectEventArgs e)
        {
            var template = e.Row as Template;

            if (template != null)
            {
                string tmplateName = template.Name;
                if (tmplateName.Length > 50)
                {
                    MessageBox.ShowExclamation("��������� ������������ �����.","������������ ����� ������� (50 ��������) ��e������. �������� ����� �������.");
                    template.Name = tmplateName.Substring( 0, 50 );
                }
                try
                {
                    // Submit changes.
                    Program.DbSingletone.SubmitChanges();
                }
                catch (DbException ex)
                {
                    Program.HandleDbException(ex);
                }
            }
        }

        /// <summary>
        /// Grid control embedded navigator button click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>       
        private void GridControl_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Append:

                    // Suppress parts inserting.
                    if (_entityType == typeof (Part))
                    {
                        XtraMessageBox.Show("����� ����� �������� � ������� �� ����� \"����\".", "����������",
                                            MessageBoxButtons.OK, MessageBoxIcon.Information);

                        e.Handled = true;
                    }

                    break;

                case NavigatorButtonType.Remove:

                    // Selected row will not be removed.
                    e.Handled = true;

                    object row = _nameOwnersGridView.GetFocusedRow();

                    if (row != null)
                    {
                        if (_entityType == typeof (Statuse) && row is Statuse && ((Statuse) row).CreateProbationer)
                        {
                            XtraMessageBox.Show(
                                "���� ������ ������ �������, ��� ��� �� ������������ �������� ��� ����� � ��� \"������������-2\"",
                                "����������� ��������",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Exclamation);

                            return;
                        }
                        if (_entityType == typeof (LessonType) && row is LessonType && ((LessonType) row).IsTest)
                        {
                            XtraMessageBox.Show(
                                "���� ��� ������� ������ �������, ��� ��� �� ������������ �������� ��� ����� � ��� \"������������-2\"",
                                "����������� ��������",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Exclamation);

                            return;
                        }

                        ITable table;
                        try
                        {
                            table = Program.DbSingletone.GetTable(_entityType);
                        }
                        catch
                        {
                            return;
                        }

                        if (table != null &&
                            XtraMessageBox.Show(
                                "�� �������, ��� ������ ������� ���������� ������?",
                                "������������� ��������",
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Question,
                                MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                        {
                            // Set disciplines part id as NULL.
                            // There is no SET NULL action for corresponding reference.
                            if (_entityType == typeof (Part))
                            {
                                var part = (Part) row;

                                try
                                {
                                    Program.DbSingletone.ExecuteCommand("UPDATE Disciplines SET PartId=NULL WHERE PartId={0}",
                                                              part.PartId);
                                }
                                catch (DbException ex)
                                {
                                    Program.HandleDbException(ex);
                                    break;
                                }
                            }

                            table.DeleteOnSubmit(row);

                            try
                            {
                                Program.DbSingletone.SubmitChanges();

                                // Selected row will be removed.
                                e.Handled = false;
                            }
                            catch (DbException ex)
                            {
                                Program.HandleDbException(ex);
                            }
                        }
                    }
                    break;

                case NavigatorButtonType.Custom:

                    var tag = e.Button.Tag as string;

                    if (tag != null)
                    {
                        switch (tag)
                        {
                            case "Refresh":
                                try
                                {
                                    Program.DbSingletone.SubmitChanges();
                                }
                                catch (DbException ex)
                                {
                                    Program.HandleDbException(ex);
                                }
                                finally
                                {
                                    RefreshDataSource();

                                    _nameOwnersGridView.CollapseAllDetails();
                                }
                                break;
                        }
                    }
                    break;
            }
        }

        #region Code was added by V.Kovalev

        private INameOwner _prevNameOwner;

        /// <summary>
        /// Validate the name owner.
        /// </summary>
        /// <param name="nameOwner">Name owner to validate</param>
        /// <param name="errorText">Error if occured.</param>
        /// <returns>True if name owner is valid; otherwise, false.</returns>
        private bool ValidateNameOwner(INameOwner nameOwner, out string errorText)
        {
            // Validate name.
            return ValidateName(nameOwner.Name, out errorText);
        }

        /// <summary>
        /// Handles owners grid view focused row changed event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void NameOwnersGridView_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            var view = sender as GridView;
            if (view != null)
            {
                string errorText;
                if (_prevNameOwner != null && !ValidateNameOwner(_prevNameOwner, out errorText))
                {
                    //If _previous name owner is not validated then discard data context changes.
                    Program.DiscardDataChanges();
                }

                if (view.IsGroupRow(e.FocusedRowHandle))
                {
                    _prevNameOwner = null;
                }
                else
                {
                    _prevNameOwner = view.GetRow(e.FocusedRowHandle) as INameOwner;
                }
            }

            if (FocusedObjectChanged != null)
            {
                FocusedObjectChanged.Invoke(this, EventArgs.Empty);
            }
        }

        #endregion
    }
}
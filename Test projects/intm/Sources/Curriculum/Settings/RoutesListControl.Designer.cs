﻿namespace Curriculum.Settings
{
    partial class RoutesListControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if (disposing && ( components != null ))
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RoutesListControl));
            this._routesGrid = new DevExpress.XtraGrid.GridControl();
            this._routesGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._routeName = new DevExpress.XtraGrid.Columns.GridColumn();
            this._imageList = new System.Windows.Forms.ImageList(this.components);
            ((System.ComponentModel.ISupportInitialize)(this._routesGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._routesGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // _routesGrid
            // 
            this._routesGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._routesGrid.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this._routesGrid.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this._routesGrid.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this._routesGrid.EmbeddedNavigator.Buttons.ImageList = this._imageList;
            this._routesGrid.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "", "Edit#rc")});
            this._routesGrid.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this._routesGrid.Location = new System.Drawing.Point(0, 0);
            this._routesGrid.MainView = this._routesGridView;
            this._routesGrid.Name = "_routesGrid";
            this._routesGrid.Size = new System.Drawing.Size(385, 383);
            this._routesGrid.TabIndex = 0;
            this._routesGrid.UseEmbeddedNavigator = true;
            this._routesGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this._routesGridView});
            // 
            // _routesGridView
            // 
            this._routesGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._routeName});
            this._routesGridView.GridControl = this._routesGrid;
            this._routesGridView.Name = "_routesGridView";
            this._routesGridView.OptionsView.ShowGroupPanel = false;
            // 
            // _routeName
            // 
            this._routeName.Caption = "Маршрут";
            this._routeName.FieldName = "Name";
            this._routeName.Name = "_routeName";
            this._routeName.Visible = true;
            this._routeName.VisibleIndex = 0;
            // 
            // _imageList
            // 
            this._imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("_imageList.ImageStream")));
            this._imageList.TransparentColor = System.Drawing.Color.Transparent;
            this._imageList.Images.SetKeyName(0, "pen16.png");
            // 
            // RoutesListControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._routesGrid);
            this.Name = "RoutesListControl";
            this.Size = new System.Drawing.Size(385, 383);
            ((System.ComponentModel.ISupportInitialize)(this._routesGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._routesGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl _routesGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView _routesGridView;
        private DevExpress.XtraGrid.Columns.GridColumn _routeName;
        private System.Windows.Forms.ImageList _imageList;
    }
}

﻿namespace Curriculum.Forms
{
    partial class PeopleGroupSelectionForms
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if (disposing && ( components != null ))
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            this.label1 = new System.Windows.Forms.Label();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this._cancelButton = new DevExpress.XtraEditors.SimpleButton();
            this._groupNameText = new System.Windows.Forms.TextBox();
            this._usersSelectionControl = new Curriculum.Controls.UsersSelectionControl();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 13);
            this.label1.TabIndex = 0;
            this.label1.Tag = "Участники группы#r";
            this.label1.Text = "Название группы:";
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(657, 610);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 3;
            this.simpleButton1.Tag = "Участники группы#re";
            this.simpleButton1.Text = "Ок";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // _cancelButton
            // 
            this._cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._cancelButton.Location = new System.Drawing.Point(561, 610);
            this._cancelButton.Name = "_cancelButton";
            this._cancelButton.Size = new System.Drawing.Size(75, 23);
            this._cancelButton.TabIndex = 5;
            this._cancelButton.Tag = "Участники группы#r";
            this._cancelButton.Text = "Отмена";
            // 
            // _groupNameText
            // 
            this._groupNameText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._groupNameText.Location = new System.Drawing.Point(130, 23);
            this._groupNameText.Name = "_groupNameText";
            this._groupNameText.Size = new System.Drawing.Size(602, 21);
            this._groupNameText.TabIndex = 6;
            // 
            // _usersSelectionControl
            // 
            this._usersSelectionControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._usersSelectionControl.Location = new System.Drawing.Point(27, 59);
            this._usersSelectionControl.Name = "_usersSelectionControl";
            this._usersSelectionControl.Size = new System.Drawing.Size(705, 545);
            this._usersSelectionControl.TabIndex = 7;
            // 
            // PeopleGroupSelectionForms
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(765, 655);
            this.Controls.Add(this._usersSelectionControl);
            this.Controls.Add(this._groupNameText);
            this.Controls.Add(this._cancelButton);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.label1);
            this.Name = "PeopleGroupSelectionForms";
            this.Text = "PeopleGroupSelectionForms";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton _cancelButton;
        private System.Windows.Forms.TextBox _groupNameText;
        private Controls.UsersSelectionControl _usersSelectionControl;

    }
}
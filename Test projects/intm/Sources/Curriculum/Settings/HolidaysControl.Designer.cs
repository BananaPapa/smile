namespace Curriculum.Settings
{
    partial class HolidaysControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HolidaysControl));
            this._gridControl = new DevExpress.XtraGrid.GridControl();
            this._imageList = new System.Windows.Forms.ImageList(this.components);
            this._holidaysGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._nameGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._startGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._dateEdit = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this._finishGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)(this._gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._holidaysGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._dateEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._dateEdit.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // _gridControl
            // 
            this._gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._gridControl.EmbeddedNavigator.Buttons.Append.Tag = "������� ���������#ra";
            this._gridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this._gridControl.EmbeddedNavigator.Buttons.Edit.Tag = "������� ���������#rc";
            this._gridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this._gridControl.EmbeddedNavigator.Buttons.ImageList = this._imageList;
            this._gridControl.EmbeddedNavigator.Buttons.Remove.Tag = "������� ���������#rd";
            this._gridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "�������� ������", "Refresh")});
            this._gridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.GridControlEmbeddedNavigatorButtonClick);
            this._gridControl.Location = new System.Drawing.Point(0, 0);
            this._gridControl.MainView = this._holidaysGridView;
            this._gridControl.Name = "_gridControl";
            this._gridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this._dateEdit});
            this._gridControl.Size = new System.Drawing.Size(534, 467);
            this._gridControl.TabIndex = 0;
            this._gridControl.Tag = "������� ���������#r";
            this._gridControl.UseEmbeddedNavigator = true;
            this._gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this._holidaysGridView,
            this.gridView2});
            // 
            // _imageList
            // 
            this._imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("_imageList.ImageStream")));
            this._imageList.TransparentColor = System.Drawing.Color.White;
            this._imageList.Images.SetKeyName(0, "Edit16.png");
            this._imageList.Images.SetKeyName(1, "Refresh16.png");
            // 
            // _holidaysGridView
            // 
            this._holidaysGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._nameGridColumn,
            this._startGridColumn,
            this._finishGridColumn});
            this._holidaysGridView.GridControl = this._gridControl;
            this._holidaysGridView.Name = "_holidaysGridView";
            this._holidaysGridView.OptionsDetail.EnableMasterViewMode = false;
            this._holidaysGridView.OptionsView.ShowGroupPanel = false;
            this._holidaysGridView.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this._startGridColumn, DevExpress.Data.ColumnSortOrder.Ascending)});
            this._holidaysGridView.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.HolidaysGridViewValidateRow);
            this._holidaysGridView.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.HolidaysGridViewRowUpdated);
            this._holidaysGridView.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.HolidaysGridViewValidatingEditor);
            // 
            // _nameGridColumn
            // 
            this._nameGridColumn.Caption = "��������";
            this._nameGridColumn.FieldName = "Name";
            this._nameGridColumn.Name = "_nameGridColumn";
            this._nameGridColumn.Tag = "��������#r";
            this._nameGridColumn.Visible = true;
            this._nameGridColumn.VisibleIndex = 0;
            this._nameGridColumn.Width = 275;
            // 
            // _startGridColumn
            // 
            this._startGridColumn.AppearanceCell.Options.UseTextOptions = true;
            this._startGridColumn.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this._startGridColumn.Caption = "������";
            this._startGridColumn.ColumnEdit = this._dateEdit;
            this._startGridColumn.FieldName = "Start";
            this._startGridColumn.Name = "_startGridColumn";
            this._startGridColumn.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this._startGridColumn.OptionsColumn.FixedWidth = true;
            this._startGridColumn.Tag = "������#r";
            this._startGridColumn.Visible = true;
            this._startGridColumn.VisibleIndex = 1;
            this._startGridColumn.Width = 116;
            // 
            // _dateEdit
            // 
            this._dateEdit.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this._dateEdit.AutoHeight = false;
            this._dateEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._dateEdit.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this._dateEdit.DisplayFormat.FormatString = "d MMMM";
            this._dateEdit.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this._dateEdit.EditFormat.FormatString = "d MMMM";
            this._dateEdit.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this._dateEdit.Mask.EditMask = "d MMMM";
            this._dateEdit.Name = "_dateEdit";
            this._dateEdit.ShowDropDown = DevExpress.XtraEditors.Controls.ShowDropDown.Never;
            // 
            // _finishGridColumn
            // 
            this._finishGridColumn.Caption = "���������";
            this._finishGridColumn.FieldName = "Finish";
            this._finishGridColumn.Name = "_finishGridColumn";
            this._finishGridColumn.OptionsColumn.FixedWidth = true;
            this._finishGridColumn.Tag = "���������#r";
            this._finishGridColumn.Visible = true;
            this._finishGridColumn.VisibleIndex = 2;
            this._finishGridColumn.Width = 122;
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this._gridControl;
            this.gridView2.Name = "gridView2";
            // 
            // HolidaysControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._gridControl);
            this.Name = "HolidaysControl";
            this.Size = new System.Drawing.Size(534, 467);
            this.Tag = "���������#r";
            this.Load += new System.EventHandler(this.ExceptionSchedulesControlLoad);
            ((System.ComponentModel.ISupportInitialize)(this._gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._holidaysGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._dateEdit.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._dateEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl _gridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView _holidaysGridView;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn _startGridColumn;
        private System.Windows.Forms.ImageList _imageList;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit _dateEdit;
        private DevExpress.XtraGrid.Columns.GridColumn _nameGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _finishGridColumn;
    }
}

using System;
using System.Data.Common;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid.Views.Base;
using Eureca.Integrator.Common;

namespace Curriculum.Settings
{
    /// <summary>
    /// Represents aliases control.
    /// </summary>
    public partial class AliasesControl : ControlWithAccessAreas
    {
        /// <summary>
        /// Initializes new instance of aliases control.
        /// </summary>
        public AliasesControl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initializes grid control data source.
        /// </summary>
        private void InitDataSource()
        {
            _gridControl.DataSource = Program.DbSingletone.GetDataSource( typeof( Aliase ) );
        }

        /// <summary>
        /// Refreshes data source.
        /// </summary>
        private void RefreshDataSource()
        {
            Program.DbSingletone.RefreshDataSource( typeof( Aliase ) );
            _aliasesGridView.RefreshData();
        }

        /// <summary>
        /// Handles aliases control load event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void AliasesControl_Load(object sender, EventArgs e)
        {
            InitDataSource();
        }

        /// <summary>
        /// Handles aliases grid view row updated event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void AliasesGridView_RowUpdated(object sender, RowObjectEventArgs e)
        {
            try
            {
                Program.DbSingletone.SubmitChanges();

                // Apply aliases.
                if (TopLevelControl is StartForm)
                {
                    ((StartForm) TopLevelControl).ApplyAliases();
                }
            }
            catch (DbException ex)
            {
                Program.HandleDbException(ex);
            }
        }

        /// <summary>
        /// Handles aliases grid validating editor event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void AliasesGridView_ValidatingEditor(object sender, BaseContainerValidateEditorEventArgs e)
        {
            var text = e.Value as string;

            if (text != null && text.Trim().Length == 0)
            {
                e.Value = null;
            }
        }

        /// <summary>
        /// Handles aliases grid view custom unbound column data event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void AliasesGridView_CustomUnboundColumnData(object sender, CustomColumnDataEventArgs e)
        {
            if (e.IsGetData && e.Column == _defaultGridColumn)
            {
                var alias = e.Row as Aliase;
                if (alias != null)
                {
                    e.Value = Aliase.GetDefaultValue( alias.AliasKey );
                }
            }
        }

        /// <summary>
        /// Handles grid control embedded navigator button click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void GridControl_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:

                    if ((e.Button.Tag as string) == "Refresh")
                    {
                        try
                        {
                            Program.DbSingletone.SubmitChanges();
                        }
                        catch (DbException ex)
                        {
                            Program.HandleDbException(ex);
                        }
                        finally
                        {
                            RefreshDataSource();
                        }
                    }

                    break;
            }
        }
    }
}
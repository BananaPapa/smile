namespace Curriculum.Settings
{
    partial class CommissionMembersControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CommissionMembersControl));
            this._gridControl = new DevExpress.XtraGrid.GridControl();
            this._imageList = new System.Windows.Forms.ImageList(this.components);
            this._commissionMemberGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._CommissionMemberRankGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._CommissionMemberNameGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this._gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._commissionMemberGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // _gridControl
            // 
            this._gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._gridControl.EmbeddedNavigator.Buttons.Append.Tag = "..\\������� ����� ��������#ra";
            this._gridControl.EmbeddedNavigator.Buttons.Edit.Tag = "..\\������� ����� ��������#rc";
            this._gridControl.EmbeddedNavigator.Buttons.ImageList = this._imageList;
            this._gridControl.EmbeddedNavigator.Buttons.Remove.Tag = "..\\������� ����� ��������#rd";
            this._gridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "", "Refresh")});
            this._gridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.GridControl_EmbeddedNavigator_ButtonClick);
            this._gridControl.Location = new System.Drawing.Point(0, 0);
            this._gridControl.MainView = this._commissionMemberGridView;
            this._gridControl.Name = "_gridControl";
            this._gridControl.Size = new System.Drawing.Size(498, 417);
            this._gridControl.TabIndex = 0;
            this._gridControl.Tag = "..\\������� ����� ��������#r";
            this._gridControl.UseEmbeddedNavigator = true;
            this._gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this._commissionMemberGridView});
            // 
            // _imageList
            // 
            this._imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("_imageList.ImageStream")));
            this._imageList.TransparentColor = System.Drawing.Color.White;
            this._imageList.Images.SetKeyName(0, "Refresh16.png");
            // 
            // _commissionMemberGridView
            // 
            this._commissionMemberGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._CommissionMemberRankGridColumn,
            this._CommissionMemberNameGridColumn});
            this._commissionMemberGridView.GridControl = this._gridControl;
            this._commissionMemberGridView.Name = "_commissionMemberGridView";
            this._commissionMemberGridView.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.CommissionMemberGridView_FocusedRowChanged);
            this._commissionMemberGridView.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.CommissionMemberGridView_ValidateRow);
            this._commissionMemberGridView.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.CommissionMembersGridView_RowUpdated);
            this._commissionMemberGridView.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.CommissionMembersGridView_ValidatingEditor);
            // 
            // _CommissionMemberRankGridColumn
            // 
            this._CommissionMemberRankGridColumn.Caption = "���������";
            this._CommissionMemberRankGridColumn.FieldName = "Rank";
            this._CommissionMemberRankGridColumn.Name = "_CommissionMemberRankGridColumn";
            this._CommissionMemberRankGridColumn.Tag = "���������#r";
            this._CommissionMemberRankGridColumn.Visible = true;
            this._CommissionMemberRankGridColumn.VisibleIndex = 0;
            // 
            // _CommissionMemberNameGridColumn
            // 
            this._CommissionMemberNameGridColumn.Caption = "���";
            this._CommissionMemberNameGridColumn.FieldName = "Name";
            this._CommissionMemberNameGridColumn.Name = "_CommissionMemberNameGridColumn";
            this._CommissionMemberNameGridColumn.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this._CommissionMemberNameGridColumn.Tag = "���#r";
            this._CommissionMemberNameGridColumn.Visible = true;
            this._CommissionMemberNameGridColumn.VisibleIndex = 1;
            // 
            // CommissionMembersControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._gridControl);
            this.Name = "CommissionMembersControl";
            this.Size = new System.Drawing.Size(498, 417);
            this.Tag = "������� ����� ��������#r";
            this.Load += new System.EventHandler(this.CommissionMembersControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this._gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._commissionMemberGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl _gridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView _commissionMemberGridView;
        private DevExpress.XtraGrid.Columns.GridColumn _CommissionMemberNameGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _CommissionMemberRankGridColumn;
        private System.Windows.Forms.ImageList _imageList;
    }
}

namespace Curriculum.Settings
{
    partial class SchedulesControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode2 = new DevExpress.XtraGrid.GridLevelNode();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SchedulesControl));
            this._daySchedulesGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._dayOfWeekGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._dayScheduleGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._gridControl = new DevExpress.XtraGrid.GridControl();
            this._imageList = new System.Windows.Forms.ImageList(this.components);
            this._exceptionSchedulesGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._dateGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._exceptionScheduleGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._schedulesGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._nameGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._isHolidayGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._checkEdit = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this._startGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._timeEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit();
            this._breakStartGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._breakFinishGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._finishGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)(this._daySchedulesGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._exceptionSchedulesGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._schedulesGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._checkEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._timeEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // _daySchedulesGridView
            // 
            this._daySchedulesGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._dayOfWeekGridColumn,
            this._dayScheduleGridColumn});
            this._daySchedulesGridView.GridControl = this._gridControl;
            this._daySchedulesGridView.Name = "_daySchedulesGridView";
            this._daySchedulesGridView.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this._daySchedulesGridView.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this._daySchedulesGridView.OptionsBehavior.Editable = false;
            this._daySchedulesGridView.Tag = "���������� ���!!!#r";
            this._daySchedulesGridView.ViewCaption = "���������� ���";
            this._daySchedulesGridView.CustomColumnDisplayText += new DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventHandler(this.DaySchedulesGridViewCustomColumnDisplayText);
            // 
            // _dayOfWeekGridColumn
            // 
            this._dayOfWeekGridColumn.AppearanceCell.Options.UseTextOptions = true;
            this._dayOfWeekGridColumn.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this._dayOfWeekGridColumn.Caption = "���� ������";
            this._dayOfWeekGridColumn.FieldName = "DayOfWeek";
            this._dayOfWeekGridColumn.Name = "_dayOfWeekGridColumn";
            this._dayOfWeekGridColumn.Tag = "���� ������#r";
            this._dayOfWeekGridColumn.Visible = true;
            this._dayOfWeekGridColumn.VisibleIndex = 0;
            // 
            // _dayScheduleGridColumn
            // 
            this._dayScheduleGridColumn.Caption = "����������";
            this._dayScheduleGridColumn.FieldName = "Schedule";
            this._dayScheduleGridColumn.Name = "_dayScheduleGridColumn";
            this._dayScheduleGridColumn.Tag = "����������#r";
            this._dayScheduleGridColumn.Visible = true;
            this._dayScheduleGridColumn.VisibleIndex = 1;
            // 
            // _gridControl
            // 
            this._gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._gridControl.EmbeddedNavigator.Buttons.Append.Tag = "������� ����������#ra";
            this._gridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this._gridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this._gridControl.EmbeddedNavigator.Buttons.EndEdit.Tag = "������� ����������#rc";
            this._gridControl.EmbeddedNavigator.Buttons.ImageList = this._imageList;
            this._gridControl.EmbeddedNavigator.Buttons.Remove.Tag = "������� ����������#rd";
            this._gridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "�������� ������", "Refresh")});
            this._gridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.GridControlEmbeddedNavigatorButtonClick);
            gridLevelNode1.LevelTemplate = this._daySchedulesGridView;
            gridLevelNode1.RelationName = "DaySchedules";
            gridLevelNode2.LevelTemplate = this._exceptionSchedulesGridView;
            gridLevelNode2.RelationName = "ExceptionSchedules";
            this._gridControl.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1,
            gridLevelNode2});
            this._gridControl.Location = new System.Drawing.Point(0, 0);
            this._gridControl.MainView = this._schedulesGridView;
            this._gridControl.Name = "_gridControl";
            this._gridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this._timeEdit,
            this._checkEdit});
            this._gridControl.Size = new System.Drawing.Size(534, 467);
            this._gridControl.TabIndex = 0;
            this._gridControl.Tag = "������� ����������#r";
            this._gridControl.UseEmbeddedNavigator = true;
            this._gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this._exceptionSchedulesGridView,
            this._schedulesGridView,
            this.gridView2,
            this._daySchedulesGridView});
            // 
            // _imageList
            // 
            this._imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("_imageList.ImageStream")));
            this._imageList.TransparentColor = System.Drawing.Color.White;
            this._imageList.Images.SetKeyName(0, "Edit16.png");
            this._imageList.Images.SetKeyName(1, "Refresh16.png");
            // 
            // _exceptionSchedulesGridView
            // 
            this._exceptionSchedulesGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._dateGridColumn,
            this._exceptionScheduleGridColumn});
            this._exceptionSchedulesGridView.GridControl = this._gridControl;
            this._exceptionSchedulesGridView.Name = "_exceptionSchedulesGridView";
            this._exceptionSchedulesGridView.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this._exceptionSchedulesGridView.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this._exceptionSchedulesGridView.OptionsBehavior.Editable = false;
            this._exceptionSchedulesGridView.Tag = "����������!!!#r";
            this._exceptionSchedulesGridView.ViewCaption = "����������";
            // 
            // _dateGridColumn
            // 
            this._dateGridColumn.Caption = "����";
            this._dateGridColumn.FieldName = "Date";
            this._dateGridColumn.Name = "_dateGridColumn";
            this._dateGridColumn.Tag = "����#r";
            this._dateGridColumn.Visible = true;
            this._dateGridColumn.VisibleIndex = 0;
            // 
            // _exceptionScheduleGridColumn
            // 
            this._exceptionScheduleGridColumn.Caption = "����������";
            this._exceptionScheduleGridColumn.FieldName = "Schedule";
            this._exceptionScheduleGridColumn.Name = "_exceptionScheduleGridColumn";
            this._exceptionScheduleGridColumn.Tag = "����������#r";
            this._exceptionScheduleGridColumn.Visible = true;
            this._exceptionScheduleGridColumn.VisibleIndex = 1;
            // 
            // _schedulesGridView
            // 
            this._schedulesGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._nameGridColumn,
            this._isHolidayGridColumn,
            this._startGridColumn,
            this._breakStartGridColumn,
            this._breakFinishGridColumn,
            this._finishGridColumn});
            this._schedulesGridView.GridControl = this._gridControl;
            this._schedulesGridView.Name = "_schedulesGridView";
            this._schedulesGridView.OptionsView.ShowGroupPanel = false;
            this._schedulesGridView.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.SchedulesGridViewValidateRow);
            this._schedulesGridView.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.SchedulesGridViewRowUpdated);
            this._schedulesGridView.CustomColumnDisplayText += new DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventHandler(this.SchedulesGridViewCustomColumnDisplayText);
            this._schedulesGridView.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.SchedulesGridViewValidatingEditor);
            // 
            // _nameGridColumn
            // 
            this._nameGridColumn.Caption = "��������";
            this._nameGridColumn.FieldName = "Name";
            this._nameGridColumn.Name = "_nameGridColumn";
            this._nameGridColumn.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this._nameGridColumn.Tag = "��������#r";
            this._nameGridColumn.Visible = true;
            this._nameGridColumn.VisibleIndex = 0;
            // 
            // _isHolidayGridColumn
            // 
            this._isHolidayGridColumn.Caption = "��������";
            this._isHolidayGridColumn.ColumnEdit = this._checkEdit;
            this._isHolidayGridColumn.FieldName = "IsHoliday";
            this._isHolidayGridColumn.Name = "_isHolidayGridColumn";
            this._isHolidayGridColumn.Tag = "��������#r";
            this._isHolidayGridColumn.Visible = true;
            this._isHolidayGridColumn.VisibleIndex = 1;
            // 
            // _checkEdit
            // 
            this._checkEdit.AutoHeight = false;
            this._checkEdit.Caption = "Check";
            this._checkEdit.Name = "_checkEdit";
            this._checkEdit.CheckedChanged += new System.EventHandler(this.CheckEditCheckedChanged);
            // 
            // _startGridColumn
            // 
            this._startGridColumn.Caption = "������ ���";
            this._startGridColumn.ColumnEdit = this._timeEdit;
            this._startGridColumn.FieldName = "Start";
            this._startGridColumn.Name = "_startGridColumn";
            this._startGridColumn.Tag = "������ ���#r";
            this._startGridColumn.Visible = true;
            this._startGridColumn.VisibleIndex = 2;
            // 
            // _timeEdit
            // 
            this._timeEdit.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this._timeEdit.AutoHeight = false;
            this._timeEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this._timeEdit.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.DisplayText;
            this._timeEdit.Mask.EditMask = "HH:mm";
            this._timeEdit.Name = "_timeEdit";
            this._timeEdit.EditValueChanged += new System.EventHandler(this.TimeEditEditValueChanged);
            // 
            // _breakStartGridColumn
            // 
            this._breakStartGridColumn.Caption = "������ ��������";
            this._breakStartGridColumn.ColumnEdit = this._timeEdit;
            this._breakStartGridColumn.FieldName = "BreakStart";
            this._breakStartGridColumn.Name = "_breakStartGridColumn";
            this._breakStartGridColumn.Tag = "������ ��������#r";
            this._breakStartGridColumn.Visible = true;
            this._breakStartGridColumn.VisibleIndex = 3;
            // 
            // _breakFinishGridColumn
            // 
            this._breakFinishGridColumn.Caption = "��������� ��������";
            this._breakFinishGridColumn.ColumnEdit = this._timeEdit;
            this._breakFinishGridColumn.FieldName = "BreakFinish";
            this._breakFinishGridColumn.Name = "_breakFinishGridColumn";
            this._breakFinishGridColumn.Tag = "��������� ��������#r";
            this._breakFinishGridColumn.Visible = true;
            this._breakFinishGridColumn.VisibleIndex = 4;
            // 
            // _finishGridColumn
            // 
            this._finishGridColumn.Caption = "��������� ���";
            this._finishGridColumn.ColumnEdit = this._timeEdit;
            this._finishGridColumn.FieldName = "Finish";
            this._finishGridColumn.Name = "_finishGridColumn";
            this._finishGridColumn.Tag = "��������� ���#r";
            this._finishGridColumn.Visible = true;
            this._finishGridColumn.VisibleIndex = 5;
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this._gridControl;
            this.gridView2.Name = "gridView2";
            // 
            // SchedulesControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._gridControl);
            this.Name = "SchedulesControl";
            this.Size = new System.Drawing.Size(534, 467);
            this.Tag = "����������#r";
            this.Load += new System.EventHandler(this.SchedulesControlLoad);
            ((System.ComponentModel.ISupportInitialize)(this._daySchedulesGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._exceptionSchedulesGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._schedulesGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._checkEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._timeEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl _gridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView _schedulesGridView;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn _nameGridColumn;
        private System.Windows.Forms.ImageList _imageList;
        private DevExpress.XtraGrid.Columns.GridColumn _startGridColumn;
        private DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit _timeEdit;
        private DevExpress.XtraGrid.Columns.GridColumn _breakStartGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _breakFinishGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _finishGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _isHolidayGridColumn;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit _checkEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView _daySchedulesGridView;
        private DevExpress.XtraGrid.Columns.GridColumn _dayOfWeekGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _dayScheduleGridColumn;
        private DevExpress.XtraGrid.Views.Grid.GridView _exceptionSchedulesGridView;
        private DevExpress.XtraGrid.Columns.GridColumn _dateGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _exceptionScheduleGridColumn;
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Curriculum.AccessAreas;
using Curriculum.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using Eureca.Integrator.Common;
using Eureca.Integrator.Extensions;
using Eureca.Utility.Extensions;
using Eureca.Utility.Helper;
using NHibernate.Criterion;
using NHibernate.Hql.Ast.ANTLR;
using MessageBox = Eureca.Integrator.Utility.Forms.MessageBox;

namespace Curriculum.Controls
{
    public partial class RolesListControl : ControlWithAccessAreas
    {
        public RolesListControl( )
        {
            InitializeComponent( );
            Program.DbSingletone.SubmitChanges( );
            _roleGridControl.DataSource = Program.DbSingletone.
                Roles.Where(role => true);
            Load += RolesListControl_Load;
        }

        void RolesListControl_Load( object sender, EventArgs e )
        {
            _roleGridControl.EmbeddedNavigator.ButtonClick += _blancsGrid_EmbeddedNavigator_ButtonClick;
        }

        private void _blancsGrid_EmbeddedNavigator_ButtonClick( object sender, NavigatorButtonClickEventArgs e )
        {
            //int index = -1;
            var selectedRow = _roleView.GetSelectedItem( ) as Role;
            var wishedSelection = selectedRow;
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Append:
                    var newRole = new Role();
                    EditRole(newRole, true);
                    break;
                case NavigatorButtonType.Edit:
                    if (selectedRow == null)
                        return;
                    EditRole(selectedRow);
                    break;
                case NavigatorButtonType.Remove:
                    if (selectedRow == null)
                        return;
                    DeleteRole(selectedRow);
                    break;
                case NavigatorButtonType.Custom:
                     var tag = e.Button.Tag as string;

                    if (tag != null)
                    {
                        tag = tag.Split( '#' )[0];
                        switch (tag)
                        {
                            case "Edit":
                                EditRole(selectedRow);
                                break;
                            case "Copy":
                                Role role = CopyRole(selectedRow,Program.DbSingletone.Roles);
                                Program.DbSingletone.Roles.InsertOnSubmit(role);
                                Program.DbSingletone.SubmitChanges();
                                wishedSelection = role;
                                break;
                        }
                    }
                    break;
            }
            e.Handled = true;
            RefreshGrid(wishedSelection);
        }

        private void RefreshGrid(Role wishedSelection)
        {
            _roleGridControl.BeginUpdate();
            _roleGridControl.DataSource = Program.DbSingletone.Roles.Where(role => true);
            _roleGridControl.EndUpdate();
            if (wishedSelection != null)
            {
                int rowHandle = _roleView.LocateByValue("RoleId", wishedSelection.RoleId);
                if (rowHandle != GridControl.InvalidRowHandle)
                    _roleView.FocusedRowHandle = rowHandle;
            }
        }

        private Role CopyRole( Role selectedRow , IEnumerable<Role> allRoles )
        {
            Role newRole = new Role
            {
                Name = GenerateName(selectedRow.Name,allRoles.Select(role => role.Name)),
                ParentRole = selectedRow.ParentRole
            };
            newRole.RolesPermissions.AddRange( selectedRow.RolesPermissions.Select( permission => new RolesPermission( ) {Tag =  permission.Tag, Permission = permission.Permission} ) );
            return newRole;
        }

        private string GenerateName(string originalName,IEnumerable<string> unallowedNames)
        {
            string newName ="{0} копия".FormatString(originalName);
            string newNameTemplate =newName + "({0})";
            int counter = 1;
            while (unallowedNames.Contains(newName))
            {
                newName = newNameTemplate.FormatString(counter++);
            }
            return newName;
        }

        private static void DeleteRole(Role selectedRow)
        {
            if (selectedRow.Admin && selectedRow.Name == "Администратор")
            {
                MessageBox.ShowValidationError("Нельзя удалить роль администратора");
                return;
            }
            if(MessageBox.Confirm("Вы уверены, что хотите удалить роль и все дочернии роли?"))
                OrmHelper.DeleteRole(selectedRow);  
            //Program.DbSingletone.SubmitChanges();
        }

        private bool EditRole( Role updatedRole,bool isNew = false )
        {
            var form = new EditRoleForm(updatedRole,Program.DbSingletone.Roles){HierarchyParent = this};
            bool res = form.ShowDialog() == DialogResult.OK;    
            if (res)
            {
                if (isNew)
                {
                    Program.DbSingletone.Roles.InsertOnSubmit( updatedRole );
                    Program.DbSingletone.SubmitChanges();
                }
                UpdateRole(updatedRole,form);
                Program.DbSingletone.SubmitChanges();
            }
            else 
                Program.DiscardDataChanges();
            return res;
        }

        private static void UpdateRole(Role role, EditRoleForm editorForm)
        {
            var form = new MarqueeProgressForm("Обновление роли...", (f) =>
            {
                BindingListWithRemoveEvent<Permission> bindingListWithRemoveEvent = editorForm.Permissions;
                foreach (var removedPermission in bindingListWithRemoveEvent.RemovedItems)
                {
                    var rPermission =
                        Program.DbSingletone.RolesPermissions.Single(
                            item => item.RoleId == role.RoleId && item.TagId == removedPermission.TagId);
                    Program.DbSingletone.RolesPermissions.DeleteOnSubmit(rPermission);
                }

                foreach (var changedPermission in bindingListWithRemoveEvent.ChangedItems)
                {
                    var rPermission =
                        Program.DbSingletone.RolesPermissions.Single(
                            item => item.RoleId == role.RoleId && item.TagId == changedPermission.TagId);
                    rPermission.Permission = (int) changedPermission.PermissionType;
                }

                IEnumerable<RolesPermission> rolesPermissions =
                    bindingListWithRemoveEvent.AddedItems.Select(
                        permission =>
                            new RolesPermission()
                            {
                                Role = role,
                                TagId = permission.TagId,
                                Permission = (int) permission.PermissionType
                            });
                role.RolesPermissions.AddRange(rolesPermissions);

                Program.DbSingletone.SubmitChanges();
            });
            form.ShowDialog();
        }


        public override IEnumerable<ISKOCBCTDNBTIDED> GetHiddenChilds( bool withLazy )
        {
            var hiddenControls = new List<ISKOCBCTDNBTIDED>( );
            if (withLazy)
            {
                var dummy = EditRoleForm.CreateDummy();
                dummy.HierarchyParent = this;
                hiddenControls.Add( new ControlInterfaceImplementation( dummy ) );                 
            }
            return hiddenControls;
        }

        private void _roleView_FocusedRowChanged( object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e )
        {
            // Enable/disable edit button (is there user to edit).
            var navigatorCustomButtons = _roleGridControl.EmbeddedNavigator.Buttons.CustomButtons;
            navigatorCustomButtons[0].Enabled = navigatorCustomButtons[1].Enabled = e.FocusedRowHandle >= 0;
        }
    }
}

namespace Curriculum.Settings
{
    partial class AliasesControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AliasesControl));
            this._gridControl = new DevExpress.XtraGrid.GridControl();
            this._imageList = new System.Windows.Forms.ImageList(this.components);
            this._aliasesGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._defaultGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._aliasGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)(this._gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._aliasesGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // _gridControl
            // 
            this._gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._gridControl.EmbeddedNavigator.Buttons.Append.Tag = "������� ����������#ra";
            this._gridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this._gridControl.EmbeddedNavigator.Buttons.Edit.Tag = "������� ����������#rc";
            this._gridControl.EmbeddedNavigator.Buttons.EndEdit.Tag = "������� ����������#rc";
            this._gridControl.EmbeddedNavigator.Buttons.CancelEdit.Tag = "������� ����������#rc";
            this._gridControl.EmbeddedNavigator.Buttons.ImageList = this._imageList;
            this._gridControl.EmbeddedNavigator.Buttons.Remove.Tag = "������� ����������#rd";
            this._gridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this._gridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "�������� ������", "Refresh")});
            this._gridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.GridControl_EmbeddedNavigator_ButtonClick);
            this._gridControl.Location = new System.Drawing.Point(0, 0);
            this._gridControl.MainView = this._aliasesGridView;
            this._gridControl.Name = "_gridControl";
            this._gridControl.Size = new System.Drawing.Size(586, 483);
            this._gridControl.TabIndex = 0;
            this._gridControl.Tag = "������� ����������#r";
            this._gridControl.UseEmbeddedNavigator = true;
            this._gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this._aliasesGridView,
            this.gridView2});
            // 
            // _imageList
            // 
            this._imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("_imageList.ImageStream")));
            this._imageList.TransparentColor = System.Drawing.Color.White;
            this._imageList.Images.SetKeyName(0, "Refresh16.png");
            // 
            // _aliasesGridView
            // 
            this._aliasesGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._defaultGridColumn,
            this._aliasGridColumn});
            this._aliasesGridView.GridControl = this._gridControl;
            this._aliasesGridView.Name = "_aliasesGridView";
            this._aliasesGridView.OptionsView.ShowGroupPanel = false;
            this._aliasesGridView.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this._defaultGridColumn, DevExpress.Data.ColumnSortOrder.Ascending)});
            this._aliasesGridView.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.AliasesGridView_RowUpdated);
            this._aliasesGridView.CustomUnboundColumnData += new DevExpress.XtraGrid.Views.Base.CustomColumnDataEventHandler(this.AliasesGridView_CustomUnboundColumnData);
            this._aliasesGridView.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.AliasesGridView_ValidatingEditor);
            // 
            // _defaultGridColumn
            // 
            this._defaultGridColumn.Caption = "�� ���������";
            this._defaultGridColumn.FieldName = "_defaultGridColumn";
            this._defaultGridColumn.Name = "_defaultGridColumn";
            this._defaultGridColumn.OptionsColumn.AllowEdit = false;
            this._defaultGridColumn.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this._defaultGridColumn.OptionsColumn.ReadOnly = true;
            this._defaultGridColumn.Tag = "�� ���������#r";
            this._defaultGridColumn.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this._defaultGridColumn.Visible = true;
            this._defaultGridColumn.VisibleIndex = 0;
            // 
            // _aliasGridColumn
            // 
            this._aliasGridColumn.Caption = "���������";
            this._aliasGridColumn.FieldName = "Value";
            this._aliasGridColumn.Name = "_aliasGridColumn";
            this._aliasGridColumn.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this._aliasGridColumn.Tag = "���������#r";
            this._aliasGridColumn.Visible = true;
            this._aliasGridColumn.VisibleIndex = 1;
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this._gridControl;
            this.gridView2.Name = "gridView2";
            // 
            // AliasesControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._gridControl);
            this.Name = "AliasesControl";
            this.Size = new System.Drawing.Size(586, 483);
            this.Tag = "����������#r";
            this.Load += new System.EventHandler(this.AliasesControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this._gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._aliasesGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl _gridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView _aliasesGridView;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn _defaultGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _aliasGridColumn;
        private System.Windows.Forms.ImageList _imageList;
    }
}

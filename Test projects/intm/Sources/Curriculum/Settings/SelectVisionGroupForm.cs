﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Curriculum.Settings
{
    public partial class SelectVisionGroupForm : Form
    {
        List<UsersGroup> _usersList = new List<UsersGroup>();

        public IEnumerable<UsersGroup> SelectedGroups
        {
            get
            {
                var selectedHandles = _groupsView.GetSelectedRows();
                return selectedHandles.Select(rowHandle => _groupsView.GetRow(rowHandle) as UsersGroup );
            }
            set
            {
                _groupsView.ClearSelection();
                foreach (var usersGroup in value??Enumerable.Empty<UsersGroup>())
                {
                    _groupsView.SelectRow(_groupsView.GetRowHandle(_usersList.IndexOf(usersGroup)));
                }
            }
        }

        public SelectVisionGroupForm(IEnumerable<UsersGroup> groups )
        {
            _usersList = groups.ToList();
            InitializeComponent( );
            _groupsGridControl.DataSource = _usersList;
        }
    }
}

using System;
using System.Data.Common;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using Eureca.Integrator.Common;

namespace Curriculum.Settings
{
    /// <summary>
    /// Represents exception schedules control.
    /// </summary>
    public partial class ExceptionSchedulesControl : ControlWithAccessAreas
    {
        /// <summary>
        /// Initializes new instance of exception schedules control.
        /// </summary>
        public ExceptionSchedulesControl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initializes grid control data source.
        /// </summary>
        private void InitDataSource()
        {
            _gridControl.DataSource = Program.DbSingletone.GetDataSource(typeof (ExceptionSchedule));
            _schedulesGridLookUpEdit.DataSource = Program.DbSingletone.GetDataSource(typeof (Schedule));
        }

        /// <summary>
        /// Refreshes data source.
        /// </summary>
        private void RefreshDataSource()
        {
            Program.DbSingletone.RefreshDataSource(typeof (ExceptionSchedule));
            Program.DbSingletone.RefreshDataSource(typeof (Schedule));

            _exceptionSchedulesGridView.RefreshData();
            _schedulesGridLookUpEdit.View.RefreshData();
        }

        /// <summary>
        /// Handles exception schedules control load event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void ExceptionSchedulesControlLoad(object sender, EventArgs e)
        {
            InitDataSource();
        }

        /// <summary>
        /// Grid control embedded navigator button click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param> 
        private void GridControlEmbeddedNavigatorButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Remove:

                    e.Handled = true;

                    var exceptionSchedule = _exceptionSchedulesGridView.GetFocusedRow() as ExceptionSchedule;

                    if (exceptionSchedule != null)
                    {
                        if (XtraMessageBox.Show(this,
                                                "�� �������, ��� ������ ������� ���������� ����������?",
                                                "������������� ��������",
                                                MessageBoxButtons.YesNo,
                                                MessageBoxIcon.Question,
                                                MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                        {
                            try
                            {
                                Program.DbSingletone.ExceptionSchedules.DeleteOnSubmit(exceptionSchedule);
                                Program.DbSingletone.SubmitChanges();

                                e.Handled = false;
                            }
                            catch (DbException ex)
                            {
                                Program.HandleDbException(ex);
                            }
                        }
                    }

                    break;

                case NavigatorButtonType.Custom:
                    {
                        var tag = e.Button.Tag as string;

                        if (tag != null)
                        {
                            if (tag == "Refresh")
                            {
                                try
                                {
                                    Program.DbSingletone.SubmitChanges();
                                }
                                catch (DbException ex)
                                {
                                    Program.HandleDbException(ex);
                                }
                                finally
                                {
                                    RefreshDataSource();
                                }
                            }
                        }
                    }
                    break;
            }
        }

        private void ExceptionSchedulesGridViewValidateRow(object sender, ValidateRowEventArgs e)
        {
            var exceptionSchedule = _exceptionSchedulesGridView.GetFocusedRow() as ExceptionSchedule;

            if (exceptionSchedule != null && exceptionSchedule.Schedule == null)
            {
                e.ErrorText = "���������� ������� ����������.";
                e.Valid = false;
            }
        }

        private void ExceptionSchedulesGridViewRowUpdated(object sender, RowObjectEventArgs e)
        {
            if (e.Row != null)
            {
                try
                {
                    // Submit changes.
                    Program.DbSingletone.SubmitChanges();
                }
                catch (DbException ex)
                {
                    Program.HandleDbException(ex);
                }
            }
        }

        private void ExceptionSchedulesGridViewInitNewRow(object sender, InitNewRowEventArgs e)
        {
            var exceptionSchedule = _exceptionSchedulesGridView.GetRow(e.RowHandle) as ExceptionSchedule;

            if (exceptionSchedule != null) exceptionSchedule.Date = DateTime.Now;
        }
    }
}
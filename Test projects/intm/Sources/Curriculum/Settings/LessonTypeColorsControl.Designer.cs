namespace Curriculum.Settings
{
    partial class LessonTypeColorsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LessonTypeColorsControl));
            this._gridControl = new DevExpress.XtraGrid.GridControl();
            this._imageList = new System.Windows.Forms.ImageList(this.components);
            this._colorsGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._lessonTypeGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._fillColorGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._fillColorEdit = new DevExpress.XtraEditors.Repository.RepositoryItemColorEdit();
            this._borderColorGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._borderColorEdit = new DevExpress.XtraEditors.Repository.RepositoryItemColorEdit();
            ((System.ComponentModel.ISupportInitialize)(this._gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._colorsGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._fillColorEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._borderColorEdit)).BeginInit();
            this.SuspendLayout();
            // 
            // _gridControl
            // 
            this._gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._gridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this._gridControl.EmbeddedNavigator.Buttons.CancelEdit.Tag = "������� ����� �������#rc";
            this._gridControl.EmbeddedNavigator.Buttons.Edit.Tag = "������� ����� �������#rc";
            this._gridControl.EmbeddedNavigator.Buttons.EndEdit.Tag = "������� ����� �������#rc";
            this._gridControl.EmbeddedNavigator.Buttons.ImageList = this._imageList;
            this._gridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this._gridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "�������� ������", "Refresh")});
            this._gridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.GridControl_EmbeddedNavigator_ButtonClick);
            this._gridControl.Location = new System.Drawing.Point(0, 0);
            this._gridControl.MainView = this._colorsGridView;
            this._gridControl.Name = "_gridControl";
            this._gridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this._fillColorEdit,
            this._borderColorEdit});
            this._gridControl.Size = new System.Drawing.Size(635, 543);
            this._gridControl.TabIndex = 0;
            this._gridControl.Tag = "..\\������� ����� �������#r";
            this._gridControl.UseEmbeddedNavigator = true;
            this._gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this._colorsGridView});
            // 
            // _imageList
            // 
            this._imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("_imageList.ImageStream")));
            this._imageList.TransparentColor = System.Drawing.Color.White;
            this._imageList.Images.SetKeyName(0, "Refresh16.png");
            // 
            // _colorsGridView
            // 
            this._colorsGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._lessonTypeGridColumn,
            this._fillColorGridColumn,
            this._borderColorGridColumn});
            this._colorsGridView.GridControl = this._gridControl;
            this._colorsGridView.Name = "_colorsGridView";
            this._colorsGridView.OptionsDetail.EnableMasterViewMode = false;
            this._colorsGridView.OptionsView.ShowGroupPanel = false;
            this._colorsGridView.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this._lessonTypeGridColumn, DevExpress.Data.ColumnSortOrder.Ascending)});
            this._colorsGridView.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.ColorsGridView_RowUpdated);
            // 
            // _lessonTypeGridColumn
            // 
            this._lessonTypeGridColumn.Caption = "��� �������";
            this._lessonTypeGridColumn.FieldName = "Name";
            this._lessonTypeGridColumn.Name = "_lessonTypeGridColumn";
            this._lessonTypeGridColumn.OptionsColumn.AllowEdit = false;
            this._lessonTypeGridColumn.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this._lessonTypeGridColumn.OptionsColumn.ReadOnly = true;
            this._lessonTypeGridColumn.Tag = "��� �������#r";
            this._lessonTypeGridColumn.Visible = true;
            this._lessonTypeGridColumn.VisibleIndex = 0;
            // 
            // _fillColorGridColumn
            // 
            this._fillColorGridColumn.Caption = "���� �������";
            this._fillColorGridColumn.ColumnEdit = this._fillColorEdit;
            this._fillColorGridColumn.FieldName = "FillColor";
            this._fillColorGridColumn.Name = "_fillColorGridColumn";
            this._fillColorGridColumn.Tag = "���� �������#r";
            this._fillColorGridColumn.Visible = true;
            this._fillColorGridColumn.VisibleIndex = 1;
            // 
            // _fillColorEdit
            // 
            this._fillColorEdit.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this._fillColorEdit.AutoHeight = false;
            this._fillColorEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._fillColorEdit.Name = "_fillColorEdit";
            // 
            // _borderColorGridColumn
            // 
            this._borderColorGridColumn.Caption = "���� �������";
            this._borderColorGridColumn.ColumnEdit = this._borderColorEdit;
            this._borderColorGridColumn.FieldName = "BorderColor";
            this._borderColorGridColumn.Name = "_borderColorGridColumn";
            // 
            // _borderColorEdit
            // 
            this._borderColorEdit.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this._borderColorEdit.AutoHeight = false;
            this._borderColorEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._borderColorEdit.Name = "_borderColorEdit";
            // 
            // LessonTypeColorsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._gridControl);
            this.Name = "LessonTypeColorsControl";
            this.Size = new System.Drawing.Size(635, 543);
            this.Tag = "������� ����� �������#r";
            this.Load += new System.EventHandler(this.LessonTypeColorsControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this._gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._colorsGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._fillColorEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._borderColorEdit)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl _gridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView _colorsGridView;
        private DevExpress.XtraGrid.Columns.GridColumn _lessonTypeGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _fillColorGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _borderColorGridColumn;
        private DevExpress.XtraEditors.Repository.RepositoryItemColorEdit _fillColorEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemColorEdit _borderColorEdit;
        private System.Windows.Forms.ImageList _imageList;
    }
}

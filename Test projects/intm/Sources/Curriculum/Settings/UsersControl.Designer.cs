namespace Curriculum.Settings
{
    partial class UsersControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UsersControl));
            this._gridControl = new DevExpress.XtraGrid.GridControl();
            this._imageList = new System.Windows.Forms.ImageList(this.components);
            this._usersGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._nameGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._userIdColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this._gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._usersGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // _gridControl
            // 
            this._gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._gridControl.EmbeddedNavigator.Buttons.Append.Tag = "..\\Пользователи#ra";
            this._gridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this._gridControl.EmbeddedNavigator.Buttons.Edit.Tag = "..\\Пользователи#rc";
            this._gridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this._gridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this._gridControl.EmbeddedNavigator.Buttons.ImageList = this._imageList;
            this._gridControl.EmbeddedNavigator.Buttons.Remove.Tag = "..\\Пользователи#rd";
            this._gridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, false, true, "Редактировать параметры пользователя", "Edit#rc"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Обновить данные", "Refresh")});
            this._gridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.GridControl_EmbeddedNavigator_ButtonClick);
            this._gridControl.Location = new System.Drawing.Point(0, 0);
            this._gridControl.MainView = this._usersGridView;
            this._gridControl.Name = "_gridControl";
            this._gridControl.Size = new System.Drawing.Size(534, 467);
            this._gridControl.TabIndex = 0;
            this._gridControl.Tag = "..\\Пользователи#r";
            this._gridControl.UseEmbeddedNavigator = true;
            this._gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this._usersGridView,
            this.gridView2});
            // 
            // _imageList
            // 
            this._imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("_imageList.ImageStream")));
            this._imageList.TransparentColor = System.Drawing.Color.White;
            this._imageList.Images.SetKeyName(0, "Edit16.png");
            this._imageList.Images.SetKeyName(1, "Refresh16.png");
            // 
            // _usersGridView
            // 
            this._usersGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._userIdColumn,
            this._nameGridColumn});
            this._usersGridView.GridControl = this._gridControl;
            this._usersGridView.Name = "_usersGridView";
            this._usersGridView.OptionsDetail.EnableMasterViewMode = false;
            this._usersGridView.OptionsView.ShowGroupPanel = false;
            this._usersGridView.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this._nameGridColumn, DevExpress.Data.ColumnSortOrder.Ascending)});
            this._usersGridView.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.UsersGridView_FocusedRowChanged);
            // 
            // _nameGridColumn
            // 
            this._nameGridColumn.Caption = "Пользователи";
            this._nameGridColumn.FieldName = "Name";
            this._nameGridColumn.Name = "_nameGridColumn";
            this._nameGridColumn.OptionsColumn.AllowEdit = false;
            this._nameGridColumn.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this._nameGridColumn.OptionsColumn.ReadOnly = true;
            this._nameGridColumn.Visible = true;
            this._nameGridColumn.VisibleIndex = 0;
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this._gridControl;
            this.gridView2.Name = "gridView2";
            // 
            // _userIdColumn
            // 
            this._userIdColumn.Caption = "gridColumn1";
            this._userIdColumn.FieldName = "UserId";
            this._userIdColumn.Name = "_userIdColumn";
            // 
            // UsersControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._gridControl);
            this.Name = "UsersControl";
            this.Size = new System.Drawing.Size(534, 467);
            this.Tag = "Пользователи#r";
            this.Load += new System.EventHandler(this.UsersControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this._gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._usersGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl _gridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView _usersGridView;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn _nameGridColumn;
        private System.Windows.Forms.ImageList _imageList;
        private DevExpress.XtraGrid.Columns.GridColumn _userIdColumn;
    }
}

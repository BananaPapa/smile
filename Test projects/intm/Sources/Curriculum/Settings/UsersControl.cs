using System;
using System.Data.Linq;
using System.Linq;
using System.Collections.Generic;
using System.Data.Common;
using System.Windows.Forms;
using Curriculum.Forms;
using DevExpress.Data.WcfLinq.Helpers;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using Eureca.Integrator.Common;
using Eureca.Integrator.Extensions;
using DevExpress.XtraGrid;

namespace Curriculum.Settings
{
    /// <summary>
    /// Represents users control.
    /// </summary>
    public partial class UsersControl : ControlWithAccessAreas
    {
        /// <summary>
        /// Initializes new instance of users control.
        /// </summary>
        public UsersControl()
        {
            ManualFilter = true;
            InitializeComponent();
        }

        /// <summary>
        /// Gets currently focused user.
        /// </summary>
        private User FocusedUser
        {
            get { return _usersGridView.GetFocusedRow() as User; }
        }

        /// <summary>
        /// Initializes grid control data source.
        /// </summary>
        private void InitDataSource()
        {
            _gridControl.DataSource =  Program.DbSingletone.Users.Where(user=>true);
        }

        /// <summary>
        /// Refreshes data source.
        /// </summary>
        private void RefreshDataSource(User wishedSelection)
        {
            _gridControl.BeginInit();
            _gridControl.DataSource = Program.DbSingletone.Users.Where( user => true );
            _gridControl.EndInit();
            if (wishedSelection != null)
            {
                int rowHandle = _usersGridView.LocateByValue( "UserId", wishedSelection.UserId );
                if (rowHandle != GridControl.InvalidRowHandle)
                    _usersGridView.FocusedRowHandle = rowHandle;
            }
        }
 
        /// <summary>
        /// Handles users control load event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void UsersControl_Load(object sender, EventArgs e)
        {
            InitDataSource();
        }

        /// <summary>
        /// Handles users grid view focused row changed event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void UsersGridView_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            // Enable/disable edit button (is there user to edit).
            _gridControl.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = FocusedUser != null;
        }

        /// <summary>
        /// Grid control embedded navigator button click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param> 
        private void GridControl_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            UserEditForm form;
            User wishedSelection = null;
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Append:

                    e.Handled = true;

                    form = new UserEditForm(Program.DbSingletone.Users.ToList() ){HierarchyParent = this};
                    form.ShowDialog(this);
                    wishedSelection = form.User;
                    break;

                case NavigatorButtonType.Remove:

                    e.Handled = true;

                    User userToDelete = FocusedUser;

                    if (userToDelete != null)
                    {
                        if (userToDelete.UserId == Program.CurrentUser.UserId)
                        {
                            XtraMessageBox.Show(
                                "�� �� ������ ������� ������ ����!",
                                "������",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                        }
                        else if (XtraMessageBox.Show(
                            "�� �������, ��� ������ ������� ����������� ������������?" +
                            Environment.NewLine +
                            "���������, ��� ��� ���� ������� ������� �� �������� � ������ ������.",
                            "������������� ��������",
                            MessageBoxButtons.YesNo,
                            MessageBoxIcon.Question,
                            MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                        {
                            try
                            {
                                Program.DbSingletone.Users.DeleteOnSubmit(userToDelete);
                                Program.DbSingletone.SubmitChanges();

                                //e.Handled = false;
                            }
                            catch (DbException ex)
                            {
                                Program.HandleDbException(ex);
                            }
                        }
                    }

                    break;

                case NavigatorButtonType.Custom:

                    var tag = e.Button.Tag as string;

                    if (tag != null)
                    {
                        tag = tag.Split('#')[0];
                        switch (tag)
                        {
                            case "Edit":
                                User userToEdit = FocusedUser;

                                if (userToEdit != null)
                                {
                                    form = new UserEditForm(userToEdit,Program.DbSingletone.Users.Where(user=>true)){HierarchyParent = this};
                                    if (form.ShowDialog(this) == DialogResult.OK)
                                    {
                                        try
                                        {
                                            Program.DbSingletone.SubmitChanges();
                                        }
                                        catch (DbException ex)
                                        {
                                            Program.HandleDbException(ex);
                                        }

                                        _usersGridView.RefreshRow(_usersGridView.FocusedRowHandle);
                                    }
                                    wishedSelection = form.User;
                                }

                                break;

                            case "Refresh":
                                try
                                {
                                    Program.DbSingletone.SubmitChanges();
                                }
                                catch (DbException ex)
                                {
                                    Program.HandleDbException(ex);
                                }
                                finally
                                {
                                    RefreshDataSource(null);
                                }
                                break;
                        }
                    }

                    break;
            }
            RefreshDataSource( wishedSelection );
        }


        public override IEnumerable<ISKOCBCTDNBTIDED> GetHiddenChilds( bool withLazy )
        {
            var hiddenControls = new List<ISKOCBCTDNBTIDED>( );
            if(withLazy)
            {
                var userEditForm = new UserEditForm(Program.DbSingletone.Users.Where(user=>true)){HierarchyParent = this};
                hiddenControls.Add( new ControlInterfaceImplementation( userEditForm ) );
            }
            return hiddenControls;
        }
    }
}
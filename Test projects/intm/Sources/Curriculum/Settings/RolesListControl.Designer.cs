﻿namespace Curriculum.Controls
{
    partial class RolesListControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if (disposing && ( components != null ))
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RolesListControl));
            this._roleGridControl = new DevExpress.XtraGrid.GridControl();
            this._imageList = new System.Windows.Forms.ImageList(this.components);
            this._roleView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._rolesColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._parentRoleColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._briefAlows = new DevExpress.XtraGrid.Columns.GridColumn();
            this._roleIdColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._repoComboRoleParent = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            ((System.ComponentModel.ISupportInitialize)(this._roleGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._roleView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._repoComboRoleParent)).BeginInit();
            this.SuspendLayout();
            // 
            // _roleGridControl
            // 
            this._roleGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._roleGridControl.EmbeddedNavigator.Buttons.ImageList = this._imageList;
            this._roleGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, false, true, "Редактировать роль", "Edit#rc"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, false, true, "Копировать роль", "Copy#ra")});
            this._roleGridControl.Location = new System.Drawing.Point(0, 0);
            this._roleGridControl.MainView = this._roleView;
            this._roleGridControl.Name = "_roleGridControl";
            this._roleGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this._repoComboRoleParent});
            this._roleGridControl.Size = new System.Drawing.Size(561, 516);
            this._roleGridControl.TabIndex = 0;
            this._roleGridControl.Tag = "..\\Таблица Ролей#r";
            this._roleGridControl.UseEmbeddedNavigator = true;
            this._roleGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this._roleView});
            // 
            // _imageList
            // 
            this._imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("_imageList.ImageStream")));
            this._imageList.TransparentColor = System.Drawing.Color.White;
            this._imageList.Images.SetKeyName(0, "pen16.png");
            this._imageList.Images.SetKeyName(1, "stock_copy.png");
            // 
            // _roleView
            // 
            this._roleView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._rolesColumn,
            this._parentRoleColumn,
            this._briefAlows,
            this._roleIdColumn});
            this._roleView.GridControl = this._roleGridControl;
            this._roleView.Name = "_roleView";
            this._roleView.OptionsDetail.EnableMasterViewMode = false;
            this._roleView.OptionsDetail.ShowDetailTabs = false;
            this._roleView.OptionsView.ShowGroupPanel = false;
            this._roleView.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this._roleView_FocusedRowChanged);
            // 
            // _rolesColumn
            // 
            this._rolesColumn.Caption = "Роль";
            this._rolesColumn.FieldName = "Name";
            this._rolesColumn.Name = "_rolesColumn";
            this._rolesColumn.OptionsColumn.AllowEdit = false;
            this._rolesColumn.OptionsColumn.ReadOnly = true;
            this._rolesColumn.Visible = true;
            this._rolesColumn.VisibleIndex = 0;
            // 
            // _parentRoleColumn
            // 
            this._parentRoleColumn.Caption = "Родительская роль";
            this._parentRoleColumn.FieldName = "ParentRole.Name";
            this._parentRoleColumn.Name = "_parentRoleColumn";
            this._parentRoleColumn.OptionsColumn.AllowEdit = false;
            this._parentRoleColumn.OptionsColumn.ReadOnly = true;
            this._parentRoleColumn.Visible = true;
            this._parentRoleColumn.VisibleIndex = 1;
            // 
            // _briefAlows
            // 
            this._briefAlows.Caption = "Доступные Окна";
            this._briefAlows.Name = "_briefAlows";
            // 
            // _roleIdColumn
            // 
            this._roleIdColumn.FieldName = "RoleId";
            this._roleIdColumn.Name = "_roleIdColumn";
            // 
            // _repoComboRoleParent
            // 
            this._repoComboRoleParent.AutoHeight = false;
            this._repoComboRoleParent.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._repoComboRoleParent.Name = "_repoComboRoleParent";
            // 
            // RolesListControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._roleGridControl);
            this.Name = "RolesListControl";
            this.Size = new System.Drawing.Size(561, 516);
            this.Tag = "Таблица Ролей#r";
            ((System.ComponentModel.ISupportInitialize)(this._roleGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._roleView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._repoComboRoleParent)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl _roleGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView _roleView;
        private DevExpress.XtraGrid.Columns.GridColumn _rolesColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _parentRoleColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _briefAlows;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox _repoComboRoleParent;
        private System.Windows.Forms.ImageList _imageList;
        private DevExpress.XtraGrid.Columns.GridColumn _roleIdColumn;
    }
}

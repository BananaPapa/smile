﻿namespace Curriculum.Controls
{
    partial class UsersGroupsList
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if (disposing && ( components != null ))
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UsersGroupsList));
            this._usersGroupsGrid = new DevExpress.XtraGrid.GridControl();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this._groupNamesView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._groupNameColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._groupBriefColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._groupUsersCardView = new DevExpress.XtraGrid.Views.Card.CardView();
            this._userNameColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this._usersGroupsGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._groupNamesView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._groupUsersCardView)).BeginInit();
            this.SuspendLayout();
            // 
            // _usersGroupsGrid
            // 
            this._usersGroupsGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._usersGroupsGrid.EmbeddedNavigator.Buttons.ImageList = this.imageList1;
            this._usersGroupsGrid.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Редактировать группу", "Edit#r")});
            this._usersGroupsGrid.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this._usersGroups_EmbeddedNavigator_ButtonClick);
            this._usersGroupsGrid.Location = new System.Drawing.Point(0, 0);
            this._usersGroupsGrid.MainView = this._groupNamesView;
            this._usersGroupsGrid.Name = "_usersGroupsGrid";
            this._usersGroupsGrid.Size = new System.Drawing.Size(952, 768);
            this._usersGroupsGrid.TabIndex = 0;
            this._usersGroupsGrid.UseEmbeddedNavigator = true;
            this._usersGroupsGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this._groupNamesView,
            this._groupUsersCardView});
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "pen16.png");
            // 
            // _groupNamesView
            // 
            this._groupNamesView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._groupNameColumn,
            this._groupBriefColumn});
            this._groupNamesView.GridControl = this._usersGroupsGrid;
            this._groupNamesView.Name = "_groupNamesView";
            this._groupNamesView.OptionsView.ShowDetailButtons = false;
            this._groupNamesView.OptionsView.ShowGroupPanel = false;
            // 
            // _groupNameColumn
            // 
            this._groupNameColumn.Caption = "Группа";
            this._groupNameColumn.FieldName = "UserGroupName";
            this._groupNameColumn.Name = "_groupNameColumn";
            this._groupNameColumn.OptionsColumn.AllowEdit = false;
            this._groupNameColumn.OptionsColumn.ReadOnly = true;
            this._groupNameColumn.Visible = true;
            this._groupNameColumn.VisibleIndex = 0;
            // 
            // _groupBriefColumn
            // 
            this._groupBriefColumn.Caption = "gridColumn1";
            this._groupBriefColumn.Name = "_groupBriefColumn";
            // 
            // _groupUsersCardView
            // 
            this._groupUsersCardView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._userNameColumn});
            this._groupUsersCardView.FocusedCardTopFieldIndex = 0;
            this._groupUsersCardView.GridControl = this._usersGroupsGrid;
            this._groupUsersCardView.Name = "_groupUsersCardView";
            this._groupUsersCardView.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Auto;
            // 
            // _userNameColumn
            // 
            this._userNameColumn.Caption = "Имя";
            this._userNameColumn.Name = "_userNameColumn";
            this._userNameColumn.Visible = true;
            this._userNameColumn.VisibleIndex = 0;
            // 
            // UsersGroupsList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._usersGroupsGrid);
            this.Name = "UsersGroupsList";
            this.Size = new System.Drawing.Size(952, 768);
            this.Tag = "Группы Пользователей#r";
            ((System.ComponentModel.ISupportInitialize)(this._usersGroupsGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._groupNamesView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._groupUsersCardView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl _usersGroupsGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView _groupNamesView;
        private DevExpress.XtraGrid.Columns.GridColumn _groupNameColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _groupBriefColumn;
        private System.Windows.Forms.ImageList imageList1;
        private DevExpress.XtraGrid.Views.Card.CardView _groupUsersCardView;
        private DevExpress.XtraGrid.Columns.GridColumn _userNameColumn;
    }
}

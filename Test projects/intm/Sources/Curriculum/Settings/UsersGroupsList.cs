﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Curriculum.Forms;
using DevExpress.XtraEditors;
using Eureca.Integrator.Common;
using Eureca.Integrator.Extensions;

namespace Curriculum.Controls
{
    public partial class UsersGroupsList : ControlWithAccessAreas 
    {
        public UsersGroupsList( )
        {
            InitializeComponent( );
            FillGroupNames();
        }

        private void FillGroupNames()
        {
            _usersGroupsGrid.DataSource = null;
            _usersGroupsGrid.DataSource = Program.DbSingletone.UsersGroups.Where(@group => true);
        }

        private void _usersGroups_EmbeddedNavigator_ButtonClick( object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e )
        {
            var selectedItem = _groupNamesView.GetSelectedItem() as UsersGroup;
            switch (e.Button.ButtonType)
            {
                    case NavigatorButtonType.Append:
                    EditGroup( );
                    
                    e.Handled = true;
                    break;

                    case NavigatorButtonType.Remove:
                    
                        Program.DbSingletone.UsersGroups.DeleteOnSubmit(selectedItem);
                        Program.DbSingletone.SubmitChanges();
                        e.Handled = true;
                        break;

                    case NavigatorButtonType.Custom:

                    var tag = e.Button.Tag as string;

                    if (tag != null)
                    {
                        tag = tag.Split( '#' )[0];
                        switch (tag)
                        {
                            case "Edit":
                                EditGroup( selectedItem );
                                break;
                        }
                    }
                    e.Handled = true;
                    break;
            }
            if(e.Handled)
                FillGroupNames();
        }

        private void EditGroup( UsersGroup editedGroup = null )
        {
            bool isNew = editedGroup == null;
            string groupName = isNew ?  "Новая группа" : editedGroup.UserGroupName;

            var peopleGroupSelectionForm = new PeopleGroupSelectionForms( Program.DbSingletone.Users.Where( user => true ) ) { GroupName = groupName , UnallowedNames  = Program.DbSingletone.UsersGroups.Where(group => group.UserGroupName != groupName).Select(group => group.UserGroupName)};
            if (!isNew)
                peopleGroupSelectionForm.SelectedUsers = editedGroup.UsersGroupsUsers.Select(user => user.User);
            var res = peopleGroupSelectionForm.ShowDialog( ) == DialogResult.OK;
            if (res)
            {
                if(isNew)
                {
                    editedGroup = new UsersGroup(){UserGroupName = peopleGroupSelectionForm.GroupName};
                    Program.DbSingletone.UsersGroups.InsertOnSubmit( editedGroup);
                    Program.DbSingletone.SubmitChanges();
                }
                ApplyEditChanges(editedGroup,peopleGroupSelectionForm);
            }
        }

        private void ApplyEditChanges( UsersGroup group, PeopleGroupSelectionForms editGroupSelectionForms )
        {
            group.UserGroupName = editGroupSelectionForms.GroupName;
            var groupUsersCopy = new List<User>( editGroupSelectionForms.SelectedUsers);
            foreach (var usersGroupsUser in group.UsersGroupsUsers)
            {
                User value = usersGroupsUser.User;
                if (groupUsersCopy.Contains(value))
                    groupUsersCopy.Remove(value);
                else
                    Program.DbSingletone.UsersGroupsUsers.DeleteOnSubmit(usersGroupsUser);
            }
            foreach (var user in groupUsersCopy)
            {
                Program.DbSingletone.UsersGroupsUsers.InsertOnSubmit(new UsersGroupsUser(){User = user,UsersGroup = group});
            }
            Program.DbSingletone.SubmitChanges();
        }

        public override IEnumerable<ISKOCBCTDNBTIDED> GetHiddenChilds(bool withLazy)
        {
            var hidden = new List<ISKOCBCTDNBTIDED>();
            if (withLazy)
            {
                var editUsersGroupControl = new PeopleGroupSelectionForms(Enumerable.Empty<User>()){ManualFilter = true};
                hidden.Add(new ControlInterfaceImplementation(editUsersGroupControl));
            }
            return hidden;
        }
    }
}

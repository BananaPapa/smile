using System;
using System.Data.Common;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Views.Base;
using Eureca.Integrator.Common;

namespace Curriculum.Settings
{
    /// <summary>
    /// Represents holidays control.
    /// </summary>
    public partial class HolidaysControl : ControlWithAccessAreas
    {
        /// <summary>
        /// Initializes new instance of holidays control.
        /// </summary>
        public HolidaysControl()
        {
            InitializeComponent();

            _dateEdit.MinValue = new DateTime(DateTime.Now.Year, 1, 1);
            _dateEdit.MaxValue = new DateTime(DateTime.Now.Year + 1, 1, 31);

            var nullableDateEdit = (RepositoryItemDateEdit) _dateEdit.Clone();
            nullableDateEdit.AllowNullInput = DefaultBoolean.True;
            nullableDateEdit.NullText = "���� ����";
            nullableDateEdit.ShowClear = true;
            _finishGridColumn.ColumnEdit = nullableDateEdit;
        }


        /// <summary>
        /// Initializes grid control data source.
        /// </summary>
        private void InitDataSource()
        {
            _gridControl.DataSource = Program.DbSingletone.GetDataSource(typeof (Holiday));
        }

        /// <summary>
        /// Refreshes data source.
        /// </summary>
        private void RefreshDataSource()
        {
            Program.DbSingletone.RefreshDataSource(typeof (Holiday));
            _holidaysGridView.RefreshData();
        }

        /// <summary>
        /// Handles holidays control load event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void ExceptionSchedulesControlLoad(object sender, EventArgs e)
        {
            InitDataSource();
        }

        /// <summary>
        /// Grid control embedded navigator button click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param> 
        private void GridControlEmbeddedNavigatorButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Remove:

                    e.Handled = true;

                    var holiday = _holidaysGridView.GetFocusedRow() as Holiday;

                    if (holiday != null)
                    {
                        if (XtraMessageBox.Show(this,
                                                "�� �������, ��� ������ ������� ���������� ��������?",
                                                "������������� ��������",
                                                MessageBoxButtons.YesNo,
                                                MessageBoxIcon.Question,
                                                MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                        {
                            try
                            {
                                Program.DbSingletone.Holidays.DeleteOnSubmit(holiday);
                                Program.DbSingletone.SubmitChanges();

                                e.Handled = false;
                            }
                            catch (DbException ex)
                            {
                                Program.HandleDbException(ex);
                            }
                        }
                    }

                    break;

                case NavigatorButtonType.Custom:
                    {
                        var tag = e.Button.Tag as string;

                        if (tag != null)
                        {
                            if (tag == "Refresh")
                            {
                                try
                                {
                                    Program.DbSingletone.SubmitChanges();
                                }
                                catch (DbException ex)
                                {
                                    Program.HandleDbException(ex);
                                }
                                finally
                                {
                                    RefreshDataSource();
                                }
                            }
                        }
                    }
                    break;
            }
        }

        private void HolidaysGridViewValidateRow(object sender, ValidateRowEventArgs e)
        {
            var holiday = _holidaysGridView.GetFocusedRow() as Holiday;

            if (holiday != null)
            {
                if (String.IsNullOrEmpty(holiday.Name))
                {
                    e.ErrorText = "�������� �� ������ ���� ������.";
                    e.Valid = false;
                }
                else if (
                    Program.DbSingletone.Holidays.Any(
                        h => h.Name.Equals(holiday.Name) && h.HolidayId != holiday.HolidayId))
                {
                    e.ErrorText = "����� �������� ��� ������������.";
                    e.Valid = false;
                }
            }
        }

        private void HolidaysGridViewRowUpdated(object sender, RowObjectEventArgs e)
        {
            if (e.Row != null)
            {
                try
                {
                    // Submit changes.
                    Program.DbSingletone.SubmitChanges();
                }
                catch (DbException ex)
                {
                    Program.HandleDbException(ex);
                }
            }
        }

        private void HolidaysGridViewValidatingEditor(object sender, BaseContainerValidateEditorEventArgs e)
        {
            if (e.Value != null && e.Value is DateTime)
            {
                var dateTime = (DateTime) e.Value;
                e.Value = new DateTime(DateTime.MinValue.Year, dateTime.Month, dateTime.Day);
            }
        }
    }
}
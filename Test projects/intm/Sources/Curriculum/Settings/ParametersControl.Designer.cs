﻿namespace Curriculum.Settings
{
    partial class ParametersControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._labelControl = new DevExpress.XtraEditors.LabelControl();
            this._pathButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this._saveSimpleButton = new DevExpress.XtraEditors.SimpleButton();
            this._exclusionStatusComboBox = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControlStatuse = new DevExpress.XtraEditors.LabelControl();
            this._eventExpirationPeriodSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this._birthDayAffiliationsCheckedComboBoxEdit = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this._birthDayAffiliationsLabelControl = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this._pathButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._exclusionStatusComboBox.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._eventExpirationPeriodSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._birthDayAffiliationsCheckedComboBoxEdit.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // _labelControl
            // 
            this._labelControl.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._labelControl.Location = new System.Drawing.Point(12, 16);
            this._labelControl.Name = "_labelControl";
            this._labelControl.Size = new System.Drawing.Size(216, 13);
            this._labelControl.TabIndex = 0;
            this._labelControl.Tag = "Корневая папка с матерьялами#r";
            this._labelControl.Text = "Корневая папка с учебными материалами:";
            // 
            // _pathButtonEdit
            // 
            this._pathButtonEdit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._pathButtonEdit.Location = new System.Drawing.Point(241, 13);
            this._pathButtonEdit.Name = "_pathButtonEdit";
            this._pathButtonEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._pathButtonEdit.Properties.Appearance.Options.UseFont = true;
            this._pathButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this._pathButtonEdit.Size = new System.Drawing.Size(323, 20);
            this._pathButtonEdit.TabIndex = 1;
            this._pathButtonEdit.Tag = "Корневая папка с матерьялами#r";
            this._pathButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.PathButtonEditButtonClick);
            this._pathButtonEdit.EditValueChanged += new System.EventHandler(this.PathButtonEditEditValueChanged);
            // 
            // _saveSimpleButton
            // 
            this._saveSimpleButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._saveSimpleButton.Location = new System.Drawing.Point(578, 12);
            this._saveSimpleButton.Name = "_saveSimpleButton";
            this._saveSimpleButton.Size = new System.Drawing.Size(75, 23);
            this._saveSimpleButton.TabIndex = 2;
            this._saveSimpleButton.Tag = "Корневая папка с матерьялами#rc";
            this._saveSimpleButton.Text = "Сохранить";
            this._saveSimpleButton.Click += new System.EventHandler(this.SaveSimpleButtonClick);
            // 
            // _exclusionStatusComboBox
            // 
            this._exclusionStatusComboBox.Location = new System.Drawing.Point(241, 65);
            this._exclusionStatusComboBox.Name = "_exclusionStatusComboBox";
            this._exclusionStatusComboBox.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._exclusionStatusComboBox.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this._exclusionStatusComboBox.Size = new System.Drawing.Size(133, 20);
            this._exclusionStatusComboBox.TabIndex = 6;
            this._exclusionStatusComboBox.Tag = "Исключить из календаря#r";
            this._exclusionStatusComboBox.SelectedIndexChanged += new System.EventHandler(this.ComboBoxEditStatuseSelectedIndexChanged);
            // 
            // labelControlStatuse
            // 
            this.labelControlStatuse.Location = new System.Drawing.Point(12, 68);
            this.labelControlStatuse.Name = "labelControlStatuse";
            this.labelControlStatuse.Size = new System.Drawing.Size(133, 13);
            this.labelControlStatuse.TabIndex = 5;
            this.labelControlStatuse.Tag = "Исключить из календаря#r";
            this.labelControlStatuse.Text = "Исключить из календаря:";
            // 
            // _eventExpirationPeriodSpinEdit
            // 
            this._eventExpirationPeriodSpinEdit.EditValue = new decimal(new int[] {
            365,
            0,
            0,
            0});
            this._eventExpirationPeriodSpinEdit.Location = new System.Drawing.Point(241, 91);
            this._eventExpirationPeriodSpinEdit.Name = "_eventExpirationPeriodSpinEdit";
            this._eventExpirationPeriodSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this._eventExpirationPeriodSpinEdit.Properties.IsFloatValue = false;
            this._eventExpirationPeriodSpinEdit.Properties.Mask.EditMask = "N00";
            this._eventExpirationPeriodSpinEdit.Properties.MaxValue = new decimal(new int[] {
            365,
            0,
            0,
            0});
            this._eventExpirationPeriodSpinEdit.Properties.MinValue = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this._eventExpirationPeriodSpinEdit.Size = new System.Drawing.Size(68, 20);
            this._eventExpirationPeriodSpinEdit.TabIndex = 8;
            this._eventExpirationPeriodSpinEdit.Tag = "Срок хранения#r";
            this._eventExpirationPeriodSpinEdit.EditValueChanged += new System.EventHandler(this._eventExpirationPeriodSpinEdit_EditValueChanged);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(12, 94);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(195, 13);
            this.labelControl1.TabIndex = 7;
            this.labelControl1.Tag = "Срок хранения#r";
            this.labelControl1.Text = "Срок хранения личных событий (дн.):";
            // 
            // _birthDayAffiliationsCheckedComboBoxEdit
            // 
            this._birthDayAffiliationsCheckedComboBoxEdit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._birthDayAffiliationsCheckedComboBoxEdit.Location = new System.Drawing.Point(241, 39);
            this._birthDayAffiliationsCheckedComboBoxEdit.Name = "_birthDayAffiliationsCheckedComboBoxEdit";
            this._birthDayAffiliationsCheckedComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._birthDayAffiliationsCheckedComboBoxEdit.Size = new System.Drawing.Size(323, 20);
            this._birthDayAffiliationsCheckedComboBoxEdit.TabIndex = 4;
            this._birthDayAffiliationsCheckedComboBoxEdit.Tag = "Оповещения о днях рождения#r";
            // 
            // _birthDayAffiliationsLabelControl
            // 
            this._birthDayAffiliationsLabelControl.Location = new System.Drawing.Point(12, 42);
            this._birthDayAffiliationsLabelControl.Name = "_birthDayAffiliationsLabelControl";
            this._birthDayAffiliationsLabelControl.Size = new System.Drawing.Size(215, 13);
            this._birthDayAffiliationsLabelControl.TabIndex = 3;
            this._birthDayAffiliationsLabelControl.Tag = "Оповещения о днях рождения#r";
            this._birthDayAffiliationsLabelControl.Text = "Оповещать о днях рождения только для:";
            // 
            // ParametersControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._birthDayAffiliationsCheckedComboBoxEdit);
            this.Controls.Add(this._eventExpirationPeriodSpinEdit);
            this.Controls.Add(this._exclusionStatusComboBox);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this._birthDayAffiliationsLabelControl);
            this.Controls.Add(this.labelControlStatuse);
            this.Controls.Add(this._saveSimpleButton);
            this.Controls.Add(this._pathButtonEdit);
            this.Controls.Add(this._labelControl);
            this.Name = "ParametersControl";
            this.Size = new System.Drawing.Size(665, 372);
            this.Tag = "Параметры#r";
            this.Load += new System.EventHandler(this.AttachmentsControlLoad);
            ((System.ComponentModel.ISupportInitialize)(this._pathButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._exclusionStatusComboBox.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._eventExpirationPeriodSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._birthDayAffiliationsCheckedComboBoxEdit.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl _labelControl;
        private DevExpress.XtraEditors.ButtonEdit _pathButtonEdit;
        private DevExpress.XtraEditors.SimpleButton _saveSimpleButton;
        private DevExpress.XtraEditors.ComboBoxEdit _exclusionStatusComboBox;
        private DevExpress.XtraEditors.LabelControl labelControlStatuse;
        private DevExpress.XtraEditors.SpinEdit _eventExpirationPeriodSpinEdit;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.CheckedComboBoxEdit _birthDayAffiliationsCheckedComboBoxEdit;
        private DevExpress.XtraEditors.LabelControl _birthDayAffiliationsLabelControl;
    }
}

using System;
using System.Data.Common;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using Eureca.Integrator.Common;

namespace Curriculum.Settings
{
    public partial class CommissionMembersControl : ControlWithAccessAreas
    {
        private CommissionMember _prevMember;

        public CommissionMembersControl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initializes grid control data source.
        /// </summary>
        private void InitDataSource()
        {
            _gridControl.DataSource = Program.DbSingletone.GetDataSource(typeof (CommissionMember));
        }

        /// <summary>
        /// Refreshes data source.
        /// </summary>
        private void RefreshDataSource()
        {
            Program.DbSingletone.RefreshDataSource(typeof (CommissionMember));
            _commissionMemberGridView.RefreshData();
        }

        /// <summary>
        /// Handles commision members control load event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void CommissionMembersControl_Load(object sender, EventArgs e)
        {
            InitDataSource();
        }

        /// <summary>
        /// Handles commision members grid view row updated event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void CommissionMembersGridView_RowUpdated(object sender, RowObjectEventArgs e)
        {
            try
            {
                Program.DbSingletone.SubmitChanges();
            }
            catch (DbException ex)
            {
                Program.HandleDbException(ex);
            }
        }

        /// <summary>
        /// Handles commision members grid validating editor event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void CommissionMembersGridView_ValidatingEditor(object sender, BaseContainerValidateEditorEventArgs e)
        {
            var text = e.Value as string;

            if (text != null && text.Trim().Length == 0)
            {
                e.Value = null;
            }
        }

        /// <summary>
        /// Validates member name.
        /// </summary>
        /// <param name="name">Member name.</param>
        /// <param name="errorText">Error text if occured.</param>
        /// <returns>true if member name is valid; otherwise, false.</returns>
        private static bool ValidateMemberName(string name, out string errorText)
        {
            if (String.IsNullOrEmpty(name))
            {
                errorText = "��� �� ������ ���� �������.";
                return false;
            }

            errorText = null;
            return true;
        }

        /// <summary>
        /// Validates member rank.
        /// </summary>
        /// <param name="rank">Member rank.</param>
        /// <param name="errorText">Error text if occured.</param>
        /// <returns>true if member rank is valid; otherwise, false.</returns>
        private static bool ValidateMemberRank(string rank, out string errorText)
        {
            if (String.IsNullOrEmpty(rank))
            {
                errorText = "��������� �� ������ ���� ������.";
                return false;
            }

            errorText = null;
            return true;
        }

        /// <summary>
        /// Handles commision members grid validate row event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void CommissionMemberGridView_ValidateRow(object sender, ValidateRowEventArgs e)
        {
            var member = e.Row as CommissionMember;
            if (member == null) return;

            try
            {
                string errorText;

                if (!ValidateMemberName(member.Name, out errorText))
                {
                    e.ErrorText = errorText;
                }
                else if (!ValidateMemberRank(member.Rank, out errorText))
                {
                    e.ErrorText = errorText;
                }
            }
            catch (DbException ex)
            {
                Program.HandleDbException(ex);
            }

            // If there is an error show message.
            if (!String.IsNullOrEmpty(e.ErrorText))
            {
                e.ErrorText += Environment.NewLine;
                e.Valid = false;
            }
        }

        /// <summary>
        /// Handles commision members grid focused row changed event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void CommissionMemberGridView_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            var view = sender as GridView;
            if (view != null)
            {
                string errorText;
                if (_prevMember != null &&
                    (!ValidateMemberName(_prevMember.Name, out errorText) ||
                     !ValidateMemberRank(_prevMember.Rank, out errorText)))
                {
                    //If previous member is not validated then discard data context changes.
                    Program.DiscardDataChanges();
                }

                if (view.IsGroupRow(e.FocusedRowHandle))
                {
                    _prevMember = null;
                }
                else
                {
                    _prevMember = view.GetRow(e.FocusedRowHandle) as CommissionMember;
                }
            }
        }

        /// <summary>
        /// Gets user's confirmation for deletion.
        /// </summary>
        /// <returns>true if deletion is confirmed; otherwise, false.</returns>
        private static bool ConfirmDeletion()
        {
            // Show question message box and return result.
            return XtraMessageBox.Show(
                "�� �������, ��� ������ ������� ���������� ������?",
                "������������� ��������",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Question,
                MessageBoxDefaultButton.Button2) == DialogResult.Yes;
        }

        /// <summary>
        /// Handles grid control embedded navigator button click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void GridControl_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Remove:

                    // Selected row will not be removed.
                    e.Handled = true;

                    var member = _commissionMemberGridView.GetFocusedRow() as CommissionMember;

                    if (member != null && ConfirmDeletion())
                    {
                        Program.DbSingletone.CommissionMembers.DeleteOnSubmit(member);

                        try
                        {
                            Program.DbSingletone.SubmitChanges();

                            // Selected row will be removed.
                            e.Handled = false;
                        }
                        catch (DbException ex)
                        {
                            Program.HandleDbException(ex);
                        }
                    }
                    break;

                case NavigatorButtonType.Custom:

                    if ((e.Button.Tag as string) == "Refresh")
                    {
                        try
                        {
                            Program.DbSingletone.SubmitChanges();
                        }
                        catch (DbException ex)
                        {
                            Program.HandleDbException(ex);
                        }
                        finally
                        {
                            RefreshDataSource();
                        }
                    }

                    break;
            }
        }
    }
}
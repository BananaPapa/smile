using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Eureca.Integrator.Common;
using Integrator.Curriculum.Constants;
using MessageBox = Eureca.Integrator.Utility.Forms.MessageBox;

namespace Curriculum.Settings
{
    /// <summary>
    /// Represents user edit form.
    /// </summary>
    public partial class UserEditForm : FormWithAccessAreas
    {
        private readonly bool _isNewUser = false;
        private readonly IEnumerable<string> _unacceptableNames;
        private UserDepartment _userDepartment;
        private UserPosition _userPosition;
        private IEnumerable<UsersGroup> _userVisionGroups;

        public Role UserRole 
        {
            get
            {
                var userRole = Program.DbSingletone.UsersRoles.SingleOrDefault( role => role.UserId == User.UserId );
                if(userRole == null)
                    return null;
                return userRole.Role;
            }
            set 
            {
                CreateOrUpdateUserRole(value,User);
            }
        }

        private void CreateOrUpdateUserRole(Role value , User user)
        {
            var userRole = Program.DbSingletone.UsersRoles.SingleOrDefault(role => role.UserId == user.UserId);
            if (userRole == null)
            {
                if (value != null)
                    Program.DbSingletone.UsersRoles.InsertOnSubmit(new UsersRole() {User = user, Role = value});
            }
            else
            {
                if (value == null)
                    Program.DbSingletone.UsersRoles.DeleteOnSubmit(userRole);
                else
                    userRole.Role = value;
            }
            Program.DbSingletone.SubmitChanges();
        }

        public Role TempRole
        {
            get;
            set;
        }


        /// <summary>
        /// Initializes new instance of user edit form to create new user.
        /// </summary>
        public UserEditForm(IEnumerable<User> users) : this(null,users)
        {
            _isNewUser = true;
        }

        /// <summary>
        /// Initializes new instance of user edit form to edit specified user.
        /// </summary>
        /// <param name="user">User to edit.</param>
        public UserEditForm(User user,IEnumerable<User> users )
        {
            User = user;
            InitializeComponent( );


            _unacceptableNames = ( User == null ? users : users.Where( user1 => user1.UserId != User.UserId ) ).Select( user1 => user1.Name );
            
            if(User == null)
            {
                UserDepartment = Constants.EmptyUserDepartment;
                UserPosition = Constants.EmptyUserPosition;
                _userVisionGroups = Enumerable.Empty<UsersGroup>();
            }
            else
            {
                _userVisionGroups = User.UsersGroupsUsers.Select( groupsUser => groupsUser.UsersGroup );
                UserDepartment = User.GetUserDepartment() ?? Constants.EmptyUserDepartment;
                UserPosition = User.GetUserPosition() ?? Constants.EmptyUserPosition;
            }

            

            _groupEditButton.Text = String.Join( ", ", _userVisionGroups.Select( @group => @group.UserGroupName ) );

            _passwordTextEdit.MaskBox.UseSystemPasswordChar =
                _confirmPasswordTextEdit.MaskBox.UseSystemPasswordChar = true;

            //User = user;

        }

        private void InitComboBox( )
        {
            #region role

            Binding binding = null;
            
            binding = new Binding("SelectedItem", this, User == null? "TempRole": "UserRole", false, DataSourceUpdateMode.OnPropertyChanged, Constants.EmptyRole);
            binding.Format += binding_Format;
            
            var datasource = Program.DbSingletone.Roles.ToList();
            datasource.Insert(0, Constants.EmptyRole);
            _roleComboBox.DataSource = datasource;
            _roleComboBox.DataBindings.Add(binding);
            if (User != null && User.UserId == Program.CurrentUser.UserId)
                _roleComboBox.Enabled = false;

            #endregion

            #region department

            List<UserDepartment> userDepartments = Program.DbSingletone.UserDepartments.Where(document => true).ToList();
            userDepartments.Add(Constants.EmptyUserDepartment);
            _departmentComboBox.DataSource = userDepartments;
            _departmentComboBox.DataBindings.Add( new Binding( "SelectedItem", this, "UserDepartment",false,DataSourceUpdateMode.OnPropertyChanged,Constants.EmptyUserDepartment ) );
            
            #endregion

            #region positions

            List<UserPosition> userPositions = Program.DbSingletone.UserPositions.Where( position => true ).ToList();     
            userPositions.Add(Constants.EmptyUserPosition);
            _positionComboBox.DataSource = userPositions;
            _positionComboBox.DataBindings.Add( new Binding( "SelectedItem", this, "UserPosition",false, DataSourceUpdateMode.OnPropertyChanged, Constants.EmptyUserPosition ) );

            #endregion

        }

        void binding_Format( object sender, ConvertEventArgs e )
        {
            if (e.Value == null)
                e.Value = Constants.EmptyRole;
            else if (e.Value == Constants.EmptyRole)
                e.Value = null;
        }

        /// <summary>
        /// Gets created or edited user.
        /// </summary>
        public User User { get; private set; }

        public UserPosition UserPosition
        {
            get { return _userPosition; }
            set { _userPosition = value; }
        }

        public UserDepartment UserDepartment
        {
            get { return _userDepartment; }
            set { _userDepartment = value; }
        }

        /// <summary>
        /// Handles user edit form load event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void UserEditForm_Load(object sender, EventArgs e)
        {
            InitComboBox( );

            if (User != null)
            {
                _nameTextEdit.Text = User.Name;

                _passwordLayoutControlItem.Text = "����� ������";
            }
        }

        /// <summary>
        /// Shows occured error.
        /// </summary>
        /// <param name="error">Error text.</param>
        private static void ShowError(string error)
        {
            DevExpress.XtraEditors.XtraMessageBox.Show(error, "������", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        /// <summary>
        /// Handles ok simple button click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void OkSimpleButton_Click(object sender, EventArgs e)
        {
            string name = _nameTextEdit.Text.Trim();
            string password = _passwordTextEdit.Text.Trim();
            string confirmedPassword = _confirmPasswordTextEdit.Text.Trim();

            if (!ValidateUser(name, password, confirmedPassword)) return;

            // Create new user.
            if (User == null)
            {
                User = new User();
            }

            // Set new name.
            User.Name = name;

            // Generate new password hash.
            if (password.Length > 0)
            {
                User.PasswordHash = password.ComputeSHA1Hash(Encoding.UTF8);
            }

            if (_isNewUser)
                Program.DbSingletone.Users.InsertOnSubmit( User );
            Program.DbSingletone.SubmitChanges( );

            // Add vision
            var visionGroupsCopy = new List<UsersGroup>( _userVisionGroups );
            foreach (var userVisionGroup in Program.DbSingletone.UsersVisions.Where(vision => vision.User == User))
            {
                if (visionGroupsCopy.Contains(userVisionGroup.UsersGroup))
                    visionGroupsCopy.Remove(userVisionGroup.UsersGroup);
                else
                    Program.DbSingletone.UsersVisions.DeleteOnSubmit(userVisionGroup);
            }
            foreach (var usersGroup in visionGroupsCopy)
            {
                var userVision = new UsersVision {User = User, UsersGroup = usersGroup};
                Program.DbSingletone.UsersVisions.InsertOnSubmit(userVision);
            }
            

            User.SetUserPosition(UserPosition == Constants.EmptyUserPosition? null : UserPosition);
            User.SetUserDepartment( UserDepartment == Constants.EmptyUserDepartment ? null : UserDepartment );

            Program.DbSingletone.SubmitChanges();
            if(TempRole != null && TempRole != Constants.EmptyRole)
                CreateOrUpdateUserRole(TempRole,User);

            DialogResult = DialogResult.OK;
            Close();
        }

        private bool ValidateUser(string name, string password, string confirmedPassword)
        {
            // Check input.

            StringBuilder sb = new StringBuilder();

            if (name.Length == 0)
                sb.AppendLine("��� �� ������ ���� ������.");
            if (_unacceptableNames.Contains(name))
                sb.AppendLine("������������ � ����� ������ ��� ����������.");
            if (User == null && (password.Length == 0 || confirmedPassword.Length == 0))
                sb.AppendLine("������� ������ ��� ����.");
            if ((password.Length > 0 || confirmedPassword.Length > 0) && password != confirmedPassword)
                sb.AppendLine("������ �� ��������� � �������� �������� �������.");
            if (UserPosition == Constants.EmptyUserPosition)
                sb.AppendLine( "���������� ��������� ��������� ������������." );
            if (UserDepartment == Constants.EmptyUserDepartment)
                sb.AppendLine( "���������� ������� ����� ������������." );
            if (sb.Length > 0)
            {
                sb.Insert(0, "������������ �������� ��������� ������\n:");
                MessageBox.ShowValidationError(sb.ToString());
                return false;
            }
            return true;
        }


        private void buttonEdit1_ButtonClick( object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e )
        {
            var selectGroupsForm = new SelectVisionGroupForm(Program.DbSingletone.UsersGroups){SelectedGroups = _userVisionGroups};
            if (selectGroupsForm.ShowDialog() == DialogResult.OK)
            {
                _userVisionGroups = selectGroupsForm.SelectedGroups;
                _groupEditButton.Text = String.Join( ", ", _userVisionGroups.Select( @group => @group.UserGroupName ) );
            }
            
        }
    }
}
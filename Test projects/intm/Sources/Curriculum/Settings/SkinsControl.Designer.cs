namespace Curriculum.Settings
{
    partial class SkinsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._skinsListBoxControl = new DevExpress.XtraEditors.ListBoxControl();
            this._applySkinPanelControl = new DevExpress.XtraEditors.PanelControl();
            this._applySkinSimpleButton = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this._skinsListBoxControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._applySkinPanelControl)).BeginInit();
            this._applySkinPanelControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // _skinsListBoxControl
            // 
            this._skinsListBoxControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._skinsListBoxControl.Location = new System.Drawing.Point(0, 0);
            this._skinsListBoxControl.Name = "_skinsListBoxControl";
            this._skinsListBoxControl.Size = new System.Drawing.Size(446, 428);
            this._skinsListBoxControl.TabIndex = 0;
            this._skinsListBoxControl.Tag = "..\\����#r";
            this._skinsListBoxControl.SelectedIndexChanged += new System.EventHandler(this.SkinsListBoxControl_SelectedIndexChanged);
            // 
            // _applySkinPanelControl
            // 
            this._applySkinPanelControl.Controls.Add(this._applySkinSimpleButton);
            this._applySkinPanelControl.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._applySkinPanelControl.Location = new System.Drawing.Point(0, 428);
            this._applySkinPanelControl.Name = "_applySkinPanelControl";
            this._applySkinPanelControl.Size = new System.Drawing.Size(446, 30);
            this._applySkinPanelControl.TabIndex = 6;
            // 
            // _applySkinSimpleButton
            // 
            this._applySkinSimpleButton.Dock = System.Windows.Forms.DockStyle.Right;
            this._applySkinSimpleButton.Enabled = false;
            this._applySkinSimpleButton.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this._applySkinSimpleButton.Location = new System.Drawing.Point(359, 2);
            this._applySkinSimpleButton.Name = "_applySkinSimpleButton";
            this._applySkinSimpleButton.Size = new System.Drawing.Size(85, 26);
            this._applySkinSimpleButton.TabIndex = 2;
            this._applySkinSimpleButton.Tag = "..\\����#rc";
            this._applySkinSimpleButton.Text = "���������";
            this._applySkinSimpleButton.ToolTip = "������������� ����������";
            this._applySkinSimpleButton.Click += new System.EventHandler(this.ApplySkinSimpleButton_Click);
            // 
            // SkinsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._skinsListBoxControl);
            this.Controls.Add(this._applySkinPanelControl);
            this.Name = "SkinsControl";
            this.Size = new System.Drawing.Size(446, 458);
            this.Tag = "����#r";
            this.Load += new System.EventHandler(this.SkinsControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this._skinsListBoxControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._applySkinPanelControl)).EndInit();
            this._applySkinPanelControl.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.ListBoxControl _skinsListBoxControl;
        private DevExpress.XtraEditors.PanelControl _applySkinPanelControl;
        private DevExpress.XtraEditors.SimpleButton _applySkinSimpleButton;
    }
}

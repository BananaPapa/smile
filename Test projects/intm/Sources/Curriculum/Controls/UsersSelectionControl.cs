﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Curriculum.Controls
{
    public partial class UsersSelectionControl : UserControl
    {
        private BindingList<UserView> _users = new BindingList<UserView>( );

        public IEnumerable<User> SelectedUsers
        {
            get
            {
                return _users.Where( view => view.IsSelected ).Select( view => view.Model );
            }
            set
            {
                foreach (var userView in _users)
                    userView.IsSelected = value != null && value.Contains( userView.Model );
            }
        }

        public BindingList<UserView> Users
        {
            get { return _users; }
        }

        public UsersSelectionControl():this(null)
        {
            
        }

        public UsersSelectionControl( IEnumerable<User> allUsers )
        {
            if(allUsers!=null && allUsers.Any())
                Init(allUsers);
            InitializeComponent( );
            _usersGridControl.DataSource = allUsers;
        }

        public void Init(IEnumerable<User> allUsers)
        {
            foreach (var user in allUsers)
            {
                _users.Add(new UserView(user, user.GetUserDepartment(), user.GetUserPosition()));
            }
            _usersGridControl.DataSource = _users;
        }

        private void _selectAllCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            var checkBoxSender = sender as CheckBox;
            var value = (sender as CheckBox).Checked;
            for (int i = 0 ; i< _users.Count ; i++)
            {
                var userView = _users[i];
                if (userView.IsSelected != value)
                {
                    userView.IsSelected = checkBoxSender.Checked;
                    _userView.RefreshRow(_userView.GetRowHandle(i));
                }
            }
        }

        private void _userSelectionCheckEdit_CheckedChanged( object sender, EventArgs e )
        {
            _selectAllCheckBox.Checked = _users.All(view => view.IsSelected);
        }   

       
    }
    public class UserView
    {
        private User _model;

        public User Model
        {
            get { return _model; }
        }


        public UserDepartment UserDepartment { get; private set; }

        public UserPosition UserPosition { get; private set; }

        public bool IsSelected
        {
            get;
            set;
        }

        public UserView( User model, UserDepartment userDepartment, UserPosition userPosition )
        {
            _model = model;
            UserDepartment = userDepartment;
            UserPosition = userPosition;
        }
    }
}

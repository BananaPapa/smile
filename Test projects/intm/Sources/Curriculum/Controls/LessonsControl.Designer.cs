namespace Curriculum
{
    partial class LessonsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraScheduler.TimeRuler timeRuler1 = new DevExpress.XtraScheduler.TimeRuler();
            DevExpress.XtraScheduler.TimeRuler timeRuler2 = new DevExpress.XtraScheduler.TimeRuler();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LessonsControl));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            this._coursePartGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._coursesGridControl = new DevExpress.XtraGrid.GridControl();
            this._coursesGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._courseUnitGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._courseGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._courseLessonTypeGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._courseTotalDurationGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._disciplinesGridControl = new DevExpress.XtraGrid.GridControl();
            this._disciplinesGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._disciplineGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._disciplineLessonTypeGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._disciplineUnitGgridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._disciplinePartGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lessonDisciplineGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lessonTypeGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lessonTotalDurationGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._lessonsSchedulerControl = new DevExpress.XtraScheduler.SchedulerControl();
            this._barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.activeViewBar1 = new DevExpress.XtraScheduler.UI.ActiveViewBar();
            this.gotoTodayItem1 = new DevExpress.XtraScheduler.UI.GotoTodayItem();
            this.switchToDayViewItem1 = new DevExpress.XtraScheduler.UI.SwitchToDayViewItem();
            this.switchToWorkWeekViewItem1 = new DevExpress.XtraScheduler.UI.SwitchToWorkWeekViewItem();
            this.switchToWeekViewItem1 = new DevExpress.XtraScheduler.UI.SwitchToWeekViewItem();
            this.switchToMonthViewItem1 = new DevExpress.XtraScheduler.UI.SwitchToMonthViewItem();
            this.switchToTimelineViewItem1 = new DevExpress.XtraScheduler.UI.SwitchToTimelineViewItem();
            this.switchToGanttViewItem1 = new DevExpress.XtraScheduler.UI.SwitchToGanttViewItem();
            this._testReportBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.navigatorBar1 = new DevExpress.XtraScheduler.UI.NavigatorBar();
            this._refreshBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.navigateViewBackwardItem1 = new DevExpress.XtraScheduler.UI.NavigateViewBackwardItem();
            this.navigateViewForwardItem1 = new DevExpress.XtraScheduler.UI.NavigateViewForwardItem();
            this.viewZoomInItem1 = new DevExpress.XtraScheduler.UI.ViewZoomInItem();
            this.viewZoomOutItem1 = new DevExpress.XtraScheduler.UI.ViewZoomOutItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.viewSelectorItem1 = new DevExpress.XtraScheduler.UI.ViewSelectorItem();
            this.viewSelectorItem2 = new DevExpress.XtraScheduler.UI.ViewSelectorItem();
            this.viewSelectorItem3 = new DevExpress.XtraScheduler.UI.ViewSelectorItem();
            this.viewSelectorItem4 = new DevExpress.XtraScheduler.UI.ViewSelectorItem();
            this.viewSelectorItem5 = new DevExpress.XtraScheduler.UI.ViewSelectorItem();
            this.viewNavigatorBackwardItem1 = new DevExpress.XtraScheduler.UI.ViewNavigatorBackwardItem();
            this.viewNavigatorForwardItem1 = new DevExpress.XtraScheduler.UI.ViewNavigatorForwardItem();
            this.viewNavigatorTodayItem1 = new DevExpress.XtraScheduler.UI.ViewNavigatorTodayItem();
            this.viewNavigatorZoomInItem1 = new DevExpress.XtraScheduler.UI.ViewNavigatorZoomInItem();
            this.viewNavigatorZoomOutItem1 = new DevExpress.XtraScheduler.UI.ViewNavigatorZoomOutItem();
            this._barButtonItemRemoveLesson = new DevExpress.XtraBars.BarButtonItem();
            this.switchTimeScalesCaptionItem1 = new DevExpress.XtraScheduler.UI.SwitchTimeScalesCaptionItem();
            this.switchCompressWeekendItem1 = new DevExpress.XtraScheduler.UI.SwitchCompressWeekendItem();
            this.switchShowWorkTimeOnlyItem1 = new DevExpress.XtraScheduler.UI.SwitchShowWorkTimeOnlyItem();
            this.switchCellsAutoHeightItem1 = new DevExpress.XtraScheduler.UI.SwitchCellsAutoHeightItem();
            this.changeSnapToCellsUIItem1 = new DevExpress.XtraScheduler.UI.ChangeSnapToCellsUIItem();
            this.switchTimeScalesItem1 = new DevExpress.XtraScheduler.UI.SwitchTimeScalesItem();
            this.changeScaleWidthItem1 = new DevExpress.XtraScheduler.UI.ChangeScaleWidthItem();
            this.repositoryItemSpinEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.newAppointmentItem1 = new DevExpress.XtraScheduler.UI.NewAppointmentItem();
            this.newRecurringAppointmentItem1 = new DevExpress.XtraScheduler.UI.NewRecurringAppointmentItem();
            this.groupByNoneItem1 = new DevExpress.XtraScheduler.UI.GroupByNoneItem();
            this.groupByDateItem1 = new DevExpress.XtraScheduler.UI.GroupByDateItem();
            this.groupByResourceItem1 = new DevExpress.XtraScheduler.UI.GroupByResourceItem();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this._schedulerStorage = new DevExpress.XtraScheduler.SchedulerStorage(this.components);
            this._toolTipController = new DevExpress.Utils.ToolTipController(this.components);
            this.splitterControl1 = new DevExpress.XtraEditors.SplitterControl();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitterControl2 = new DevExpress.XtraEditors.SplitterControl();
            this.dateNavigator1 = new DevExpress.XtraScheduler.DateNavigator();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this._coursesPanelControl = new DevExpress.XtraEditors.PanelControl();
            this._clearLessonsSimpleButton = new DevExpress.XtraEditors.SimpleButton();
            this._autoAllocationSimpleButton = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabControl2 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this._lessonsGridControl = new DevExpress.XtraGrid.GridControl();
            this._lessonsGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.schedulerBarController1 = new DevExpress.XtraScheduler.UI.SchedulerBarController();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._coursesGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._coursesGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._disciplinesGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._disciplinesGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._lessonsSchedulerControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._schedulerStorage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateNavigator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._coursesPanelControl)).BeginInit();
            this._coursesPanelControl.SuspendLayout();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl2)).BeginInit();
            this.xtraTabControl2.SuspendLayout();
            this.xtraTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lessonsGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._lessonsGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerBarController1)).BeginInit();
            this.SuspendLayout();
            // 
            // _coursePartGridColumn
            // 
            this._coursePartGridColumn.Caption = "�����";
            this._coursePartGridColumn.FieldName = "Part";
            this._coursePartGridColumn.Name = "_coursePartGridColumn";
            this._coursePartGridColumn.Visible = true;
            this._coursePartGridColumn.VisibleIndex = 3;
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this._coursesGridControl;
            this.gridView2.Name = "gridView2";
            // 
            // _coursesGridControl
            // 
            this._coursesGridControl.AllowDrop = true;
            this._coursesGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._coursesGridControl.Location = new System.Drawing.Point(0, 0);
            this._coursesGridControl.MainView = this._coursesGridView;
            this._coursesGridControl.Name = "_coursesGridControl";
            this._coursesGridControl.Size = new System.Drawing.Size(387, 215);
            this._coursesGridControl.TabIndex = 0;
            this._coursesGridControl.Tag = "������� �����#r";
            this._coursesGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this._coursesGridView,
            this.gridView2});
            this._coursesGridControl.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridControlMouseDown);
            this._coursesGridControl.MouseMove += new System.Windows.Forms.MouseEventHandler(this.GridControlMouseMove);
            // 
            // _coursesGridView
            // 
            this._coursesGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._courseUnitGridColumn,
            this._courseGridColumn,
            this._courseLessonTypeGridColumn,
            this._courseTotalDurationGridColumn,
            this._coursePartGridColumn});
            this._coursesGridView.GridControl = this._coursesGridControl;
            this._coursesGridView.GroupCount = 2;
            this._coursesGridView.Name = "_coursesGridView";
            this._coursesGridView.OptionsBehavior.Editable = false;
            this._coursesGridView.OptionsDetail.EnableMasterViewMode = false;
            this._coursesGridView.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this._courseUnitGridColumn, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this._coursePartGridColumn, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // _courseUnitGridColumn
            // 
            this._courseUnitGridColumn.Caption = "������";
            this._courseUnitGridColumn.FieldName = "Unit";
            this._courseUnitGridColumn.Name = "_courseUnitGridColumn";
            this._courseUnitGridColumn.Visible = true;
            this._courseUnitGridColumn.VisibleIndex = 3;
            // 
            // _courseGridColumn
            // 
            this._courseGridColumn.AppearanceHeader.Options.UseTextOptions = true;
            this._courseGridColumn.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._courseGridColumn.Caption = "����";
            this._courseGridColumn.FieldName = "Discipline";
            this._courseGridColumn.Name = "_courseGridColumn";
            this._courseGridColumn.Visible = true;
            this._courseGridColumn.VisibleIndex = 0;
            // 
            // _courseLessonTypeGridColumn
            // 
            this._courseLessonTypeGridColumn.AppearanceHeader.Options.UseTextOptions = true;
            this._courseLessonTypeGridColumn.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._courseLessonTypeGridColumn.Caption = "��� �������";
            this._courseLessonTypeGridColumn.FieldName = "LessonType";
            this._courseLessonTypeGridColumn.Name = "_courseLessonTypeGridColumn";
            this._courseLessonTypeGridColumn.Visible = true;
            this._courseLessonTypeGridColumn.VisibleIndex = 1;
            // 
            // _courseTotalDurationGridColumn
            // 
            this._courseTotalDurationGridColumn.AppearanceHeader.Options.UseTextOptions = true;
            this._courseTotalDurationGridColumn.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._courseTotalDurationGridColumn.Caption = "�����������������";
            this._courseTotalDurationGridColumn.FieldName = "TotalDuration";
            this._courseTotalDurationGridColumn.Name = "_courseTotalDurationGridColumn";
            this._courseTotalDurationGridColumn.Visible = true;
            this._courseTotalDurationGridColumn.VisibleIndex = 2;
            // 
            // _disciplinesGridControl
            // 
            this._disciplinesGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._disciplinesGridControl.EmbeddedNavigator.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridControlMouseDown);
            this._disciplinesGridControl.EmbeddedNavigator.MouseMove += new System.Windows.Forms.MouseEventHandler(this.GridControlMouseMove);
            this._disciplinesGridControl.Location = new System.Drawing.Point(0, 0);
            this._disciplinesGridControl.MainView = this._disciplinesGridView;
            this._disciplinesGridControl.Name = "_disciplinesGridControl";
            this._disciplinesGridControl.Size = new System.Drawing.Size(387, 244);
            this._disciplinesGridControl.TabIndex = 0;
            this._disciplinesGridControl.Tag = "..\\������� ��� ����#r";
            this._disciplinesGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this._disciplinesGridView});
            this._disciplinesGridControl.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridControlMouseDown);
            this._disciplinesGridControl.MouseMove += new System.Windows.Forms.MouseEventHandler(this.GridControlMouseMove);
            // 
            // _disciplinesGridView
            // 
            this._disciplinesGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._disciplineGridColumn,
            this._disciplineLessonTypeGridColumn,
            this._disciplineUnitGgridColumn,
            this._disciplinePartGridColumn});
            this._disciplinesGridView.GridControl = this._disciplinesGridControl;
            this._disciplinesGridView.GroupCount = 3;
            this._disciplinesGridView.Name = "_disciplinesGridView";
            this._disciplinesGridView.OptionsBehavior.Editable = false;
            this._disciplinesGridView.OptionsDetail.EnableMasterViewMode = false;
            this._disciplinesGridView.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this._disciplineLessonTypeGridColumn, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this._disciplineUnitGgridColumn, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this._disciplinePartGridColumn, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // _disciplineGridColumn
            // 
            this._disciplineGridColumn.Caption = "����";
            this._disciplineGridColumn.FieldName = "Discipline";
            this._disciplineGridColumn.Name = "_disciplineGridColumn";
            this._disciplineGridColumn.Tag = "����#r";
            this._disciplineGridColumn.Visible = true;
            this._disciplineGridColumn.VisibleIndex = 0;
            this._disciplineGridColumn.Width = 83;
            // 
            // _disciplineLessonTypeGridColumn
            // 
            this._disciplineLessonTypeGridColumn.Caption = "��� �������";
            this._disciplineLessonTypeGridColumn.FieldName = "LessonType";
            this._disciplineLessonTypeGridColumn.Name = "_disciplineLessonTypeGridColumn";
            // 
            // _disciplineUnitGgridColumn
            // 
            this._disciplineUnitGgridColumn.Caption = "������";
            this._disciplineUnitGgridColumn.FieldName = "Unit";
            this._disciplineUnitGgridColumn.Name = "_disciplineUnitGgridColumn";
            this._disciplineUnitGgridColumn.Tag = "������#r";
            this._disciplineUnitGgridColumn.Visible = true;
            this._disciplineUnitGgridColumn.VisibleIndex = 1;
            // 
            // _disciplinePartGridColumn
            // 
            this._disciplinePartGridColumn.Caption = "�����";
            this._disciplinePartGridColumn.FieldName = "Part";
            this._disciplinePartGridColumn.Name = "_disciplinePartGridColumn";
            this._disciplinePartGridColumn.Tag = "�����#r";
            this._disciplinePartGridColumn.Visible = true;
            this._disciplinePartGridColumn.VisibleIndex = 1;
            // 
            // lessonDisciplineGridColumn
            // 
            this.lessonDisciplineGridColumn.AppearanceHeader.Options.UseTextOptions = true;
            this.lessonDisciplineGridColumn.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lessonDisciplineGridColumn.Caption = "����";
            this.lessonDisciplineGridColumn.FieldName = "Discipline";
            this.lessonDisciplineGridColumn.Name = "lessonDisciplineGridColumn";
            this.lessonDisciplineGridColumn.Tag = "����#r";
            this.lessonDisciplineGridColumn.Visible = true;
            this.lessonDisciplineGridColumn.VisibleIndex = 0;
            this.lessonDisciplineGridColumn.Width = 138;
            // 
            // lessonTypeGridColumn
            // 
            this.lessonTypeGridColumn.AppearanceHeader.Options.UseTextOptions = true;
            this.lessonTypeGridColumn.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lessonTypeGridColumn.Caption = "��� �������";
            this.lessonTypeGridColumn.FieldName = "LessonType";
            this.lessonTypeGridColumn.Name = "lessonTypeGridColumn";
            this.lessonTypeGridColumn.Tag = "��� �������#r";
            this.lessonTypeGridColumn.Visible = true;
            this.lessonTypeGridColumn.VisibleIndex = 1;
            this.lessonTypeGridColumn.Width = 152;
            // 
            // lessonTotalDurationGridColumn
            // 
            this.lessonTotalDurationGridColumn.AppearanceHeader.Options.UseTextOptions = true;
            this.lessonTotalDurationGridColumn.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lessonTotalDurationGridColumn.Caption = "�����������������";
            this.lessonTotalDurationGridColumn.FieldName = "TotalDuration";
            this.lessonTotalDurationGridColumn.Name = "lessonTotalDurationGridColumn";
            this.lessonTotalDurationGridColumn.Tag = "�����������������#r";
            this.lessonTotalDurationGridColumn.Visible = true;
            this.lessonTotalDurationGridColumn.VisibleIndex = 2;
            this.lessonTotalDurationGridColumn.Width = 126;
            // 
            // _lessonsSchedulerControl
            // 
            this._lessonsSchedulerControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lessonsSchedulerControl.LimitInterval.Duration = System.TimeSpan.Parse("364.00:00:00");
            this._lessonsSchedulerControl.LimitInterval.Start = new System.DateTime(2009, 1, 1, 0, 0, 0, 0);
            this._lessonsSchedulerControl.Location = new System.Drawing.Point(0, 0);
            this._lessonsSchedulerControl.MenuManager = this._barManager;
            this._lessonsSchedulerControl.Name = "_lessonsSchedulerControl";
            this._lessonsSchedulerControl.OptionsBehavior.SelectOnRightClick = true;
            this._lessonsSchedulerControl.OptionsCustomization.AllowAppointmentConflicts = DevExpress.XtraScheduler.AppointmentConflictsMode.Custom;
            this._lessonsSchedulerControl.OptionsCustomization.AllowAppointmentDragBetweenResources = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this._lessonsSchedulerControl.OptionsCustomization.AllowAppointmentResize = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this._lessonsSchedulerControl.OptionsCustomization.AllowDisplayAppointmentForm = DevExpress.XtraScheduler.AllowDisplayAppointmentForm.Always;
            this._lessonsSchedulerControl.OptionsCustomization.AllowInplaceEditor = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this._lessonsSchedulerControl.OptionsRangeControl.RangeMaximum = new System.DateTime(2014, 9, 1, 0, 0, 0, 0);
            this._lessonsSchedulerControl.OptionsRangeControl.RangeMinimum = new System.DateTime(2014, 6, 1, 0, 0, 0, 0);
            this._lessonsSchedulerControl.OptionsView.ToolTipVisibility = DevExpress.XtraScheduler.ToolTipVisibility.Always;
            this._lessonsSchedulerControl.Size = new System.Drawing.Size(741, 381);
            this._lessonsSchedulerControl.Start = new System.DateTime(2009, 12, 30, 0, 0, 0, 0);
            this._lessonsSchedulerControl.Storage = this._schedulerStorage;
            this._lessonsSchedulerControl.TabIndex = 0;
            this._lessonsSchedulerControl.Tag = "���������#r";
            this._lessonsSchedulerControl.Text = "lessonsSchedulerControl";
            this._lessonsSchedulerControl.ToolTipController = this._toolTipController;
            this._lessonsSchedulerControl.Views.DayView.TimeRulers.Add(timeRuler1);
            this._lessonsSchedulerControl.Views.DayView.TimeScale = System.TimeSpan.Parse("01:00:00");
            this._lessonsSchedulerControl.Views.MonthView.CompressWeekend = false;
            this._lessonsSchedulerControl.Views.WorkWeekView.TimeRulers.Add(timeRuler2);
            this._lessonsSchedulerControl.Views.WorkWeekView.TimeScale = System.TimeSpan.Parse("01:00:00");
            this._lessonsSchedulerControl.InitAppointmentImages += new DevExpress.XtraScheduler.AppointmentImagesEventHandler(this.LessonsSchedulerControl_InitAppointmentImages);
            this._lessonsSchedulerControl.AppointmentDrag += new DevExpress.XtraScheduler.AppointmentDragEventHandler(this.LessonsSchedulerControl_AppointmentDrag);
            this._lessonsSchedulerControl.AppointmentDrop += new DevExpress.XtraScheduler.AppointmentDragEventHandler(this.LessonsSchedulerControl_AppointmentDrop);
            this._lessonsSchedulerControl.AllowAppointmentConflicts += new DevExpress.XtraScheduler.AppointmentConflictEventHandler(this.LessonsSchedulerControl_AllowAppointmentConflicts);
            this._lessonsSchedulerControl.PreparePopupMenu += new DevExpress.XtraScheduler.PreparePopupMenuEventHandler(this.LessonsSchedulerControl_PreparePopupMenu);
            this._lessonsSchedulerControl.EditAppointmentFormShowing += new DevExpress.XtraScheduler.AppointmentFormEventHandler(this.LessonsSchedulerControl_EditAppointmentFormShowing);
            this._lessonsSchedulerControl.CustomDrawTimeCell += new DevExpress.XtraScheduler.CustomDrawObjectEventHandler(this.LessonsSchedulerControl_CustomDrawTimeCell);
            this._lessonsSchedulerControl.Click += new System.EventHandler(this.LessonsSchedulerControlClick);
            // 
            // _barManager
            // 
            this._barManager.AllowCustomization = false;
            this._barManager.AllowMoveBarOnToolbar = false;
            this._barManager.AllowQuickCustomization = false;
            this._barManager.AllowShowToolbarsPopup = false;
            this._barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.activeViewBar1,
            this.navigatorBar1});
            this._barManager.DockControls.Add(this.barDockControlTop);
            this._barManager.DockControls.Add(this.barDockControlBottom);
            this._barManager.DockControls.Add(this.barDockControlLeft);
            this._barManager.DockControls.Add(this.barDockControlRight);
            this._barManager.Form = this;
            this._barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.viewSelectorItem1,
            this.viewSelectorItem2,
            this.viewSelectorItem3,
            this.viewSelectorItem4,
            this.viewSelectorItem5,
            this.viewNavigatorBackwardItem1,
            this.viewNavigatorForwardItem1,
            this.viewNavigatorTodayItem1,
            this.viewNavigatorZoomInItem1,
            this.viewNavigatorZoomOutItem1,
            this._refreshBarButtonItem,
            this._barButtonItemRemoveLesson,
            this._testReportBarButtonItem,
            this.switchToDayViewItem1,
            this.switchToWorkWeekViewItem1,
            this.switchToWeekViewItem1,
            this.switchToMonthViewItem1,
            this.switchToTimelineViewItem1,
            this.switchToGanttViewItem1,
            this.switchTimeScalesCaptionItem1,
            this.switchCompressWeekendItem1,
            this.switchShowWorkTimeOnlyItem1,
            this.switchCellsAutoHeightItem1,
            this.changeSnapToCellsUIItem1,
            this.switchTimeScalesItem1,
            this.changeScaleWidthItem1,
            this.newAppointmentItem1,
            this.newRecurringAppointmentItem1,
            this.navigateViewBackwardItem1,
            this.navigateViewForwardItem1,
            this.gotoTodayItem1,
            this.viewZoomInItem1,
            this.viewZoomOutItem1,
            this.groupByNoneItem1,
            this.groupByDateItem1,
            this.groupByResourceItem1});
            this._barManager.MaxItemId = 65;
            this._barManager.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemSpinEdit1,
            this.repositoryItemSpinEdit2});
            // 
            // activeViewBar1
            // 
            this.activeViewBar1.Control = this._lessonsSchedulerControl;
            this.activeViewBar1.DockCol = 1;
            this.activeViewBar1.DockRow = 0;
            this.activeViewBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.activeViewBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.gotoTodayItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.switchToDayViewItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.switchToWorkWeekViewItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.switchToWeekViewItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.switchToMonthViewItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.switchToTimelineViewItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.switchToGanttViewItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this._testReportBarButtonItem)});
            this.activeViewBar1.OptionsBar.DrawDragBorder = false;
            // 
            // gotoTodayItem1
            // 
            this.gotoTodayItem1.Id = 42;
            this.gotoTodayItem1.Name = "gotoTodayItem1";
            this.gotoTodayItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.gotoTodayItem1.Tag = "���������#r";
            // 
            // switchToDayViewItem1
            // 
            this.switchToDayViewItem1.Id = 31;
            this.switchToDayViewItem1.Name = "switchToDayViewItem1";
            this.switchToDayViewItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.switchToDayViewItem1.Tag = "���������#r";
            // 
            // switchToWorkWeekViewItem1
            // 
            this.switchToWorkWeekViewItem1.Id = 32;
            this.switchToWorkWeekViewItem1.Name = "switchToWorkWeekViewItem1";
            this.switchToWorkWeekViewItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.switchToWorkWeekViewItem1.Tag = "���������#r";
            // 
            // switchToWeekViewItem1
            // 
            this.switchToWeekViewItem1.Id = 33;
            this.switchToWeekViewItem1.Name = "switchToWeekViewItem1";
            this.switchToWeekViewItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.switchToWeekViewItem1.Tag = "���������#r";
            // 
            // switchToMonthViewItem1
            // 
            this.switchToMonthViewItem1.Id = 34;
            this.switchToMonthViewItem1.Name = "switchToMonthViewItem1";
            this.switchToMonthViewItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.switchToMonthViewItem1.Tag = "���������#r";
            // 
            // switchToTimelineViewItem1
            // 
            this.switchToTimelineViewItem1.Id = 35;
            this.switchToTimelineViewItem1.Name = "switchToTimelineViewItem1";
            this.switchToTimelineViewItem1.Tag = "���������#r";
            // 
            // switchToGanttViewItem1
            // 
            this.switchToGanttViewItem1.Id = 36;
            this.switchToGanttViewItem1.Name = "switchToGanttViewItem1";
            this.switchToGanttViewItem1.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // _testReportBarButtonItem
            // 
            this._testReportBarButtonItem.Caption = "���������� ����������";
            this._testReportBarButtonItem.Enabled = false;
            this._testReportBarButtonItem.Glyph = global::Curriculum.Properties.Resources.Report24;
            this._testReportBarButtonItem.Id = 28;
            this._testReportBarButtonItem.Name = "_testReportBarButtonItem";
            this._testReportBarButtonItem.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this._testReportBarButtonItem.Tag = "���������#r";
            this._testReportBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.TestReportBarButtonItemItemClick);
            // 
            // navigatorBar1
            // 
            this.navigatorBar1.Control = this._lessonsSchedulerControl;
            this.navigatorBar1.DockCol = 0;
            this.navigatorBar1.DockRow = 0;
            this.navigatorBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.navigatorBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this._refreshBarButtonItem),
            new DevExpress.XtraBars.LinkPersistInfo(this.navigateViewBackwardItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.navigateViewForwardItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.viewZoomInItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.viewZoomOutItem1)});
            this.navigatorBar1.OptionsBar.DrawDragBorder = false;
            // 
            // _refreshBarButtonItem
            // 
            this._refreshBarButtonItem.Caption = "��������";
            this._refreshBarButtonItem.Glyph = ((System.Drawing.Image)(resources.GetObject("_refreshBarButtonItem.Glyph")));
            this._refreshBarButtonItem.Id = 26;
            this._refreshBarButtonItem.Name = "_refreshBarButtonItem";
            this._refreshBarButtonItem.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem1.Text = "��������";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "��� ������� �� ������, ���������� ���������� �����.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this._refreshBarButtonItem.SuperTip = superToolTip1;
            this._refreshBarButtonItem.Tag = "���������#r";
            this._refreshBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.RefreshBarButtonItemItemClick);
            // 
            // navigateViewBackwardItem1
            // 
            this.navigateViewBackwardItem1.Id = 43;
            this.navigateViewBackwardItem1.Name = "navigateViewBackwardItem1";
            this.navigateViewBackwardItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.navigateViewBackwardItem1.Tag = "���������#r";
            // 
            // navigateViewForwardItem1
            // 
            this.navigateViewForwardItem1.Id = 44;
            this.navigateViewForwardItem1.Name = "navigateViewForwardItem1";
            this.navigateViewForwardItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.navigateViewForwardItem1.Tag = "���������#r";
            // 
            // viewZoomInItem1
            // 
            this.viewZoomInItem1.Id = 45;
            this.viewZoomInItem1.Name = "viewZoomInItem1";
            this.viewZoomInItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.viewZoomInItem1.Tag = "���������#r";
            // 
            // viewZoomOutItem1
            // 
            this.viewZoomOutItem1.Id = 46;
            this.viewZoomOutItem1.Name = "viewZoomOutItem1";
            this.viewZoomOutItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.viewZoomOutItem1.Tag = "���������#r";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(935, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 684);
            this.barDockControlBottom.Size = new System.Drawing.Size(935, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 658);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(935, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 658);
            // 
            // viewSelectorItem1
            // 
            this.viewSelectorItem1.Id = 56;
            this.viewSelectorItem1.Name = "viewSelectorItem1";
            this.viewSelectorItem1.SchedulerViewType = DevExpress.XtraScheduler.SchedulerViewType.Day;
            // 
            // viewSelectorItem2
            // 
            this.viewSelectorItem2.Id = 57;
            this.viewSelectorItem2.Name = "viewSelectorItem2";
            this.viewSelectorItem2.SchedulerViewType = DevExpress.XtraScheduler.SchedulerViewType.Day;
            // 
            // viewSelectorItem3
            // 
            this.viewSelectorItem3.Id = 58;
            this.viewSelectorItem3.Name = "viewSelectorItem3";
            this.viewSelectorItem3.SchedulerViewType = DevExpress.XtraScheduler.SchedulerViewType.Day;
            // 
            // viewSelectorItem4
            // 
            this.viewSelectorItem4.Id = 59;
            this.viewSelectorItem4.Name = "viewSelectorItem4";
            this.viewSelectorItem4.SchedulerViewType = DevExpress.XtraScheduler.SchedulerViewType.Day;
            // 
            // viewSelectorItem5
            // 
            this.viewSelectorItem5.Id = 60;
            this.viewSelectorItem5.Name = "viewSelectorItem5";
            this.viewSelectorItem5.SchedulerViewType = DevExpress.XtraScheduler.SchedulerViewType.Day;
            // 
            // viewNavigatorBackwardItem1
            // 
            this.viewNavigatorBackwardItem1.Id = 61;
            this.viewNavigatorBackwardItem1.Name = "viewNavigatorBackwardItem1";
            // 
            // viewNavigatorForwardItem1
            // 
            this.viewNavigatorForwardItem1.Id = 62;
            this.viewNavigatorForwardItem1.Name = "viewNavigatorForwardItem1";
            // 
            // viewNavigatorTodayItem1
            // 
            this.viewNavigatorTodayItem1.Caption = "";
            this.viewNavigatorTodayItem1.Id = 63;
            this.viewNavigatorTodayItem1.Name = "viewNavigatorTodayItem1";
            // 
            // viewNavigatorZoomInItem1
            // 
            this.viewNavigatorZoomInItem1.Id = 64;
            this.viewNavigatorZoomInItem1.Name = "viewNavigatorZoomInItem1";
            // 
            // viewNavigatorZoomOutItem1
            // 
            this.viewNavigatorZoomOutItem1.Id = 54;
            this.viewNavigatorZoomOutItem1.Name = "viewNavigatorZoomOutItem1";
            // 
            // _barButtonItemRemoveLesson
            // 
            this._barButtonItemRemoveLesson.Id = 55;
            this._barButtonItemRemoveLesson.Name = "_barButtonItemRemoveLesson";
            // 
            // switchTimeScalesCaptionItem1
            // 
            this.switchTimeScalesCaptionItem1.Id = 37;
            this.switchTimeScalesCaptionItem1.Name = "switchTimeScalesCaptionItem1";
            // 
            // switchCompressWeekendItem1
            // 
            this.switchCompressWeekendItem1.Id = 38;
            this.switchCompressWeekendItem1.Name = "switchCompressWeekendItem1";
            // 
            // switchShowWorkTimeOnlyItem1
            // 
            this.switchShowWorkTimeOnlyItem1.Id = 39;
            this.switchShowWorkTimeOnlyItem1.Name = "switchShowWorkTimeOnlyItem1";
            // 
            // switchCellsAutoHeightItem1
            // 
            this.switchCellsAutoHeightItem1.Id = 40;
            this.switchCellsAutoHeightItem1.Name = "switchCellsAutoHeightItem1";
            // 
            // changeSnapToCellsUIItem1
            // 
            this.changeSnapToCellsUIItem1.Id = 41;
            this.changeSnapToCellsUIItem1.Name = "changeSnapToCellsUIItem1";
            // 
            // switchTimeScalesItem1
            // 
            this.switchTimeScalesItem1.Id = 47;
            this.switchTimeScalesItem1.Name = "switchTimeScalesItem1";
            // 
            // changeScaleWidthItem1
            // 
            this.changeScaleWidthItem1.Edit = this.repositoryItemSpinEdit2;
            this.changeScaleWidthItem1.Id = 48;
            this.changeScaleWidthItem1.Name = "changeScaleWidthItem1";
            this.changeScaleWidthItem1.UseCommandCaption = true;
            // 
            // repositoryItemSpinEdit2
            // 
            this.repositoryItemSpinEdit2.AutoHeight = false;
            this.repositoryItemSpinEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit2.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.repositoryItemSpinEdit2.MaxValue = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.repositoryItemSpinEdit2.MinValue = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.repositoryItemSpinEdit2.Name = "repositoryItemSpinEdit2";
            // 
            // newAppointmentItem1
            // 
            this.newAppointmentItem1.Id = 49;
            this.newAppointmentItem1.Name = "newAppointmentItem1";
            // 
            // newRecurringAppointmentItem1
            // 
            this.newRecurringAppointmentItem1.Id = 50;
            this.newRecurringAppointmentItem1.Name = "newRecurringAppointmentItem1";
            // 
            // groupByNoneItem1
            // 
            this.groupByNoneItem1.Id = 51;
            this.groupByNoneItem1.Name = "groupByNoneItem1";
            // 
            // groupByDateItem1
            // 
            this.groupByDateItem1.Id = 52;
            this.groupByDateItem1.Name = "groupByDateItem1";
            // 
            // groupByResourceItem1
            // 
            this.groupByResourceItem1.Id = 53;
            this.groupByResourceItem1.Name = "groupByResourceItem1";
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit1.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.repositoryItemSpinEdit1.MaxValue = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.MinValue = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // _schedulerStorage
            // 
            this._schedulerStorage.AppointmentDeleting += new DevExpress.XtraScheduler.PersistentObjectCancelEventHandler(this.SchedulerStorageAppointmentDeleting);
            // 
            // _toolTipController
            // 
            this._toolTipController.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(221)))), ((int)(((byte)(233)))));
            this._toolTipController.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(242)))), ((int)(((byte)(245)))));
            this._toolTipController.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(199)))), ((int)(((byte)(223)))));
            this._toolTipController.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(120)))), ((int)(((byte)(127)))));
            this._toolTipController.Appearance.Options.UseBackColor = true;
            this._toolTipController.Appearance.Options.UseBorderColor = true;
            this._toolTipController.Appearance.Options.UseFont = true;
            this._toolTipController.Appearance.Options.UseForeColor = true;
            this._toolTipController.AppearanceTitle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(221)))), ((int)(((byte)(233)))));
            this._toolTipController.AppearanceTitle.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(199)))), ((int)(((byte)(223)))));
            this._toolTipController.AppearanceTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(53)))), ((int)(((byte)(60)))));
            this._toolTipController.AppearanceTitle.Options.UseBackColor = true;
            this._toolTipController.AppearanceTitle.Options.UseBorderColor = true;
            this._toolTipController.AppearanceTitle.Options.UseFont = true;
            this._toolTipController.AppearanceTitle.Options.UseForeColor = true;
            this._toolTipController.AutoPopDelay = 60000;
            this._toolTipController.Rounded = true;
            this._toolTipController.ShowBeak = true;
            this._toolTipController.BeforeShow += new DevExpress.Utils.ToolTipControllerBeforeShowEventHandler(this.ToolTipController_BeforeShow);
            // 
            // splitterControl1
            // 
            this.splitterControl1.Location = new System.Drawing.Point(537, 0);
            this.splitterControl1.Name = "splitterControl1";
            this.splitterControl1.Size = new System.Drawing.Size(6, 271);
            this.splitterControl1.TabIndex = 3;
            this.splitterControl1.TabStop = false;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 26);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this._lessonsSchedulerControl);
            this.splitContainerControl1.Panel1.Controls.Add(this.splitterControl2);
            this.splitContainerControl1.Panel1.Controls.Add(this.dateNavigator1);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.xtraTabControl1);
            this.splitContainerControl1.Panel2.Controls.Add(this.splitterControl1);
            this.splitContainerControl1.Panel2.Controls.Add(this.xtraTabControl2);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(935, 658);
            this.splitContainerControl1.SplitterPosition = 381;
            this.splitContainerControl1.TabIndex = 5;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // splitterControl2
            // 
            this.splitterControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitterControl2.Location = new System.Drawing.Point(741, 0);
            this.splitterControl2.Name = "splitterControl2";
            this.splitterControl2.Size = new System.Drawing.Size(6, 381);
            this.splitterControl2.TabIndex = 2;
            this.splitterControl2.TabStop = false;
            // 
            // dateNavigator1
            // 
            this.dateNavigator1.Dock = System.Windows.Forms.DockStyle.Right;
            this.dateNavigator1.HotDate = null;
            this.dateNavigator1.Location = new System.Drawing.Point(747, 0);
            this.dateNavigator1.Name = "dateNavigator1";
            this.dateNavigator1.SchedulerControl = this._lessonsSchedulerControl;
            this.dateNavigator1.Size = new System.Drawing.Size(188, 381);
            this.dateNavigator1.TabIndex = 3;
            this.dateNavigator1.Tag = "���������#r";
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(543, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(392, 271);
            this.xtraTabControl1.TabIndex = 1;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this._coursesGridControl);
            this.xtraTabPage1.Controls.Add(this._coursesPanelControl);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(387, 245);
            this.xtraTabPage1.Tag = "������� �����#r";
            this.xtraTabPage1.Text = "����";
            // 
            // _coursesPanelControl
            // 
            this._coursesPanelControl.Controls.Add(this._clearLessonsSimpleButton);
            this._coursesPanelControl.Controls.Add(this._autoAllocationSimpleButton);
            this._coursesPanelControl.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._coursesPanelControl.Location = new System.Drawing.Point(0, 215);
            this._coursesPanelControl.Name = "_coursesPanelControl";
            this._coursesPanelControl.Size = new System.Drawing.Size(387, 30);
            this._coursesPanelControl.TabIndex = 6;
            // 
            // _clearLessonsSimpleButton
            // 
            this._clearLessonsSimpleButton.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this._clearLessonsSimpleButton.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this._clearLessonsSimpleButton.Location = new System.Drawing.Point(619, -103);
            this._clearLessonsSimpleButton.Name = "_clearLessonsSimpleButton";
            this._clearLessonsSimpleButton.Size = new System.Drawing.Size(140, 26);
            this._clearLessonsSimpleButton.TabIndex = 3;
            this._clearLessonsSimpleButton.Text = "������� ������ ����";
            this._clearLessonsSimpleButton.ToolTip = "������� ��� �������";
            this._clearLessonsSimpleButton.Click += new System.EventHandler(this.ClearLessonsSimpleButtonClick);
            // 
            // _autoAllocationSimpleButton
            // 
            this._autoAllocationSimpleButton.Dock = System.Windows.Forms.DockStyle.Right;
            this._autoAllocationSimpleButton.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this._autoAllocationSimpleButton.Location = new System.Drawing.Point(204, 2);
            this._autoAllocationSimpleButton.Name = "_autoAllocationSimpleButton";
            this._autoAllocationSimpleButton.Size = new System.Drawing.Size(181, 26);
            this._autoAllocationSimpleButton.TabIndex = 2;
            this._autoAllocationSimpleButton.Text = "������������ �������������";
            this._autoAllocationSimpleButton.ToolTip = "�������������� �������� ������� � ������� ����� �� ������������ �����.";
            this._autoAllocationSimpleButton.Click += new System.EventHandler(this.AutoAllocationSimpleButtonClick);
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this._disciplinesGridControl);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(387, 244);
            this.xtraTabPage2.Tag = "������� ��� ����#r";
            this.xtraTabPage2.Text = "��� ����";
            // 
            // xtraTabControl2
            // 
            this.xtraTabControl2.Dock = System.Windows.Forms.DockStyle.Left;
            this.xtraTabControl2.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl2.Name = "xtraTabControl2";
            this.xtraTabControl2.SelectedTabPage = this.xtraTabPage3;
            this.xtraTabControl2.Size = new System.Drawing.Size(537, 271);
            this.xtraTabControl2.TabIndex = 4;
            this.xtraTabControl2.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage3});
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this._lessonsGridControl);
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(532, 245);
            this.xtraTabPage3.Tag = "������� ��������� ����#r";
            this.xtraTabPage3.Text = "��������� ����";
            // 
            // _lessonsGridControl
            // 
            this._lessonsGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lessonsGridControl.Location = new System.Drawing.Point(0, 0);
            this._lessonsGridControl.MainView = this._lessonsGridView;
            this._lessonsGridControl.Name = "_lessonsGridControl";
            this._lessonsGridControl.Size = new System.Drawing.Size(532, 245);
            this._lessonsGridControl.TabIndex = 3;
            this._lessonsGridControl.Tag = "..\\������� ��������� ����#r";
            this._lessonsGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this._lessonsGridView});
            // 
            // _lessonsGridView
            // 
            this._lessonsGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.lessonDisciplineGridColumn,
            this.lessonTypeGridColumn,
            this.lessonTotalDurationGridColumn});
            this._lessonsGridView.GridControl = this._lessonsGridControl;
            this._lessonsGridView.Name = "_lessonsGridView";
            this._lessonsGridView.OptionsBehavior.Editable = false;
            this._lessonsGridView.OptionsDetail.EnableMasterViewMode = false;
            // 
            // schedulerBarController1
            // 
            this.schedulerBarController1.BarItems.Add(this.switchToDayViewItem1);
            this.schedulerBarController1.BarItems.Add(this.switchToWorkWeekViewItem1);
            this.schedulerBarController1.BarItems.Add(this.switchToWeekViewItem1);
            this.schedulerBarController1.BarItems.Add(this.switchToMonthViewItem1);
            this.schedulerBarController1.BarItems.Add(this.switchToTimelineViewItem1);
            this.schedulerBarController1.BarItems.Add(this.switchToGanttViewItem1);
            this.schedulerBarController1.BarItems.Add(this.switchTimeScalesCaptionItem1);
            this.schedulerBarController1.BarItems.Add(this.switchCompressWeekendItem1);
            this.schedulerBarController1.BarItems.Add(this.switchShowWorkTimeOnlyItem1);
            this.schedulerBarController1.BarItems.Add(this.switchCellsAutoHeightItem1);
            this.schedulerBarController1.BarItems.Add(this.changeSnapToCellsUIItem1);
            this.schedulerBarController1.BarItems.Add(this.switchTimeScalesItem1);
            this.schedulerBarController1.BarItems.Add(this.changeScaleWidthItem1);
            this.schedulerBarController1.BarItems.Add(this.newAppointmentItem1);
            this.schedulerBarController1.BarItems.Add(this.newRecurringAppointmentItem1);
            this.schedulerBarController1.BarItems.Add(this.navigateViewBackwardItem1);
            this.schedulerBarController1.BarItems.Add(this.navigateViewForwardItem1);
            this.schedulerBarController1.BarItems.Add(this.gotoTodayItem1);
            this.schedulerBarController1.BarItems.Add(this.viewZoomInItem1);
            this.schedulerBarController1.BarItems.Add(this.viewZoomOutItem1);
            this.schedulerBarController1.BarItems.Add(this.groupByNoneItem1);
            this.schedulerBarController1.BarItems.Add(this.groupByDateItem1);
            this.schedulerBarController1.BarItems.Add(this.groupByResourceItem1);
            this.schedulerBarController1.Control = this._lessonsSchedulerControl;
            // 
            // LessonsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "LessonsControl";
            this.Size = new System.Drawing.Size(935, 684);
            this.Tag = "���������� ���������#r";
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._coursesGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._coursesGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._disciplinesGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._disciplinesGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._lessonsSchedulerControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._schedulerStorage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dateNavigator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._coursesPanelControl)).EndInit();
            this._coursesPanelControl.ResumeLayout(false);
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl2)).EndInit();
            this.xtraTabControl2.ResumeLayout(false);
            this.xtraTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lessonsGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._lessonsGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerBarController1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.Columns.GridColumn _coursePartGridColumn;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.GridControl _coursesGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView _coursesGridView;
        private DevExpress.XtraGrid.Columns.GridColumn _courseUnitGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _courseGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _courseLessonTypeGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _courseTotalDurationGridColumn;
        private DevExpress.XtraGrid.GridControl _disciplinesGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView _disciplinesGridView;
        private DevExpress.XtraGrid.Columns.GridColumn _disciplineGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _disciplineLessonTypeGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _disciplineUnitGgridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _disciplinePartGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn lessonDisciplineGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn lessonTypeGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn lessonTotalDurationGridColumn;
        private DevExpress.XtraScheduler.SchedulerControl _lessonsSchedulerControl;
        private DevExpress.XtraScheduler.SchedulerStorage _schedulerStorage;
        private DevExpress.XtraGrid.Views.Grid.GridView _lessonsGridView;
        private DevExpress.XtraGrid.GridControl _lessonsGridControl;
        private DevExpress.XtraEditors.SplitterControl splitterControl1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.SplitterControl splitterControl2;
        private DevExpress.XtraScheduler.DateNavigator dateNavigator1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        //private DevExpress.XtraScheduler.UI.ViewSelector _viewSelector;
        private DevExpress.Utils.ToolTipController _toolTipController;
        //private DevExpress.XtraScheduler.UI.ViewNavigator _viewNavigator;
        private DevExpress.XtraEditors.PanelControl _coursesPanelControl;
        private DevExpress.XtraEditors.SimpleButton _autoAllocationSimpleButton;
        private DevExpress.XtraEditors.SimpleButton _clearLessonsSimpleButton;
        private DevExpress.XtraBars.BarManager _barManager;
        private DevExpress.XtraBars.BarButtonItem _refreshBarButtonItem;
        private DevExpress.XtraBars.BarButtonItem _testReportBarButtonItem;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem _barButtonItemRemoveLesson;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit2;
        private DevExpress.XtraScheduler.UI.ViewSelectorItem viewSelectorItem1;
        private DevExpress.XtraScheduler.UI.ViewSelectorItem viewSelectorItem2;
        private DevExpress.XtraScheduler.UI.ViewSelectorItem viewSelectorItem3;
        private DevExpress.XtraScheduler.UI.ViewSelectorItem viewSelectorItem4;
        private DevExpress.XtraScheduler.UI.ViewSelectorItem viewSelectorItem5;
        private DevExpress.XtraScheduler.UI.ViewNavigatorBackwardItem viewNavigatorBackwardItem1;
        private DevExpress.XtraScheduler.UI.ViewNavigatorForwardItem viewNavigatorForwardItem1;
        private DevExpress.XtraScheduler.UI.ViewNavigatorTodayItem viewNavigatorTodayItem1;
        private DevExpress.XtraScheduler.UI.ViewNavigatorZoomInItem viewNavigatorZoomInItem1;
        private DevExpress.XtraScheduler.UI.ViewNavigatorZoomOutItem viewNavigatorZoomOutItem1;
        private DevExpress.XtraScheduler.UI.ActiveViewBar activeViewBar1;
        private DevExpress.XtraScheduler.UI.SwitchToDayViewItem switchToDayViewItem1;
        private DevExpress.XtraScheduler.UI.SwitchToWorkWeekViewItem switchToWorkWeekViewItem1;
        private DevExpress.XtraScheduler.UI.SwitchToWeekViewItem switchToWeekViewItem1;
        private DevExpress.XtraScheduler.UI.SwitchToMonthViewItem switchToMonthViewItem1;
        private DevExpress.XtraScheduler.UI.SwitchToTimelineViewItem switchToTimelineViewItem1;
        private DevExpress.XtraScheduler.UI.SwitchToGanttViewItem switchToGanttViewItem1;
        private DevExpress.XtraScheduler.UI.SwitchTimeScalesCaptionItem switchTimeScalesCaptionItem1;
        private DevExpress.XtraScheduler.UI.SwitchCompressWeekendItem switchCompressWeekendItem1;
        private DevExpress.XtraScheduler.UI.SwitchShowWorkTimeOnlyItem switchShowWorkTimeOnlyItem1;
        private DevExpress.XtraScheduler.UI.SwitchCellsAutoHeightItem switchCellsAutoHeightItem1;
        private DevExpress.XtraScheduler.UI.ChangeSnapToCellsUIItem changeSnapToCellsUIItem1;
        private DevExpress.XtraScheduler.UI.SchedulerBarController schedulerBarController1;
        private DevExpress.XtraScheduler.UI.GotoTodayItem gotoTodayItem1;
        private DevExpress.XtraScheduler.UI.NavigatorBar navigatorBar1;
        private DevExpress.XtraScheduler.UI.NavigateViewBackwardItem navigateViewBackwardItem1;
        private DevExpress.XtraScheduler.UI.NavigateViewForwardItem navigateViewForwardItem1;
        private DevExpress.XtraScheduler.UI.ViewZoomInItem viewZoomInItem1;
        private DevExpress.XtraScheduler.UI.ViewZoomOutItem viewZoomOutItem1;
        private DevExpress.XtraScheduler.UI.SwitchTimeScalesItem switchTimeScalesItem1;
        private DevExpress.XtraScheduler.UI.ChangeScaleWidthItem changeScaleWidthItem1;
        private DevExpress.XtraScheduler.UI.NewAppointmentItem newAppointmentItem1;
        private DevExpress.XtraScheduler.UI.NewRecurringAppointmentItem newRecurringAppointmentItem1;
        private DevExpress.XtraScheduler.UI.GroupByNoneItem groupByNoneItem1;
        private DevExpress.XtraScheduler.UI.GroupByDateItem groupByDateItem1;
        private DevExpress.XtraScheduler.UI.GroupByResourceItem groupByResourceItem1;
    }
}

namespace Curriculum
{
    partial class RtfEditControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RtfEditControl));
            this._barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this._boldBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this._italicBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this._underlineBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this._alignLeftBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this._centerBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this._alignRightBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this._fontBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this._bulletsBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.bar4 = new DevExpress.XtraBars.Bar();
            this._cutBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this._copyBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this._pasteBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this._undoBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this._imageList = new System.Windows.Forms.ImageList(this.components);
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this._richTextBox = new System.Windows.Forms.RichTextBox();
            ((System.ComponentModel.ISupportInitialize)(this._barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            this.SuspendLayout();
            // 
            // _barManager
            // 
            this._barManager.AllowCustomization = false;
            this._barManager.AllowQuickCustomization = false;
            this._barManager.AllowShowToolbarsPopup = false;
            this._barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar4});
            this._barManager.DockControls.Add(this.barDockControlTop);
            this._barManager.DockControls.Add(this.barDockControlBottom);
            this._barManager.DockControls.Add(this.barDockControlLeft);
            this._barManager.DockControls.Add(this.barDockControlRight);
            this._barManager.Form = this;
            this._barManager.Images = this._imageList;
            this._barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this._boldBarButtonItem,
            this._italicBarButtonItem,
            this._underlineBarButtonItem,
            this._alignLeftBarButtonItem,
            this._centerBarButtonItem,
            this._alignRightBarButtonItem,
            this._bulletsBarButtonItem,
            this._fontBarButtonItem,
            this._copyBarButtonItem,
            this._cutBarButtonItem,
            this._pasteBarButtonItem,
            this._undoBarButtonItem});
            this._barManager.MaxItemId = 20;
            this._barManager.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1,
            this.repositoryItemTextEdit2});
            this._barManager.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BarManager_ItemClick);
            // 
            // bar1
            // 
            this.bar1.BarName = "Format";
            this.bar1.DockCol = 1;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(52, 144);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this._boldBarButtonItem),
            new DevExpress.XtraBars.LinkPersistInfo(this._italicBarButtonItem),
            new DevExpress.XtraBars.LinkPersistInfo(this._underlineBarButtonItem),
            new DevExpress.XtraBars.LinkPersistInfo(this._alignLeftBarButtonItem, true),
            new DevExpress.XtraBars.LinkPersistInfo(this._centerBarButtonItem),
            new DevExpress.XtraBars.LinkPersistInfo(this._alignRightBarButtonItem),
            new DevExpress.XtraBars.LinkPersistInfo(this._fontBarButtonItem, true),
            new DevExpress.XtraBars.LinkPersistInfo(this._bulletsBarButtonItem)});
            this.bar1.Offset = 94;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.Text = "������";
            // 
            // _boldBarButtonItem
            // 
            this._boldBarButtonItem.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this._boldBarButtonItem.Caption = "����&������";
            this._boldBarButtonItem.Glyph = global::Curriculum.Properties.Resources.Bold16;
            this._boldBarButtonItem.Hint = "����������";
            this._boldBarButtonItem.Id = 0;
            this._boldBarButtonItem.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.B));
            this._boldBarButtonItem.Name = "_boldBarButtonItem";
            this._boldBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.FontStyleBarButtonItem_ItemClick);
            // 
            // _italicBarButtonItem
            // 
            this._italicBarButtonItem.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this._italicBarButtonItem.Caption = "&������";
            this._italicBarButtonItem.Glyph = global::Curriculum.Properties.Resources.Italic16;
            this._italicBarButtonItem.Hint = "������";
            this._italicBarButtonItem.Id = 1;
            this._italicBarButtonItem.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.I));
            this._italicBarButtonItem.Name = "_italicBarButtonItem";
            this._italicBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.FontStyleBarButtonItem_ItemClick);
            // 
            // _underlineBarButtonItem
            // 
            this._underlineBarButtonItem.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this._underlineBarButtonItem.Caption = "���&���������";
            this._underlineBarButtonItem.Glyph = global::Curriculum.Properties.Resources.Underline16;
            this._underlineBarButtonItem.Hint = "������������";
            this._underlineBarButtonItem.Id = 2;
            this._underlineBarButtonItem.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.U));
            this._underlineBarButtonItem.Name = "_underlineBarButtonItem";
            this._underlineBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.FontStyleBarButtonItem_ItemClick);
            // 
            // _alignLeftBarButtonItem
            // 
            this._alignLeftBarButtonItem.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this._alignLeftBarButtonItem.Caption = "�� &������ ����";
            this._alignLeftBarButtonItem.CategoryGuid = new System.Guid("d3052f28-4b3e-4bae-b581-b3bb1c432258");
            this._alignLeftBarButtonItem.Glyph = global::Curriculum.Properties.Resources.AlignLeft16;
            this._alignLeftBarButtonItem.GroupIndex = 1;
            this._alignLeftBarButtonItem.Hint = "�� ������ ����";
            this._alignLeftBarButtonItem.Id = 3;
            this._alignLeftBarButtonItem.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.L));
            this._alignLeftBarButtonItem.Name = "_alignLeftBarButtonItem";
            this._alignLeftBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.AlignBarButtonItem_ItemClick);
            // 
            // _centerBarButtonItem
            // 
            this._centerBarButtonItem.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this._centerBarButtonItem.Caption = "�� �&�����";
            this._centerBarButtonItem.CategoryGuid = new System.Guid("d3052f28-4b3e-4bae-b581-b3bb1c432258");
            this._centerBarButtonItem.Glyph = global::Curriculum.Properties.Resources.AlignCenter16;
            this._centerBarButtonItem.GroupIndex = 1;
            this._centerBarButtonItem.Hint = "�� ������";
            this._centerBarButtonItem.Id = 4;
            this._centerBarButtonItem.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E));
            this._centerBarButtonItem.Name = "_centerBarButtonItem";
            this._centerBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.AlignBarButtonItem_ItemClick);
            // 
            // _alignRightBarButtonItem
            // 
            this._alignRightBarButtonItem.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this._alignRightBarButtonItem.Caption = "�� &������� ����";
            this._alignRightBarButtonItem.Glyph = global::Curriculum.Properties.Resources.AlignRight16;
            this._alignRightBarButtonItem.GroupIndex = 1;
            this._alignRightBarButtonItem.Hint = "�� ������� ����";
            this._alignRightBarButtonItem.Id = 5;
            this._alignRightBarButtonItem.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.R));
            this._alignRightBarButtonItem.Name = "_alignRightBarButtonItem";
            this._alignRightBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.AlignBarButtonItem_ItemClick);
            // 
            // _fontBarButtonItem
            // 
            this._fontBarButtonItem.Caption = "&�����";
            this._fontBarButtonItem.Glyph = global::Curriculum.Properties.Resources.Font16;
            this._fontBarButtonItem.Hint = "�����";
            this._fontBarButtonItem.Id = 8;
            this._fontBarButtonItem.Name = "_fontBarButtonItem";
            this._fontBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.FontBarButtonItem_ItemClick);
            // 
            // _bulletsBarButtonItem
            // 
            this._bulletsBarButtonItem.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this._bulletsBarButtonItem.Caption = "&������";
            this._bulletsBarButtonItem.Glyph = global::Curriculum.Properties.Resources.Bullets16;
            this._bulletsBarButtonItem.Hint = "������";
            this._bulletsBarButtonItem.Id = 7;
            this._bulletsBarButtonItem.Name = "_bulletsBarButtonItem";
            this._bulletsBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BulletsBarButtonItem_ItemClick);
            // 
            // bar4
            // 
            this.bar4.BarName = "Standard";
            this.bar4.DockCol = 0;
            this.bar4.DockRow = 0;
            this.bar4.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar4.FloatLocation = new System.Drawing.Point(312, 156);
            this.bar4.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this._cutBarButtonItem),
            new DevExpress.XtraBars.LinkPersistInfo(this._copyBarButtonItem),
            new DevExpress.XtraBars.LinkPersistInfo(this._pasteBarButtonItem),
            new DevExpress.XtraBars.LinkPersistInfo(this._undoBarButtonItem, true)});
            this.bar4.OptionsBar.DrawDragBorder = false;
            this.bar4.Text = "�����������";
            // 
            // _cutBarButtonItem
            // 
            this._cutBarButtonItem.Caption = "��&������";
            this._cutBarButtonItem.Glyph = global::Curriculum.Properties.Resources.Cut16;
            this._cutBarButtonItem.Hint = "��������";
            this._cutBarButtonItem.Id = 13;
            this._cutBarButtonItem.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X));
            this._cutBarButtonItem.Name = "_cutBarButtonItem";
            this._cutBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.CutBarButtonItem_ItemClick);
            // 
            // _copyBarButtonItem
            // 
            this._copyBarButtonItem.Caption = "���&�������";
            this._copyBarButtonItem.Glyph = global::Curriculum.Properties.Resources.Copy16;
            this._copyBarButtonItem.Hint = "����������";
            this._copyBarButtonItem.Id = 12;
            this._copyBarButtonItem.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C));
            this._copyBarButtonItem.Name = "_copyBarButtonItem";
            this._copyBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.CopyBarButtonItem_ItemClick);
            // 
            // _pasteBarButtonItem
            // 
            this._pasteBarButtonItem.Caption = "&��������";
            this._pasteBarButtonItem.Glyph = global::Curriculum.Properties.Resources.Paste16;
            this._pasteBarButtonItem.Hint = "��������";
            this._pasteBarButtonItem.Id = 14;
            this._pasteBarButtonItem.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V));
            this._pasteBarButtonItem.Name = "_pasteBarButtonItem";
            this._pasteBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.PasteBarButtonItem_ItemClick);
            // 
            // _undoBarButtonItem
            // 
            this._undoBarButtonItem.Caption = "&��������";
            this._undoBarButtonItem.Glyph = global::Curriculum.Properties.Resources.Undo16;
            this._undoBarButtonItem.Hint = "��������";
            this._undoBarButtonItem.Id = 19;
            this._undoBarButtonItem.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z));
            this._undoBarButtonItem.Name = "_undoBarButtonItem";
            this._undoBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.UndoBarButtonItem_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(463, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 423);
            this.barDockControlBottom.Size = new System.Drawing.Size(463, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 397);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(463, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 397);
            // 
            // _imageList
            // 
            this._imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("_imageList.ImageStream")));
            this._imageList.TransparentColor = System.Drawing.Color.Magenta;
            this._imageList.Images.SetKeyName(0, "text_bold.png");
            this._imageList.Images.SetKeyName(1, "text_italic.png");
            this._imageList.Images.SetKeyName(2, "text_underline.png");
            this._imageList.Images.SetKeyName(3, "text_align_left.png");
            this._imageList.Images.SetKeyName(4, "text_align_center.png");
            this._imageList.Images.SetKeyName(5, "text_align_right.png");
            this._imageList.Images.SetKeyName(6, "font.png");
            this._imageList.Images.SetKeyName(7, "font_color.png");
            this._imageList.Images.SetKeyName(8, "text_list_bullets.png");
            this._imageList.Images.SetKeyName(9, "Cut-16x16.png");
            this._imageList.Images.SetKeyName(10, "Paste-16x16.png");
            this._imageList.Images.SetKeyName(11, "Undo-16x16.png");
            this._imageList.Images.SetKeyName(12, "Redo-16x16.png");
            this._imageList.Images.SetKeyName(13, "Copy-16x16.png");
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // _richTextBox
            // 
            this._richTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this._richTextBox.Location = new System.Drawing.Point(0, 26);
            this._richTextBox.Name = "_richTextBox";
            this._richTextBox.Size = new System.Drawing.Size(463, 397);
            this._richTextBox.TabIndex = 4;
            this._richTextBox.Text = "";
            this._richTextBox.SelectionChanged += new System.EventHandler(this.RichTextBox_SelectionChanged);
            this._richTextBox.TextChanged += new System.EventHandler(this.RichTextBox_TextChanged);
            // 
            // RtfEditControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._richTextBox);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.MinimumSize = new System.Drawing.Size(420, 100);
            this.Name = "RtfEditControl";
            this.Size = new System.Drawing.Size(463, 423);
            this.Load += new System.EventHandler(this.RtfEditControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this._barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.BarManager _barManager;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem _boldBarButtonItem;
        private DevExpress.XtraBars.BarButtonItem _italicBarButtonItem;
        private DevExpress.XtraBars.BarButtonItem _underlineBarButtonItem;
        private System.Windows.Forms.ImageList _imageList;
        private DevExpress.XtraBars.BarButtonItem _alignLeftBarButtonItem;
        private DevExpress.XtraBars.BarButtonItem _centerBarButtonItem;
        private DevExpress.XtraBars.BarButtonItem _alignRightBarButtonItem;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraBars.BarButtonItem _bulletsBarButtonItem;
        private DevExpress.XtraBars.BarButtonItem _fontBarButtonItem;
        private DevExpress.XtraBars.Bar bar4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraBars.BarButtonItem _copyBarButtonItem;
        private DevExpress.XtraBars.BarButtonItem _cutBarButtonItem;
        private DevExpress.XtraBars.BarButtonItem _pasteBarButtonItem;
        private System.Windows.Forms.RichTextBox _richTextBox;
        private DevExpress.XtraBars.BarButtonItem _undoBarButtonItem;
    }
}

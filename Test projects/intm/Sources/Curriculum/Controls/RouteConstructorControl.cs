﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Curriculum.PersonPage;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using Eureca.Integrator.Common.Controls;
using NHibernate.Linq.Visitors;

namespace Curriculum.Controls
{
    public class RouteConstructorControl : AutoWidthLayoutPanel
    {
        private List<TargetView> _targets;


        private readonly SimpleButton _addTargetButton = new SimpleButton( )
        {
            ButtonStyle = BorderStyles.NoBorder,
            Text = "Добавить этап"
        };
        
        public List<TargetView> Targets
        {
            get { return _targets; }
        }

        public RouteConstructorControl()
        {
            _addTargetButton.Click += _addTargetButton_Click;
            this.Controls.Add(_addTargetButton);
        }

        

        private void _addTargetButton_Click(object sender, EventArgs e)
        {

        }

        protected override void BeforeLayoutEvent()
        {
            var controlCount = this.Controls.Count;
            var buttonIndex = this.Controls.IndexOf(_addTargetButton);
            var lastItemIndex = controlCount - 1;
            if (buttonIndex == lastItemIndex)
                return;
            var lastControl = this.Controls[lastItemIndex];
            this.Controls.SetChildIndex(lastControl,buttonIndex);
            this.Controls.SetChildIndex(_addTargetButton,lastItemIndex);
        }
    }
}

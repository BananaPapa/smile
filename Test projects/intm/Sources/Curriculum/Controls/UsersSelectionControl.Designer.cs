﻿namespace Curriculum.Controls
{
    partial class UsersSelectionControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if (disposing && ( components != null ))
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            this._usersGridControl = new DevExpress.XtraGrid.GridControl();
            this._userView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._userName = new DevExpress.XtraGrid.Columns.GridColumn();
            this._userDepartments = new DevExpress.XtraGrid.Columns.GridColumn();
            this._userPosition = new DevExpress.XtraGrid.Columns.GridColumn();
            this._userVisibility = new DevExpress.XtraGrid.Columns.GridColumn();
            this._userSelectionCheckEdit = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this._selectAllCheckBox = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this._usersGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._userView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._userSelectionCheckEdit)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _usersGridControl
            // 
            this._usersGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._usersGridControl.Location = new System.Drawing.Point(0, 0);
            this._usersGridControl.MainView = this._userView;
            this._usersGridControl.Name = "_usersGridControl";
            this._usersGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this._userSelectionCheckEdit});
            this._usersGridControl.Size = new System.Drawing.Size(595, 552);
            this._usersGridControl.TabIndex = 3;
            this._usersGridControl.Tag = "Участники группы#r";
            this._usersGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this._userView});
            // 
            // _userView
            // 
            this._userView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._userName,
            this._userDepartments,
            this._userPosition,
            this._userVisibility});
            this._userView.GridControl = this._usersGridControl;
            this._userView.Name = "_userView";
            // 
            // _userName
            // 
            this._userName.Caption = "ФИО";
            this._userName.FieldName = "Model.Name";
            this._userName.Name = "_userName";
            this._userName.OptionsColumn.AllowEdit = false;
            this._userName.OptionsColumn.ReadOnly = true;
            this._userName.Tag = "Имя#r";
            this._userName.Visible = true;
            this._userName.VisibleIndex = 0;
            // 
            // _userDepartments
            // 
            this._userDepartments.Caption = "Отдел";
            this._userDepartments.FieldName = "UserDepartment.Name";
            this._userDepartments.Name = "_userDepartments";
            this._userDepartments.OptionsColumn.AllowEdit = false;
            this._userDepartments.OptionsColumn.ReadOnly = true;
            this._userDepartments.Tag = "Отдел#r";
            this._userDepartments.Visible = true;
            this._userDepartments.VisibleIndex = 1;
            // 
            // _userPosition
            // 
            this._userPosition.Caption = "Должность";
            this._userPosition.FieldName = "UserPosition.Name";
            this._userPosition.Name = "_userPosition";
            this._userPosition.OptionsColumn.AllowEdit = false;
            this._userPosition.OptionsColumn.ReadOnly = true;
            this._userPosition.Tag = "Должность#r";
            this._userPosition.Visible = true;
            this._userPosition.VisibleIndex = 2;
            // 
            // _userVisibility
            // 
            this._userVisibility.Caption = "В Группе";
            this._userVisibility.ColumnEdit = this._userSelectionCheckEdit;
            this._userVisibility.FieldName = "IsSelected";
            this._userVisibility.Name = "_userVisibility";
            this._userVisibility.Tag = "В группе#r";
            this._userVisibility.Visible = true;
            this._userVisibility.VisibleIndex = 3;
            // 
            // _userSelectionCheckEdit
            // 
            this._userSelectionCheckEdit.AutoHeight = false;
            this._userSelectionCheckEdit.Caption = "Check";
            this._userSelectionCheckEdit.Name = "_userSelectionCheckEdit";
            this._userSelectionCheckEdit.CheckedChanged += new System.EventHandler(this._userSelectionCheckEdit_CheckedChanged);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.Controls.Add(this._selectAllCheckBox);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 552);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(595, 23);
            this.flowLayoutPanel1.TabIndex = 4;
            // 
            // _selectAllCheckBox
            // 
            this._selectAllCheckBox.AutoSize = true;
            this._selectAllCheckBox.Location = new System.Drawing.Point(3, 3);
            this._selectAllCheckBox.Name = "_selectAllCheckBox";
            this._selectAllCheckBox.Size = new System.Drawing.Size(97, 17);
            this._selectAllCheckBox.TabIndex = 0;
            this._selectAllCheckBox.Tag = "Участники группы#r";
            this._selectAllCheckBox.Text = "Выделить всё";
            this._selectAllCheckBox.UseVisualStyleBackColor = true;
            this._selectAllCheckBox.CheckedChanged += new System.EventHandler(this._selectAllCheckBox_CheckedChanged);
            // 
            // UsersSelectionControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._usersGridControl);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Name = "UsersSelectionControl";
            this.Size = new System.Drawing.Size(595, 575);
            ((System.ComponentModel.ISupportInitialize)(this._usersGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._userView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._userSelectionCheckEdit)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl _usersGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView _userView;
        private DevExpress.XtraGrid.Columns.GridColumn _userName;
        private DevExpress.XtraGrid.Columns.GridColumn _userDepartments;
        private DevExpress.XtraGrid.Columns.GridColumn _userPosition;
        private DevExpress.XtraGrid.Columns.GridColumn _userVisibility;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit _userSelectionCheckEdit;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.CheckBox _selectAllCheckBox;
    }
}

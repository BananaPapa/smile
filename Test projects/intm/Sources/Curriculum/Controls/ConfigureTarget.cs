﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Curriculum.Controls
{
    public partial class ConfigureTarget : UserControl
    {
        private IEnumerable<int> _operations;// = new List<Operation>(); 
        private List<Operation> _allOperations; 
        public ConfigureTarget( IEnumerable<Operation> allOperations )
        {
            _allOperations = new List<Operation>(allOperations);
            _operationsGrid.DataSource = _allOperations;
            InitializeComponent( );
        }

        public IEnumerable<Operation> SelectedOperations
        {
            get
            {
                return _operations.Select(handle => _operationsGridView.GetRow(handle) as Operation);
            }
        }

        private void _selectOperationPopup_QueryDisplayText( object sender, DevExpress.XtraEditors.Controls.QueryDisplayTextEventArgs e )
        {
            e.DisplayText = "";
            var selectedHandels = e.EditValue as IEnumerable<int>;
            if (selectedHandels == null)
                return;
            e.DisplayText = string.Join(", ", selectedHandels.Select(_operationsGridView.GetRowHandle));
        }

        private void _selectOperationPopup_QueryPopUp( object sender, CancelEventArgs e )
        {
            _operationsGridView.ClearSelection();
            if(!_operations.Any())
                return;
            _operations.ForEach(handle => _operationsGridView.SelectRow(handle));
        }

        private void _selectOperationPopup_QueryResultValue( object sender, DevExpress.XtraEditors.Controls.QueryResultValueEventArgs e )
        {
            e.Value = _operations = _operationsGridView.GetSelectedRows( );
        }

        private void ConfigureTarget_Load( object sender, EventArgs e )
        {

        }
    }
}

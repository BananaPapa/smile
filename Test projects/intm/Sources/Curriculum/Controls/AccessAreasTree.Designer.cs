﻿namespace Curriculum.Controls
{
    partial class AccessAreasTreeControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if (disposing && ( components != null ))
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            this._accessAreasTreeContol = new Aga.Controls.Tree.TreeViewAdv();
            this._nameColumn = new Aga.Controls.Tree.TreeColumn();
            this._canReadColumn = new Aga.Controls.Tree.TreeColumn();
            this._canAddColumn = new Aga.Controls.Tree.TreeColumn();
            this._canEditColumn = new Aga.Controls.Tree.TreeColumn();
            this._canRemoveColumn = new Aga.Controls.Tree.TreeColumn();
            this._nameNodeTextBox = new Aga.Controls.Tree.NodeControls.NodeTextBox();
            this._canReadNodeCheckBox = new Aga.Controls.Tree.NodeControls.NodeCheckBox();
            this._canAddNodeCheckBox = new Aga.Controls.Tree.NodeControls.NodeCheckBox();
            this._canEditNodeCheckBox = new Aga.Controls.Tree.NodeControls.NodeCheckBox();
            this._canRemoveNodeCheckBox = new Aga.Controls.Tree.NodeControls.NodeCheckBox();
            this.SuspendLayout();
            // 
            // _accessAreasTreeContol
            // 
            this._accessAreasTreeContol.BackColor = System.Drawing.SystemColors.Window;
            this._accessAreasTreeContol.Columns.Add(this._nameColumn);
            this._accessAreasTreeContol.Columns.Add(this._canReadColumn);
            this._accessAreasTreeContol.Columns.Add(this._canAddColumn);
            this._accessAreasTreeContol.Columns.Add(this._canEditColumn);
            this._accessAreasTreeContol.Columns.Add(this._canRemoveColumn);
            this._accessAreasTreeContol.DefaultToolTipProvider = null;
            this._accessAreasTreeContol.Dock = System.Windows.Forms.DockStyle.Fill;
            this._accessAreasTreeContol.DragDropMarkColor = System.Drawing.Color.Black;
            this._accessAreasTreeContol.DrawPerformanceStat = false;
            this._accessAreasTreeContol.GridLineStyle = Aga.Controls.Tree.GridLineStyle.Horizontal;
            this._accessAreasTreeContol.HideSelection = true;
            this._accessAreasTreeContol.LineColor = System.Drawing.SystemColors.ControlDark;
            this._accessAreasTreeContol.LoadOnDemand = true;
            this._accessAreasTreeContol.Location = new System.Drawing.Point(0, 0);
            this._accessAreasTreeContol.Model = null;
            this._accessAreasTreeContol.Name = "_accessAreasTreeContol";
            this._accessAreasTreeContol.NodeControls.Add(this._nameNodeTextBox);
            this._accessAreasTreeContol.NodeControls.Add(this._canReadNodeCheckBox);
            this._accessAreasTreeContol.NodeControls.Add(this._canAddNodeCheckBox);
            this._accessAreasTreeContol.NodeControls.Add(this._canEditNodeCheckBox);
            this._accessAreasTreeContol.NodeControls.Add(this._canRemoveNodeCheckBox);
            this._accessAreasTreeContol.SelectedNode = null;
            this._accessAreasTreeContol.Size = new System.Drawing.Size(150, 150);
            this._accessAreasTreeContol.TabIndex = 0;
            this._accessAreasTreeContol.UseColumns = true;
            // 
            // _nameColumn
            // 
            this._nameColumn.Header = "Группа";
            this._nameColumn.SortOrder = System.Windows.Forms.SortOrder.None;
            this._nameColumn.TooltipText = "";
            this._nameColumn.Width = 250;
            // 
            // _canReadColumn
            // 
            this._canReadColumn.Header = "Чтение";
            this._canReadColumn.MaxColumnWidth = 70;
            this._canReadColumn.SortOrder = System.Windows.Forms.SortOrder.None;
            this._canReadColumn.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this._canReadColumn.TooltipText = null;
            this._canReadColumn.Width = 70;
            // 
            // _canAddColumn
            // 
            this._canAddColumn.Header = "Добавление";
            this._canAddColumn.MaxColumnWidth = 70;
            this._canAddColumn.SortOrder = System.Windows.Forms.SortOrder.None;
            this._canAddColumn.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this._canAddColumn.TooltipText = null;
            this._canAddColumn.Width = 70;
            // 
            // _canEditColumn
            // 
            this._canEditColumn.Header = "Изменение ";
            this._canEditColumn.MaxColumnWidth = 70;
            this._canEditColumn.SortOrder = System.Windows.Forms.SortOrder.None;
            this._canEditColumn.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this._canEditColumn.TooltipText = null;
            this._canEditColumn.Width = 70;
            // 
            // _canRemoveColumn
            // 
            this._canRemoveColumn.Header = "Удаление";
            this._canRemoveColumn.MaxColumnWidth = 70;
            this._canRemoveColumn.SortOrder = System.Windows.Forms.SortOrder.None;
            this._canRemoveColumn.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this._canRemoveColumn.TooltipText = null;
            this._canRemoveColumn.Width = 70;
            // 
            // _nameNodeTextBox
            // 
            this._nameNodeTextBox.DataPropertyName = "Name";
            this._nameNodeTextBox.IncrementalSearchEnabled = true;
            this._nameNodeTextBox.LeftMargin = 3;
            this._nameNodeTextBox.ParentColumn = this._nameColumn;
            // 
            // _canReadNodeCheckBox
            // 
            this._canReadNodeCheckBox.DataPropertyName = "IsAllowRead";
            this._canReadNodeCheckBox.EditEnabled = true;
            this._canReadNodeCheckBox.LeftMargin = 0;
            this._canReadNodeCheckBox.ParentColumn = this._canReadColumn;
            // 
            // _canAddNodeCheckBox
            // 
            this._canAddNodeCheckBox.DataPropertyName = "IsAllowAdd";
            this._canAddNodeCheckBox.EditEnabled = true;
            this._canAddNodeCheckBox.LeftMargin = 0;
            this._canAddNodeCheckBox.ParentColumn = this._canAddColumn;
            // 
            // _canEditNodeCheckBox
            // 
            this._canEditNodeCheckBox.DataPropertyName = "IsAllowEdit";
            this._canEditNodeCheckBox.EditEnabled = true;
            this._canEditNodeCheckBox.LeftMargin = 0;
            this._canEditNodeCheckBox.ParentColumn = this._canEditColumn;
            // 
            // _canRemoveNodeCheckBox
            // 
            this._canRemoveNodeCheckBox.DataPropertyName = "IsAllowRemove";
            this._canRemoveNodeCheckBox.EditEnabled = true;
            this._canRemoveNodeCheckBox.LeftMargin = 0;
            this._canRemoveNodeCheckBox.ParentColumn = this._canRemoveColumn;
            // 
            // AccessAreasTreeControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._accessAreasTreeContol);
            this.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.Name = "AccessAreasTreeControl";
            this.ResumeLayout(false);

        }

        #endregion

        private Aga.Controls.Tree.TreeViewAdv _accessAreasTreeContol;
        private Aga.Controls.Tree.TreeColumn _nameColumn;
        private Aga.Controls.Tree.TreeColumn _canReadColumn;
        private Aga.Controls.Tree.TreeColumn _canAddColumn;
        private Aga.Controls.Tree.TreeColumn _canEditColumn;
        private Aga.Controls.Tree.TreeColumn _canRemoveColumn;
        private Aga.Controls.Tree.NodeControls.NodeTextBox _nameNodeTextBox;
        private Aga.Controls.Tree.NodeControls.NodeCheckBox _canReadNodeCheckBox;
        private Aga.Controls.Tree.NodeControls.NodeCheckBox _canAddNodeCheckBox;
        private Aga.Controls.Tree.NodeControls.NodeCheckBox _canEditNodeCheckBox;
        private Aga.Controls.Tree.NodeControls.NodeCheckBox _canRemoveNodeCheckBox;
    }
}

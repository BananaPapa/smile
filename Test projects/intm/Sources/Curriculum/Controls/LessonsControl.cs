using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.Common;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Curriculum.Properties;
using Curriculum.Reports;
using DevExpress.Utils;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraScheduler;
using DevExpress.XtraScheduler.Drawing;
using Eureca.Integrator.Common;
using Eureca.Utility.Extensions;
using log4net;

namespace Curriculum
{
    public partial class LessonsControl : ControlWithAccessAreas
    {
        private static ILog Log = LogManager.GetLogger(typeof (LessonsControl));

        #region Nested type: LessonSummary

        /// <summary>
        /// Helper class for showing lesson summary.
        /// </summary>
        private class LessonSummary : INotifyPropertyChanged
        {
            #region Fields

            private Discipline _discipline;
            private bool _isImportant;
            private LessonType _lessonType;
            private Part _part;
            private int _totalDuration;
            private Unit _unit;

            #endregion

            #region Public properties

            public Unit Unit
            {
                get { return _unit; }
                set
                {
                    _unit = value;
                    OnPropertyChanged("Unit");
                }
            }

            public Part Part
            {
                get { return _part; }
                set
                {
                    _part = value;
                    OnPropertyChanged("Part");
                }
            }

            public Discipline Discipline
            {
                get { return _discipline; }
                set
                {
                    _discipline = value;
                    OnPropertyChanged("Discipline");
                }
            }

            public LessonType LessonType
            {
                get { return _lessonType; }
                set
                {
                    _lessonType = value;
                    OnPropertyChanged("LessonType");
                }
            }

            public int TotalDuration
            {
                get { return _totalDuration; }
                set
                {
                    _totalDuration = value;
                    OnPropertyChanged("TotalDuration");
                }
            }

            public bool IsImportant
            {
                get { return _isImportant; }
                set
                {
                    _isImportant = value;
                    OnPropertyChanged("IsImportant");
                }
            }

            #endregion

            #region Constructors

            public LessonSummary(Discipline discipline, LessonType lessonType, int totalDuration,
                Unit unit, Part part, bool isImportant)
            {
                _discipline = discipline;
                _lessonType = lessonType;
                _totalDuration = totalDuration;
                _unit = unit;
                _part = part;
                _isImportant = isImportant;
            }

            public LessonSummary(Discipline discipline, LessonType lessonType, int totalDuration) :
                this(discipline, lessonType, totalDuration, null, null, false)
            {
            }

            #endregion

            #region INotifyPropertyChanged Members

            public event PropertyChangedEventHandler PropertyChanged;

            #endregion

            public void OnPropertyChanged(string propertyName)
            {
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                }
            }
        }

        #endregion

        #region Fields

        private readonly ObservableCollection<LessonSummary> _courses = new ObservableCollection<LessonSummary>();
        private readonly ObservableCollection<LessonSummary> _disciplines = new ObservableCollection<LessonSummary>();

        private readonly Dictionary<LessonType, AppointmentLabel> _labels =
            new Dictionary<LessonType, AppointmentLabel>();

        private readonly ObservableCollection<LessonSummary> _lessons = new ObservableCollection<LessonSummary>();
        private readonly User _user = Program.CurrentUser;
        private Appointment _dragAppointment;
        private LessonSummary _dragLesson;
        private Rectangle _dragRect;
        private Person _person;
        private bool _readOnly;

        #endregion

        #region Public properties

        public DateTime CurrentDate
        {
            get { return dateNavigator1.DateTime; }
            set { _lessonsSchedulerControl.GoToDate(value); }
        }

        /// <summary>
        /// Gets or sets current person.
        /// </summary>
        public Person Person
        {
            get { return _person; }
            set
            {
                _person = value;
                RefreshAll();
            }
        }

        /// <summary>
        /// Gets or sets whether displayed data is read-only.
        /// </summary>
        [Category("Behavior")]
        public bool ReadOnly
        {
            get { return _readOnly; }
            set
            {
                _readOnly = value;

                if (_readOnly)
                {
                    splitContainerControl1.PanelVisibility = SplitPanelVisibility.Panel1;
                    _lessonsSchedulerControl.OptionsCustomization.AllowDisplayAppointmentForm =
                        AllowDisplayAppointmentForm.Never;
                    _lessonsSchedulerControl.OptionsCustomization.AllowAppointmentDelete = UsedAppointmentType.None;
                    _lessonsSchedulerControl.OptionsCustomization.AllowAppointmentDrag = UsedAppointmentType.None;
                    _lessonsSchedulerControl.OptionsCustomization.AllowAppointmentCopy = UsedAppointmentType.None;
                    _lessonsSchedulerControl.OptionsCustomization.AllowAppointmentCreate = UsedAppointmentType.None;
                    _lessonsSchedulerControl.ToolTipController = _toolTipController;
                    _lessonsSchedulerControl.OptionsView.ToolTipVisibility = ToolTipVisibility.Always;
                }
                else
                {
                    splitContainerControl1.PanelVisibility = SplitPanelVisibility.Both;
                    _lessonsSchedulerControl.OptionsCustomization.AllowDisplayAppointmentForm =
                        AllowDisplayAppointmentForm.Auto;
                    _lessonsSchedulerControl.OptionsCustomization.AllowAppointmentDelete = UsedAppointmentType.All;
                    _lessonsSchedulerControl.OptionsCustomization.AllowAppointmentDrag = UsedAppointmentType.All;
                    _lessonsSchedulerControl.OptionsCustomization.AllowAppointmentCopy = UsedAppointmentType.All;
                    _lessonsSchedulerControl.OptionsCustomization.AllowAppointmentCreate = UsedAppointmentType.All;
                    _lessonsSchedulerControl.ToolTipController = null;
                    _lessonsSchedulerControl.OptionsView.ToolTipVisibility = ToolTipVisibility.Never;
                }
            }
        }

        #endregion

        #region Events

        private Guid? SelectedTestGuid { get; set; }

        /// <summary>
        /// Lesson appointment click event.
        /// </summary>
        public event EventHandler<LessonClickEventArgs> LessonClick;

        /// <summary>
        /// Rise LessonClick event. 
        /// </summary>
        /// <param name="lesson">lesson</param>
        private void RaiseLessonClick(Lesson lesson)
        {
            Guid testGuid = Guid.Empty;
            if (lesson != null &&
                lesson.Discipline != null &&
                lesson.Discipline.TestGuid.HasValue)
            {
                //���� � ������� �������� ����
                testGuid = lesson.Discipline.TestGuid.Value;
            }

            Person person = Person;

            if (lesson != null && lesson.LessonType.IsTest && person.ProfessionalUserUniqueId.HasValue &&
                testGuid != Guid.Empty)
            {
                SelectedTestGuid = testGuid;

                _testReportBarButtonItem.Enabled = true;
            }
            else
            {
                SelectedTestGuid = null;

                _testReportBarButtonItem.Enabled = false;
            }

            if (LessonClick != null)
            {
                LessonClick(this, new LessonClickEventArgs(lesson));
            }
        }

        #endregion Events

        #region Constructors

        public LessonsControl()
        {
            InitializeComponent();

            if (DesignMode && Parent != null)
            {
                Parent.Refresh( );
            }
            _schedulerStorage.Appointments.CustomFieldMappings.Add( new AppointmentCustomFieldMapping( "Lesson", "Lesson" ) );
            _schedulerStorage.Appointments.CustomFieldMappings.Add( new AppointmentCustomFieldMapping( "CommentText", "CommentText" ) );
            _schedulerStorage.Appointments.CustomFieldMappings.Add( new AppointmentCustomFieldMapping( "IsImportant", "IsImportant" ) );
            _schedulerStorage.Appointments.CustomFieldMappings.Add( new AppointmentCustomFieldMapping( "Duration",
                "Duration" ) );
            _schedulerStorage.Appointments.CustomFieldMappings.Add( new AppointmentCustomFieldMapping( "TimeBeforeStart",
                "TimeBeforeStart" ) );

            _lessonsSchedulerControl.ActiveView.AppointmentDisplayOptions.SnapToCellsMode =
                AppointmentSnapToCellsMode.Disabled;
            _coursesGridControl.DataSource = _courses;
            _lessonsGridControl.DataSource = _lessons;
            _disciplinesGridControl.DataSource = _disciplines;
        }

        public LessonsControl(Person person)
            : this()
        {
            Person = person;
        }

        public LessonsControl(Guid? userId)
            : this()
        {
            Person = Program.DbSingletone.Persons
                .Where(i => i.ProfessionalUserUniqueId == userId)
                .SingleOrDefault();
        }

        #endregion

        #region Private static methods

        /// <summary>
        /// Creates new instance of lesson.
        /// </summary>
        /// <param name="discipline">discipline</param>
        /// <param name="lessonType">lesson type</param>
        /// <param name="person">person</param>
        /// <returns>new lesson</returns>
        private static Lesson CreateLesson(Discipline discipline, LessonType lessonType, Person person)
        {
            #region Debug.Assert()

            Debug.Assert(person != null, "Person must be not null.");
            Debug.Assert(lessonType != null, "LessonType must be not null.");
            Debug.Assert(discipline != null, "Discipline must be not null.");

            #endregion

            #region Checking input

            if (person == null)
            {
                throw new ArgumentNullException("Person must be not null");
            }
            if (lessonType == null)
            {
                throw new ArgumentNullException("LessonType must be not null");
            }
            if (discipline == null)
            {
                throw new ArgumentNullException("Discipline must be not null");
            }

            #endregion

            var lesson = new Lesson {Discipline = discipline, LessonType = lessonType, Duration = 1, Person = person};

            return lesson;
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Creates appointment for the lesson.
        /// </summary>
        /// <param name="lesson">lesson</param>
        /// <returns>new appointment</returns>
        private Appointment CreateLessonAppointment(Lesson lesson)
        {
            #region Debug.Assert()

            Debug.Assert(lesson != null, "Lesson must be not null.");
            Debug.Assert(lesson.LessonType != null, "LessonType must be not null.");
            Debug.Assert(lesson.Discipline != null, "Discipline must be not null.");

            #endregion

            #region Checking input

            if (lesson == null)
            {
                throw new ArgumentNullException("Lesson must be not null");
            }
            if (lesson.LessonType == null)
            {
                throw new ArgumentNullException("LessonType must be not null");
            }
            if (lesson.Discipline == null)
            {
                throw new ArgumentNullException("Discipline must be not null");
            }

            #endregion

            AppointmentLabel label = null;
            _labels.TryGetValue(lesson.LessonType, out label);


            if (label == null)
            {
                return null;
            }

            Appointment apt = _schedulerStorage.CreateAppointment(AppointmentType.Normal);
            apt.Subject = lesson.Discipline.Name;
            apt.LabelId = _schedulerStorage.Appointments.Labels.IndexOf(label);
            apt.Duration = TimeSpan.FromMinutes(Math.Abs(lesson.Duration));
            apt.Start = lesson.Datetime;
            apt.Description = lesson.Comment;
            apt.CustomFields["CommentText"] = lesson.Comment;
            apt.CustomFields["Lesson"] = lesson;
            apt.CustomFields["Duration"] = lesson.Duration;
            apt.CustomFields["Start"] = lesson.Datetime;

            bool isImportant = false;
            try
            {
                ImportantLesson il = Program.DbSingletone.ImportantLessons.Single(item => item.Lesson == lesson &&
                                                                                item.User == _user);
                isImportant = true;
                apt.CustomFields["TimeBeforeStart"] = (il.ReminderTime.HasValue)
                    ? (TimeSpan?) TimeSpan.FromSeconds(il.ReminderTime.Value)
                    : null;
            }
            catch (Exception ex)
            {
                //Log.Warn("Not important lesson");
                ///TODO: Handle Exception
            }
            apt.CustomFields["IsImportant"] = isImportant;
            AppointmentStatus status = _schedulerStorage.Appointments.Statuses[AppointmentStatusType.Free];
            if (isImportant)
            {
                status = _schedulerStorage.Appointments.Statuses[AppointmentStatusType.OutOfOffice];
            }
            if (status != AppointmentStatus.Empty)
            {
                apt.StatusId = _schedulerStorage.Appointments.Statuses.IndexOf(status);
            }

            return apt;
        }

        /// <summary>
        /// Creates appointment for the lesson.
        /// </summary>
        /// <param name="lesson">lesson summary</param>
        /// <returns>new appointment</returns>
        private Appointment CreateLessonAppointment(LessonSummary lesson)
        {
            #region Debug.Assert()

            Debug.Assert(lesson != null, "Lesson must be not null.");
            Debug.Assert(lesson.LessonType != null, "LessonType must be not null.");
            Debug.Assert(lesson.Discipline != null, "Discipline must be not null.");

            #endregion

            #region Checking input

            if (lesson == null)
            {
                throw new ArgumentNullException("Lesson must be not null");
            }
            if (lesson.LessonType == null)
            {
                throw new ArgumentNullException("LessonType must be not null");
            }
            if (lesson.Discipline == null)
            {
                throw new ArgumentNullException("Discipline must be not null");
            }

            #endregion

            AppointmentLabel label = null;
            _labels.TryGetValue(lesson.LessonType, out label);

            if (label == null)
            {
                return null;
            }

            Appointment apt = _schedulerStorage.CreateAppointment(AppointmentType.Normal);
            apt.Subject = lesson.Discipline.Name;
            apt.LabelId = _schedulerStorage.Appointments.Labels.IndexOf(label);
            apt.Duration = TimeSpan.FromMinutes(1);
            apt.CustomFields["IsImportant"] = lesson.IsImportant;
            AppointmentStatus status = _schedulerStorage.Appointments.Statuses[AppointmentStatusType.Free];
            if (lesson.IsImportant)
            {
                status = _schedulerStorage.Appointments.Statuses[AppointmentStatusType.OutOfOffice];
            }
            if (status != AppointmentStatus.Empty)
            {
                apt.StatusId = _schedulerStorage.Appointments.Statuses.IndexOf(status);
            }
            return apt;
        }

        private List<Appointment> CreateBirthdayAppointments(DateTime date)
        {
            var apts = new List<Appointment>();
            int year = Person.TeachingStartDate.HasValue ? Person.TeachingStartDate.Value.Year : DateTime.Today.Year;
            int examYear = Person.ExaminationDate.HasValue ? Person.ExaminationDate.Value.Year : year;
            int departureYear = Person.DepartureDate.HasValue ? Person.DepartureDate.Value.Year : year;
            int endYear = Math.Max(year, Math.Max(examYear, departureYear));

            do
            {
                DateTime birthDate = SchedulerTools.CheckDateTime(year, date.Month, date.Day);

                var sb = new StringBuilder();
                sb.Append("���� ��������");
                sb.Append(" (");
                sb.Append(year - Person.BirthDate.Year);
                sb.Append(")");
                apts.Add(
                    CreateKnownDateAppointment(
                        birthDate, sb.ToString(),
                        !Greeting.Exists(Person, birthDate)
                            ? SchedulerTools.BirthdayDateType
                            : SchedulerTools.BirthdayWithGreetingDateType));

                year++;
            } while (year <= endYear);
            return apts;
        }

        private Appointment CreateKnownDateAppointment(DateTime date, string subject, string dateType)
        {
            var apt = new Appointment(AppointmentType.Normal) {AllDay = true, Start = date};
            try
            {
                AppointmentLabel label =
                    (AppointmentLabel) _schedulerStorage.Appointments.Labels.Find(item => item.DisplayName == dateType);
                apt.LabelId = _schedulerStorage.Appointments.Labels.IndexOf(label);
            }
            catch (Exception)
            {
            }
            apt.Subject = subject;

            return apt;
        }

        /// <summary>
        /// Adds a important lesson for current lesson.
        /// </summary>
        /// <param name="lesson">lesson</param>
        private void AddImportantLesson(Lesson lesson, TimeSpan? timeBeforeStart)
        {
            #region Debug.Assert()

            Debug.Assert(lesson != null, "Lesson must be not null.");

            #endregion

            #region Checking input

            if (lesson == null)
            {
                throw new ArgumentNullException("Lesson must be not null");
            }

            #endregion

            var il = new ImportantLesson
            {
                Lesson = lesson,
                User = _user,
                ReminderTime =
                    (timeBeforeStart.HasValue) ? (int?) timeBeforeStart.Value.TotalSeconds : null
            };
            Program.DbSingletone.ImportantLessons.InsertOnSubmit(il);

            if (Program.StartForm != null)
            {
                Program.StartForm.CountImportantEvents(ImportantEvent.Lesson);
            }
        }

        /// <summary>
        /// Update important lesson for current lesson.
        /// </summary>
        /// <param name="lesson">lesson</param>
        private void UpdateImportantLesson(Lesson lesson, TimeSpan? timeBeforeStart)
        {
            #region Debug.Assert()

            Debug.Assert(lesson != null, "Lesson must be not null.");

            #endregion

            #region Checking input

            if (lesson == null)
            {
                throw new ArgumentNullException("Lesson must be not null");
            }

            #endregion

            ImportantLesson il = Program.DbSingletone.ImportantLessons.Where(i => i.LessonId == lesson.LessonId &&
                                                                        i.User == _user).SingleOrDefault();
            if (il == null)
            {
                return;
            }
            il.ReminderTime = (timeBeforeStart.HasValue) ? (int?) timeBeforeStart.Value.TotalSeconds : null;
        }

        /// <summary>
        /// Removes the important lesson for current lesson.
        /// </summary>
        /// <param name="lesson">lesson</param>
        private void RemoveImportantLesson(Lesson lesson)
        {
            #region Debug.Assert()

            Debug.Assert(lesson != null, "Lesson must be not null.");

            #endregion

            #region Checking input

            if (lesson == null)
            {
                throw new ArgumentNullException("Lesson must be not null");
            }

            #endregion

            try
            {
                ImportantLesson il = Program.DbSingletone.ImportantLessons.Single(item => item.Lesson == lesson &&
                                                                                item.User == _user);
                Program.DbSingletone.ImportantLessons.DeleteOnSubmit(il);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                ///TODO: Handle Exception
            }
        }

        /// <summary>
        /// Updates lesson summary.
        /// </summary>
        /// <param name="lesson">updating lesson</param>
        /// <param name="duration">lesson duration</param>
        private void UpdateLessonSummary(Lesson lesson, int duration)
        {
            LessonSummary ls = null;
            try
            {
                // looks for lesson summary
                ls = _lessons.Single(item => item.Discipline == lesson.Discipline &&
                                             item.LessonType == lesson.LessonType);

                // updates lesson summary
                ls.TotalDuration += duration;

                // checks total duration
                if (ls.TotalDuration <= 0)
                {
                    _lessons.Remove(ls);
                }
            }
            catch (InvalidOperationException)
            {
                // updated lesson has't been found
                return;
            }
            try
            {
                // changes course summary
                ls = _courses.Single(item => item.Discipline == lesson.Discipline &&
                                             item.LessonType == lesson.LessonType);
                ls.TotalDuration -= duration;
            }
            catch (InvalidOperationException)
            {
            }

            _lessonsGridView.RefreshData();
            _coursesGridView.RefreshData();
        }

        private void AddLessonSummary(Lesson lesson)
        {
            LessonSummary ls = null;
            try
            {
                ls = _lessons.Single(item => item.Discipline == lesson.Discipline &&
                                             item.LessonType == lesson.LessonType);
                ls.TotalDuration += lesson.Duration;
            }
            catch (InvalidOperationException)
            {
                ls = new LessonSummary(lesson.Discipline, lesson.LessonType, lesson.Duration);
                _lessons.Add(ls);
            }
            try
            {
                ls = _courses.Single(item => item.Discipline == lesson.Discipline &&
                                             item.LessonType == lesson.LessonType);
                ls.TotalDuration -= lesson.Duration;
            }
            catch (InvalidOperationException)
            {
            }

            _lessonsGridView.RefreshData();
            _coursesGridView.RefreshData();
        }

        private void RemoveLessonSummary(Lesson lesson)
        {
            LessonSummary ls;
            try
            {
                ls = _lessons.Single(item => item.Discipline == lesson.Discipline &&
                                             item.LessonType == lesson.LessonType);
                ls.TotalDuration -= lesson.Duration;
                if (ls.TotalDuration <= 0)
                {
                    _lessons.Remove(ls);
                }
                _lessonsGridView.RefreshData();
            }
            catch (InvalidOperationException)
            {
            }
            try
            {
                ls = _courses.Single(item => item.Discipline == lesson.Discipline &&
                                             item.LessonType == lesson.LessonType);
                ls.TotalDuration += lesson.Duration;
                _coursesGridView.RefreshData();
            }
            catch (InvalidOperationException)
            {
            }
        }

        private void FillLabels()
        {
            _labels.Clear();
            AppointmentLabel label;
            _schedulerStorage.Appointments.Labels.Clear();
            foreach (LessonType lessonType in Program.DbSingletone.LessonTypes)
            {
                label = new AppointmentLabel(lessonType.FillColor, lessonType.Name);
                _schedulerStorage.Appointments.Labels.Add(label);
                _labels.Add(lessonType, label);
            }

            _schedulerStorage.Appointments.Labels.AddRange
                (new[]
                {
                    new AppointmentLabel(Color.Tomato, SchedulerTools.BirthdayDateType),
                    new AppointmentLabel(Color.Snow, SchedulerTools.KnowndateDateType),
                    new AppointmentLabel(Color.Firebrick, SchedulerTools.HolidayDateType),
                    new AppointmentLabel(Color.LightSlateGray, SchedulerTools.BirthdayWithGreetingDateType)
                }
                );
        }

        private void RefreshLessons()
        {
            _lessons.Clear();

            if (Person == null)
            {
                return;
            }

            #region Current person lessons

            IQueryable<Discipline> disciplines = (from item in Program.DbSingletone.Lessons
                where item.Person == Person
                select item.Discipline).Distinct();

            foreach (Discipline d in disciplines)
            {
                var items = from item in Program.DbSingletone.Lessons
                    where item.Person == Person && item.Discipline == d
                    group item by item.LessonType
                    into grp
                    select new {LessonType = grp.Key, TotalDuration = grp.Sum(i => i.Duration)};
                foreach (var item in items)
                {
                    _lessons.Add(new LessonSummary(d, item.LessonType, item.TotalDuration));
                }
            }

            #endregion
        }

        private void RefreshCourses()
        {
            _courses.Clear();

            if (Person == null)
            {
                return;
            }

            #region Courses for current category of person

            IQueryable<Discipline> disciplines = (from item in Program.DbSingletone.Lessons
                where item.Person == Person
                select item.Discipline).Distinct();

            List<LessonSummary> lessons = (from d in disciplines
                let items = (from item in Program.DbSingletone.Lessons
                    where item.Person == Person && item.Discipline == d
                    group item by item.LessonType
                    into grp
                    select
                        new
                        {
                            LessonType = grp.Key,
                            TotalDuration = grp.Sum(i => i.Duration)
                        })
                from item in items
                select new LessonSummary(d, item.LessonType, item.TotalDuration)).ToList();

            IQueryable<LessonSummary> courses = from course in Program.DbSingletone.Courses
                where course.Category == Person.Category && course.Ship == Person.Ship
                select
                    new LessonSummary(course.Discipline, course.LessonType,
                        course.Duration,
                        course.Discipline.Unit, course.Discipline.Part,
                        false);

            var coursesList = new List<LessonSummary>(courses);

            foreach (var group in coursesList.GroupBy(ls => ls.Discipline).OrderBy(g => g.Key))
            {
                foreach (LessonSummary item in group.OrderBy(ls => ls.LessonType))
                {
                    int duration = 0;
                    try
                    {
                        LessonSummary lessonSummary = item;
                        LessonSummary ls = lessons.Single(i => i.Discipline == lessonSummary.Discipline &&
                                                               i.LessonType == lessonSummary.LessonType);
                        duration = ls.TotalDuration;
                    }
                    catch (InvalidOperationException)
                    {
                    }
                    _courses.Add(new LessonSummary(item.Discipline, item.LessonType,
                        item.TotalDuration - duration, item.Unit, item.Part, false));
                }
            }

            #endregion
        }

        private void RefreshDisciplines()
        {
            _disciplines.Clear();

            if (Person == null)
            {
                return;
            }

            #region All disciplines

            var disciplines = from discipline in Program.DbSingletone.Disciplines
                from lessonType in Program.DbSingletone.LessonTypes
                select new
                {
                    Discipline = discipline,
                    LessonType = lessonType,
                    discipline.Unit,
                    discipline.Part
                };
            foreach (var item in disciplines)
            {
                _disciplines.Add(new LessonSummary(item.Discipline, item.LessonType, 0, item.Unit, item.Part, false));
            }

            #endregion
        }

        private void RefreshAppointments()
        {
            _schedulerStorage.Appointments.Clear();

            SchedulerTools.SetTimeRange(_lessonsSchedulerControl);
            SchedulerTools.FillHolidays(_lessonsSchedulerControl);

            if (Person == null)
            {
                return;
            }

            // Adds lesson's appointments.
            IQueryable<Lesson> lessons = from lesson in Program.DbSingletone.Lessons
                where lesson.Person == Person
                select lesson;
            foreach (Lesson lesson in lessons)
            {
                _schedulerStorage.Appointments.Add(CreateLessonAppointment(lesson));
            }

            // Adds person's main dates.
            if (Person.BirthDate != null)
            {
                _schedulerStorage.Appointments.Items.AddRange(CreateBirthdayAppointments(Person.BirthDate));
            }
            if (Person.TeachingStartDate != null)
            {
                _schedulerStorage.Appointments.Add(CreateKnownDateAppointment((DateTime) Person.TeachingStartDate,
                    "������ ��������",
                    SchedulerTools.KnowndateDateType));
            }
            if (Person.ExaminationDate != null)
            {
                _schedulerStorage.Appointments.Add(CreateKnownDateAppointment((DateTime) Person.ExaminationDate,
                    "���� ��������",
                    SchedulerTools.KnowndateDateType));
            }
            if (Person.DepartureDate != null)
            {
                _schedulerStorage.Appointments.Add(CreateKnownDateAppointment((DateTime) Person.DepartureDate,
                    "���� ������",
                    SchedulerTools.KnowndateDateType));
            }
        }

        /// <summary>
        /// ������ ���������� �������� � ����������
        /// </summary>
        public void RefreshAll()
        {
            FillLabels();

            #region Sets the time interval

            _lessonsSchedulerControl.LimitInterval = new TimeInterval(DateTime.MinValue, DateTime.MaxValue);
            if (Person != null)
            {
                if (Person.TeachingStartDate.HasValue)
                {
                    _lessonsSchedulerControl.LimitInterval.Start = Person.TeachingStartDate.Value;
                }

                if (Person.DepartureDate.HasValue)
                {
                    _lessonsSchedulerControl.LimitInterval.End = Person.DepartureDate.Value + TimeSpan.FromDays(1);
                }
                else if (Person.ExaminationDate.HasValue)
                {
                    _lessonsSchedulerControl.LimitInterval.End = Person.ExaminationDate.Value + TimeSpan.FromDays(1);
                }
            }

            #endregion

            DateTime today = DateTime.Today;
            if (today < _lessonsSchedulerControl.LimitInterval.Start)
            {
                _lessonsSchedulerControl.GoToDate(_lessonsSchedulerControl.LimitInterval.Start);
            }
            else if (today > _lessonsSchedulerControl.LimitInterval.End)
            {
                _lessonsSchedulerControl.GoToDate(_lessonsSchedulerControl.LimitInterval.End);
            }
            else
            {
                _lessonsSchedulerControl.GoToToday();
            }

            RefreshLessons();
            RefreshCourses();
            RefreshDisciplines();
            RefreshAppointments();

            _coursesGridView.RefreshData();
            _lessonsGridView.RefreshData();
            _disciplinesGridView.RefreshData();
        }

        /// <summary>
        /// Handlers scheduler appointment deleting event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void SchedulerStorageAppointmentDeleting(object sender, PersistentObjectCancelEventArgs e)
        {
            if (ReadOnly)
            {
                e.Cancel = true;
                return;
            }

            var apt = e.Object as Appointment;
            if (apt != null)
            {
                var lesson = apt.CustomFields["Lesson"] as Lesson;
                if (lesson != null)
                {
                    RemoveLessonSummary(lesson);
                    RemoveImportantLesson(lesson);

                    Program.DbSingletone.Lessons.DeleteOnSubmit(lesson);

                    if (SubmitChanges() && Program.StartForm != null)
                    {
                        Program.StartForm.CountImportantEvents(ImportantEvent.Lesson);
                    }
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void ToolTipController_BeforeShow(object sender, ToolTipControllerShowEventArgs e)
        {
            var controller = sender as ToolTipController;
            if (controller == null) return;
            var aptViewInfo = controller.ActiveObject as AppointmentViewInfo;
            if (aptViewInfo == null) return;
            Appointment apt = aptViewInfo.Appointment;
            if (apt == null) return;
            e.IconType = ToolTipIconType.Information;
            var sb = new StringBuilder();
            if (!apt.AllDay)
            {
                sb.Append(apt.Start.ToShortTimeString());
                sb.Append(" - ");
                sb.Append(apt.End.ToShortTimeString());
                sb.Append(" (");

                var lesson = apt.CustomFields["Lesson"] as Lesson;
                if (lesson != null)
                {
                    sb.Append(lesson.LessonType.Name);
                }
                sb.Append(") ");
            }
            sb.Append(e.ToolTip);
            e.Title = sb.ToString();
            e.ToolTip = apt.Description;
        }

        private void RefreshBarButtonItemItemClick(object sender, ItemClickEventArgs e)
        {
            Program.DbSingletone.RefreshDataSource(typeof (LessonType), typeof (Schedule), typeof (Holiday),
                typeof (Person), typeof (Lesson), typeof (Discipline), typeof (Ship),
                typeof (Course));

            RefreshAll();
        }

        /// <summary>
        /// Commit changes in db.
        /// </summary>
        /// <returns>result</returns>
        private bool SubmitChanges()
        {
            try
            {
                Program.DbSingletone.SubmitChanges();
                return true;
            }
            catch (DbException ex)
            {
                Program.HandleDbException(ex);
            }
            catch (Exception)
            {
                var sb = new StringBuilder();
                sb.Append("��� ������� ��������� ��������� � ���� ������");
                sb.Append(Environment.NewLine);
                sb.Append("��������� ������. ��� ��������� ���� ��������.");
                sb.Append(Environment.NewLine);
                sb.Append("������������� ���������.");
                XtraMessageBox.Show(sb.ToString(), "����������� ������", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }

            return false;
        }

        private void BarButtonItemRemoveLessonItemClick(object sender, ItemClickEventArgs e)
        {
            if (_lessonsSchedulerControl.SelectedAppointments.Count > 0)
            {
                if (XtraMessageBox.Show("������� ��������� �������?", "�������������", MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question) == DialogResult.No)
                {
                    return;
                }

                List<Appointment> apts = _lessonsSchedulerControl.SelectedAppointments.ToList();

                foreach (Appointment apt in apts)
                {
                    _schedulerStorage.Appointments.Remove(apt);
                }
            }
        }

        private void LessonsSchedulerControlClick(object sender, EventArgs e)
        {
            //������ ������� ��������� ������� � ������
            if (_lessonsSchedulerControl.SelectedAppointments.Count > 0)
            {
                foreach (Appointment apt in _lessonsSchedulerControl.SelectedAppointments)
                {
                    var lesson = apt.CustomFields["Lesson"] as Lesson;
                    if (lesson == null)
                    {
                        continue;
                    }

                    RaiseLessonClick(lesson);
                }
            }
            else
            {
                RaiseLessonClick(null);
            }
        }

        #region Disciplines and courses grid control event handlers.

        private void GridControlMouseDown(object sender, MouseEventArgs e)
        {
            if (ReadOnly)
            {
                return;
            }

            if (e.Button == MouseButtons.Left && sender is GridControl)
            {
                var view = (sender as GridControl).MainView as GridView;
                if (view != null)
                {
                    _lessonsSchedulerControl.SelectedAppointments.Clear();
                    _dragAppointment = null;
                    GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
                    if (hitInfo.HitTest != GridHitTest.RowIndicator && hitInfo.RowHandle >= 0)
                    {
                        var ls = view.GetRow(hitInfo.RowHandle) as LessonSummary;
                        if (ls != null)
                        {
                            _dragLesson = ls;
                            _dragAppointment = CreateLessonAppointment(ls);

                            _dragRect = new Rectangle(new Point(e.X + SystemInformation.DragSize.Width,
                                e.Y + SystemInformation.DragSize.Height),
                                SystemInformation.DragSize);
                        }
                    }
                }
            }
        }

        private void GridControlMouseMove(object sender, MouseEventArgs e)
        {
            if (ReadOnly)
            {
                return;
            }

            if (_dragAppointment != null && sender is GridControl)
            {
                if (!_dragRect.Contains(new Point(e.X, e.Y)))
                {
                    var data = new SchedulerDragData(_dragAppointment);
                    _dragAppointment = null;
                    _coursesGridControl.DoDragDrop(data, DragDropEffects.All);
                }
            }
        }

        #endregion

        #region Scheduler control event handlers

        private void LessonsSchedulerControl_AppointmentDrag(object sender, AppointmentDragEventArgs e)
        {
            //Disables drag&drop if appointment.AllDay == true
            if (e.SourceAppointment != null && e.SourceAppointment.AllDay)
            {
                e.Allow = false;
                e.Handled = true;
            }
        }

        private void LessonsSchedulerControl_AllowAppointmentConflicts(object sender, AppointmentConflictEventArgs e)
        {
            var apts = new List<Appointment>();
            //Trace.WriteLine( "source " + e.Appointment.GetHashCode( ) );

            foreach (Appointment apt in e.Conflicts)
            {
                if (apt.AllDay)
                {
                    apts.Add(apt);
                }
                else
                {
                    var l1 = apt.CustomFields["Lesson"] as Lesson;
                    var l2 = e.AppointmentClone.CustomFields["Lesson"] as Lesson;
                    if (l1 != null && l2 != null && l1.Equals(l2))
                    {
                        apts.Add(apt);
                    }
                }
            }
            foreach (Appointment apt in apts)
            {
                //Trace.WriteLine( "delete " + apt.GetHashCode( ) );
                e.Conflicts.Remove(apt);
            }
        }

        private void LessonsSchedulerControl_AppointmentDrop(object sender, AppointmentDragEventArgs e)
        {
            // checks input
            if (e == null)
            {
                return;
            }
            if (e.SourceAppointment == null)
            {
                return;
            }
            if (e.EditedAppointment == null)
            {
                return;
            }

            DateTime srcTime = e.SourceAppointment.Start;
            DateTime newTime = e.EditedAppointment.Start;
            var srcDuration = (int) e.SourceAppointment.Duration.TotalMinutes;
            var newDuration = (int) e.EditedAppointment.Duration.TotalMinutes;
            var lesson = e.SourceAppointment.CustomFields["Lesson"] as Lesson;

            //Checks date interval
            if (!CheckLessonDuration(lesson, newTime, newDuration))
            {
                e.Allow = false;
                e.Handled = true;
                return;
            }

            //New lesson
            if (lesson == null)
            {
                //if (Person == null)
                //{
                //    throw new ArgumentNullException("Person must be not null.");
                //}

                if (_dragLesson.LessonType.IsTest &
                    Program.DbSingletone.Lessons.Where(i => i.LessonType.IsTest
                                                  & i.Discipline == _dragLesson.Discipline
                                                  & i.Person == Person).Any())
                {
                    XtraMessageBox.Show("� ������� ����� ��� ���������� ����� ����!", "��������������",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    e.Allow = false;
                    e.Handled = true;
                    return;
                }
                if (_dragLesson.LessonType.IsTest &
                    !_dragLesson.Discipline.TestGuid.HasValue)
                {
                    XtraMessageBox.Show("������� ���� �� ����������� ����!", "��������������",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    e.Allow = false;
                    e.Handled = true;
                    return;
                }

                lesson = CreateLesson(_dragLesson.Discipline, _dragLesson.LessonType, Person);
                lesson.Datetime = newTime;
                Program.DbSingletone.Lessons.InsertOnSubmit(lesson);

                if (SubmitChanges())
                {
                    e.EditedAppointment.CustomFields["Lesson"] = lesson;
                    AddLessonSummary(lesson);
                }

                _dragLesson = null;
            }
                //Edit lesson
            else
            {
                lesson.Datetime = newTime;
                SubmitChanges();
            }
        }

        private void LessonsSchedulerControl_EditAppointmentFormShowing(object sender, AppointmentFormEventArgs e)
        {
            e.Handled = true;

            Appointment apt = e.Appointment;
            var lesson = apt.CustomFields["Lesson"] as Lesson;
            if (lesson == null)
            {
                return;
            }

            var isImportantOld = (bool) apt.CustomFields["IsImportant"];
            var timeBeforeStartOld = (TimeSpan?) apt.CustomFields["TimeBeforeStart"];

            // creates and shows lesson edit form
            var frm = new LessonEditForm((SchedulerControl) sender, apt) {Icon = ParentForm.Icon,HierarchyParent = this};
            frm.LookAndFeel.ParentLookAndFeel = LookAndFeel.ParentLookAndFeel;
            e.DialogResult = frm.ShowDialog();
            if (e.DialogResult == DialogResult.OK)
            {
                try
                {
                    apt.BeginUpdate();
                    var duration = (int) apt.CustomFields["Duration"];
                    DateTime start = apt.Start;
                    var isImportant = (bool) apt.CustomFields["IsImportant"];
                    var timeBeforeStart = (TimeSpan?) apt.CustomFields["TimeBeforeStart"];

                    // 1. changes lesson comment
                    lesson.Comment = apt.CustomFields["CommentText"] as string;
                    apt.Description = lesson.Comment;

                    // 2. checks lesson new start time and duration value
                    if (CheckLessonDuration(lesson, start, duration))
                    {
                        UpdateLessonSummary(lesson, duration - lesson.Duration);

                        // changes lesson and appointment properties
                        lesson.Datetime = apt.Start;
                        lesson.Duration = duration;
                        apt.Duration = new TimeSpan(0, lesson.Duration, 0);
                    }
                    else
                    {
                        // reject changes
                        apt.CustomFields["Duration"] = lesson.Duration;
                        apt.Start = lesson.Datetime;
                    }

                    // 4. changes lesson type
                    try
                    {
                        AppointmentLabel label = _schedulerStorage.Appointments.Labels[apt.LabelId];
                        LessonType lessonType = _labels.Where(i => i.Value == label).First().Key;

                        if (lesson.LessonTypeId != lessonType.LessonTypeId)
                        {
                            // removes old lesson summary
                            RemoveLessonSummary(lesson);

                            // updates lesson
                            lesson.LessonType = lessonType;
                            SubmitChanges();

                            // adds new lesson summary
                            AddLessonSummary(lesson);
                        }
                    }
                    catch
                    {
                        AppointmentLabel label = null;
                        _labels.TryGetValue(lesson.LessonType, out label);
                        if (label != null)
                        {
                            apt.LabelId = _schedulerStorage.Appointments.Labels.IndexOf(label);
                        }
                    }

                    // 3. changes important lessons
                    if (isImportant != isImportantOld)
                    {
                        if (isImportant)
                        {
                            //Adds new important lesson
                            AddImportantLesson(lesson, timeBeforeStart);
                        }
                        else
                        {
                            //Removes the important lesson
                            RemoveImportantLesson(lesson);
                        }
                        AppointmentStatus status = _schedulerStorage.Appointments.Statuses[AppointmentStatusType.Free];
                        if (isImportant)
                        {
                            status = _schedulerStorage.Appointments.Statuses[AppointmentStatusType.OutOfOffice];
                        }
                        if (status != AppointmentStatus.Empty)
                        {
                            apt.StatusId = _schedulerStorage.Appointments.Statuses.IndexOf(status);
                        }
                    }
                    else if (timeBeforeStart != timeBeforeStartOld)
                    {
                        // update important lesson
                        UpdateImportantLesson(lesson, timeBeforeStart);
                    }

                    if (SubmitChanges() && Program.StartForm != null)
                    {
                        Program.StartForm.CountImportantEvents(ImportantEvent.Lesson);
                    }
                    apt.EndUpdate();
                }
                catch(Exception ex)
                {
                    Log.Error(e.ToString());
                    apt.CancelUpdate();
                    apt.EndUpdate();
                    //throw;
                }
            }

            // refreshes view
            _lessonsSchedulerControl.Refresh();
            _lessonsGridView.RefreshData();
            _coursesGridView.RefreshData();
        }

        /// <summary>
        /// Checks lesson new value.
        /// </summary>
        /// <param name="lesson">checking lesson or null</param>
        /// <param name="start">new value of lesson start</param>
        /// <param name="duration">new value of lesson duration</param>
        /// <returns>checking result</returns>
        private bool CheckLessonDuration(Lesson lesson, DateTime start, int duration)
        {
            if (lesson != null && lesson.Datetime == start && lesson.Duration == duration)
            {
                return false;
            }
            if (start < _lessonsSchedulerControl.LimitInterval.Start)
            {
                return false;
            }
            if (start.AddMinutes(duration) > _lessonsSchedulerControl.LimitInterval.End)
            {
                return false;
            }
            if (start.AddMinutes(duration).Date != start.Date)
            {
                return false;
            }

            foreach (Lesson l in Program.DbSingletone.Lessons.Where(i => i.PersonId == Person.PersonId))
            {
                if (lesson != null && lesson.LessonId == l.LessonId)
                {
                    continue;
                }
                if (start > l.Datetime && start < l.Datetime.AddMinutes(l.Duration))
                {
                    return false;
                }
                if (start.AddMinutes(duration) > l.Datetime && start < l.Datetime.AddMinutes(l.Duration))
                {
                    return false;
                }
                if (start < l.Datetime && start.AddMinutes(duration) > l.Datetime.AddMinutes(l.Duration))
                {
                    return false;
                }
            }

            return true;
        }

        private void LessonsSchedulerControl_PreparePopupMenu(object sender,
            DevExpress.XtraScheduler.PopupMenuShowingEventArgs e)
        {
            e.Menu = null;
        }

        private void LessonsSchedulerControl_CustomDrawTimeCell(object sender, CustomDrawObjectEventArgs e)
        {
            var viewInfo = e.ObjectInfo as SelectableIntervalViewInfo;

            var cell = e.ObjectInfo as SchedulerViewCellBase;

            DateTime start1 = _lessonsSchedulerControl.LimitInterval.Start;
            DateTime start2 = _lessonsSchedulerControl.LimitInterval.Start.AddDays(1);
            DateTime end2 = _lessonsSchedulerControl.LimitInterval.End;
            DateTime end1 = _lessonsSchedulerControl.LimitInterval.End.AddDays(-1);

            if ((viewInfo.Interval.Start >= start1 && viewInfo.Interval.End <= start2) ||
                viewInfo.Interval.Start >= end1 && viewInfo.Interval.End <= end2)
            {
                var brush = new HatchBrush(HatchStyle.LargeCheckerBoard,
                    Color.FromArgb(255, 255, 213), Color.LightGray);
                e.Cache.FillRectangle(brush, cell.Bounds);
                e.Cache.DrawRectangle(Pens.LightGray, cell.Bounds);
                e.Handled = true;
            }
            if (Person != null && viewInfo.Interval.Start.Month == Person.BirthDate.Month &&
                viewInfo.Interval.Start.Day == Person.BirthDate.Day)
            {
                e.Cache.FillRectangle(new SolidBrush(Color.FromArgb(255, 213, 213)), cell.Bounds);
                e.Cache.DrawRectangle(new Pen(new SolidBrush(Color.FromArgb(255, 184, 184)), 1), cell.Bounds);
                e.Handled = true;
            }
            if (viewInfo.Interval.Start < start1 || viewInfo.Interval.End > end2)
            {
                e.Cache.FillRectangle(Brushes.DarkGray, cell.Bounds);
                e.Handled = true;
            }
        }

        /// <summary>
        /// Handles [InitAppointmentImages] for appointments and adds image to test appointment. 
        /// </summary>
        private void LessonsSchedulerControl_InitAppointmentImages(object sender, AppointmentImagesEventArgs e)
        {
            Appointment apt = e.Appointment;
            var lesson = apt.CustomFields["Lesson"] as Lesson;
            if (lesson == null)
            {
                return;
            }
            var info = new AppointmentImageInfo();
            if (lesson.Discipline != null && !string.IsNullOrEmpty(lesson.Discipline.AttachmentsFolder))
            {
                info.Image = Resources.DocumentsFolder16;
                e.ImageInfoList.Add(info);
            }
            if (lesson.LessonType == null || !lesson.LessonType.IsTest)
            {
                return;
            }
            Person person = Person;
            if (person == null)
            {
                return;
            }

            info = new AppointmentImageInfo();
            if (person.ProfessionalUserUniqueId.HasValue && lesson.Discipline != null)
            {
                int testCompleted = Program.ProfessionalDb.GetTestStatus(lesson.Discipline.TestGuid,
                    person.ProfessionalUserUniqueId);
                if (testCompleted == 1)
                {
                    info.Image = Resources.Checked16;
                }
                else if (lesson.Datetime < DateTime.Now)
                {
                    info.Image = Resources.Critical16;
                }
                else if (lesson.Datetime >= DateTime.Now)
                {
                    info.Image = Resources.Todo16;
                }
            }
            else
            {
                info.Image = Resources.Todo16;
            }
            e.ImageInfoList.Add(info);
        }

        private void TestReportBarButtonItemItemClick(object sender, ItemClickEventArgs e)
        {
            Person person = Person;

            if (person.ProfessionalUserUniqueId.HasValue && SelectedTestGuid.HasValue)
            {
                var uc = new TestPassingControl(
                    SelectedTestGuid.Value,
                    person.ProfessionalUserUniqueId.Value);
                uc.ShowDialog();
            }
        }


        private static bool IsUsableInterval(TimeSpan span)
        {
            return span.TotalMinutes >= 1;
        }

        private bool CoursesLeft(int courseIndex)
        {
            return courseIndex < _courses.Count;
        }

        private bool ClearLessons(MarqueeProgressForm progressForm)
        {
            if (progressForm != null)
            {
                progressForm.StopAnimation();
            }

            if ((DialogResult)
                Invoke(new Func<DialogResult>(() =>
                    XtraMessageBox.Show(
                        this,
                        "��� ����� ����������� ������� ����� �������." +
                        Environment.NewLine +
                        "�� �������?",
                        "��������������",
                        MessageBoxButtons.YesNo,
                        MessageBoxIcon.Question,
                        MessageBoxDefaultButton.Button2))) !=
                DialogResult.Yes)
            {
                return false;
            }

            if (progressForm != null)
            {
                progressForm.StartAnimation();
                progressForm.Message = "�������� ����� ����������� �������...";
            }

            try
            {
                Program.DbSingletone.Lessons.DeleteAllOnSubmit(Person.Lessons);
                Program.DbSingletone.SubmitChanges();

                if (InvokeRequired)
                {
                    Invoke(new Action(RefreshAll));
                }
                else
                {
                    RefreshAll();
                }
            }
            catch (DbException ex)
            {
                Program.HandleDbException(ex);
                return true;
            }
            return true;
        }

        private void ClearLessons()
        {
            ClearLessons(null);
        }

        private void AutoAllocationSimpleButtonClick(object sender, EventArgs e)
        {
            if (_courses.Count == 0)
            {
                return;
            }

            new MarqueeProgressForm("�������������� ������������� �����...",
                progressForm =>
                {
                    if (!Person.TeachingStartDate.HasValue)
                    {
                        progressForm.StopAnimation();

                        Invoke(new Action(() => XtraMessageBox.Show(this,
                            "������� ���� ������ �������� � ���� \"��������\".",
                            "������",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Exclamation)));
                        return;
                    }

                    if (Person.Lessons.Count > 0)
                    {
                        if(!ClearLessons(progressForm))
                            return;
                    }

                    progressForm.Message = "������������� �����...";

                    DateTime date = Person.TeachingStartDate.Value;
                    Log.Info( "### ������ ����� {0}".FormatString(date.ToLongDateString()) );
                    int courseIndex = 0;
                    int courseHoursLeft = GetCourseDuration(courseIndex);

                    while (CoursesLeft(courseIndex))
                    {
                        if (Person.ExaminationDate.HasValue &&
                            date == Person.ExaminationDate.Value)
                        {
                            break;
                        }

                        Schedule schedule;

                        ExceptionSchedule exceptionSchedule =
                            ExceptionSchedule.GetException(date);

                        if (exceptionSchedule != null)
                        {
                            schedule = exceptionSchedule.Schedule;
                        }
                        else
                        {
                            Holiday holiday = Holiday.GetCurrentHoliday(date);

                            if (holiday != null)
                            {
                                date = holiday.Finish.HasValue
                                    ? SchedulerTools.IncrementDate(holiday.Finish.Value)
                                    : SchedulerTools.IncrementDate(holiday.Start);

                                continue;
                            }

                            schedule = DaySchedule.GetSchedule(date);
                        }

                        if (schedule.IsHoliday || !schedule.Start.HasValue ||
                            !schedule.Finish.HasValue)
                        {
                            date = SchedulerTools.IncrementDate(date);
                            continue;
                        }

                        TimeSpan startTime = schedule.Start.Value;

                        while (CoursesLeft(courseIndex) && startTime < schedule.Finish.Value &&
                               IsUsableInterval(schedule.Finish.Value - startTime))
                        {
                            TimeSpan finishTime;

                            if (schedule.BreakStart.HasValue && schedule.BreakFinish.HasValue &&
                                startTime <= schedule.BreakStart.Value)
                            {
                                if (IsUsableInterval(schedule.BreakStart.Value - startTime))
                                {
                                    finishTime = schedule.BreakStart.Value;
                                }
                                else
                                {
                                    startTime = schedule.BreakFinish.Value;
                                    finishTime = schedule.Finish.Value;
                                }
                            }
                            else
                            {
                                finishTime = schedule.Finish.Value;
                            }

                            int minutes = (int) Math.Min((finishTime - startTime).TotalMinutes, courseHoursLeft);

                            if (!_courses[courseIndex].LessonType.IsTest ||
                                GetCourseDuration(courseIndex) <= minutes)
                            {
                                Person.Lessons.Add(new Lesson
                                {
                                    Discipline =
                                        _courses[courseIndex].Discipline,
                                    LessonType =
                                        _courses[courseIndex].LessonType,
                                    Datetime = date + startTime,
                                    Duration = minutes
                                });

                                courseHoursLeft -= minutes;
                                if (courseHoursLeft <= 0)
                                {
                                    courseIndex++;
                                    courseHoursLeft = courseIndex < _courses.Count
                                        ? GetCourseDuration(courseIndex)
                                        : 0;
                                }
                            }

                            startTime = startTime + TimeSpan.FromMinutes(minutes);
                        }

                        date = SchedulerTools.IncrementDate(date);
                    }

                    progressForm.Message = "���������� ����������� � ���� ������...";

                    try
                    {
                        Program.DbSingletone.SubmitChanges();
                    }
                    catch (DbException ex)
                    {
                        Program.HandleDbException(ex);
                    }

                    progressForm.StopAnimation();
                    progressForm.Message = "������������� ���������.";

                    Thread.Sleep(3000);

                    if (CoursesLeft(courseIndex))
                    {
                        int totalHoursLeft = _courses.Aggregate(
                            courseHoursLeft,
                            (sum, course) =>
                            {
                                if (_courses.IndexOf(course) > courseIndex)
                                {
                                    sum += course.TotalDuration;
                                }

                                return sum;
                            });

                        Invoke(new Action(() => XtraMessageBox.Show(this,
                            "��� ������������� ����� ����� �� ������� " +
                            totalHoursLeft +
                            " ���. ����� ������ ������ �� �������� � ��������.",
                            "��������������",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Exclamation)));
                    }
                }
                ).ShowDialog(this);

            AutoSetExamDay();
            RefreshAll();
        }

        private void AutoSetExamDay()
        {
            if (!Person.ExaminationDate.HasValue)
            {
                // Get last lesson.
                Lesson lastLesson =
                    Person.Lessons.LastOrDefault();

                if (lastLesson != null)
                {
                    // Get last lesson date and day of week.
                    DateTime lastDate = lastLesson.Datetime.Date;
                    var dayOfWeek = (int) lastDate.DayOfWeek;

                    try
                    {
                        // Set examination date on the next Tuesday after last lesson date.
                        Person.ExaminationDate = lastDate + TimeSpan.FromDays((dayOfWeek >= 2 ? 9 : 2) - dayOfWeek);
                        Program.DbSingletone.SubmitChanges();
                    }
                    catch (DbException ex)
                    {
                        Program.HandleDbException(ex);
                    }
                }
            }
        }

        private int GetCourseDuration(int courseIndex)
        {
            return _courses[courseIndex].TotalDuration;
        }

        private void ClearLessonsSimpleButtonClick(object sender, EventArgs e)
        {
            ClearLessons();
        }

        #endregion

        #endregion

        public override IEnumerable<ISKOCBCTDNBTIDED> GetHiddenChilds(bool withLazy)
        {
            var hidden = new List<ISKOCBCTDNBTIDED>();
            if (withLazy)
            {
                var form = LessonEditForm.GetDummyForm();
                form.ManualFilter = true;
                form.HierarchyParent = this;
                hidden.Add(new ControlInterfaceImplementation(form));
            }
            return hidden;
        }
    }
}
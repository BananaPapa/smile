namespace Curriculum
{
    partial class CoursesControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this._coursesGridControl = new DevExpress.XtraGrid.GridControl();
            this._coursesBandedGridView = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.unitGridColumn = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.partGridColumn = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.disciplineGridColumn = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.lessonTypeGridColumn = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.lessonTypeRepositoryComboBox = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this._disciplinesGridControl = new DevExpress.XtraGrid.GridControl();
            this._disciplinesGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.disciplineUnitGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.disciplinePartGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.disciplineNameGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._barManager = new DevExpress.XtraBars.BarManager(this.components);
            this._editTemplateBar = new DevExpress.XtraBars.Bar();
            this._barButtonItemRefresh = new DevExpress.XtraBars.BarButtonItem();
            this._barStaticTemplate = new DevExpress.XtraBars.BarStaticItem();
            this._barComboEditTemplate = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this._barButtonCopy = new DevExpress.XtraBars.BarButtonItem();
            this._templateNameBarStaticItem = new DevExpress.XtraBars.BarStaticItem();
            this._barEditTemplateName = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this._barButtonCreateTemplate = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.repositoryItemComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._coursesGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._coursesBandedGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lessonTypeRepositoryComboBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._disciplinesGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._disciplinesGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 26);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this._coursesGridControl);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this._disciplinesGridControl);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(966, 527);
            this.splitContainerControl1.SplitterPosition = 367;
            this.splitContainerControl1.TabIndex = 0;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // _coursesGridControl
            // 
            this._coursesGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._coursesGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this._coursesGridControl.Location = new System.Drawing.Point(0, 0);
            this._coursesGridControl.MainView = this._coursesBandedGridView;
            this._coursesGridControl.Name = "_coursesGridControl";
            this._coursesGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.lessonTypeRepositoryComboBox});
            this._coursesGridControl.Size = new System.Drawing.Size(966, 367);
            this._coursesGridControl.TabIndex = 0;
            this._coursesGridControl.Tag = "������� �����#r";
            this._coursesGridControl.UseEmbeddedNavigator = true;
            this._coursesGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this._coursesBandedGridView});
            this._coursesGridControl.DragDrop += new System.Windows.Forms.DragEventHandler(this.CoursesGridControl_DragDrop);
            this._coursesGridControl.DragEnter += new System.Windows.Forms.DragEventHandler(this.CoursesGridControl_DragEnter);
            this._coursesGridControl.MouseDown += new System.Windows.Forms.MouseEventHandler(this.CoursesGridControl_MouseDown);
            this._coursesGridControl.MouseMove += new System.Windows.Forms.MouseEventHandler(this.CoursesGridControl_MouseMove);
            // 
            // _coursesBandedGridView
            // 
            this._coursesBandedGridView.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand1,
            this.gridBand2});
            this._coursesBandedGridView.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.unitGridColumn,
            this.partGridColumn,
            this.disciplineGridColumn,
            this.lessonTypeGridColumn});
            this._coursesBandedGridView.CustomizationFormBounds = new System.Drawing.Rectangle(1062, 695, 208, 173);
            this._coursesBandedGridView.GridControl = this._coursesGridControl;
            this._coursesBandedGridView.Name = "_coursesBandedGridView";
            this._coursesBandedGridView.OptionsDetail.EnableMasterViewMode = false;
            this._coursesBandedGridView.OptionsView.ShowFooter = true;
            this._coursesBandedGridView.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.CoursesBandedGridView_ValidatingEditor);
            // 
            // gridBand1
            // 
            this.gridBand1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand1.Caption = "����";
            this.gridBand1.Columns.Add(this.unitGridColumn);
            this.gridBand1.Columns.Add(this.partGridColumn);
            this.gridBand1.Columns.Add(this.disciplineGridColumn);
            this.gridBand1.Columns.Add(this.lessonTypeGridColumn);
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.VisibleIndex = 0;
            this.gridBand1.Width = 321;
            // 
            // unitGridColumn
            // 
            this.unitGridColumn.AppearanceHeader.Options.UseTextOptions = true;
            this.unitGridColumn.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.unitGridColumn.Caption = "������";
            this.unitGridColumn.FieldName = "Unit";
            this.unitGridColumn.Name = "unitGridColumn";
            this.unitGridColumn.OptionsColumn.AllowEdit = false;
            this.unitGridColumn.Tag = "������#r";
            this.unitGridColumn.Visible = true;
            // 
            // partGridColumn
            // 
            this.partGridColumn.AppearanceHeader.Options.UseTextOptions = true;
            this.partGridColumn.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.partGridColumn.Caption = "�����";
            this.partGridColumn.FieldName = "Part";
            this.partGridColumn.Name = "partGridColumn";
            this.partGridColumn.OptionsColumn.AllowEdit = false;
            this.partGridColumn.Tag = "�����#r";
            this.partGridColumn.Visible = true;
            this.partGridColumn.Width = 96;
            // 
            // disciplineGridColumn
            // 
            this.disciplineGridColumn.AppearanceHeader.Options.UseTextOptions = true;
            this.disciplineGridColumn.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.disciplineGridColumn.Caption = "����";
            this.disciplineGridColumn.FieldName = "Discipline";
            this.disciplineGridColumn.Name = "disciplineGridColumn";
            this.disciplineGridColumn.OptionsColumn.AllowEdit = false;
            this.disciplineGridColumn.Tag = "����#r";
            this.disciplineGridColumn.Visible = true;
            // 
            // lessonTypeGridColumn
            // 
            this.lessonTypeGridColumn.AppearanceHeader.Options.UseTextOptions = true;
            this.lessonTypeGridColumn.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lessonTypeGridColumn.Caption = "��� �������";
            this.lessonTypeGridColumn.ColumnEdit = this.lessonTypeRepositoryComboBox;
            this.lessonTypeGridColumn.FieldName = "LessonType";
            this.lessonTypeGridColumn.Name = "lessonTypeGridColumn";
            this.lessonTypeGridColumn.Tag = "��� �������#r";
            this.lessonTypeGridColumn.Visible = true;
            // 
            // lessonTypeRepositoryComboBox
            // 
            this.lessonTypeRepositoryComboBox.AutoHeight = false;
            this.lessonTypeRepositoryComboBox.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lessonTypeRepositoryComboBox.Name = "lessonTypeRepositoryComboBox";
            this.lessonTypeRepositoryComboBox.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // gridBand2
            // 
            this.gridBand2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand2.Caption = "���������";
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.VisibleIndex = 1;
            // 
            // _disciplinesGridControl
            // 
            this._disciplinesGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._disciplinesGridControl.Location = new System.Drawing.Point(0, 0);
            this._disciplinesGridControl.MainView = this._disciplinesGridView;
            this._disciplinesGridControl.Name = "_disciplinesGridControl";
            this._disciplinesGridControl.Size = new System.Drawing.Size(966, 154);
            this._disciplinesGridControl.TabIndex = 0;
            this._disciplinesGridControl.Tag = "������� �����#ra";
            this._disciplinesGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this._disciplinesGridView});
            this._disciplinesGridControl.DragDrop += new System.Windows.Forms.DragEventHandler(this.DisciplinesGridControl_DragDrop);
            this._disciplinesGridControl.DragEnter += new System.Windows.Forms.DragEventHandler(this.DisciplinesGridControl_DragEnter);
            this._disciplinesGridControl.MouseDown += new System.Windows.Forms.MouseEventHandler(this.DisciplinesGridControl_MouseDown);
            this._disciplinesGridControl.MouseMove += new System.Windows.Forms.MouseEventHandler(this.DisciplinesGridControl_MouseMove);
            // 
            // _disciplinesGridView
            // 
            this._disciplinesGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.disciplineUnitGridColumn,
            this.disciplinePartGridColumn,
            this.disciplineNameGridColumn});
            this._disciplinesGridView.GridControl = this._disciplinesGridControl;
            this._disciplinesGridView.GroupCount = 2;
            this._disciplinesGridView.Name = "_disciplinesGridView";
            this._disciplinesGridView.OptionsBehavior.Editable = false;
            this._disciplinesGridView.OptionsDetail.EnableMasterViewMode = false;
            this._disciplinesGridView.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.disciplineUnitGridColumn, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.disciplinePartGridColumn, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // disciplineUnitGridColumn
            // 
            this.disciplineUnitGridColumn.Caption = "������";
            this.disciplineUnitGridColumn.FieldName = "Unit";
            this.disciplineUnitGridColumn.Name = "disciplineUnitGridColumn";
            this.disciplineUnitGridColumn.Tag = "������#r";
            this.disciplineUnitGridColumn.Visible = true;
            this.disciplineUnitGridColumn.VisibleIndex = 0;
            // 
            // disciplinePartGridColumn
            // 
            this.disciplinePartGridColumn.Caption = "�����";
            this.disciplinePartGridColumn.FieldName = "Part";
            this.disciplinePartGridColumn.Name = "disciplinePartGridColumn";
            this.disciplinePartGridColumn.Tag = "�����#r";
            this.disciplinePartGridColumn.Visible = true;
            this.disciplinePartGridColumn.VisibleIndex = 0;
            // 
            // disciplineNameGridColumn
            // 
            this.disciplineNameGridColumn.AppearanceHeader.Options.UseTextOptions = true;
            this.disciplineNameGridColumn.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.disciplineNameGridColumn.Caption = "����";
            this.disciplineNameGridColumn.FieldName = "Name";
            this.disciplineNameGridColumn.Name = "disciplineNameGridColumn";
            this.disciplineNameGridColumn.Tag = "����#r";
            this.disciplineNameGridColumn.Visible = true;
            this.disciplineNameGridColumn.VisibleIndex = 0;
            // 
            // _barManager
            // 
            this._barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this._editTemplateBar});
            this._barManager.DockControls.Add(this.barDockControlTop);
            this._barManager.DockControls.Add(this.barDockControlBottom);
            this._barManager.DockControls.Add(this.barDockControlLeft);
            this._barManager.DockControls.Add(this.barDockControlRight);
            this._barManager.Form = this;
            this._barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this._barButtonItemRefresh,
            this._barComboEditTemplate,
            this._barStaticTemplate,
            this._barButtonCopy,
            this._barButtonCreateTemplate,
            this._barEditTemplateName,
            this._templateNameBarStaticItem});
            this._barManager.MaxItemId = 10;
            this._barManager.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox1,
            this.repositoryItemComboBox2,
            this.repositoryItemTextEdit1});
            // 
            // _editTemplateBar
            // 
            this._editTemplateBar.BarName = "Custom 1";
            this._editTemplateBar.DockCol = 0;
            this._editTemplateBar.DockRow = 0;
            this._editTemplateBar.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this._editTemplateBar.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this._barButtonItemRefresh),
            new DevExpress.XtraBars.LinkPersistInfo(this._barStaticTemplate, true),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this._barComboEditTemplate, "", false, true, true, 222),
            new DevExpress.XtraBars.LinkPersistInfo(this._barButtonCopy),
            new DevExpress.XtraBars.LinkPersistInfo(this._templateNameBarStaticItem, true),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this._barEditTemplateName, "", false, true, true, 177),
            new DevExpress.XtraBars.LinkPersistInfo(this._barButtonCreateTemplate)});
            this._editTemplateBar.OptionsBar.AllowQuickCustomization = false;
            this._editTemplateBar.OptionsBar.DrawDragBorder = false;
            this._editTemplateBar.Text = "Custom 1";
            // 
            // _barButtonItemRefresh
            // 
            this._barButtonItemRefresh.Caption = "��������";
            this._barButtonItemRefresh.Glyph = global::Curriculum.Properties.Resources.Refresh16;
            this._barButtonItemRefresh.Id = 2;
            this._barButtonItemRefresh.Name = "_barButtonItemRefresh";
            this._barButtonItemRefresh.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this._barButtonItemRefresh.Tag = "������� �����#r";
            this._barButtonItemRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this._barButtonItemRefresh_ItemClick);
            // 
            // _barStaticTemplate
            // 
            this._barStaticTemplate.Border = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this._barStaticTemplate.Caption = "������:";
            this._barStaticTemplate.Id = 4;
            this._barStaticTemplate.Name = "_barStaticTemplate";
            this._barStaticTemplate.Tag = "������� �����#rc";
            this._barStaticTemplate.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // _barComboEditTemplate
            // 
            this._barComboEditTemplate.Edit = this.repositoryItemComboBox1;
            this._barComboEditTemplate.Id = 3;
            this._barComboEditTemplate.Name = "_barComboEditTemplate";
            this._barComboEditTemplate.Tag = "������� �����#rc";
            this._barComboEditTemplate.EditValueChanged += new System.EventHandler(this.BarComboEditTemplate_EditValueChanged);
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            this.repositoryItemComboBox1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // _barButtonCopy
            // 
            this._barButtonCopy.Caption = "�������� �� �������";
            this._barButtonCopy.Enabled = false;
            this._barButtonCopy.Glyph = global::Curriculum.Properties.Resources.Paste16;
            this._barButtonCopy.Id = 5;
            this._barButtonCopy.Name = "_barButtonCopy";
            this._barButtonCopy.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem4.Text = "���������� ������ �� ������� �����";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "� ������� ���� (��� ������ �����) ����� ��������� ������� �� ���������� ������� �" +
    "����. ������ �� ���������� ����� � ����� ������� ����� ������������.";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this._barButtonCopy.SuperTip = superToolTip4;
            this._barButtonCopy.Tag = "������� �����#rc";
            this._barButtonCopy.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this._barButtonCopy_ItemClick);
            // 
            // _templateNameBarStaticItem
            // 
            this._templateNameBarStaticItem.Border = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this._templateNameBarStaticItem.Caption = "��������:";
            this._templateNameBarStaticItem.Id = 9;
            this._templateNameBarStaticItem.Name = "_templateNameBarStaticItem";
            this._templateNameBarStaticItem.Tag = "������� �����#ra";
            this._templateNameBarStaticItem.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // _barEditTemplateName
            // 
            this._barEditTemplateName.Caption = "barEditItem1";
            this._barEditTemplateName.Edit = this.repositoryItemTextEdit1;
            this._barEditTemplateName.Id = 8;
            this._barEditTemplateName.Name = "_barEditTemplateName";
            toolTipTitleItem6.Text = "��� �������";
            toolTipItem6.LeftIndent = 6;
            toolTipItem6.Text = "������� ���������� ��� ������� � ������� ������ \"������� ������\"";
            superToolTip6.Items.Add(toolTipTitleItem6);
            superToolTip6.Items.Add(toolTipItem6);
            this._barEditTemplateName.SuperTip = superToolTip6;
            this._barEditTemplateName.Tag = "������� �����#ra";
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // _barButtonCreateTemplate
            // 
            this._barButtonCreateTemplate.Caption = "������� ������";
            this._barButtonCreateTemplate.Glyph = global::Curriculum.Properties.Resources.Add16;
            this._barButtonCreateTemplate.Id = 7;
            this._barButtonCreateTemplate.Name = "_barButtonCreateTemplate";
            this._barButtonCreateTemplate.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem5.Text = "�������� ������� �� ������ �����";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "����� ������ ������ ����� � ��������� ���������, ���������� �������� ����� ������" +
    "��� ��������������� �������� ���������� �����.";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this._barButtonCreateTemplate.SuperTip = superToolTip5;
            this._barButtonCreateTemplate.Tag = "������� �����#ra";
            this._barButtonCreateTemplate.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BarButtonCreateTemplate_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(966, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 553);
            this.barDockControlBottom.Size = new System.Drawing.Size(966, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 527);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(966, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 527);
            // 
            // repositoryItemComboBox2
            // 
            this.repositoryItemComboBox2.AutoHeight = false;
            this.repositoryItemComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox2.Name = "repositoryItemComboBox2";
            // 
            // CoursesControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "CoursesControl";
            this.Size = new System.Drawing.Size(966, 553);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._coursesGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._coursesBandedGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lessonTypeRepositoryComboBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._disciplinesGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._disciplinesGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraGrid.GridControl _coursesGridControl;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView _coursesBandedGridView;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn unitGridColumn;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn partGridColumn;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn disciplineGridColumn;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn lessonTypeGridColumn;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox lessonTypeRepositoryComboBox;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.GridControl _disciplinesGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView _disciplinesGridView;
        private DevExpress.XtraGrid.Columns.GridColumn disciplineUnitGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn disciplinePartGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn disciplineNameGridColumn;
        private DevExpress.XtraBars.BarManager _barManager;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.Bar _editTemplateBar;
        private DevExpress.XtraBars.BarButtonItem _barButtonItemRefresh;
        private DevExpress.XtraBars.BarStaticItem _barStaticTemplate;
        private DevExpress.XtraBars.BarEditItem _barComboEditTemplate;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraBars.BarButtonItem _barButtonCopy;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox2;
        private DevExpress.XtraBars.BarButtonItem _barButtonCreateTemplate;
        private DevExpress.XtraBars.BarEditItem _barEditTemplateName;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraBars.BarStaticItem _templateNameBarStaticItem;

    }
}

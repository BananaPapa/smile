﻿namespace Curriculum.Controls
{
    partial class ConfigureTarget
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if (disposing && ( components != null ))
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this._popupContainerControl = new DevExpress.XtraEditors.PopupContainerControl();
            this._operationsGrid = new DevExpress.XtraGrid.GridControl();
            this._operationsGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._nameColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._selectOperationPopup = new DevExpress.XtraEditors.PopupContainerEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.deleteButton = new DevExpress.XtraEditors.SimpleButton();
            this.lowButton = new DevExpress.XtraEditors.SimpleButton();
            this._raiseButton = new DevExpress.XtraEditors.SimpleButton();
            this._usersComboBox = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this._popupContainerControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._operationsGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._operationsGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._selectOperationPopup.Properties)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(3, 3);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(65, 13);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "Получатель:";
            // 
            // _popupContainerControl
            // 
            this._popupContainerControl.AutoSize = true;
            this._popupContainerControl.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this._popupContainerControl.Location = new System.Drawing.Point(117, 19);
            this._popupContainerControl.Name = "_popupContainerControl";
            this._popupContainerControl.Size = new System.Drawing.Size(0, 0);
            this._popupContainerControl.TabIndex = 4;
            // 
            // _operationsGrid
            // 
            this._operationsGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._operationsGrid.Location = new System.Drawing.Point(0, 0);
            this._operationsGrid.MainView = this._operationsGridView;
            this._operationsGrid.Name = "_operationsGrid";
            this._operationsGrid.Size = new System.Drawing.Size(0, 0);
            this._operationsGrid.TabIndex = 0;
            this._operationsGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this._operationsGridView,
            this.gridView1});
            // 
            // _operationsGridView
            // 
            this._operationsGridView.GridControl = this._operationsGrid;
            this._operationsGridView.Name = "_operationsGridView";
            this._operationsGridView.OptionsView.ShowGroupPanel = false;
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this._operationsGrid;
            this.gridView1.Name = "gridView1";
            // 
            // _nameColumn
            // 
            this._nameColumn.Caption = "Операции";
            this._nameColumn.FieldName = "Name";
            this._nameColumn.Name = "_nameColumn";
            this._nameColumn.Visible = true;
            this._nameColumn.VisibleIndex = 0;
            // 
            // _selectOperationPopup
            // 
            this._selectOperationPopup.Dock = System.Windows.Forms.DockStyle.Fill;
            this._selectOperationPopup.Location = new System.Drawing.Point(274, 3);
            this._selectOperationPopup.Name = "_selectOperationPopup";
            this._selectOperationPopup.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._selectOperationPopup.Properties.PopupControl = this._popupContainerControl;
            this._selectOperationPopup.Size = new System.Drawing.Size(334, 20);
            this._selectOperationPopup.TabIndex = 2;
            this._selectOperationPopup.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this._selectOperationPopup_QueryResultValue);
            this._selectOperationPopup.QueryDisplayText += new DevExpress.XtraEditors.Controls.QueryDisplayTextEventHandler(this._selectOperationPopup_QueryDisplayText);
            this._selectOperationPopup.QueryPopUp += new System.ComponentModel.CancelEventHandler(this._selectOperationPopup_QueryPopUp);
            // 
            // labelControl2
            // 
            this.labelControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl2.Location = new System.Drawing.Point(215, 3);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(54, 13);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "Операции:";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.ColumnCount = 7;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 13.80672F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.94416F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.666533F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 55.58258F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 39F));
            this.tableLayoutPanel1.Controls.Add(this.deleteButton, 6, 0);
            this.tableLayoutPanel1.Controls.Add(this.lowButton, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.labelControl1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.labelControl2, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this._selectOperationPopup, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this._raiseButton, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this._usersComboBox, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.MinimumSize = new System.Drawing.Size(711, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(711, 29);
            this.tableLayoutPanel1.TabIndex = 5;
            // 
            // deleteButton
            // 
            this.deleteButton.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.deleteButton.Image = global::Curriculum.Properties.Resources.black_remove;
            this.deleteButton.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.deleteButton.Location = new System.Drawing.Point(674, 3);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(25, 23);
            this.deleteButton.TabIndex = 6;
            // 
            // lowButton
            // 
            this.lowButton.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.lowButton.Image = global::Curriculum.Properties.Resources.arrow_down;
            this.lowButton.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.lowButton.Location = new System.Drawing.Point(644, 3);
            this.lowButton.Name = "lowButton";
            this.lowButton.Size = new System.Drawing.Size(24, 23);
            this.lowButton.TabIndex = 5;
            // 
            // _raiseButton
            // 
            this._raiseButton.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this._raiseButton.Image = global::Curriculum.Properties.Resources.arrow_up;
            this._raiseButton.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this._raiseButton.Location = new System.Drawing.Point(614, 3);
            this._raiseButton.Name = "_raiseButton";
            this._raiseButton.Size = new System.Drawing.Size(24, 23);
            this._raiseButton.TabIndex = 4;
            // 
            // _usersComboBox
            // 
            this._usersComboBox.FormattingEnabled = true;
            this._usersComboBox.Location = new System.Drawing.Point(87, 3);
            this._usersComboBox.Name = "_usersComboBox";
            this._usersComboBox.Size = new System.Drawing.Size(121, 21);
            this._usersComboBox.TabIndex = 7;
            // 
            // ConfigureTarget
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this._popupContainerControl);
            this.MinimumSize = new System.Drawing.Size(711, 0);
            this.Name = "ConfigureTarget";
            this.Size = new System.Drawing.Size(711, 29);
            ((System.ComponentModel.ISupportInitialize)(this._popupContainerControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._operationsGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._operationsGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._selectOperationPopup.Properties)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.PopupContainerControl _popupContainerControl;
        private DevExpress.XtraGrid.GridControl _operationsGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView _operationsGridView;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn _nameColumn;
        private DevExpress.XtraEditors.PopupContainerEdit _selectOperationPopup;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton deleteButton;
        private DevExpress.XtraEditors.SimpleButton lowButton;
        private DevExpress.XtraEditors.SimpleButton _raiseButton;
        private System.Windows.Forms.ComboBox _usersComboBox;

    }
}

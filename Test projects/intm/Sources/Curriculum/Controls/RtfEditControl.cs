using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using Eureca.Integrator.Common;

namespace Curriculum
{
    /// <summary>
    /// Represents rich text edit control.
    /// </summary>
    public partial class RtfEditControl : XtraUserControl, INotifyPropertyChanged
    {
        /// <summary>
        /// Current control cursor.
        /// </summary>
        private Cursor _currentCursor;

        /// <summary>
        /// Whether rich text was modified.
        /// </summary>
        private bool _modified;

        /// <summary>
        /// Initializes new instance of rich text box edit control.
        /// </summary>
        public RtfEditControl()
        {
            InitializeComponent();

            ClearRichTextBox();
            Modified = false;
        }

        /// <summary>
        /// Gets whether rich text was modified.
        /// </summary>
        [Browsable(false)]
        public bool Modified
        {
            get { return _modified; }
            protected set
            {
                if (value != _modified)
                {
                    _modified = value;

                    if (!_modified)
                    {
                        _undoBarButtonItem.Enabled = false;
                    }
                    OnPropertyChanged("Modified");
                }
            }
        }

        /// <summary>
        /// Gets or sets whether the rich text is read-only.
        /// </summary>
        [Browsable(true)]
        [Category("Behaviour")]
        public bool ReadOnly
        {
            get { return _richTextBox.ReadOnly; }
            set { _richTextBox.ReadOnly = value; }
        }

        /// <summary>
        /// Gets or sets the rich text of the control.
        /// </summary>
        public string Rtf
        {
            get { return _richTextBox.Rtf; }
            set { _richTextBox.Rtf = value; }
        }

        /// <summary>
        /// Gets selection font.
        /// </summary>
        protected Font SelectionFont
        {
            get { return _richTextBox.SelectionFont ?? _richTextBox.Font; }
        }

        /// <summary>
        /// Gets rich text box font style.
        /// </summary>
        private FontStyle RichTextBoxFontStyle
        {
            get
            {
                var fs = new FontStyle();
                if (_boldBarButtonItem.Down)
                    fs |= FontStyle.Bold;
                if (_italicBarButtonItem.Down)
                    fs |= FontStyle.Italic;
                if (_underlineBarButtonItem.Down)
                    fs |= FontStyle.Underline;
                return fs;
            }
        }

        #region INotifyPropertyChanged Members

        /// <summary>
        /// Occurs when 
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        /// <summary>
        /// Resets modified property and clears undo list.
        /// </summary>
        /// <remarks>Usually after saving contents of RTF control.</remarks>
        public void ResetModifiedProperty()
        {
            _richTextBox.ClearUndo();
            Modified = false;
        }

        /// <summary>
        /// Handles bar manager item click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void BarManager_ItemClick(object sender, ItemClickEventArgs e)
        {
            _undoBarButtonItem.Enabled = Modified;
        }

        /// <summary>
        /// Handles bullets bar button item click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void BulletsBarButtonItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            _richTextBox.SelectionBullet = _bulletsBarButtonItem.Down;
        }

        /// <summary>
        /// Initializes format toolbar.
        /// </summary>
        protected void InitFormat()
        {
            _boldBarButtonItem.Down = SelectionFont.Bold;
            _italicBarButtonItem.Down = SelectionFont.Italic;
            _underlineBarButtonItem.Down = SelectionFont.Underline;
            _bulletsBarButtonItem.Down = _richTextBox.SelectionBullet;

            switch (_richTextBox.SelectionAlignment)
            {
                case HorizontalAlignment.Left:
                    _alignLeftBarButtonItem.Down = true;
                    break;
                case HorizontalAlignment.Center:
                    _centerBarButtonItem.Down = true;
                    break;
                case HorizontalAlignment.Right:
                    _alignRightBarButtonItem.Down = true;
                    break;
            }
        }

        /// <summary>
        /// Initializes paste button.
        /// </summary>
        protected void InitPaste()
        {
            _pasteBarButtonItem.Enabled = _richTextBox.CanPaste(DataFormats.GetFormat(0));
        }

        /// <summary>
        /// Initializes cut, copy and undo buttons.
        /// </summary>
        /// <param name="b">Wether to enable copy and cut.</param>
        protected void InitEdit(bool b)
        {
            _cutBarButtonItem.Enabled = b;
            _copyBarButtonItem.Enabled = b;
            _undoBarButtonItem.Enabled = _richTextBox.CanUndo;
        }

        /// <summary>
        /// Handles rich text box selection changed event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void RichTextBox_SelectionChanged(object sender, EventArgs e)
        {
            InitFormat();
            InitEdit(_richTextBox.SelectionLength > 0);
        }

        /// <summary>
        /// Handles font bar button item click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void FontStyleBarButtonItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            _richTextBox.SelectionFont = new Font(SelectionFont.FontFamily.Name, SelectionFont.Size,
                                                  RichTextBoxFontStyle);
        }

        /// <summary>
        /// Handles align bar button item click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void AlignBarButtonItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (_alignLeftBarButtonItem.Down)
                _richTextBox.SelectionAlignment = HorizontalAlignment.Left;
            if (_centerBarButtonItem.Down)
                _richTextBox.SelectionAlignment = HorizontalAlignment.Center;
            if (_alignRightBarButtonItem.Down)
                _richTextBox.SelectionAlignment = HorizontalAlignment.Right;
        }

        /// <summary>
        /// Clears rich text box.
        /// </summary>
        protected void ClearRichTextBox()
        {
            RefreshControl(true);
            _richTextBox.SelectionBullet = false;
            _richTextBox.SelectionProtected = false;
            _richTextBox.Clear();
            _richTextBox.ClearUndo();
            _undoBarButtonItem.Enabled = false;
            _richTextBox.Focus();
            RichTextBox_SelectionChanged(null, null);
            RefreshControl(false);
        }

        /// <summary>
        /// Refreshes control.
        /// </summary>
        /// <param name="b">True to refresh; false to finish refreshing.</param>
        protected void RefreshControl(bool b)
        {
            if (b)
            {
                _currentCursor = Cursor.Current;
                Cursor.Current = Cursors.WaitCursor;
                Refresh();
            }
            else
            {
                Cursor.Current = _currentCursor;
            }
        }

        /// <summary>
        /// Handles rich text box text changed event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void RichTextBox_TextChanged(object sender, EventArgs e)
        {
            Modified = _richTextBox.CanUndo;
        }

        /// <summary>
        /// Handles undo bar button item click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void UndoBarButtonItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            _richTextBox.Undo();

            Modified = _undoBarButtonItem.Enabled = _richTextBox.CanUndo;

            InitFormat();
        }

        /// <summary>
        /// Handles cut bar button item click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void CutBarButtonItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            _richTextBox.Cut();

            InitPaste();
        }

        /// <summary>
        /// Handles copy bar button item click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void CopyBarButtonItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            _richTextBox.Copy();

            InitPaste();
        }

        /// <summary>
        /// Handles paste bar button item click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void PasteBarButtonItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            _richTextBox.Paste();
        }

        /// <summary>
        /// Handles font bar button item click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void FontBarButtonItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            var dlg = new FontDialog
                          {
                              Font = ((Font) SelectionFont.Clone()),
                              ShowColor = true,
                              Color = _richTextBox.SelectionColor
                          };

            if (dlg.ShowDialog() == DialogResult.OK)
            {
                _richTextBox.SelectionFont = (Font) dlg.Font.Clone();
                _richTextBox.SelectionColor = dlg.Color;
            }
        }

        /// <summary>
        /// Handles RTF edit control load event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void RtfEditControl_Load(object sender, EventArgs e)
        {
            InitPaste();
        }

        /// <summary>
        /// Raises property changed event.
        /// </summary>
        /// <param name="propertyName">Name of property.</param>
        private void OnPropertyChanged(string propertyName)
        {
            Type t = typeof (RtfEditControl);
            if (t.GetProperty(propertyName) != null)
            {
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                }
            }
            else
            {
                Debug.Fail(string.Format("� ������ {0} ����������� �������� �������� {1}",
                                         t, propertyName));
            }
        }
    }
}
﻿namespace Curriculum.Controls
{
    partial class ChatControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if (disposing && ( components != null ))
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            this._closeDialogBtn = new DevExpress.XtraEditors.SimpleButton();
            this._chatRTB = new System.Windows.Forms.RichTextBox();
            this._messageInboxFormMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this._sendMessageBtn = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this._messageInboxFormMemoEdit.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // _closeDialogBtn
            // 
            this._closeDialogBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._closeDialogBtn.Location = new System.Drawing.Point(430, 14);
            this._closeDialogBtn.Name = "_closeDialogBtn";
            this._closeDialogBtn.Size = new System.Drawing.Size(106, 23);
            this._closeDialogBtn.TabIndex = 3;
            this._closeDialogBtn.Text = "Закончить диалог";
            this._closeDialogBtn.Click += new System.EventHandler(this._closeDialogBtn_Click);
            // 
            // _chatRTB
            // 
            this._chatRTB.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._chatRTB.Location = new System.Drawing.Point(23, 49);
            this._chatRTB.Name = "_chatRTB";
            this._chatRTB.ReadOnly = true;
            this._chatRTB.Size = new System.Drawing.Size(513, 625);
            this._chatRTB.TabIndex = 4;
            this._chatRTB.Text = "";
            // 
            // _messageInboxFormMemoEdit
            // 
            this._messageInboxFormMemoEdit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._messageInboxFormMemoEdit.Location = new System.Drawing.Point(23, 693);
            this._messageInboxFormMemoEdit.Name = "_messageInboxFormMemoEdit";
            this._messageInboxFormMemoEdit.Size = new System.Drawing.Size(444, 54);
            this._messageInboxFormMemoEdit.TabIndex = 0;
            this._messageInboxFormMemoEdit.UseOptimizedRendering = true;
            this._messageInboxFormMemoEdit.KeyDown += new System.Windows.Forms.KeyEventHandler(this._messageInboxFormMemoEdit_KeyDown);
            // 
            // _sendMessageBtn
            // 
            this._sendMessageBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._sendMessageBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._sendMessageBtn.Image = global::Curriculum.Properties.Resources.mail_send;
            this._sendMessageBtn.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this._sendMessageBtn.Location = new System.Drawing.Point(488, 693);
            this._sendMessageBtn.Margin = new System.Windows.Forms.Padding(10);
            this._sendMessageBtn.Name = "_sendMessageBtn";
            this._sendMessageBtn.Padding = new System.Windows.Forms.Padding(10);
            this._sendMessageBtn.Size = new System.Drawing.Size(48, 54);
            this._sendMessageBtn.TabIndex = 2;
            this._sendMessageBtn.Click += new System.EventHandler(this._sendButton_Click);
            // 
            // ChatControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._messageInboxFormMemoEdit);
            this.Controls.Add(this._chatRTB);
            this.Controls.Add(this._closeDialogBtn);
            this.Controls.Add(this._sendMessageBtn);
            this.MinimumSize = new System.Drawing.Size(300, 400);
            this.Name = "ChatControl";
            this.Size = new System.Drawing.Size(564, 768);
            ((System.ComponentModel.ISupportInitialize)(this._messageInboxFormMemoEdit.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton _sendMessageBtn;
        private DevExpress.XtraEditors.SimpleButton _closeDialogBtn;
        private System.Windows.Forms.RichTextBox _chatRTB;
        private DevExpress.XtraEditors.MemoEdit _messageInboxFormMemoEdit;
    }
}

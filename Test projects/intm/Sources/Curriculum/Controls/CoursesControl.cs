using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Views.BandedGrid;
using DevExpress.XtraGrid.Views.BandedGrid.ViewInfo;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using Eureca.Integrator.Common;

namespace Curriculum
{
    public partial class CoursesControl : XtraUserControl , IHasHiddenChildren
    {
        #region Fields

        /// <summary>
        /// The temp table that contains information about course duration for each category of persons 
        /// </summary>
        private readonly DataTable _courses = new DataTable();

        /// <summary>
        /// Data manager for working with database.
        /// </summary>
        private CoursesDataManager _coursesDataManager;

        private Discipline _dragDiscipline;
        private Rectangle _dragRect;
        private bool _editable;
        private BandedGridHitInfo _removeCourseInfo;

        #endregion

        #region Public properties

        /// <summary>
        /// Gets and sets current ship.
        /// </summary>
        [Browsable(false)]
        public Ship Ship
        {
            get { return (_coursesDataManager == null) ? null : _coursesDataManager.Ship; }
            set
            {
                if (_coursesDataManager != null && _coursesDataManager.Ship == value)
                {
                    return;
                }
                _coursesDataManager = (value == null) ? null : new CoursesDataManager(value);
                RefreshCoursesView();
            }
        }

        /// <summary>
        /// Gets and sets current ship.
        /// </summary>
        [Browsable(false)]
        public Template Template
        {
            get { return (_coursesDataManager == null) ? null : _coursesDataManager.Template; }
            set
            {
                if (_coursesDataManager != null && _coursesDataManager.Template == value)
                {
                    return;
                }
                _coursesDataManager = (value == null) ? null : new CoursesDataManager(value);
                RefreshCoursesView();
            }
        }

        /// <summary>
        /// Gets or sets whether displayed data is read-only.
        /// </summary>
        [Category("Behavior")]
        public bool Editable
        {
            get { return _editable; }
            set
            {
                _editable = value;
                // Sets visibility of template controls
                _barComboEditTemplate.Visibility = _barButtonCopy.Visibility = _barStaticTemplate.Visibility =
                                                                               (!_editable)
                                                                                   ? BarItemVisibility.Never
                                                                                   : BarItemVisibility.Always;

                if (!_editable)
                {
                    splitContainerControl1.PanelVisibility = SplitPanelVisibility.Panel1;
                    _coursesGridControl.AllowDrop = false;
                    _coursesBandedGridView.OptionsBehavior.Editable = false;
                }
                else
                {
                    splitContainerControl1.PanelVisibility = SplitPanelVisibility.Both;
                    _coursesGridControl.AllowDrop = true;
                    _coursesBandedGridView.OptionsBehavior.Editable = true;
                }
            }
        }

        #endregion

        #region Constructors

        public CoursesControl()
        {
            InitializeComponent();
            if (LicenseManager.UsageMode == LicenseUsageMode.Designtime)
                return;
            NameOwnerTools.FillComboBox<LessonType>( lessonTypeRepositoryComboBox );

            _courses.Columns.Add( new DataColumn( "Unit", typeof( Unit ) ) );
            _courses.Columns.Add( new DataColumn( "Part", typeof( Part ) ) );
            _courses.Columns.Add( new DataColumn( "Discipline", typeof( Discipline ) ) );
            _courses.Columns.Add( new DataColumn( "LessonType", typeof( LessonType ) ) );

            const int index = 0;
            foreach (Category category in Program.DbSingletone.Categories)
            {
                var column = new BandedGridColumn
                {
                    Caption = category.Name,
                    Tag = category.CategoryId,
                    VisibleIndex = index,
                    FieldName = category.CategoryId.ToString( ),
                    ColumnEdit = new RepositoryItemSpinEdit
                    {
                        MinValue = 0,
                        MaxValue = Int32.MaxValue,
                        IsFloatValue = false,
                        Mask =
                        {
                            EditMask = "N0"
                        }
                    },
                    AppearanceHeader =
                    {
                        TextOptions = { HAlignment = HorzAlignment.Center }
                    },
                    SummaryItem =
                    {
                        FieldName = category.CategoryId.ToString( ),
                        SummaryType = SummaryItemType.Sum
                    }
                };

                _courses.Columns.Add( new DataColumn( category.CategoryId.ToString( ), typeof( int ) ) );

                _coursesBandedGridView.Bands[1].Columns.Add( column );
            }

            this.Invalidated += CoursesControl_Invalidated;
        }

        void CoursesControl_Invalidated( object sender, InvalidateEventArgs e )
        {
            FilterInterface();
        }


        public CoursesControl(Ship ship)
            : this()
        {
            Ship = ship;
        }

        public CoursesControl(Template template)
            : this()
        {
            Template = template;
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Refreshes all data source views.
        /// </summary>
        public void RefreshCoursesView()
        {
            _coursesGridControl.DataSource = null;
            _coursesGridControl.AllowDrop = false;
            _disciplinesGridControl.AllowDrop = false;

            if (_coursesDataManager == null)
            {
                return;
            }

            _coursesGridControl.AllowDrop = Editable;
            _disciplinesGridControl.AllowDrop = Editable;

            var courses = from course in _coursesDataManager.GetCourses()
                          select
                              new {course.Discipline, course.Discipline.Part, course.Discipline.Unit, course.LessonType};
            courses = courses.Distinct();

            _courses.Clear();
            foreach (var course in courses)
            {
                DataRow row = _courses.NewRow();
                row["Unit"] = course.Discipline.Unit;
                row["Part"] = course.Discipline.Part;
                row["Discipline"] = course.Discipline;
                row["LessonType"] = course.LessonType;

                IQueryable<ICourse> courseDurations = _coursesDataManager.GetCourses().
                    Where(item => item.Discipline == course.Discipline && item.LessonType == course.LessonType).
                    Select(item => item);
                foreach (ICourse c in courseDurations)
                {
                    row[c.Category.CategoryId.ToString()] = c.Duration;
                }

                _courses.Rows.Add(row);
            }

            _coursesGridControl.BeginUpdate();
            int handle = _coursesBandedGridView.FocusedRowHandle;
            _coursesGridControl.DataSource = _courses;
            try
            {
                _coursesBandedGridView.FocusedRowHandle = handle;
            }
            catch
            {
            }
            _coursesGridControl.EndUpdate();

            IQueryable<Discipline> disciplines = from d in Program.DbSingletone.Disciplines
                                                 select d;
            _disciplinesGridControl.DataSource = disciplines;

            // Fills templete combobox
            IOrderedQueryable<Template> templates = _coursesDataManager.GetTemplates().OrderBy(item => item.Name);
            if (Template != null)
            {
                templates = templates.Where(i => i.TemplateId != Template.TemplateId)
                    .Select(i => i)
                    .OrderBy(i => i.Name);
            }
            repositoryItemComboBox1.Items.Clear();
            repositoryItemComboBox1.Items.AddRange(templates.ToArray());

            FilterInterface();
        }

        private void FilterInterface()
        {
            if(_coursesDataManager==null)
                return;
            var allowAdd = _coursesDataManager.Ship != null && Editable;
            _templateNameBarStaticItem.Visibility =
                _barButtonCreateTemplate.Visibility = _barEditTemplateName.Visibility =
                    (allowAdd)
                        ? BarItemVisibility.Always
                        : BarItemVisibility.Never;
        }

        /// <summary>
        /// Gets dragging discipline from mechanism for transferring data.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="data">format-independent mechanism for transferring data</param>
        /// <returns>discipline</returns>
        private Discipline GetDiscipline(object sender, IDataObject data)
        {
            var discipline = data.GetData(typeof (Discipline)) as Discipline;
            if (sender == _coursesGridControl)
            {
                return discipline;
            }
            return null;
        }

        /// <summary>
        /// Gets grid hit info that corresponded dragging course from mechanism for transferring data.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="data">format-independent mechanism for transferring data</param>
        /// <returns>grid hit info</returns>
        private GridHitInfo GetCourseInfo(object sender, IDataObject data)
        {
            var courseInfo = data.GetData(typeof (BandedGridHitInfo)) as BandedGridHitInfo;
            if (sender == _disciplinesGridControl)
            {
                return courseInfo;
            }
            return null;
        }

        //private void CoursesBandedGridView_KeyUp(object sender, KeyEventArgs e)
        //{
        //    if (e.KeyData == Keys.Delete)
        //    {
        //        foreach (int rowHandle in _coursesBandedGridView.GetSelectedRows())
        //        {
        //            DeleteAllCourses(rowHandle);
        //        }
        //    }
        //}

        private void _barButtonItemRefresh_ItemClick(object sender, ItemClickEventArgs e)
        {
            Program.DbSingletone.RefreshDataSource(typeof (LessonType), typeof (Person), typeof (Lesson), typeof (Discipline),
                                         typeof (Ship), typeof (Course));

            RefreshCoursesView();
        }

        private void BarComboEditTemplate_EditValueChanged(object sender, EventArgs e)
        {
            OnSelectedTempleteChanged();
        }

        /// <summary>
        /// Handles the selected templete changed event. 
        /// </summary>
        private void OnSelectedTempleteChanged()
        {
            var template = _barComboEditTemplate.EditValue as Template;
            _barButtonCopy.Enabled = (template != null);
        }

        #region Disciplines grid control event handlers.

        private void DisciplinesGridControl_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                GridHitInfo info = _disciplinesGridView.CalcHitInfo(new Point(e.X, e.Y));
                if (info.RowHandle >= 0)
                {
                    _dragDiscipline = _disciplinesGridView.GetRow(info.RowHandle) as Discipline;
                    _dragRect = new Rectangle(new Point(e.X - SystemInformation.DragSize.Width/2,
                                                        e.Y - SystemInformation.DragSize.Height/2),
                                              SystemInformation.DragSize);
                }
            }
        }

        private void DisciplinesGridControl_MouseMove(object sender, MouseEventArgs e)
        {
            if (_dragDiscipline == null)
            {
                return;
            }
            if (e.Button != MouseButtons.Left)
            {
                return;
            }

            if (!_dragRect.Contains(new Point(e.X, e.Y)))
            {
                _disciplinesGridControl.DoDragDrop(_dragDiscipline, DragDropEffects.Copy);
            }
        }

        private void DisciplinesGridControl_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = GetCourseInfo(sender, e.Data) != null ? DragDropEffects.Copy : DragDropEffects.None;
        }

        private void DisciplinesGridControl_DragDrop(object sender, DragEventArgs e)
        {
            GridHitInfo courseInfo = GetCourseInfo(sender, e.Data);
            if (courseInfo != null)
            {
                DeleteAllCourses(courseInfo.RowHandle);
            }
        }

        private void DeleteAllCourses(int rowHandle)
        {
            DataRow row = _coursesBandedGridView.GetDataRow(rowHandle);
            if (row != null)
            {
                var discipline = row["Discipline"] as Discipline;
                var lessonType = row["LessonType"] as LessonType;

                if (_coursesDataManager.RemoveAllCourses(discipline, lessonType))
                {
                    _coursesBandedGridView.DeleteRow(rowHandle);
                }
            }
        }

        #endregion

        #region Courses grid control and view event handlers.

        private void CoursesGridControl_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = GetDiscipline(sender, e.Data) != null ? DragDropEffects.Copy : DragDropEffects.None;
        }

        private void CoursesGridControl_DragDrop(object sender, DragEventArgs e)
        {
            Discipline discipline = GetDiscipline(sender, e.Data);
            if (discipline != null)
            {
                DataRow row = _courses.NewRow();
                row["Unit"] = discipline.Unit;
                row["Part"] = discipline.Part;
                row["Discipline"] = discipline;

                _courses.Rows.Add(row);

                _coursesBandedGridView.SelectCell(_coursesBandedGridView.FocusedRowHandle,
                                                  _coursesBandedGridView.Bands[0].Columns[3]);
                _coursesBandedGridView.ShowEditor();
            }
        }

        private void CoursesGridControl_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                _removeCourseInfo = _coursesBandedGridView.CalcHitInfo(new Point(e.X, e.Y));
                if (_removeCourseInfo.RowHandle >= 0)
                {
                    _dragRect = new Rectangle(new Point(e.X - SystemInformation.DragSize.Width/2,
                                                        e.Y - SystemInformation.DragSize.Height/2),
                                              SystemInformation.DragSize);
                }
                else
                {
                    _removeCourseInfo = null;
                }
            }
        }

        private void CoursesGridControl_MouseMove(object sender, MouseEventArgs e)
        {
            if (_removeCourseInfo == null)
            {
                return;
            }
            if (e.Button != MouseButtons.Left)
            {
                return;
            }

            if (!_dragRect.Contains(new Point(e.X, e.Y)))
            {
                _coursesGridControl.DoDragDrop(_removeCourseInfo, DragDropEffects.Copy);
            }
        }

        private void CoursesBandedGridView_ValidatingEditor(object sender, BaseContainerValidateEditorEventArgs e)
        {
            if (sender != _coursesBandedGridView)
            {
                return;
            }
            string fieldName = _coursesBandedGridView.FocusedColumn.Name;

            LessonType lessonType;
            var discipline = _coursesBandedGridView.GetRowCellValue(_coursesBandedGridView.FocusedRowHandle,
                                                                    _coursesBandedGridView.Bands[0].Columns[
                                                                        "disciplineGridColumn"]) as Discipline;

            switch (fieldName)
            {
                case "lessonTypeGridColumn":

                    #region lesson's type column

                    var oldLessonType = _coursesBandedGridView.FocusedValue as LessonType;

                    lessonType = e.Value as LessonType;
                    if (lessonType == null)
                    {
                        e.ErrorText += "�� ������ ��� �������!";
                        e.Valid = false;
                    }
                    if (oldLessonType != null && lessonType != oldLessonType)
                    {
                        _coursesDataManager.UpdateCourseLessonType(discipline, oldLessonType, lessonType);
                    }

                    #endregion

                    break;
                default:

                    #region category's column

                    int duration = 0;
                    try
                    {
                        duration = Convert.ToInt32(e.Value);
                    }
                    catch
                    {
                        ///TODO ...
                    }

                    var column = _coursesBandedGridView.FocusedColumn as BandedGridColumn;
                    if (column != null && column.OwnerBand == _coursesBandedGridView.Bands[1] && column.Tag != null &&
                        column.Tag is int)
                    {
                        lessonType = _coursesBandedGridView.GetRowCellValue( _coursesBandedGridView.FocusedRowHandle,
                                                                            _coursesBandedGridView.Bands[0].Columns[
                                                                                "lessonTypeGridColumn"] ) as LessonType;
                        if (lessonType == null)
                        {
                            e.ErrorText += "�� ������ ��� �������!";
                            e.Valid = false;
                            e.Value = null;
                        }
                        if (lessonType != null)
                        {
                            IQueryable<Category> categories = from item in Program.DbSingletone.Categories
                                                              where item.CategoryId == (int) column.Tag
                                                              select item;

                            if (categories.Count() == 1)
                            {
                                Category category = categories.Single();

                                _coursesDataManager.UpdateCourse(discipline, lessonType, category, duration);
                            }
                        }
                    }

                    #endregion

                    break;
            }
        }

        #endregion

        #endregion

        private void _barButtonCopy_ItemClick(object sender, ItemClickEventArgs e)
        {
            OnCopyTemplate();
        }

        /// <summary>
        /// Handles the copy template button click event.
        /// </summary>
        private void OnCopyTemplate()
        {
            var template = _barComboEditTemplate.EditValue as Template;
            if (template == null)
            {
                return;
            }
            if (_coursesDataManager == null)
            {
                return;
            }

            _coursesDataManager.CopyTemplateCourses(template);
            RefreshCoursesView();
        }

        private void BarButtonCreateTemplate_ItemClick(object sender, ItemClickEventArgs e)
        {
            OnCreateTemplate();
        }

        /// <summary>
        /// Handles the create template button click event.
        /// </summary>
        private void OnCreateTemplate()
        {
            if (_coursesDataManager == null)
            {
                return;
            }

            string errorText;
            string templateName = (_barEditTemplateName.EditValue == null)
                                      ? string.Empty
                                      : _barEditTemplateName.EditValue.ToString();

            if (!_coursesDataManager.CreateTemplate(templateName, out errorText))
            {
                XtraMessageBox.Show(errorText, "������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                XtraMessageBox.Show(string.Format("������ {0} ������� ������.", templateName),
                                    "�����������", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            RefreshCoursesView();
        }

        public IEnumerable<ISKOCBCTDNBTIDED> GetHiddenChilds(bool withLazy)
        {
            var hidden = new List<ISKOCBCTDNBTIDED>();

            hidden.Add(new BarManagerBase(_barManager));

            return hidden;
        }
    }
}
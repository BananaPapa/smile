﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Aga.Controls.Tree;
using Aga.Controls.Tree.NodeControls;
using Curriculum.AccessAreas;
using Curriculum.Controls.AccessAreasTree;
using DevExpress.XtraEditors;
using Eureca.Integrator.Common;
using Eureca.Utility.Helper;

namespace Curriculum.Controls
{
    public partial class AccessAreasTreeControl : XtraUserControl
    {
        private AppControlItemsModel _context;

        public AccessAreasTreeControl( )
        {
            InitializeComponent( );
            Load += AccessAreasTreeControl_Load;
        }

        void AccessAreasTreeControl_Load( object sender, EventArgs e )
        {
            _nameNodeTextBox.DrawText += _nameNodeTextBox_DrawText;
        }


        void _nameNodeTextBox_DrawText( object sender, Aga.Controls.Tree.NodeControls.DrawEventArgs e )
        {
            var nodeItem = e.Node.Tag as ControlItemNode;
            if (nodeItem.LockedItemPermissionType !=PermissionType.None)
            {
                e.TextColor = Color.Gray;
            }
        }

        public BindingListWithRemoveEvent<Permission> Projection
        {
            get { return _context.Changes; }
        }


        public void LoadPermissions(IEnumerable<Permission> permissions, bool withLock = false)
        {
            if(_context == null)
                throw new InvalidOperationException("Control must be initialized before call projecting permissions");
            _context.LoadProjection( permissions , withLock );
            _accessAreasTreeContol.Refresh();
        }


        public void DropProjection()
        {
            _context.ResetView();
        }

        public void Init(IEnumerable<Tag> tags, List<Permission> permisions = null)
        {
            _context = new AppControlItemsModel();
            _accessAreasTreeContol.BeginUpdate();
            _context.LoadTree( tags );
            _accessAreasTreeContol.Model = _context;
            _accessAreasTreeContol.EndUpdate();
            _accessAreasTreeContol.FullUpdate();
           // _accessAreasTreeContol.ExpandAll();
            ExpandToLevel2();
        }

        private void ExpandToLevel2( )
        {
            foreach (var treeNodeAdv in _context.GetChildren( TreePath.Empty ).Cast<ControlItemNode>())
            {
                var node = _accessAreasTreeContol.FindNodeByTag(treeNodeAdv);
                node.Expand( true );
                foreach (var treeNodeAdvChild in _context.GetChildByPath(treeNodeAdv.ItemPath))
                {
                    var childNode = _accessAreasTreeContol.FindNodeByTag( treeNodeAdvChild );
                    childNode.Expand( true );
                }
            }
            
        }
    }
}

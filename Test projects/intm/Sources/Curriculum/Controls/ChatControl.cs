﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraTab;
using Eureca.Utility.Extensions;

namespace Curriculum.Controls
{
    public partial class ChatControl : XtraUserControl
    {
        static Font dateFont,messageFont;
        private Color _interlacutorColor = Color.Black;
        private Color _recieverColor = Color.DarkGreen;
        private User _interlacutor,_currentUser;
        private XtraTabPage _pageOwner;
        List<ReceiveMessageArgs> _unhandledMessageArgs = new List<ReceiveMessageArgs>(); 

        public event EventHandler EndOfConversation;
        public event EventHandler<MessagesChangedArgs> MessagesChanged;

        public User Interlacutor {
            get { return _interlacutor; }
        }


        void activateElement( object sender, EventArgs e )
        {
            if(_unhandledMessageArgs.Count == 0)
                return;
            foreach (var unhandledMessageArg in _unhandledMessageArgs)
            {
                unhandledMessageArg.Handled = true;
            }
            _unhandledMessageArgs.Clear( );
            UpdateHeader( );
        }

        static ChatControl()
        {
            FontFamily fontFamily = new FontFamily( "Consolas" );
            dateFont = new Font(
               fontFamily,
               10,
               FontStyle.Regular,
               GraphicsUnit.Pixel );
            messageFont = new Font(
               fontFamily,
               12,
               FontStyle.Regular,
               GraphicsUnit.Pixel );
        }

        private void SubscribeClickOnAllChildControl(Control control ,Action<object, EventArgs> activateElement )
        {
            var eventHandler = new EventHandler(activateElement);
            foreach (var childControl in control.Controls.Cast<Control>())
            {
                childControl.Click += eventHandler;
                if(childControl.Controls.Count>0)
                    SubscribeClickOnAllChildControl(childControl,activateElement);
            }
        }

        public ChatControl( User interlacutor )
        {
            _interlacutor = interlacutor;
            _currentUser = Program.CurrentUser;
            InitializeComponent( );
            SubscribeClickOnAllChildControl(this, activateElement );
            PrintHistory( );
        }

         void PrintHistory()
        {
            var oldMessages =
                Program.DbSingletone.Messages.Where(
                    message => message.Delivered && message.CreationDate > DateTime.Now.AddDays(-7) && ( (message.ReceiverUser == _interlacutor && message.SenderUser == _currentUser) || (message.ReceiverUser == _currentUser && message.SenderUser == _interlacutor)) ).OrderBy(message => message.CreationDate).ToList();
            if (oldMessages.Any( ))
            {
                foreach (var oldMessage in oldMessages)
                {
                    ShowMessage(oldMessage);
                }
                Print( "________________", Color.DimGray, messageFont, HorizontalAlignment.Center );
                Print( "\u2028", Color.DimGray, messageFont, HorizontalAlignment.Center );
                _chatRTB.Select( 0, _chatRTB.TextLength );
                _chatRTB.SelectionColor = Color.DimGray;
                _chatRTB.Select( 0, 0 );
            }

        }

        private void _sendButton_Click( object sender, EventArgs e )
        {
            SendMessage();
        }

        private void SendMessage()
        {
            string messageText = _messageInboxFormMemoEdit.Text.Trim(new []{'\r','\n',' ','\t'});
            if (String.IsNullOrEmpty(messageText))
                return;
            var curriculumDataContext = Program.Db;
            Message msg = new Message()
            {
                CreationDate = DateTime.Now,
                ReceiverUser = curriculumDataContext.Users.SingleOrDefault( user => user.UserId == _interlacutor .UserId),
                SenderUser = curriculumDataContext.Users.SingleOrDefault( user => user.UserId == _currentUser.UserId ),
                MessageText = messageText
            };
            curriculumDataContext.Messages.InsertOnSubmit(msg);
            curriculumDataContext.SubmitChanges();
            _messageInboxFormMemoEdit.Text = "";
            ShowMessage(msg);
        }

        private void _closeDialogBtn_Click( object sender, EventArgs e )
        {
            //TODO: отправить сообщение 

            if (EndOfConversation != null)
                EndOfConversation(this, EventArgs.Empty);
        }

        public void ReceiveMessage( ReceiveMessageArgs messageArgs )
        {
            ShowMessage( messageArgs.Message );
            _unhandledMessageArgs.Add( messageArgs );
            UpdateHeader( );
        }

        private void UpdateHeader( )
        {
            if(MessagesChanged!= null)
                MessagesChanged(this,new MessagesChangedArgs(_unhandledMessageArgs.Count));
        }

        #region printing

        public void ShowMessage(Message message)
        {
            HorizontalAlignment aligment;
            Color color = default(Color);
            User author;
            if (message.SenderUserId == _currentUser.UserId)
            {
                aligment = HorizontalAlignment.Right;
                color = _interlacutorColor;
                author = _currentUser;
            }
            else
            {
                aligment = HorizontalAlignment.Left;
                color = _recieverColor;
                author = _interlacutor;
            }
            Print(message.MessageText + Environment.NewLine, color, messageFont, aligment);
            Print("{0} написал: {1:G}".FormatString(author.Name, message.CreationDate) + Environment.NewLine, color, dateFont, aligment);
            Print( Environment.NewLine );

            //drop selection and scroll to end
            ScrollToEnd();
        }

        private void ScrollToEnd()
        {
            if (_chatRTB.InvokeRequired)
                _chatRTB.Invoke((Action) ScrollToEnd);
            else
            {
                _chatRTB.SelectionStart = _chatRTB.Text.Length;
                _chatRTB.ScrollToCaret();
                _chatRTB.Select(0, 0);
            }
        }

        private void Print(string text, Color foreColor, Font font,
            HorizontalAlignment? hAlignment = null)
        {
            if (_chatRTB.InvokeRequired)
                _chatRTB.Invoke((Action<string, Color, Font, HorizontalAlignment?>) Print,
                    new object[] {text, foreColor, font, hAlignment});
            else
            {
                AppendAndSelect(text);
                if (hAlignment.HasValue)
                    _chatRTB.SelectionAlignment = hAlignment.Value;
                _chatRTB.SelectionColor = foreColor;
                _chatRTB.SelectionFont = font;
                _chatRTB.Select(0, 0);
            }
        }

        private void AppendAndSelect(string text)
        {
            var textLength = _chatRTB.TextLength;
            _chatRTB.AppendText(text);
            _chatRTB.Select(textLength, _chatRTB.TextLength - textLength);
        }

        private void Print(string text)
        {
            Print(text,Color.Black,messageFont);
        }

        #endregion

        private void _messageInboxFormMemoEdit_KeyDown( object sender, KeyEventArgs e )
        {
            if (e.KeyCode == Keys.Enter && Control.ModifierKeys == Keys.Control)
            {
                SendMessage();
                e.Handled = false;
            }
        }
    }

    public class MessagesChangedArgs : EventArgs
    {
        public int MessageCount { get; private set; }

        public MessagesChangedArgs(int messageCount)
        {
            MessageCount = messageCount;
        }   
    }


}

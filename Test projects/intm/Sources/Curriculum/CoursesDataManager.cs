﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using log4net;

namespace Curriculum
{
    /// <summary>
    /// Data manager for courses controls.
    /// Incapsulates work with database.
    /// </summary>
    internal class CoursesDataManager
    {
        readonly static ILog Log = LogManager.GetLogger( typeof( CoursesDataManager ) );

        #region Nested type: ProxyCourse

        /// <summary>
        /// Proxy class for entity "Course".
        /// </summary>
        private class ProxyCourse : ICourse
        {
            #region ICourse Members

            /// <summary>
            /// Gets primary id of course.
            /// </summary>
            public int CourseId { get; set; }

            /// <summary>
            /// Gets or sets category of person.
            /// </summary>
            public Category Category { get; set; }

            /// <summary>
            /// Gets or sets descipline.
            /// </summary>
            public Discipline Discipline { get; set; }

            /// <summary>
            /// Gets or sets lesson type.
            /// </summary>
            public LessonType LessonType { get; set; }

            /// <summary>
            /// Gets or sets duration.
            /// </summary>
            public int Duration { get; set; }

            #endregion
        }

        #endregion

        #region Fields and Properties

        /// <summary>
        /// Current ship.
        /// </summary>
        private readonly Ship _ship;

        /// <summary>
        /// Current template.
        /// </summary>
        private readonly Template _template;

        /// <summary>
        /// Gets the current ship.
        /// </summary>
        public Ship Ship
        {
            get { return _ship; }
        }

        /// <summary>
        /// Gets the current template
        /// </summary>
        public Template Template
        {
            get { return _template; }
        }

        #endregion Fields and Properties

        #region Constructors

        /// <summary>
        /// Constructor.
        /// Uses for working with ship courses.
        /// </summary>
        /// <param name="ship">ship</param>
        public CoursesDataManager(Ship ship)
        {
            _ship = ship;
        }

        /// <summary>
        /// Constructor.
        /// Uses for working with template courses.
        /// </summary>
        /// <param name="ship">template</param>
        public CoursesDataManager(Template template)
        {
            _template = template;
        }

        #endregion Constructors

        #region Private methods

        /// <summary>
        /// Gets the query to get all courses for current ship.
        /// </summary>
        /// <returns>query</returns>
        private IQueryable<ICourse> GetShipCourses()
        {
            if (_ship == null)
            {
                return null;
            }

            IQueryable<ICourse> query = Program.DbSingletone.Courses.
                Where(item => item.ShipId == _ship.ShipId).
                Select(item => (ICourse) new ProxyCourse
                                             {
                                                 CourseId = item.CourseId,
                                                 Category = item.Category,
                                                 Discipline = item.Discipline,
                                                 LessonType = item.LessonType,
                                                 Duration = item.Duration
                                             }
                );
            return query;
        }

        /// <summary>
        /// Gets the query to get all courses for current template.
        /// </summary>
        /// <returns>query</returns>
        private IQueryable<ICourse> GetTemplateCourses()
        {
            if (_template == null)
            {
                return null;
            }

            IQueryable<ICourse> query = Program.DbSingletone.TemplateCourses.
                Where(item => item.TemplateId == _template.TemplateId).
                Select(item => (ICourse) new ProxyCourse
                                             {
                                                 CourseId = item.CourseId,
                                                 Category = item.Category,
                                                 Discipline = item.Discipline,
                                                 LessonType = item.LessonType,
                                                 Duration = item.Duration
                                             }
                );
            return query;
        }

        /// <summary>
        /// Adds a new course in the db for current ship.
        /// </summary>
        /// <param name="discipline">discipline</param>
        /// <param name="lessonType">lesson's type</param>
        /// <param name="category">category</param>
        /// <param name="duration">course's duration</param>
        private void AddNewShipCourse(Discipline discipline, LessonType lessonType,
                                      Category category, int duration)
        {
            #region Debug.Assert()

            Debug.Assert(discipline != null, "При добавлении нового курса возникла ошибка!" +
                                             Environment.NewLine + "Не указана тема.");
            Debug.Assert(_ship != null, "При добавлении нового курса возникла ошибка!" +
                                        Environment.NewLine + "Не указан корабль.");
            Debug.Assert(lessonType != null, "При добавлении нового курса возникла ошибка!" +
                                             Environment.NewLine + "Не указан тип занятия");
            Debug.Assert(category != null, "При добавлении нового курса возникла ошибка!" +
                                           Environment.NewLine + "Не указана категория");

            #endregion

            var course = new Course();
            course.Discipline = discipline;
            course.Ship = _ship;
            course.LessonType = lessonType;
            course.Category = category;
            course.Duration = duration;

            Program.DbSingletone.Courses.InsertOnSubmit(course);
        }

        /// <summary>
        /// Adds a new course in the db for current template.
        /// </summary>
        /// <param name="discipline">discipline</param>
        /// <param name="lessonType">lesson's type</param>
        /// <param name="category">category</param>
        /// <param name="duration">course's duration</param>
        private void AddNewTemplateCourse(Discipline discipline, LessonType lessonType,
                                          Category category, int duration)
        {
            #region Debug.Assert()

            Debug.Assert(discipline != null, "При добавлении нового курса возникла ошибка!" +
                                             Environment.NewLine + "Не указана тема.");
            Debug.Assert(_template != null, "При добавлении нового курса возникла ошибка!" +
                                            Environment.NewLine + "Не указан шаблон.");
            Debug.Assert(lessonType != null, "При добавлении нового курса возникла ошибка!" +
                                             Environment.NewLine + "Не указан тип занятия");
            Debug.Assert(category != null, "При добавлении нового курса возникла ошибка!" +
                                           Environment.NewLine + "Не указана категория");

            #endregion

            var course = new TemplateCourse();
            course.Discipline = discipline;
            course.Template = _template;
            course.LessonType = lessonType;
            course.Category = category;
            course.Duration = duration;

            Program.DbSingletone.TemplateCourses.InsertOnSubmit(course);
        }

        /// <summary>
        /// Removes the course from the db for current ship.
        /// </summary>
        /// <param name="discipline">discipline</param>
        /// <param name="lessonType">lesson's type</param>
        /// <param name="category">category</param>
        private void RemoveShipCourse(Discipline discipline, LessonType lessonType, Category category)
        {
            #region Debug.Assert()

            Debug.Assert(discipline != null, "При удалении курса возникла ошибка!" +
                                             Environment.NewLine + "Не указана тема.");
            Debug.Assert(_ship != null, "При удалении курса возникла ошибка!" +
                                        Environment.NewLine + "Не указан корабль.");
            Debug.Assert(lessonType != null, "При удалении курса возникла ошибка!" +
                                             Environment.NewLine + "Не указан тип занятия");
            Debug.Assert(category != null, "При удалении курса возникла ошибка!" +
                                           Environment.NewLine + "Не указана категория");

            #endregion

            try
            {
                Course course = Program.DbSingletone.Courses.Single(item => item.Discipline == discipline &&
                                                                  item.Ship == _ship && item.LessonType == lessonType &&
                                                                  item.Category == category);
                Program.DbSingletone.Courses.DeleteOnSubmit(course);
            }
            catch
            {
            }
        }

        /// <summary>
        /// Removes the course from the db for current template.
        /// </summary>
        /// <param name="discipline">discipline</param>
        /// <param name="lessonType">lesson's type</param>
        /// <param name="category">category</param>
        private void RemoveTemplateCourse(Discipline discipline, LessonType lessonType, Category category)
        {
            #region Debug.Assert()

            Debug.Assert(discipline != null, "При удалении курса возникла ошибка!" +
                                             Environment.NewLine + "Не указана тема.");
            Debug.Assert(_template != null, "При удалении курса возникла ошибка!" +
                                            Environment.NewLine + "Не указан шаблон.");
            Debug.Assert(lessonType != null, "При удалении курса возникла ошибка!" +
                                             Environment.NewLine + "Не указан тип занятия");
            Debug.Assert(category != null, "При удалении курса возникла ошибка!" +
                                           Environment.NewLine + "Не указана категория");

            #endregion

            try
            {
                TemplateCourse course = Program.DbSingletone.TemplateCourses.Single(item => item.Discipline == discipline &&
                                                                                  item.Template == _template &&
                                                                                  item.LessonType == lessonType &&
                                                                                  item.Category == category);
                Program.DbSingletone.TemplateCourses.DeleteOnSubmit(course);
            }
            catch
            {
            }
        }

        /// <summary>
        /// Updates course for current ship.
        /// If there no such course for the category in the db, then creates a new course.
        /// </summary>
        /// <param name="discipline">discipline</param>
        /// <param name="lessonType">lesson's type</param>
        /// <param name="category">category</param>
        /// <param name="duration">course's duration</param>
        private void UpdateShipCourse(Discipline discipline, LessonType lessonType,
                                      Category category, int duration)
        {
            #region Debug.Assert()

            Debug.Assert(discipline != null, "При обновлении курса возникла ошибка!" +
                                             Environment.NewLine + "Не указана тема.");
            Debug.Assert(_ship != null, "При обновлении курса возникла ошибка!" +
                                        Environment.NewLine + "Не указан корабль.");
            Debug.Assert(lessonType != null, "При обновлении курса возникла ошибка!" +
                                             Environment.NewLine + "Не указан тип занятия");
            Debug.Assert(category != null, "При обновлении курса возникла ошибка!" +
                                           Environment.NewLine + "Не указана категория");

            #endregion

            if (duration <= 0)
            {
                RemoveShipCourse(discipline, lessonType, category);
            }
            else
            {
                try
                {
                    Course course = Program.DbSingletone.Courses.Single(item => item.Discipline == discipline &&
                                                                      item.Ship == _ship &&
                                                                      item.LessonType == lessonType &&
                                                                      item.Category == category);
                    course.Duration = duration;
                }
                catch (InvalidOperationException)
                {
                    AddNewShipCourse(discipline, lessonType, category, duration);
                }
            }
        }

        /// <summary>
        /// Updates course for current template.
        /// If there no such course for the category in the db, then creates a new course.
        /// </summary>
        /// <param name="discipline">discipline</param>
        /// <param name="lessonType">lesson's type</param>
        /// <param name="category">category</param>
        /// <param name="duration">course's duration</param>
        private void UpdateTemplateCourse(Discipline discipline, LessonType lessonType,
                                          Category category, int duration)
        {
            #region Debug.Assert()

            Debug.Assert(discipline != null, "При обновлении курса возникла ошибка!" +
                                             Environment.NewLine + "Не указана тема.");
            Debug.Assert(_template != null, "При обновлении курса возникла ошибка!" +
                                            Environment.NewLine + "Не указан шаблон.");
            Debug.Assert(lessonType != null, "При обновлении курса возникла ошибка!" +
                                             Environment.NewLine + "Не указан тип занятия");
            Debug.Assert(category != null, "При обновлении курса возникла ошибка!" +
                                           Environment.NewLine + "Не указана категория");

            #endregion

            if (duration == 0)
            {
                RemoveTemplateCourse(discipline, lessonType, category);
            }
            else
            {
                try
                {
                    TemplateCourse course = Program.DbSingletone.TemplateCourses.Single(item => item.Discipline == discipline &&
                                                                                      item.Template == _template &&
                                                                                      item.LessonType == lessonType &&
                                                                                      item.Category == category);
                    course.Duration = duration;
                }
                catch (InvalidOperationException)
                {
                    AddNewTemplateCourse(discipline, lessonType, category, duration);
                }
            }
        }

        /// <summary>
        /// Changes lesson's type for all categories of courses. 
        /// </summary>
        /// <param name="discipline">discipline</param>
        /// <param name="oldLessonType">old lesson's type</param>
        /// <param name="newLessonType">new lesson's type</param>
        private void UpdateShipCourseLessonType(Discipline discipline, LessonType oldLessonType,
                                                LessonType newLessonType)
        {
            #region Debug.Assert()

            Debug.Assert(discipline != null, "При обновлении курса возникла ошибка!" +
                                             Environment.NewLine + "Не указана тема.");
            Debug.Assert(_ship != null, "При обновлении курса возникла ошибка!" +
                                        Environment.NewLine + "Не указан корабль.");
            Debug.Assert(newLessonType != null, "При обновлении курса возникла ошибка!" +
                                                Environment.NewLine + "Не указан старый тип занятия");
            Debug.Assert(newLessonType != null, "При обновлении курса возникла ошибка!" +
                                                Environment.NewLine + "Не указан новый тип занятия");

            #endregion

            foreach (Category category in Program.DbSingletone.Categories)
            {
                try
                {
                    Course course = Program.DbSingletone.Courses.Single(item => item.Discipline == discipline &&
                                                                      item.Ship == _ship &&
                                                                      item.LessonType == oldLessonType &&
                                                                      item.Category == category);
                    course.LessonType = newLessonType;
                }
                catch (InvalidOperationException)
                {
                }
            }
        }

        /// <summary>
        /// Changes lesson's type for all categories of courses. 
        /// </summary>
        /// <param name="discipline">discipline</param>
        /// <param name="oldLessonType">old lesson's type</param>
        /// <param name="newLessonType">new lesson's type</param>
        private void UpdateTemplateCourseLessonType(Discipline discipline, LessonType oldLessonType,
                                                    LessonType newLessonType)
        {
            #region Debug.Assert()

            Debug.Assert(discipline != null, "При обновлении курса возникла ошибка!" +
                                             Environment.NewLine + "Не указана тема.");
            Debug.Assert(_template != null, "При обновлении курса возникла ошибка!" +
                                            Environment.NewLine + "Не указан шаблон.");
            Debug.Assert(newLessonType != null, "При обновлении курса возникла ошибка!" +
                                                Environment.NewLine + "Не указан старый тип занятия");
            Debug.Assert(newLessonType != null, "При обновлении курса возникла ошибка!" +
                                                Environment.NewLine + "Не указан новый тип занятия");

            #endregion

            foreach (Category category in Program.DbSingletone.Categories)
            {
                try
                {
                    TemplateCourse course = Program.DbSingletone.TemplateCourses.Single(item => item.Discipline == discipline &&
                                                                                      item.Template == _template &&
                                                                                      item.LessonType == oldLessonType &&
                                                                                      item.Category == category);
                    course.LessonType = newLessonType;
                }
                catch (InvalidOperationException)
                {
                }
            }
        }

        /// <summary>
        /// Removes all courses for current ship.
        /// </summary>
        /// <param name="discipline">discipline</param>
        /// <param name="lessonType">lesson's type</param>
        public void RemoveAllShipCourses(Discipline discipline, LessonType lessonType)
        {
            #region Debug.Assert()

            Debug.Assert(discipline != null, "При обновлении курса возникла ошибка!" +
                                             Environment.NewLine + "Не указана тема.");
            Debug.Assert(_ship != null, "При обновлении курса возникла ошибка!" +
                                        Environment.NewLine + "Не указан корабль.");
            Debug.Assert(lessonType != null, "При обновлении курса возникла ошибка!" +
                                             Environment.NewLine + "Не указан тип занятия");

            #endregion

            IQueryable<Course> courses = from course in Program.DbSingletone.Courses
                                         where course.Ship == _ship && course.Discipline == discipline &&
                                               course.LessonType == lessonType
                                         select course;
            foreach (Course course in courses)
            {
                Program.DbSingletone.Courses.DeleteOnSubmit(course);
            }
        }

        /// <summary>
        /// Removes all courses for current ship.
        /// </summary>
        /// <param name="discipline">discipline</param>
        /// <param name="lessonType">lesson's type</param>
        public void RemoveAllTemplateCourses(Discipline discipline, LessonType lessonType)
        {
            #region Debug.Assert()

            Debug.Assert(discipline != null, "При обновлении курса возникла ошибка!" +
                                             Environment.NewLine + "Не указана тема.");
            Debug.Assert(_template != null, "При обновлении курса возникла ошибка!" +
                                            Environment.NewLine + "Не указан шаблон.");
            Debug.Assert(lessonType != null, "При обновлении курса возникла ошибка!" +
                                             Environment.NewLine + "Не указан тип занятия");

            #endregion

            IQueryable<TemplateCourse> courses = from course in Program.DbSingletone.TemplateCourses
                                                 where
                                                     course.Template == _template && course.Discipline == discipline &&
                                                     course.LessonType == lessonType
                                                 select course;
            foreach (TemplateCourse course in courses)
            {
                Program.DbSingletone.TemplateCourses.DeleteOnSubmit(course);
            }
        }

        /// <summary>
        /// Copy course template to ship.
        /// </summary>
        /// <param name="templateCourse">course template</param>
        private void CopyTempleteCourseToShip(TemplateCourse templateCourse)
        {
            if (_ship == null)
            {
                return;
            }
            if (templateCourse == null)
            {
                return;
            }
            if (templateCourse.Duration == 0)
            {
                return;
            }

            try
            {
                Course course = Program.DbSingletone.Courses.Single(item => item.Discipline == templateCourse.Discipline &&
                                                                  item.Ship == _ship &&
                                                                  item.LessonType == templateCourse.LessonType &&
                                                                  item.Category == templateCourse.Category);
                course.Duration += templateCourse.Duration;
            }
            catch (InvalidOperationException)
            {
                AddNewShipCourse(templateCourse.Discipline, templateCourse.LessonType,
                                 templateCourse.Category, templateCourse.Duration);
            }
        }

        /// <summary>
        /// Copy course template to another template.
        /// </summary>
        /// <param name="templateCourse">course template</param>
        private void CopyTempleteCourseToTemplate(TemplateCourse templateCourse)
        {
            if (_template == null)
            {
                return;
            }
            if (templateCourse == null)
            {
                return;
            }
            if (templateCourse.Duration == 0)
            {
                return;
            }

            try
            {
                TemplateCourse course =
                    Program.DbSingletone.TemplateCourses.Single(item => item.Discipline == templateCourse.Discipline &&
                                                              item.Template == _template &&
                                                              item.LessonType == templateCourse.LessonType &&
                                                              item.Category == templateCourse.Category);
                course.Duration += templateCourse.Duration;
            }
            catch (InvalidOperationException)
            {
                AddNewTemplateCourse(templateCourse.Discipline, templateCourse.LessonType,
                                     templateCourse.Category, templateCourse.Duration);
            }
        }

        /// <summary>
        /// Copy templete courses to ship.
        /// </summary>
        /// <param name="template">template</param>
        private void CopyTemplateCoursesToShip(Template template)
        {
            if (_ship == null)
            {
                return;
            }
            if (template == null)
            {
                return;
            }

            IEnumerable<TemplateCourse> templateCourses = Program.DbSingletone.TemplateCourses.
                Where(item => item.TemplateId == template.TemplateId).
                Select(item => item);
            foreach (TemplateCourse templateCourse in templateCourses)
            {
                CopyTempleteCourseToShip(templateCourse);
            }
        }

        /// <summary>
        /// Copy templete courses to ship.
        /// </summary>
        /// <param name="template">template</param>
        private void CopyTemplateCoursesToTemplate(Template template)
        {
            if (_template == null)
            {
                return;
            }
            if (template == null)
            {
                return;
            }

            IEnumerable<TemplateCourse> templateCourses = Program.DbSingletone.TemplateCourses.
                Where(item => item.TemplateId == template.TemplateId).
                Select(item => item);
            foreach (TemplateCourse templateCourse in templateCourses)
            {
                CopyTempleteCourseToTemplate(templateCourse);
            }

            SubmitChanges();
        }

        /// <summary>
        /// Commit changes in db.
        /// </summary>
        /// <returns>result</returns>
        private bool SubmitChanges()
        {
            try
            {
                Program.DbSingletone.SubmitChanges();
                return true;
            }
            catch (DbException ex)
            {
                Program.HandleDbException(ex);
            }
            catch (Exception ex)
            {
                var sb = new StringBuilder();
                sb.Append("При попытке сохранить изменения в базе данных");
                sb.Append(Environment.NewLine);
                sb.Append("произошла ошибка. Все изменения были потеряны.");
                sb.Append(Environment.NewLine);
                sb.Append("Перезапустите программу.");
                XtraMessageBox.Show(sb.ToString(), "Критическая ошибка", MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
                Log.Error( sb.ToString( ) + "\n" + ex.ToString( ) );
            }

            return false;
        }

        #endregion Private methods

        #region Public methods

        /// <summary>
        /// Gets the query to get all templates.
        /// </summary>
        /// <returns>query</returns>
        public IQueryable<Template> GetTemplates()
        {
            IQueryable<Template> templates = Program.DbSingletone.Templates.Select(item => item);
            return templates;
        }

        /// <summary>
        /// Gets query to all courses from database.
        /// </summary>
        /// <returns>query</returns>
        public IQueryable<ICourse> GetCourses()
        {
            if (_ship != null)
            {
                return GetShipCourses();
            }
            if (_template != null)
            {
                return GetTemplateCourses();
            }

            return null;
        }

        /// <summary>
        /// Adds a new course in the db.
        /// </summary>
        /// <param name="discipline">discipline</param>
        /// <param name="lessonType">lesson's type</param>
        /// <param name="category">category</param>
        /// <param name="duration">course's duration</param>
        public void AddNewCourse(Discipline discipline, LessonType lessonType,
                                 Category category, int duration)
        {
            if (_ship != null)
            {
                AddNewShipCourse(discipline, lessonType, category, duration);
                SubmitChanges();
                return;
            }
            if (_template != null)
            {
                AddNewTemplateCourse(discipline, lessonType, category, duration);
                SubmitChanges();
                return;
            }
        }

        /// <summary>
        /// Removes the course from the db.
        /// </summary>
        /// <param name="discipline">discipline</param>
        /// <param name="lessonType">lesson's type</param>
        /// <param name="category">category</param>
        public void RemoveCourse(Discipline discipline, LessonType lessonType, Category category)
        {
            if (_ship != null)
            {
                RemoveShipCourse(discipline, lessonType, category);
                SubmitChanges();
                return;
            }
            if (_template != null)
            {
                RemoveTemplateCourse(discipline, lessonType, category);
                SubmitChanges();
                return;
            }
        }

        /// <summary>
        /// Updates course. If there no such course for the category in the db, then creates a new course.
        /// </summary>
        /// <param name="discipline">discipline</param>
        /// <param name="lessonType">lesson's type</param>
        /// <param name="category">category</param>
        /// <param name="duration">course's duration</param>
        public void UpdateCourse(Discipline discipline, LessonType lessonType,
                                 Category category, int duration)
        {
            if (_ship != null)
            {
                UpdateShipCourse(discipline, lessonType, category, duration);
                SubmitChanges();
                return;
            }
            if (_template != null)
            {
                UpdateTemplateCourse(discipline, lessonType, category, duration);
                SubmitChanges();
                return;
            }
        }

        /// <summary>
        /// Adds a new course for each category.
        /// </summary>
        /// <param name="discipline">discipline</param>
        /// <param name="lessonType">lesson's type</param>
        public void AddNewCoursesForAllCategories(Discipline discipline, LessonType lessonType)
        {
            foreach (Category category in Program.DbSingletone.Categories)
            {
                AddNewCourse(discipline, lessonType, category, 0);
            }

            SubmitChanges();
        }

        /// <summary>
        /// Changes lesson's type for all categories of courses. 
        /// </summary>
        /// <param name="discipline">discipline</param>
        /// <param name="oldLessonType">old lesson's type</param>
        /// <param name="newLessonType">new lesson's type</param>
        public void UpdateCourseLessonType(Discipline discipline, LessonType oldLessonType,
                                           LessonType newLessonType)
        {
            if (_ship != null)
            {
                UpdateShipCourseLessonType(discipline, oldLessonType, newLessonType);
                SubmitChanges();
                return;
            }
            if (_template != null)
            {
                UpdateTemplateCourseLessonType(discipline, oldLessonType, newLessonType);
                SubmitChanges();
                return;
            }
        }

        /// <summary>
        /// Removes all courses.
        /// </summary>
        /// <param name="discipline">discipline</param>
        /// <param name="lessonType">lesson's type</param>
        public bool RemoveAllCourses(Discipline discipline, LessonType lessonType)
        {
            if (_ship != null)
            {
                RemoveAllShipCourses(discipline, lessonType);
                return SubmitChanges();
            }
            if (_template != null)
            {
                RemoveAllTemplateCourses(discipline, lessonType);
                return SubmitChanges();
            }

            return false;
        }

        /// <summary>
        /// Copy templete courses.
        /// </summary>
        /// <param name="template">template</param>
        public bool CopyTemplateCourses(Template template)
        {
            if (_ship != null)
            {
                CopyTemplateCoursesToShip(template);
                return SubmitChanges();
            }
            if (_template != null)
            {
                CopyTemplateCoursesToTemplate(template);
                return SubmitChanges();
            }

            return false;
        }

        /// <summary>
        /// Create template courses for current ship. 
        /// </summary>        
        /// <param name="templateName">template name</param>
        /// <param name="errorText">text of error</param>
        /// <returns>result of operation</returns>
        public bool CreateTemplate(string templateName, out string errorText)
        {
            errorText = string.Empty;
            if (_ship == null)
            {
                return false;
            }

            // Gets all courses for ship
            IQueryable<Course> courses = Program.DbSingletone.Courses.Where(i => i.ShipId == _ship.ShipId).Select(i => i);
            if (courses.Count() == 0)
            {
                errorText = "Шаблон курса не может быть пустым.";
                return false;
            }

            // Checks template name.
            if (string.IsNullOrEmpty(templateName))
            {
                errorText = "Имя шаблона не может быть пустым!";
                return false;
            }

            // Checks uniqueness of template name.
            Template temp = Program.DbSingletone.Templates.Where(i => i.Name == templateName).FirstOrDefault();
            if (temp != null)
            {
                errorText = "Шаблон с таким именем уже существует!";
                return false;
            }

            var template = new Template();
            template.Name = templateName;
            Program.DbSingletone.Templates.InsertOnSubmit(template);

            foreach (Course course in courses)
            {
                var templateCourse = new TemplateCourse
                                         {
                                             Category = course.Category,
                                             Discipline = course.Discipline,
                                             Duration = course.Duration,
                                             LessonType = course.LessonType,
                                             Template = template
                                         };
                Program.DbSingletone.TemplateCourses.InsertOnSubmit(templateCourse);
            }

            return SubmitChanges();
        }

        #endregion Public methods
    }
}
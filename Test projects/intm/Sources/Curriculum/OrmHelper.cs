﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Curriculum
{
    public static class OrmHelper
    {

        public  static  void DeleteRole(Role role)
        {
            Stack<Role> hierarchyStack = new Stack<Role>( );
            hierarchyStack.Push( role );
            while (true)
            {
                if (hierarchyStack.Count == 0)
                    break;
                var curentNode = hierarchyStack.Peek( );
                var childs = Program.DbSingletone.Roles.Where(role1 => role1.ParentRole.RoleId == curentNode.RoleId);
                if (childs.Any())
                {
                    foreach (var child in childs)
                    {
                        hierarchyStack.Push( child );
                    }
                }
                else
                {
                    Program.DbSingletone.Roles.DeleteOnSubmit( hierarchyStack.Pop( ) ); 
                    Program.DbSingletone.SubmitChanges();
                }
            }
        }

        public static UserPosition GetUserPosition(this User user)
        {
            var manyToManyUPURecord = Program.DbSingletone.UserPositionsUsers.SingleOrDefault( departmentsUser => departmentsUser.User == user );
            return manyToManyUPURecord == null ? null : manyToManyUPURecord.UserPosition;
        }

        public static UserDepartment GetUserDepartment( this User user )
        {
            var manyToManyUDURecord = Program.DbSingletone.UsersDepartmentsUsers.SingleOrDefault( departmentsUser => departmentsUser.User == user );
            return manyToManyUDURecord== null? null : manyToManyUDURecord.UserDepartment;
        }

        public static void SetUserPosition( this User user,UserPosition userPosition )
        {
            var manyToManyUPURecord = Program.DbSingletone.UserPositionsUsers.SingleOrDefault( departmentsUser => departmentsUser.User == user );
            if (manyToManyUPURecord == null)
            {
                manyToManyUPURecord = new UserPositionsUser(){User = user,UserPosition = userPosition};
                Program.DbSingletone.UserPositionsUsers.InsertOnSubmit(manyToManyUPURecord);
            }
            else
            {
                if (manyToManyUPURecord.UserPosition != userPosition)
                    manyToManyUPURecord.UserPosition = userPosition;
            }
            //Program.Db.SubmitChanges();
        }

        public static void SetUserDepartment( this User user, UserDepartment userDepartment )
        {
            var manyToManyUDURecord = Program.DbSingletone.UsersDepartmentsUsers.SingleOrDefault( departmentsUser => departmentsUser.User == user );
            if (manyToManyUDURecord == null)
            {
                manyToManyUDURecord = new UsersDepartmentsUser( ) { User = user, UserDepartment = userDepartment };
                Program.DbSingletone.UsersDepartmentsUsers.InsertOnSubmit( manyToManyUDURecord );
            }
            else
            {
                if (manyToManyUDURecord.UserDepartment != userDepartment)
                    manyToManyUDURecord.UserDepartment = userDepartment;
            }
            //Program.Db.SubmitChanges( );
        }

    }
}

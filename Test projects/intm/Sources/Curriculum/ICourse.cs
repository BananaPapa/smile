﻿namespace Curriculum
{
    /// <summary>
    /// Course interface.
    /// </summary>
    public interface ICourse
    {
        /// <summary>
        /// Gets primary id of course.
        /// </summary>
        int CourseId { get; }

        /// <summary>
        /// Gets or sets category of person.
        /// </summary>
        Category Category { get; set; }

        /// <summary>
        /// Gets or sets descipline.
        /// </summary>
        Discipline Discipline { get; set; }

        /// <summary>
        /// Gets or sets lesson type.
        /// </summary>
        LessonType LessonType { get; set; }

        /// <summary>
        /// Gets or sets duration.
        /// </summary>
        int Duration { get; set; }
    }
}
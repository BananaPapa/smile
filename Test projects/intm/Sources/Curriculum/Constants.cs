﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Curriculum;

namespace Integrator.Curriculum.Constants
{
    public static class Constants
    {
        public const char splitCharapter = '#';
        public const string splitString = "#";
        public static Role EmptyRole;
        public static UserDepartment EmptyUserDepartment;
        public static UserPosition EmptyUserPosition;

        static Constants( )
        {
            EmptyRole = new Role( ) { Name = "Не задано" };
            Program.DbSingletone.Roles.InsertOnSubmit( EmptyRole );
            Program.DbSingletone.Roles.DeleteOnSubmit( EmptyRole );

            EmptyUserPosition = new UserPosition( ) { Name = "Не задано" };
            Program.DbSingletone.UserPositions.InsertOnSubmit( EmptyUserPosition );
            Program.DbSingletone.UserPositions.DeleteOnSubmit( EmptyUserPosition );

            EmptyUserDepartment = new UserDepartment( ) { Name = "Не задано" };
            Program.DbSingletone.UserDepartments.InsertOnSubmit( EmptyUserDepartment );
            Program.DbSingletone.UserDepartments.DeleteOnSubmit( EmptyUserDepartment );
        }
    }

    public static class Operations
    {
        public static Operation Send = Program.DbSingletone.Operations.SingleOrDefault( operation => operation.Name == "Отправить" );
        public static Operation Return = Program.DbSingletone.Operations.SingleOrDefault( operation => operation.Name == "Возврат" );
        public static Operation Save = Program.DbSingletone.Operations.SingleOrDefault( operation => operation.Name == "Сохранить" );
        public static Operation Delete = Program.DbSingletone.Operations.SingleOrDefault( operation => operation.Name == "Удалить" );
        public static Operation Print = Program.DbSingletone.Operations.SingleOrDefault( operation => operation.Name == "Печать" );
        public static Operation Confirm = Program.DbSingletone.Operations.SingleOrDefault( operation => operation.Name == "Утвердить" );
    }

    public static class TargetsTypes
    {
        public static TargetsType Route = Program.DbSingletone.TargetsTypes.SingleOrDefault( targetsType => targetsType.Name == "Маршрут" );
        public static TargetsType Person = Program.DbSingletone.TargetsTypes.SingleOrDefault( targetsType => targetsType.Name == "Пользователь" );
    }

    public static class States
    {
        public static State Complete = Program.DbSingletone.States.SingleOrDefault( state => state.Name == "Завершено" );

    }


}

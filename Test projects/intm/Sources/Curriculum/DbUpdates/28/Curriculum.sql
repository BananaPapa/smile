﻿if exists(select * from sys.columns 
    where Name = N'OperationDescriptor' and Object_ID = Object_ID(N'Targets'))
begin
    -- Column Exists
	alter table Targets add [OperationDescriptor] [varchar](max)
end

if exists(select * from sys.columns 
    where Name = N'Attachments' and Object_ID = Object_ID(N'DocumentsId'))
begin
    -- Column Exists
	EXEC sp_RENAME 'Attachments.DocumentsId' , 'FileId', 'COLUMN'
end

if exists(select * from sys.columns 
    where Name = N'OperationId' and Object_ID = Object_ID(N'Documents'))
begin
	alter table Documents drop CONSTRAINT [FK_Documents_Operations]
	alter table Documents drop column OperationId
end


if not exists(select * from sys.columns 
    where Name = N'OperationId' and Object_ID = Object_ID(N'Targets'))
begin
	alter table Targets add OperationId [int]
	ALTER TABLE [dbo].[Targets]  WITH CHECK ADD CONSTRAINT [FK_Targets_Operations] FOREIGN KEY([OperationId]) REFERENCES [dbo].[Operations] ([OperationId]) 
end

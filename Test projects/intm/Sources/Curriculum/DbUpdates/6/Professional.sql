﻿IF EXISTS(select * FROM sys.views where name = 'vUnverifiedAssignments')
begin
		drop view [dbo].[vUnverifiedAssignments]
end
go
Create view [dbo].[vUnverifiedAssignments] AS
		Select FirstName, MiddleName,LastName ,sTime as 'start' , eTime as 'end' , s.TestPassingId as 'tpId' , s.TestId as 'tId' 
		from			Statistic		as s 
			INNER JOIN	Tests			as t	on t.TestId = s.testId 
			INNER JOIN	TestPassings	as tp	on tp.TestPassingId = s.TestPassingId
			INNER JOIN	Users			as u	on u.UserId = tp.UserId
 go
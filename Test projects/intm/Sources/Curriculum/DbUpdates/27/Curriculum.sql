﻿IF (NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'FilesUsersGroups'))
begin

Create table FilesUsersGroups  
(
 FilesUsersGroupsId int IDENTITY(1,1) not null,
 FileId int not null,
 UsersGroupId int not null,
   	CONSTRAINT [PK_FilesUsersGroups] PRIMARY KEY CLUSTERED 
([FilesUsersGroupsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

alter table [dbo].FilesUsersGroups with check add constraint [FK_FilesUsersGroups_Files] foreign key ([FileId]) references [dbo].[Files] ([FileId]) ON DELETE CASCADE 	
alter table [dbo].FilesUsersGroups with check add constraint [FK_FilesUsersGroups_UsersGroups] foreign key ([UsersGroupId]) references [dbo].[UsersGroups] ([UsersGroupId]) ON DELETE CASCADE 	

end

﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT (EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'Roles'))
BEGIN

	CREATE TABLE [dbo].[Roles](
		[RoleId] [int] IDENTITY(1,1) NOT NULL,
		[ParentRoleId] [int] ,
		[Name] [nvarchar](255) ,
	 CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED 
	([RoleId] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]

	ALTER TABLE [dbo].[Roles]  WITH CHECK ADD CONSTRAINT [FK_Roles_Roles] FOREIGN KEY([ParentRoleId])
	REFERENCES [dbo].[Roles] ([RoleId])

END

IF NOT (EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'Tags'))
BEGIN

	CREATE TABLE [dbo].[Tags](
		[TagId] [int] IDENTITY(1,1) NOT NULL,
		[Tag] [nvarchar](4000) NOT NULL,
	 CONSTRAINT [PK_Tags] PRIMARY KEY CLUSTERED 
	([TagId] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
END



IF NOT (EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'RolesPermissions'))
BEGIN

	CREATE TABLE [dbo].[RolesPermissions](
		[PermissionId] [int] IDENTITY(1,1) NOT NULL,
		[RoleId] [int] NOT NULL ,
		[TagId] [int] NOT NULL,
		[Permission] [int] NOT NULL,
	 CONSTRAINT [PK_FORMS] PRIMARY KEY CLUSTERED 
	([PermissionId] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]

	ALTER TABLE [dbo].[RolesPermissions]  WITH CHECK ADD CONSTRAINT [FK_RolesPermission_Roles] FOREIGN KEY([RoleId])
	REFERENCES [dbo].[Roles] ([RoleId])

	ALTER TABLE [dbo].[RolesPermissions]  WITH CHECK ADD CONSTRAINT [FK_RolesPermission_Tags] FOREIGN KEY([TagId])
	REFERENCES [dbo].[Tags] ([TagId])

END

IF NOT (EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'UsersRoles'))
BEGIN

	CREATE TABLE [dbo].[UsersRoles](
		[UserId] [int] NOT NULL Unique,
		[RoleId] [int] NOT NULL )
		ON [PRIMARY]

	ALTER TABLE [dbo].[UsersRoles]  WITH CHECK ADD  CONSTRAINT [FK_UsersRoles_Roles] FOREIGN KEY([RoleId])
	REFERENCES [dbo].[Roles] ([RoleId])

	ALTER TABLE [dbo].[UsersRoles]  WITH CHECK ADD  CONSTRAINT [FK_UsersRoles_Users] FOREIGN KEY([UserId])
	REFERENCES [dbo].[Users] ([UserId])

END
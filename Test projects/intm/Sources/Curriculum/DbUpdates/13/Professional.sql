﻿IF EXISTS(select * FROM sys.views where name = 'vUnverifiedAssignments')
begin
		drop view [dbo].[vUnverifiedAssignments]
end
go
Create view [dbo].[vUnverifiedAssignments] AS
		SELECT        s.StatisticId, u.FirstName, u.MiddleName, u.LastName, s.sTime AS Start, s.eTime AS [End], t.Title, s.TestPassingId AS TPId, s.testId AS TId
FROM            dbo.Statistic AS s INNER JOIN
                         dbo.Tests AS t ON t.TestId = s.testId INNER JOIN
                         dbo.TestPassings AS tp ON s.TestPassingId = tp.TestPassingId INNER JOIN
                         dbo.Users AS u ON u.UserId = tp.UserId
where s.Verified = 0 and tp.IsSupervise = 1
			go



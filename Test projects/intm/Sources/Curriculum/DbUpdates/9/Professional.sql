﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF COL_LENGTH('JournalsBranchs','Order') IS NULL
 BEGIN
	alter table [dbo].[JournalsBranchs] add [Order] [int] NOT NULL DEFAULT(0)
 /*Column does not exist or caller does not have permission to view the object*/
 END


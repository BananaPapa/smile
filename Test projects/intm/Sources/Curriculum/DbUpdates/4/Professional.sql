﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT (EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'FillingFormsTestResults'))
BEGIN

CREATE TABLE [dbo].[FillingFormsTestResults](
	[TestPassingId] [int] NOT NULL,
	[FormFillingId] [int] NOT NULL,
) ON [PRIMARY]

ALTER TABLE [dbo].[FillingFormsTestResults]  WITH CHECK ADD  CONSTRAINT [FK_TestPassingsFormFillings_TestPassings] FOREIGN KEY([TestPassingId])
REFERENCES [dbo].[TestPassings] ([TestPassingId])

ALTER TABLE [dbo].[FillingFormsTestResults]  WITH CHECK ADD  CONSTRAINT [FK_TestPassingsFormFillings_Questions] FOREIGN KEY([FormFillingId])
REFERENCES [dbo].[FormFillings] ([FormFillingId])

END


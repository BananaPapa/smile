﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF COL_LENGTH('FillingFormsTestResults','QuestionId') IS NULL
begin
	ALTER TABLE [dbo].[FillingFormsTestResults] add QuestionId int not null;
	ALTER TABLE [dbo].[FillingFormsTestResults] WITH CHECK ADD  CONSTRAINT [FK_FillingFormsTestResults_Questions] FOREIGN KEY([QuestionId]) REFERENCES [dbo].[Questions] ([QuestionId]);
	ALTER TABLE [dbo].[FillingFormsTestResults] ADD CONSTRAINT TestPassingsFormFillings_UNIQUE UNIQUE ([TestPassingId],[FormFillingId],[QuestionId]);
end
﻿
IF COL_LENGTH('Logs','SenderUser') IS NULL
begin 
	alter table [Logs] add [SenderUser] nvarchar(128) not null
end 

IF COL_LENGTH('Logs','ReceiverUser') IS NULL
begin 
	alter table [Logs] add [ReceiverUser] nvarchar(128) not null
end 

IF ( COL_LENGTH('Logs','OperationName') IS NULL)
begin 
	alter table [Logs] add [OperationName] nvarchar(255) not null
end 


if exists (SELECT * FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS 
    WHERE CONSTRAINT_NAME ='FK_Logs_FromUser')
begin
	alter table [Logs] drop constraint [FK_Logs_FromUser]
end

if exists (SELECT * FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS 
    WHERE CONSTRAINT_NAME ='FK_Logs_Operations')
begin
	alter table [Logs] drop constraint [FK_Logs_Operations]
end

if exists (SELECT * FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS 
    WHERE CONSTRAINT_NAME ='FK_Logs_ToUser')
begin
	alter table [Logs] drop constraint [FK_Logs_ToUser]
end



IF COL_LENGTH('Logs','FromUserId') IS NOT NULL
begin 
	alter table [Logs] drop column [FromUserId]
end 

IF COL_LENGTH('Logs','ToUserId') IS NOT NULL
begin 
	alter table [Logs] drop column [ToUserId]
end 

IF COL_LENGTH('Logs','OperationId') IS NOT NULL
begin 
	alter table [Logs] drop column [OperationId]
end 


if exists (SELECT * FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS 
    WHERE CONSTRAINT_NAME ='FK_Logs_FromUser')
begin
	alter table [Logs] drop constraint [FK_Logs_FromUser]
end

if exists (SELECT * FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS 
    WHERE CONSTRAINT_NAME ='FK_Logs_Operations')
begin
	alter table [Logs] drop constraint [FK_Logs_Operations]
end

if exists (SELECT * FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS 
    WHERE CONSTRAINT_NAME ='FK_Logs_ToUser')
begin
	alter table [Logs] drop constraint [FK_Logs_ToUser]
end



IF COL_LENGTH('Logs','FromUserId') IS NOT NULL
begin 
	alter table [Logs] drop column [FromUserId]
end 

IF COL_LENGTH('Logs','ToUserId') IS NOT NULL
begin 
	alter table [Logs] drop column [ToUserId]
end 

IF COL_LENGTH('Logs','OperationId') IS NOT NULL
begin 
	alter table [Logs] drop column [OperationId]
end 

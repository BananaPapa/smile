﻿IF EXISTS(select * FROM sys.views where name = 'vUnverifiedAssignments')
begin
		drop view [dbo].[vUnverifiedAssignments]
end
go
Create view [dbo].[vUnverifiedAssignments] AS
		Select StatisticId ,FirstName , MiddleName,LastName ,sTime as 'Start' , eTime as 'End', t.Title as 'Title' , s.TestPassingId as 'TPId' , s.TestId as 'TId' from 
			Statistic as s 
			INNER JOIN Tests as t on t.TestId = s.testId 
			INNER JOIN TestPassings as tp on s.TestPassingId = tp.TestPassingId
			INNER JOIN Users as u on u.UserId = tp.UserId
			go



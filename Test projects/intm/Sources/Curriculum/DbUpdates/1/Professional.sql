﻿IF NOT EXISTS(SELECT [Value] FROM [SystemProperties] WHERE EnumId = 3)
    BEGIN
        INSERT INTO SystemProperties(EnumID, Value)
        VALUES(3, '0')
    END
    ELSE
    BEGIN
        UPDATE SystemProperties
        SET Value = '0'
        WHERE EnumId=3
    END
IF NOT EXISTS(SELECT [Value] FROM [SystemProperties] WHERE EnumId = 4)
    BEGIN
	INSERT INTO SystemProperties(EnumID, Value) VALUES(4, '<?xml version="1.0"?>
<ArrayOfRepositoryConnectionSetting xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <RepositoryConnectionSetting xsi:type="FtpConnectionSettings">
    <Host>192.168.17.208</Host>
    <Port>21</Port>
    <UrlPath>/UpdateIntegrator</UrlPath>
    <Login>dimadiv</Login>
    <Password>1</Password>
  </RepositoryConnectionSetting>
</ArrayOfRepositoryConnectionSetting>')
	END
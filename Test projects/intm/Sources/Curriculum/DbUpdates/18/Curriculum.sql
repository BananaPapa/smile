﻿IF COL_LENGTH('[UsersRoles]','UserRoleId') IS NULL

BEGIN

	ALTER TABLE [dbo].[UsersRoles] ADD UserRoleId INT IDENTITY

	ALTER TABLE [dbo].[UsersRoles] ADD CONSTRAINT PK_UserRole PRIMARY KEY(UserRoleId)

END
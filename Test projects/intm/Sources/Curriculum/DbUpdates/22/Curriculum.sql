﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF (NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'Operations'))
begin
CREATE TABLE [dbo].[Operations]
(
[OperationId] [int] IDENTITY(1,1) NOT NULL,
[Name] [nvarchar](255),
CONSTRAINT  [PK_Operations] PRIMARY KEY CLUSTERED 
	(OperationId ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
end

IF (NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'States'))
begin
CREATE TABLE [dbo].[States]
(
[StateId] [int] IDENTITY(1,1) NOT NULL,
[Name] [nvarchar](255) NOT NULL,
CONSTRAINT [PK_States] PRIMARY KEY CLUSTERED
	(StateId ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
end

IF (NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'TargetsTypes'))
begin
CREATE TABLE [dbo].[TargetsTypes]
(
[TargetsTypeId] [int] IDENTITY(1,1) NOT NULL,
[Name] [nvarchar](255) NOT NULL,
CONSTRAINT [PK_TargetsTypes] PRIMARY KEY CLUSTERED
	(TargetsTypeId ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
end

IF (NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'Documents'))
begin
CREATE TABLE [dbo].[Documents]
(
[DocumentId] [int] IDENTITY(1,1) NOT NULL,
[Name] [nvarchar](255),
[FileName] [nvarchar](255),
[Content] [image] NOT NULL,
[Owner] [int] NOT NULL,
CONSTRAINT [PK_Documents] PRIMARY KEY CLUSTERED
	(DocumentId ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
ALTER TABLE [dbo].[Documents]  WITH CHECK ADD CONSTRAINT [FK_Documents_Users] FOREIGN KEY([Owner])  REFERENCES [dbo].[Users] ([UserId]) ON DELETE CASCADE
end

IF (NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'Targets'))
begin
Create Table [dbo].[Targets]
([TargetId] [int] IDENTITY(1,1) NOT NULL,
[TargetsTypeId] [int] NOT NULL,
[TargetObjectId] [int] NOT NULL,
[IsDone] [int] NOT NULL
CONSTRAINT [PK_Targets] PRIMARY KEY CLUSTERED
	(TargetId ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
ALTER TABLE [dbo].[Targets]  WITH CHECK ADD CONSTRAINT [FK_Targets_TargetsTypes] FOREIGN KEY([TargetsTypeId])  REFERENCES [dbo].[TargetsTypes] ([TargetsTypeId])
end

IF (NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'Messages'))
begin
CREATE TABLE [dbo].[Messages]
(
[MessageId] [int] not null IDENTITY(1,1) ,
[Text] [nvarchar](1000),
[TargetId] [int] not null ,
[OperationId] [int] not null ,
[StateId] [int] not null ,
CONSTRAINT [PK_Messages] PRIMARY KEY CLUSTERED
	(MessageId ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
ALTER TABLE [dbo].[Messages]  WITH CHECK ADD CONSTRAINT [FK_Messages_Operations] FOREIGN KEY([OperationId])  REFERENCES [dbo].[Operations] ([OperationId]) 
ALTER TABLE [dbo].[Messages]  WITH CHECK ADD CONSTRAINT [FK_Messages_Targets] FOREIGN KEY([TargetId])  REFERENCES [dbo].[Targets] ([TargetId]) ON DELETE CASCADE
ALTER TABLE [dbo].[Messages]  WITH CHECK ADD CONSTRAINT [FK_Messages_States] FOREIGN KEY([StateId])  REFERENCES [dbo].[States] ([StateId]) 
end

IF (NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'UserDepartments'))
begin
CREATE TABLE [dbo].[UserDepartments]
(
[UserDepartmentId] [int] not null IDENTITY(1,1) ,
[Name] [nvarchar](255),
CONSTRAINT [PK_UserDepartments] PRIMARY KEY CLUSTERED
	(UserDepartmentId ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
end

IF (NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'UsersDepartmentsUsers'))
begin
CREATE TABLE [dbo].[UsersDepartmentsUsers]
(
[uduId] [int] IDENTITY(1,1) NOT NULL,
[UserId] [int] NOT NULL,
[UserDepartmentId] [int] NOT NULL,
	CONSTRAINT [PK_UsersDepartmentsUsers] PRIMARY KEY CLUSTERED
	(uduId ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
ALTER TABLE [dbo].[UsersDepartmentsUsers]  WITH CHECK ADD CONSTRAINT [FK_UsersDepartmentsUsers_Users] FOREIGN KEY([UserId])  REFERENCES [dbo].[Users] ([UserId]) ON DELETE CASCADE
ALTER TABLE [dbo].[UsersDepartmentsUsers]  WITH CHECK ADD CONSTRAINT [FK_UsersDepartmentsUsers_Departments] FOREIGN KEY([UserDepartmentId])  REFERENCES [dbo].[UserDepartments] ([UserDepartmentId]) ON DELETE CASCADE
end

IF (NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'UserPositions'))
begin
CREATE TABLE [dbo].[UserPositions]
(
[UserPositionId] [int] not null IDENTITY(1,1) ,
[Name] [nvarchar](255),
CONSTRAINT [PK_UserPositions] PRIMARY KEY CLUSTERED
	(UserPositionId ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
end

IF (NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'UserPositionsUsers'))
begin
CREATE TABLE [dbo].[UserPositionsUsers]
(
[upuId] [int] IDENTITY(1,1) NOT NULL,
[UserId] [int] NOT NULL,
[UserPositionId] [int] NOT NULL,
	CONSTRAINT [PK_UserPositionsUsers] PRIMARY KEY CLUSTERED
	(upuId ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
ALTER TABLE [dbo].[UserPositionsUsers]  WITH CHECK ADD CONSTRAINT [FK_UserPositionsUsers_Users] FOREIGN KEY([UserId])  REFERENCES [dbo].[Users] ([UserId]) ON DELETE CASCADE
ALTER TABLE [dbo].[UserPositionsUsers]  WITH CHECK ADD CONSTRAINT [FK_UserPositionsUsers_Positions] FOREIGN KEY([UserPositionId])  REFERENCES [dbo].[UserPositions] ([UserPositionId]) ON DELETE CASCADE
end

IF (NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'Attachments'))
begin
CREATE TABLE [dbo].[Attachments]
(
[AttachmentId] [int] IDENTITY(1,1) NOT NULL,
[MessageId] [int] NOT NULL,
[DocumentId] [int] NOT NULL,
	CONSTRAINT [PK_Attachments] PRIMARY KEY CLUSTERED
	(AttachmentId ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
ALTER TABLE [dbo].[Attachments]  WITH CHECK ADD CONSTRAINT [FK_Attachments_Messages] FOREIGN KEY([MessageId])  REFERENCES [dbo].[Messages] ([MessageId]) ON DELETE CASCADE
ALTER TABLE [dbo].[Attachments]  WITH CHECK ADD CONSTRAINT [FK_Attachments_Documents] FOREIGN KEY([DocumentId])  REFERENCES [dbo].[Documents] ([DocumentId]) ON DELETE CASCADE
end

IF (NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'Substituents'))
begin
CREATE TABLE [dbo].[Substituents]
(
[SubstituentsId] [int] not null IDENTITY(1,1) ,
[UserId] [int] not null,
[SubstituentUserId] [int] not null,
CONSTRAINT [PK_Substituents] PRIMARY KEY CLUSTERED
	(SubstituentsId ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
ALTER TABLE [dbo].[Substituents]  WITH CHECK ADD CONSTRAINT [FK_Substituents_User] FOREIGN KEY([UserId])  REFERENCES [dbo].[Users] ([UserId])
ALTER TABLE [dbo].[Substituents]  WITH CHECK ADD CONSTRAINT [FK_Substituents_SubstituentUser] FOREIGN KEY([SubstituentUserId])  REFERENCES [dbo].[Users] ([UserId])
end

IF (NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'VisionExtenstions'))
begin
CREATE TABLE [dbo].[VisionExtenstions]
(
[VisionExtensionId] [int] not null IDENTITY(1,1) ,
[UserId] [int] not null,
[ExtensionUserId] [int] not null,
CONSTRAINT [PK_Departaments] PRIMARY KEY CLUSTERED
	(VisionExtensionId ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
ALTER TABLE [dbo].[VisionExtenstions]  WITH CHECK ADD CONSTRAINT [FK_VisionExtenstions_User] FOREIGN KEY([UserId])  REFERENCES [dbo].[Users] ([UserId])
ALTER TABLE [dbo].[VisionExtenstions]  WITH CHECK ADD CONSTRAINT [FK_VisionExtenstions_ExtensionUser] FOREIGN KEY([ExtensionUserId])  REFERENCES [dbo].[Users] ([UserId])
end

IF (NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'Logs'))
begin
CREATE TABLE [dbo].[Logs]
(
[LogId] [int] not null IDENTITY(1,1) ,
[EventDate] [date] not null,
[FromUserId] [int],
[ToUserId] [int],
[Message] [varchar](1000),
[OperationId] [int],
[DocumentName] [int],
CONSTRAINT [PK_Logs] PRIMARY KEY CLUSTERED
	(LogId ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
ALTER TABLE [dbo].[Logs]  WITH CHECK ADD CONSTRAINT [FK_Logs_FromUser] FOREIGN KEY([FromUserId])  REFERENCES [dbo].[Users] ([UserId])
ALTER TABLE [dbo].[Logs]  WITH CHECK ADD CONSTRAINT [FK_Logs_ToUser] FOREIGN KEY([ToUserId])  REFERENCES [dbo].[Users] ([UserId]) 
ALTER TABLE [dbo].[Logs]  WITH CHECK ADD CONSTRAINT [FK_Logs_Operations] FOREIGN KEY([OperationId])  REFERENCES [dbo].[Operations] ([OperationId]) ON DELETE SET NULL
end


﻿IF COL_LENGTH('[Roles]','Admin') IS NULL
begin
	ALter Table [Roles] Add [Admin] bit default(0) Not null 
end
go

DECLARE @adminId AS int
DECLARE @roleId AS int

if Not (Exists (Select * from [Users] where Name = 'Администратор'))
begin
	INSERT [dbo].[Users] ([Name], [PasswordHash]) VALUES (N'Администратор', 0x356A192B7913B04C54574D18C28D46E6395428AB)
	SET @adminId = @@IDENTITY
end
else
begin
	SET @adminId = (Select UserId from Users where name = 'Администратор')
end

if Not (Exists (Select * from [ROLES] where Name = 'Администратор'))
begin
	insert into [dbo].[Roles] ([Name],[Admin]) values ('Администратор','1')
	SET @roleId = @@IDENTITY
end
else
begin
	SET @roleId = (select roleId from [Roles] where Name='Администратор')
end
IF Not ( Exists (Select * from UsersRoles where userId = @adminId ) )
begin
	insert into UsersRoles (UserId,RoleId) values (@adminId,@roleId)
end
go

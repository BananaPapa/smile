﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF COL_LENGTH('Statistic','Verified') IS NULL
 BEGIN
	alter table [dbo].[Statistic] add [Verified] [bit] not null DEFAULT(1) 
	
 /*Column does not exist or caller does not have permission to view the object*/
 END


IF EXISTS(select * FROM sys.views where name = 'vStatistic')
begin
		drop view [dbo].[vStatistic]
end
go
Create view [dbo].[vStatistic] AS
		SELECT     s.StatisticId, s.TestPassingId, s.UserId, s.UserUniqueId, s.sTime, s.eTime, s.IsSupervise, s.MaxEff, s.TimeDuration, s.Result, s.Perc, s.Valuation, s.testId, 
                      s.Speed,s.ErrorCount, s.[Description], s.[Verified], t.Title, t.PassingScore, tp.ProfilePassingId, t.UniqueId AS TestUniqueId, dbo.TestTypes.PlugInFileName
FROM         dbo.Statistic AS s INNER JOIN
                      dbo.Tests AS t ON s.testId = t.TestId INNER JOIN
                      dbo.TestPassings AS tp ON tp.TestPassingId = s.TestPassingId INNER JOIN
                      dbo.TestTypes ON t.TypeId = dbo.TestTypes.TypeId

 go
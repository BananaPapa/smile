﻿
IF (EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'FillingFormsTestResults'))
BEGIN
	ALTER TABLE FillingFormsTestResults
	ALTER COLUMN FormFillingId int NULL
END

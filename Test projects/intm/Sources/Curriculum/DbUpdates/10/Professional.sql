﻿IF NOT (EXISTS (SELECT * 
                 FROM [dbo].[TestTypes] 
                 WHERE PlugInFileName = 'FillingFormTest'))
BEGIN
	Insert into TestTypes ([Title],[PlugInFileName],[IsAvailable])
	values ('Заполнение форм','FillingFormTest',1)
END
﻿if exists (SELECT
    * 
    FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS 
    WHERE CONSTRAINT_NAME ='UC_Unique_Permission')
begin
	alter table [RolesPermissions] drop constraint [UC_Unique_Permission]
end

alter table [RolesPermissions] add constraint [UC_Unique_Permission] unique ([RoleId],[TagId])

IF (NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'Routes'))
	begin
		Create table Routes
		(
			[RouteId] [int] IDENTITY(1,1) NOT NULL,
			[RouteName] varchar(255) NOT NULL,
			[DynamicFlag] bit NOT NULL default(0),
			CONSTRAINT [PK_Routes] PRIMARY KEY CLUSTERED 
			([RouteId] ASC )
			WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) 
		ON [PRIMARY]
	end

if( not exists (select * from information_schema.tables where table_schema = 'dbo' and table_name = 'RoutesSteps'))
begin
	Create table RoutesSteps
	(
		RouteStepId [int] Identity(1,1) not null,
		RouteId [int] not null,
		TargetId [int] not null,
		[Order] [int] not null,
		constraint [PK_RoutesSteps] primary key  clustered ([RouteStepId] ASC)
		with (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [Primary]

	alter table [dbo].[RoutesSteps] with check add constraint  [FK_RoutesSteps_Routes] foreign key ([RouteId]) references [dbo].[Routes] (RouteId)
	alter table [dbo].[RoutesSteps] with check add constraint  [FK_RoutesSteps_Targets] foreign key ([TargetId]) references [dbo].[Targets] (TargetId)
end

if( not exists (select * from information_schema.tables where table_schema = 'dbo' and table_name = 'RoutesVisions'))
begin
	Create table RoutesVisions
	(
		RouteVisionId [int] Identity(1,1) not null,
		RouteId [int] not null,
		UsersGroupId [int] not null,
		constraint [PK_RoutesVisions] primary key  clustered ([RouteVisionId] ASC)
		with (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [Primary]

	alter table [dbo].[RoutesVisions] with check add constraint  [FK_RoutesVisions_Routes] foreign key ([RouteId]) references [dbo].[Routes] (RouteId)
	alter table [dbo].[RoutesVisions] with check add constraint  [FK_RoutesVisions_UsersGroups] foreign key ([UsersGroupId]) references [dbo].[UsersGroups] (UsersGroupId)
end

if( not exists (select * from information_schema.tables where table_schema = 'dbo' and table_name = 'TargetsOperations'))
begin
	Create table TargetsOperations
	(
		TargetOperationsId [int] Identity(1,1) not null,
		TargetId [int] not null,
		OperationId [int] not null,
		constraint [PK_TargetsOperations] primary key  clustered (TargetOperationsId ASC)
		with (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [Primary]

	alter table [dbo].[TargetsOperations] with check add constraint  [FK_TargetsOperations_Targets] foreign key ([TargetId]) references [dbo].[Targets] (TargetId)
	alter table [dbo].[TargetsOperations] with check add constraint  [FK_TargetsOperations_Operations] foreign key ([OperationId]) references [dbo].[Operations] (OperationId)
end

IF COL_LENGTH('Documents','Metadata') IS NULL
begin 
	alter table [Documents] add [Metadata] varchar(4000)
end 

IF COL_LENGTH('Documents','RoutePosition') IS NULL
begin 
	alter table [Documents] add [RoutePosition] int not null
end 
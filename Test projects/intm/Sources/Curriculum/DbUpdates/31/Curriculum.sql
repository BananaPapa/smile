﻿/* состояния */
if(not exists (select * from [States] where Name = 'Создано'))
	begin 
		insert into [States] (Name) values ('Создано')
	end

if(not exists (select * from [States] where Name = 'В процессе'))
	begin
		insert into [States] (Name) values ('В процессе')
	end

if(not exists (select * from [States] where Name = 'Завершено'))
	begin
		insert into [States] (Name) values ('Завершено')
	end

/*Операции*/
if(not exists (select * from [Operations] where Name = 'Отправить'))
	begin 
		insert into [Operations] (Name) values ('Отправить')
	end

if(not exists (select * from [Operations] where Name = 'Печать'))
	begin 
		insert into [Operations] (Name) values ('Печать')
	end

if(not exists (select * from [Operations] where Name = 'Возврат'))
	begin 
		insert into [Operations] (Name) values ('Возврат')
	end

if(not exists (select * from [Operations] where Name = 'Утвердить'))
	begin 
		insert into [Operations] (Name) values ('Утвердить')
	end

if(not exists (select * from [Operations] where Name = 'Удалить'))
	begin 
		insert into [Operations] (Name) values ('Удалить')
	end

if(not exists (select * from [Operations] where Name = 'Сохранить'))
	begin 
		insert into [Operations] (Name) values ('Сохранить')
	end

/*Типы назночений*/
if(not exists (select * from [TargetsTypes] where Name = 'Пользователь'))
	begin 
		insert into [TargetsTypes] (Name) values ('Пользователь')
	end

if(not exists (select * from [TargetsTypes] where Name = 'Маршрут'))
	begin 
		insert into [TargetsTypes] (Name) values ('Маршрут')
	end


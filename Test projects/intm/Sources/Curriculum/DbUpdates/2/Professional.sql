﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF (EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'Standarts'))
BEGIN
	drop table [dbo].[Standarts]
END

IF NOT (EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'Forms'))
BEGIN

CREATE TABLE [dbo].[Forms](
	[FormId] [bigint] IDENTITY(1,1) NOT NULL,
	[FormType] [int] ,
	[FormName] [nvarchar](255) ,
	[Content] [image] NOT NULL,
 CONSTRAINT [PK_FORMS] PRIMARY KEY CLUSTERED 
([FormId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

END

IF NOT (EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'Comments'))
BEGIN

CREATE TABLE [dbo].[Comments](
	[CommentId] [int] IDENTITY(1,1) NOT NULL,
	[StartPosition] [int] NOT NULL,
	[EndPosition] [int] NOT NULL,
	[CommentType] [int] NOT NULL,
	[FormId] [bigint] NOT NULL,
	[Comment] [varchar](max) NOT NULL
 CONSTRAINT [PK_COMMENTS] PRIMARY KEY CLUSTERED 
([CommentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[Comments]  WITH CHECK ADD CONSTRAINT [FK_Comment_Forms] FOREIGN KEY([FormId])
REFERENCES [dbo].[Forms] ([FormId])

END

IF NOT (EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'FormFillings'))
BEGIN

CREATE TABLE [dbo].[FormFillings](
	[FormFillingId] [int] IDENTITY(1,1) NOT NULL,
	[FormId] [bigint] NOT NULL,
	[CompletedFormId] [bigint],
 CONSTRAINT [PK_FormFillings] PRIMARY KEY CLUSTERED 
([FormFillingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[FormFillings]  WITH CHECK ADD  CONSTRAINT [FK_FormFillings_Forms_Form] FOREIGN KEY([FormId])
REFERENCES [dbo].[Forms] ([FormId])

ALTER TABLE [dbo].[FormFillings]  WITH CHECK ADD  CONSTRAINT [FK_FormFillings_Forms_CompletedForm] FOREIGN KEY([CompletedFormId])
REFERENCES [dbo].[Forms] ([FormId])

END

IF NOT (EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'Standarts'))
BEGIN

CREATE TABLE [dbo].[Standarts](
	[QuestionId] [int] NOT NULL,
	[FormFillingId] [int] NOT NULL,
)  ON [PRIMARY]

ALTER TABLE [dbo].[Standarts]  WITH CHECK ADD  CONSTRAINT [FK_Standarts_FormFilling] FOREIGN KEY([FormFillingId])
REFERENCES [dbo].[FormFillings] ([FormFillingId])

ALTER TABLE [dbo].[Standarts]  WITH CHECK ADD  CONSTRAINT [FK_Standarts_Questions] FOREIGN KEY([QuestionId])
REFERENCES [dbo].[Questions] ([QuestionId])

END

IF NOT (EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'Journals'))
BEGIN

CREATE TABLE [dbo].[Journals](
	[JournalId] [INT] IDENTITY(1,1) NOT NULL,
	[JournalName] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_Journals] PRIMARY KEY CLUSTERED 
([JournalId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

END


IF NOT (EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'JournalsBranchs'))
BEGIN

CREATE TABLE [dbo].[JournalsBranchs](
	[JournalsBranchId] [INT] IDENTITY(1,1) NOT NULL,
	[JournalId] [int] NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[ParentId] [int],
	[FormId] [bigint] ,
 CONSTRAINT [PK_JournalsBranchs] PRIMARY KEY CLUSTERED 
([JournalsBranchId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[JournalsBranchs]  WITH CHECK ADD CONSTRAINT [FK_JournalsBranchs_Forms] FOREIGN KEY([FormId])
REFERENCES [dbo].[Forms] ([FormId])

ALTER TABLE [dbo].[JournalsBranchs]  WITH CHECK ADD CONSTRAINT [FK_JournalsBranchs_JournalBranchs] FOREIGN KEY([ParentId])
REFERENCES [dbo].[JournalsBranchs] ([JournalsBranchId])

ALTER TABLE [dbo].[JournalsBranchs]  WITH CHECK ADD CONSTRAINT [FK_JournalsBranchs_Journals] FOREIGN KEY([JournalId])
REFERENCES [dbo].[Journals] ([JournalId])

END

IF NOT (EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'JournalsViews'))
BEGIN

CREATE TABLE [dbo].[JournalsViews](
	[JournalViewId] [int]  IDENTITY(1,1) NOT NULL,
	[JournalId] [int] NOT NULL,
	[FormId] [bigint] NOT NULL,
	[Default] [bit] NOT NULL,
CONSTRAINT PK_JournalsViews PRIMARY KEY CLUSTERED 
([JournalViewId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[JournalsViews] ADD CONSTRAINT ucCodes UNIQUE (JournalId, FormId)

ALTER TABLE [dbo].[JournalsViews] WITH CHECK ADD CONSTRAINT [FK_JournalsViews_Forms] FOREIGN KEY([FormId])
REFERENCES [dbo].[Forms] ([FormId])

ALTER TABLE [dbo].[JournalsViews] WITH CHECK ADD CONSTRAINT [FK_JournalsViews_Journals] FOREIGN KEY([JournalId])
REFERENCES [dbo].[Journals] ([JournalId])

END
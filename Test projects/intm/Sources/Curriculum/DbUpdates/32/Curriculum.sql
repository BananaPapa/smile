﻿
if exists (SELECT * 
    FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS 
    WHERE CONSTRAINT_NAME ='FK_Documents_Targets')
begin
	    alter table documents drop constraint [FK_Documents_Targets]
end

if exists(select * from sys.columns 
    where Name = N'TargetId' and Object_ID = Object_ID(N'Documents'))
begin
	exec sp_rename 'Documents.TargetId' , 'RouteId', 'COLUMN'
end
go

if not exists (SELECT * 
    FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS 
    WHERE CONSTRAINT_NAME ='FK_Documents_Routes')
begin
	ALTER TABLE [dbo].[Documents]  WITH CHECK ADD CONSTRAINT [FK_Documents_Routes] FOREIGN KEY([RouteId])  REFERENCES [dbo].[Routes] ([RouteId]) 
end
﻿
ALTER TABLE [dbo].[RolesPermissions]  drop CONSTRAINT [FK_RolesPermission_Roles]
ALTER TABLE [dbo].[RolesPermissions]  WITH CHECK ADD CONSTRAINT [FK_RolesPermission_Roles] FOREIGN KEY([RoleId])REFERENCES [dbo].[Roles] ([RoleId]) on delete cascade 
ALTER TABLE [dbo].[UsersRoles]  drop CONSTRAINT [FK_UsersRoles_Roles]
ALTER TABLE [dbo].[UsersRoles]  WITH CHECK ADD  CONSTRAINT [FK_UsersRoles_Roles] FOREIGN KEY([RoleId]) REFERENCES [dbo].[Roles] ([RoleId]) on delete cascade
ALTER TABLE [dbo].[UsersRoles]  drop CONSTRAINT [FK_UsersRoles_Users]
ALTER TABLE [dbo].[UsersRoles]  WITH CHECK ADD  CONSTRAINT [FK_UsersRoles_Users] FOREIGN KEY([UserId]) REFERENCES [dbo].[Users] ([UserId]) on delete cascade

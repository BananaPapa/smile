﻿USE [Professional]
GO
/****** Object:  StoredProcedure [dbo].[ClearDataBase]    Script Date: 21.06.2014 21:04:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF COL_LENGTH('FillingFormsTestResults','ResultId') IS NULL
begin
ALTER TABLE dbo.FillingFormsTestResults
   ADD ResultId INT IDENTITY(1,1)

ALTER TABLE dbo.FillingFormsTestResults
   ADD CONSTRAINT PK_FillingFormsTestResultId
   PRIMARY KEY(ResultId)
end

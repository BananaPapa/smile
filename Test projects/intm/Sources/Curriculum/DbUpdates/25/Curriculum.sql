﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF (NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'UsersGroups'))
begin
Create table [dbo].[UsersGroups]
(
	[UsersGroupId] [int] IDENTITY(1,1) NOT NULL,
	[UserGroupName] [varchar](500) NOT NULL,
	CONSTRAINT [PK_UsersGroups] PRIMARY KEY CLUSTERED 
([UsersGroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
end

IF (NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'UsersGroupsUsers'))
begin
Create table [dbo].[UsersGroupsUsers]
(
	[UsersGroupsUsersId] [int] IDENTITY(1,1) NOT NULL,
	[UsersGroupId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	CONSTRAINT [PK_UsersGroupsUsers] PRIMARY KEY CLUSTERED 
([UsersGroupsUsersId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
ALTER TABLE [dbo].[UsersGroupsUsers]  WITH CHECK ADD CONSTRAINT [FK_UsersGroupsUsers_Users] FOREIGN KEY([UserId]) REFERENCES [dbo].[Users] ([UserId]) ON DELETE CASCADE 	
ALTER TABLE [dbo].[UsersGroupsUsers]  WITH CHECK ADD CONSTRAINT [FK_UsersGroupsUsers_UsersGroups] FOREIGN KEY([UsersGroupId]) REFERENCES [dbo].[UsersGroups] ([UsersGroupId]) ON DELETE CASCADE
end

IF (NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'UsersVisions'))
begin
Create table [dbo].[UsersVisions]
(
	[UserVisionId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[UsersGroupId] [int] NOT NULL,
	CONSTRAINT [PK_UsersVisions] PRIMARY KEY CLUSTERED 
([UserVisionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]	
ALTER TABLE [dbo].[UsersVisions]  WITH CHECK ADD CONSTRAINT [FK_UsersVisions_Users] FOREIGN KEY([UserId]) REFERENCES [dbo].[Users] ([UserId]) ON DELETE CASCADE 
ALTER TABLE [dbo].[UsersVisions]  WITH CHECK ADD CONSTRAINT [FK_UsersVisions_UsersGroups] FOREIGN KEY([UsersGroupId]) REFERENCES [dbo].[UsersGroups] ([UsersGroupId]) ON DELETE CASCADE 

end

IF( EXISTS ( select * from sys.triggers where name = 'InsteadOfDeleteUser'))
   DROP TRIGGER InsteadOfDeleteUser;
GO
CREATE TRIGGER InsteadOfDeleteUser
ON [dbo].[Users]
INSTEAD OF DELETE  as
	DELETE FROM [Messages] WHERE SenderUserId in (Select UserId from deleted)
	DELETE FROM [Messages] WHERE ReceiverUserId in (Select UserId from deleted)
	delete from [Substituents] where UserId in (Select UserId from deleted)
	delete from [Substituents] where SubstituentsId in (Select UserId from deleted)
	delete from [Users] where UserId in (Select UserId from deleted )
GO
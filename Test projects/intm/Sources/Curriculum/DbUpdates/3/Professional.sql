﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT (EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'QuestionJournals'))
BEGIN

CREATE TABLE [dbo].[QuestionJournals](
	[QuestionJournalId] [int] IDENTITY(1,1) NOT NULL,
	[QuestionId] [int] NOT NULL,
	[JournalId] [int] NOT NULL,
	[JournalViewId] [int],
CONSTRAINT [PK_QuestionJournals] PRIMARY KEY CLUSTERED 
(
	[QuestionJournalId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[QuestionJournals]  WITH CHECK ADD  CONSTRAINT [FK_QuestionJournals_Journals] FOREIGN KEY([JournalId])
REFERENCES [dbo].[Journals] ([JournalId])

ALTER TABLE [dbo].[QuestionJournals]  WITH CHECK ADD  CONSTRAINT [FK_QuestionJournals_JournalView] FOREIGN KEY([JournalViewId])
REFERENCES [dbo].[JournalsViews] ([JournalViewId])

ALTER TABLE [dbo].[QuestionJournals]  WITH CHECK ADD  CONSTRAINT [FK_QuestionJournals_Questions] FOREIGN KEY([QuestionId])
REFERENCES [dbo].[Questions] ([QuestionId])

END
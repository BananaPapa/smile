﻿USE [Professional]
GO
/****** Object:  StoredProcedure [dbo].[ClearDataBase]    Script Date: 21.06.2014 21:04:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
ALTER procedure [dbo].[ClearDataBase]  as 
begin 
delete from FormFillings
where 
FormFillingId not in (select FormFillingId from FillingFormsTestResults) 
and FormFillingId not in (select FormFillingId from Standarts) 
delete from QuestionJournals
where 
questionid not in (select questionid from testquestions) 
and questionid not in (select questionid from controlquestions) 
delete from Standarts
where 
questionid not in (select questionid from testquestions) 
and questionid not in (select questionid from controlquestions) 
delete from questionanswers 
where 
questionid not in (select questionid from testquestions) 
and questionid not in (select questionid from controlquestions) 
delete from answers 
where Answerid not in(select answerid from questionanswers) 
and answerid not in (select answerid from testanswers) 
and answerid not in (select rightanswerid from controlquestions) 
delete from questions 
where 
QuestionId not in(select QuestionId from  questionAnswers) 
and QuestionId not in(select QuestionId from  testquestions) 
and QuestionId not in(select QuestionId from  controlquestions) 
delete from timeintervals 
where 
TimeIntervalId not in(select TimeIntervalId from testresults) 
and TimeIntervalId not in (select TimeIntervalid from testPassings) 
and TimeIntervalId not in (select TimeIntervalid from profilePassings) 
delete from contents 
where 
ContentId not in(select InstructionContentId from Methods) 
and Contentid not in (select ContentId from answers) 
and ContentId not in (select ContentId from Questions) 
and contentid not in (select DescriptionContentId from tests) 
and ContentId not in (select CommentContentId from ControlQuestions) 
end 
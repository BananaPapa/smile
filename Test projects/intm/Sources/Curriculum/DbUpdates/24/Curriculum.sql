﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF (EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_CATALOG = 'Curriculum' 
                 AND  TABLE_NAME = 'Documents') 
	AND NOT 
	EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_CATALOG = 'Curriculum' 
                 AND  TABLE_NAME = 'Files') )
BEGIN
	
	ALTER TABLE [dbo].[Attachments] DROP CONSTRAINT [FK_Attachments_Documents]
	
	ALTER TABLE [dbo].[Documents]  DROP CONSTRAINT [FK_Documents_Users]
	ALTER TABLE [dbo].[Documents]  DROP CONSTRAINT [PK_Documents]

	EXEC sp_rename 'Documents', 'Files'
	EXEC sp_rename 'Files.DocumentId', 'FileId','COLUMN'
	
	ALTER TABLE Files ADD CONSTRAINT PK_Files PRIMARY KEY (FileId)
	ALTER TABLE [dbo].[Files]  WITH CHECK ADD CONSTRAINT [FK_Files_Users] FOREIGN KEY([Owner])  REFERENCES [dbo].[Users] ([UserId]) ON DELETE CASCADE

	ALTER TABLE [dbo].[Attachments]  WITH CHECK ADD CONSTRAINT [FK_Attachments_Files] FOREIGN KEY([DocumentId])  REFERENCES [dbo].[Files] ([FileId])
	
END 
ELSE 
	PRINT 'fail rename documents'
go
IF (EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_CATALOG = 'Curriculum' 
                 AND  TABLE_NAME = 'Messages') 
	AND NOT 
	EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_CATALOG = 'Curriculum' 
                 AND  TABLE_NAME = 'Documents') )
BEGIN
	ALTER TABLE [dbo].[Attachments]  DROP CONSTRAINT [FK_Attachments_Messages]

	ALTER TABLE [dbo].[Messages]  DROP CONSTRAINT [FK_Messages_Operations]
	ALTER TABLE [dbo].[Messages]  DROP CONSTRAINT [FK_Messages_Targets]
	ALTER TABLE [dbo].[Messages]  DROP CONSTRAINT [FK_Messages_States]
	ALTER TABLE [dbo].[Messages]  DROP CONSTRAINT [PK_Messages]

	EXEC sp_rename 'Messages', 'Documents'
	EXEC sp_rename 'Documents.MessageId', 'DocumentId','COLUMN'

	ALTER TABLE [dbo].[Documents] ADD CONSTRAINT PK_Documents PRIMARY KEY ([DocumentId])
	ALTER TABLE [dbo].[Documents]  WITH CHECK ADD CONSTRAINT [FK_Documents_Operations] FOREIGN KEY([OperationId])  REFERENCES [dbo].[Operations] ([OperationId]) 
	ALTER TABLE [dbo].[Documents]  WITH CHECK ADD CONSTRAINT [FK_Documents_Targets] FOREIGN KEY([TargetId])  REFERENCES [dbo].[Targets] ([TargetId]) ON DELETE CASCADE
	ALTER TABLE [dbo].[Documents]  WITH CHECK ADD CONSTRAINT [FK_Documents_States] FOREIGN KEY([StateId])  REFERENCES [dbo].[States] ([StateId]) 
	
	ALTER TABLE [dbo].[Attachments]  WITH CHECK ADD CONSTRAINT [FK_Attachments_Documents] FOREIGN KEY([MessageId])  REFERENCES [dbo].[Documents] ([DocumentId]) ON DELETE CASCADE

END
ELSE 
	PRINT 'fail rename Messages'
go
IF (NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_CATALOG = 'Curriculum' 
                 AND  TABLE_NAME = 'Messages') )
BEGIN
	
	Create table [dbo].[Messages]
	(
	 [MessageId] [int] IDENTITY(1,1) NOT NULL,
	 [MessageText] [varchar](500) NOT NULL,
	 [SenderUserId] [int] NOT NULL,
	 [ReceiverUserId] [int] NOT NULL,
	 [CreationDate] [datetime] NOT NULL,
	  CONSTRAINT [PK_Messages] PRIMARY KEY CLUSTERED 
	([MessageId] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE [dbo].[Messages]  WITH CHECK ADD CONSTRAINT [FK_Messages_Sender_User] FOREIGN KEY([SenderUserId]) REFERENCES [dbo].[Users] ([UserId])
	ALTER TABLE [dbo].[Messages]  WITH CHECK ADD CONSTRAINT [FK_Messages_Receiver_User] FOREIGN KEY([ReceiverUserId]) REFERENCES [dbo].[Users] ([UserId])

END 
ELSE 
	PRINT 'fail create Messages'
go
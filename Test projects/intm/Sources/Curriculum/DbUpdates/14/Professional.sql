﻿if(EXISTS (SELECT 
    OBJECT_NAME(OBJECT_ID) AS NameofConstraint
        ,SCHEMA_NAME(schema_id) AS SchemaName
        ,OBJECT_NAME(parent_object_id) AS TableName
        ,type_desc AS ConstraintType
    FROM sys.objects
    WHERE type_desc LIKE '%CONSTRAINT'
        AND OBJECT_NAME(OBJECT_ID)='FK_TestPassingsFormFillings_TestPassings') )
		begin
ALTER TABLE [dbo].[FillingFormsTestResults]  DROP CONSTRAINT [FK_TestPassingsFormFillings_TestPassings]
end
ALTER TABLE [dbo].[FillingFormsTestResults]  WITH CHECK ADD  CONSTRAINT [FK_TestPassingsFormFillings_TestPassings] FOREIGN KEY([TestPassingId]) REFERENCES [dbo].[TestPassings] ([TestPassingId]) ON DELETE CASCADE
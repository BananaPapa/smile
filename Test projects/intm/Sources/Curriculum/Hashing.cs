﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Curriculum
{
    /// <summary>
    /// Contains extension methods to compute string hashs and compare them as byte arrays.
    /// </summary>
    internal static class HashingExtensions
    {
        /// <summary>
        /// Computes SHA1 hash for current string.
        /// </summary>
        /// <param name="source">Source string.</param>
        /// <param name="encoding">String encoding.</param>
        /// <returns>Hash as a byte array.</returns>
        /// <exception cref="ArgumentNullException" />
        /// <exception cref="ArgumentException" />
        public static byte[] ComputeSHA1Hash(this string source, Encoding encoding)
        {
            // Check arguments.
            if (source == null)
            {
                throw new ArgumentNullException("source");
            }
            if (source.Length == 0)
            {
                throw new ArgumentException("Length of source must be greater than zerro", "source");
            }
            if (encoding == null)
            {
                throw new ArgumentNullException("encoding");
            }

            var sha1 = new SHA1Managed();

            // Initialize and compute hash.
            try
            {
                sha1.Initialize();
                return sha1.ComputeHash(encoding.GetBytes(source));
            }
            finally
            {
                // Release resources.
                sha1.Clear();
            }
        }

        /// <summary>
        /// Determines whether current array is identical to another array.
        /// </summary>
        /// <param name="array">Current array.</param>
        /// <param name="anotherArray">Another array to compare with.</param>
        /// <returns>true if identical; otherwise, false.</returns>
        public static bool IsIdenticalTo(this byte[] array, byte[] anotherArray)
        {
            // Are they both null.
            if (array == null && anotherArray == null)
            {
                return true;
            }

            // Is any of them null.
            if (array != null && anotherArray == null || array == null)
            {
                return false;
            }

            // Are their lengths equal.
            if (array.Length != anotherArray.Length)
            {
                return false;
            }

            // Is any element of current array not equal to corresponding element of another one.
            return !array.Where((t, i) => t != anotherArray[i]).Any();
        }
    }
}
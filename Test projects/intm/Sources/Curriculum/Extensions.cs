using System;
using System.Collections.Generic;

namespace Curriculum
{
    /// <summary>
    /// System.Collections.Generic.IEnumerable extension methods.
    /// </summary>
    public static class IEnumerableExtensions
    {
        /// <summary>
        /// Performs the specified action on each element of the System.Collections.Generic.IEnumerable.
        /// </summary>
        /// <typeparam name="T">CommentType of element.</typeparam>
        /// <param name="enumerable">Enumerable source.</param>
        /// <param name="action">The delegate to perform on each element.</param>
        public static void ForEach<T>(this IEnumerable<T> enumerable, Action<T> action)
        {
            if (enumerable == null)
            {
                throw new ArgumentNullException("enumerable");
            }

            if (action == null)
            {
                throw new ArgumentNullException("action");
            }

            foreach (T item in enumerable)
            {
                action(item);
            }
        }
    }

    public static class EntityExtensions
    {
        public static List<RolesPermission> GetParentPermissions(this Role role)
        {
            List<RolesPermission> permissions = new List<RolesPermission>();
            var parent = role;
            do
            {
                parent = parent.ParentRole;
                if(parent == null)
                    break;
                permissions.AddRange(parent.RolesPermissions);
            } while (true);
            return permissions;
        }
    }

}
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Curriculum.Reports;
using DevExpress.XtraEditors;
using DevExpress.XtraNavBar;
using Eureca.Integrator.Common;
using Eureca.Integrator.Extensions;

namespace Curriculum
{
    /// <summary>
    /// Represents reports form.
    /// </summary>
    public partial class ReportsForm : FormWithAccessAreas, IAliasesHolder
    {
        /// <summary>
        /// Contains control to navigation bar item associations.
        /// </summary>
        private readonly Dictionary<NavBarItem, Control> _controls;

        /// <summary>
        /// Initializes new instance of reports form.
        /// </summary>
        public ReportsForm()
        {
            InitializeComponent();

            // Init control to navigation bar item associations.
            _controls = new Dictionary<NavBarItem, Control>
                            {
                                {_dailyRoutineNavBarItem, new DailyRoutineControl(){HierarchyParent = this, PrefixTag = _dailyRoutineNavBarItem.GetNavBarMenuPath()}},
                                {_birthDatesNavBarItem, new BirthDatesControl(){HierarchyParent = this, PrefixTag = _dailyRoutineNavBarItem.GetNavBarMenuPath()}},
                                {_teachingConclusionNavBarItem, new TeachingConclusionControl(){HierarchyParent = this, PrefixTag = _dailyRoutineNavBarItem.GetNavBarMenuPath()}},
                                {_workDirectionNavBarItem, new WorkDirectionControl(){HierarchyParent = this, PrefixTag = _dailyRoutineNavBarItem.GetNavBarMenuPath()}}
                            };
            ApplyAliases();
        }

        #region IAliasesHolder Members

        public void ApplyAliases()
        {
            foreach (Control control in _controls.Values)
            {
                if (control is IAliasesHolder)
                {
                    ((IAliasesHolder) control).ApplyAliases();
                }
            }
        }

        #endregion

        /// <summary>
        /// Handles nav bar control link clicked.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void NavBarControl_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            Control control = _controls.ContainsKey(e.Link.Item)
                                  ? _controls[e.Link.Item]
                                  : _hintLabelControl;

            if (!_panelControl.Controls.Contains(control))
            {
                control.Dock = DockStyle.Fill;
                _panelControl.Controls.Add(control);
            }

            control.BringToFront();
        }

        /// <summary>
        /// Opens coming birthdays report.
        /// </summary>
        public void OpenBirthdaysReport()
        {
            NavBarControl_LinkClicked(_navBarControl, new NavBarLinkEventArgs(_birthDatesNavBarItem.Links[0]));
        }

        public override IEnumerable<ISKOCBCTDNBTIDED> GetHiddenChilds( bool withLazy )
        {
            var hiddenControls = new List<ISKOCBCTDNBTIDED>( );
            if (withLazy)
            {
                hiddenControls.AddRange(  _controls.Values.Select(control => new ControlInterfaceImplementation(control)) );
            }
            return hiddenControls;
        }
    }
}
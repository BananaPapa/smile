using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Curriculum.Forms;
using Curriculum.Properties;
using DevExpress.LookAndFeel;
using DevExpress.XtraBars;
using DevExpress.XtraBars.Alerter;
using DevExpress.XtraEditors;
using DevExpress.XtraTabbedMdi;
using Eureca.Integrator.Common;
using Eureca.Integrator.Utility;
using Eureca.Professional.BaseComponents;
using Eureca.Utility.Extensions;
using MessageBox = Eureca.Integrator.Utility.Forms.MessageBox;

namespace Curriculum
{
    /// <summary>
    /// Represents form instance creation method delegate.
    ///  </summary>
    /// <returns>New instance of form.</returns>
    public delegate Form CreateNewFormInstanceDelegate();

    /// <summary>
    /// Enumeration of important events.
    /// </summary>
    [Flags]
    public enum ImportantEvent
    {
        Lesson = 1<<0, // 2^0
        PersonDate = 1<<1, // 2^1
        UserEvent = 1<<2 // (2^2) and so on.
    }

    /// <summary>
    /// Represents main MDI parent form of application.
    /// </summary>
    public partial class StartForm : FormWithAccessAreas, IAliasesHolder,IMessageListener
    {
        /// <summary>
        /// Dictionary of tab page images accessed by form type.
        /// </summary>
        private readonly Dictionary<Type, Bitmap> _tabPageImages = new Dictionary<Type, Bitmap>
                                                        {
                                                            {typeof (ShipsForm), Resources.Ship16},
                                                            {typeof (PersonsForm), Resources.User16},
                                                            {
                                                                typeof (DisciplinesForm),
                                                                Resources.Discipline16
                                                                },
                                                            {typeof (CalendarForm), Resources.Calendar16},
                                                            {typeof (PanelForm), Resources.Panel16},
                                                            {typeof (CoursesForm), Resources.Courses16},
                                                            {typeof (LessonsForm), Resources.Lessons16},
                                                            {typeof (SettingsForm), Resources.Settings16},
                                                            {typeof (ReportsForm), Resources.Reports16},
                                                            {
                                                                typeof (TemplatesForm),
                                                                Resources.Templates16
                                                                },
                                                            {
                                                                typeof (TemplateCoursesForm),
                                                                Resources.Documents16
                                                                },
                                                            {typeof (CrewForm), Resources.People16}
                                                        };

        /// <summary>
        /// Important dates count.
        /// </summary>
        private int _importantDatesCount;

        /// <summary>
        /// Important lessons count.
        /// </summary>
        private int _importantLessonsCount;

        /// <summary>
        /// User events count.
        /// </summary>
        private int _userEventsCount;

        /// <summary>
        /// Initializes new instance of main form.
        /// </summary>
        public StartForm()
        {
            ManualFilter = true;
            InitializeComponent();
            ApplyAliases();
            ApplySkin();
        }

        void TestPassingDispatcher_OnNewTestPassings( object sender, Eureca.Utility.NewTestPassingEventArgs e )
        {
            if(UnverifiendFormShown)
                return;
            int assigmentsCount = e.TestPassingsIds.Count;
            object[] endings = (assigmentsCount%10 == 1 && assigmentsCount%100 != 11)
                ? new object[] {assigmentsCount, "��", ""}
                : new object[] { assigmentsCount, "��", "��" };
            if (!ShowAlertWindow( _alertControl, "�������� ����� ����.", "����� {0} �����������{1} ����{2}.".FormatString( endings ), null, null, Program.TestPassingDispatcher ))
            { 
                //throw new Exception("Failed show alert");
            }
           
        }

        public bool UnverifiendFormShown { get; private set; }
        public void ShowUnverifiedTests( Form owner )
        {
            if (!UnverifiendFormShown)
            {
                UnverifiendFormShown = true;
                foreach (var alertForm in _alertControl.AlertFormList)
                {
                    alertForm.Close();
                }
                RunnerApplication.JournalManager.ShowUnverifiedTests( owner );
                UnverifiendFormShown = false;
            }
        }

        /// <summary>
        /// Gets or sets if extra tabbed MDI manager is enabled.
        /// </summary>
        private bool TabManagerEnabled
        {
            get { return _xtraTabbedMdiManager.MdiParent != null; }
            set
            {
                if (value)
                {
                    if (_xtraTabbedMdiManager.MdiParent == null)
                    {
                        // Set extra tabbed MDI manager mdi parent form.
                        _xtraTabbedMdiManager.MdiParent = this;
                    }

                    _tabsBarButtonItem.Down = true;
                }
                else
                {
                    if (_xtraTabbedMdiManager.MdiParent != null)
                    {
                        // Unset extra tabbed MDI manager mdi parent form.
                        _xtraTabbedMdiManager.MdiParent = null;
                    }

                    _tabsBarButtonItem.Down = false;
                }
            }
        }

        #region IAliasesHolder Members

        /// <summary>
        /// Applies aliases for the current form.
        /// </summary>
        public void ApplyAliases()
        {
            try
            {
                Aliase.ApplyAlias( "Ships", a => _shipsBarButtonItem.Caption = a );
            }
            catch (DbException ex)
            {
                Program.HandleDbException(ex);
            }

            // Apply alieses for all aliases holder among MDI children.
            foreach (Form form in MdiChildren)
            {
                var aliasesHolder = form as IAliasesHolder;

                if (aliasesHolder != null)
                {
                    aliasesHolder.ApplyAliases();
                }
            }
        }

        #endregion

        /// <summary>
        /// Applies skin.
        /// </summary>
        private void ApplySkin()
        {
            if (!String.IsNullOrEmpty(Program.Settings.SkinName))
            {
                UserLookAndFeel.Default.SetSkinStyle(Program.Settings.SkinName);
            }
            else
            {
                UserLookAndFeel.Default.SetSkinStyle(_defaultLookAndFeel.LookAndFeel.SkinName);
                Program.Settings.SkinName = UserLookAndFeel.Default.SkinName;
                Program.Settings.Save();
            }
        }

        /// <summary>
        /// Opens or activates MDI child form.
        /// </summary>
        /// <param name="formType">CommentType of form.</param>
        /// <param name="createNewFormInstanceDelegate">Method returning new form instance to be opened.</param>
        /// <param name="args">Form attribute arguments.</param>
        public Form OpenOrActivateMdiChild( Type formType, CreateNewFormInstanceDelegate createNewFormInstanceDelegate, bool withActivate = true,
                                           params object[] args)
        {
            // Search for already opened form.
            foreach (Form form in MdiChildren)
            {
                // Form with such type is already opened.
                if (form.GetType() == formType)
                {
                    if (form is CoursesForm)
                    {
                        Ship ship;

                        // Check if courses form for such SHIP is already opened.
                        if (args.Length > 0 && (ship = args[0] as Ship) != null)
                        {
                            if (((CoursesForm) form).Ship.ShipId == ship.ShipId)
                            {
                                if(withActivate)
                                    form.Activate();
                                return form;
                            }
                        }
                    }
                    else if (form is TemplateCoursesForm)
                    {
                        Template template;

                        // Check if template courses form for such TEMPLATE is already opened.
                        if (args.Length > 0 && (template = args[0] as Template) != null)
                        {
                            if (((TemplateCoursesForm) form).Template.TemplateId == template.TemplateId)
                            {
                                if(withActivate)
                                    form.Activate();
                                return form;
                            }
                        }
                    }
                    else if (form is LessonsForm)
                    {
                        Person person;

                        // Check if lessons form for such PERSON is already opened.
                        if (args.Length > 0 && (person = args[0] as Person) != null)
                        {
                            if (((LessonsForm) form).Person.PersonId == person.PersonId)
                            {
                                if(withActivate)
                                    form.Activate();
                                return form;
                            }
                        }
                    }
                    else
                    {
                        if (withActivate)
                            ShowMdiChild(form);

                        return form;
                    }
                }
            }

            // Else create new instance of form and open it.
            if (createNewFormInstanceDelegate != null)
            {
                Form newForm = createNewFormInstanceDelegate();

                if (newForm != null)
                {
                    newForm.SizeChanged += delegate(object sender, EventArgs e)
                                               {
                                                   // On MDI child maximized enable tabs.
                                                   if (!TabManagerEnabled &&
                                                       ((Form) sender).WindowState == FormWindowState.Maximized)
                                                   {
                                                       TabManagerEnabled = true;
                                                   }
                                               };
                    newForm.MdiParent = this;
                    Trace.WriteLine(newForm.Tag + ":");
                    var res = newForm as IAliasesHolder;
                    if (res != null)
                        res.ApplyAliases( );
                    var supportAccessAreas = newForm as ISupportAccessAreas;
                    if (supportAccessAreas != null && supportAccessAreas.HierarchyParent == null)
                        supportAccessAreas.HierarchyParent = this;
                    if (withActivate)
                        ShowMdiChild(newForm);
                }

                return newForm;
            }

            return null;
        }

        private static void ShowMdiChild( Form form)
        {
            var supportAccessAreas = form as ISupportAccessAreas;
            if (supportAccessAreas == null || Program.PermissionManager.CheckAllow( supportAccessAreas ))
            {
                if (!form.Visible) form.Show(); 
                else form.Activate();
            }
            else
            {
                MessageBox.ShowExclamation( "������ ��������������", "� ��� ������������ ���� ��� ��������� �����." );
            }
        }

        /// <summary>
        /// Counts today's important events.
        /// <param name="events">Important events to count.</param>
        /// </summary>
        public void CountImportantEvents(ImportantEvent events)
        {
            DateTime today = DateTime.Today;

            try
            {
                if ((events & ImportantEvent.Lesson) > 0)
                {
                    // Count today's important lessons.
                    _importantLessonsCount = Program.DbSingletone.ImportantLessons.Where( importantLesson =>
                                                                               importantLesson.UserId ==
                                                                               Program.CurrentUser.UserId &&
                                                                               importantLesson.Lesson.Datetime.Date ==
                                                                               today
                        ).Count( );
                }

                if ((events & ImportantEvent.PersonDate) > 0)
                {
                    int[] affiliationIds = Setting.GetBirthAffiliationIds();
                    int? exclusionStatusId = Setting.GetExtclusionStatusId();

                    // Today's important dates.
                    //BirthDays 

                    int birthDays = Person.SelectWithBirthdayAtDate(today, exclusionStatusId, affiliationIds).Count();

                    _importantDatesCount = birthDays
                                           +
                                           Program.DbSingletone.Persons.Count(person => person.TeachingStartDate == today)
                                           +
                                           Program.DbSingletone.Persons.Count(person => person.ExaminationDate == today)
                                           +
                                           Program.DbSingletone.Persons.Count(person => person.DepartureDate == today);

                    _birthdaysCountBarStaticItem.Caption =
                        Person.SelectByComingBirthdays(5, exclusionStatusId, affiliationIds).Count().ToString();
                }

                if ((events & ImportantEvent.UserEvent) > 0)
                {
                    _userEventsCount =
                        Program.DbSingletone.Events.Count(e => e.UserId == Program.CurrentUser.UserId &&
                                                     (
                                                         (e.StartDateTime.Date == e.EndDateTime.Date) && today == e.StartDateTime.Date ||
                                                         today >= e.StartDateTime.Date &&
                                                         (e.EndDateTime.TimeOfDay.TotalSeconds == 0
                                                             ? today < e.EndDateTime.Date
                                                             : today <= e.EndDateTime.Date)
                                                         ));
                }
            }
            catch (DbException ex)
            {
                Program.HandleDbException(ex);
            }

            int count = _importantLessonsCount + _importantDatesCount + _userEventsCount;

            _eventsBarStaticItem.Caption = count.ToString();
        }

        /// <summary>
        /// Hides window and shows authorization form.
        /// </summary>
        /// <returns>True if username and password pair is correct; otherwise, false.</returns>
        private bool LockWindow()
        {
            Program.TestPassingDispatcher.OnNewTestPassings -= TestPassingDispatcher_OnNewTestPassings;
            Program.MessageDispatcher.Stop();
            Program.TestPassingDispatcher.Stop();

            // Hide form.
            Hide();
            // Show authorization form.

            do
            {
                var authForm =
                    new AuthForm(Program.CurrentUser ??
                                 Program.DbSingletone.Users.FirstOrDefault(user => user.UserId == RunnerApplication.PreviousUserID));
                DialogResult result = authForm.ShowDialog();

                // If password is valid.
                if (result == DialogResult.OK)
                {
                    var role = Program.DbSingletone.UsersRoles.SingleOrDefault( usersRole => usersRole.UserId == authForm.User.UserId );
                    if (role == null)
                    {
                        XtraMessageBox.Show( "� ��� ������������ ���� ��� ������� ��� \"������������\" .", "������ ���������������", MessageBoxButtons.OK,
                            MessageBoxIcon.Error );
                        continue;
                    }
                    
                    if ((Program.CurrentUser == null || Program.CurrentUser.UserId != authForm.User.UserId))
                    {
                        // Save current user.
                        Program.CurrentUser = authForm.User;
                        RunnerApplication.PreviousUserID = authForm.User.UserId;
                        // Close all forms opened by previous user.
                        foreach (Form form in MdiChildren)
                        {
                            if (form is CalendarForm)
                            {
                                var supportAccessAreas = form as ISupportAccessAreas;
                                if (Program.PermissionManager.CheckAllow(supportAccessAreas))
                                {
                                    ((CalendarForm) form).RefreshCalendar();
                                    Program.PermissionManager.ApplyFilter(supportAccessAreas);
                                }
                                else
                                    form.Close();
                            }
                            else
                            {
                                form.Close();
                            }
                        }

                        Program.PermissionManager.ApplyFilter(Program.StartForm);
                    }

                    if (Program.PermissionManager.AllowUncheckedTestNatification)
                    {
                        Program.TestPassingDispatcher.OnNewTestPassings += TestPassingDispatcher_OnNewTestPassings;
                        Program.TestPassingDispatcher.Start();
                    }

                    if (Program.PermissionManager.AllowMessages)
                    {
                        Program.MessageDispatcher.CurrentUser = Program.CurrentUser;
                        Program.MessageDispatcher.Subscribe(this,1);
                        Program.MessageDispatcher.Start( );
                    }

                    // Count important events.
                    CountImportantEvents(ImportantEvent.Lesson | ImportantEvent.PersonDate | ImportantEvent.UserEvent);

                    // Show main form.
                    Show();

                    return true;
                }
                break;
            } while (true);
            return false;
        }

        /// <summary>
        /// Handles main form load event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void MainForm_Load(object sender, EventArgs e)
        {
            // Lock window showing autorization form.
            if (LockWindow() == false)
            {
                // Close form and application.
                Close();
            }
            else
            {
                //Program.PermissionManager.ApplyFilter(this);
                //_calendarBarButtonItem.PerformClick();
            }

            // Start timer.
            _timeTimer.Start();
        }

        /// <summary>
        /// Handles main form shown event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void MainForm_Shown(object sender, EventArgs e)
        {
            Activate();
        }

        /// <summary>
        /// Handles main form closed event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            _timeTimer.Stop();
        }

        /// <summary>
        /// Handles time timer tick event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void TimeTimer_Tick(object sender, EventArgs e)
        {
            // Get current date and time.
            DateTime now = DateTime.Now;

            // Set formatted date and time parts to corresponding labels.
            _timeBarStaticItem.Caption = now.ToLongTimeString();
            _dateBarStaticItem.Caption = String.Format("{0} ({1})", now.ToLongDateString(), now.ToString("dddd"));
        }

        /// <summary>
        /// Handles xtra tabbed MDI manager page added event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void XtraTabbedMdiManager_PageAdded(object sender, MdiTabPageEventArgs e)
        {
            Type formType = e.Page.MdiChild.GetType();

            // Set tab's image.
            if (_tabPageImages.ContainsKey(formType))
            {
                e.Page.Image = _tabPageImages[formType];
            }
        }

        /// <summary>
        /// Handles cascade bar button item click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void CascadeBarButtonItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Disable tabs.
            TabManagerEnabled = false;

            // Cascade MDI windows.
            LayoutMdi(MdiLayout.Cascade);
        }

        /// <summary>
        /// Handles tile horizontally bar button item click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void TileHorizontallyBarButtonItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Disable tabs.
            TabManagerEnabled = false;

            // Tile horizontally MDI windows.
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        /// <summary>
        /// Handles tile vertically bar button item click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void TileVerticallyBarButtonItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Disable tabs.
            TabManagerEnabled = false;

            // Tile vertically MDI windows.
            LayoutMdi(MdiLayout.TileVertical);
        }

        /// <summary>
        /// Handles tabs bar button item click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void TabsBarButtonItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Toggle extra tabbed MDI manager enable state.
            TabManagerEnabled = !TabManagerEnabled;
        }

        /// <summary>
        /// Handles ships bar button item click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void ShipsBarButtonItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Open or activate ships form.
            OpenOrActivateMdiChild(typeof (ShipsForm), () => new ShipsForm());
        }

        /// <summary>
        /// Handles persons bar button item click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void PersonsBarButtonItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Open or activate persons form.
            OpenOrActivateMdiChild(typeof (PersonsForm), () => new PersonsForm());
        }

        /// <summary>
        /// Handles disciplines bar button item click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void DisciplinesBarButtonItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Open or activate disciplines form.
            OpenOrActivateMdiChild(typeof (DisciplinesForm), () => new DisciplinesForm());
        }

        /// <summary>
        /// Handles templates bar button item click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void TemplatesBarButtonItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Open or activate templates form.
            OpenOrActivateMdiChild(typeof (TemplatesForm), () => new TemplatesForm());
        }

        /// <summary>
        /// Handles calendar or events bar button item click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void CalendarOrEventsBarButtonItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Open or activate calendar form.
            Form form = OpenOrActivateMdiChild(typeof (CalendarForm), () => new CalendarForm());

            if (form != null && form is CalendarForm)
            {
                ((CalendarForm) form).GoToToday();
            }
        }

        /// <summary>
        /// Handles reports bar button item click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void ReportsBarButtonItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Open or activate reports form.
            OpenOrActivateMdiChild(typeof (ReportsForm), () => new ReportsForm());
        }

        /// <summary>
        /// Handles settings bar button item click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void SettingsBarButtonItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Open or activate curriculums form.
            OpenOrActivateMdiChild(typeof (SettingsForm), () => new SettingsForm());
        }

        /// <summary>
        /// Handles lock bar button item click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void LockBarButtonItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (LockWindow() == false)
            {
                // Close form and application.
                Close();
            }
        }

        /// <summary>
        /// Handles exit bar button item click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void ExitBarButtonItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (XtraMessageBox.Show(
                "�� �������, ��� ������ ��������� ������ ����������?",
                "������������� ������",
                MessageBoxButtons.OKCancel,
                MessageBoxIcon.Question,
                MessageBoxDefaultButton.Button2
                    ) == DialogResult.OK)
            {
                Close();
            }
        }

        /// <summary>
        /// Handles birthdays bar button item click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void BirthdaysBarButtonItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            Form form = OpenOrActivateMdiChild(typeof (ReportsForm), () => new ReportsForm());

            if (form != null && form is ReportsForm)
            {
                ((ReportsForm) form).OpenBirthdaysReport();
            }
        }

        private void PanelBarButtonItemItemClick(object sender, ItemClickEventArgs e)
        {
            // Open or activate panel form.
            OpenOrActivateMdiChild(typeof (PanelForm), () => new PanelForm());
        }

        private void CrewBarButtonItemItemClick(object sender, ItemClickEventArgs e)
        {
            OpenOrActivateMdiChild(typeof (CrewForm), () => new CrewForm());
        }

        private void _infoBarButtonItem_ItemClick( object sender, ItemClickEventArgs e )
        {
            RunnerApplication.ShowInfoPage( );
        }

        private bool ShowAlertWindow( AlertControl control, string caption, string text, Image image,
           DateTime? dateTime = null, object tag = null )
        {
            if (InvokeRequired)
            {
                return
                    (bool)
                        Invoke( new Func<AlertControl, string, string, Image, DateTime?, object, bool>( ShowAlertWindow ),
                            control,
                            caption, text, image, dateTime, tag );
            }

            if (control.AlertFormList.Any(
                f =>
                    ( f.AlertInfo.Tag != null && tag != null && f.AlertInfo.Tag.Equals( tag ) ||
                     f.AlertInfo.Tag == null && tag == null ) && f.AlertInfo.Caption == caption &&
                    f.AlertInfo.HotTrackedText == text ))
            {
                return false;
            }

            CloseAlertWindowByTag(control, tag);

            control.ShowToolTips = true;
            control.Show( this, caption,
                ( dateTime.HasValue ? dateTime.Value : DateTime.Now ) + Environment.NewLine + text, text,
                image, tag );
            return true;
        }

        private static void CloseAlertWindowByTag(AlertControl control, object tag)
        {
            var alertForm = control.AlertFormList.FirstOrDefault(f => f.AlertInfo.Tag != null && f.AlertInfo.Tag.Equals(tag));

            if (alertForm != null)
            {
                alertForm.AlertInfo.Tag = null;
                if (alertForm.InvokeRequired)
                    alertForm.Invoke((Action) (alertForm.Close));
                else
                    alertForm.Close();
            }
        }

        private void AlertControlAlertClick( object sender, AlertClickEventArgs e )
        {
            if (e.AlertForm.Info.Tag == Program.MessageDispatcher)
                OpenOrActivateMdiChild( typeof( ChatForm ), ( ) =>
                {
                    var form = new ChatForm( ); form.Shown += ( s, a ) => Program.MessageDispatcher.Subscribe( form );
                                                                    return form;
                });
            else if(e.AlertForm.Info.Tag == Program.TestPassingDispatcher )
                ShowUnverifiedTests( this );
            e.AlertForm.Close();
        }

        private void _unverifiedTests_ItemClick( object sender, ItemClickEventArgs e )
        {
            ShowUnverifiedTests( this );
        }

        public override IEnumerable<ISKOCBCTDNBTIDED> GetHiddenChilds( bool withLazy )
        {
            //TODO refactor to return forms without add in mdi collection
            if (withLazy)
            {
                OpenOrActivateMdiChild( typeof( ShipsForm ),        ( ) => new ShipsForm( )         { ManualFilter = true},false ) ;
                OpenOrActivateMdiChild( typeof( PersonsForm ),      ( ) => new PersonsForm( )       { ManualFilter = true }, false );
                OpenOrActivateMdiChild( typeof( DisciplinesForm ),  ( ) => new DisciplinesForm( )   { ManualFilter = true }, false );
                OpenOrActivateMdiChild( typeof( TemplatesForm ),    ( ) => new TemplatesForm( )     { ManualFilter = true }, false );
                OpenOrActivateMdiChild( typeof( CalendarForm ),     ( ) => new CalendarForm( )      { ManualFilter = true }, false );
                OpenOrActivateMdiChild( typeof( ReportsForm ),      ( ) => new ReportsForm( )       { ManualFilter = true }, false );
                OpenOrActivateMdiChild( typeof( SettingsForm ),     ( ) => new SettingsForm( )      { ManualFilter = true }, false );
                OpenOrActivateMdiChild( typeof( CrewForm ),         ( ) => new CrewForm( )          { ManualFilter = true }, false );
                OpenOrActivateMdiChild( typeof( PanelForm ),        ( ) => new PanelForm( )         { ManualFilter = true }, false );
                OpenOrActivateMdiChild( typeof( ChatForm ),         ( ) => new ChatForm()           { ManualFilter = true }, false );
                OpenOrActivateMdiChild( typeof( FilesForm ),        ( ) => new FilesForm( )         { ManualFilter = true }, false ); 
            }
            return Enumerable.Empty<ISKOCBCTDNBTIDED>( );
        }

        private void _messagesBarButton_ItemClick( object sender, ItemClickEventArgs e )
        {
            OpenOrActivateMdiChild( typeof( ChatForm ), () => { var form = new ChatForm(); form.Shown += (s,a)=> Program.MessageDispatcher.Subscribe(form,0);
                                                                  return form;
            });
        }

        #region IMessageListener Members

        public void OnNewMessage(IEnumerable<ReceiveMessageArgs> args)
        {
            int assigmentsCount = args.Count();
            List<Message> massegesList = Program.MessageDispatcher.UnHandledMessages.ToList();
            int unreadedMassegesCount = massegesList.Count;
            string description = "����� {0} �����������{1} �������{2}.".FormatString( unreadedMassegesCount, unreadedMassegesCount.Decline( "��", "��", "��" ), unreadedMassegesCount.Decline( "��", "��", "��" ) );
            if (assigmentsCount == 1)
                ShowAlertWindow( _alertControl, "������ ����� ���������.", description, null, null, Program.MessageDispatcher );
            else
                ShowAlertWindow(_alertControl,
                    "������ {0} ���{1} �������{2}.".FormatString(assigmentsCount,
                        unreadedMassegesCount.Decline("��", "��", "��"), unreadedMassegesCount.Decline("��", "��", "��")),
                    description, null, null, Program.MessageDispatcher );
        }

        public void OnMessageHasBeenHandled( ReceiveMessageArgs args )
        {
            if(args != null)
                CloseAlertWindowByTag( _alertControl, Program.MessageDispatcher );
        }

        #endregion

        private void _filesBarButton_ItemClick( object sender, ItemClickEventArgs e )
        {
            OpenOrActivateMdiChild(typeof (FilesForm), () => new FilesForm());
        }
    }
}
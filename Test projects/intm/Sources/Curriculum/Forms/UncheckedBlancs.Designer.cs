﻿namespace Eureca.Professional.FillingFormsTest.Forms
{
    partial class UncheckedBlancs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if (disposing && ( components != null ))
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            this._uncheckedBlancList = new DevExpress.XtraGrid.GridControl();
            this._uncheckedBlancView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this._okButton = new DevExpress.XtraEditors.SimpleButton();
            this._fNameColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._mNameColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._lName = new DevExpress.XtraGrid.Columns.GridColumn();
            this._testStartTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this._testEndTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this._testName = new DevExpress.XtraGrid.Columns.GridColumn();
            this._verifyCurrentButton = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this._uncheckedBlancList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._uncheckedBlancView)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _uncheckedBlancList
            // 
            this._uncheckedBlancList.Dock = System.Windows.Forms.DockStyle.Fill;
            this._uncheckedBlancList.Location = new System.Drawing.Point(0, 0);
            this._uncheckedBlancList.MainView = this._uncheckedBlancView;
            this._uncheckedBlancList.Name = "_uncheckedBlancList";
            this._uncheckedBlancList.Size = new System.Drawing.Size(620, 479);
            this._uncheckedBlancList.TabIndex = 0;
            this._uncheckedBlancList.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this._uncheckedBlancView});
            // 
            // _uncheckedBlancView
            // 
            this._uncheckedBlancView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._fNameColumn,
            this._mNameColumn,
            this._lName,
            this._testStartTime,
            this._testEndTime,
            this._testName});
            this._uncheckedBlancView.GridControl = this._uncheckedBlancList;
            this._uncheckedBlancView.Name = "_uncheckedBlancView";
            this._uncheckedBlancView.DoubleClick += new System.EventHandler(this._uncheckedBlancView_DoubleClick);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.Controls.Add(this._okButton);
            this.flowLayoutPanel1.Controls.Add(this._verifyCurrentButton);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 450);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(620, 29);
            this.flowLayoutPanel1.TabIndex = 1;
            // 
            // _okButton
            // 
            this._okButton.Location = new System.Drawing.Point(542, 3);
            this._okButton.Name = "_okButton";
            this._okButton.Size = new System.Drawing.Size(75, 23);
            this._okButton.TabIndex = 0;
            this._okButton.Text = "Готово";
            this._okButton.Click += new System.EventHandler(this._okButton_Click);
            // 
            // _fNameColumn
            // 
            this._fNameColumn.Caption = "Имя";
            this._fNameColumn.FieldName = "FirstName";
            this._fNameColumn.Name = "_fNameColumn";
            this._fNameColumn.Visible = true;
            this._fNameColumn.VisibleIndex = 0;
            // 
            // _mNameColumn
            // 
            this._mNameColumn.Caption = "Фамилия";
            this._mNameColumn.FieldName = "MiddleName";
            this._mNameColumn.Name = "_mNameColumn";
            this._mNameColumn.Visible = true;
            this._mNameColumn.VisibleIndex = 1;
            // 
            // _lName
            // 
            this._lName.Caption = "Отчество";
            this._lName.FieldName = "LastName";
            this._lName.Name = "_lName";
            this._lName.Visible = true;
            this._lName.VisibleIndex = 2;
            // 
            // _testStartTime
            // 
            this._testStartTime.Caption = "Начало теста";
            this._testStartTime.DisplayFormat.FormatString = "d";
            this._testStartTime.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this._testStartTime.FieldName = "start";
            this._testStartTime.Name = "_testStartTime";
            this._testStartTime.Visible = true;
            this._testStartTime.VisibleIndex = 3;
            // 
            // _testEndTime
            // 
            this._testEndTime.Caption = "Окончание теста";
            this._testEndTime.DisplayFormat.FormatString = "d";
            this._testEndTime.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this._testEndTime.FieldName = "end";
            this._testEndTime.Name = "_testEndTime";
            this._testEndTime.Visible = true;
            this._testEndTime.VisibleIndex = 4;
            // 
            // _testName
            // 
            this._testName.Caption = "Название теста";
            this._testName.FieldName = "title";
            this._testName.Name = "_testName";
            this._testName.Visible = true;
            this._testName.VisibleIndex = 5;
            // 
            // _verifyCurrentButton
            // 
            this._verifyCurrentButton.Location = new System.Drawing.Point(461, 3);
            this._verifyCurrentButton.Name = "_verifyCurrentButton";
            this._verifyCurrentButton.Size = new System.Drawing.Size(75, 23);
            this._verifyCurrentButton.TabIndex = 1;
            this._verifyCurrentButton.Text = "Проверить";
            this._verifyCurrentButton.Click += new System.EventHandler(this._verifyCurrentButton_Click);
            // 
            // UncheckedBlancs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(620, 479);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this._uncheckedBlancList);
            this.Name = "UncheckedBlancs";
            this.Text = "Непроверенные задания";
            ((System.ComponentModel.ISupportInitialize)(this._uncheckedBlancList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._uncheckedBlancView)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl _uncheckedBlancList;
        private DevExpress.XtraGrid.Views.Grid.GridView _uncheckedBlancView;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton _okButton;
        private DevExpress.XtraGrid.Columns.GridColumn _fNameColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _mNameColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _lName;
        private DevExpress.XtraGrid.Columns.GridColumn _testStartTime;
        private DevExpress.XtraGrid.Columns.GridColumn _testEndTime;
        private DevExpress.XtraGrid.Columns.GridColumn _testName;
        private DevExpress.XtraEditors.SimpleButton _verifyCurrentButton;
    }
}
namespace Curriculum
{
    partial class CoursesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CoursesForm));
            this._coursesControl = new Curriculum.CoursesControl();
            this.SuspendLayout();
            // 
            // _coursesControl
            // 
            this._coursesControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._coursesControl.Editable = true;
            this._coursesControl.Location = new System.Drawing.Point(0, 0);
            this._coursesControl.Name = "_coursesControl";
            this._coursesControl.Ship = null;
            this._coursesControl.Size = new System.Drawing.Size(743, 607);
            this._coursesControl.TabIndex = 1;
            this._coursesControl.Template = null;
            // 
            // CoursesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(743, 607);
            this.Controls.Add(this._coursesControl);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CoursesForm";
            this.Tag = "�����#r";
            this.Text = "�����";
            this.ResumeLayout(false);

        }

        #endregion

        private CoursesControl _coursesControl;
    }
}
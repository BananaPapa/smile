namespace Curriculum
{
    partial class ShipsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ShipsForm));
            this._referencesGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._referenceItemGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._referenceItemTextEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this._referenceValueGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._referenceValueMemoEdit = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this._referencesGridControl = new DevExpress.XtraGrid.GridControl();
            this._shipsGridControl = new DevExpress.XtraGrid.GridControl();
            this._imageList = new System.Windows.Forms.ImageList(this.components);
            this._shipsLayoutView = new DevExpress.XtraGrid.Views.Layout.LayoutView();
            this._shipIdLayoutViewColumn = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_layoutViewColumn1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this._shipNameLayoutViewColumn = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this._shipNameItemTextEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.layoutViewField__shipNameLayoutViewColumn_1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this._shipIndexLayoutViewColumn = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this._shipIndexTextEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.layoutViewField__shipIndexLayoutViewColumn_1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this._shipRiverLayoutViewColumn = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this._shipRiverComboBox = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.layoutViewField__shipRiverLayoutViewColumn_1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this._shipLineLayoutViewColumn = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this._shipLineComboBox = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.layoutViewField__shipLineLayoutViewColumn = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this._shipConditionLayoutViewColumn = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this._shipConditionComboBox = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.layoutViewField__shipConditionLayoutViewColumn_1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewCard1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewCard();
            this._shipToolComboBox = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this._shipToolCategoryComboBox = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this._shipToolSubCategoryComboBox = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this._personsGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._personPositionGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._personPositionTextEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this._personNameGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._personStatusGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._personsGridControl = new DevExpress.XtraGrid.GridControl();
            this._shipToolsGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._shipToolGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._shipToolCategoryGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._shipToolSubcategoryGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._shipToolsGridControl = new DevExpress.XtraGrid.GridControl();
            this._shipsSplitContainerControl = new DevExpress.XtraEditors.SplitContainerControl();
            this._verticalSplitContainerControl = new DevExpress.XtraEditors.SplitContainerControl();
            this._dynamicDetailsXtraTabControl = new DevExpress.XtraTab.XtraTabControl();
            this._shipToolsXtraTabPage = new DevExpress.XtraTab.XtraTabPage();
            this._responsibilityZoneXtraTabPage = new DevExpress.XtraTab.XtraTabPage();
            this._responsibilityZonesSplitContainerControl = new DevExpress.XtraEditors.SplitContainerControl();
            this._responsibilityZoneLayoutControl = new DevExpress.XtraLayout.LayoutControl();
            this._responsibilityZoneRoomTextEdit = new DevExpress.XtraEditors.TextEdit();
            this._responsibilityZonePorchTextEdit = new DevExpress.XtraEditors.TextEdit();
            this._responsibilityZoneBuildingTextEdit = new DevExpress.XtraEditors.TextEdit();
            this._responsibilityZoneNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this._responsibilityZoneOYTextEdit = new DevExpress.XtraEditors.TextEdit();
            this._responsibilityZoneNTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this._responsibilityZoneNLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this._responsibilityZoneOYLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this._responsibilityZoneNameLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this._responsibilityZoneBuildingLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this._responsibilityZoneRoomLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this._responsibilityZonePorchLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this._saveResponsibilityZonePanelControl = new DevExpress.XtraEditors.PanelControl();
            this._saveResponsibilityZoneSimpleButton = new DevExpress.XtraEditors.SimpleButton();
            this._resposibilityZonesXtraTabControl = new DevExpress.XtraTab.XtraTabControl();
            this._resposibilityZonePhonesXtraTabPage = new DevExpress.XtraTab.XtraTabPage();
            this._phonesGridControl = new DevExpress.XtraGrid.GridControl();
            this._phonesGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._phoneTypeGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._phoneTypeComboBox = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this._phoneNumberGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._phoneNumberTextEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this._resposibilityZoneNotesXtraTabPage = new DevExpress.XtraTab.XtraTabPage();
            this._responsibilityZoneNotesRtfEditControl = new Curriculum.RtfEditControl();
            this._saveResponsibilityZoneNotesPanelControl = new DevExpress.XtraEditors.PanelControl();
            this._saveResponsibilityZoneNotesSimpleButton = new DevExpress.XtraEditors.SimpleButton();
            this._referencesXtraTabPage = new DevExpress.XtraTab.XtraTabPage();
            this._notesXtraTabPage = new DevExpress.XtraTab.XtraTabPage();
            this._shipNotesRtfEditControl = new Curriculum.RtfEditControl();
            this._savePersonNotesPanelControl = new DevExpress.XtraEditors.PanelControl();
            this._saveShipNotesSimpleButton = new DevExpress.XtraEditors.SimpleButton();
            this._staticDetailsXtraTabControl = new DevExpress.XtraTab.XtraTabControl();
            this._personsXtraTabPage = new DevExpress.XtraTab.XtraTabPage();
            this._editPersonPanelControl = new DevExpress.XtraEditors.PanelControl();
            this._editPersonSimpleButton = new DevExpress.XtraEditors.SimpleButton();
            this._coursesXtraTabPage = new DevExpress.XtraTab.XtraTabPage();
            this._showCourcesLinkLabel = new System.Windows.Forms.LinkLabel();
            this._editCoursesPanelControl = new DevExpress.XtraEditors.PanelControl();
            this._editCoursesSimpleButton = new DevExpress.XtraEditors.SimpleButton();
            this._coursesControl = new Curriculum.CoursesControl();
            this._popupMenu = new DevExpress.XtraBars.PopupMenu(this.components);
            this._produceReportBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this._produceReportWithPersonsBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this._barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            ((System.ComponentModel.ISupportInitialize)(this._referencesGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._referenceItemTextEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._referenceValueMemoEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._referencesGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._shipsGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._shipsLayoutView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._shipNameItemTextEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField__shipNameLayoutViewColumn_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._shipIndexTextEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField__shipIndexLayoutViewColumn_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._shipRiverComboBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField__shipRiverLayoutViewColumn_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._shipLineComboBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField__shipLineLayoutViewColumn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._shipConditionComboBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField__shipConditionLayoutViewColumn_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewCard1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._shipToolComboBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._shipToolCategoryComboBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._shipToolSubCategoryComboBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._personsGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._personPositionTextEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._personsGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._shipToolsGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._shipToolsGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._shipsSplitContainerControl)).BeginInit();
            this._shipsSplitContainerControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._verticalSplitContainerControl)).BeginInit();
            this._verticalSplitContainerControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._dynamicDetailsXtraTabControl)).BeginInit();
            this._dynamicDetailsXtraTabControl.SuspendLayout();
            this._shipToolsXtraTabPage.SuspendLayout();
            this._responsibilityZoneXtraTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._responsibilityZonesSplitContainerControl)).BeginInit();
            this._responsibilityZonesSplitContainerControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._responsibilityZoneLayoutControl)).BeginInit();
            this._responsibilityZoneLayoutControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._responsibilityZoneRoomTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._responsibilityZonePorchTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._responsibilityZoneBuildingTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._responsibilityZoneNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._responsibilityZoneOYTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._responsibilityZoneNTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._responsibilityZoneNLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._responsibilityZoneOYLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._responsibilityZoneNameLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._responsibilityZoneBuildingLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._responsibilityZoneRoomLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._responsibilityZonePorchLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._saveResponsibilityZonePanelControl)).BeginInit();
            this._saveResponsibilityZonePanelControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._resposibilityZonesXtraTabControl)).BeginInit();
            this._resposibilityZonesXtraTabControl.SuspendLayout();
            this._resposibilityZonePhonesXtraTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._phonesGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._phonesGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._phoneTypeComboBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._phoneNumberTextEdit)).BeginInit();
            this._resposibilityZoneNotesXtraTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._saveResponsibilityZoneNotesPanelControl)).BeginInit();
            this._saveResponsibilityZoneNotesPanelControl.SuspendLayout();
            this._referencesXtraTabPage.SuspendLayout();
            this._notesXtraTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._savePersonNotesPanelControl)).BeginInit();
            this._savePersonNotesPanelControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._staticDetailsXtraTabControl)).BeginInit();
            this._staticDetailsXtraTabControl.SuspendLayout();
            this._personsXtraTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._editPersonPanelControl)).BeginInit();
            this._editPersonPanelControl.SuspendLayout();
            this._coursesXtraTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._editCoursesPanelControl)).BeginInit();
            this._editCoursesPanelControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._popupMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._barManager)).BeginInit();
            this.SuspendLayout();
            // 
            // _referencesGridView
            // 
            this._referencesGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._referenceItemGridColumn,
            this._referenceValueGridColumn});
            this._referencesGridView.GridControl = this._referencesGridControl;
            this._referencesGridView.Name = "_referencesGridView";
            this._referencesGridView.OptionsView.RowAutoHeight = true;
            this._referencesGridView.OptionsView.ShowGroupPanel = false;
            this._referencesGridView.RowHeight = 38;
            this._referencesGridView.ViewCaption = "�������";
            this._referencesGridView.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.ReferencesGridView_InitNewRow);
            this._referencesGridView.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.ReferencesGridView_FocusedRowChanged);
            this._referencesGridView.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.ReferencesGridView_ValidateRow);
            this._referencesGridView.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.ReferencesGridView_RowUpdated);
            // 
            // _referenceItemGridColumn
            // 
            this._referenceItemGridColumn.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this._referenceItemGridColumn.AppearanceCell.Options.UseFont = true;
            this._referenceItemGridColumn.AppearanceCell.Options.UseTextOptions = true;
            this._referenceItemGridColumn.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._referenceItemGridColumn.Caption = "�������";
            this._referenceItemGridColumn.ColumnEdit = this._referenceItemTextEdit;
            this._referenceItemGridColumn.FieldName = "Item";
            this._referenceItemGridColumn.Name = "_referenceItemGridColumn";
            this._referenceItemGridColumn.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this._referenceItemGridColumn.Tag = "�������#r";
            this._referenceItemGridColumn.Visible = true;
            this._referenceItemGridColumn.VisibleIndex = 0;
            // 
            // _referenceItemTextEdit
            // 
            this._referenceItemTextEdit.AutoHeight = false;
            this._referenceItemTextEdit.MaxLength = 128;
            this._referenceItemTextEdit.Name = "_referenceItemTextEdit";
            // 
            // _referenceValueGridColumn
            // 
            this._referenceValueGridColumn.Caption = "��������";
            this._referenceValueGridColumn.ColumnEdit = this._referenceValueMemoEdit;
            this._referenceValueGridColumn.FieldName = "Value";
            this._referenceValueGridColumn.Name = "_referenceValueGridColumn";
            this._referenceValueGridColumn.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this._referenceValueGridColumn.Tag = "��������#r";
            this._referenceValueGridColumn.Visible = true;
            this._referenceValueGridColumn.VisibleIndex = 1;
            // 
            // _referenceValueMemoEdit
            // 
            this._referenceValueMemoEdit.Name = "_referenceValueMemoEdit";
            // 
            // _referencesGridControl
            // 
            this._referencesGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._referencesGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.ReferencesGridControl_EmbeddedNavigator_ButtonClick);
            this._referencesGridControl.Location = new System.Drawing.Point(0, 0);
            this._referencesGridControl.MainView = this._referencesGridView;
            this._referencesGridControl.Name = "_referencesGridControl";
            this._referencesGridControl.Size = new System.Drawing.Size(570, 399);
            this._referencesGridControl.TabIndex = 0;
            this._referencesGridControl.Tag = "..\\������� �������#r";
            this._referencesGridControl.UseEmbeddedNavigator = true;
            this._referencesGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this._referencesGridView});
            // 
            // _shipsGridControl
            // 
            this._shipsGridControl.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this._shipsGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._shipsGridControl.EmbeddedNavigator.Buttons.Append.Tag = "������� ������#ra";
            this._shipsGridControl.EmbeddedNavigator.Buttons.CancelEdit.Tag = "������� ������#rc";
            this._shipsGridControl.EmbeddedNavigator.Buttons.Edit.Tag = "������� ������#rc";
            this._shipsGridControl.EmbeddedNavigator.Buttons.EndEdit.Tag = "������� ������#rc";
            this._shipsGridControl.EmbeddedNavigator.Buttons.ImageList = this._imageList;
            this._shipsGridControl.EmbeddedNavigator.Buttons.Remove.Tag = "������� ������#rd";
            this._shipsGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "���������/��������� ����������� ������", "EditLock#r"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "�������� ������", "Refresh"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "������������ ������", "Report#r")});
            this._shipsGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.ShipsGridControl_EmbeddedNavigator_ButtonClick);
            this._shipsGridControl.Location = new System.Drawing.Point(0, 0);
            this._shipsGridControl.MainView = this._shipsLayoutView;
            this._shipsGridControl.Name = "_shipsGridControl";
            this._shipsGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this._shipRiverComboBox,
            this._shipLineComboBox,
            this._shipConditionComboBox,
            this._shipNameItemTextEdit,
            this._shipIndexTextEdit,
            this._shipToolComboBox,
            this._shipToolCategoryComboBox,
            this._shipToolSubCategoryComboBox,
            this._referenceItemTextEdit,
            this._referenceValueMemoEdit});
            this._shipsGridControl.Size = new System.Drawing.Size(905, 244);
            this._shipsGridControl.TabIndex = 1;
            this._shipsGridControl.Tag = "������� ������#r";
            this._shipsGridControl.UseEmbeddedNavigator = true;
            this._shipsGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this._shipsLayoutView});
            // 
            // _imageList
            // 
            this._imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("_imageList.ImageStream")));
            this._imageList.TransparentColor = System.Drawing.Color.White;
            this._imageList.Images.SetKeyName(0, "EditLockOpen16.png");
            this._imageList.Images.SetKeyName(1, "EditLock16.png");
            this._imageList.Images.SetKeyName(2, "Refresh16.png");
            this._imageList.Images.SetKeyName(3, "Reports16.png");
            // 
            // _shipsLayoutView
            // 
            this._shipsLayoutView.CardCaptionFormat = "{3}";
            this._shipsLayoutView.CardMinSize = new System.Drawing.Size(267, 146);
            this._shipsLayoutView.Columns.AddRange(new DevExpress.XtraGrid.Columns.LayoutViewColumn[] {
            this._shipIdLayoutViewColumn,
            this._shipNameLayoutViewColumn,
            this._shipIndexLayoutViewColumn,
            this._shipRiverLayoutViewColumn,
            this._shipLineLayoutViewColumn,
            this._shipConditionLayoutViewColumn});
            this._shipsLayoutView.GridControl = this._shipsGridControl;
            this._shipsLayoutView.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutViewField_layoutViewColumn1});
            this._shipsLayoutView.Name = "_shipsLayoutView";
            this._shipsLayoutView.OptionsBehavior.AllowPanCards = false;
            this._shipsLayoutView.OptionsBehavior.AllowRuntimeCustomization = false;
            this._shipsLayoutView.OptionsBehavior.AutoFocusCardOnScrolling = true;
            this._shipsLayoutView.OptionsBehavior.AutoFocusNewCard = true;
            this._shipsLayoutView.OptionsHeaderPanel.EnableCarouselModeButton = false;
            this._shipsLayoutView.OptionsHeaderPanel.ShowCarouselModeButton = false;
            this._shipsLayoutView.OptionsView.ShowCardFieldBorders = true;
            this._shipsLayoutView.TemplateCard = this.layoutViewCard1;
            this._shipsLayoutView.ViewCaption = "�������";
            this._shipsLayoutView.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.ShipsLayoutView_FocusedRowChanged);
            this._shipsLayoutView.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.ShipsLayoutView_ValidateRow);
            this._shipsLayoutView.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.ShipsLayoutView_RowUpdated);
            this._shipsLayoutView.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.ShipsLayoutView_ValidatingEditor);
            // 
            // _shipIdLayoutViewColumn
            // 
            this._shipIdLayoutViewColumn.Caption = "Id";
            this._shipIdLayoutViewColumn.FieldName = "ShipId";
            this._shipIdLayoutViewColumn.LayoutViewField = this.layoutViewField_layoutViewColumn1;
            this._shipIdLayoutViewColumn.Name = "_shipIdLayoutViewColumn";
            this._shipIdLayoutViewColumn.OptionsColumn.AllowEdit = false;
            this._shipIdLayoutViewColumn.OptionsColumn.ReadOnly = true;
            // 
            // layoutViewField_layoutViewColumn1
            // 
            this.layoutViewField_layoutViewColumn1.EditorPreferredWidth = 10;
            this.layoutViewField_layoutViewColumn1.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField_layoutViewColumn1.Name = "layoutViewField_layoutViewColumn1";
            this.layoutViewField_layoutViewColumn1.Size = new System.Drawing.Size(246, 100);
            this.layoutViewField_layoutViewColumn1.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutViewField_layoutViewColumn1.TextSize = new System.Drawing.Size(71, 20);
            this.layoutViewField_layoutViewColumn1.TextToControlDistance = 5;
            // 
            // _shipNameLayoutViewColumn
            // 
            this._shipNameLayoutViewColumn.Caption = "��������";
            this._shipNameLayoutViewColumn.ColumnEdit = this._shipNameItemTextEdit;
            this._shipNameLayoutViewColumn.FieldName = "Name";
            this._shipNameLayoutViewColumn.LayoutViewField = this.layoutViewField__shipNameLayoutViewColumn_1;
            this._shipNameLayoutViewColumn.Name = "_shipNameLayoutViewColumn";
            this._shipNameLayoutViewColumn.Tag = "��������#r";
            // 
            // _shipNameItemTextEdit
            // 
            this._shipNameItemTextEdit.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this._shipNameItemTextEdit.AutoHeight = false;
            this._shipNameItemTextEdit.MaxLength = 128;
            this._shipNameItemTextEdit.Name = "_shipNameItemTextEdit";
            // 
            // layoutViewField__shipNameLayoutViewColumn_1
            // 
            this.layoutViewField__shipNameLayoutViewColumn_1.EditorPreferredWidth = 177;
            this.layoutViewField__shipNameLayoutViewColumn_1.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField__shipNameLayoutViewColumn_1.Name = "layoutViewField__shipNameLayoutViewColumn_1";
            this.layoutViewField__shipNameLayoutViewColumn_1.Size = new System.Drawing.Size(261, 24);
            this.layoutViewField__shipNameLayoutViewColumn_1.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutViewField__shipNameLayoutViewColumn_1.TextSize = new System.Drawing.Size(71, 13);
            this.layoutViewField__shipNameLayoutViewColumn_1.TextToControlDistance = 5;
            // 
            // _shipIndexLayoutViewColumn
            // 
            this._shipIndexLayoutViewColumn.Caption = "������";
            this._shipIndexLayoutViewColumn.ColumnEdit = this._shipIndexTextEdit;
            this._shipIndexLayoutViewColumn.FieldName = "Index";
            this._shipIndexLayoutViewColumn.LayoutViewField = this.layoutViewField__shipIndexLayoutViewColumn_1;
            this._shipIndexLayoutViewColumn.Name = "_shipIndexLayoutViewColumn";
            this._shipIndexLayoutViewColumn.Tag = "������#r";
            // 
            // _shipIndexTextEdit
            // 
            this._shipIndexTextEdit.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this._shipIndexTextEdit.AutoHeight = false;
            this._shipIndexTextEdit.MaxLength = 16;
            this._shipIndexTextEdit.Name = "_shipIndexTextEdit";
            // 
            // layoutViewField__shipIndexLayoutViewColumn_1
            // 
            this.layoutViewField__shipIndexLayoutViewColumn_1.EditorPreferredWidth = 177;
            this.layoutViewField__shipIndexLayoutViewColumn_1.Location = new System.Drawing.Point(0, 24);
            this.layoutViewField__shipIndexLayoutViewColumn_1.Name = "layoutViewField__shipIndexLayoutViewColumn_1";
            this.layoutViewField__shipIndexLayoutViewColumn_1.Size = new System.Drawing.Size(261, 24);
            this.layoutViewField__shipIndexLayoutViewColumn_1.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutViewField__shipIndexLayoutViewColumn_1.TextSize = new System.Drawing.Size(71, 13);
            this.layoutViewField__shipIndexLayoutViewColumn_1.TextToControlDistance = 5;
            // 
            // _shipRiverLayoutViewColumn
            // 
            this._shipRiverLayoutViewColumn.Caption = "����";
            this._shipRiverLayoutViewColumn.ColumnEdit = this._shipRiverComboBox;
            this._shipRiverLayoutViewColumn.FieldName = "River";
            this._shipRiverLayoutViewColumn.LayoutViewField = this.layoutViewField__shipRiverLayoutViewColumn_1;
            this._shipRiverLayoutViewColumn.Name = "_shipRiverLayoutViewColumn";
            this._shipRiverLayoutViewColumn.Tag = "����#r";
            // 
            // _shipRiverComboBox
            // 
            this._shipRiverComboBox.AutoHeight = false;
            this._shipRiverComboBox.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._shipRiverComboBox.MaxLength = 64;
            this._shipRiverComboBox.Name = "_shipRiverComboBox";
            this._shipRiverComboBox.NullText = "�� �������";
            this._shipRiverComboBox.Sorted = true;
            // 
            // layoutViewField__shipRiverLayoutViewColumn_1
            // 
            this.layoutViewField__shipRiverLayoutViewColumn_1.EditorPreferredWidth = 177;
            this.layoutViewField__shipRiverLayoutViewColumn_1.Location = new System.Drawing.Point(0, 48);
            this.layoutViewField__shipRiverLayoutViewColumn_1.Name = "layoutViewField__shipRiverLayoutViewColumn_1";
            this.layoutViewField__shipRiverLayoutViewColumn_1.Size = new System.Drawing.Size(261, 24);
            this.layoutViewField__shipRiverLayoutViewColumn_1.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutViewField__shipRiverLayoutViewColumn_1.TextSize = new System.Drawing.Size(71, 13);
            this.layoutViewField__shipRiverLayoutViewColumn_1.TextToControlDistance = 5;
            // 
            // _shipLineLayoutViewColumn
            // 
            this._shipLineLayoutViewColumn.Caption = "�����������";
            this._shipLineLayoutViewColumn.ColumnEdit = this._shipLineComboBox;
            this._shipLineLayoutViewColumn.FieldName = "ShipLine";
            this._shipLineLayoutViewColumn.LayoutViewField = this.layoutViewField__shipLineLayoutViewColumn;
            this._shipLineLayoutViewColumn.Name = "_shipLineLayoutViewColumn";
            this._shipLineLayoutViewColumn.Tag = "�����������#r";
            // 
            // _shipLineComboBox
            // 
            this._shipLineComboBox.AutoHeight = false;
            this._shipLineComboBox.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._shipLineComboBox.MaxLength = 128;
            this._shipLineComboBox.Name = "_shipLineComboBox";
            this._shipLineComboBox.NullText = "�� �������";
            this._shipLineComboBox.Sorted = true;
            // 
            // layoutViewField__shipLineLayoutViewColumn
            // 
            this.layoutViewField__shipLineLayoutViewColumn.EditorPreferredWidth = 177;
            this.layoutViewField__shipLineLayoutViewColumn.Location = new System.Drawing.Point(0, 72);
            this.layoutViewField__shipLineLayoutViewColumn.Name = "layoutViewField__shipLineLayoutViewColumn";
            this.layoutViewField__shipLineLayoutViewColumn.Size = new System.Drawing.Size(261, 24);
            this.layoutViewField__shipLineLayoutViewColumn.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutViewField__shipLineLayoutViewColumn.TextSize = new System.Drawing.Size(71, 13);
            this.layoutViewField__shipLineLayoutViewColumn.TextToControlDistance = 5;
            // 
            // _shipConditionLayoutViewColumn
            // 
            this._shipConditionLayoutViewColumn.Caption = "�������";
            this._shipConditionLayoutViewColumn.ColumnEdit = this._shipConditionComboBox;
            this._shipConditionLayoutViewColumn.FieldName = "Condition";
            this._shipConditionLayoutViewColumn.LayoutViewField = this.layoutViewField__shipConditionLayoutViewColumn_1;
            this._shipConditionLayoutViewColumn.Name = "_shipConditionLayoutViewColumn";
            this._shipConditionLayoutViewColumn.Tag = "�������#r";
            // 
            // _shipConditionComboBox
            // 
            this._shipConditionComboBox.AutoHeight = false;
            this._shipConditionComboBox.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._shipConditionComboBox.MaxLength = 64;
            this._shipConditionComboBox.Name = "_shipConditionComboBox";
            this._shipConditionComboBox.NullText = "�� �������";
            this._shipConditionComboBox.Sorted = true;
            // 
            // layoutViewField__shipConditionLayoutViewColumn_1
            // 
            this.layoutViewField__shipConditionLayoutViewColumn_1.EditorPreferredWidth = 177;
            this.layoutViewField__shipConditionLayoutViewColumn_1.Location = new System.Drawing.Point(0, 96);
            this.layoutViewField__shipConditionLayoutViewColumn_1.Name = "layoutViewField__shipConditionLayoutViewColumn_1";
            this.layoutViewField__shipConditionLayoutViewColumn_1.Size = new System.Drawing.Size(261, 24);
            this.layoutViewField__shipConditionLayoutViewColumn_1.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutViewField__shipConditionLayoutViewColumn_1.TextSize = new System.Drawing.Size(71, 13);
            this.layoutViewField__shipConditionLayoutViewColumn_1.TextToControlDistance = 5;
            // 
            // layoutViewCard1
            // 
            this.layoutViewCard1.CustomizationFormText = "layoutViewTemplateCard";
            this.layoutViewCard1.ExpandButtonLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutViewCard1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutViewField__shipNameLayoutViewColumn_1,
            this.layoutViewField__shipIndexLayoutViewColumn_1,
            this.layoutViewField__shipRiverLayoutViewColumn_1,
            this.layoutViewField__shipLineLayoutViewColumn,
            this.layoutViewField__shipConditionLayoutViewColumn_1});
            this.layoutViewCard1.Name = "layoutViewTemplateCard";
            this.layoutViewCard1.OptionsItemText.TextToControlDistance = 5;
            this.layoutViewCard1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutViewCard1.Text = "TemplateCard";
            // 
            // _shipToolComboBox
            // 
            this._shipToolComboBox.AutoHeight = false;
            this._shipToolComboBox.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._shipToolComboBox.MaxLength = 128;
            this._shipToolComboBox.Name = "_shipToolComboBox";
            this._shipToolComboBox.NullText = "�� �������";
            // 
            // _shipToolCategoryComboBox
            // 
            this._shipToolCategoryComboBox.AutoHeight = false;
            this._shipToolCategoryComboBox.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._shipToolCategoryComboBox.MaxLength = 64;
            this._shipToolCategoryComboBox.Name = "_shipToolCategoryComboBox";
            this._shipToolCategoryComboBox.NullText = "�� �������";
            // 
            // _shipToolSubCategoryComboBox
            // 
            this._shipToolSubCategoryComboBox.AutoHeight = false;
            this._shipToolSubCategoryComboBox.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._shipToolSubCategoryComboBox.MaxLength = 64;
            this._shipToolSubCategoryComboBox.Name = "_shipToolSubCategoryComboBox";
            this._shipToolSubCategoryComboBox.NullText = "�� �������";
            // 
            // _personsGridView
            // 
            this._personsGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._personPositionGridColumn,
            this._personNameGridColumn,
            this._personStatusGridColumn});
            this._personsGridView.CustomizationFormBounds = new System.Drawing.Rectangle(1058, 518, 208, 168);
            this._personsGridView.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this._personsGridView.GridControl = this._personsGridControl;
            this._personsGridView.GroupCount = 1;
            this._personsGridView.Name = "_personsGridView";
            this._personsGridView.OptionsBehavior.AutoExpandAllGroups = true;
            this._personsGridView.OptionsBehavior.Editable = false;
            this._personsGridView.OptionsDetail.EnableMasterViewMode = false;
            this._personsGridView.OptionsPrint.PrintDetails = true;
            this._personsGridView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this._personsGridView.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this._personStatusGridColumn, DevExpress.Data.ColumnSortOrder.Descending)});
            this._personsGridView.ViewCaption = "��������";
            this._personsGridView.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.PersonsGridView_FocusedRowChanged);
            // 
            // _personPositionGridColumn
            // 
            this._personPositionGridColumn.Caption = "���������";
            this._personPositionGridColumn.ColumnEdit = this._personPositionTextEdit;
            this._personPositionGridColumn.FieldName = "Position";
            this._personPositionGridColumn.Name = "_personPositionGridColumn";
            this._personPositionGridColumn.Visible = true;
            this._personPositionGridColumn.VisibleIndex = 0;
            this._personPositionGridColumn.Width = 69;
            // 
            // _personPositionTextEdit
            // 
            this._personPositionTextEdit.AutoHeight = false;
            this._personPositionTextEdit.Name = "_personPositionTextEdit";
            this._personPositionTextEdit.NullText = "�� �������";
            // 
            // _personNameGridColumn
            // 
            this._personNameGridColumn.Caption = "���";
            this._personNameGridColumn.FieldName = "Name";
            this._personNameGridColumn.Name = "_personNameGridColumn";
            this._personNameGridColumn.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this._personNameGridColumn.Visible = true;
            this._personNameGridColumn.VisibleIndex = 1;
            this._personNameGridColumn.Width = 169;
            // 
            // _personStatusGridColumn
            // 
            this._personStatusGridColumn.Caption = "������";
            this._personStatusGridColumn.FieldName = "Status";
            this._personStatusGridColumn.Name = "_personStatusGridColumn";
            this._personStatusGridColumn.Visible = true;
            this._personStatusGridColumn.VisibleIndex = 2;
            // 
            // _personsGridControl
            // 
            this._personsGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._personsGridControl.Location = new System.Drawing.Point(0, 0);
            this._personsGridControl.MainView = this._personsGridView;
            this._personsGridControl.Name = "_personsGridControl";
            this._personsGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this._personPositionTextEdit});
            this._personsGridControl.Size = new System.Drawing.Size(319, 369);
            this._personsGridControl.TabIndex = 0;
            this._personsGridControl.Tag = "������� ��������#r";
            this._personsGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this._personsGridView});
            // 
            // _shipToolsGridView
            // 
            this._shipToolsGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._shipToolGridColumn,
            this._shipToolCategoryGridColumn,
            this._shipToolSubcategoryGridColumn});
            this._shipToolsGridView.GridControl = this._shipToolsGridControl;
            this._shipToolsGridView.Name = "_shipToolsGridView";
            this._shipToolsGridView.OptionsBehavior.AutoExpandAllGroups = true;
            this._shipToolsGridView.OptionsDetail.EnableMasterViewMode = false;
            this._shipToolsGridView.ViewCaption = "��������";
            this._shipToolsGridView.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.ShipToolsGridView_InitNewRow);
            this._shipToolsGridView.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.ShipToolsGridView_FocusedRowChanged);
            this._shipToolsGridView.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.ShipToolsGridView_ValidateRow);
            this._shipToolsGridView.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.ShipToolsGridView_RowUpdated);
            this._shipToolsGridView.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.ShipToolsGridView_ValidatingEditor);
            // 
            // _shipToolGridColumn
            // 
            this._shipToolGridColumn.Caption = "��������";
            this._shipToolGridColumn.ColumnEdit = this._shipToolComboBox;
            this._shipToolGridColumn.FieldName = "Tool";
            this._shipToolGridColumn.Name = "_shipToolGridColumn";
            this._shipToolGridColumn.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this._shipToolGridColumn.Tag = "��������#r";
            this._shipToolGridColumn.Visible = true;
            this._shipToolGridColumn.VisibleIndex = 2;
            // 
            // _shipToolCategoryGridColumn
            // 
            this._shipToolCategoryGridColumn.Caption = "���������";
            this._shipToolCategoryGridColumn.ColumnEdit = this._shipToolCategoryComboBox;
            this._shipToolCategoryGridColumn.FieldName = "ToolCategory";
            this._shipToolCategoryGridColumn.Name = "_shipToolCategoryGridColumn";
            this._shipToolCategoryGridColumn.Tag = "���������#r";
            this._shipToolCategoryGridColumn.Visible = true;
            this._shipToolCategoryGridColumn.VisibleIndex = 0;
            // 
            // _shipToolSubcategoryGridColumn
            // 
            this._shipToolSubcategoryGridColumn.Caption = "������������";
            this._shipToolSubcategoryGridColumn.ColumnEdit = this._shipToolSubCategoryComboBox;
            this._shipToolSubcategoryGridColumn.FieldName = "ToolSubCategory";
            this._shipToolSubcategoryGridColumn.Name = "_shipToolSubcategoryGridColumn";
            this._shipToolSubcategoryGridColumn.Tag = "������������#r";
            this._shipToolSubcategoryGridColumn.Visible = true;
            this._shipToolSubcategoryGridColumn.VisibleIndex = 1;
            // 
            // _shipToolsGridControl
            // 
            this._shipToolsGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._shipToolsGridControl.EmbeddedNavigator.Buttons.Append.Tag = "..\\������� ��������#ra";
            this._shipToolsGridControl.EmbeddedNavigator.Buttons.CancelEdit.Tag = "..\\������� ��������#rc";
            this._shipToolsGridControl.EmbeddedNavigator.Buttons.Edit.Tag = "..\\������� ��������#rc";
            this._shipToolsGridControl.EmbeddedNavigator.Buttons.EndEdit.Tag = "..\\������� ��������#rc";
            this._shipToolsGridControl.EmbeddedNavigator.Buttons.Remove.Tag = "..\\������� ��������#rd";
            this._shipToolsGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.ShipToolsGridControl_EmbeddedNavigator_ButtonClick);
            this._shipToolsGridControl.Location = new System.Drawing.Point(0, 0);
            this._shipToolsGridControl.MainView = this._shipToolsGridView;
            this._shipToolsGridControl.Name = "_shipToolsGridControl";
            this._shipToolsGridControl.Size = new System.Drawing.Size(570, 399);
            this._shipToolsGridControl.TabIndex = 0;
            this._shipToolsGridControl.Tag = "..\\������� ��������#r";
            this._shipToolsGridControl.UseEmbeddedNavigator = true;
            this._shipToolsGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this._shipToolsGridView});
            // 
            // _shipsSplitContainerControl
            // 
            this._shipsSplitContainerControl.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this._shipsSplitContainerControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._shipsSplitContainerControl.Horizontal = false;
            this._shipsSplitContainerControl.Location = new System.Drawing.Point(0, 0);
            this._shipsSplitContainerControl.Name = "_shipsSplitContainerControl";
            this._shipsSplitContainerControl.Panel1.Controls.Add(this._shipsGridControl);
            this._shipsSplitContainerControl.Panel1.MinSize = 200;
            this._shipsSplitContainerControl.Panel1.Text = "Panel1";
            this._shipsSplitContainerControl.Panel2.Controls.Add(this._verticalSplitContainerControl);
            this._shipsSplitContainerControl.Panel2.Text = "Panel2";
            this._shipsSplitContainerControl.Size = new System.Drawing.Size(905, 678);
            this._shipsSplitContainerControl.SplitterPosition = 244;
            this._shipsSplitContainerControl.TabIndex = 4;
            // 
            // _verticalSplitContainerControl
            // 
            this._verticalSplitContainerControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._verticalSplitContainerControl.Location = new System.Drawing.Point(0, 0);
            this._verticalSplitContainerControl.Name = "_verticalSplitContainerControl";
            this._verticalSplitContainerControl.Panel1.Controls.Add(this._dynamicDetailsXtraTabControl);
            this._verticalSplitContainerControl.Panel1.Text = "Panel1";
            this._verticalSplitContainerControl.Panel2.Controls.Add(this._staticDetailsXtraTabControl);
            this._verticalSplitContainerControl.Panel2.Text = "Panel2";
            this._verticalSplitContainerControl.Size = new System.Drawing.Size(905, 428);
            this._verticalSplitContainerControl.SplitterPosition = 575;
            this._verticalSplitContainerControl.TabIndex = 1;
            this._verticalSplitContainerControl.Text = "splitContainerControl1";
            // 
            // _dynamicDetailsXtraTabControl
            // 
            this._dynamicDetailsXtraTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._dynamicDetailsXtraTabControl.Location = new System.Drawing.Point(0, 0);
            this._dynamicDetailsXtraTabControl.Name = "_dynamicDetailsXtraTabControl";
            this._dynamicDetailsXtraTabControl.SelectedTabPage = this._shipToolsXtraTabPage;
            this._dynamicDetailsXtraTabControl.Size = new System.Drawing.Size(575, 428);
            this._dynamicDetailsXtraTabControl.TabIndex = 0;
            this._dynamicDetailsXtraTabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this._shipToolsXtraTabPage,
            this._responsibilityZoneXtraTabPage,
            this._referencesXtraTabPage,
            this._notesXtraTabPage});
            // 
            // _shipToolsXtraTabPage
            // 
            this._shipToolsXtraTabPage.Controls.Add(this._shipToolsGridControl);
            this._shipToolsXtraTabPage.Image = global::Curriculum.Properties.Resources.Tools16;
            this._shipToolsXtraTabPage.ImageIndex = 6;
            this._shipToolsXtraTabPage.Name = "_shipToolsXtraTabPage";
            this._shipToolsXtraTabPage.Size = new System.Drawing.Size(570, 399);
            this._shipToolsXtraTabPage.Tag = "������� ��������#r";
            this._shipToolsXtraTabPage.Text = "��������";
            // 
            // _responsibilityZoneXtraTabPage
            // 
            this._responsibilityZoneXtraTabPage.Controls.Add(this._responsibilityZonesSplitContainerControl);
            this._responsibilityZoneXtraTabPage.Image = global::Curriculum.Properties.Resources.ResponsibilityZone16;
            this._responsibilityZoneXtraTabPage.Name = "_responsibilityZoneXtraTabPage";
            this._responsibilityZoneXtraTabPage.Size = new System.Drawing.Size(570, 399);
            this._responsibilityZoneXtraTabPage.Tag = "���� ���������������#r";
            this._responsibilityZoneXtraTabPage.Text = "���� ���������������";
            // 
            // _responsibilityZonesSplitContainerControl
            // 
            this._responsibilityZonesSplitContainerControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._responsibilityZonesSplitContainerControl.Horizontal = false;
            this._responsibilityZonesSplitContainerControl.Location = new System.Drawing.Point(0, 0);
            this._responsibilityZonesSplitContainerControl.Name = "_responsibilityZonesSplitContainerControl";
            this._responsibilityZonesSplitContainerControl.Panel1.Controls.Add(this._responsibilityZoneLayoutControl);
            this._responsibilityZonesSplitContainerControl.Panel1.Controls.Add(this._saveResponsibilityZonePanelControl);
            this._responsibilityZonesSplitContainerControl.Panel1.MinSize = 135;
            this._responsibilityZonesSplitContainerControl.Panel1.Text = "Panel1";
            this._responsibilityZonesSplitContainerControl.Panel2.Controls.Add(this._resposibilityZonesXtraTabControl);
            this._responsibilityZonesSplitContainerControl.Panel2.Text = "Panel2";
            this._responsibilityZonesSplitContainerControl.Size = new System.Drawing.Size(570, 399);
            this._responsibilityZonesSplitContainerControl.SplitterPosition = 135;
            this._responsibilityZonesSplitContainerControl.TabIndex = 3;
            this._responsibilityZonesSplitContainerControl.Text = "splitContainerControl1";
            // 
            // _responsibilityZoneLayoutControl
            // 
            this._responsibilityZoneLayoutControl.Controls.Add(this._responsibilityZoneRoomTextEdit);
            this._responsibilityZoneLayoutControl.Controls.Add(this._responsibilityZonePorchTextEdit);
            this._responsibilityZoneLayoutControl.Controls.Add(this._responsibilityZoneBuildingTextEdit);
            this._responsibilityZoneLayoutControl.Controls.Add(this._responsibilityZoneNameTextEdit);
            this._responsibilityZoneLayoutControl.Controls.Add(this._responsibilityZoneOYTextEdit);
            this._responsibilityZoneLayoutControl.Controls.Add(this._responsibilityZoneNTextEdit);
            this._responsibilityZoneLayoutControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._responsibilityZoneLayoutControl.Location = new System.Drawing.Point(0, 0);
            this._responsibilityZoneLayoutControl.Name = "_responsibilityZoneLayoutControl";
            this._responsibilityZoneLayoutControl.Root = this.layoutControlGroup1;
            this._responsibilityZoneLayoutControl.Size = new System.Drawing.Size(570, 105);
            this._responsibilityZoneLayoutControl.TabIndex = 0;
            this._responsibilityZoneLayoutControl.Text = "layoutControl1";
            // 
            // _responsibilityZoneRoomTextEdit
            // 
            this._responsibilityZoneRoomTextEdit.Location = new System.Drawing.Point(334, 60);
            this._responsibilityZoneRoomTextEdit.Name = "_responsibilityZoneRoomTextEdit";
            this._responsibilityZoneRoomTextEdit.Properties.MaxLength = 8;
            this._responsibilityZoneRoomTextEdit.Size = new System.Drawing.Size(88, 20);
            this._responsibilityZoneRoomTextEdit.StyleController = this._responsibilityZoneLayoutControl;
            this._responsibilityZoneRoomTextEdit.TabIndex = 9;
            // 
            // _responsibilityZonePorchTextEdit
            // 
            this._responsibilityZonePorchTextEdit.Location = new System.Drawing.Point(474, 60);
            this._responsibilityZonePorchTextEdit.Name = "_responsibilityZonePorchTextEdit";
            this._responsibilityZonePorchTextEdit.Properties.MaxLength = 8;
            this._responsibilityZonePorchTextEdit.Size = new System.Drawing.Size(84, 20);
            this._responsibilityZonePorchTextEdit.StyleController = this._responsibilityZoneLayoutControl;
            this._responsibilityZonePorchTextEdit.TabIndex = 8;
            // 
            // _responsibilityZoneBuildingTextEdit
            // 
            this._responsibilityZoneBuildingTextEdit.Location = new System.Drawing.Point(60, 60);
            this._responsibilityZoneBuildingTextEdit.Name = "_responsibilityZoneBuildingTextEdit";
            this._responsibilityZoneBuildingTextEdit.Properties.MaxLength = 8;
            this._responsibilityZoneBuildingTextEdit.Size = new System.Drawing.Size(222, 20);
            this._responsibilityZoneBuildingTextEdit.StyleController = this._responsibilityZoneLayoutControl;
            this._responsibilityZoneBuildingTextEdit.TabIndex = 7;
            // 
            // _responsibilityZoneNameTextEdit
            // 
            this._responsibilityZoneNameTextEdit.Location = new System.Drawing.Point(60, 36);
            this._responsibilityZoneNameTextEdit.Name = "_responsibilityZoneNameTextEdit";
            this._responsibilityZoneNameTextEdit.Properties.MaxLength = 128;
            this._responsibilityZoneNameTextEdit.Size = new System.Drawing.Size(498, 20);
            this._responsibilityZoneNameTextEdit.StyleController = this._responsibilityZoneLayoutControl;
            this._responsibilityZoneNameTextEdit.TabIndex = 6;
            // 
            // _responsibilityZoneOYTextEdit
            // 
            this._responsibilityZoneOYTextEdit.Location = new System.Drawing.Point(333, 12);
            this._responsibilityZoneOYTextEdit.Name = "_responsibilityZoneOYTextEdit";
            this._responsibilityZoneOYTextEdit.Properties.MaxLength = 32;
            this._responsibilityZoneOYTextEdit.Size = new System.Drawing.Size(225, 20);
            this._responsibilityZoneOYTextEdit.StyleController = this._responsibilityZoneLayoutControl;
            this._responsibilityZoneOYTextEdit.TabIndex = 5;
            // 
            // _responsibilityZoneNTextEdit
            // 
            this._responsibilityZoneNTextEdit.Location = new System.Drawing.Point(60, 12);
            this._responsibilityZoneNTextEdit.Name = "_responsibilityZoneNTextEdit";
            this._responsibilityZoneNTextEdit.Properties.MaxLength = 32;
            this._responsibilityZoneNTextEdit.Size = new System.Drawing.Size(221, 20);
            this._responsibilityZoneNTextEdit.StyleController = this._responsibilityZoneLayoutControl;
            this._responsibilityZoneNTextEdit.TabIndex = 4;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this._responsibilityZoneNLayoutControlItem,
            this._responsibilityZoneOYLayoutControlItem,
            this._responsibilityZoneNameLayoutControlItem,
            this._responsibilityZoneBuildingLayoutControlItem,
            this._responsibilityZoneRoomLayoutControlItem,
            this._responsibilityZonePorchLayoutControlItem});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(570, 105);
            this.layoutControlGroup1.Tag = "�������������#r";
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // _responsibilityZoneNLayoutControlItem
            // 
            this._responsibilityZoneNLayoutControlItem.Control = this._responsibilityZoneNTextEdit;
            this._responsibilityZoneNLayoutControlItem.CustomizationFormText = "N";
            this._responsibilityZoneNLayoutControlItem.Location = new System.Drawing.Point(0, 0);
            this._responsibilityZoneNLayoutControlItem.Name = "_responsibilityZoneNLayoutControlItem";
            this._responsibilityZoneNLayoutControlItem.Size = new System.Drawing.Size(273, 24);
            this._responsibilityZoneNLayoutControlItem.Text = "N";
            this._responsibilityZoneNLayoutControlItem.TextSize = new System.Drawing.Size(45, 13);
            // 
            // _responsibilityZoneOYLayoutControlItem
            // 
            this._responsibilityZoneOYLayoutControlItem.Control = this._responsibilityZoneOYTextEdit;
            this._responsibilityZoneOYLayoutControlItem.CustomizationFormText = "OY";
            this._responsibilityZoneOYLayoutControlItem.Location = new System.Drawing.Point(273, 0);
            this._responsibilityZoneOYLayoutControlItem.Name = "_responsibilityZoneOYLayoutControlItem";
            this._responsibilityZoneOYLayoutControlItem.Size = new System.Drawing.Size(277, 24);
            this._responsibilityZoneOYLayoutControlItem.Text = "OY";
            this._responsibilityZoneOYLayoutControlItem.TextSize = new System.Drawing.Size(45, 13);
            // 
            // _responsibilityZoneNameLayoutControlItem
            // 
            this._responsibilityZoneNameLayoutControlItem.Control = this._responsibilityZoneNameTextEdit;
            this._responsibilityZoneNameLayoutControlItem.CustomizationFormText = "���";
            this._responsibilityZoneNameLayoutControlItem.Location = new System.Drawing.Point(0, 24);
            this._responsibilityZoneNameLayoutControlItem.Name = "_responsibilityZoneNameLayoutControlItem";
            this._responsibilityZoneNameLayoutControlItem.Size = new System.Drawing.Size(550, 24);
            this._responsibilityZoneNameLayoutControlItem.Text = "���";
            this._responsibilityZoneNameLayoutControlItem.TextSize = new System.Drawing.Size(45, 13);
            // 
            // _responsibilityZoneBuildingLayoutControlItem
            // 
            this._responsibilityZoneBuildingLayoutControlItem.Control = this._responsibilityZoneBuildingTextEdit;
            this._responsibilityZoneBuildingLayoutControlItem.CustomizationFormText = "������";
            this._responsibilityZoneBuildingLayoutControlItem.Location = new System.Drawing.Point(0, 48);
            this._responsibilityZoneBuildingLayoutControlItem.Name = "_responsibilityZoneBuildingLayoutControlItem";
            this._responsibilityZoneBuildingLayoutControlItem.Size = new System.Drawing.Size(274, 37);
            this._responsibilityZoneBuildingLayoutControlItem.Text = "������";
            this._responsibilityZoneBuildingLayoutControlItem.TextSize = new System.Drawing.Size(45, 13);
            // 
            // _responsibilityZoneRoomLayoutControlItem
            // 
            this._responsibilityZoneRoomLayoutControlItem.Control = this._responsibilityZoneRoomTextEdit;
            this._responsibilityZoneRoomLayoutControlItem.CustomizationFormText = "�������";
            this._responsibilityZoneRoomLayoutControlItem.Location = new System.Drawing.Point(274, 48);
            this._responsibilityZoneRoomLayoutControlItem.Name = "_responsibilityZoneRoomLayoutControlItem";
            this._responsibilityZoneRoomLayoutControlItem.Size = new System.Drawing.Size(140, 37);
            this._responsibilityZoneRoomLayoutControlItem.Text = "�������";
            this._responsibilityZoneRoomLayoutControlItem.TextSize = new System.Drawing.Size(45, 13);
            // 
            // _responsibilityZonePorchLayoutControlItem
            // 
            this._responsibilityZonePorchLayoutControlItem.Control = this._responsibilityZonePorchTextEdit;
            this._responsibilityZonePorchLayoutControlItem.CustomizationFormText = "�������";
            this._responsibilityZonePorchLayoutControlItem.Location = new System.Drawing.Point(414, 48);
            this._responsibilityZonePorchLayoutControlItem.Name = "_responsibilityZonePorchLayoutControlItem";
            this._responsibilityZonePorchLayoutControlItem.Size = new System.Drawing.Size(136, 37);
            this._responsibilityZonePorchLayoutControlItem.Text = "�������";
            this._responsibilityZonePorchLayoutControlItem.TextSize = new System.Drawing.Size(45, 13);
            this._responsibilityZonePorchLayoutControlItem.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // _saveResponsibilityZonePanelControl
            // 
            this._saveResponsibilityZonePanelControl.Controls.Add(this._saveResponsibilityZoneSimpleButton);
            this._saveResponsibilityZonePanelControl.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._saveResponsibilityZonePanelControl.Location = new System.Drawing.Point(0, 105);
            this._saveResponsibilityZonePanelControl.Name = "_saveResponsibilityZonePanelControl";
            this._saveResponsibilityZonePanelControl.Size = new System.Drawing.Size(570, 30);
            this._saveResponsibilityZonePanelControl.TabIndex = 7;
            // 
            // _saveResponsibilityZoneSimpleButton
            // 
            this._saveResponsibilityZoneSimpleButton.Dock = System.Windows.Forms.DockStyle.Right;
            this._saveResponsibilityZoneSimpleButton.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this._saveResponsibilityZoneSimpleButton.Location = new System.Drawing.Point(486, 2);
            this._saveResponsibilityZoneSimpleButton.Name = "_saveResponsibilityZoneSimpleButton";
            this._saveResponsibilityZoneSimpleButton.Size = new System.Drawing.Size(82, 26);
            this._saveResponsibilityZoneSimpleButton.TabIndex = 2;
            this._saveResponsibilityZoneSimpleButton.Tag = "�������������#rc";
            this._saveResponsibilityZoneSimpleButton.Text = "���������";
            this._saveResponsibilityZoneSimpleButton.ToolTip = "��������� ���������";
            this._saveResponsibilityZoneSimpleButton.Click += new System.EventHandler(this.SaveResponsibilityZoneSimpleButton_Click);
            // 
            // _resposibilityZonesXtraTabControl
            // 
            this._resposibilityZonesXtraTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._resposibilityZonesXtraTabControl.Location = new System.Drawing.Point(0, 0);
            this._resposibilityZonesXtraTabControl.Name = "_resposibilityZonesXtraTabControl";
            this._resposibilityZonesXtraTabControl.SelectedTabPage = this._resposibilityZonePhonesXtraTabPage;
            this._resposibilityZonesXtraTabControl.Size = new System.Drawing.Size(570, 258);
            this._resposibilityZonesXtraTabControl.TabIndex = 2;
            this._resposibilityZonesXtraTabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this._resposibilityZonePhonesXtraTabPage,
            this._resposibilityZoneNotesXtraTabPage});
            // 
            // _resposibilityZonePhonesXtraTabPage
            // 
            this._resposibilityZonePhonesXtraTabPage.Controls.Add(this._phonesGridControl);
            this._resposibilityZonePhonesXtraTabPage.Image = global::Curriculum.Properties.Resources.Phone16;
            this._resposibilityZonePhonesXtraTabPage.Name = "_resposibilityZonePhonesXtraTabPage";
            this._resposibilityZonePhonesXtraTabPage.Size = new System.Drawing.Size(565, 229);
            this._resposibilityZonePhonesXtraTabPage.Tag = "������� ��������#r";
            this._resposibilityZonePhonesXtraTabPage.Text = "��������";
            // 
            // _phonesGridControl
            // 
            this._phonesGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._phonesGridControl.EmbeddedNavigator.Buttons.Append.Tag = "..\\������� ��������#ra";
            this._phonesGridControl.EmbeddedNavigator.Buttons.CancelEdit.Tag = "..\\������� ��������#rc";
            this._phonesGridControl.EmbeddedNavigator.Buttons.Edit.Tag = "..\\������� ��������#rc";
            this._phonesGridControl.EmbeddedNavigator.Buttons.EndEdit.Tag = "..\\������� ��������#rc";
            this._phonesGridControl.EmbeddedNavigator.Buttons.Remove.Tag = "..\\������� ��������#rd";
            this._phonesGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.PhonesGridControl_EmbeddedNavigator_ButtonClick);
            this._phonesGridControl.Location = new System.Drawing.Point(0, 0);
            this._phonesGridControl.MainView = this._phonesGridView;
            this._phonesGridControl.Name = "_phonesGridControl";
            this._phonesGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this._phoneTypeComboBox,
            this._phoneNumberTextEdit});
            this._phonesGridControl.Size = new System.Drawing.Size(565, 229);
            this._phonesGridControl.TabIndex = 1;
            this._phonesGridControl.Tag = "..\\������� ��������#r";
            this._phonesGridControl.UseEmbeddedNavigator = true;
            this._phonesGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this._phonesGridView});
            // 
            // _phonesGridView
            // 
            this._phonesGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._phoneTypeGridColumn,
            this._phoneNumberGridColumn});
            this._phonesGridView.GridControl = this._phonesGridControl;
            this._phonesGridView.Name = "_phonesGridView";
            this._phonesGridView.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.PhonesGridView_InitNewRow);
            this._phonesGridView.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.PhonesGridView_FocusedRowChanged);
            this._phonesGridView.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.PhonesGridView_ValidateRow);
            this._phonesGridView.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.PhonesGridView_RowUpdated);
            this._phonesGridView.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.PhonesGridView_ValidatingEditor);
            // 
            // _phoneTypeGridColumn
            // 
            this._phoneTypeGridColumn.Caption = "���";
            this._phoneTypeGridColumn.ColumnEdit = this._phoneTypeComboBox;
            this._phoneTypeGridColumn.FieldName = "PhoneType";
            this._phoneTypeGridColumn.Name = "_phoneTypeGridColumn";
            this._phoneTypeGridColumn.Tag = "���#r";
            this._phoneTypeGridColumn.Visible = true;
            this._phoneTypeGridColumn.VisibleIndex = 0;
            // 
            // _phoneTypeComboBox
            // 
            this._phoneTypeComboBox.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this._phoneTypeComboBox.AutoHeight = false;
            this._phoneTypeComboBox.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._phoneTypeComboBox.MaxLength = 32;
            this._phoneTypeComboBox.Name = "_phoneTypeComboBox";
            this._phoneTypeComboBox.NullText = "�� ������";
            this._phoneTypeComboBox.Sorted = true;
            // 
            // _phoneNumberGridColumn
            // 
            this._phoneNumberGridColumn.Caption = "�����";
            this._phoneNumberGridColumn.ColumnEdit = this._phoneNumberTextEdit;
            this._phoneNumberGridColumn.FieldName = "Number";
            this._phoneNumberGridColumn.Name = "_phoneNumberGridColumn";
            this._phoneNumberGridColumn.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this._phoneNumberGridColumn.Tag = "�����#r";
            this._phoneNumberGridColumn.Visible = true;
            this._phoneNumberGridColumn.VisibleIndex = 1;
            // 
            // _phoneNumberTextEdit
            // 
            this._phoneNumberTextEdit.AutoHeight = false;
            this._phoneNumberTextEdit.MaxLength = 32;
            this._phoneNumberTextEdit.Name = "_phoneNumberTextEdit";
            // 
            // _resposibilityZoneNotesXtraTabPage
            // 
            this._resposibilityZoneNotesXtraTabPage.Controls.Add(this._responsibilityZoneNotesRtfEditControl);
            this._resposibilityZoneNotesXtraTabPage.Controls.Add(this._saveResponsibilityZoneNotesPanelControl);
            this._resposibilityZoneNotesXtraTabPage.Image = global::Curriculum.Properties.Resources.Notes16;
            this._resposibilityZoneNotesXtraTabPage.Name = "_resposibilityZoneNotesXtraTabPage";
            this._resposibilityZoneNotesXtraTabPage.Size = new System.Drawing.Size(565, 229);
            this._resposibilityZoneNotesXtraTabPage.Tag = "�������#r";
            this._resposibilityZoneNotesXtraTabPage.Text = "�������";
            // 
            // _responsibilityZoneNotesRtfEditControl
            // 
            this._responsibilityZoneNotesRtfEditControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._responsibilityZoneNotesRtfEditControl.Location = new System.Drawing.Point(0, 0);
            this._responsibilityZoneNotesRtfEditControl.MinimumSize = new System.Drawing.Size(420, 100);
            this._responsibilityZoneNotesRtfEditControl.Name = "_responsibilityZoneNotesRtfEditControl";
            this._responsibilityZoneNotesRtfEditControl.ReadOnly = false;
            this._responsibilityZoneNotesRtfEditControl.Rtf = "{\\rtf1\\ansi\\ansicpg1251\\deff0\\deflang1049{\\fonttbl{\\f0\\fnil\\fcharset204 Microsoft" +
    " Sans Serif;}}\r\n\\viewkind4\\uc1\\pard\\f0\\fs17\\par\r\n}\r\n";
            this._responsibilityZoneNotesRtfEditControl.Size = new System.Drawing.Size(565, 199);
            this._responsibilityZoneNotesRtfEditControl.TabIndex = 5;
            this._responsibilityZoneNotesRtfEditControl.Tag = "..\\�������#r";
            // 
            // _saveResponsibilityZoneNotesPanelControl
            // 
            this._saveResponsibilityZoneNotesPanelControl.Controls.Add(this._saveResponsibilityZoneNotesSimpleButton);
            this._saveResponsibilityZoneNotesPanelControl.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._saveResponsibilityZoneNotesPanelControl.Location = new System.Drawing.Point(0, 199);
            this._saveResponsibilityZoneNotesPanelControl.Name = "_saveResponsibilityZoneNotesPanelControl";
            this._saveResponsibilityZoneNotesPanelControl.Size = new System.Drawing.Size(565, 30);
            this._saveResponsibilityZoneNotesPanelControl.TabIndex = 6;
            // 
            // _saveResponsibilityZoneNotesSimpleButton
            // 
            this._saveResponsibilityZoneNotesSimpleButton.Dock = System.Windows.Forms.DockStyle.Right;
            this._saveResponsibilityZoneNotesSimpleButton.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this._saveResponsibilityZoneNotesSimpleButton.Location = new System.Drawing.Point(481, 2);
            this._saveResponsibilityZoneNotesSimpleButton.Name = "_saveResponsibilityZoneNotesSimpleButton";
            this._saveResponsibilityZoneNotesSimpleButton.Size = new System.Drawing.Size(82, 26);
            this._saveResponsibilityZoneNotesSimpleButton.TabIndex = 2;
            this._saveResponsibilityZoneNotesSimpleButton.Tag = "..\\�������#rc";
            this._saveResponsibilityZoneNotesSimpleButton.Text = "���������";
            this._saveResponsibilityZoneNotesSimpleButton.ToolTip = "��������� ���������";
            this._saveResponsibilityZoneNotesSimpleButton.Click += new System.EventHandler(this.SaveResponsibilityZoneNotesSimpleButton_Click);
            // 
            // _referencesXtraTabPage
            // 
            this._referencesXtraTabPage.Controls.Add(this._referencesGridControl);
            this._referencesXtraTabPage.Image = global::Curriculum.Properties.Resources.Reference16;
            this._referencesXtraTabPage.ImageIndex = 4;
            this._referencesXtraTabPage.Name = "_referencesXtraTabPage";
            this._referencesXtraTabPage.Size = new System.Drawing.Size(570, 399);
            this._referencesXtraTabPage.Tag = "������� �������#r";
            this._referencesXtraTabPage.Text = "�������";
            // 
            // _notesXtraTabPage
            // 
            this._notesXtraTabPage.Controls.Add(this._shipNotesRtfEditControl);
            this._notesXtraTabPage.Controls.Add(this._savePersonNotesPanelControl);
            this._notesXtraTabPage.Image = global::Curriculum.Properties.Resources.Notes16;
            this._notesXtraTabPage.ImageIndex = 5;
            this._notesXtraTabPage.Name = "_notesXtraTabPage";
            this._notesXtraTabPage.Size = new System.Drawing.Size(570, 399);
            this._notesXtraTabPage.Tag = "�������#r";
            this._notesXtraTabPage.Text = "�������";
            // 
            // _shipNotesRtfEditControl
            // 
            this._shipNotesRtfEditControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._shipNotesRtfEditControl.Location = new System.Drawing.Point(0, 0);
            this._shipNotesRtfEditControl.MinimumSize = new System.Drawing.Size(420, 100);
            this._shipNotesRtfEditControl.Name = "_shipNotesRtfEditControl";
            this._shipNotesRtfEditControl.ReadOnly = false;
            this._shipNotesRtfEditControl.Rtf = "{\\rtf1\\ansi\\ansicpg1251\\deff0\\deflang1049{\\fonttbl{\\f0\\fnil\\fcharset204 Microsoft" +
    " Sans Serif;}}\r\n\\viewkind4\\uc1\\pard\\f0\\fs17\\par\r\n}\r\n";
            this._shipNotesRtfEditControl.Size = new System.Drawing.Size(570, 369);
            this._shipNotesRtfEditControl.TabIndex = 0;
            this._shipNotesRtfEditControl.Tag = "..\\�������#r";
            // 
            // _savePersonNotesPanelControl
            // 
            this._savePersonNotesPanelControl.Controls.Add(this._saveShipNotesSimpleButton);
            this._savePersonNotesPanelControl.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._savePersonNotesPanelControl.Location = new System.Drawing.Point(0, 369);
            this._savePersonNotesPanelControl.Name = "_savePersonNotesPanelControl";
            this._savePersonNotesPanelControl.Size = new System.Drawing.Size(570, 30);
            this._savePersonNotesPanelControl.TabIndex = 4;
            // 
            // _saveShipNotesSimpleButton
            // 
            this._saveShipNotesSimpleButton.Dock = System.Windows.Forms.DockStyle.Right;
            this._saveShipNotesSimpleButton.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this._saveShipNotesSimpleButton.Location = new System.Drawing.Point(486, 2);
            this._saveShipNotesSimpleButton.Name = "_saveShipNotesSimpleButton";
            this._saveShipNotesSimpleButton.Size = new System.Drawing.Size(82, 26);
            this._saveShipNotesSimpleButton.TabIndex = 2;
            this._saveShipNotesSimpleButton.Tag = "..\\�������#rc";
            this._saveShipNotesSimpleButton.Text = "���������";
            this._saveShipNotesSimpleButton.ToolTip = "��������� ���������";
            this._saveShipNotesSimpleButton.Click += new System.EventHandler(this.SaveShipNotesSimpleButton_Click);
            // 
            // _staticDetailsXtraTabControl
            // 
            this._staticDetailsXtraTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._staticDetailsXtraTabControl.Location = new System.Drawing.Point(0, 0);
            this._staticDetailsXtraTabControl.Name = "_staticDetailsXtraTabControl";
            this._staticDetailsXtraTabControl.SelectedTabPage = this._personsXtraTabPage;
            this._staticDetailsXtraTabControl.Size = new System.Drawing.Size(324, 428);
            this._staticDetailsXtraTabControl.TabIndex = 0;
            this._staticDetailsXtraTabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this._personsXtraTabPage,
            this._coursesXtraTabPage});
            // 
            // _personsXtraTabPage
            // 
            this._personsXtraTabPage.Controls.Add(this._personsGridControl);
            this._personsXtraTabPage.Controls.Add(this._editPersonPanelControl);
            this._personsXtraTabPage.Image = global::Curriculum.Properties.Resources.User16;
            this._personsXtraTabPage.ImageIndex = 3;
            this._personsXtraTabPage.Name = "_personsXtraTabPage";
            this._personsXtraTabPage.Size = new System.Drawing.Size(319, 399);
            this._personsXtraTabPage.Tag = "������� ��������#r";
            this._personsXtraTabPage.Text = "��������";
            // 
            // _editPersonPanelControl
            // 
            this._editPersonPanelControl.Controls.Add(this._editPersonSimpleButton);
            this._editPersonPanelControl.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._editPersonPanelControl.Location = new System.Drawing.Point(0, 369);
            this._editPersonPanelControl.Name = "_editPersonPanelControl";
            this._editPersonPanelControl.Size = new System.Drawing.Size(319, 30);
            this._editPersonPanelControl.TabIndex = 5;
            // 
            // _editPersonSimpleButton
            // 
            this._editPersonSimpleButton.Dock = System.Windows.Forms.DockStyle.Right;
            this._editPersonSimpleButton.Enabled = false;
            this._editPersonSimpleButton.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this._editPersonSimpleButton.Location = new System.Drawing.Point(207, 2);
            this._editPersonSimpleButton.Name = "_editPersonSimpleButton";
            this._editPersonSimpleButton.Size = new System.Drawing.Size(110, 26);
            this._editPersonSimpleButton.TabIndex = 2;
            this._editPersonSimpleButton.Tag = "������� ��������#r";
            this._editPersonSimpleButton.Text = "�������������";
            this._editPersonSimpleButton.ToolTip = "������������� ����������";
            this._editPersonSimpleButton.Click += new System.EventHandler(this.EditPersonSimpleButton_Click);
            // 
            // _coursesXtraTabPage
            // 
            this._coursesXtraTabPage.Controls.Add(this._showCourcesLinkLabel);
            this._coursesXtraTabPage.Controls.Add(this._editCoursesPanelControl);
            this._coursesXtraTabPage.Controls.Add(this._coursesControl);
            this._coursesXtraTabPage.Image = global::Curriculum.Properties.Resources.Courses16;
            this._coursesXtraTabPage.ImageIndex = 7;
            this._coursesXtraTabPage.Name = "_coursesXtraTabPage";
            this._coursesXtraTabPage.Size = new System.Drawing.Size(319, 399);
            this._coursesXtraTabPage.Tag = "�����#r";
            this._coursesXtraTabPage.Text = "�����";
            // 
            // _showCourcesLinkLabel
            // 
            this._showCourcesLinkLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._showCourcesLinkLabel.Location = new System.Drawing.Point(0, 0);
            this._showCourcesLinkLabel.Name = "_showCourcesLinkLabel";
            this._showCourcesLinkLabel.Size = new System.Drawing.Size(319, 369);
            this._showCourcesLinkLabel.TabIndex = 7;
            this._showCourcesLinkLabel.TabStop = true;
            this._showCourcesLinkLabel.Tag = "..\\�����#r";
            this._showCourcesLinkLabel.Text = "�������� �����";
            this._showCourcesLinkLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this._showCourcesLinkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.ShowCourcesLinkLabel_LinkClicked);
            // 
            // _editCoursesPanelControl
            // 
            this._editCoursesPanelControl.Controls.Add(this._editCoursesSimpleButton);
            this._editCoursesPanelControl.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._editCoursesPanelControl.Location = new System.Drawing.Point(0, 369);
            this._editCoursesPanelControl.Name = "_editCoursesPanelControl";
            this._editCoursesPanelControl.Size = new System.Drawing.Size(319, 30);
            this._editCoursesPanelControl.TabIndex = 5;
            // 
            // _editCoursesSimpleButton
            // 
            this._editCoursesSimpleButton.Dock = System.Windows.Forms.DockStyle.Right;
            this._editCoursesSimpleButton.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this._editCoursesSimpleButton.Location = new System.Drawing.Point(207, 2);
            this._editCoursesSimpleButton.Name = "_editCoursesSimpleButton";
            this._editCoursesSimpleButton.Size = new System.Drawing.Size(110, 26);
            this._editCoursesSimpleButton.TabIndex = 2;
            this._editCoursesSimpleButton.Tag = "..\\�����#rc";
            this._editCoursesSimpleButton.Text = "�������������";
            this._editCoursesSimpleButton.ToolTip = "�������� �����";
            this._editCoursesSimpleButton.Click += new System.EventHandler(this.EditCoursesSimpleButton_Click);
            // 
            // _coursesControl
            // 
            this._coursesControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._coursesControl.Editable = false;
            this._coursesControl.Location = new System.Drawing.Point(0, 0);
            this._coursesControl.Name = "_coursesControl";
            this._coursesControl.Ship = null;
            this._coursesControl.Size = new System.Drawing.Size(319, 399);
            this._coursesControl.TabIndex = 0;
            this._coursesControl.Tag = "..\\�����#r";
            this._coursesControl.Template = null;
            // 
            // _popupMenu
            // 
            this._popupMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this._produceReportBarButtonItem),
            new DevExpress.XtraBars.LinkPersistInfo(this._produceReportWithPersonsBarButtonItem)});
            this._popupMenu.Manager = this._barManager;
            this._popupMenu.Name = "_popupMenu";
            // 
            // _produceReportBarButtonItem
            // 
            this._produceReportBarButtonItem.Caption = "������������ �����";
            this._produceReportBarButtonItem.Glyph = global::Curriculum.Properties.Resources.Reports16;
            this._produceReportBarButtonItem.Id = 9;
            this._produceReportBarButtonItem.Name = "_produceReportBarButtonItem";
            this._produceReportBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.ProduceReportBarButtonItemItemClick);
            // 
            // _produceReportWithPersonsBarButtonItem
            // 
            this._produceReportWithPersonsBarButtonItem.Caption = "������������ ����� � ����������";
            this._produceReportWithPersonsBarButtonItem.Id = 18;
            this._produceReportWithPersonsBarButtonItem.ImageIndex = 3;
            this._produceReportWithPersonsBarButtonItem.Name = "_produceReportWithPersonsBarButtonItem";
            this._produceReportWithPersonsBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.ProduceReportWithPersonsBarButtonItemItemClick);
            // 
            // _barManager
            // 
            this._barManager.DockControls.Add(this.barDockControlTop);
            this._barManager.DockControls.Add(this.barDockControlBottom);
            this._barManager.DockControls.Add(this.barDockControlLeft);
            this._barManager.DockControls.Add(this.barDockControlRight);
            this._barManager.Form = this;
            this._barManager.Images = this._imageList;
            this._barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this._produceReportBarButtonItem,
            this._produceReportWithPersonsBarButtonItem});
            this._barManager.MaxItemId = 19;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(905, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 678);
            this.barDockControlBottom.Size = new System.Drawing.Size(905, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 678);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(905, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 678);
            // 
            // ShipsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(905, 678);
            this.Controls.Add(this._shipsSplitContainerControl);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.HasInnerHideLogic = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ShipsForm";
            this.Tag = "������ ����������\\�������\\�������#r";
            this.Text = "�������";
            this.Load += new System.EventHandler(this.ShipsForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this._referencesGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._referenceItemTextEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._referenceValueMemoEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._referencesGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._shipsGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._shipsLayoutView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._shipNameItemTextEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField__shipNameLayoutViewColumn_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._shipIndexTextEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField__shipIndexLayoutViewColumn_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._shipRiverComboBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField__shipRiverLayoutViewColumn_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._shipLineComboBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField__shipLineLayoutViewColumn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._shipConditionComboBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField__shipConditionLayoutViewColumn_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewCard1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._shipToolComboBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._shipToolCategoryComboBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._shipToolSubCategoryComboBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._personsGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._personPositionTextEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._personsGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._shipToolsGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._shipToolsGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._shipsSplitContainerControl)).EndInit();
            this._shipsSplitContainerControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._verticalSplitContainerControl)).EndInit();
            this._verticalSplitContainerControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._dynamicDetailsXtraTabControl)).EndInit();
            this._dynamicDetailsXtraTabControl.ResumeLayout(false);
            this._shipToolsXtraTabPage.ResumeLayout(false);
            this._responsibilityZoneXtraTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._responsibilityZonesSplitContainerControl)).EndInit();
            this._responsibilityZonesSplitContainerControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._responsibilityZoneLayoutControl)).EndInit();
            this._responsibilityZoneLayoutControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._responsibilityZoneRoomTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._responsibilityZonePorchTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._responsibilityZoneBuildingTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._responsibilityZoneNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._responsibilityZoneOYTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._responsibilityZoneNTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._responsibilityZoneNLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._responsibilityZoneOYLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._responsibilityZoneNameLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._responsibilityZoneBuildingLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._responsibilityZoneRoomLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._responsibilityZonePorchLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._saveResponsibilityZonePanelControl)).EndInit();
            this._saveResponsibilityZonePanelControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._resposibilityZonesXtraTabControl)).EndInit();
            this._resposibilityZonesXtraTabControl.ResumeLayout(false);
            this._resposibilityZonePhonesXtraTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._phonesGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._phonesGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._phoneTypeComboBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._phoneNumberTextEdit)).EndInit();
            this._resposibilityZoneNotesXtraTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._saveResponsibilityZoneNotesPanelControl)).EndInit();
            this._saveResponsibilityZoneNotesPanelControl.ResumeLayout(false);
            this._referencesXtraTabPage.ResumeLayout(false);
            this._notesXtraTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._savePersonNotesPanelControl)).EndInit();
            this._savePersonNotesPanelControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._staticDetailsXtraTabControl)).EndInit();
            this._staticDetailsXtraTabControl.ResumeLayout(false);
            this._personsXtraTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._editPersonPanelControl)).EndInit();
            this._editPersonPanelControl.ResumeLayout(false);
            this._coursesXtraTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._editCoursesPanelControl)).EndInit();
            this._editCoursesPanelControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._popupMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._barManager)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl _shipsGridControl;
        private DevExpress.XtraGrid.Views.Layout.LayoutView _shipsLayoutView;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn _shipNameLayoutViewColumn;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn _shipIndexLayoutViewColumn;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn _shipRiverLayoutViewColumn;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox _shipRiverComboBox;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn _shipLineLayoutViewColumn;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox _shipLineComboBox;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn _shipConditionLayoutViewColumn;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox _shipConditionComboBox;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit _shipNameItemTextEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit _shipIndexTextEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView _personsGridView;
        private DevExpress.XtraGrid.Columns.GridColumn _personPositionGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _personNameGridColumn;
        private DevExpress.XtraGrid.Views.Grid.GridView _shipToolsGridView;
        private DevExpress.XtraGrid.Columns.GridColumn _shipToolGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _shipToolCategoryGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _shipToolSubcategoryGridColumn;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox _shipToolComboBox;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox _shipToolCategoryComboBox;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox _shipToolSubCategoryComboBox;
        private DevExpress.XtraGrid.Views.Grid.GridView _referencesGridView;
        private DevExpress.XtraGrid.Columns.GridColumn _referenceItemGridColumn;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit _referenceItemTextEdit;
        private DevExpress.XtraGrid.Columns.GridColumn _referenceValueGridColumn;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit _referenceValueMemoEdit;
        private DevExpress.XtraEditors.SplitContainerControl _shipsSplitContainerControl;
        private DevExpress.XtraTab.XtraTabControl _dynamicDetailsXtraTabControl;
        private DevExpress.XtraTab.XtraTabPage _shipToolsXtraTabPage;
        private DevExpress.XtraTab.XtraTabPage _responsibilityZoneXtraTabPage;
        private DevExpress.XtraTab.XtraTabPage _referencesXtraTabPage;
        private DevExpress.XtraGrid.GridControl _shipToolsGridControl;
        private DevExpress.XtraGrid.GridControl _personsGridControl;
        private DevExpress.XtraGrid.GridControl _referencesGridControl;
        private DevExpress.XtraEditors.SplitContainerControl _verticalSplitContainerControl;
        private DevExpress.XtraTab.XtraTabControl _staticDetailsXtraTabControl;
        private DevExpress.XtraTab.XtraTabPage _personsXtraTabPage;
        private DevExpress.XtraTab.XtraTabPage _notesXtraTabPage;
        private DevExpress.XtraTab.XtraTabPage _coursesXtraTabPage;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit _personPositionTextEdit;
        private RtfEditControl _shipNotesRtfEditControl;
        private DevExpress.XtraEditors.PanelControl _savePersonNotesPanelControl;
        private DevExpress.XtraEditors.SimpleButton _saveShipNotesSimpleButton;
        private DevExpress.XtraEditors.PanelControl _editPersonPanelControl;
        private DevExpress.XtraEditors.SimpleButton _editPersonSimpleButton;
        private CoursesControl _coursesControl;
        private DevExpress.XtraEditors.PanelControl _editCoursesPanelControl;
        private DevExpress.XtraEditors.SimpleButton _editCoursesSimpleButton;
        private System.Windows.Forms.LinkLabel _showCourcesLinkLabel;
        private System.Windows.Forms.ImageList _imageList;
        private DevExpress.XtraTab.XtraTabControl _resposibilityZonesXtraTabControl;
        private DevExpress.XtraTab.XtraTabPage _resposibilityZonePhonesXtraTabPage;
        private DevExpress.XtraTab.XtraTabPage _resposibilityZoneNotesXtraTabPage;
        private RtfEditControl _responsibilityZoneNotesRtfEditControl;
        private DevExpress.XtraEditors.PanelControl _saveResponsibilityZoneNotesPanelControl;
        private DevExpress.XtraEditors.SimpleButton _saveResponsibilityZoneNotesSimpleButton;
        private DevExpress.XtraGrid.GridControl _phonesGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView _phonesGridView;
        private DevExpress.XtraGrid.Columns.GridColumn _phoneTypeGridColumn;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox _phoneTypeComboBox;
        private DevExpress.XtraGrid.Columns.GridColumn _phoneNumberGridColumn;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit _phoneNumberTextEdit;
        private DevExpress.XtraEditors.SplitContainerControl _responsibilityZonesSplitContainerControl;
        private DevExpress.XtraLayout.LayoutControl _responsibilityZoneLayoutControl;
        private DevExpress.XtraEditors.TextEdit _responsibilityZoneNameTextEdit;
        private DevExpress.XtraEditors.TextEdit _responsibilityZoneOYTextEdit;
        private DevExpress.XtraEditors.TextEdit _responsibilityZoneNTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem _responsibilityZoneNLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem _responsibilityZoneOYLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem _responsibilityZoneNameLayoutControlItem;
        private DevExpress.XtraEditors.PanelControl _saveResponsibilityZonePanelControl;
        private DevExpress.XtraEditors.SimpleButton _saveResponsibilityZoneSimpleButton;
        private DevExpress.XtraEditors.TextEdit _responsibilityZoneRoomTextEdit;
        private DevExpress.XtraEditors.TextEdit _responsibilityZonePorchTextEdit;
        private DevExpress.XtraEditors.TextEdit _responsibilityZoneBuildingTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem _responsibilityZoneBuildingLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem _responsibilityZonePorchLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem _responsibilityZoneRoomLayoutControlItem;
        private DevExpress.XtraGrid.Columns.GridColumn _personStatusGridColumn;
        private DevExpress.XtraBars.PopupMenu _popupMenu;
        private DevExpress.XtraBars.BarManager _barManager;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem _produceReportBarButtonItem;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn _shipIdLayoutViewColumn;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn1;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField__shipNameLayoutViewColumn_1;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField__shipIndexLayoutViewColumn_1;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField__shipRiverLayoutViewColumn_1;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField__shipLineLayoutViewColumn;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField__shipConditionLayoutViewColumn_1;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewCard layoutViewCard1;
        private DevExpress.XtraBars.BarButtonItem _produceReportWithPersonsBarButtonItem;
    }
}
namespace Curriculum
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            this._navBarControl = new DevExpress.XtraNavBar.NavBarControl();
            this._listsNavBarGroup = new DevExpress.XtraNavBar.NavBarGroup();
            this._riversNavBarItem = new DevExpress.XtraNavBar.NavBarItem();
            this._shipLinesNavBarItem = new DevExpress.XtraNavBar.NavBarItem();
            this._conditionsNavBarItem = new DevExpress.XtraNavBar.NavBarItem();
            this._toolsNavBarItem = new DevExpress.XtraNavBar.NavBarItem();
            this._toolCategoriesNavBarItem = new DevExpress.XtraNavBar.NavBarItem();
            this._toolSubCategoriesNavBarItem = new DevExpress.XtraNavBar.NavBarItem();
            this._phoneTypesNavBarItem = new DevExpress.XtraNavBar.NavBarItem();
            this._qualificationsNavBarItem = new DevExpress.XtraNavBar.NavBarItem();
            this._educationsNavBarItem = new DevExpress.XtraNavBar.NavBarItem();
            this._degreesNavBarItem = new DevExpress.XtraNavBar.NavBarItem();
            this._positionsNavBarItem = new DevExpress.XtraNavBar.NavBarItem();
            this._departmentsNavBarItem = new DevExpress.XtraNavBar.NavBarItem();
            this._affiliationsnavBarItem = new DevExpress.XtraNavBar.NavBarItem();
            this._statusesNavBarItem = new DevExpress.XtraNavBar.NavBarItem();
            this._categoriesNavBarItem = new DevExpress.XtraNavBar.NavBarItem();
            this._addresseesNavBarItem = new DevExpress.XtraNavBar.NavBarItem();
            this._unitsNavBarItem = new DevExpress.XtraNavBar.NavBarItem();
            this._partsNavBarItem = new DevExpress.XtraNavBar.NavBarItem();
            this._lessonTypesNavBarItem = new DevExpress.XtraNavBar.NavBarItem();
            this._userPositionsNavBarItem = new DevExpress.XtraNavBar.NavBarItem();
            this._CommissionMembersNavBarItem = new DevExpress.XtraNavBar.NavBarItem();
            this._userDepartmentsNavBarItem = new DevExpress.XtraNavBar.NavBarItem();
            this._usersGroupsNavBarItem = new DevExpress.XtraNavBar.NavBarItem();
            this._rolesNavBarItem = new DevExpress.XtraNavBar.NavBarItem();
            this._scheduleNavBarGroup = new DevExpress.XtraNavBar.NavBarGroup();
            this._scheduleNavBarItem = new DevExpress.XtraNavBar.NavBarItem();
            this._holidaysNavBarItem = new DevExpress.XtraNavBar.NavBarItem();
            this._dayScheduleNavBarItem = new DevExpress.XtraNavBar.NavBarItem();
            this._exceptionScheduleNavBarItem = new DevExpress.XtraNavBar.NavBarItem();
            this._appearanceNavBarGroup = new DevExpress.XtraNavBar.NavBarGroup();
            this._skinNavBarItem = new DevExpress.XtraNavBar.NavBarItem();
            this._lessonTypeColorsNavBarItem = new DevExpress.XtraNavBar.NavBarItem();
            this._othersNavBarGroup = new DevExpress.XtraNavBar.NavBarGroup();
            this._usersNavBarItem = new DevExpress.XtraNavBar.NavBarItem();
            this._aliasesNavBarItem = new DevExpress.XtraNavBar.NavBarItem();
            this._dbConnectionNavBarItem = new DevExpress.XtraNavBar.NavBarItem();
            this._parametersNavBarItem = new DevExpress.XtraNavBar.NavBarItem();
            this._updateNavBarItem = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarItem1 = new DevExpress.XtraNavBar.NavBarItem();
            this._splitterControl = new DevExpress.XtraEditors.SplitterControl();
            this._panelControl = new DevExpress.XtraEditors.PanelControl();
            this._hintLabelControl = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this._navBarControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._panelControl)).BeginInit();
            this._panelControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // _navBarControl
            // 
            this._navBarControl.ActiveGroup = this._listsNavBarGroup;
            this._navBarControl.ContentButtonHint = null;
            this._navBarControl.Dock = System.Windows.Forms.DockStyle.Left;
            this._navBarControl.Groups.AddRange(new DevExpress.XtraNavBar.NavBarGroup[] {
            this._listsNavBarGroup,
            this._scheduleNavBarGroup,
            this._appearanceNavBarGroup,
            this._othersNavBarGroup});
            this._navBarControl.Items.AddRange(new DevExpress.XtraNavBar.NavBarItem[] {
            this._riversNavBarItem,
            this._shipLinesNavBarItem,
            this._conditionsNavBarItem,
            this._toolsNavBarItem,
            this._toolCategoriesNavBarItem,
            this._toolSubCategoriesNavBarItem,
            this._statusesNavBarItem,
            this.navBarItem1,
            this._qualificationsNavBarItem,
            this._degreesNavBarItem,
            this._departmentsNavBarItem,
            this._positionsNavBarItem,
            this._affiliationsnavBarItem,
            this._educationsNavBarItem,
            this._categoriesNavBarItem,
            this._phoneTypesNavBarItem,
            this._addresseesNavBarItem,
            this._lessonTypesNavBarItem,
            this._lessonTypeColorsNavBarItem,
            this._aliasesNavBarItem,
            this._dbConnectionNavBarItem,
            this._skinNavBarItem,
            this._usersNavBarItem,
            this._unitsNavBarItem,
            this._partsNavBarItem,
            this._CommissionMembersNavBarItem,
            this._dayScheduleNavBarItem,
            this._holidaysNavBarItem,
            this._exceptionScheduleNavBarItem,
            this._parametersNavBarItem,
            this._scheduleNavBarItem,
            this._updateNavBarItem,
            this._rolesNavBarItem,
            this._userPositionsNavBarItem,
            this._userDepartmentsNavBarItem,
            this._usersGroupsNavBarItem});
            this._navBarControl.LinkSelectionMode = DevExpress.XtraNavBar.LinkSelectionModeType.OneInControl;
            this._navBarControl.Location = new System.Drawing.Point(0, 0);
            this._navBarControl.Name = "_navBarControl";
            this._navBarControl.OptionsNavPane.ExpandedWidth = 225;
            this._navBarControl.Size = new System.Drawing.Size(225, 755);
            this._navBarControl.TabIndex = 0;
            this._navBarControl.View = new DevExpress.XtraNavBar.ViewInfo.SkinExplorerBarViewInfoRegistrator();
            this._navBarControl.LinkPressed += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.NavBarControlLinkClicked);
            // 
            // _listsNavBarGroup
            // 
            this._listsNavBarGroup.Caption = "������";
            this._listsNavBarGroup.Expanded = true;
            this._listsNavBarGroup.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this._riversNavBarItem),
            new DevExpress.XtraNavBar.NavBarItemLink(this._shipLinesNavBarItem),
            new DevExpress.XtraNavBar.NavBarItemLink(this._conditionsNavBarItem),
            new DevExpress.XtraNavBar.NavBarItemLink(this._toolsNavBarItem),
            new DevExpress.XtraNavBar.NavBarItemLink(this._toolCategoriesNavBarItem),
            new DevExpress.XtraNavBar.NavBarItemLink(this._toolSubCategoriesNavBarItem),
            new DevExpress.XtraNavBar.NavBarItemLink(this._phoneTypesNavBarItem),
            new DevExpress.XtraNavBar.NavBarItemLink(this._qualificationsNavBarItem),
            new DevExpress.XtraNavBar.NavBarItemLink(this._educationsNavBarItem),
            new DevExpress.XtraNavBar.NavBarItemLink(this._degreesNavBarItem),
            new DevExpress.XtraNavBar.NavBarItemLink(this._positionsNavBarItem),
            new DevExpress.XtraNavBar.NavBarItemLink(this._departmentsNavBarItem),
            new DevExpress.XtraNavBar.NavBarItemLink(this._affiliationsnavBarItem),
            new DevExpress.XtraNavBar.NavBarItemLink(this._statusesNavBarItem),
            new DevExpress.XtraNavBar.NavBarItemLink(this._categoriesNavBarItem),
            new DevExpress.XtraNavBar.NavBarItemLink(this._addresseesNavBarItem),
            new DevExpress.XtraNavBar.NavBarItemLink(this._unitsNavBarItem),
            new DevExpress.XtraNavBar.NavBarItemLink(this._partsNavBarItem),
            new DevExpress.XtraNavBar.NavBarItemLink(this._lessonTypesNavBarItem),
            new DevExpress.XtraNavBar.NavBarItemLink(this._userPositionsNavBarItem),
            new DevExpress.XtraNavBar.NavBarItemLink(this._CommissionMembersNavBarItem),
            new DevExpress.XtraNavBar.NavBarItemLink(this._userDepartmentsNavBarItem),
            new DevExpress.XtraNavBar.NavBarItemLink(this._usersGroupsNavBarItem),
            new DevExpress.XtraNavBar.NavBarItemLink(this._rolesNavBarItem)});
            this._listsNavBarGroup.Name = "_listsNavBarGroup";
            this._listsNavBarGroup.SelectedLinkIndex = 24;
            this._listsNavBarGroup.Tag = "������#r";
            // 
            // _riversNavBarItem
            // 
            this._riversNavBarItem.Caption = "����";
            this._riversNavBarItem.Name = "_riversNavBarItem";
            this._riversNavBarItem.SmallImage = ((System.Drawing.Image)(resources.GetObject("_riversNavBarItem.SmallImage")));
            this._riversNavBarItem.Tag = "����#r";
            // 
            // _shipLinesNavBarItem
            // 
            this._shipLinesNavBarItem.Caption = "�����������";
            this._shipLinesNavBarItem.Name = "_shipLinesNavBarItem";
            this._shipLinesNavBarItem.SmallImage = ((System.Drawing.Image)(resources.GetObject("_shipLinesNavBarItem.SmallImage")));
            this._shipLinesNavBarItem.Tag = "�����������#r";
            // 
            // _conditionsNavBarItem
            // 
            this._conditionsNavBarItem.Caption = "�������";
            this._conditionsNavBarItem.Name = "_conditionsNavBarItem";
            this._conditionsNavBarItem.SmallImage = ((System.Drawing.Image)(resources.GetObject("_conditionsNavBarItem.SmallImage")));
            this._conditionsNavBarItem.Tag = "�������#r";
            // 
            // _toolsNavBarItem
            // 
            this._toolsNavBarItem.Caption = "��������";
            this._toolsNavBarItem.Name = "_toolsNavBarItem";
            this._toolsNavBarItem.SmallImage = ((System.Drawing.Image)(resources.GetObject("_toolsNavBarItem.SmallImage")));
            this._toolsNavBarItem.Tag = "��������#r";
            // 
            // _toolCategoriesNavBarItem
            // 
            this._toolCategoriesNavBarItem.Caption = "��������� �������";
            this._toolCategoriesNavBarItem.Name = "_toolCategoriesNavBarItem";
            this._toolCategoriesNavBarItem.SmallImage = ((System.Drawing.Image)(resources.GetObject("_toolCategoriesNavBarItem.SmallImage")));
            this._toolCategoriesNavBarItem.Tag = "��������� �������#r";
            // 
            // _toolSubCategoriesNavBarItem
            // 
            this._toolSubCategoriesNavBarItem.Caption = "������������ �������";
            this._toolSubCategoriesNavBarItem.Name = "_toolSubCategoriesNavBarItem";
            this._toolSubCategoriesNavBarItem.SmallImage = ((System.Drawing.Image)(resources.GetObject("_toolSubCategoriesNavBarItem.SmallImage")));
            this._toolSubCategoriesNavBarItem.Tag = "������������ �������#r";
            // 
            // _phoneTypesNavBarItem
            // 
            this._phoneTypesNavBarItem.Caption = "���� ���������";
            this._phoneTypesNavBarItem.Name = "_phoneTypesNavBarItem";
            this._phoneTypesNavBarItem.SmallImage = ((System.Drawing.Image)(resources.GetObject("_phoneTypesNavBarItem.SmallImage")));
            this._phoneTypesNavBarItem.Tag = "���� ���������#r";
            // 
            // _qualificationsNavBarItem
            // 
            this._qualificationsNavBarItem.Caption = "������������";
            this._qualificationsNavBarItem.Name = "_qualificationsNavBarItem";
            this._qualificationsNavBarItem.SmallImage = ((System.Drawing.Image)(resources.GetObject("_qualificationsNavBarItem.SmallImage")));
            this._qualificationsNavBarItem.Tag = "������������#r";
            // 
            // _educationsNavBarItem
            // 
            this._educationsNavBarItem.Caption = "�����������";
            this._educationsNavBarItem.Name = "_educationsNavBarItem";
            this._educationsNavBarItem.SmallImage = ((System.Drawing.Image)(resources.GetObject("_educationsNavBarItem.SmallImage")));
            this._educationsNavBarItem.Tag = "�����������#r";
            // 
            // _degreesNavBarItem
            // 
            this._degreesNavBarItem.Caption = "������ �������";
            this._degreesNavBarItem.Name = "_degreesNavBarItem";
            this._degreesNavBarItem.SmallImage = ((System.Drawing.Image)(resources.GetObject("_degreesNavBarItem.SmallImage")));
            this._degreesNavBarItem.Tag = "������ �������#r";
            // 
            // _positionsNavBarItem
            // 
            this._positionsNavBarItem.Caption = "���������";
            this._positionsNavBarItem.Name = "_positionsNavBarItem";
            this._positionsNavBarItem.SmallImage = ((System.Drawing.Image)(resources.GetObject("_positionsNavBarItem.SmallImage")));
            this._positionsNavBarItem.Tag = "���������#r";
            // 
            // _departmentsNavBarItem
            // 
            this._departmentsNavBarItem.Caption = "���������";
            this._departmentsNavBarItem.Name = "_departmentsNavBarItem";
            this._departmentsNavBarItem.SmallImage = ((System.Drawing.Image)(resources.GetObject("_departmentsNavBarItem.SmallImage")));
            this._departmentsNavBarItem.Tag = "���������#r";
            // 
            // _affiliationsnavBarItem
            // 
            this._affiliationsnavBarItem.Caption = "��������������";
            this._affiliationsnavBarItem.Name = "_affiliationsnavBarItem";
            this._affiliationsnavBarItem.SmallImage = ((System.Drawing.Image)(resources.GetObject("_affiliationsnavBarItem.SmallImage")));
            this._affiliationsnavBarItem.Tag = "��������������#r";
            // 
            // _statusesNavBarItem
            // 
            this._statusesNavBarItem.Caption = "�������";
            this._statusesNavBarItem.Name = "_statusesNavBarItem";
            this._statusesNavBarItem.SmallImage = ((System.Drawing.Image)(resources.GetObject("_statusesNavBarItem.SmallImage")));
            this._statusesNavBarItem.Tag = "�������#r";
            // 
            // _categoriesNavBarItem
            // 
            this._categoriesNavBarItem.Caption = "��������� ���������";
            this._categoriesNavBarItem.Name = "_categoriesNavBarItem";
            this._categoriesNavBarItem.SmallImage = ((System.Drawing.Image)(resources.GetObject("_categoriesNavBarItem.SmallImage")));
            this._categoriesNavBarItem.Tag = "��������� ���������#r";
            // 
            // _addresseesNavBarItem
            // 
            this._addresseesNavBarItem.Caption = "��������";
            this._addresseesNavBarItem.Name = "_addresseesNavBarItem";
            this._addresseesNavBarItem.SmallImage = ((System.Drawing.Image)(resources.GetObject("_addresseesNavBarItem.SmallImage")));
            this._addresseesNavBarItem.Tag = "��������#r";
            // 
            // _unitsNavBarItem
            // 
            this._unitsNavBarItem.Caption = "�������";
            this._unitsNavBarItem.Name = "_unitsNavBarItem";
            this._unitsNavBarItem.SmallImage = global::Curriculum.Properties.Resources.Bullet16;
            this._unitsNavBarItem.Tag = "�������#r";
            // 
            // _partsNavBarItem
            // 
            this._partsNavBarItem.Caption = "�����";
            this._partsNavBarItem.Name = "_partsNavBarItem";
            this._partsNavBarItem.SmallImage = global::Curriculum.Properties.Resources.Bullet16;
            this._partsNavBarItem.Tag = "�����#r";
            // 
            // _lessonTypesNavBarItem
            // 
            this._lessonTypesNavBarItem.Caption = "���� �������";
            this._lessonTypesNavBarItem.Name = "_lessonTypesNavBarItem";
            this._lessonTypesNavBarItem.SmallImage = ((System.Drawing.Image)(resources.GetObject("_lessonTypesNavBarItem.SmallImage")));
            this._lessonTypesNavBarItem.Tag = "���� �������#r";
            // 
            // _userPositionsNavBarItem
            // 
            this._userPositionsNavBarItem.Caption = "��������� ������������� ���";
            this._userPositionsNavBarItem.Name = "_userPositionsNavBarItem";
            this._userPositionsNavBarItem.SmallImage = global::Curriculum.Properties.Resources.Bullet16;
            // 
            // _CommissionMembersNavBarItem
            // 
            this._CommissionMembersNavBarItem.Caption = "����� ��������";
            this._CommissionMembersNavBarItem.Name = "_CommissionMembersNavBarItem";
            this._CommissionMembersNavBarItem.SmallImage = global::Curriculum.Properties.Resources.Bullet16;
            this._CommissionMembersNavBarItem.Tag = "����� ��������#r";
            // 
            // _userDepartmentsNavBarItem
            // 
            this._userDepartmentsNavBarItem.Caption = "������ ������������� ���";
            this._userDepartmentsNavBarItem.Name = "_userDepartmentsNavBarItem";
            this._userDepartmentsNavBarItem.SmallImage = global::Curriculum.Properties.Resources.Bullet16;
            // 
            // _usersGroupsNavBarItem
            // 
            this._usersGroupsNavBarItem.Caption = "������ �������������";
            this._usersGroupsNavBarItem.Name = "_usersGroupsNavBarItem";
            this._usersGroupsNavBarItem.SmallImage = global::Curriculum.Properties.Resources.Bullet16;
            this._usersGroupsNavBarItem.Tag = "������ �������������#r";
            // 
            // _rolesNavBarItem
            // 
            this._rolesNavBarItem.Caption = "����";
            this._rolesNavBarItem.Name = "_rolesNavBarItem";
            this._rolesNavBarItem.SmallImage = global::Curriculum.Properties.Resources.Bullet16;
            this._rolesNavBarItem.Tag = "����#r";
            // 
            // _scheduleNavBarGroup
            // 
            this._scheduleNavBarGroup.Caption = "����������";
            this._scheduleNavBarGroup.Expanded = true;
            this._scheduleNavBarGroup.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this._scheduleNavBarItem),
            new DevExpress.XtraNavBar.NavBarItemLink(this._holidaysNavBarItem),
            new DevExpress.XtraNavBar.NavBarItemLink(this._dayScheduleNavBarItem),
            new DevExpress.XtraNavBar.NavBarItemLink(this._exceptionScheduleNavBarItem)});
            this._scheduleNavBarGroup.Name = "_scheduleNavBarGroup";
            this._scheduleNavBarGroup.Tag = "����������#r";
            // 
            // _scheduleNavBarItem
            // 
            this._scheduleNavBarItem.Caption = "����������";
            this._scheduleNavBarItem.Name = "_scheduleNavBarItem";
            this._scheduleNavBarItem.SmallImage = global::Curriculum.Properties.Resources.Schedule16;
            this._scheduleNavBarItem.Tag = "����������#r";
            // 
            // _holidaysNavBarItem
            // 
            this._holidaysNavBarItem.Caption = "���������";
            this._holidaysNavBarItem.Name = "_holidaysNavBarItem";
            this._holidaysNavBarItem.SmallImage = global::Curriculum.Properties.Resources.Balloons16;
            this._holidaysNavBarItem.Tag = "���������#r";
            // 
            // _dayScheduleNavBarItem
            // 
            this._dayScheduleNavBarItem.Caption = "������� ����";
            this._dayScheduleNavBarItem.Name = "_dayScheduleNavBarItem";
            this._dayScheduleNavBarItem.SmallImage = global::Curriculum.Properties.Resources.CalendarWeek16;
            this._dayScheduleNavBarItem.Tag = "������� ����#r";
            // 
            // _exceptionScheduleNavBarItem
            // 
            this._exceptionScheduleNavBarItem.Caption = "����������";
            this._exceptionScheduleNavBarItem.Name = "_exceptionScheduleNavBarItem";
            this._exceptionScheduleNavBarItem.SmallImage = global::Curriculum.Properties.Resources.CalendarDay16;
            this._exceptionScheduleNavBarItem.Tag = "����������#r";
            // 
            // _appearanceNavBarGroup
            // 
            this._appearanceNavBarGroup.Caption = "������� ���";
            this._appearanceNavBarGroup.Expanded = true;
            this._appearanceNavBarGroup.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this._skinNavBarItem),
            new DevExpress.XtraNavBar.NavBarItemLink(this._lessonTypeColorsNavBarItem)});
            this._appearanceNavBarGroup.Name = "_appearanceNavBarGroup";
            this._appearanceNavBarGroup.Tag = "������� ���#r";
            // 
            // _skinNavBarItem
            // 
            this._skinNavBarItem.Caption = "����";
            this._skinNavBarItem.Name = "_skinNavBarItem";
            this._skinNavBarItem.SmallImage = global::Curriculum.Properties.Resources.Themes16;
            this._skinNavBarItem.Tag = "����#r";
            // 
            // _lessonTypeColorsNavBarItem
            // 
            this._lessonTypeColorsNavBarItem.Caption = "����� �������";
            this._lessonTypeColorsNavBarItem.Name = "_lessonTypeColorsNavBarItem";
            this._lessonTypeColorsNavBarItem.SmallImage = global::Curriculum.Properties.Resources.LessonColor16;
            this._lessonTypeColorsNavBarItem.Tag = "����� �������#r";
            // 
            // _othersNavBarGroup
            // 
            this._othersNavBarGroup.Caption = "������";
            this._othersNavBarGroup.Expanded = true;
            this._othersNavBarGroup.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this._usersNavBarItem),
            new DevExpress.XtraNavBar.NavBarItemLink(this._aliasesNavBarItem),
            new DevExpress.XtraNavBar.NavBarItemLink(this._dbConnectionNavBarItem),
            new DevExpress.XtraNavBar.NavBarItemLink(this._parametersNavBarItem),
            new DevExpress.XtraNavBar.NavBarItemLink(this._updateNavBarItem)});
            this._othersNavBarGroup.Name = "_othersNavBarGroup";
            this._othersNavBarGroup.Tag = "������#r";
            // 
            // _usersNavBarItem
            // 
            this._usersNavBarItem.Caption = "������������";
            this._usersNavBarItem.Name = "_usersNavBarItem";
            this._usersNavBarItem.SmallImage = global::Curriculum.Properties.Resources.Users16;
            this._usersNavBarItem.Tag = "������������#r";
            // 
            // _aliasesNavBarItem
            // 
            this._aliasesNavBarItem.Caption = "����������";
            this._aliasesNavBarItem.Name = "_aliasesNavBarItem";
            this._aliasesNavBarItem.SmallImage = global::Curriculum.Properties.Resources.Aliases16;
            this._aliasesNavBarItem.Tag = "����������#r";
            // 
            // _dbConnectionNavBarItem
            // 
            this._dbConnectionNavBarItem.Caption = "����������� � ��";
            this._dbConnectionNavBarItem.Name = "_dbConnectionNavBarItem";
            this._dbConnectionNavBarItem.SmallImage = global::Curriculum.Properties.Resources.Database16;
            this._dbConnectionNavBarItem.Visible = false;
            // 
            // _parametersNavBarItem
            // 
            this._parametersNavBarItem.AppearancePressed.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Underline);
            this._parametersNavBarItem.AppearancePressed.Options.UseFont = true;
            this._parametersNavBarItem.Caption = "���������";
            this._parametersNavBarItem.Name = "_parametersNavBarItem";
            this._parametersNavBarItem.SmallImage = global::Curriculum.Properties.Resources.Settings161;
            this._parametersNavBarItem.Tag = "���������#r";
            // 
            // _updateNavBarItem
            // 
            this._updateNavBarItem.Caption = "����������";
            this._updateNavBarItem.Name = "_updateNavBarItem";
            this._updateNavBarItem.SmallImage = global::Curriculum.Properties.Resources.Update16;
            this._updateNavBarItem.Tag = "����������#r";
            // 
            // navBarItem1
            // 
            this.navBarItem1.Caption = "navBarItem1";
            this.navBarItem1.Name = "navBarItem1";
            // 
            // _splitterControl
            // 
            this._splitterControl.Location = new System.Drawing.Point(225, 0);
            this._splitterControl.Name = "_splitterControl";
            this._splitterControl.Size = new System.Drawing.Size(6, 755);
            this._splitterControl.TabIndex = 1;
            this._splitterControl.TabStop = false;
            // 
            // _panelControl
            // 
            this._panelControl.Controls.Add(this._hintLabelControl);
            this._panelControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._panelControl.Location = new System.Drawing.Point(231, 0);
            this._panelControl.Name = "_panelControl";
            this._panelControl.Size = new System.Drawing.Size(740, 755);
            this._panelControl.TabIndex = 2;
            // 
            // _hintLabelControl
            // 
            this._hintLabelControl.Appearance.Image = global::Curriculum.Properties.Resources.Left24;
            this._hintLabelControl.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this._hintLabelControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._hintLabelControl.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this._hintLabelControl.Location = new System.Drawing.Point(2, 2);
            this._hintLabelControl.Name = "_hintLabelControl";
            this._hintLabelControl.Padding = new System.Windows.Forms.Padding(20);
            this._hintLabelControl.Size = new System.Drawing.Size(736, 751);
            this._hintLabelControl.TabIndex = 1;
            this._hintLabelControl.Text = "�������� ����������� ������ ���������";
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(971, 755);
            this.Controls.Add(this._panelControl);
            this.Controls.Add(this._splitterControl);
            this.Controls.Add(this._navBarControl);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SettingsForm";
            this.Tag = "������ ����������\\������������\\���������#r";
            this.Text = "���������";
            ((System.ComponentModel.ISupportInitialize)(this._navBarControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._panelControl)).EndInit();
            this._panelControl.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraNavBar.NavBarControl _navBarControl;
        private DevExpress.XtraNavBar.NavBarGroup _listsNavBarGroup;
        private DevExpress.XtraNavBar.NavBarItem _riversNavBarItem;
        private DevExpress.XtraNavBar.NavBarItem _shipLinesNavBarItem;
        private DevExpress.XtraNavBar.NavBarItem _conditionsNavBarItem;
        private DevExpress.XtraNavBar.NavBarItem _toolsNavBarItem;
        private DevExpress.XtraNavBar.NavBarItem _toolCategoriesNavBarItem;
        private DevExpress.XtraEditors.SplitterControl _splitterControl;
        private DevExpress.XtraEditors.PanelControl _panelControl;
        private DevExpress.XtraNavBar.NavBarGroup _othersNavBarGroup;
        private DevExpress.XtraNavBar.NavBarItem _toolSubCategoriesNavBarItem;
        private DevExpress.XtraNavBar.NavBarItem _statusesNavBarItem;
        private DevExpress.XtraNavBar.NavBarItem _qualificationsNavBarItem;
        private DevExpress.XtraNavBar.NavBarItem _degreesNavBarItem;
        private DevExpress.XtraNavBar.NavBarItem _departmentsNavBarItem;
        private DevExpress.XtraNavBar.NavBarItem _positionsNavBarItem;
        private DevExpress.XtraNavBar.NavBarItem _affiliationsnavBarItem;
        private DevExpress.XtraNavBar.NavBarItem navBarItem1;
        private DevExpress.XtraNavBar.NavBarItem _educationsNavBarItem;
        private DevExpress.XtraNavBar.NavBarItem _categoriesNavBarItem;
        private DevExpress.XtraNavBar.NavBarItem _phoneTypesNavBarItem;
        private DevExpress.XtraNavBar.NavBarItem _addresseesNavBarItem;
        private DevExpress.XtraNavBar.NavBarItem _lessonTypesNavBarItem;
        private DevExpress.XtraNavBar.NavBarItem _lessonTypeColorsNavBarItem;
        private DevExpress.XtraEditors.LabelControl _hintLabelControl;
        private DevExpress.XtraNavBar.NavBarItem _aliasesNavBarItem;
        private DevExpress.XtraNavBar.NavBarItem _dbConnectionNavBarItem;
        private DevExpress.XtraNavBar.NavBarGroup _appearanceNavBarGroup;
        private DevExpress.XtraNavBar.NavBarItem _skinNavBarItem;
        private DevExpress.XtraNavBar.NavBarItem _usersNavBarItem;
        private DevExpress.XtraNavBar.NavBarItem _unitsNavBarItem;
        private DevExpress.XtraNavBar.NavBarItem _partsNavBarItem;
        private DevExpress.XtraNavBar.NavBarItem _CommissionMembersNavBarItem;
        private DevExpress.XtraNavBar.NavBarGroup _scheduleNavBarGroup;
        private DevExpress.XtraNavBar.NavBarItem _dayScheduleNavBarItem;
        private DevExpress.XtraNavBar.NavBarItem _holidaysNavBarItem;
        private DevExpress.XtraNavBar.NavBarItem _exceptionScheduleNavBarItem;
        private DevExpress.XtraNavBar.NavBarItem _parametersNavBarItem;
        private DevExpress.XtraNavBar.NavBarItem _scheduleNavBarItem;
        private DevExpress.XtraNavBar.NavBarItem _updateNavBarItem;
        private DevExpress.XtraNavBar.NavBarItem _rolesNavBarItem;
        private DevExpress.XtraNavBar.NavBarItem _userPositionsNavBarItem;
        private DevExpress.XtraNavBar.NavBarItem _userDepartmentsNavBarItem;
        private DevExpress.XtraNavBar.NavBarItem _usersGroupsNavBarItem;
    }
}
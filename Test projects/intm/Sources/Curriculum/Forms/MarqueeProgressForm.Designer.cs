﻿namespace Curriculum
{
    partial class MarqueeProgressForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._marqueeProgressBarControl = new DevExpress.XtraEditors.MarqueeProgressBarControl();
            ((System.ComponentModel.ISupportInitialize)(this._marqueeProgressBarControl.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // _marqueeProgressBarControl
            // 
            this._marqueeProgressBarControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._marqueeProgressBarControl.EditValue = "Текст...";
            this._marqueeProgressBarControl.Location = new System.Drawing.Point(0, 0);
            this._marqueeProgressBarControl.Name = "_marqueeProgressBarControl";
            this._marqueeProgressBarControl.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marqueeProgressBarControl.Properties.MarqueeAnimationSpeed = 50;
            this._marqueeProgressBarControl.Properties.ProgressAnimationMode = DevExpress.Utils.Drawing.ProgressAnimationMode.Cycle;
            this._marqueeProgressBarControl.Properties.ReadOnly = true;
            this._marqueeProgressBarControl.Properties.ShowTitle = true;
            this._marqueeProgressBarControl.Size = new System.Drawing.Size(500, 40);
            this._marqueeProgressBarControl.TabIndex = 2;
            // 
            // MarqueeProgressForm
            // 
            this.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(500, 40);
            this.ControlBox = false;
            this.Controls.Add(this._marqueeProgressBarControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.LookAndFeel.SkinName = "Black";
            this.Name = "MarqueeProgressForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MarqueeProgressForm";
            this.Tag = "MarqueeProgressForm#r";
            this.Load += new System.EventHandler(this.MarqueeProgressFormLoad);
            ((System.ComponentModel.ISupportInitialize)(this._marqueeProgressBarControl.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.MarqueeProgressBarControl _marqueeProgressBarControl;
    }
}
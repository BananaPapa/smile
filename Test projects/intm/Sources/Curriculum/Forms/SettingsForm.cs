using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Linq;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Curriculum.Controls;
using Curriculum.Settings;
using DevExpress.XtraEditors;
using DevExpress.XtraNavBar;
using Eureca.Common.Settings;
using Eureca.Integrator.Common;
using Eureca.Professional.BaseComponents;
using Eureca.Integrator.Extensions;

namespace Curriculum
{
    /// <summary>
    /// Represents settings form.
    /// </summary>
    public partial class SettingsForm : FormWithAccessAreas, IAliasesHolder
    {
        /// <summary>
        /// Contains control to navigation bar item associations.
        /// </summary>
        private readonly Dictionary<NavBarItem, Control> _controls;

        private Control _topMostControl;
        /// <summary>
        /// Initializes new instance of settings form.
        /// </summary>
        public SettingsForm()
        {
            InitializeComponent();

            #region Init Associations

            // Init control to navigation bar item associations.
            _controls = new Dictionary<NavBarItem, Control>
                            {
                {_riversNavBarItem, new NameOwnersControl(typeof (River)){Tag = _riversNavBarItem.Caption,HierarchyParent = this,PrefixTag = _riversNavBarItem.GetNavBarMenuPath()}},
                {_shipLinesNavBarItem, new NameOwnersControl(typeof (ShipLine)){Tag = _shipLinesNavBarItem.Caption,HierarchyParent = this,PrefixTag = _shipLinesNavBarItem.GetNavBarMenuPath()}},
                {_conditionsNavBarItem, new NameOwnersControl(typeof (Condition)){Tag = _conditionsNavBarItem.Caption,HierarchyParent = this,PrefixTag = _conditionsNavBarItem.GetNavBarMenuPath()}},
                {_toolsNavBarItem, new NameOwnersControl(typeof (Tool)){Tag = _toolsNavBarItem.Caption,HierarchyParent = this,PrefixTag = _toolsNavBarItem.GetNavBarMenuPath()}},
                {_toolCategoriesNavBarItem, new NameOwnersControl(typeof (ToolCategory)){Tag = _toolCategoriesNavBarItem.Caption,HierarchyParent = this,PrefixTag = _toolCategoriesNavBarItem.GetNavBarMenuPath()}},
                {_toolSubCategoriesNavBarItem, new NameOwnersControl(typeof (ToolSubCategory)){Tag = _toolSubCategoriesNavBarItem.Caption,HierarchyParent = this,PrefixTag = _toolSubCategoriesNavBarItem.GetNavBarMenuPath()}},
                {_phoneTypesNavBarItem, new NameOwnersControl(typeof (PhoneType)){Tag = _phoneTypesNavBarItem.Caption,HierarchyParent = this,PrefixTag = _phoneTypesNavBarItem.GetNavBarMenuPath()}},
                {_qualificationsNavBarItem, new NameOwnersControl(typeof (Qualification)){Tag = _qualificationsNavBarItem.Caption,HierarchyParent = this,PrefixTag = _qualificationsNavBarItem.GetNavBarMenuPath()}},
                {_educationsNavBarItem, new NameOwnersControl(typeof (Education)){Tag = _educationsNavBarItem.Caption,HierarchyParent = this,PrefixTag = _educationsNavBarItem.GetNavBarMenuPath()}},
                {_degreesNavBarItem, new NameOwnersControl(typeof (Degree)){Tag = _degreesNavBarItem.Caption,HierarchyParent = this,PrefixTag = _degreesNavBarItem.GetNavBarMenuPath()}},
                {_positionsNavBarItem, new NameOwnersControl(typeof (Position)){Tag = _positionsNavBarItem.Caption,HierarchyParent = this,PrefixTag = _positionsNavBarItem.GetNavBarMenuPath()}},
                {_departmentsNavBarItem, new NameOwnersControl(typeof (Department)){Tag = _departmentsNavBarItem.Caption,HierarchyParent = this,PrefixTag = _departmentsNavBarItem.GetNavBarMenuPath()}},
                {_affiliationsnavBarItem, new NameOwnersControl(typeof (Affiliation)){Tag = _affiliationsnavBarItem.Caption,HierarchyParent = this,PrefixTag = _affiliationsnavBarItem.GetNavBarMenuPath()}},
                {_statusesNavBarItem, new NameOwnersControl(typeof (Statuse)){Tag = _statusesNavBarItem.Caption,HierarchyParent = this,PrefixTag = _statusesNavBarItem.GetNavBarMenuPath()}},
                {_categoriesNavBarItem, new NameOwnersControl(typeof (Category)){Tag = _categoriesNavBarItem.Caption,HierarchyParent = this,PrefixTag = _categoriesNavBarItem.GetNavBarMenuPath()}},
                {_addresseesNavBarItem, new NameOwnersControl(typeof (Addressee)){Tag = _addresseesNavBarItem.Caption,HierarchyParent = this,PrefixTag = _addresseesNavBarItem.GetNavBarMenuPath()}},
                {_unitsNavBarItem, new NameOwnersControl(typeof (Unit)){Tag = _unitsNavBarItem.Caption,HierarchyParent = this,PrefixTag = _unitsNavBarItem.GetNavBarMenuPath()}},
                {_partsNavBarItem, new NameOwnersControl(typeof (Part)){Tag = _partsNavBarItem.Caption,HierarchyParent = this,PrefixTag = _partsNavBarItem.GetNavBarMenuPath()}},
                {_lessonTypesNavBarItem, new NameOwnersControl(typeof (LessonType)){Tag = _lessonTypesNavBarItem.Caption,HierarchyParent = this,PrefixTag = _lessonTypesNavBarItem.GetNavBarMenuPath()}},
                {_lessonTypeColorsNavBarItem, new LessonTypeColorsControl(){HierarchyParent = this,PrefixTag = _lessonTypeColorsNavBarItem.GetNavBarMenuPath()}},
                {_aliasesNavBarItem, new AliasesControl(){HierarchyParent = this,PrefixTag = _aliasesNavBarItem.GetNavBarMenuPath()}},
                {_skinNavBarItem, new SkinsControl(){HierarchyParent = this,PrefixTag = _skinNavBarItem.GetNavBarMenuPath()}},
                {_usersNavBarItem, new UsersControl(){HierarchyParent = this,PrefixTag = _usersNavBarItem.GetNavBarMenuPath()}},
                {_userDepartmentsNavBarItem, new NameOwnersControl(typeof (UserDepartment)){Tag = _userDepartmentsNavBarItem.Caption,HierarchyParent = this,PrefixTag = _userDepartmentsNavBarItem.GetNavBarMenuPath()}},
                {_userPositionsNavBarItem, new NameOwnersControl(typeof (UserPosition)){Tag = _userPositionsNavBarItem.Caption,HierarchyParent = this,PrefixTag = _userPositionsNavBarItem.GetNavBarMenuPath()}},
                {_updateNavBarItem, new UpdatesSettingsControl(){RepositorySettings = RunnerApplication.UpdateSettingsContext}},
                {_CommissionMembersNavBarItem, new CommissionMembersControl(){HierarchyParent = this,PrefixTag = _CommissionMembersNavBarItem.GetNavBarMenuPath()}},
                {
                    _dbConnectionNavBarItem, new Label
                                                    {
                                                        Text = "� ����������...",
                                                        AutoSize = false,
                                                        TextAlign = ContentAlignment.MiddleCenter
                                                    }
                },
                {_parametersNavBarItem, new ParametersControl(){HierarchyParent = this,PrefixTag = _parametersNavBarItem.GetNavBarMenuPath()}},
                {_scheduleNavBarItem, new SchedulesControl(){HierarchyParent = this,PrefixTag = _scheduleNavBarItem.GetNavBarMenuPath()}},
                {_dayScheduleNavBarItem, new DaySchedulesControl(){HierarchyParent = this,PrefixTag = _dayScheduleNavBarItem.GetNavBarMenuPath()}},
                {_exceptionScheduleNavBarItem, new ExceptionSchedulesControl(){HierarchyParent = this,PrefixTag = _exceptionScheduleNavBarItem.GetNavBarMenuPath()}},
                {_holidaysNavBarItem, new HolidaysControl(){HierarchyParent = this,PrefixTag = _holidaysNavBarItem.GetNavBarMenuPath()}},
                {_rolesNavBarItem, new RolesListControl(){HierarchyParent = this,PrefixTag = _rolesNavBarItem.GetNavBarMenuPath()}},
                {_usersGroupsNavBarItem, new UsersGroupsList(){HierarchyParent = this,PrefixTag = _usersGroupsNavBarItem.GetNavBarMenuPath()}}};
            #endregion
            ApplyAliases();
        }

        #region IAliasesHolder Members

        public void ApplyAliases()
        {
            try
            {

                Aliase.ApplyAlias( "Rivers", a => _riversNavBarItem.Caption = a );
                Aliase.ApplyAlias( "ShipLines", a => { _shipLinesNavBarItem.Caption = a;
                                                         _controls[_shipLinesNavBarItem].Tag = a;
                });
                Aliase.ApplyAlias( "Statuses", a => { _statusesNavBarItem.Caption = a;
                                                        _controls[_statusesNavBarItem].Tag = a;
                } );
                Aliase.ApplyAlias( "Qualifications", a => { _qualificationsNavBarItem.Caption = a;
                                                              _controls[_qualificationsNavBarItem].Tag = a;
                } );
                Aliase.ApplyAlias( "Educations", a => _educationsNavBarItem.Caption = a );
                Aliase.ApplyAlias( "Degrees", a => { _degreesNavBarItem.Caption = a;
                                                       _controls[_degreesNavBarItem].Tag = a;
                } );
                Aliase.ApplyAlias( "Positions", a => { _positionsNavBarItem.Caption = a;
                                                         _controls[_positionsNavBarItem].Tag = a;
                } );
                Aliase.ApplyAlias( "Departments", a => { _departmentsNavBarItem.Caption = a;
                                                           _controls[_degreesNavBarItem].Tag = a;
                } );
                Aliase.ApplyAlias( "Affiliations", a => _affiliationsnavBarItem.Caption = a );
            }
            catch (DbException ex)
            {
                Program.HandleDbException(ex);
            }

            foreach (Control control in _controls.Values)
            {
                if (control is IAliasesHolder)
                {
                    ((IAliasesHolder) control).ApplyAliases();
                }
            }
        }

        #endregion

        private static Action GetRefreshAction(params object[] tables)
        {
            return () => Program.DbSingletone.Refresh(RefreshMode.OverwriteCurrentValues, tables);
        }

        /// <summary>
        /// Handles nav bar control link clicked.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void NavBarControlLinkClicked(object sender, NavBarLinkEventArgs e)
        {
            Control control = _controls.ContainsKey(e.Link.Item)
                                  ? _controls[e.Link.Item]
                                  : _hintLabelControl;
            
            if (_topMostControl is IApplySettings && control != _topMostControl && !(_topMostControl as IApplySettings).OnLoseFocus())
                    return;
            
            if (!_panelControl.Controls.Contains(control))
            {
                control.Dock = DockStyle.Fill;
                _panelControl.Controls.Add(control);
            }

            control.BringToFront();
            _topMostControl = control;
        }

        public override IEnumerable<ISKOCBCTDNBTIDED> GetHiddenChilds(bool withLazy)
        {
            List<ISKOCBCTDNBTIDED> hiddenChilds = new List<ISKOCBCTDNBTIDED>( );
            hiddenChilds.Add( new ControlInterfaceImplementation( _controls[_updateNavBarItem] ) );
            if (withLazy)
            {
                hiddenChilds.AddRange(_controls.Values.Where(control => control is ControlWithAccessAreas).Select(control => new ControlInterfaceImplementation(control)));
            }
            return hiddenChilds;
        }

    }
}
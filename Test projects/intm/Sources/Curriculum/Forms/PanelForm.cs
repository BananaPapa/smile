﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Curriculum.Properties;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using Eureca.Integrator.Common;

namespace Curriculum
{
    internal partial class PanelForm : FormWithAccessAreas
    {
        public PanelForm()
        {
            InitializeComponent();
        }

        public void ShowPersons()
        {
            var stateEdutionItems = new List<EducationStateItem>();
            IQueryable<Lesson> items = from lesson in Program.DbSingletone.Lessons
                                       where lesson.Person.Statuse.CreateProbationer &&
                                             (!lesson.Person.ExaminationDate.HasValue ||
                                              lesson.Person.ExaminationDate.Value >= DateTime.Now) &&
                                             lesson.Discipline.TestGuid != null &&
                                             lesson.LessonType.IsTest
                                       select lesson;
            foreach (Lesson item in items)
            {
                var newItem = new EducationStateItem
                                  {
                                      Person = item.Person,
                                      Discipline = item.Discipline,
                                      LessonType = item.LessonType,
                                      DateTime = item.Datetime,
                                      Duration = item.Duration,
                                      Comment = item.Comment,
                                  };
                //получение статуса
                int testCompleted = Program.ProfessionalDb.GetTestStatus(newItem.Discipline.TestGuid,
                                                                         newItem.Person.ProfessionalUserUniqueId);
                if (testCompleted == 1)
                {
                    newItem.State = "Завершен успешно";
                    newItem.Image = Resources.Checked16;
                }
                else if (newItem.DateTime < DateTime.Now)
                {
                    newItem.State = "Просрочен";
                    newItem.Image = Resources.Critical16;
                }
                else if (newItem.DateTime >= DateTime.Now)
                {
                    newItem.State = "В процессе";
                    newItem.Image = Resources.Todo16;
                }
                //получение максимальной статистики
                vMaxPassingTest testPassing =
                    Program.ProfessionalDb.vMaxPassingTests.Where(i => i.TestUniqueId == newItem.Discipline.TestGuid &&
                                                                       i.UserUniqueId ==
                                                                       newItem.Person.ProfessionalUserUniqueId).
                        FirstOrDefault();

                if (testPassing != null)
                {
                    newItem.PassingScore = testPassing.PassingScore != null
                                               ? testPassing.PassingScore.Value.ToString()
                                               : string.Empty;
                    newItem.Perc = testPassing.perc != null
                                       ? testPassing.perc.Value.ToString()
                                       : string.Empty;
                    newItem.Valuation = testPassing.Valuation != null
                                            ? testPassing.Valuation.Value.ToString()
                                            : string.Empty;
                    //newItem.CommentText = 
                    if (!testPassing.IsSupervise)
                        continue;
                }
                stateEdutionItems.Add(newItem);
            }

            _gridControl.DataSource = stateEdutionItems;
            _gridView.BestFitColumns();
        }

        private void GridViewRowStyle(object sender, RowStyleEventArgs e)
        {
            var view = sender as GridView;
            if (e.RowHandle >= 0)
            {
                if (view != null)
                {
                    var stateEducationItem = view.GetRow(e.RowHandle) as EducationStateItem;
                    if (stateEducationItem != null)
                    {
                        if (string.IsNullOrEmpty(stateEducationItem.State))
                            return;
                        if (stateEducationItem.State == "Просрочен")
                        {
                            e.Appearance.BackColor = Color.MistyRose;
                            e.Appearance.BackColor2 = Color.SeaShell;
                        }
                        if (stateEducationItem.State == "Завершен успешно")
                        {
                            e.Appearance.BackColor = Color.FromArgb(192, 255, 192);
                            e.Appearance.BackColor2 = Color.LightCyan;
                        }
                    }
                }
            }
        }

        private void PrintSimpleButtonClick(object sender, EventArgs e)
        {
            _gridControl.ShowPrintPreview();
        }

        private void PanelFormLoad(object sender, EventArgs e)
        {
            ShowPersons();
        }

        private void RefreshSimpleButtonClick(object sender, EventArgs e)
        {
            Program.DbSingletone.RefreshDataSource(typeof (Statuse), typeof (Discipline), typeof (Person), typeof (Lesson));

            ShowPersons();
        }

        private void PersonelSimpleButtonClick(object sender, EventArgs e)
        {
            // Check if the parent form is main form.
            if (ParentForm != null && ParentForm is StartForm)
            {
                var state = _gridView.GetFocusedRow() as EducationStateItem;

                // Chick if there is a person to edit.
                if (state != null)
                {
                    // Open or activate persons form and focus desired person.
                    ((PersonsForm)
                     ((StartForm) ParentForm).OpenOrActivateMdiChild(typeof (PersonsForm), () => new PersonsForm())).
                        FocusedPerson = state.Person;
                }
            }
        }

        private void CurriculumSimpleButtonClick(object sender, EventArgs e)
        {
            if (ParentForm != null && ParentForm is StartForm)
            {
                var state = _gridView.GetFocusedRow() as EducationStateItem;

                // Chick if there is a person to edit.
                if (state != null)
                {
                    var startForm = ( (StartForm)ParentForm );

                    var personForm = startForm.OpenOrActivateMdiChild( typeof( PersonsForm ), ( ) => new PersonsForm( ), false ) as PersonsForm;
                    // Open or activate lessons form for desired person.
                    startForm.OpenOrActivateMdiChild(typeof (LessonsForm),() => new LessonsForm(state.Person)
                                                                       {CurrentDate = state.DateTime.Date},
                                                                       args: state.Person);
                }
            }
        }

        public override IEnumerable<ISKOCBCTDNBTIDED> GetHiddenChilds( bool withLazy )
        {
            var hidden = new List<ISKOCBCTDNBTIDED>();
            if (withLazy)
            {
                var lessonsForm = new LessonsForm( null ){ManualFilter = true};
                hidden.Add(new ControlInterfaceImplementation(lessonsForm));
                var personsForm = new PersonsForm( ) { ManualFilter = true };
                hidden.Add( new ControlInterfaceImplementation( personsForm ) );
            }

            return Enumerable.Empty<ISKOCBCTDNBTIDED>();
        }

        #region Nested type: EducationStateItem

        internal class EducationStateItem
        {
            public Person Person { get; set; }
            public Discipline Discipline { get; set; }
            public LessonType LessonType { get; set; }
            public DateTime DateTime { get; set; }
            public int Duration { get; set; }
            public string Comment { get; set; }
            public string State { get; set; }
            public Image Image { get; set; }
            public string PassingScore { get; set; }
            public string Perc { get; set; }
            public string Valuation { get; set; }
        }

        #endregion
    }
}
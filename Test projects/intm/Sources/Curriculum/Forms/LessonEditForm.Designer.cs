namespace Curriculum
{
    partial class LessonEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.timeStart = new DevExpress.XtraEditors.TimeEdit();
            this.dtStart = new DevExpress.XtraEditors.DateEdit();
            this.lblStart = new System.Windows.Forms.Label();
            this.lblLabel = new System.Windows.Forms.Label();
            this._disciplineTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.lblSubject = new System.Windows.Forms.Label();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.label1 = new System.Windows.Forms.Label();
            this._descriptionMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this._lessonTypeAppointmentLabelEdit = new DevExpress.XtraScheduler.UI.AppointmentLabelEdit();
            this._isImportantCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this._spinEditDuration = new DevExpress.XtraEditors.SpinEdit();
            this._checkEditHasReminder = new DevExpress.XtraEditors.CheckEdit();
            this._durationEditTimeBeforeStart = new DevExpress.XtraScheduler.UI.DurationEdit();
            ((System.ComponentModel.ISupportInitialize)(this.timeStart.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtStart.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtStart.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._disciplineTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._descriptionMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._lessonTypeAppointmentLabelEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._isImportantCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._spinEditDuration.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._checkEditHasReminder.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._durationEditTimeBeforeStart.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // timeStart
            // 
            this.timeStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.timeStart.EditValue = new System.DateTime(2006, 3, 28, 0, 0, 0, 0);
            this.timeStart.Location = new System.Drawing.Point(249, 64);
            this.timeStart.Name = "timeStart";
            this.timeStart.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.timeStart.Size = new System.Drawing.Size(80, 20);
            this.timeStart.TabIndex = 16;
            this.timeStart.Tag = "������#r";
            // 
            // dtStart
            // 
            this.dtStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.dtStart.EditValue = new System.DateTime(2008, 6, 27, 0, 0, 0, 0);
            this.dtStart.Location = new System.Drawing.Point(147, 64);
            this.dtStart.Name = "dtStart";
            this.dtStart.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtStart.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtStart.Size = new System.Drawing.Size(96, 20);
            this.dtStart.TabIndex = 15;
            this.dtStart.Tag = "������#r";
            // 
            // lblStart
            // 
            this.lblStart.Location = new System.Drawing.Point(12, 67);
            this.lblStart.Name = "lblStart";
            this.lblStart.Size = new System.Drawing.Size(56, 18);
            this.lblStart.TabIndex = 24;
            this.lblStart.Tag = "������#r";
            this.lblStart.Text = "������:";
            // 
            // lblLabel
            // 
            this.lblLabel.Location = new System.Drawing.Point(12, 41);
            this.lblLabel.Name = "lblLabel";
            this.lblLabel.Size = new System.Drawing.Size(73, 19);
            this.lblLabel.TabIndex = 23;
            this.lblLabel.Tag = "��� �������#r";
            this.lblLabel.Text = "��� �������:";
            // 
            // _disciplineTextEdit
            // 
            this._disciplineTextEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._disciplineTextEdit.EditValue = "����";
            this._disciplineTextEdit.Location = new System.Drawing.Point(147, 12);
            this._disciplineTextEdit.Name = "_disciplineTextEdit";
            this._disciplineTextEdit.Properties.ReadOnly = true;
            this._disciplineTextEdit.Size = new System.Drawing.Size(321, 20);
            this._disciplineTextEdit.TabIndex = 14;
            this._disciplineTextEdit.Tag = "����#r";
            // 
            // lblSubject
            // 
            this.lblSubject.Location = new System.Drawing.Point(12, 14);
            this.lblSubject.Name = "lblSubject";
            this.lblSubject.Size = new System.Drawing.Size(73, 18);
            this.lblSubject.TabIndex = 18;
            this.lblSubject.Tag = "����#r";
            this.lblSubject.Text = "����:";
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(393, 286);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 27);
            this.btnCancel.TabIndex = 22;
            this.btnCancel.Tag = "������#r";
            this.btnCancel.Text = "������";
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(312, 286);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 27);
            this.btnOK.TabIndex = 21;
            this.btnOK.Tag = "���������#rc";
            this.btnOK.Text = "���������";
            this.btnOK.Click += new System.EventHandler(this.BtnOkClick);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(12, 168);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 18);
            this.label1.TabIndex = 26;
            this.label1.Tag = "����������#r";
            this.label1.Text = "����������:";
            // 
            // _descriptionMemoEdit
            // 
            this._descriptionMemoEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._descriptionMemoEdit.Location = new System.Drawing.Point(147, 166);
            this._descriptionMemoEdit.Name = "_descriptionMemoEdit";
            this._descriptionMemoEdit.Size = new System.Drawing.Size(321, 107);
            this._descriptionMemoEdit.TabIndex = 28;
            this._descriptionMemoEdit.Tag = "����������#r";
            this._descriptionMemoEdit.UseOptimizedRendering = true;
            // 
            // _lessonTypeAppointmentLabelEdit
            // 
            this._lessonTypeAppointmentLabelEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._lessonTypeAppointmentLabelEdit.Location = new System.Drawing.Point(147, 38);
            this._lessonTypeAppointmentLabelEdit.Name = "_lessonTypeAppointmentLabelEdit";
            this._lessonTypeAppointmentLabelEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._lessonTypeAppointmentLabelEdit.Size = new System.Drawing.Size(321, 20);
            this._lessonTypeAppointmentLabelEdit.TabIndex = 29;
            this._lessonTypeAppointmentLabelEdit.Tag = "��� �������#r";
            // 
            // _isImportantCheckEdit
            // 
            this._isImportantCheckEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._isImportantCheckEdit.Location = new System.Drawing.Point(145, 116);
            this._isImportantCheckEdit.Name = "_isImportantCheckEdit";
            this._isImportantCheckEdit.Properties.Caption = "������ �������";
            this._isImportantCheckEdit.Size = new System.Drawing.Size(176, 19);
            this._isImportantCheckEdit.TabIndex = 30;
            this._isImportantCheckEdit.Tag = "������ ������� (����)#r";
            this._isImportantCheckEdit.CheckedChanged += new System.EventHandler(this.IsImportantCheckEditCheckedChanged);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(15, 93);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(106, 13);
            this.labelControl1.TabIndex = 31;
            this.labelControl1.Tag = "������������#r";
            this.labelControl1.Text = "������������ (���):";
            // 
            // _spinEditDuration
            // 
            this._spinEditDuration.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._spinEditDuration.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this._spinEditDuration.Location = new System.Drawing.Point(147, 90);
            this._spinEditDuration.Name = "_spinEditDuration";
            this._spinEditDuration.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this._spinEditDuration.Properties.IsFloatValue = false;
            this._spinEditDuration.Properties.Mask.EditMask = "N00";
            this._spinEditDuration.Properties.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this._spinEditDuration.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this._spinEditDuration.Size = new System.Drawing.Size(66, 20);
            this._spinEditDuration.TabIndex = 32;
            this._spinEditDuration.Tag = "������������#r";
            // 
            // _checkEditHasReminder
            // 
            this._checkEditHasReminder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._checkEditHasReminder.Location = new System.Drawing.Point(145, 140);
            this._checkEditHasReminder.Name = "_checkEditHasReminder";
            this._checkEditHasReminder.Properties.Caption = "��������� ��";
            this._checkEditHasReminder.Size = new System.Drawing.Size(98, 19);
            this._checkEditHasReminder.TabIndex = 45;
            this._checkEditHasReminder.Tag = "�����������#r";
            this._checkEditHasReminder.CheckedChanged += new System.EventHandler(this.CheckEditHasReminderCheckedChanged);
            // 
            // _durationEditTimeBeforeStart
            // 
            this._durationEditTimeBeforeStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._durationEditTimeBeforeStart.Enabled = false;
            this._durationEditTimeBeforeStart.Location = new System.Drawing.Point(249, 141);
            this._durationEditTimeBeforeStart.Name = "_durationEditTimeBeforeStart";
            this._durationEditTimeBeforeStart.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._durationEditTimeBeforeStart.Properties.ShowEmptyItem = false;
            this._durationEditTimeBeforeStart.Size = new System.Drawing.Size(219, 20);
            this._durationEditTimeBeforeStart.TabIndex = 44;
            this._durationEditTimeBeforeStart.Tag = "�����������#r";
            // 
            // LessonEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(482, 325);
            this.Controls.Add(this._checkEditHasReminder);
            this.Controls.Add(this._durationEditTimeBeforeStart);
            this.Controls.Add(this._spinEditDuration);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this._isImportantCheckEdit);
            this.Controls.Add(this._lessonTypeAppointmentLabelEdit);
            this.Controls.Add(this._descriptionMemoEdit);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.timeStart);
            this.Controls.Add(this.dtStart);
            this.Controls.Add(this.lblStart);
            this.Controls.Add(this.lblLabel);
            this.Controls.Add(this._disciplineTextEdit);
            this.Controls.Add(this.lblSubject);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LessonEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "�������#r";
            this.Text = "�������";
            ((System.ComponentModel.ISupportInitialize)(this.timeStart.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtStart.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtStart.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._disciplineTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._descriptionMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._lessonTypeAppointmentLabelEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._isImportantCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._spinEditDuration.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._checkEditHasReminder.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._durationEditTimeBeforeStart.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.TimeEdit timeStart;
        private DevExpress.XtraEditors.DateEdit dtStart;
        private System.Windows.Forms.Label lblStart;
        private System.Windows.Forms.Label lblLabel;
        private DevExpress.XtraEditors.TextEdit _disciplineTextEdit;
        private System.Windows.Forms.Label lblSubject;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.MemoEdit _descriptionMemoEdit;
        private DevExpress.XtraScheduler.UI.AppointmentLabelEdit _lessonTypeAppointmentLabelEdit;
        private DevExpress.XtraEditors.CheckEdit _isImportantCheckEdit;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SpinEdit _spinEditDuration;
        private DevExpress.XtraEditors.CheckEdit _checkEditHasReminder;
        private DevExpress.XtraScheduler.UI.DurationEdit _durationEditTimeBeforeStart;
    }
}
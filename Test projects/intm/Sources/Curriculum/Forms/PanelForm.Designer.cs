﻿namespace Curriculum
{
    partial class PanelForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PanelForm));
            this._gridControl = new DevExpress.XtraGrid.GridControl();
            this._gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._imageGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._statePictureEdit = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this._probationerGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._disciplineGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._dateTimeGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._dateEdit = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this._durationGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._stateGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._passingPercentGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._realPercentGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._markGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._commentGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._commentMemoExEdit = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this._editPersonPanelControl = new DevExpress.XtraEditors.PanelControl();
            this._personelSimpleButton = new DevExpress.XtraEditors.SimpleButton();
            this._refreshSimpleButton = new DevExpress.XtraEditors.SimpleButton();
            this._curriculumSimpleButton = new DevExpress.XtraEditors.SimpleButton();
            this._printSimpleButton = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this._gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._statePictureEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._dateEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._dateEdit.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._commentMemoExEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._editPersonPanelControl)).BeginInit();
            this._editPersonPanelControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // _gridControl
            // 
            this._gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._gridControl.Location = new System.Drawing.Point(0, 0);
            this._gridControl.MainView = this._gridView;
            this._gridControl.Name = "_gridControl";
            this._gridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this._dateEdit,
            this._statePictureEdit,
            this._commentMemoExEdit});
            this._gridControl.Size = new System.Drawing.Size(878, 644);
            this._gridControl.TabIndex = 19;
            this._gridControl.Tag = "Таблица Успеваемость#r";
            this._gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this._gridView});
            // 
            // _gridView
            // 
            this._gridView.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._gridView.Appearance.GroupRow.Options.UseFont = true;
            this._gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._imageGridColumn,
            this._probationerGridColumn,
            this._disciplineGridColumn,
            this._dateTimeGridColumn,
            this._durationGridColumn,
            this._stateGridColumn,
            this._passingPercentGridColumn,
            this._realPercentGridColumn,
            this._markGridColumn,
            this._commentGridColumn});
            this._gridView.GridControl = this._gridControl;
            this._gridView.GroupCount = 1;
            this._gridView.Name = "_gridView";
            this._gridView.OptionsBehavior.AutoExpandAllGroups = true;
            this._gridView.OptionsDetail.AllowZoomDetail = false;
            this._gridView.OptionsDetail.EnableMasterViewMode = false;
            this._gridView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this._gridView.OptionsView.ShowAutoFilterRow = true;
            this._gridView.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this._probationerGridColumn, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this._dateTimeGridColumn, DevExpress.Data.ColumnSortOrder.Ascending)});
            this._gridView.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.GridViewRowStyle);
            // 
            // _imageGridColumn
            // 
            this._imageGridColumn.Caption = " ";
            this._imageGridColumn.ColumnEdit = this._statePictureEdit;
            this._imageGridColumn.FieldName = "Image";
            this._imageGridColumn.Name = "_imageGridColumn";
            this._imageGridColumn.OptionsColumn.AllowEdit = false;
            this._imageGridColumn.OptionsFilter.AllowAutoFilter = false;
            this._imageGridColumn.OptionsFilter.AllowFilter = false;
            this._imageGridColumn.OptionsFilter.ImmediateUpdateAutoFilter = false;
            this._imageGridColumn.Visible = true;
            this._imageGridColumn.VisibleIndex = 0;
            this._imageGridColumn.Width = 95;
            // 
            // _statePictureEdit
            // 
            this._statePictureEdit.Name = "_statePictureEdit";
            this._statePictureEdit.NullText = " ";
            // 
            // _probationerGridColumn
            // 
            this._probationerGridColumn.Caption = "Обучаемый";
            this._probationerGridColumn.FieldName = "Person";
            this._probationerGridColumn.Name = "_probationerGridColumn";
            this._probationerGridColumn.OptionsColumn.AllowEdit = false;
            this._probationerGridColumn.Tag = "Обучаемый#r";
            // 
            // _disciplineGridColumn
            // 
            this._disciplineGridColumn.Caption = "Тема";
            this._disciplineGridColumn.FieldName = "Discipline";
            this._disciplineGridColumn.Name = "_disciplineGridColumn";
            this._disciplineGridColumn.OptionsColumn.AllowEdit = false;
            this._disciplineGridColumn.Tag = "Тема#r";
            this._disciplineGridColumn.Visible = true;
            this._disciplineGridColumn.VisibleIndex = 1;
            this._disciplineGridColumn.Width = 95;
            // 
            // _dateTimeGridColumn
            // 
            this._dateTimeGridColumn.Caption = "Дата и время";
            this._dateTimeGridColumn.ColumnEdit = this._dateEdit;
            this._dateTimeGridColumn.DisplayFormat.FormatString = "\"YYYY.MM.dd mm:HH:ss\"";
            this._dateTimeGridColumn.FieldName = "DateTime";
            this._dateTimeGridColumn.Name = "_dateTimeGridColumn";
            this._dateTimeGridColumn.OptionsColumn.AllowEdit = false;
            this._dateTimeGridColumn.Tag = "Дата и время#r";
            this._dateTimeGridColumn.Visible = true;
            this._dateTimeGridColumn.VisibleIndex = 2;
            this._dateTimeGridColumn.Width = 95;
            // 
            // _dateEdit
            // 
            this._dateEdit.AutoHeight = false;
            this._dateEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._dateEdit.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this._dateEdit.Name = "_dateEdit";
            // 
            // _durationGridColumn
            // 
            this._durationGridColumn.Caption = "Длительность (мин)";
            this._durationGridColumn.FieldName = "Duration";
            this._durationGridColumn.Name = "_durationGridColumn";
            this._durationGridColumn.OptionsColumn.AllowEdit = false;
            this._durationGridColumn.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this._durationGridColumn.Tag = "Длительность#r";
            this._durationGridColumn.Visible = true;
            this._durationGridColumn.VisibleIndex = 3;
            this._durationGridColumn.Width = 95;
            // 
            // _stateGridColumn
            // 
            this._stateGridColumn.Caption = "Состояние";
            this._stateGridColumn.FieldName = "State";
            this._stateGridColumn.Name = "_stateGridColumn";
            this._stateGridColumn.OptionsColumn.AllowEdit = false;
            this._stateGridColumn.Tag = "Состояние#r";
            this._stateGridColumn.Visible = true;
            this._stateGridColumn.VisibleIndex = 4;
            this._stateGridColumn.Width = 95;
            // 
            // _passingPercentGridColumn
            // 
            this._passingPercentGridColumn.Caption = "Проходной %";
            this._passingPercentGridColumn.FieldName = "PassingScore";
            this._passingPercentGridColumn.Name = "_passingPercentGridColumn";
            this._passingPercentGridColumn.OptionsColumn.AllowEdit = false;
            this._passingPercentGridColumn.Tag = "Проходной#r";
            this._passingPercentGridColumn.Visible = true;
            this._passingPercentGridColumn.VisibleIndex = 5;
            this._passingPercentGridColumn.Width = 95;
            // 
            // _realPercentGridColumn
            // 
            this._realPercentGridColumn.Caption = "Набранный %";
            this._realPercentGridColumn.FieldName = "Perc";
            this._realPercentGridColumn.Name = "_realPercentGridColumn";
            this._realPercentGridColumn.OptionsColumn.AllowEdit = false;
            this._realPercentGridColumn.Tag = "Набранный#r";
            this._realPercentGridColumn.Visible = true;
            this._realPercentGridColumn.VisibleIndex = 6;
            this._realPercentGridColumn.Width = 109;
            // 
            // _markGridColumn
            // 
            this._markGridColumn.Caption = "Оценка";
            this._markGridColumn.FieldName = "Valuation";
            this._markGridColumn.Name = "_markGridColumn";
            this._markGridColumn.OptionsColumn.AllowEdit = false;
            this._markGridColumn.Tag = "Оценка#r";
            this._markGridColumn.Visible = true;
            this._markGridColumn.VisibleIndex = 7;
            this._markGridColumn.Width = 101;
            // 
            // _commentGridColumn
            // 
            this._commentGridColumn.Caption = "Комментарий";
            this._commentGridColumn.ColumnEdit = this._commentMemoExEdit;
            this._commentGridColumn.FieldName = "CommentText";
            this._commentGridColumn.Name = "_commentGridColumn";
            this._commentGridColumn.OptionsColumn.ReadOnly = true;
            this._commentGridColumn.Tag = "Комментарий#r";
            this._commentGridColumn.Visible = true;
            this._commentGridColumn.VisibleIndex = 8;
            this._commentGridColumn.Width = 77;
            // 
            // _commentMemoExEdit
            // 
            this._commentMemoExEdit.AutoHeight = false;
            this._commentMemoExEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._commentMemoExEdit.Name = "_commentMemoExEdit";
            this._commentMemoExEdit.ShowIcon = false;
            // 
            // _editPersonPanelControl
            // 
            this._editPersonPanelControl.Controls.Add(this._personelSimpleButton);
            this._editPersonPanelControl.Controls.Add(this._refreshSimpleButton);
            this._editPersonPanelControl.Controls.Add(this._curriculumSimpleButton);
            this._editPersonPanelControl.Controls.Add(this._printSimpleButton);
            this._editPersonPanelControl.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._editPersonPanelControl.Location = new System.Drawing.Point(0, 644);
            this._editPersonPanelControl.Name = "_editPersonPanelControl";
            this._editPersonPanelControl.Size = new System.Drawing.Size(878, 39);
            this._editPersonPanelControl.TabIndex = 23;
            // 
            // _personelSimpleButton
            // 
            this._personelSimpleButton.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this._personelSimpleButton.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this._personelSimpleButton.Location = new System.Drawing.Point(685, 5);
            this._personelSimpleButton.Name = "_personelSimpleButton";
            this._personelSimpleButton.Size = new System.Drawing.Size(91, 29);
            this._personelSimpleButton.TabIndex = 3;
            this._personelSimpleButton.Tag = "Персонал#r";
            this._personelSimpleButton.Text = "Персонал";
            this._personelSimpleButton.ToolTip = "Открыть карточку выделенного обучаемого";
            this._personelSimpleButton.Click += new System.EventHandler(this.PersonelSimpleButtonClick);
            // 
            // _refreshSimpleButton
            // 
            this._refreshSimpleButton.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._refreshSimpleButton.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this._refreshSimpleButton.Location = new System.Drawing.Point(5, 5);
            this._refreshSimpleButton.Name = "_refreshSimpleButton";
            this._refreshSimpleButton.Size = new System.Drawing.Size(91, 29);
            this._refreshSimpleButton.TabIndex = 1;
            this._refreshSimpleButton.Tag = "Таблица Успеваемость#r";
            this._refreshSimpleButton.Text = "Обновить";
            this._refreshSimpleButton.ToolTip = "Обновить данные";
            this._refreshSimpleButton.Click += new System.EventHandler(this.RefreshSimpleButtonClick);
            // 
            // _curriculumSimpleButton
            // 
            this._curriculumSimpleButton.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this._curriculumSimpleButton.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this._curriculumSimpleButton.Location = new System.Drawing.Point(782, 5);
            this._curriculumSimpleButton.Name = "_curriculumSimpleButton";
            this._curriculumSimpleButton.Size = new System.Drawing.Size(91, 29);
            this._curriculumSimpleButton.TabIndex = 4;
            this._curriculumSimpleButton.Tag = "Учебный план#r";
            this._curriculumSimpleButton.Text = "Учебный план";
            this._curriculumSimpleButton.ToolTip = "Открыть учебный план выделенного обучаемого";
            this._curriculumSimpleButton.Click += new System.EventHandler(this.CurriculumSimpleButtonClick);
            // 
            // _printSimpleButton
            // 
            this._printSimpleButton.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._printSimpleButton.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this._printSimpleButton.Location = new System.Drawing.Point(102, 5);
            this._printSimpleButton.Name = "_printSimpleButton";
            this._printSimpleButton.Size = new System.Drawing.Size(91, 29);
            this._printSimpleButton.TabIndex = 2;
            this._printSimpleButton.Tag = "Таблица Успеваемость\\\\Печать#r";
            this._printSimpleButton.Text = "Печать";
            this._printSimpleButton.ToolTip = "Сформировать отчет";
            this._printSimpleButton.Click += new System.EventHandler(this.PrintSimpleButtonClick);
            // 
            // PanelForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(878, 683);
            this.Controls.Add(this._gridControl);
            this.Controls.Add(this._editPersonPanelControl);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PanelForm";
            this.Tag = "Панель управления\\Результаты\\Успеваемость#r";
            this.Text = "Успеваемость";
            this.Load += new System.EventHandler(this.PanelFormLoad);
            ((System.ComponentModel.ISupportInitialize)(this._gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._statePictureEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._dateEdit.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._dateEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._commentMemoExEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._editPersonPanelControl)).EndInit();
            this._editPersonPanelControl.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl _gridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView _gridView;
        private DevExpress.XtraGrid.Columns.GridColumn _imageGridColumn;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit _statePictureEdit;
        private DevExpress.XtraGrid.Columns.GridColumn _probationerGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _disciplineGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _dateTimeGridColumn;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit _dateEdit;
        private DevExpress.XtraGrid.Columns.GridColumn _durationGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _stateGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _passingPercentGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _realPercentGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _markGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _commentGridColumn;
        private DevExpress.XtraEditors.PanelControl _editPersonPanelControl;
        private DevExpress.XtraEditors.SimpleButton _printSimpleButton;
        private DevExpress.XtraEditors.SimpleButton _refreshSimpleButton;
        private DevExpress.XtraEditors.SimpleButton _personelSimpleButton;
        private DevExpress.XtraEditors.SimpleButton _curriculumSimpleButton;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit _commentMemoExEdit;

    }
}
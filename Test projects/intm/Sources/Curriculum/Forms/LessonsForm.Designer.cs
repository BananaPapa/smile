namespace Curriculum
{
    partial class LessonsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LessonsForm));
            this._curriculumControl = new Curriculum.LessonsControl();
            this.SuspendLayout();
            // 
            // _curriculumControl
            // 
            this._curriculumControl.CurrentDate = new System.DateTime(2014, 9, 3, 0, 0, 0, 0);
            this._curriculumControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._curriculumControl.HasInnerHideLogic = false;
            this._curriculumControl.HierarchyParent = this;
            this._curriculumControl.Location = new System.Drawing.Point(0, 0);
            this._curriculumControl.ManualFilter = true;
            this._curriculumControl.Name = "_curriculumControl";
            this._curriculumControl.Person = null;
            this._curriculumControl.PrefixTag = null;
            this._curriculumControl.ReadOnly = false;
            this._curriculumControl.Size = new System.Drawing.Size(901, 655);
            this._curriculumControl.TabIndex = 3;
            this._curriculumControl.Tag = "������������#r";
            // 
            // LessonsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(901, 655);
            this.Controls.Add(this._curriculumControl);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "LessonsForm";
            this.Tag = "������� ����#r";
            this.Text = "������� ����";
            this.ResumeLayout(false);

        }

        #endregion

        private LessonsControl _curriculumControl;

    }
}
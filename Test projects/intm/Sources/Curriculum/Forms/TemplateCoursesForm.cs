﻿using System.ComponentModel;
using DevExpress.XtraEditors;
using Eureca.Integrator.Common;

namespace Curriculum
{
    public partial class TemplateCoursesForm : FormWithAccessAreas
    {
        private Template _template;

        public TemplateCoursesForm(Template template)
        {
            InitializeComponent();

            Template = template;
            _coursesControl.Editable = true;
        }

        [Browsable(false)]
        public Template Template
        {
            get { return _template; }
            set
            {
                _template = value;

                string name = (_template != null) ? _template.Name : "";
                Text = string.Format("Содержимое шаблона курсов [{0}]", name);
                _coursesControl.Template = _template;
            }
        }
    }
}
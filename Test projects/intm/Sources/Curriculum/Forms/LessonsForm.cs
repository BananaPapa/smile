using System;
using System.ComponentModel;
using System.Linq;
using DevExpress.XtraEditors;
using Eureca.Integrator.Common;
using log4net;

namespace Curriculum
{
    public partial class LessonsForm : FormWithAccessAreas
    {
        readonly static ILog Log = LogManager.GetLogger( typeof( LessonsForm ) );

        #region Fields

        private Person _person;

        #endregion

        #region Private properties

        /// <summary>
        /// Gets or sets current person.
        /// </summary>
        [Browsable(false)]
        public Person Person
        {
            get { return _person; }
            set
            {
                _person = value;
                string name = (_person != null) ? _person.Name : "";
                Text = string.Format("������� ���� [{0}]", name);
                _curriculumControl.Person = _person;
            }
        }

        #endregion

        #region Constructors

        public LessonsForm()
        {
            InitializeComponent();

            try
            {
                Person = Program.DbSingletone.Persons.First();
            }
            catch (ArgumentNullException ex)
            {
                Log.Error( ex.ToString( ) );
            }
            catch (Exception ex)
            {
                Log.Error( ex.ToString( ) );
                ///TODO : Handle Exception
            }
        }

        public LessonsForm(Person person)
        {
            InitializeComponent();

            Person = person;
        }

        #endregion

        public DateTime CurrentDate
        {
            get { return _curriculumControl.CurrentDate; }
            set { _curriculumControl.CurrentDate = value; }
        }
    }
}
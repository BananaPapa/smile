using System;
using DevExpress.XtraLayout;

namespace Curriculum
{
    partial class PersonsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            ///http://www.devexpress.com/Support/Center/Question/Details/Q489500  w8 for some fix
            try
            {
                base.Dispose(disposing);
            }
            catch (Exception e)
            {
                Log.Error("Failed dispose correctly: " + e.ToString());
            }
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PersonsForm));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            this._lettersGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._letterAddresseeGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._letterAddresseeComboBox = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this._letterCodeGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._letterCodeTextEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this._letterDateGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._letterDateEdit = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this._lettersGridControl = new DevExpress.XtraGrid.GridControl();
            this._imageList = new System.Windows.Forms.ImageList(this.components);
            this._personsGridControl = new DevExpress.XtraGrid.GridControl();
            this._personsLayoutView = new DevExpress.XtraGrid.Views.Layout.LayoutView();
            this._personIdLayoutViewColumn = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this._personIdLayoutViewField = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this._personNameLayoutViewColumn = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this._personNameTextEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.Item27 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this._personBirthDateLayoutViewColumn = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this._personBirthDateEdit = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.Item26 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this._personShipLayoutViewColumn = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this._personShipGridLookUpEdit = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this._personShipGridLookUpEditView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._shipNameGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._shipIndexGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._shipRiverGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._shipLineGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._shipConditionsGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Item32 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this._personCategoryLayoutViewColumn = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this._personCategoryComboBox = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.Item33 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this._personAddressLayoutViewColumn = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this._personAddressMemoEdit = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.Item31 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this._personQualificationLayoutViewColumn = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this._personQualificationComboBox = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.Item11 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this._personEducationLayoutViewColumn = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this._personEducationComboBox = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.Item17 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this._personDegreeLayoutViewColumn = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this._personDegreeComboBox = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.Item24 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this._personPositionLayoutViewColumn = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this._personPositionComboBox = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.Item28 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this._personDepartmentLayoutViewColumn = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this._personDepartmentComboBox = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.Item30 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this._personAffiliationLayoutViewColumn = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this._personAffiliationComboBox = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.Item29 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this._personChildNickNameLayoutViewColumn = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this._personChildNickNameTextEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.Item25 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this._personDepartureDateLayoutViewColumn = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this._personDepartureDateEdit = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.Item39 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this._personPeriodLayoutViewColumn = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this._personPeriodTextEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.Item40 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this._personTripCountLayoutViewColumn = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this._personTripCountSpinEdit = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.Item38 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this._personTeachingStartDateLayoutViewColumn = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this._personTeachingStartDateEdit = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.Item41 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this._personExaminationDateLayoutViewColumn = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this._personExaminationDateEdit = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.Item34 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this._personAppointmentOrderCodeLayoutViewColumn = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this._personCodeTextEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.Item35 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this._personAppointmentOrderDateLayoutViewColumn = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this._personDateEdit = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.Item36 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this._personStatusLayoutViewColumn = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this._personStatusComboBox = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.Item44 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this._personPhotoLayoutViewColumn = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this._personPhotoPictureEdit = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.Item43 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this._personRequestCodeLayoutViewColumn = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_layoutViewColumn1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this._personRequestDateLayoutViewColumn = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_layoutViewColumn1_1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewCard1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewCard();
            this._qualificationInfoGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this._personalInfoGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this._qadreInfoGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this._contactInfoGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this._appointmentGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.Item42 = new DevExpress.XtraLayout.SimpleLabelItem();
            this._requestOrderItem = new DevExpress.XtraLayout.SimpleLabelItem();
            this._appointmentOrderItem = new DevExpress.XtraLayout.SimpleLabelItem();
            this._photoGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this._splitContainerControl = new DevExpress.XtraEditors.SplitContainerControl();
            this._detailsSplitContainerControl = new DevExpress.XtraEditors.SplitContainerControl();
            this._dynamicDetailsXtraTabControl = new DevExpress.XtraTab.XtraTabControl();
            this._phonesXtraTabPage = new DevExpress.XtraTab.XtraTabPage();
            this._phonesGridControl = new DevExpress.XtraGrid.GridControl();
            this._phonesGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._phoneTypeGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._phoneTypeComboBox = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this._phoneNumberGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._phoneNumberTextEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this._lettersXtraTabPage = new DevExpress.XtraTab.XtraTabPage();
            this._personPropertiesXtraTabPage = new DevExpress.XtraTab.XtraTabPage();
            this._personPropertiesGridControl = new DevExpress.XtraGrid.GridControl();
            this._personPropertiesGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._personPropertyNameGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._personPropertyValueGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this._notesXtraTabPage = new DevExpress.XtraTab.XtraTabPage();
            this._personNotesRtfEditControl = new Curriculum.RtfEditControl();
            this._savePersonNotesPanelControl = new DevExpress.XtraEditors.PanelControl();
            this._savePersonNotesSimpleButton = new DevExpress.XtraEditors.SimpleButton();
            this._professionalXtraTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this._professionalLoginTextEdit = new DevExpress.XtraEditors.TextEdit();
            this._professionalPasswordTextEdit = new DevExpress.XtraEditors.TextEdit();
            this._showProfessionalPasswordCheckButton = new DevExpress.XtraEditors.CheckButton();
            this._createProfessionalUserSimpleButton = new DevExpress.XtraEditors.SimpleButton();
            this._deleteProfessionalUserSimpleButton = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this._saveProfessionalCredentialsPanelControl = new DevExpress.XtraEditors.PanelControl();
            this._saveProfessionalCredentialsButton = new DevExpress.XtraEditors.SimpleButton();
            this._staticDetailsXtraTabControl = new DevExpress.XtraTab.XtraTabControl();
            this._lessonsXtraTabPage = new DevExpress.XtraTab.XtraTabPage();
            this._showLessonsLinkLabel = new System.Windows.Forms.LinkLabel();
            this._lessonsControl = new Curriculum.LessonsControl();
            this._editPersonLessonsPanelControl = new DevExpress.XtraEditors.PanelControl();
            this._editLessonsSimpleButton = new DevExpress.XtraEditors.SimpleButton();
            this._barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this._produceReportBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem5 = new DevExpress.XtraBars.BarButtonItem();
            this._popupMenu = new DevExpress.XtraBars.PopupMenu(this.components);
            ((System.ComponentModel.ISupportInitialize)(this._lettersGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._letterAddresseeComboBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._letterCodeTextEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._letterDateEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._letterDateEdit.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._lettersGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._personsGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._personsLayoutView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._personIdLayoutViewField)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._personNameTextEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._personBirthDateEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._personBirthDateEdit.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._personShipGridLookUpEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._personShipGridLookUpEditView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._personCategoryComboBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._personAddressMemoEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._personQualificationComboBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._personEducationComboBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._personDegreeComboBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._personPositionComboBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._personDepartmentComboBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._personAffiliationComboBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._personChildNickNameTextEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._personDepartureDateEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._personDepartureDateEdit.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._personPeriodTextEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._personTripCountSpinEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._personTeachingStartDateEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._personTeachingStartDateEdit.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._personExaminationDateEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._personExaminationDateEdit.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._personCodeTextEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._personDateEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._personDateEdit.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._personStatusComboBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._personPhotoPictureEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn1_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewCard1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._qualificationInfoGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._personalInfoGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._qadreInfoGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._contactInfoGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._appointmentGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._requestOrderItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._appointmentOrderItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._photoGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._splitContainerControl)).BeginInit();
            this._splitContainerControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._detailsSplitContainerControl)).BeginInit();
            this._detailsSplitContainerControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._dynamicDetailsXtraTabControl)).BeginInit();
            this._dynamicDetailsXtraTabControl.SuspendLayout();
            this._phonesXtraTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._phonesGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._phonesGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._phoneTypeComboBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._phoneNumberTextEdit)).BeginInit();
            this._lettersXtraTabPage.SuspendLayout();
            this._personPropertiesXtraTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._personPropertiesGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._personPropertiesGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            this._notesXtraTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._savePersonNotesPanelControl)).BeginInit();
            this._savePersonNotesPanelControl.SuspendLayout();
            this._professionalXtraTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.tableLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._professionalLoginTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._professionalPasswordTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._saveProfessionalCredentialsPanelControl)).BeginInit();
            this._saveProfessionalCredentialsPanelControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._staticDetailsXtraTabControl)).BeginInit();
            this._staticDetailsXtraTabControl.SuspendLayout();
            this._lessonsXtraTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._editPersonLessonsPanelControl)).BeginInit();
            this._editPersonLessonsPanelControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._popupMenu)).BeginInit();
            this.SuspendLayout();
            // 
            // _lettersGridView
            // 
            this._lettersGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._letterAddresseeGridColumn,
            this._letterCodeGridColumn,
            this._letterDateGridColumn});
            this._lettersGridView.GridControl = this._lettersGridControl;
            this._lettersGridView.Images = this._imageList;
            this._lettersGridView.Name = "_lettersGridView";
            this._lettersGridView.OptionsBehavior.AutoExpandAllGroups = true;
            this._lettersGridView.ViewCaption = "������";
            this._lettersGridView.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.LettersGridView_InitNewRow);
            this._lettersGridView.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.LettersGridView_FocusedRowChanged);
            this._lettersGridView.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.LettersGridView_ValidateRow);
            this._lettersGridView.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.LettersGridView_RowUpdated);
            this._lettersGridView.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.LettersGridView_ValidatingEditor);
            // 
            // _letterAddresseeGridColumn
            // 
            this._letterAddresseeGridColumn.Caption = "�������";
            this._letterAddresseeGridColumn.ColumnEdit = this._letterAddresseeComboBox;
            this._letterAddresseeGridColumn.FieldName = "Addressee";
            this._letterAddresseeGridColumn.Name = "_letterAddresseeGridColumn";
            this._letterAddresseeGridColumn.Tag = "�������#r";
            this._letterAddresseeGridColumn.Visible = true;
            this._letterAddresseeGridColumn.VisibleIndex = 0;
            // 
            // _letterAddresseeComboBox
            // 
            this._letterAddresseeComboBox.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this._letterAddresseeComboBox.AutoHeight = false;
            this._letterAddresseeComboBox.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._letterAddresseeComboBox.MaxLength = 64;
            this._letterAddresseeComboBox.Name = "_letterAddresseeComboBox";
            this._letterAddresseeComboBox.NullText = "�� ������";
            this._letterAddresseeComboBox.Sorted = true;
            // 
            // _letterCodeGridColumn
            // 
            this._letterCodeGridColumn.Caption = "�";
            this._letterCodeGridColumn.ColumnEdit = this._letterCodeTextEdit;
            this._letterCodeGridColumn.FieldName = "Code";
            this._letterCodeGridColumn.Name = "_letterCodeGridColumn";
            this._letterCodeGridColumn.Tag = "�����#r";
            this._letterCodeGridColumn.Visible = true;
            this._letterCodeGridColumn.VisibleIndex = 1;
            // 
            // _letterCodeTextEdit
            // 
            this._letterCodeTextEdit.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this._letterCodeTextEdit.AutoHeight = false;
            this._letterCodeTextEdit.MaxLength = 16;
            this._letterCodeTextEdit.Name = "_letterCodeTextEdit";
            this._letterCodeTextEdit.NullText = "�� ������";
            // 
            // _letterDateGridColumn
            // 
            this._letterDateGridColumn.Caption = "����";
            this._letterDateGridColumn.ColumnEdit = this._letterDateEdit;
            this._letterDateGridColumn.FieldName = "Date";
            this._letterDateGridColumn.ImageIndex = 3;
            this._letterDateGridColumn.Name = "_letterDateGridColumn";
            this._letterDateGridColumn.Tag = "����#r";
            this._letterDateGridColumn.Visible = true;
            this._letterDateGridColumn.VisibleIndex = 2;
            // 
            // _letterDateEdit
            // 
            this._letterDateEdit.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this._letterDateEdit.AutoHeight = false;
            this._letterDateEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._letterDateEdit.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this._letterDateEdit.Name = "_letterDateEdit";
            this._letterDateEdit.NullText = "�� �������";
            // 
            // _lettersGridControl
            // 
            this._lettersGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lettersGridControl.EmbeddedNavigator.Buttons.Append.Tag = "..\\������� ������#ra";
            this._lettersGridControl.EmbeddedNavigator.Buttons.Edit.Tag = "..\\������� ������#rc";
            this._lettersGridControl.EmbeddedNavigator.Buttons.Remove.Tag = "..\\������� ������#rd";
            this._lettersGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.LettersGridControl_EmbeddedNavigator_ButtonClick);
            this._lettersGridControl.Location = new System.Drawing.Point(0, 0);
            this._lettersGridControl.MainView = this._lettersGridView;
            this._lettersGridControl.Name = "_lettersGridControl";
            this._lettersGridControl.Size = new System.Drawing.Size(551, 297);
            this._lettersGridControl.TabIndex = 0;
            this._lettersGridControl.Tag = "..\\������� ������#r";
            this._lettersGridControl.UseEmbeddedNavigator = true;
            this._lettersGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this._lettersGridView});
            // 
            // _imageList
            // 
            this._imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("_imageList.ImageStream")));
            this._imageList.TransparentColor = System.Drawing.Color.White;
            this._imageList.Images.SetKeyName(0, "EditLockOpen16.png");
            this._imageList.Images.SetKeyName(1, "EditLock16.png");
            this._imageList.Images.SetKeyName(2, "Refresh16.png");
            this._imageList.Images.SetKeyName(3, "Date16.png");
            this._imageList.Images.SetKeyName(4, "Address16.png");
            this._imageList.Images.SetKeyName(5, "Ship16.png");
            this._imageList.Images.SetKeyName(6, "Reports16.png");
            // 
            // _personsGridControl
            // 
            this._personsGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._personsGridControl.EmbeddedNavigator.Buttons.Append.Tag = "������� ��������#ra";
            this._personsGridControl.EmbeddedNavigator.Buttons.Edit.Tag = "������� ��������#rc";
            this._personsGridControl.EmbeddedNavigator.Buttons.ImageList = this._imageList;
            this._personsGridControl.EmbeddedNavigator.Buttons.Remove.Tag = "������� ��������#rd";
            this._personsGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "���������/��������� ��������������", "EditLock#r"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "", "Refresh"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "������������ ������", "Report#r")});
            this._personsGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.PersonsGridControl_EmbeddedNavigator_ButtonClick);
            this._personsGridControl.Location = new System.Drawing.Point(0, 0);
            this._personsGridControl.MainView = this._personsLayoutView;
            this._personsGridControl.Name = "_personsGridControl";
            this._personsGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this._personNameTextEdit,
            this._personBirthDateEdit,
            this._personCategoryComboBox,
            this._personAddressMemoEdit,
            this._personQualificationComboBox,
            this._personEducationComboBox,
            this._personDegreeComboBox,
            this._personPositionComboBox,
            this._personDepartmentComboBox,
            this._personAffiliationComboBox,
            this._personChildNickNameTextEdit,
            this._personDepartureDateEdit,
            this._personStatusComboBox,
            this._personTripCountSpinEdit,
            this._personTeachingStartDateEdit,
            this._personExaminationDateEdit,
            this._personDateEdit,
            this._personCodeTextEdit,
            this._personShipGridLookUpEdit,
            this._letterAddresseeComboBox,
            this._letterCodeTextEdit,
            this._letterDateEdit,
            this._personPeriodTextEdit,
            this._personPhotoPictureEdit});
            this._personsGridControl.Size = new System.Drawing.Size(1130, 375);
            this._personsGridControl.TabIndex = 0;
            this._personsGridControl.Tag = "������� ��������#r";
            this._personsGridControl.UseEmbeddedNavigator = true;
            this._personsGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this._personsLayoutView});
            // 
            // _personsLayoutView
            // 
            this._personsLayoutView.CardCaptionFormat = "{3} ({4})";
            this._personsLayoutView.CardMinSize = new System.Drawing.Size(914, 289);
            this._personsLayoutView.Columns.AddRange(new DevExpress.XtraGrid.Columns.LayoutViewColumn[] {
            this._personIdLayoutViewColumn,
            this._personNameLayoutViewColumn,
            this._personBirthDateLayoutViewColumn,
            this._personShipLayoutViewColumn,
            this._personCategoryLayoutViewColumn,
            this._personAddressLayoutViewColumn,
            this._personQualificationLayoutViewColumn,
            this._personEducationLayoutViewColumn,
            this._personDegreeLayoutViewColumn,
            this._personPositionLayoutViewColumn,
            this._personDepartmentLayoutViewColumn,
            this._personAffiliationLayoutViewColumn,
            this._personChildNickNameLayoutViewColumn,
            this._personDepartureDateLayoutViewColumn,
            this._personPeriodLayoutViewColumn,
            this._personTripCountLayoutViewColumn,
            this._personTeachingStartDateLayoutViewColumn,
            this._personExaminationDateLayoutViewColumn,
            this._personAppointmentOrderCodeLayoutViewColumn,
            this._personAppointmentOrderDateLayoutViewColumn,
            this._personStatusLayoutViewColumn,
            this._personPhotoLayoutViewColumn,
            this._personRequestCodeLayoutViewColumn,
            this._personRequestDateLayoutViewColumn});
            this._personsLayoutView.GridControl = this._personsGridControl;
            this._personsLayoutView.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this._personIdLayoutViewField});
            this._personsLayoutView.Images = this._imageList;
            this._personsLayoutView.Name = "_personsLayoutView";
            this._personsLayoutView.OptionsBehavior.AllowPanCards = false;
            this._personsLayoutView.OptionsBehavior.AllowRuntimeCustomization = false;
            this._personsLayoutView.OptionsBehavior.AutoFocusCardOnScrolling = true;
            this._personsLayoutView.OptionsBehavior.AutoFocusNewCard = true;
            this._personsLayoutView.OptionsHeaderPanel.EnableCarouselModeButton = false;
            this._personsLayoutView.OptionsHeaderPanel.ShowCarouselModeButton = false;
            this._personsLayoutView.OptionsItemText.AlignMode = DevExpress.XtraGrid.Views.Layout.FieldTextAlignMode.AutoSize;
            this._personsLayoutView.OptionsView.ShowCardFieldBorders = true;
            this._personsLayoutView.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this._personRequestDateLayoutViewColumn, DevExpress.Data.ColumnSortOrder.Ascending)});
            this._personsLayoutView.TemplateCard = this.layoutViewCard1;
            this._personsLayoutView.ViewCaption = "��������";
            this._personsLayoutView.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.PersonsLayoutView_InitNewRow);
            this._personsLayoutView.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.PersonsLayoutView_FocusedRowChanged);
            this._personsLayoutView.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.PersonsLayoutView_ValidateRow);
            this._personsLayoutView.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.PersonsLayoutView_RowUpdated);
            this._personsLayoutView.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.PersonsLayoutView_ValidatingEditor);
            // 
            // _personIdLayoutViewColumn
            // 
            this._personIdLayoutViewColumn.Caption = "ID";
            this._personIdLayoutViewColumn.FieldName = "PersonId";
            this._personIdLayoutViewColumn.LayoutViewField = this._personIdLayoutViewField;
            this._personIdLayoutViewColumn.Name = "_personIdLayoutViewColumn";
            this._personIdLayoutViewColumn.OptionsColumn.AllowEdit = false;
            this._personIdLayoutViewColumn.OptionsColumn.ReadOnly = true;
            // 
            // _personIdLayoutViewField
            // 
            this._personIdLayoutViewField.EditorPreferredWidth = 10;
            this._personIdLayoutViewField.Location = new System.Drawing.Point(0, 0);
            this._personIdLayoutViewField.Name = "_personIdLayoutViewField";
            this._personIdLayoutViewField.Size = new System.Drawing.Size(908, 263);
            this._personIdLayoutViewField.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this._personIdLayoutViewField.TextSize = new System.Drawing.Size(103, 20);
            this._personIdLayoutViewField.TextToControlDistance = 5;
            // 
            // _personNameLayoutViewColumn
            // 
            this._personNameLayoutViewColumn.Caption = "�. �. �.";
            this._personNameLayoutViewColumn.ColumnEdit = this._personNameTextEdit;
            this._personNameLayoutViewColumn.FieldName = "Name";
            this._personNameLayoutViewColumn.LayoutViewField = this.Item27;
            this._personNameLayoutViewColumn.Name = "_personNameLayoutViewColumn";
            this._personNameLayoutViewColumn.Tag = "�. �. �.#r";
            // 
            // _personNameTextEdit
            // 
            this._personNameTextEdit.AutoHeight = false;
            this._personNameTextEdit.MaxLength = 128;
            this._personNameTextEdit.Name = "_personNameTextEdit";
            this._personNameTextEdit.NullText = "�� �������";
            // 
            // Item27
            // 
            this.Item27.EditorPreferredWidth = 172;
            this.Item27.Location = new System.Drawing.Point(0, 0);
            this.Item27.Name = "Item27";
            this.Item27.Size = new System.Drawing.Size(232, 30);
            this.Item27.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.Item27.TextSize = new System.Drawing.Size(45, 13);
            this.Item27.TextToControlDistance = 5;
            // 
            // _personBirthDateLayoutViewColumn
            // 
            this._personBirthDateLayoutViewColumn.Caption = "���� ��������";
            this._personBirthDateLayoutViewColumn.ColumnEdit = this._personBirthDateEdit;
            this._personBirthDateLayoutViewColumn.FieldName = "BirthDate";
            this._personBirthDateLayoutViewColumn.ImageIndex = 3;
            this._personBirthDateLayoutViewColumn.LayoutViewField = this.Item26;
            this._personBirthDateLayoutViewColumn.Name = "_personBirthDateLayoutViewColumn";
            this._personBirthDateLayoutViewColumn.Tag = "���� ��������#r";
            // 
            // _personBirthDateEdit
            // 
            this._personBirthDateEdit.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this._personBirthDateEdit.AutoHeight = false;
            this._personBirthDateEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._personBirthDateEdit.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this._personBirthDateEdit.Name = "_personBirthDateEdit";
            // 
            // Item26
            // 
            this.Item26.EditorPreferredWidth = 112;
            this.Item26.ImageIndex = 3;
            this.Item26.Location = new System.Drawing.Point(0, 30);
            this.Item26.Name = "Item26";
            this.Item26.Size = new System.Drawing.Size(232, 30);
            this.Item26.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.Item26.TextSize = new System.Drawing.Size(105, 16);
            this.Item26.TextToControlDistance = 5;
            // 
            // _personShipLayoutViewColumn
            // 
            this._personShipLayoutViewColumn.Caption = "�������";
            this._personShipLayoutViewColumn.ColumnEdit = this._personShipGridLookUpEdit;
            this._personShipLayoutViewColumn.FieldName = "Ship";
            this._personShipLayoutViewColumn.ImageIndex = 5;
            this._personShipLayoutViewColumn.LayoutViewField = this.Item32;
            this._personShipLayoutViewColumn.Name = "_personShipLayoutViewColumn";
            this._personShipLayoutViewColumn.Tag = "�������#r";
            // 
            // _personShipGridLookUpEdit
            // 
            this._personShipGridLookUpEdit.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this._personShipGridLookUpEdit.AutoHeight = false;
            this._personShipGridLookUpEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "�������", null, null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Close, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "������ ����������", null, null, true)});
            this._personShipGridLookUpEdit.DisplayMember = "Name";
            this._personShipGridLookUpEdit.Name = "_personShipGridLookUpEdit";
            this._personShipGridLookUpEdit.NullText = "�� ��������";
            this._personShipGridLookUpEdit.PopupFormMinSize = new System.Drawing.Size(600, 300);
            this._personShipGridLookUpEdit.View = this._personShipGridLookUpEditView;
            this._personShipGridLookUpEdit.ViewType = DevExpress.XtraEditors.Repository.GridLookUpViewType.GridView;
            this._personShipGridLookUpEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.PersonShipGridLookUpEdit_ButtonClick);
            // 
            // _personShipGridLookUpEditView
            // 
            this._personShipGridLookUpEditView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._shipNameGridColumn,
            this._shipIndexGridColumn,
            this._shipRiverGridColumn,
            this._shipLineGridColumn,
            this._shipConditionsGridColumn});
            this._personShipGridLookUpEditView.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this._personShipGridLookUpEditView.Name = "_personShipGridLookUpEditView";
            this._personShipGridLookUpEditView.OptionsBehavior.Editable = false;
            this._personShipGridLookUpEditView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this._personShipGridLookUpEditView.OptionsView.ShowGroupPanel = false;
            this._personShipGridLookUpEditView.ViewCaption = "�������";
            // 
            // _shipNameGridColumn
            // 
            this._shipNameGridColumn.Caption = "��������";
            this._shipNameGridColumn.FieldName = "Name";
            this._shipNameGridColumn.Name = "_shipNameGridColumn";
            this._shipNameGridColumn.Tag = "��������#r";
            this._shipNameGridColumn.Visible = true;
            this._shipNameGridColumn.VisibleIndex = 0;
            // 
            // _shipIndexGridColumn
            // 
            this._shipIndexGridColumn.Caption = "������";
            this._shipIndexGridColumn.FieldName = "Index";
            this._shipIndexGridColumn.Name = "_shipIndexGridColumn";
            this._shipIndexGridColumn.Tag = "������#r";
            this._shipIndexGridColumn.Visible = true;
            this._shipIndexGridColumn.VisibleIndex = 1;
            // 
            // _shipRiverGridColumn
            // 
            this._shipRiverGridColumn.Caption = "����";
            this._shipRiverGridColumn.FieldName = "River";
            this._shipRiverGridColumn.Name = "_shipRiverGridColumn";
            this._shipRiverGridColumn.Tag = "����#r";
            this._shipRiverGridColumn.Visible = true;
            this._shipRiverGridColumn.VisibleIndex = 2;
            // 
            // _shipLineGridColumn
            // 
            this._shipLineGridColumn.Caption = "�����������";
            this._shipLineGridColumn.FieldName = "ShipLine";
            this._shipLineGridColumn.Name = "_shipLineGridColumn";
            this._shipLineGridColumn.Tag = "�����������#r";
            this._shipLineGridColumn.Visible = true;
            this._shipLineGridColumn.VisibleIndex = 3;
            // 
            // _shipConditionsGridColumn
            // 
            this._shipConditionsGridColumn.Caption = "�������";
            this._shipConditionsGridColumn.FieldName = "Condition";
            this._shipConditionsGridColumn.Name = "_shipConditionsGridColumn";
            this._shipConditionsGridColumn.Tag = "�������#r";
            this._shipConditionsGridColumn.Visible = true;
            this._shipConditionsGridColumn.VisibleIndex = 4;
            // 
            // Item32
            // 
            this.Item32.EditorPreferredWidth = 178;
            this.Item32.ImageIndex = 5;
            this.Item32.Location = new System.Drawing.Point(0, 0);
            this.Item32.Name = "Item32";
            this.Item32.Size = new System.Drawing.Size(261, 30);
            this.Item32.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.Item32.TextSize = new System.Drawing.Size(68, 16);
            this.Item32.TextToControlDistance = 5;
            // 
            // _personCategoryLayoutViewColumn
            // 
            this._personCategoryLayoutViewColumn.Caption = "���������";
            this._personCategoryLayoutViewColumn.ColumnEdit = this._personCategoryComboBox;
            this._personCategoryLayoutViewColumn.FieldName = "Category";
            this._personCategoryLayoutViewColumn.LayoutViewField = this.Item33;
            this._personCategoryLayoutViewColumn.Name = "_personCategoryLayoutViewColumn";
            this._personCategoryLayoutViewColumn.Tag = "���������#r";
            // 
            // _personCategoryComboBox
            // 
            this._personCategoryComboBox.AutoHeight = false;
            this._personCategoryComboBox.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._personCategoryComboBox.MaxLength = 16;
            this._personCategoryComboBox.Name = "_personCategoryComboBox";
            this._personCategoryComboBox.NullText = "�� ���������";
            this._personCategoryComboBox.Sorted = true;
            // 
            // Item33
            // 
            this.Item33.EditorPreferredWidth = 188;
            this.Item33.Location = new System.Drawing.Point(0, 30);
            this.Item33.Name = "Item33";
            this.Item33.Size = new System.Drawing.Size(261, 30);
            this.Item33.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.Item33.TextSize = new System.Drawing.Size(58, 13);
            this.Item33.TextToControlDistance = 5;
            // 
            // _personAddressLayoutViewColumn
            // 
            this._personAddressLayoutViewColumn.Caption = "�����";
            this._personAddressLayoutViewColumn.ColumnEdit = this._personAddressMemoEdit;
            this._personAddressLayoutViewColumn.FieldName = "Address";
            this._personAddressLayoutViewColumn.ImageIndex = 4;
            this._personAddressLayoutViewColumn.LayoutViewField = this.Item31;
            this._personAddressLayoutViewColumn.Name = "_personAddressLayoutViewColumn";
            this._personAddressLayoutViewColumn.Tag = "�����#r";
            // 
            // _personAddressMemoEdit
            // 
            this._personAddressMemoEdit.MaxLength = 256;
            this._personAddressMemoEdit.Name = "_personAddressMemoEdit";
            this._personAddressMemoEdit.NullText = "�� ������";
            // 
            // Item31
            // 
            this.Item31.EditorPreferredWidth = 161;
            this.Item31.ImageIndex = 4;
            this.Item31.Location = new System.Drawing.Point(0, 0);
            this.Item31.Name = "Item31";
            this.Item31.Size = new System.Drawing.Size(232, 90);
            this.Item31.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.Item31.TextSize = new System.Drawing.Size(56, 16);
            this.Item31.TextToControlDistance = 5;
            // 
            // _personQualificationLayoutViewColumn
            // 
            this._personQualificationLayoutViewColumn.Caption = "������������";
            this._personQualificationLayoutViewColumn.ColumnEdit = this._personQualificationComboBox;
            this._personQualificationLayoutViewColumn.FieldName = "Qualification";
            this._personQualificationLayoutViewColumn.LayoutViewField = this.Item11;
            this._personQualificationLayoutViewColumn.Name = "_personQualificationLayoutViewColumn";
            this._personQualificationLayoutViewColumn.Tag = "������������#r";
            // 
            // _personQualificationComboBox
            // 
            this._personQualificationComboBox.AutoHeight = false;
            this._personQualificationComboBox.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._personQualificationComboBox.MaxLength = 64;
            this._personQualificationComboBox.Name = "_personQualificationComboBox";
            this._personQualificationComboBox.NullText = "�� �������";
            this._personQualificationComboBox.Sorted = true;
            // 
            // Item11
            // 
            this.Item11.EditorPreferredWidth = 127;
            this.Item11.Location = new System.Drawing.Point(0, 0);
            this.Item11.Name = "Item11";
            this.Item11.Size = new System.Drawing.Size(221, 30);
            this.Item11.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.Item11.TextSize = new System.Drawing.Size(79, 13);
            this.Item11.TextToControlDistance = 5;
            // 
            // _personEducationLayoutViewColumn
            // 
            this._personEducationLayoutViewColumn.Caption = "�����������";
            this._personEducationLayoutViewColumn.ColumnEdit = this._personEducationComboBox;
            this._personEducationLayoutViewColumn.FieldName = "Education";
            this._personEducationLayoutViewColumn.LayoutViewField = this.Item17;
            this._personEducationLayoutViewColumn.Name = "_personEducationLayoutViewColumn";
            this._personEducationLayoutViewColumn.Tag = "�����������#r";
            // 
            // _personEducationComboBox
            // 
            this._personEducationComboBox.AutoHeight = false;
            this._personEducationComboBox.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._personEducationComboBox.MaxLength = 64;
            this._personEducationComboBox.Name = "_personEducationComboBox";
            this._personEducationComboBox.NullText = "�� �������";
            this._personEducationComboBox.Sorted = true;
            // 
            // Item17
            // 
            this.Item17.EditorPreferredWidth = 135;
            this.Item17.Location = new System.Drawing.Point(0, 60);
            this.Item17.Name = "Item17";
            this.Item17.Size = new System.Drawing.Size(221, 30);
            this.Item17.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.Item17.TextSize = new System.Drawing.Size(71, 13);
            this.Item17.TextToControlDistance = 5;
            // 
            // _personDegreeLayoutViewColumn
            // 
            this._personDegreeLayoutViewColumn.Caption = "������ �������";
            this._personDegreeLayoutViewColumn.ColumnEdit = this._personDegreeComboBox;
            this._personDegreeLayoutViewColumn.FieldName = "Degree";
            this._personDegreeLayoutViewColumn.LayoutViewField = this.Item24;
            this._personDegreeLayoutViewColumn.Name = "_personDegreeLayoutViewColumn";
            this._personDegreeLayoutViewColumn.Tag = "������ �������#r";
            // 
            // _personDegreeComboBox
            // 
            this._personDegreeComboBox.AutoHeight = false;
            this._personDegreeComboBox.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._personDegreeComboBox.MaxLength = 64;
            this._personDegreeComboBox.Name = "_personDegreeComboBox";
            this._personDegreeComboBox.NullText = "�� �������";
            this._personDegreeComboBox.Sorted = true;
            // 
            // Item24
            // 
            this.Item24.EditorPreferredWidth = 121;
            this.Item24.Location = new System.Drawing.Point(0, 60);
            this.Item24.Name = "Item24";
            this.Item24.Size = new System.Drawing.Size(221, 30);
            this.Item24.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.Item24.TextSize = new System.Drawing.Size(85, 13);
            this.Item24.TextToControlDistance = 5;
            // 
            // _personPositionLayoutViewColumn
            // 
            this._personPositionLayoutViewColumn.Caption = "���������";
            this._personPositionLayoutViewColumn.ColumnEdit = this._personPositionComboBox;
            this._personPositionLayoutViewColumn.FieldName = "Position";
            this._personPositionLayoutViewColumn.LayoutViewField = this.Item28;
            this._personPositionLayoutViewColumn.Name = "_personPositionLayoutViewColumn";
            this._personPositionLayoutViewColumn.Tag = "���������#r";
            // 
            // _personPositionComboBox
            // 
            this._personPositionComboBox.AutoHeight = false;
            this._personPositionComboBox.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._personPositionComboBox.MaxLength = 64;
            this._personPositionComboBox.Name = "_personPositionComboBox";
            this._personPositionComboBox.NullText = "�� �������";
            this._personPositionComboBox.Sorted = true;
            // 
            // Item28
            // 
            this.Item28.EditorPreferredWidth = 145;
            this.Item28.Location = new System.Drawing.Point(0, 0);
            this.Item28.Name = "Item28";
            this.Item28.Size = new System.Drawing.Size(221, 30);
            this.Item28.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.Item28.TextSize = new System.Drawing.Size(61, 13);
            this.Item28.TextToControlDistance = 5;
            // 
            // _personDepartmentLayoutViewColumn
            // 
            this._personDepartmentLayoutViewColumn.Caption = "���������";
            this._personDepartmentLayoutViewColumn.ColumnEdit = this._personDepartmentComboBox;
            this._personDepartmentLayoutViewColumn.FieldName = "Department";
            this._personDepartmentLayoutViewColumn.LayoutViewField = this.Item30;
            this._personDepartmentLayoutViewColumn.Name = "_personDepartmentLayoutViewColumn";
            this._personDepartmentLayoutViewColumn.Tag = "���������#r";
            // 
            // _personDepartmentComboBox
            // 
            this._personDepartmentComboBox.AutoHeight = false;
            this._personDepartmentComboBox.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._personDepartmentComboBox.MaxLength = 64;
            this._personDepartmentComboBox.Name = "_personDepartmentComboBox";
            this._personDepartmentComboBox.NullText = "�� �������";
            this._personDepartmentComboBox.Sorted = true;
            // 
            // Item30
            // 
            this.Item30.EditorPreferredWidth = 148;
            this.Item30.Location = new System.Drawing.Point(0, 30);
            this.Item30.Name = "Item30";
            this.Item30.Size = new System.Drawing.Size(221, 30);
            this.Item30.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.Item30.TextSize = new System.Drawing.Size(58, 13);
            this.Item30.TextToControlDistance = 5;
            // 
            // _personAffiliationLayoutViewColumn
            // 
            this._personAffiliationLayoutViewColumn.Caption = "��������������";
            this._personAffiliationLayoutViewColumn.ColumnEdit = this._personAffiliationComboBox;
            this._personAffiliationLayoutViewColumn.FieldName = "Affiliation";
            this._personAffiliationLayoutViewColumn.LayoutViewField = this.Item29;
            this._personAffiliationLayoutViewColumn.Name = "_personAffiliationLayoutViewColumn";
            this._personAffiliationLayoutViewColumn.Tag = "��������������#r";
            // 
            // _personAffiliationComboBox
            // 
            this._personAffiliationComboBox.AutoHeight = false;
            this._personAffiliationComboBox.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._personAffiliationComboBox.MaxLength = 64;
            this._personAffiliationComboBox.Name = "_personAffiliationComboBox";
            this._personAffiliationComboBox.NullText = "�� �������";
            this._personAffiliationComboBox.Sorted = true;
            // 
            // Item29
            // 
            this.Item29.EditorPreferredWidth = 115;
            this.Item29.Location = new System.Drawing.Point(0, 30);
            this.Item29.Name = "Item29";
            this.Item29.Size = new System.Drawing.Size(221, 30);
            this.Item29.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.Item29.TextSize = new System.Drawing.Size(91, 13);
            this.Item29.TextToControlDistance = 5;
            // 
            // _personChildNickNameLayoutViewColumn
            // 
            this._personChildNickNameLayoutViewColumn.Caption = "������� ��������";
            this._personChildNickNameLayoutViewColumn.ColumnEdit = this._personChildNickNameTextEdit;
            this._personChildNickNameLayoutViewColumn.FieldName = "ChildNickName";
            this._personChildNickNameLayoutViewColumn.LayoutViewField = this.Item25;
            this._personChildNickNameLayoutViewColumn.Name = "_personChildNickNameLayoutViewColumn";
            this._personChildNickNameLayoutViewColumn.Tag = "������� ��������#r";
            // 
            // _personChildNickNameTextEdit
            // 
            this._personChildNickNameTextEdit.AutoHeight = false;
            this._personChildNickNameTextEdit.MaxLength = 64;
            this._personChildNickNameTextEdit.Name = "_personChildNickNameTextEdit";
            this._personChildNickNameTextEdit.NullText = "�� �������";
            // 
            // Item25
            // 
            this.Item25.EditorPreferredWidth = 117;
            this.Item25.Location = new System.Drawing.Point(0, 60);
            this.Item25.Name = "Item25";
            this.Item25.Size = new System.Drawing.Size(232, 30);
            this.Item25.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.Item25.TextSize = new System.Drawing.Size(100, 13);
            this.Item25.TextToControlDistance = 5;
            // 
            // _personDepartureDateLayoutViewColumn
            // 
            this._personDepartureDateLayoutViewColumn.Caption = "������";
            this._personDepartureDateLayoutViewColumn.ColumnEdit = this._personDepartureDateEdit;
            this._personDepartureDateLayoutViewColumn.FieldName = "DepartureDate";
            this._personDepartureDateLayoutViewColumn.LayoutViewField = this.Item39;
            this._personDepartureDateLayoutViewColumn.Name = "_personDepartureDateLayoutViewColumn";
            this._personDepartureDateLayoutViewColumn.Tag = "������#r";
            // 
            // _personDepartureDateEdit
            // 
            this._personDepartureDateEdit.AutoHeight = false;
            this._personDepartureDateEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._personDepartureDateEdit.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this._personDepartureDateEdit.Name = "_personDepartureDateEdit";
            this._personDepartureDateEdit.NullText = "�� ���������";
            // 
            // Item39
            // 
            this.Item39.EditorPreferredWidth = 163;
            this.Item39.Location = new System.Drawing.Point(40, 203);
            this.Item39.Name = "Item39";
            this.Item39.Size = new System.Drawing.Size(221, 30);
            this.Item39.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.Item39.TextSize = new System.Drawing.Size(43, 13);
            this.Item39.TextToControlDistance = 5;
            // 
            // _personPeriodLayoutViewColumn
            // 
            this._personPeriodLayoutViewColumn.AppearanceCell.Options.UseTextOptions = true;
            this._personPeriodLayoutViewColumn.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this._personPeriodLayoutViewColumn.Caption = "����";
            this._personPeriodLayoutViewColumn.ColumnEdit = this._personPeriodTextEdit;
            this._personPeriodLayoutViewColumn.FieldName = "Period";
            this._personPeriodLayoutViewColumn.LayoutViewField = this.Item40;
            this._personPeriodLayoutViewColumn.Name = "_personPeriodLayoutViewColumn";
            this._personPeriodLayoutViewColumn.Tag = "����#r";
            // 
            // _personPeriodTextEdit
            // 
            this._personPeriodTextEdit.AutoHeight = false;
            this._personPeriodTextEdit.MaxLength = 32;
            this._personPeriodTextEdit.Name = "_personPeriodTextEdit";
            // 
            // Item40
            // 
            this.Item40.EditorPreferredWidth = 66;
            this.Item40.Location = new System.Drawing.Point(0, 233);
            this.Item40.Name = "Item40";
            this.Item40.Size = new System.Drawing.Size(110, 30);
            this.Item40.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.Item40.TextSize = new System.Drawing.Size(29, 13);
            this.Item40.TextToControlDistance = 5;
            // 
            // _personTripCountLayoutViewColumn
            // 
            this._personTripCountLayoutViewColumn.AppearanceCell.Options.UseTextOptions = true;
            this._personTripCountLayoutViewColumn.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this._personTripCountLayoutViewColumn.Caption = "���������� �����";
            this._personTripCountLayoutViewColumn.ColumnEdit = this._personTripCountSpinEdit;
            this._personTripCountLayoutViewColumn.FieldName = "TripCount";
            this._personTripCountLayoutViewColumn.LayoutViewField = this.Item38;
            this._personTripCountLayoutViewColumn.Name = "_personTripCountLayoutViewColumn";
            this._personTripCountLayoutViewColumn.Tag = "���������� �����#r";
            // 
            // _personTripCountSpinEdit
            // 
            this._personTripCountSpinEdit.Appearance.Options.UseTextOptions = true;
            this._personTripCountSpinEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this._personTripCountSpinEdit.AutoHeight = false;
            this._personTripCountSpinEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this._personTripCountSpinEdit.IsFloatValue = false;
            this._personTripCountSpinEdit.Mask.EditMask = "N0";
            this._personTripCountSpinEdit.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this._personTripCountSpinEdit.Name = "_personTripCountSpinEdit";
            this._personTripCountSpinEdit.NullText = "�� �������";
            // 
            // Item38
            // 
            this.Item38.EditorPreferredWidth = 38;
            this.Item38.Location = new System.Drawing.Point(110, 233);
            this.Item38.Name = "Item38";
            this.Item38.Size = new System.Drawing.Size(151, 30);
            this.Item38.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.Item38.TextSize = new System.Drawing.Size(98, 13);
            this.Item38.TextToControlDistance = 5;
            // 
            // _personTeachingStartDateLayoutViewColumn
            // 
            this._personTeachingStartDateLayoutViewColumn.Caption = "����� �� ��������";
            this._personTeachingStartDateLayoutViewColumn.ColumnEdit = this._personTeachingStartDateEdit;
            this._personTeachingStartDateLayoutViewColumn.FieldName = "TeachingStartDate";
            this._personTeachingStartDateLayoutViewColumn.LayoutViewField = this.Item41;
            this._personTeachingStartDateLayoutViewColumn.Name = "_personTeachingStartDateLayoutViewColumn";
            this._personTeachingStartDateLayoutViewColumn.Tag = "����� �� �������� #r";
            // 
            // _personTeachingStartDateEdit
            // 
            this._personTeachingStartDateEdit.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this._personTeachingStartDateEdit.AutoHeight = false;
            this._personTeachingStartDateEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._personTeachingStartDateEdit.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this._personTeachingStartDateEdit.Name = "_personTeachingStartDateEdit";
            this._personTeachingStartDateEdit.NullText = "�� ���������";
            // 
            // Item41
            // 
            this.Item41.EditorPreferredWidth = 103;
            this.Item41.Location = new System.Drawing.Point(40, 143);
            this.Item41.Name = "Item41";
            this.Item41.Size = new System.Drawing.Size(221, 30);
            this.Item41.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.Item41.TextSize = new System.Drawing.Size(103, 13);
            this.Item41.TextToControlDistance = 5;
            // 
            // _personExaminationDateLayoutViewColumn
            // 
            this._personExaminationDateLayoutViewColumn.Caption = "�������";
            this._personExaminationDateLayoutViewColumn.ColumnEdit = this._personExaminationDateEdit;
            this._personExaminationDateLayoutViewColumn.FieldName = "ExaminationDate";
            this._personExaminationDateLayoutViewColumn.LayoutViewField = this.Item34;
            this._personExaminationDateLayoutViewColumn.Name = "_personExaminationDateLayoutViewColumn";
            this._personExaminationDateLayoutViewColumn.Tag = "�������#r";
            // 
            // _personExaminationDateEdit
            // 
            this._personExaminationDateEdit.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this._personExaminationDateEdit.AutoHeight = false;
            this._personExaminationDateEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._personExaminationDateEdit.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this._personExaminationDateEdit.Name = "_personExaminationDateEdit";
            this._personExaminationDateEdit.NullText = "�� ���������";
            // 
            // Item34
            // 
            this.Item34.EditorPreferredWidth = 160;
            this.Item34.Location = new System.Drawing.Point(40, 173);
            this.Item34.Name = "Item34";
            this.Item34.Size = new System.Drawing.Size(221, 30);
            this.Item34.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.Item34.TextSize = new System.Drawing.Size(46, 13);
            this.Item34.TextToControlDistance = 5;
            // 
            // _personAppointmentOrderCodeLayoutViewColumn
            // 
            this._personAppointmentOrderCodeLayoutViewColumn.Caption = "�";
            this._personAppointmentOrderCodeLayoutViewColumn.ColumnEdit = this._personCodeTextEdit;
            this._personAppointmentOrderCodeLayoutViewColumn.FieldName = "AppointmentOrderCode";
            this._personAppointmentOrderCodeLayoutViewColumn.LayoutViewField = this.Item35;
            this._personAppointmentOrderCodeLayoutViewColumn.Name = "_personAppointmentOrderCodeLayoutViewColumn";
            this._personAppointmentOrderCodeLayoutViewColumn.Tag = "����� �������#r";
            // 
            // _personCodeTextEdit
            // 
            this._personCodeTextEdit.AutoHeight = false;
            this._personCodeTextEdit.MaxLength = 16;
            this._personCodeTextEdit.Name = "_personCodeTextEdit";
            this._personCodeTextEdit.NullText = "�� ������";
            // 
            // Item35
            // 
            this.Item35.EditorPreferredWidth = 100;
            this.Item35.Location = new System.Drawing.Point(129, 83);
            this.Item35.Name = "Item35";
            this.Item35.Size = new System.Drawing.Size(132, 30);
            this.Item35.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.Item35.TextSize = new System.Drawing.Size(17, 13);
            this.Item35.TextToControlDistance = 5;
            // 
            // _personAppointmentOrderDateLayoutViewColumn
            // 
            this._personAppointmentOrderDateLayoutViewColumn.Caption = "����";
            this._personAppointmentOrderDateLayoutViewColumn.ColumnEdit = this._personDateEdit;
            this._personAppointmentOrderDateLayoutViewColumn.FieldName = "AppointmentOrderDate";
            this._personAppointmentOrderDateLayoutViewColumn.LayoutViewField = this.Item36;
            this._personAppointmentOrderDateLayoutViewColumn.Name = "_personAppointmentOrderDateLayoutViewColumn";
            this._personAppointmentOrderDateLayoutViewColumn.Tag = "���� �������#r";
            // 
            // _personDateEdit
            // 
            this._personDateEdit.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this._personDateEdit.AutoHeight = false;
            this._personDateEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._personDateEdit.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this._personDateEdit.Name = "_personDateEdit";
            this._personDateEdit.NullText = "�� �������";
            // 
            // Item36
            // 
            this.Item36.EditorPreferredWidth = 87;
            this.Item36.Location = new System.Drawing.Point(129, 113);
            this.Item36.Name = "Item36";
            this.Item36.Size = new System.Drawing.Size(132, 30);
            this.Item36.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.Item36.TextSize = new System.Drawing.Size(30, 13);
            this.Item36.TextToControlDistance = 5;
            // 
            // _personStatusLayoutViewColumn
            // 
            this._personStatusLayoutViewColumn.Caption = "������";
            this._personStatusLayoutViewColumn.ColumnEdit = this._personStatusComboBox;
            this._personStatusLayoutViewColumn.FieldName = "Statuse";
            this._personStatusLayoutViewColumn.LayoutViewField = this.Item44;
            this._personStatusLayoutViewColumn.Name = "_personStatusLayoutViewColumn";
            this._personStatusLayoutViewColumn.Tag = "������#r";
            // 
            // _personStatusComboBox
            // 
            this._personStatusComboBox.AutoHeight = false;
            this._personStatusComboBox.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._personStatusComboBox.MaxLength = 16;
            this._personStatusComboBox.Name = "_personStatusComboBox";
            this._personStatusComboBox.NullText = "�� ������";
            this._personStatusComboBox.Sorted = true;
            // 
            // Item44
            // 
            this.Item44.EditorPreferredWidth = 580;
            this.Item44.Location = new System.Drawing.Point(0, 242);
            this.Item44.Name = "Item44";
            this.Item44.Size = new System.Drawing.Size(635, 52);
            this.Item44.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.Item44.TextSize = new System.Drawing.Size(40, 13);
            this.Item44.TextToControlDistance = 5;
            // 
            // _personPhotoLayoutViewColumn
            // 
            this._personPhotoLayoutViewColumn.Caption = "����";
            this._personPhotoLayoutViewColumn.ColumnEdit = this._personPhotoPictureEdit;
            this._personPhotoLayoutViewColumn.FieldName = "PhotoData";
            this._personPhotoLayoutViewColumn.LayoutViewField = this.Item43;
            this._personPhotoLayoutViewColumn.Name = "_personPhotoLayoutViewColumn";
            this._personPhotoLayoutViewColumn.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this._personPhotoLayoutViewColumn.OptionsFilter.AllowAutoFilter = false;
            this._personPhotoLayoutViewColumn.OptionsFilter.AllowFilter = false;
            this._personPhotoLayoutViewColumn.Tag = "..\\����#r";
            // 
            // _personPhotoPictureEdit
            // 
            this._personPhotoPictureEdit.Name = "_personPhotoPictureEdit";
            this._personPhotoPictureEdit.PictureStoreMode = DevExpress.XtraEditors.Controls.PictureStoreMode.ByteArray;
            this._personPhotoPictureEdit.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            // 
            // Item43
            // 
            this.Item43.EditorPreferredWidth = 136;
            this.Item43.Location = new System.Drawing.Point(0, 0);
            this.Item43.Name = "Item43";
            this.Item43.Size = new System.Drawing.Size(146, 211);
            this.Item43.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.Item43.TextLocation = DevExpress.Utils.Locations.Top;
            this.Item43.TextSize = new System.Drawing.Size(0, 0);
            this.Item43.TextToControlDistance = 0;
            this.Item43.TextVisible = false;
            // 
            // _personRequestCodeLayoutViewColumn
            // 
            this._personRequestCodeLayoutViewColumn.Caption = "�";
            this._personRequestCodeLayoutViewColumn.ColumnEdit = this._personCodeTextEdit;
            this._personRequestCodeLayoutViewColumn.FieldName = "RequestCode";
            this._personRequestCodeLayoutViewColumn.LayoutViewField = this.layoutViewField_layoutViewColumn1;
            this._personRequestCodeLayoutViewColumn.Name = "_personRequestCodeLayoutViewColumn";
            this._personRequestCodeLayoutViewColumn.Tag = "����� ������#r";
            // 
            // layoutViewField_layoutViewColumn1
            // 
            this.layoutViewField_layoutViewColumn1.EditorPreferredWidth = 97;
            this.layoutViewField_layoutViewColumn1.Location = new System.Drawing.Point(0, 83);
            this.layoutViewField_layoutViewColumn1.Name = "layoutViewField_layoutViewColumn1";
            this.layoutViewField_layoutViewColumn1.Size = new System.Drawing.Size(129, 30);
            this.layoutViewField_layoutViewColumn1.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutViewField_layoutViewColumn1.TextSize = new System.Drawing.Size(17, 13);
            this.layoutViewField_layoutViewColumn1.TextToControlDistance = 5;
            // 
            // _personRequestDateLayoutViewColumn
            // 
            this._personRequestDateLayoutViewColumn.Caption = "����";
            this._personRequestDateLayoutViewColumn.ColumnEdit = this._personDateEdit;
            this._personRequestDateLayoutViewColumn.FieldName = "RequestDate";
            this._personRequestDateLayoutViewColumn.LayoutViewField = this.layoutViewField_layoutViewColumn1_1;
            this._personRequestDateLayoutViewColumn.Name = "_personRequestDateLayoutViewColumn";
            this._personRequestDateLayoutViewColumn.Tag = "���� ������#r";
            // 
            // layoutViewField_layoutViewColumn1_1
            // 
            this.layoutViewField_layoutViewColumn1_1.EditorPreferredWidth = 84;
            this.layoutViewField_layoutViewColumn1_1.Location = new System.Drawing.Point(0, 113);
            this.layoutViewField_layoutViewColumn1_1.Name = "layoutViewField_layoutViewColumn1_1";
            this.layoutViewField_layoutViewColumn1_1.Size = new System.Drawing.Size(129, 30);
            this.layoutViewField_layoutViewColumn1_1.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutViewField_layoutViewColumn1_1.TextSize = new System.Drawing.Size(30, 13);
            this.layoutViewField_layoutViewColumn1_1.TextToControlDistance = 5;
            // 
            // layoutViewCard1
            // 
            this.layoutViewCard1.CustomizationFormText = "layoutViewTemplateCard";
            this.layoutViewCard1.ExpandButtonLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutViewCard1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this._qualificationInfoGroup,
            this._personalInfoGroup,
            this._qadreInfoGroup,
            this._contactInfoGroup,
            this._appointmentGroup,
            this.Item44,
            this._photoGroup});
            this.layoutViewCard1.Name = "layoutViewTemplateCard";
            this.layoutViewCard1.OptionsItemText.TextToControlDistance = 5;
            this.layoutViewCard1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutViewCard1.Text = "TemplateCard";
            // 
            // _qualificationInfoGroup
            // 
            this._qualificationInfoGroup.CustomizationFormText = "���������������� ����������";
            this._qualificationInfoGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.Item11,
            this.Item24,
            this.Item29});
            this._qualificationInfoGroup.Location = new System.Drawing.Point(402, 0);
            this._qualificationInfoGroup.Name = "_qualificationInfoGroup";
            this._qualificationInfoGroup.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this._qualificationInfoGroup.Size = new System.Drawing.Size(233, 121);
            this._qualificationInfoGroup.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this._qualificationInfoGroup.Tag = "���������������� ������#r";
            this._qualificationInfoGroup.Text = "���������������� ������";
            // 
            // _personalInfoGroup
            // 
            this._personalInfoGroup.CustomizationFormText = "������ ������";
            this._personalInfoGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.Item25,
            this.Item26,
            this.Item27});
            this._personalInfoGroup.Location = new System.Drawing.Point(158, 0);
            this._personalInfoGroup.Name = "_personalInfoGroup";
            this._personalInfoGroup.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this._personalInfoGroup.Size = new System.Drawing.Size(244, 121);
            this._personalInfoGroup.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this._personalInfoGroup.Tag = "������ ������#r";
            this._personalInfoGroup.Text = "������ ������";
            // 
            // _qadreInfoGroup
            // 
            this._qadreInfoGroup.CustomizationFormText = "�������� ����������";
            this._qadreInfoGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.Item28,
            this.Item30,
            this.Item17});
            this._qadreInfoGroup.Location = new System.Drawing.Point(402, 121);
            this._qadreInfoGroup.Name = "_qadreInfoGroup";
            this._qadreInfoGroup.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this._qadreInfoGroup.Size = new System.Drawing.Size(233, 121);
            this._qadreInfoGroup.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this._qadreInfoGroup.Tag = "�������� ����������#r";
            this._qadreInfoGroup.Text = "�������� ����������";
            // 
            // _contactInfoGroup
            // 
            this._contactInfoGroup.CustomizationFormText = "���������� ����������";
            this._contactInfoGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.Item31});
            this._contactInfoGroup.Location = new System.Drawing.Point(158, 121);
            this._contactInfoGroup.Name = "_contactInfoGroup";
            this._contactInfoGroup.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this._contactInfoGroup.Size = new System.Drawing.Size(244, 121);
            this._contactInfoGroup.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this._contactInfoGroup.Tag = "���������� ����������#r";
            this._contactInfoGroup.Text = "���������� ����������";
            // 
            // _appointmentGroup
            // 
            this._appointmentGroup.CustomizationFormText = "����������";
            this._appointmentGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.Item32,
            this.Item33,
            this.Item34,
            this.Item38,
            this.Item39,
            this.Item40,
            this.Item41,
            this.Item42,
            this._requestOrderItem,
            this.layoutViewField_layoutViewColumn1,
            this.layoutViewField_layoutViewColumn1_1,
            this.Item36,
            this.Item35,
            this._appointmentOrderItem});
            this._appointmentGroup.Location = new System.Drawing.Point(635, 0);
            this._appointmentGroup.Name = "_appointmentGroup";
            this._appointmentGroup.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this._appointmentGroup.Size = new System.Drawing.Size(273, 294);
            this._appointmentGroup.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this._appointmentGroup.Tag = "����������#r";
            this._appointmentGroup.Text = "����������";
            // 
            // Item42
            // 
            this.Item42.AllowHotTrack = false;
            this.Item42.CustomizationFormText = "����";
            this.Item42.Location = new System.Drawing.Point(0, 143);
            this.Item42.Name = "Item42";
            this.Item42.Size = new System.Drawing.Size(40, 90);
            this.Item42.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.Item42.Text = "����";
            this.Item42.TextSize = new System.Drawing.Size(28, 13);
            // 
            // _requestOrderItem
            // 
            this._requestOrderItem.AllowHotTrack = false;
            this._requestOrderItem.CustomizationFormText = "������";
            this._requestOrderItem.Location = new System.Drawing.Point(0, 60);
            this._requestOrderItem.Name = "_requestOrderItem";
            this._requestOrderItem.Size = new System.Drawing.Size(129, 23);
            this._requestOrderItem.Text = "������";
            this._requestOrderItem.TextSize = new System.Drawing.Size(36, 13);
            // 
            // _appointmentOrderItem
            // 
            this._appointmentOrderItem.AllowHotTrack = false;
            this._appointmentOrderItem.CustomizationFormText = "�������";
            this._appointmentOrderItem.Location = new System.Drawing.Point(129, 60);
            this._appointmentOrderItem.Name = "_appointmentOrderItem";
            this._appointmentOrderItem.Size = new System.Drawing.Size(132, 23);
            this._appointmentOrderItem.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this._appointmentOrderItem.Text = "�������";
            this._appointmentOrderItem.TextSize = new System.Drawing.Size(44, 13);
            // 
            // _photoGroup
            // 
            this._photoGroup.CustomizationFormText = "����";
            this._photoGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.Item43});
            this._photoGroup.Location = new System.Drawing.Point(0, 0);
            this._photoGroup.Name = "_photoGroup";
            this._photoGroup.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this._photoGroup.Size = new System.Drawing.Size(158, 242);
            this._photoGroup.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this._photoGroup.Tag = "����#r";
            this._photoGroup.Text = "����";
            // 
            // _splitContainerControl
            // 
            this._splitContainerControl.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this._splitContainerControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._splitContainerControl.Horizontal = false;
            this._splitContainerControl.Location = new System.Drawing.Point(0, 0);
            this._splitContainerControl.Name = "_splitContainerControl";
            this._splitContainerControl.Panel1.Controls.Add(this._personsGridControl);
            this._splitContainerControl.Panel1.Text = "Panel1";
            this._splitContainerControl.Panel2.Controls.Add(this._detailsSplitContainerControl);
            this._splitContainerControl.Panel2.Text = "Panel2";
            this._splitContainerControl.Size = new System.Drawing.Size(1130, 707);
            this._splitContainerControl.SplitterPosition = 375;
            this._splitContainerControl.TabIndex = 4;
            this._splitContainerControl.Text = "splitContainerControl1";
            // 
            // _detailsSplitContainerControl
            // 
            this._detailsSplitContainerControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._detailsSplitContainerControl.Location = new System.Drawing.Point(0, 0);
            this._detailsSplitContainerControl.Name = "_detailsSplitContainerControl";
            this._detailsSplitContainerControl.Panel1.Controls.Add(this._dynamicDetailsXtraTabControl);
            this._detailsSplitContainerControl.Panel1.Text = "Panel1";
            this._detailsSplitContainerControl.Panel2.Controls.Add(this._staticDetailsXtraTabControl);
            this._detailsSplitContainerControl.Panel2.Text = "Panel2";
            this._detailsSplitContainerControl.Size = new System.Drawing.Size(1130, 326);
            this._detailsSplitContainerControl.SplitterPosition = 556;
            this._detailsSplitContainerControl.TabIndex = 1;
            // 
            // _dynamicDetailsXtraTabControl
            // 
            this._dynamicDetailsXtraTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._dynamicDetailsXtraTabControl.Location = new System.Drawing.Point(0, 0);
            this._dynamicDetailsXtraTabControl.Name = "_dynamicDetailsXtraTabControl";
            this._dynamicDetailsXtraTabControl.SelectedTabPage = this._phonesXtraTabPage;
            this._dynamicDetailsXtraTabControl.Size = new System.Drawing.Size(556, 326);
            this._dynamicDetailsXtraTabControl.TabIndex = 0;
            this._dynamicDetailsXtraTabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this._phonesXtraTabPage,
            this._lettersXtraTabPage,
            this._personPropertiesXtraTabPage,
            this._notesXtraTabPage,
            this._professionalXtraTabPage});
            // 
            // _phonesXtraTabPage
            // 
            this._phonesXtraTabPage.Controls.Add(this._phonesGridControl);
            this._phonesXtraTabPage.Image = global::Curriculum.Properties.Resources.Phone16;
            this._phonesXtraTabPage.Name = "_phonesXtraTabPage";
            this._phonesXtraTabPage.Size = new System.Drawing.Size(551, 297);
            this._phonesXtraTabPage.Tag = "������� ��������#r";
            this._phonesXtraTabPage.Text = "��������";
            // 
            // _phonesGridControl
            // 
            this._phonesGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._phonesGridControl.EmbeddedNavigator.Buttons.Append.Tag = "..\\������� ��������#ra";
            this._phonesGridControl.EmbeddedNavigator.Buttons.Edit.Tag = "..\\������� ��������#rc";
            this._phonesGridControl.EmbeddedNavigator.Buttons.Remove.Tag = "..\\������� ��������#rd";
            this._phonesGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.PhonesGridControl_EmbeddedNavigator_ButtonClick);
            this._phonesGridControl.Location = new System.Drawing.Point(0, 0);
            this._phonesGridControl.MainView = this._phonesGridView;
            this._phonesGridControl.Name = "_phonesGridControl";
            this._phonesGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this._phoneTypeComboBox,
            this._phoneNumberTextEdit});
            this._phonesGridControl.Size = new System.Drawing.Size(551, 297);
            this._phonesGridControl.TabIndex = 0;
            this._phonesGridControl.Tag = "..\\������� ��������#r";
            this._phonesGridControl.UseEmbeddedNavigator = true;
            this._phonesGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this._phonesGridView});
            // 
            // _phonesGridView
            // 
            this._phonesGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._phoneTypeGridColumn,
            this._phoneNumberGridColumn});
            this._phonesGridView.GridControl = this._phonesGridControl;
            this._phonesGridView.Name = "_phonesGridView";
            this._phonesGridView.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.PhonesGridView_InitNewRow);
            this._phonesGridView.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.PhonesGridView_FocusedRowChanged);
            this._phonesGridView.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.PhonesGridView_ValidateRow);
            this._phonesGridView.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.PhonesGridView_RowUpdated);
            this._phonesGridView.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.PhonesGridView_ValidatingEditor);
            // 
            // _phoneTypeGridColumn
            // 
            this._phoneTypeGridColumn.Caption = "���";
            this._phoneTypeGridColumn.ColumnEdit = this._phoneTypeComboBox;
            this._phoneTypeGridColumn.FieldName = "PhoneType";
            this._phoneTypeGridColumn.Name = "_phoneTypeGridColumn";
            this._phoneTypeGridColumn.Tag = "���#r";
            this._phoneTypeGridColumn.Visible = true;
            this._phoneTypeGridColumn.VisibleIndex = 0;
            // 
            // _phoneTypeComboBox
            // 
            this._phoneTypeComboBox.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this._phoneTypeComboBox.AutoHeight = false;
            this._phoneTypeComboBox.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._phoneTypeComboBox.MaxLength = 32;
            this._phoneTypeComboBox.Name = "_phoneTypeComboBox";
            this._phoneTypeComboBox.NullText = "�� ������";
            this._phoneTypeComboBox.Sorted = true;
            // 
            // _phoneNumberGridColumn
            // 
            this._phoneNumberGridColumn.Caption = "�����";
            this._phoneNumberGridColumn.ColumnEdit = this._phoneNumberTextEdit;
            this._phoneNumberGridColumn.FieldName = "Number";
            this._phoneNumberGridColumn.Name = "_phoneNumberGridColumn";
            this._phoneNumberGridColumn.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this._phoneNumberGridColumn.Tag = "�����#r";
            this._phoneNumberGridColumn.Visible = true;
            this._phoneNumberGridColumn.VisibleIndex = 1;
            // 
            // _phoneNumberTextEdit
            // 
            this._phoneNumberTextEdit.AutoHeight = false;
            this._phoneNumberTextEdit.MaxLength = 32;
            this._phoneNumberTextEdit.Name = "_phoneNumberTextEdit";
            // 
            // _lettersXtraTabPage
            // 
            this._lettersXtraTabPage.Controls.Add(this._lettersGridControl);
            this._lettersXtraTabPage.Image = global::Curriculum.Properties.Resources.Letter16;
            this._lettersXtraTabPage.ImageIndex = 4;
            this._lettersXtraTabPage.Name = "_lettersXtraTabPage";
            this._lettersXtraTabPage.Size = new System.Drawing.Size(551, 297);
            this._lettersXtraTabPage.Tag = "������� ������#r";
            this._lettersXtraTabPage.Text = "������";
            // 
            // _personPropertiesXtraTabPage
            // 
            this._personPropertiesXtraTabPage.Controls.Add(this._personPropertiesGridControl);
            this._personPropertiesXtraTabPage.Image = global::Curriculum.Properties.Resources.CheckBox16;
            this._personPropertiesXtraTabPage.Name = "_personPropertiesXtraTabPage";
            this._personPropertiesXtraTabPage.Size = new System.Drawing.Size(551, 297);
            this._personPropertiesXtraTabPage.Tag = "������� ���������#r";
            this._personPropertiesXtraTabPage.Text = "���������";
            // 
            // _personPropertiesGridControl
            // 
            this._personPropertiesGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._personPropertiesGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.PhonesGridControl_EmbeddedNavigator_ButtonClick);
            this._personPropertiesGridControl.Location = new System.Drawing.Point(0, 0);
            this._personPropertiesGridControl.MainView = this._personPropertiesGridView;
            this._personPropertiesGridControl.Name = "_personPropertiesGridControl";
            this._personPropertiesGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1});
            this._personPropertiesGridControl.Size = new System.Drawing.Size(551, 297);
            this._personPropertiesGridControl.TabIndex = 1;
            this._personPropertiesGridControl.Tag = "..\\������� ���������#r";
            this._personPropertiesGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this._personPropertiesGridView});
            // 
            // _personPropertiesGridView
            // 
            this._personPropertiesGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._personPropertyNameGridColumn,
            this._personPropertyValueGridColumn});
            this._personPropertiesGridView.GridControl = this._personPropertiesGridControl;
            this._personPropertiesGridView.Name = "_personPropertiesGridView";
            this._personPropertiesGridView.OptionsView.ShowGroupPanel = false;
            this._personPropertiesGridView.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.PersonPropertiesGridViewCustomRowCellEdit);
            this._personPropertiesGridView.CustomUnboundColumnData += new DevExpress.XtraGrid.Views.Base.CustomColumnDataEventHandler(this.PersonPropertiesGridViewCustomUnboundColumnData);
            // 
            // _personPropertyNameGridColumn
            // 
            this._personPropertyNameGridColumn.Caption = "��������";
            this._personPropertyNameGridColumn.FieldName = "Name";
            this._personPropertyNameGridColumn.Name = "_personPropertyNameGridColumn";
            this._personPropertyNameGridColumn.OptionsColumn.AllowEdit = false;
            this._personPropertyNameGridColumn.OptionsColumn.AllowFocus = false;
            this._personPropertyNameGridColumn.OptionsColumn.ReadOnly = true;
            this._personPropertyNameGridColumn.Tag = "��������#r";
            this._personPropertyNameGridColumn.Visible = true;
            this._personPropertyNameGridColumn.VisibleIndex = 0;
            this._personPropertyNameGridColumn.Width = 174;
            // 
            // _personPropertyValueGridColumn
            // 
            this._personPropertyValueGridColumn.Caption = "��������";
            this._personPropertyValueGridColumn.ColumnEdit = this.repositoryItemCheckEdit1;
            this._personPropertyValueGridColumn.FieldName = "_personPropertyValueGridColumn";
            this._personPropertyValueGridColumn.Name = "_personPropertyValueGridColumn";
            this._personPropertyValueGridColumn.Tag = "��������#r";
            this._personPropertyValueGridColumn.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this._personPropertyValueGridColumn.Visible = true;
            this._personPropertyValueGridColumn.VisibleIndex = 1;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // _notesXtraTabPage
            // 
            this._notesXtraTabPage.Controls.Add(this._personNotesRtfEditControl);
            this._notesXtraTabPage.Controls.Add(this._savePersonNotesPanelControl);
            this._notesXtraTabPage.Image = global::Curriculum.Properties.Resources.Notes16;
            this._notesXtraTabPage.ImageIndex = 5;
            this._notesXtraTabPage.Name = "_notesXtraTabPage";
            this._notesXtraTabPage.Size = new System.Drawing.Size(551, 297);
            this._notesXtraTabPage.Tag = "�������#r";
            this._notesXtraTabPage.Text = "�������";
            // 
            // _personNotesRtfEditControl
            // 
            this._personNotesRtfEditControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._personNotesRtfEditControl.Location = new System.Drawing.Point(0, 0);
            this._personNotesRtfEditControl.MinimumSize = new System.Drawing.Size(420, 100);
            this._personNotesRtfEditControl.Name = "_personNotesRtfEditControl";
            this._personNotesRtfEditControl.ReadOnly = false;
            this._personNotesRtfEditControl.Rtf = "{\\rtf1\\ansi\\ansicpg1251\\deff0\\deflang1049{\\fonttbl{\\f0\\fnil\\fcharset204 Microsoft" +
    " Sans Serif;}}\r\n\\viewkind4\\uc1\\pard\\f0\\fs17\\par\r\n}\r\n";
            this._personNotesRtfEditControl.Size = new System.Drawing.Size(551, 267);
            this._personNotesRtfEditControl.TabIndex = 0;
            this._personNotesRtfEditControl.Tag = "..\\�������#r";
            // 
            // _savePersonNotesPanelControl
            // 
            this._savePersonNotesPanelControl.Controls.Add(this._savePersonNotesSimpleButton);
            this._savePersonNotesPanelControl.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._savePersonNotesPanelControl.Location = new System.Drawing.Point(0, 267);
            this._savePersonNotesPanelControl.Name = "_savePersonNotesPanelControl";
            this._savePersonNotesPanelControl.Size = new System.Drawing.Size(551, 30);
            this._savePersonNotesPanelControl.TabIndex = 3;
            // 
            // _savePersonNotesSimpleButton
            // 
            this._savePersonNotesSimpleButton.Dock = System.Windows.Forms.DockStyle.Right;
            this._savePersonNotesSimpleButton.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this._savePersonNotesSimpleButton.Location = new System.Drawing.Point(467, 2);
            this._savePersonNotesSimpleButton.Name = "_savePersonNotesSimpleButton";
            this._savePersonNotesSimpleButton.Size = new System.Drawing.Size(82, 26);
            this._savePersonNotesSimpleButton.TabIndex = 2;
            this._savePersonNotesSimpleButton.Tag = "..\\�������#r";
            this._savePersonNotesSimpleButton.Text = "���������";
            this._savePersonNotesSimpleButton.ToolTip = "��������� ���������";
            this._savePersonNotesSimpleButton.Click += new System.EventHandler(this.SavePersonNotesSimpleButton_Click);
            // 
            // _professionalXtraTabPage
            // 
            this._professionalXtraTabPage.Controls.Add(this.groupControl1);
            this._professionalXtraTabPage.Controls.Add(this._saveProfessionalCredentialsPanelControl);
            this._professionalXtraTabPage.Image = global::Curriculum.Properties.Resources.Professional16;
            this._professionalXtraTabPage.ImageIndex = 10;
            this._professionalXtraTabPage.Name = "_professionalXtraTabPage";
            this._professionalXtraTabPage.Padding = new System.Windows.Forms.Padding(15);
            this._professionalXtraTabPage.Size = new System.Drawing.Size(551, 297);
            this._professionalXtraTabPage.Tag = "������� ������#r";
            this._professionalXtraTabPage.Text = "\"������������\"";
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.tableLayoutPanel);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(15, 15);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Padding = new System.Windows.Forms.Padding(10);
            this.groupControl1.Size = new System.Drawing.Size(521, 237);
            this.groupControl1.TabIndex = 8;
            this.groupControl1.Text = "������� ������";
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 2;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.Controls.Add(this._professionalLoginTextEdit, 1, 0);
            this.tableLayoutPanel.Controls.Add(this._professionalPasswordTextEdit, 1, 1);
            this.tableLayoutPanel.Controls.Add(this._showProfessionalPasswordCheckButton, 1, 2);
            this.tableLayoutPanel.Controls.Add(this._createProfessionalUserSimpleButton, 1, 3);
            this.tableLayoutPanel.Controls.Add(this._deleteProfessionalUserSimpleButton, 0, 3);
            this.tableLayoutPanel.Controls.Add(this.labelControl1, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.labelControl2, 0, 1);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel.Location = new System.Drawing.Point(12, 31);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 4;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.Size = new System.Drawing.Size(497, 194);
            this.tableLayoutPanel.TabIndex = 8;
            // 
            // _professionalLoginTextEdit
            // 
            this._professionalLoginTextEdit.Location = new System.Drawing.Point(251, 3);
            this._professionalLoginTextEdit.Name = "_professionalLoginTextEdit";
            this._professionalLoginTextEdit.Size = new System.Drawing.Size(242, 20);
            this._professionalLoginTextEdit.TabIndex = 10;
            this._professionalLoginTextEdit.Tag = "..\\������� ������#r";
            // 
            // _professionalPasswordTextEdit
            // 
            this._professionalPasswordTextEdit.Location = new System.Drawing.Point(251, 29);
            this._professionalPasswordTextEdit.Name = "_professionalPasswordTextEdit";
            this._professionalPasswordTextEdit.Size = new System.Drawing.Size(242, 20);
            this._professionalPasswordTextEdit.TabIndex = 11;
            this._professionalPasswordTextEdit.Tag = "..\\������� ������#r";
            // 
            // _showProfessionalPasswordCheckButton
            // 
            this._showProfessionalPasswordCheckButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._showProfessionalPasswordCheckButton.Checked = true;
            this._showProfessionalPasswordCheckButton.Location = new System.Drawing.Point(381, 55);
            this._showProfessionalPasswordCheckButton.Name = "_showProfessionalPasswordCheckButton";
            this._showProfessionalPasswordCheckButton.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._showProfessionalPasswordCheckButton.Size = new System.Drawing.Size(113, 20);
            this._showProfessionalPasswordCheckButton.TabIndex = 12;
            this._showProfessionalPasswordCheckButton.Tag = "..\\������� ������#rc";
            this._showProfessionalPasswordCheckButton.Text = "�������� ������";
            this._showProfessionalPasswordCheckButton.CheckedChanged += new System.EventHandler(this.ShowProfessionalPasswordCheckButton_CheckedChanged);
            // 
            // _createProfessionalUserSimpleButton
            // 
            this._createProfessionalUserSimpleButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this._createProfessionalUserSimpleButton.Enabled = false;
            this._createProfessionalUserSimpleButton.Location = new System.Drawing.Point(251, 105);
            this._createProfessionalUserSimpleButton.Name = "_createProfessionalUserSimpleButton";
            this._createProfessionalUserSimpleButton.Size = new System.Drawing.Size(242, 22);
            this._createProfessionalUserSimpleButton.TabIndex = 13;
            this._createProfessionalUserSimpleButton.Tag = "..\\������� ������#ra";
            this._createProfessionalUserSimpleButton.Text = "������� ������� ������";
            this._createProfessionalUserSimpleButton.Click += new System.EventHandler(this.CreateProfessionalUserSimpleButtonClick);
            // 
            // _deleteProfessionalUserSimpleButton
            // 
            this._deleteProfessionalUserSimpleButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this._deleteProfessionalUserSimpleButton.Enabled = false;
            this._deleteProfessionalUserSimpleButton.Location = new System.Drawing.Point(3, 105);
            this._deleteProfessionalUserSimpleButton.Name = "_deleteProfessionalUserSimpleButton";
            this._deleteProfessionalUserSimpleButton.Size = new System.Drawing.Size(242, 22);
            this._deleteProfessionalUserSimpleButton.TabIndex = 14;
            this._deleteProfessionalUserSimpleButton.Tag = "..\\������� ������#rd";
            this._deleteProfessionalUserSimpleButton.Text = "������� ������� ������";
            this._deleteProfessionalUserSimpleButton.Click += new System.EventHandler(this.DeleteProfessionalUserSimpleButtonClick);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(3, 3);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(34, 13);
            this.labelControl1.TabIndex = 15;
            this.labelControl1.Text = "�����:";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(3, 29);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(37, 13);
            this.labelControl2.TabIndex = 16;
            this.labelControl2.Text = "������";
            // 
            // _saveProfessionalCredentialsPanelControl
            // 
            this._saveProfessionalCredentialsPanelControl.Controls.Add(this._saveProfessionalCredentialsButton);
            this._saveProfessionalCredentialsPanelControl.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._saveProfessionalCredentialsPanelControl.Location = new System.Drawing.Point(15, 252);
            this._saveProfessionalCredentialsPanelControl.Name = "_saveProfessionalCredentialsPanelControl";
            this._saveProfessionalCredentialsPanelControl.Size = new System.Drawing.Size(521, 30);
            this._saveProfessionalCredentialsPanelControl.TabIndex = 6;
            // 
            // _saveProfessionalCredentialsButton
            // 
            this._saveProfessionalCredentialsButton.Dock = System.Windows.Forms.DockStyle.Right;
            this._saveProfessionalCredentialsButton.Location = new System.Drawing.Point(436, 2);
            this._saveProfessionalCredentialsButton.Name = "_saveProfessionalCredentialsButton";
            this._saveProfessionalCredentialsButton.Size = new System.Drawing.Size(83, 26);
            this._saveProfessionalCredentialsButton.TabIndex = 8;
            this._saveProfessionalCredentialsButton.Tag = "..\\������� ������#rc";
            this._saveProfessionalCredentialsButton.Text = "���������";
            this._saveProfessionalCredentialsButton.Click += new System.EventHandler(this.SaveProfessionalCredentialsButton_Click);
            // 
            // _staticDetailsXtraTabControl
            // 
            this._staticDetailsXtraTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._staticDetailsXtraTabControl.Location = new System.Drawing.Point(0, 0);
            this._staticDetailsXtraTabControl.Name = "_staticDetailsXtraTabControl";
            this._staticDetailsXtraTabControl.SelectedTabPage = this._lessonsXtraTabPage;
            this._staticDetailsXtraTabControl.Size = new System.Drawing.Size(568, 326);
            this._staticDetailsXtraTabControl.TabIndex = 0;
            this._staticDetailsXtraTabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this._lessonsXtraTabPage});
            // 
            // _lessonsXtraTabPage
            // 
            this._lessonsXtraTabPage.Controls.Add(this._showLessonsLinkLabel);
            this._lessonsXtraTabPage.Controls.Add(this._lessonsControl);
            this._lessonsXtraTabPage.Controls.Add(this._editPersonLessonsPanelControl);
            this._lessonsXtraTabPage.Image = global::Curriculum.Properties.Resources.Lessons16;
            this._lessonsXtraTabPage.ImageIndex = 9;
            this._lessonsXtraTabPage.Name = "_lessonsXtraTabPage";
            this._lessonsXtraTabPage.Size = new System.Drawing.Size(563, 297);
            this._lessonsXtraTabPage.Tag = "������� ����#r";
            this._lessonsXtraTabPage.Text = "������� ����";
            // 
            // _showLessonsLinkLabel
            // 
            this._showLessonsLinkLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._showLessonsLinkLabel.Location = new System.Drawing.Point(0, 0);
            this._showLessonsLinkLabel.Name = "_showLessonsLinkLabel";
            this._showLessonsLinkLabel.Size = new System.Drawing.Size(563, 267);
            this._showLessonsLinkLabel.TabIndex = 6;
            this._showLessonsLinkLabel.TabStop = true;
            this._showLessonsLinkLabel.Tag = "..\\������� ����#r";
            this._showLessonsLinkLabel.Text = "�������� ������� ����";
            this._showLessonsLinkLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this._showLessonsLinkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.ShowLessonsLinkLabel_LinkClicked);
            // 
            // _lessonsControl
            // 
            this._lessonsControl.CurrentDate = new System.DateTime(2014, 9, 5, 0, 0, 0, 0);
            this._lessonsControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lessonsControl.HasInnerHideLogic = false;
            this._lessonsControl.HierarchyParent = this;
            this._lessonsControl.Location = new System.Drawing.Point(0, 0);
            this._lessonsControl.ManualFilter = true;
            this._lessonsControl.Name = "_lessonsControl";
            this._lessonsControl.Person = null;
            this._lessonsControl.PrefixTag = null;
            this._lessonsControl.ReadOnly = true;
            this._lessonsControl.Size = new System.Drawing.Size(563, 267);
            this._lessonsControl.TabIndex = 0;
            this._lessonsControl.Tag = "������� ����#r";
            // 
            // _editPersonLessonsPanelControl
            // 
            this._editPersonLessonsPanelControl.Controls.Add(this._editLessonsSimpleButton);
            this._editPersonLessonsPanelControl.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._editPersonLessonsPanelControl.Location = new System.Drawing.Point(0, 267);
            this._editPersonLessonsPanelControl.Name = "_editPersonLessonsPanelControl";
            this._editPersonLessonsPanelControl.Size = new System.Drawing.Size(563, 30);
            this._editPersonLessonsPanelControl.TabIndex = 5;
            // 
            // _editLessonsSimpleButton
            // 
            this._editLessonsSimpleButton.Dock = System.Windows.Forms.DockStyle.Right;
            this._editLessonsSimpleButton.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this._editLessonsSimpleButton.Location = new System.Drawing.Point(451, 2);
            this._editLessonsSimpleButton.Name = "_editLessonsSimpleButton";
            this._editLessonsSimpleButton.Size = new System.Drawing.Size(110, 26);
            this._editLessonsSimpleButton.TabIndex = 2;
            this._editLessonsSimpleButton.Tag = "..\\������� ����#rc";
            this._editLessonsSimpleButton.Text = "�������������";
            this._editLessonsSimpleButton.ToolTip = "�������� ������� ����";
            this._editLessonsSimpleButton.Click += new System.EventHandler(this.EditLessonsSimpleButton_Click);
            // 
            // _barManager
            // 
            this._barManager.DockControls.Add(this.barDockControlTop);
            this._barManager.DockControls.Add(this.barDockControlBottom);
            this._barManager.DockControls.Add(this.barDockControlLeft);
            this._barManager.DockControls.Add(this.barDockControlRight);
            this._barManager.Form = this;
            this._barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this._produceReportBarButtonItem,
            this.barButtonItem5});
            this._barManager.MaxItemId = 5;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(1130, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 707);
            this.barDockControlBottom.Size = new System.Drawing.Size(1130, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 707);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1130, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 707);
            // 
            // _produceReportBarButtonItem
            // 
            this._produceReportBarButtonItem.Caption = "������������ �����";
            this._produceReportBarButtonItem.Glyph = global::Curriculum.Properties.Resources.Reports16;
            this._produceReportBarButtonItem.Id = 0;
            this._produceReportBarButtonItem.Name = "_produceReportBarButtonItem";
            this._produceReportBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.ProduceReportBarButtonItemItemClick);
            // 
            // barButtonItem5
            // 
            this.barButtonItem5.Caption = "barButtonItem5";
            this.barButtonItem5.Id = 4;
            this.barButtonItem5.Name = "barButtonItem5";
            // 
            // _popupMenu
            // 
            this._popupMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this._produceReportBarButtonItem)});
            this._popupMenu.Manager = this._barManager;
            this._popupMenu.Name = "_popupMenu";
            // 
            // PersonsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1130, 707);
            this.Controls.Add(this._splitContainerControl);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.HasInnerHideLogic = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PersonsForm";
            this.Tag = "������ ����������\\�������\\��������#r";
            this.Text = "��������";
            ((System.ComponentModel.ISupportInitialize)(this._lettersGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._letterAddresseeComboBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._letterCodeTextEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._letterDateEdit.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._letterDateEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._lettersGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._personsGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._personsLayoutView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._personIdLayoutViewField)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._personNameTextEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._personBirthDateEdit.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._personBirthDateEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._personShipGridLookUpEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._personShipGridLookUpEditView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._personCategoryComboBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._personAddressMemoEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._personQualificationComboBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._personEducationComboBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._personDegreeComboBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._personPositionComboBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._personDepartmentComboBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._personAffiliationComboBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._personChildNickNameTextEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._personDepartureDateEdit.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._personDepartureDateEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._personPeriodTextEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._personTripCountSpinEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._personTeachingStartDateEdit.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._personTeachingStartDateEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._personExaminationDateEdit.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._personExaminationDateEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._personCodeTextEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._personDateEdit.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._personDateEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._personStatusComboBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._personPhotoPictureEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn1_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewCard1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._qualificationInfoGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._personalInfoGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._qadreInfoGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._contactInfoGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._appointmentGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._requestOrderItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._appointmentOrderItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._photoGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._splitContainerControl)).EndInit();
            this._splitContainerControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._detailsSplitContainerControl)).EndInit();
            this._detailsSplitContainerControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._dynamicDetailsXtraTabControl)).EndInit();
            this._dynamicDetailsXtraTabControl.ResumeLayout(false);
            this._phonesXtraTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._phonesGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._phonesGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._phoneTypeComboBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._phoneNumberTextEdit)).EndInit();
            this._lettersXtraTabPage.ResumeLayout(false);
            this._personPropertiesXtraTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._personPropertiesGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._personPropertiesGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            this._notesXtraTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._savePersonNotesPanelControl)).EndInit();
            this._savePersonNotesPanelControl.ResumeLayout(false);
            this._professionalXtraTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._professionalLoginTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._professionalPasswordTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._saveProfessionalCredentialsPanelControl)).EndInit();
            this._saveProfessionalCredentialsPanelControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._staticDetailsXtraTabControl)).EndInit();
            this._staticDetailsXtraTabControl.ResumeLayout(false);
            this._lessonsXtraTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._editPersonLessonsPanelControl)).EndInit();
            this._editPersonLessonsPanelControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._popupMenu)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl _personsGridControl;
        private DevExpress.XtraGrid.Views.Layout.LayoutView _personsLayoutView;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn _personNameLayoutViewColumn;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit _personNameTextEdit;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn _personBirthDateLayoutViewColumn;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit _personBirthDateEdit;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn _personShipLayoutViewColumn;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn _personCategoryLayoutViewColumn;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox _personCategoryComboBox;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn _personAddressLayoutViewColumn;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn _personQualificationLayoutViewColumn;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn _personEducationLayoutViewColumn;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn _personDegreeLayoutViewColumn;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit _personAddressMemoEdit;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn _personPositionLayoutViewColumn;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn _personDepartmentLayoutViewColumn;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn _personAffiliationLayoutViewColumn;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn _personChildNickNameLayoutViewColumn;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn _personDepartureDateLayoutViewColumn;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn _personPeriodLayoutViewColumn;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn _personTripCountLayoutViewColumn;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn _personTeachingStartDateLayoutViewColumn;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn _personExaminationDateLayoutViewColumn;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn _personAppointmentOrderCodeLayoutViewColumn;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn _personAppointmentOrderDateLayoutViewColumn;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox _personQualificationComboBox;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox _personEducationComboBox;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox _personDegreeComboBox;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox _personPositionComboBox;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox _personDepartmentComboBox;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox _personAffiliationComboBox;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit _personChildNickNameTextEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit _personDepartureDateEdit;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn _personStatusLayoutViewColumn;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox _personStatusComboBox;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit _personTripCountSpinEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit _personTeachingStartDateEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit _personExaminationDateEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit _personCodeTextEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit _personDateEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit _personShipGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView _personShipGridLookUpEditView;
        private DevExpress.XtraGrid.Views.Grid.GridView _lettersGridView;
        private DevExpress.XtraGrid.Columns.GridColumn _letterAddresseeGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _letterCodeGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _letterDateGridColumn;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox _letterAddresseeComboBox;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit _letterCodeTextEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit _letterDateEdit;
        private DevExpress.XtraEditors.SplitContainerControl _splitContainerControl;
        private DevExpress.XtraTab.XtraTabControl _dynamicDetailsXtraTabControl;
        private DevExpress.XtraTab.XtraTabPage _lettersXtraTabPage;
        private DevExpress.XtraGrid.GridControl _lettersGridControl;
        private DevExpress.XtraGrid.Columns.GridColumn _shipNameGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _shipIndexGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _shipRiverGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _shipLineGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _shipConditionsGridColumn;
        private DevExpress.XtraTab.XtraTabPage _professionalXtraTabPage;
        private DevExpress.XtraTab.XtraTabPage _notesXtraTabPage;
        private DevExpress.XtraEditors.SimpleButton _saveProfessionalCredentialsButton;
        private DevExpress.XtraEditors.SplitContainerControl _detailsSplitContainerControl;
        private DevExpress.XtraTab.XtraTabControl _staticDetailsXtraTabControl;
        private DevExpress.XtraTab.XtraTabPage _lessonsXtraTabPage;
        private RtfEditControl _personNotesRtfEditControl;
        private DevExpress.XtraEditors.PanelControl _savePersonNotesPanelControl;
        private DevExpress.XtraEditors.SimpleButton _savePersonNotesSimpleButton;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn _personIdLayoutViewColumn;
        private LessonsControl _lessonsControl;
        private DevExpress.XtraEditors.PanelControl _editPersonLessonsPanelControl;
        private System.Windows.Forms.LinkLabel _showLessonsLinkLabel;
        private DevExpress.XtraEditors.PanelControl _saveProfessionalCredentialsPanelControl;
        //private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private System.Windows.Forms.ImageList _imageList;
        private DevExpress.XtraTab.XtraTabPage _phonesXtraTabPage;
        private DevExpress.XtraGrid.GridControl _phonesGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView _phonesGridView;
        private DevExpress.XtraGrid.Columns.GridColumn _phoneTypeGridColumn;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox _phoneTypeComboBox;
        private DevExpress.XtraGrid.Columns.GridColumn _phoneNumberGridColumn;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit _phoneNumberTextEdit;
        private DevExpress.XtraBars.BarManager _barManager;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem _produceReportBarButtonItem;
        private DevExpress.XtraBars.BarButtonItem barButtonItem5;
        private DevExpress.XtraBars.PopupMenu _popupMenu;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit _personPeriodTextEdit;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn _personPhotoLayoutViewColumn;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit _personPhotoPictureEdit;
        private DevExpress.XtraTab.XtraTabPage _personPropertiesXtraTabPage;
        private DevExpress.XtraGrid.GridControl _personPropertiesGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView _personPropertiesGridView;
        private DevExpress.XtraGrid.Columns.GridColumn _personPropertyNameGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _personPropertyValueGridColumn;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn _personRequestCodeLayoutViewColumn;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn _personRequestDateLayoutViewColumn;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField _personIdLayoutViewField;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField Item27;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField Item26;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField Item32;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField Item33;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField Item31;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField Item11;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField Item17;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField Item24;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField Item28;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField Item30;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField Item29;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField Item25;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField Item39;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField Item40;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField Item38;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField Item41;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField Item34;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField Item35;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField Item36;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField Item44;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField Item43;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn1;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn1_1;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewCard layoutViewCard1;
        private LayoutControlGroup _qualificationInfoGroup;
        private LayoutControlGroup _personalInfoGroup;
        private LayoutControlGroup _qadreInfoGroup;
        private LayoutControlGroup _contactInfoGroup;
        private LayoutControlGroup _appointmentGroup;
        private SimpleLabelItem Item42;
        private SimpleLabelItem _requestOrderItem;
        private SimpleLabelItem _appointmentOrderItem;
        private LayoutControlGroup _photoGroup;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private DevExpress.XtraEditors.TextEdit _professionalLoginTextEdit;
        private DevExpress.XtraEditors.TextEdit _professionalPasswordTextEdit;
        private DevExpress.XtraEditors.CheckButton _showProfessionalPasswordCheckButton;
        private DevExpress.XtraEditors.SimpleButton _createProfessionalUserSimpleButton;
        private DevExpress.XtraEditors.SimpleButton _deleteProfessionalUserSimpleButton;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.SimpleButton _editLessonsSimpleButton;
    }
}
namespace Curriculum
{
    partial class StartForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StartForm));
            this._defaultLookAndFeel = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            this._ribbonControl = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this._shipsBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this._personsBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this._settingsBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this._eventsBarButtonGroup = new DevExpress.XtraBars.BarButtonGroup();
            this._eventsBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this._eventsBarStaticItem = new DevExpress.XtraBars.BarStaticItem();
            this._tabsBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this._cascadeBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this._tileHorizontallyBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this._tileVerticallyBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this._mdiBarButtonGroup = new DevExpress.XtraBars.BarButtonGroup();
            this._timeBarStaticItem = new DevExpress.XtraBars.BarStaticItem();
            this._dateBarStaticItem = new DevExpress.XtraBars.BarStaticItem();
            this._barMdiChildrenListItem = new DevExpress.XtraBars.BarMdiChildrenListItem();
            this._disciplinesBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this._calendarBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this._lockBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this._exitBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this._reportsBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this._birthdaysBarButtonGroup = new DevExpress.XtraBars.BarButtonGroup();
            this._birthdaysBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this._birthdaysCountBarStaticItem = new DevExpress.XtraBars.BarStaticItem();
            this._spaceBarStaticItem = new DevExpress.XtraBars.BarStaticItem();
            this._templatesBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this._panelBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this._crewBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this._infoBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this._unverifiedTestsBtn = new DevExpress.XtraBars.BarButtonItem();
            this._documentsBarButton = new DevExpress.XtraBars.BarButtonItem();
            this._messagesBarButton = new DevExpress.XtraBars.BarButtonItem();
            this._filesBarButton = new DevExpress.XtraBars.BarButtonItem();
            this._menuRibbonPage = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this._menuRibbonPageGroup = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this._curriculumRibbonPageGroup = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this._othersRibbonPageGroup = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this._infoRibbonPageGroup = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this._settingsGroup = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this._applicationsRibbonPageGroup = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this._personPage = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this._ribbonStatusBar = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this._xtraTabbedMdiManager = new DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(this.components);
            this._timeTimer = new System.Windows.Forms.Timer(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this._alertControl = new DevExpress.XtraBars.Alerter.AlertControl(this.components);
            ((System.ComponentModel.ISupportInitialize)(this._ribbonControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._xtraTabbedMdiManager)).BeginInit();
            this.SuspendLayout();
            // 
            // _defaultLookAndFeel
            // 
            this._defaultLookAndFeel.LookAndFeel.SkinName = "Black";
            // 
            // _ribbonControl
            // 
            this._ribbonControl.AutoSizeItems = true;
            this._ribbonControl.ExpandCollapseItem.Id = 0;
            this._ribbonControl.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this._ribbonControl.ExpandCollapseItem,
            this._shipsBarButtonItem,
            this._personsBarButtonItem,
            this._settingsBarButtonItem,
            this._eventsBarButtonGroup,
            this._eventsBarButtonItem,
            this._eventsBarStaticItem,
            this._tabsBarButtonItem,
            this._cascadeBarButtonItem,
            this._tileHorizontallyBarButtonItem,
            this._tileVerticallyBarButtonItem,
            this._mdiBarButtonGroup,
            this._timeBarStaticItem,
            this._dateBarStaticItem,
            this._barMdiChildrenListItem,
            this._disciplinesBarButtonItem,
            this._calendarBarButtonItem,
            this._lockBarButtonItem,
            this._exitBarButtonItem,
            this._reportsBarButtonItem,
            this._birthdaysBarButtonGroup,
            this._birthdaysBarButtonItem,
            this._birthdaysCountBarStaticItem,
            this._spaceBarStaticItem,
            this._templatesBarButtonItem,
            this._panelBarButtonItem,
            this._crewBarButtonItem,
            this._infoBarButtonItem,
            this._unverifiedTestsBtn,
            this._documentsBarButton,
            this._messagesBarButton,
            this._filesBarButton});
            this._ribbonControl.Location = new System.Drawing.Point(0, 0);
            this._ribbonControl.MaxItemId = 57;
            this._ribbonControl.Name = "_ribbonControl";
            this._ribbonControl.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this._menuRibbonPage});
            this._ribbonControl.ShowPageHeadersMode = DevExpress.XtraBars.Ribbon.ShowPageHeadersMode.Hide;
            this._ribbonControl.ShowToolbarCustomizeItem = false;
            this._ribbonControl.Size = new System.Drawing.Size(1075, 144);
            this._ribbonControl.StatusBar = this._ribbonStatusBar;
            this._ribbonControl.Tag = "������ ����������#r";
            this._ribbonControl.Toolbar.ItemLinks.Add(this._timeBarStaticItem);
            this._ribbonControl.Toolbar.ItemLinks.Add(this._dateBarStaticItem);
            this._ribbonControl.Toolbar.ItemLinks.Add(this._eventsBarButtonGroup);
            this._ribbonControl.Toolbar.ItemLinks.Add(this._spaceBarStaticItem);
            this._ribbonControl.Toolbar.ItemLinks.Add(this._birthdaysBarButtonGroup);
            this._ribbonControl.Toolbar.ShowCustomizeItem = false;
            this._ribbonControl.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Above;
            // 
            // _shipsBarButtonItem
            // 
            this._shipsBarButtonItem.Caption = "�������";
            this._shipsBarButtonItem.Description = "��������, �������������� � ����������";
            this._shipsBarButtonItem.Hint = "��������, �������������� � ����������";
            this._shipsBarButtonItem.Id = 0;
            this._shipsBarButtonItem.LargeGlyph = global::Curriculum.Properties.Resources.Ship32;
            this._shipsBarButtonItem.Name = "_shipsBarButtonItem";
            this._shipsBarButtonItem.Tag = "�������#r";
            this._shipsBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.ShipsBarButtonItem_ItemClick);
            // 
            // _personsBarButtonItem
            // 
            this._personsBarButtonItem.Caption = "��������";
            this._personsBarButtonItem.Description = "��������, �������������� � ����������";
            this._personsBarButtonItem.Hint = "��������, �������������� � ����������";
            this._personsBarButtonItem.Id = 1;
            this._personsBarButtonItem.LargeGlyph = global::Curriculum.Properties.Resources.User32;
            this._personsBarButtonItem.Name = "_personsBarButtonItem";
            this._personsBarButtonItem.Tag = "��������#r";
            this._personsBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.PersonsBarButtonItem_ItemClick);
            // 
            // _settingsBarButtonItem
            // 
            this._settingsBarButtonItem.Caption = "���������";
            this._settingsBarButtonItem.Description = "��������� ����������";
            this._settingsBarButtonItem.Hint = "��������� ����������";
            this._settingsBarButtonItem.Id = 3;
            this._settingsBarButtonItem.LargeGlyph = global::Curriculum.Properties.Resources.Settings32;
            this._settingsBarButtonItem.Name = "_settingsBarButtonItem";
            this._settingsBarButtonItem.Tag = "���������#r";
            this._settingsBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.SettingsBarButtonItem_ItemClick);
            // 
            // _eventsBarButtonGroup
            // 
            this._eventsBarButtonGroup.Caption = "������ �������";
            this._eventsBarButtonGroup.Description = "������ ������� �������, �� ����������";
            this._eventsBarButtonGroup.Hint = "������ ������� �������, �� ����������";
            this._eventsBarButtonGroup.Id = 6;
            this._eventsBarButtonGroup.ItemLinks.Add(this._eventsBarButtonItem);
            this._eventsBarButtonGroup.ItemLinks.Add(this._eventsBarStaticItem);
            this._eventsBarButtonGroup.Name = "_eventsBarButtonGroup";
            this._eventsBarButtonGroup.Tag = "������ �������#r";
            // 
            // _eventsBarButtonItem
            // 
            this._eventsBarButtonItem.Caption = "������ �������";
            this._eventsBarButtonItem.Description = "������� � ������ �������� �������";
            this._eventsBarButtonItem.Glyph = global::Curriculum.Properties.Resources.Flag16;
            this._eventsBarButtonItem.Hint = "������� � ������ �������� �������";
            this._eventsBarButtonItem.Id = 7;
            this._eventsBarButtonItem.Name = "_eventsBarButtonItem";
            this._eventsBarButtonItem.Tag = "������ �������#r";
            this._eventsBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.CalendarOrEventsBarButtonItem_ItemClick);
            // 
            // _eventsBarStaticItem
            // 
            this._eventsBarStaticItem.Caption = "0";
            this._eventsBarStaticItem.Description = "���������� ������ ������� �������";
            this._eventsBarStaticItem.Hint = "���������� ������ ������� �������";
            this._eventsBarStaticItem.Id = 9;
            this._eventsBarStaticItem.Name = "_eventsBarStaticItem";
            this._eventsBarStaticItem.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // _tabsBarButtonItem
            // 
            this._tabsBarButtonItem.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this._tabsBarButtonItem.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this._tabsBarButtonItem.Description = "���� � ���� �������";
            this._tabsBarButtonItem.Down = true;
            this._tabsBarButtonItem.Glyph = global::Curriculum.Properties.Resources.Tab16;
            this._tabsBarButtonItem.Hint = "�������� ��� ��������� ����������� �������� ���� � ���� �������";
            this._tabsBarButtonItem.Id = 14;
            this._tabsBarButtonItem.ImageIndex = 2;
            this._tabsBarButtonItem.Name = "_tabsBarButtonItem";
            this._tabsBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.TabsBarButtonItem_ItemClick);
            // 
            // _cascadeBarButtonItem
            // 
            this._cascadeBarButtonItem.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this._cascadeBarButtonItem.Description = "���� ��������";
            this._cascadeBarButtonItem.Glyph = global::Curriculum.Properties.Resources.Cascade16;
            this._cascadeBarButtonItem.Hint = "����������� �������� ���� ��������";
            this._cascadeBarButtonItem.Id = 15;
            this._cascadeBarButtonItem.Name = "_cascadeBarButtonItem";
            this._cascadeBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.CascadeBarButtonItem_ItemClick);
            // 
            // _tileHorizontallyBarButtonItem
            // 
            this._tileHorizontallyBarButtonItem.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this._tileHorizontallyBarButtonItem.Description = "���� �������������";
            this._tileHorizontallyBarButtonItem.Glyph = global::Curriculum.Properties.Resources.TileHorizontal16;
            this._tileHorizontallyBarButtonItem.Hint = "����������� �������� ���� �������������";
            this._tileHorizontallyBarButtonItem.Id = 16;
            this._tileHorizontallyBarButtonItem.Name = "_tileHorizontallyBarButtonItem";
            this._tileHorizontallyBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.TileHorizontallyBarButtonItem_ItemClick);
            // 
            // _tileVerticallyBarButtonItem
            // 
            this._tileVerticallyBarButtonItem.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this._tileVerticallyBarButtonItem.Description = "���� ����������� �����������";
            this._tileVerticallyBarButtonItem.Glyph = global::Curriculum.Properties.Resources.TileVertical16;
            this._tileVerticallyBarButtonItem.Hint = "����������� �������� ���� �����������";
            this._tileVerticallyBarButtonItem.Id = 17;
            this._tileVerticallyBarButtonItem.Name = "_tileVerticallyBarButtonItem";
            this._tileVerticallyBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.TileVerticallyBarButtonItem_ItemClick);
            // 
            // _mdiBarButtonGroup
            // 
            this._mdiBarButtonGroup.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this._mdiBarButtonGroup.Caption = "������������ �������� ����";
            this._mdiBarButtonGroup.Description = "������������ �������� ����";
            this._mdiBarButtonGroup.Hint = "������������ �������� ����";
            this._mdiBarButtonGroup.Id = 19;
            this._mdiBarButtonGroup.ItemLinks.Add(this._tabsBarButtonItem);
            this._mdiBarButtonGroup.ItemLinks.Add(this._cascadeBarButtonItem);
            this._mdiBarButtonGroup.ItemLinks.Add(this._tileVerticallyBarButtonItem);
            this._mdiBarButtonGroup.ItemLinks.Add(this._tileHorizontallyBarButtonItem);
            this._mdiBarButtonGroup.Name = "_mdiBarButtonGroup";
            // 
            // _timeBarStaticItem
            // 
            this._timeBarStaticItem.Description = "������� �����";
            this._timeBarStaticItem.Glyph = global::Curriculum.Properties.Resources.Time16;
            this._timeBarStaticItem.Hint = "������� �����";
            this._timeBarStaticItem.Id = 21;
            this._timeBarStaticItem.Name = "_timeBarStaticItem";
            this._timeBarStaticItem.RightIndent = 3;
            this._timeBarStaticItem.Tag = "������� �����#r";
            this._timeBarStaticItem.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // _dateBarStaticItem
            // 
            this._dateBarStaticItem.Description = "������� ���� � ���� ������";
            this._dateBarStaticItem.Glyph = global::Curriculum.Properties.Resources.Date16;
            this._dateBarStaticItem.Hint = "������� ���� � ���� ������";
            this._dateBarStaticItem.Id = 22;
            this._dateBarStaticItem.Name = "_dateBarStaticItem";
            this._dateBarStaticItem.RightIndent = 12;
            this._dateBarStaticItem.Tag = "���� � ���� ������#r";
            this._dateBarStaticItem.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // _barMdiChildrenListItem
            // 
            this._barMdiChildrenListItem.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this._barMdiChildrenListItem.Description = "�������� ����";
            this._barMdiChildrenListItem.Glyph = global::Curriculum.Properties.Resources.Windows16;
            this._barMdiChildrenListItem.Hint = "�������� ����";
            this._barMdiChildrenListItem.Id = 25;
            this._barMdiChildrenListItem.Name = "_barMdiChildrenListItem";
            // 
            // _disciplinesBarButtonItem
            // 
            this._disciplinesBarButtonItem.Caption = "����";
            this._disciplinesBarButtonItem.Description = "��������, �������������� � ����������";
            this._disciplinesBarButtonItem.Hint = "��������, �������������� � ����������";
            this._disciplinesBarButtonItem.Id = 26;
            this._disciplinesBarButtonItem.LargeGlyph = global::Curriculum.Properties.Resources.Discipline32;
            this._disciplinesBarButtonItem.Name = "_disciplinesBarButtonItem";
            this._disciplinesBarButtonItem.Tag = "����#r";
            this._disciplinesBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.DisciplinesBarButtonItem_ItemClick);
            // 
            // _calendarBarButtonItem
            // 
            this._calendarBarButtonItem.Caption = "���������";
            this._calendarBarButtonItem.Description = "��������� �������� ��������";
            this._calendarBarButtonItem.Hint = "��������� �������� ��������, ������ ����������";
            this._calendarBarButtonItem.Id = 29;
            this._calendarBarButtonItem.LargeGlyph = global::Curriculum.Properties.Resources.Calendar32;
            this._calendarBarButtonItem.Name = "_calendarBarButtonItem";
            this._calendarBarButtonItem.Tag = "���������#r";
            this._calendarBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.CalendarOrEventsBarButtonItem_ItemClick);
            // 
            // _lockBarButtonItem
            // 
            this._lockBarButtonItem.Caption = "�����������";
            this._lockBarButtonItem.Description = "����������� ���� ����������";
            this._lockBarButtonItem.Hint = "����������� ���� ����������";
            this._lockBarButtonItem.Id = 30;
            this._lockBarButtonItem.ImageIndex = 12;
            this._lockBarButtonItem.LargeGlyph = global::Curriculum.Properties.Resources.Lock32;
            this._lockBarButtonItem.Name = "_lockBarButtonItem";
            this._lockBarButtonItem.Tag = "..\\�������� �����#r";
            this._lockBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.LockBarButtonItem_ItemClick);
            // 
            // _exitBarButtonItem
            // 
            this._exitBarButtonItem.Caption = "�����";
            this._exitBarButtonItem.Description = "��������� ������ ����������";
            this._exitBarButtonItem.Hint = "��������� ������ ����������";
            this._exitBarButtonItem.Id = 31;
            this._exitBarButtonItem.LargeGlyph = global::Curriculum.Properties.Resources.Exit32;
            this._exitBarButtonItem.Name = "_exitBarButtonItem";
            this._exitBarButtonItem.Tag = "..\\�������� �����#r";
            this._exitBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.ExitBarButtonItem_ItemClick);
            // 
            // _reportsBarButtonItem
            // 
            this._reportsBarButtonItem.Caption = "������";
            this._reportsBarButtonItem.Description = "������������ �������";
            this._reportsBarButtonItem.Hint = "������������ �������";
            this._reportsBarButtonItem.Id = 32;
            this._reportsBarButtonItem.LargeGlyph = global::Curriculum.Properties.Resources.Reports32;
            this._reportsBarButtonItem.Name = "_reportsBarButtonItem";
            this._reportsBarButtonItem.Tag = "������#r";
            this._reportsBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.ReportsBarButtonItem_ItemClick);
            // 
            // _birthdaysBarButtonGroup
            // 
            this._birthdaysBarButtonGroup.Caption = "��������� ��� ��������";
            this._birthdaysBarButtonGroup.Hint = "��������� ��� ��������, �� ����������";
            this._birthdaysBarButtonGroup.Id = 36;
            this._birthdaysBarButtonGroup.ItemLinks.Add(this._birthdaysBarButtonItem);
            this._birthdaysBarButtonGroup.ItemLinks.Add(this._birthdaysCountBarStaticItem);
            this._birthdaysBarButtonGroup.Name = "_birthdaysBarButtonGroup";
            this._birthdaysBarButtonGroup.Tag = "��� ��������#r";
            // 
            // _birthdaysBarButtonItem
            // 
            this._birthdaysBarButtonItem.Caption = "��������� ��� ��������";
            this._birthdaysBarButtonItem.Glyph = global::Curriculum.Properties.Resources.Birthday16;
            this._birthdaysBarButtonItem.Hint = "������� � ������ ��������� ���� ��������.";
            this._birthdaysBarButtonItem.Id = 39;
            this._birthdaysBarButtonItem.Name = "_birthdaysBarButtonItem";
            this._birthdaysBarButtonItem.Tag = "��� ��������#r";
            this._birthdaysBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BirthdaysBarButtonItem_ItemClick);
            // 
            // _birthdaysCountBarStaticItem
            // 
            this._birthdaysCountBarStaticItem.Caption = "0";
            this._birthdaysCountBarStaticItem.Hint = "���������� ���� �������� ������� � � ������� ��������� 5 ����.";
            this._birthdaysCountBarStaticItem.Id = 41;
            this._birthdaysCountBarStaticItem.Name = "_birthdaysCountBarStaticItem";
            this._birthdaysCountBarStaticItem.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // _spaceBarStaticItem
            // 
            this._spaceBarStaticItem.Id = 45;
            this._spaceBarStaticItem.Name = "_spaceBarStaticItem";
            this._spaceBarStaticItem.ShowImageInToolbar = false;
            this._spaceBarStaticItem.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // _templatesBarButtonItem
            // 
            this._templatesBarButtonItem.Caption = "������� ������";
            this._templatesBarButtonItem.Hint = "��������, �������������� � ����������";
            this._templatesBarButtonItem.Id = 46;
            this._templatesBarButtonItem.LargeGlyph = global::Curriculum.Properties.Resources.Templates32;
            this._templatesBarButtonItem.Name = "_templatesBarButtonItem";
            this._templatesBarButtonItem.Tag = "������� ������#r";
            this._templatesBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.TemplatesBarButtonItem_ItemClick);
            // 
            // _panelBarButtonItem
            // 
            this._panelBarButtonItem.Caption = "������������";
            this._panelBarButtonItem.Hint = "�������� ������������ ��������� �� ����������.";
            this._panelBarButtonItem.Id = 47;
            this._panelBarButtonItem.LargeGlyph = global::Curriculum.Properties.Resources.Panel48;
            this._panelBarButtonItem.Name = "_panelBarButtonItem";
            this._panelBarButtonItem.Tag = "������������#r";
            this._panelBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.PanelBarButtonItemItemClick);
            // 
            // _crewBarButtonItem
            // 
            this._crewBarButtonItem.Caption = "���� �������";
            this._crewBarButtonItem.Description = "�������� ������� ���������";
            this._crewBarButtonItem.Id = 48;
            this._crewBarButtonItem.LargeGlyph = global::Curriculum.Properties.Resources.People32;
            this._crewBarButtonItem.Name = "_crewBarButtonItem";
            this._crewBarButtonItem.Tag = "���� �������#r";
            this._crewBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.CrewBarButtonItemItemClick);
            // 
            // _infoBarButtonItem
            // 
            this._infoBarButtonItem.Caption = "�������";
            this._infoBarButtonItem.Id = 49;
            this._infoBarButtonItem.LargeGlyph = global::Curriculum.Properties.Resources.help32;
            this._infoBarButtonItem.Name = "_infoBarButtonItem";
            this._infoBarButtonItem.Tag = "�������#r";
            this._infoBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this._infoBarButtonItem_ItemClick);
            // 
            // _unverifiedTestsBtn
            // 
            this._unverifiedTestsBtn.Caption = "������������� ����� ";
            this._unverifiedTestsBtn.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this._unverifiedTestsBtn.Id = 51;
            this._unverifiedTestsBtn.LargeGlyph = global::Curriculum.Properties.Resources.Forms32x32;
            this._unverifiedTestsBtn.Name = "_unverifiedTestsBtn";
            this._unverifiedTestsBtn.Tag = "������������� �����#rc";
            this._unverifiedTestsBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this._unverifiedTests_ItemClick);
            // 
            // _documentsBarButton
            // 
            this._documentsBarButton.Caption = "���������";
            this._documentsBarButton.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this._documentsBarButton.Id = 53;
            this._documentsBarButton.LargeGlyph = global::Curriculum.Properties.Resources.Documents32;
            this._documentsBarButton.Name = "_documentsBarButton";
            this._documentsBarButton.Tag = "���������#r";
            // 
            // _messagesBarButton
            // 
            this._messagesBarButton.Caption = "���������";
            this._messagesBarButton.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this._messagesBarButton.Id = 54;
            this._messagesBarButton.LargeGlyph = global::Curriculum.Properties.Resources.Message32;
            this._messagesBarButton.Name = "_messagesBarButton";
            this._messagesBarButton.Tag = "���������#r";
            this._messagesBarButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this._messagesBarButton_ItemClick);
            // 
            // _filesBarButton
            // 
            this._filesBarButton.Caption = "�����";
            this._filesBarButton.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this._filesBarButton.Id = 56;
            this._filesBarButton.LargeGlyph = global::Curriculum.Properties.Resources.Files32;
            this._filesBarButton.Name = "_filesBarButton";
            this._filesBarButton.Tag = "�����#r";
            this._filesBarButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this._filesBarButton_ItemClick);
            // 
            // _menuRibbonPage
            // 
            this._menuRibbonPage.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this._menuRibbonPageGroup,
            this._curriculumRibbonPageGroup,
            this._othersRibbonPageGroup,
            this._settingsGroup,
            this._personPage,
            this._applicationsRibbonPageGroup,
            this._infoRibbonPageGroup});
            this._menuRibbonPage.Name = "_menuRibbonPage";
            this._menuRibbonPage.Text = "����";
            // 
            // _menuRibbonPageGroup
            // 
            this._menuRibbonPageGroup.ItemLinks.Add(this._shipsBarButtonItem);
            this._menuRibbonPageGroup.ItemLinks.Add(this._personsBarButtonItem);
            this._menuRibbonPageGroup.Name = "_menuRibbonPageGroup";
            this._menuRibbonPageGroup.ShowCaptionButton = false;
            this._menuRibbonPageGroup.Tag = "�������#r";
            this._menuRibbonPageGroup.Text = "�������";
            // 
            // _curriculumRibbonPageGroup
            // 
            this._curriculumRibbonPageGroup.ItemLinks.Add(this._disciplinesBarButtonItem);
            this._curriculumRibbonPageGroup.ItemLinks.Add(this._templatesBarButtonItem);
            this._curriculumRibbonPageGroup.ItemLinks.Add(this._calendarBarButtonItem);
            this._curriculumRibbonPageGroup.Name = "_curriculumRibbonPageGroup";
            this._curriculumRibbonPageGroup.ShowCaptionButton = false;
            this._curriculumRibbonPageGroup.Tag = "������� ����#r";
            this._curriculumRibbonPageGroup.Text = "������� ����";
            // 
            // _othersRibbonPageGroup
            // 
            this._othersRibbonPageGroup.ItemLinks.Add(this._crewBarButtonItem);
            this._othersRibbonPageGroup.ItemLinks.Add(this._panelBarButtonItem);
            this._othersRibbonPageGroup.ItemLinks.Add(this._reportsBarButtonItem);
            this._othersRibbonPageGroup.ItemLinks.Add(this._unverifiedTestsBtn);
            this._othersRibbonPageGroup.Name = "_othersRibbonPageGroup";
            this._othersRibbonPageGroup.ShowCaptionButton = false;
            this._othersRibbonPageGroup.Tag = "����������#r";
            this._othersRibbonPageGroup.Text = "����������";
            // 
            // _infoRibbonPageGroup
            // 
            this._infoRibbonPageGroup.AllowTextClipping = false;
            this._infoRibbonPageGroup.ItemLinks.Add(this._infoBarButtonItem);
            this._infoRibbonPageGroup.Name = "_infoRibbonPageGroup";
            this._infoRibbonPageGroup.Tag = "����������#r";
            this._infoRibbonPageGroup.Text = "����������";
            // 
            // _settingsGroup
            // 
            this._settingsGroup.AllowMinimize = false;
            this._settingsGroup.AllowTextClipping = false;
            this._settingsGroup.ItemLinks.Add(this._settingsBarButtonItem);
            this._settingsGroup.Name = "_settingsGroup";
            this._settingsGroup.Tag = "������������#r";
            this._settingsGroup.Text = "������������ ";
            // 
            // _applicationsRibbonPageGroup
            // 
            this._applicationsRibbonPageGroup.ItemLinks.Add(this._lockBarButtonItem);
            this._applicationsRibbonPageGroup.ItemLinks.Add(this._exitBarButtonItem);
            this._applicationsRibbonPageGroup.Name = "_applicationsRibbonPageGroup";
            this._applicationsRibbonPageGroup.ShowCaptionButton = false;
            this._applicationsRibbonPageGroup.Tag = "..\\..\\�������� �����#r";
            this._applicationsRibbonPageGroup.Text = "����������";
            // 
            // _personPage
            // 
            this._personPage.AllowMinimize = false;
            this._personPage.AllowTextClipping = false;
            this._personPage.ItemLinks.Add(this._documentsBarButton);
            this._personPage.ItemLinks.Add(this._messagesBarButton);
            this._personPage.ItemLinks.Add(this._filesBarButton);
            this._personPage.Name = "_personPage";
            this._personPage.Tag = "������ �������#r";
            this._personPage.Text = "������ �������";
            // 
            // _ribbonStatusBar
            // 
            this._ribbonStatusBar.ItemLinks.Add(this._barMdiChildrenListItem);
            this._ribbonStatusBar.ItemLinks.Add(this._mdiBarButtonGroup);
            this._ribbonStatusBar.Location = new System.Drawing.Point(0, 597);
            this._ribbonStatusBar.Name = "_ribbonStatusBar";
            this._ribbonStatusBar.Ribbon = this._ribbonControl;
            this._ribbonStatusBar.Size = new System.Drawing.Size(1075, 24);
            // 
            // _xtraTabbedMdiManager
            // 
            this._xtraTabbedMdiManager.MdiParent = this;
            this._xtraTabbedMdiManager.PageAdded += new DevExpress.XtraTabbedMdi.MdiTabPageEventHandler(this.XtraTabbedMdiManager_PageAdded);
            // 
            // _timeTimer
            // 
            this._timeTimer.Tick += new System.EventHandler(this.TimeTimer_Tick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(0, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 0);
            this.barDockControlBottom.Size = new System.Drawing.Size(0, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 0);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Location = new System.Drawing.Point(0, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 0);
            // 
            // _alertControl
            // 
            this._alertControl.AlertClick += new DevExpress.XtraBars.Alerter.AlertClickEventHandler(this.AlertControlAlertClick);
            // 
            // StartForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1075, 621);
            this.Controls.Add(this._ribbonStatusBar);
            this.Controls.Add(this._ribbonControl);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Name = "StartForm";
            this.Tag = "�������� �����#r";
            this.Text = "��� �������������";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.Shown += new System.EventHandler(this.MainForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this._ribbonControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._xtraTabbedMdiManager)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.LookAndFeel.DefaultLookAndFeel _defaultLookAndFeel;
        private DevExpress.XtraBars.Ribbon.RibbonControl _ribbonControl;
        private DevExpress.XtraBars.BarButtonItem _shipsBarButtonItem;
        private DevExpress.XtraBars.Ribbon.RibbonPage _menuRibbonPage;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup _menuRibbonPageGroup;
        private DevExpress.XtraBars.BarButtonItem _personsBarButtonItem;
        private DevExpress.XtraBars.BarButtonItem _settingsBarButtonItem;
        private DevExpress.XtraTabbedMdi.XtraTabbedMdiManager _xtraTabbedMdiManager;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar _ribbonStatusBar;
        private DevExpress.XtraBars.BarButtonGroup _eventsBarButtonGroup;
        private DevExpress.XtraBars.BarButtonItem _eventsBarButtonItem;
        private DevExpress.XtraBars.BarStaticItem _eventsBarStaticItem;
        private System.Windows.Forms.Timer _timeTimer;
        private DevExpress.XtraBars.BarButtonItem _tabsBarButtonItem;
        private DevExpress.XtraBars.BarButtonItem _cascadeBarButtonItem;
        private DevExpress.XtraBars.BarButtonItem _tileHorizontallyBarButtonItem;
        private DevExpress.XtraBars.BarButtonItem _tileVerticallyBarButtonItem;
        private DevExpress.XtraBars.BarButtonGroup _mdiBarButtonGroup;
        private DevExpress.XtraBars.BarStaticItem _timeBarStaticItem;
        private DevExpress.XtraBars.BarStaticItem _dateBarStaticItem;
        private DevExpress.XtraBars.BarMdiChildrenListItem _barMdiChildrenListItem;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarButtonItem _disciplinesBarButtonItem;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup _curriculumRibbonPageGroup;
        private DevExpress.XtraBars.BarButtonItem _calendarBarButtonItem;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup _applicationsRibbonPageGroup;
        private DevExpress.XtraBars.BarButtonItem _lockBarButtonItem;
        private DevExpress.XtraBars.BarButtonItem _exitBarButtonItem;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup _othersRibbonPageGroup;
        private DevExpress.XtraBars.BarButtonItem _reportsBarButtonItem;
        private DevExpress.XtraBars.BarButtonGroup _birthdaysBarButtonGroup;
        private DevExpress.XtraBars.BarButtonItem _birthdaysBarButtonItem;
        private DevExpress.XtraBars.BarStaticItem _birthdaysCountBarStaticItem;
        private DevExpress.XtraBars.BarStaticItem _spaceBarStaticItem;
        private DevExpress.XtraBars.BarButtonItem _templatesBarButtonItem;
        private DevExpress.XtraBars.BarButtonItem _panelBarButtonItem;
        private DevExpress.XtraBars.BarButtonItem _crewBarButtonItem;
        private DevExpress.XtraBars.BarButtonItem _infoBarButtonItem;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup _infoRibbonPageGroup;
        private DevExpress.XtraBars.Alerter.AlertControl _alertControl;
        private DevExpress.XtraBars.BarButtonItem _unverifiedTestsBtn;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup _settingsGroup;
        private DevExpress.XtraBars.BarButtonItem _documentsBarButton;
        private DevExpress.XtraBars.BarButtonItem _messagesBarButton;
        private DevExpress.XtraBars.BarButtonItem _filesBarButton;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup _personPage;
    }
}
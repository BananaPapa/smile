namespace Curriculum
{
    partial class ReminderForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._listBoxControlEvents = new DevExpress.XtraEditors.ListBoxControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this._simpleButtonDismissAll = new DevExpress.XtraEditors.SimpleButton();
            this._simpleButtonDismiss = new DevExpress.XtraEditors.SimpleButton();
            this._simpleButtonOpenEvent = new DevExpress.XtraEditors.SimpleButton();
            this._simpleButtonSnooze = new DevExpress.XtraEditors.SimpleButton();
            this._durationEditSnoozeTime = new DevExpress.XtraScheduler.UI.DurationEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this._listBoxControlEvents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._durationEditSnoozeTime.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // _listBoxControlEvents
            // 
            this._listBoxControlEvents.DisplayMember = "Subject";
            this._listBoxControlEvents.Location = new System.Drawing.Point(12, 36);
            this._listBoxControlEvents.Name = "_listBoxControlEvents";
            this._listBoxControlEvents.Size = new System.Drawing.Size(370, 211);
            this._listBoxControlEvents.TabIndex = 0;
            this._listBoxControlEvents.Tag = "������ �����������#r";
            this._listBoxControlEvents.SelectedIndexChanged += new System.EventHandler(this.ListBoxControlEventsSelectedIndexChanged);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(12, 12);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(195, 13);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Tag = "������ �����������#r";
            this.labelControl1.Text = "����������� � ��������� ��������:";
            // 
            // _simpleButtonDismissAll
            // 
            this._simpleButtonDismissAll.Location = new System.Drawing.Point(12, 253);
            this._simpleButtonDismissAll.Name = "_simpleButtonDismissAll";
            this._simpleButtonDismissAll.Size = new System.Drawing.Size(83, 23);
            this._simpleButtonDismissAll.TabIndex = 2;
            this._simpleButtonDismissAll.Tag = "������ �����������#rd";
            this._simpleButtonDismissAll.Text = "������ ���";
            this._simpleButtonDismissAll.ToolTip = "������ ��� �����������";
            this._simpleButtonDismissAll.Click += new System.EventHandler(this.SimpleButtonDismissAllClick);
            // 
            // _simpleButtonDismiss
            // 
            this._simpleButtonDismiss.Location = new System.Drawing.Point(302, 253);
            this._simpleButtonDismiss.Name = "_simpleButtonDismiss";
            this._simpleButtonDismiss.Size = new System.Drawing.Size(80, 23);
            this._simpleButtonDismiss.TabIndex = 3;
            this._simpleButtonDismiss.Tag = "������ �����������#rd";
            this._simpleButtonDismiss.Text = "������";
            this._simpleButtonDismiss.ToolTip = "������ �����������";
            this._simpleButtonDismiss.Click += new System.EventHandler(this.SimpleButtonDismissClick);
            // 
            // _simpleButtonOpenEvent
            // 
            this._simpleButtonOpenEvent.Location = new System.Drawing.Point(185, 253);
            this._simpleButtonOpenEvent.Name = "_simpleButtonOpenEvent";
            this._simpleButtonOpenEvent.Size = new System.Drawing.Size(110, 23);
            this._simpleButtonOpenEvent.TabIndex = 4;
            this._simpleButtonOpenEvent.Tag = "������ �����������#r";
            this._simpleButtonOpenEvent.Text = "������� �������";
            this._simpleButtonOpenEvent.Visible = false;
            this._simpleButtonOpenEvent.Click += new System.EventHandler(this.SimpleButtonOpenEventClick);
            // 
            // _simpleButtonSnooze
            // 
            this._simpleButtonSnooze.Location = new System.Drawing.Point(302, 300);
            this._simpleButtonSnooze.Name = "_simpleButtonSnooze";
            this._simpleButtonSnooze.Size = new System.Drawing.Size(81, 23);
            this._simpleButtonSnooze.TabIndex = 5;
            this._simpleButtonSnooze.Tag = "������ �����������\\\\�����������#rc";
            this._simpleButtonSnooze.Text = "���������";
            this._simpleButtonSnooze.ToolTip = "��������� �����";
            this._simpleButtonSnooze.Click += new System.EventHandler(this.SimpleButtonSnoozeClick);
            // 
            // _durationEditSnoozeTime
            // 
            this._durationEditSnoozeTime.Location = new System.Drawing.Point(13, 303);
            this._durationEditSnoozeTime.Name = "_durationEditSnoozeTime";
            this._durationEditSnoozeTime.Properties.ShowEmptyItem = false;
            this._durationEditSnoozeTime.Size = new System.Drawing.Size(283, 20);
            this._durationEditSnoozeTime.TabIndex = 6;
            this._durationEditSnoozeTime.Tag = "������ �����������\\\\�����������#rc";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(13, 284);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(72, 13);
            this.labelControl2.TabIndex = 7;
            this.labelControl2.Tag = "������ �����������\\\\�����������#rc";
            this.labelControl2.Text = "��������� ��:";
            // 
            // ReminderForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(394, 335);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this._durationEditSnoozeTime);
            this.Controls.Add(this._simpleButtonSnooze);
            this.Controls.Add(this._simpleButtonOpenEvent);
            this.Controls.Add(this._simpleButtonDismiss);
            this.Controls.Add(this._simpleButtonDismissAll);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this._listBoxControlEvents);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "ReminderForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "�����������#r";
            this.Text = "�����������: ";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ReminderFormFormClosed);
            ((System.ComponentModel.ISupportInitialize)(this._listBoxControlEvents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._durationEditSnoozeTime.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.ListBoxControl _listBoxControlEvents;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton _simpleButtonDismissAll;
        private DevExpress.XtraEditors.SimpleButton _simpleButtonDismiss;
        private DevExpress.XtraEditors.SimpleButton _simpleButtonOpenEvent;
        private DevExpress.XtraEditors.SimpleButton _simpleButtonSnooze;
        private DevExpress.XtraScheduler.UI.DurationEdit _durationEditSnoozeTime;
        private DevExpress.XtraEditors.LabelControl labelControl2;
    }
}
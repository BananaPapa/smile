using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraScheduler;
using Eureca.Integrator.Common;

namespace Curriculum
{
    public partial class ReminderForm : FormWithAccessAreas
    {
        /// <summary>
        /// Default value for snoozing time.
        /// </summary>
        private const int SnoozeMinutes = 15;

        /// <summary>
        /// Calendar form that contains appointments being reminded by the form.
        /// </summary>
        private readonly CalendarForm _calendarForm;

        /// <summary>
        /// Collection of reminders.
        /// </summary>
        private readonly List<Reminder> _reminders = new List<Reminder>();

        /// <summary>
        /// Initializes new instance of reminder form.
        /// </summary>
        /// <param name="calendarForm">Corresponding calendar form.</param>
        public ReminderForm(CalendarForm calendarForm)
        {
            InitializeComponent();
            Icon = calendarForm.Icon;

            _calendarForm = calendarForm;
            _durationEditSnoozeTime.EditValue = TimeSpan.FromMinutes(SnoozeMinutes);
        }

        /// <summary>
        /// Handles the reminder form closed event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReminderFormFormClosed(object sender, FormClosedEventArgs e)
        {
            DismissAll();
        }

        /// <summary>
        /// Refreshes the alerts list.
        /// </summary>
        private void RefreshAlertsList()
        {
            if (_reminders.Count == 0)
            {
                Close();
            }

            _listBoxControlEvents.BeginUpdate();
            _listBoxControlEvents.DataSource = _reminders;
            _listBoxControlEvents.EndUpdate();
            Text = "Напоминания: " + _reminders.Count;
        }

        /// <summary>
        /// Creates collection of reminders.
        /// </summary>
        /// <param name="alerts">Collection of alerts.</param>
        public void AddAllertNotifications(ReminderAlertNotificationCollection alerts)
        {
            foreach (ReminderAlertNotification alert in alerts)
            {
                _reminders.Add(alert.Reminder);
                alert.Handled = true;
            }
            RefreshAlertsList();
        }

        /// <summary>
        /// Removes reminder of event and important lesson.
        /// </summary>
        /// <param name="reminder">reminder</param>
        private static bool RemoveReminder(Reminder reminder)
        {
            #region Check input params

            Debug.Assert(reminder != null, "Reminder must be not null!");

            if (reminder == null)
            {
                throw new ArgumentNullException("Reminder must be not null!");
            }

            #endregion

            if (reminder.Appointment.CustomFields == null)
            {
                return false;
            }

            var evt = reminder.Appointment.CustomFields["Event"] as Event;
            if (evt != null)
            {
                evt.ReminderTime = null;
            }
            else
            {
                var lesson = reminder.Appointment.CustomFields["Lesson"] as Lesson;
                if (lesson != null)
                {
                    ImportantLesson il = Program.DbSingletone.ImportantLessons.SingleOrDefault(i => i.LessonId == lesson.LessonId);
                    if (il != null)
                    {
                        il.ReminderTime = null;
                        reminder.Appointment.CustomFields["TimeBeforeStart"] = null;
                    }
                }
            }

            try
            {
                Program.DbSingletone.SubmitChanges();
                return true;
            }
            catch (DbException ex)
            {
                Program.HandleDbException(ex);
                return false;
            }
        }

        /// <summary>
        /// Dismisses the reminder.
        /// </summary>
        /// <param name="reminder"></param>
        private static void Dismiss(Reminder reminder)
        {
            if (RemoveReminder(reminder))
            {
                reminder.Dismiss();
            }
        }

        /// <summary>
        /// Dismisses reminders for all events.
        /// </summary>
        private void DismissAll()
        {
            foreach (Reminder reminder in _reminders)
            {
                Dismiss(reminder);
            }
        }

        #region Event Handlers

        /// <summary>
        /// Shows the selected event.
        /// </summary>
        private void ShowSelectedEventForm()
        {
            var reminder = _listBoxControlEvents.SelectedItem as Reminder;
            if (reminder != null)
            {
                if (_calendarForm != null)
                {
                    _calendarForm.ShowAppointment(reminder.Appointment);
                }
            }
        }

        /// <summary>
        /// Handles the dismiss reminder button click event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SimpleButtonDismissClick(object sender, EventArgs e)
        {
            var reminder = _listBoxControlEvents.SelectedItem as Reminder;
            if (reminder != null)
            {
                Dismiss(reminder);
                _reminders.Remove(reminder);
            }
            RefreshAlertsList();
        }

        /// <summary>
        /// Handles the dismiss all reminders button click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void SimpleButtonDismissAllClick(object sender, EventArgs e)
        {
            DismissAll();
            _reminders.Clear();
            RefreshAlertsList();
        }

        /// <summary>
        /// Handles the snooze button click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void SimpleButtonSnoozeClick(object sender, EventArgs e)
        {
            var reminder = _listBoxControlEvents.SelectedItem as Reminder;
            if (reminder != null)
            {
                RemoveReminder(reminder);
                //reminder.Snooze((TimeSpan) _durationEditSnoozeTime.EditValue);

                var evt = reminder.Appointment.CustomFields["Event"] as Event;
                if (evt != null)
                {
                    evt.ReminderTime = (int) ((TimeSpan) _durationEditSnoozeTime.EditValue).TotalSeconds;
                }
                else
                {
                    var lesson = reminder.Appointment.CustomFields["Lesson"] as Lesson;
                    if (lesson != null)
                    {
                        ImportantLesson il =
                            Program.DbSingletone.ImportantLessons.SingleOrDefault(i => i.LessonId == lesson.LessonId);
                        if (il != null)
                        {
                            il.ReminderTime = (int) ((TimeSpan) _durationEditSnoozeTime.EditValue).TotalSeconds;
                            reminder.Appointment.CustomFields["TimeBeforeStart"] =
                                ((TimeSpan) _durationEditSnoozeTime.EditValue);
                        }
                    }
                }

                try
                {
                    Program.DbSingletone.SubmitChanges();
                }
                catch (DbException ex)
                {
                    Program.HandleDbException(ex);
                }

                _reminders.Remove(reminder);

                //re-create treminder
                reminder.Appointment.HasReminder = false;
                reminder.Appointment.HasReminder = true;
                reminder.Appointment.Reminder.TimeBeforeStart = (TimeSpan) _durationEditSnoozeTime.EditValue;
            }
            RefreshAlertsList();
        }

        /// <summary>
        /// Handles the open event button click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void SimpleButtonOpenEventClick(object sender, EventArgs e)
        {
            ShowSelectedEventForm();
        }

        /// <summary>
        /// Handles event list box control selected index changed event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void ListBoxControlEventsSelectedIndexChanged(object sender, EventArgs e)
        {
            var reminder = _listBoxControlEvents.SelectedItem as Reminder;

            _simpleButtonOpenEvent.Visible = reminder != null && reminder.Appointment.CustomFields != null &&
                                             reminder.Appointment.CustomFields["Event"] as Event != null;
        }

        #endregion
    }
}
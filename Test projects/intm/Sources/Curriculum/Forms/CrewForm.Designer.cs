namespace Curriculum
{
    partial class CrewForm
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CrewForm));
            this._editPersonPanelControl = new DevExpress.XtraEditors.PanelControl();
            this.simpleButtonToShips = new DevExpress.XtraEditors.SimpleButton();
            this._personelSimpleButton = new DevExpress.XtraEditors.SimpleButton();
            this._refreshSimpleButton = new DevExpress.XtraEditors.SimpleButton();
            this._reportSimpleButton = new DevExpress.XtraEditors.SimpleButton();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButtonToShips1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonToPersonalFirst = new DevExpress.XtraEditors.SimpleButton();
            this.gridControlFirst = new DevExpress.XtraGrid.GridControl();
            this.gridViewFirst = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnShip = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridViewSecond = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn30 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn32 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn33 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn34 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn35 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn36 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn37 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn38 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn39 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn40 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn41 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn42 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn43 = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this._editPersonPanelControl)).BeginInit();
            this._editPersonPanelControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlFirst)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewFirst)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSecond)).BeginInit();
            this.SuspendLayout();
            // 
            // _editPersonPanelControl
            // 
            this._editPersonPanelControl.Controls.Add(this.simpleButtonToShips);
            this._editPersonPanelControl.Controls.Add(this._personelSimpleButton);
            this._editPersonPanelControl.Controls.Add(this._refreshSimpleButton);
            this._editPersonPanelControl.Controls.Add(this._reportSimpleButton);
            this._editPersonPanelControl.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._editPersonPanelControl.Location = new System.Drawing.Point(0, 539);
            this._editPersonPanelControl.Name = "_editPersonPanelControl";
            this._editPersonPanelControl.Size = new System.Drawing.Size(731, 35);
            this._editPersonPanelControl.TabIndex = 24;
            // 
            // simpleButtonToShips
            // 
            this.simpleButtonToShips.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonToShips.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButtonToShips.Location = new System.Drawing.Point(441, 6);
            this.simpleButtonToShips.Name = "simpleButtonToShips";
            this.simpleButtonToShips.Size = new System.Drawing.Size(91, 29);
            this.simpleButtonToShips.TabIndex = 5;
            this.simpleButtonToShips.Tag = "�����#r";
            this.simpleButtonToShips.Text = "�����";
            this.simpleButtonToShips.ToolTip = "������� �������� ����������� ����������";
            this.simpleButtonToShips.Click += new System.EventHandler(this.simpleButtonToShips_Click);
            // 
            // _personelSimpleButton
            // 
            this._personelSimpleButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._personelSimpleButton.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this._personelSimpleButton.Location = new System.Drawing.Point(538, 5);
            this._personelSimpleButton.Name = "_personelSimpleButton";
            this._personelSimpleButton.Size = new System.Drawing.Size(91, 29);
            this._personelSimpleButton.TabIndex = 4;
            this._personelSimpleButton.Tag = "��������#r";
            this._personelSimpleButton.Text = "��������";
            this._personelSimpleButton.ToolTip = "������� �������� ����������� ����������";
            this._personelSimpleButton.Click += new System.EventHandler(this._personelSimpleButton_Click);
            // 
            // _refreshSimpleButton
            // 
            this._refreshSimpleButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._refreshSimpleButton.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this._refreshSimpleButton.Location = new System.Drawing.Point(5, 6);
            this._refreshSimpleButton.Name = "_refreshSimpleButton";
            this._refreshSimpleButton.Size = new System.Drawing.Size(91, 29);
            this._refreshSimpleButton.TabIndex = 3;
            this._refreshSimpleButton.Tag = "���� �������#r";
            this._refreshSimpleButton.Text = "���� �������";
            this._refreshSimpleButton.ToolTip = "�������� ������";
            this._refreshSimpleButton.Click += new System.EventHandler(this._refreshSimpleButton_Click);
            // 
            // _reportSimpleButton
            // 
            this._reportSimpleButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._reportSimpleButton.Image = global::Curriculum.Properties.Resources.MSWordDocument;
            this._reportSimpleButton.Location = new System.Drawing.Point(635, 6);
            this._reportSimpleButton.Name = "_reportSimpleButton";
            this._reportSimpleButton.Size = new System.Drawing.Size(91, 29);
            this._reportSimpleButton.TabIndex = 2;
            this._reportSimpleButton.Tag = "�����#r";
            this._reportSimpleButton.Text = "�����";
            this._reportSimpleButton.Click += new System.EventHandler(this._reportSimpleButton_Click);
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.splitContainerControl1.Panel1.Controls.Add(this.panelControl1);
            this.splitContainerControl1.Panel1.Controls.Add(this.gridControlFirst);
            this.splitContainerControl1.Panel1.ShowCaption = true;
            this.splitContainerControl1.Panel1.Tag = "��������� �� ��������#r";
            this.splitContainerControl1.Panel1.Text = "��������� �� ��������";
            this.splitContainerControl1.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.splitContainerControl1.Panel2.Controls.Add(this.gridControl1);
            this.splitContainerControl1.Panel2.ShowCaption = true;
            this.splitContainerControl1.Panel2.Tag = "���� �������� ����������� ��� ��� ������#r";
            this.splitContainerControl1.Panel2.Text = "���� �������� ����������� ��� ��� ������";
            this.splitContainerControl1.ShowCaption = true;
            this.splitContainerControl1.Size = new System.Drawing.Size(731, 539);
            this.splitContainerControl1.SplitterPosition = 284;
            this.splitContainerControl1.TabIndex = 25;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.simpleButtonToShips1);
            this.panelControl1.Controls.Add(this.simpleButtonToPersonalFirst);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 231);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(727, 30);
            this.panelControl1.TabIndex = 25;
            // 
            // simpleButtonToShips1
            // 
            this.simpleButtonToShips1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonToShips1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButtonToShips1.Location = new System.Drawing.Point(538, 0);
            this.simpleButtonToShips1.Name = "simpleButtonToShips1";
            this.simpleButtonToShips1.Size = new System.Drawing.Size(91, 29);
            this.simpleButtonToShips1.TabIndex = 5;
            this.simpleButtonToShips1.Tag = "�����#r";
            this.simpleButtonToShips1.Text = "�����";
            this.simpleButtonToShips1.ToolTip = "������� �������� ����������� ����������";
            this.simpleButtonToShips1.Click += new System.EventHandler(this.simpleButtonToShips1_Click);
            // 
            // simpleButtonToPersonalFirst
            // 
            this.simpleButtonToPersonalFirst.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonToPersonalFirst.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButtonToPersonalFirst.Location = new System.Drawing.Point(635, 1);
            this.simpleButtonToPersonalFirst.Name = "simpleButtonToPersonalFirst";
            this.simpleButtonToPersonalFirst.Size = new System.Drawing.Size(91, 29);
            this.simpleButtonToPersonalFirst.TabIndex = 4;
            this.simpleButtonToPersonalFirst.Tag = "��������#r";
            this.simpleButtonToPersonalFirst.Text = "��������";
            this.simpleButtonToPersonalFirst.ToolTip = "������� �������� ����������� ����������";
            this.simpleButtonToPersonalFirst.Click += new System.EventHandler(this.simpleButtonToPersonalFirst_Click);
            // 
            // gridControlFirst
            // 
            this.gridControlFirst.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControlFirst.Location = new System.Drawing.Point(0, 0);
            this.gridControlFirst.MainView = this.gridViewFirst;
            this.gridControlFirst.Name = "gridControlFirst";
            this.gridControlFirst.Size = new System.Drawing.Size(724, 225);
            this.gridControlFirst.TabIndex = 16;
            this.gridControlFirst.Tag = "������� �� ��������#r";
            this.gridControlFirst.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewFirst});
            // 
            // gridViewFirst
            // 
            this.gridViewFirst.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn19,
            this.gridColumn1,
            this.gridColumn14,
            this.gridColumn28,
            this.gridColumn7,
            this.gridColumn3,
            this.gridColumnShip,
            this.gridColumn16,
            this.gridColumn13,
            this.gridColumn8,
            this.gridColumn15,
            this.gridColumn27,
            this.gridColumn17,
            this.gridColumn18,
            this.gridColumn20,
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.gridColumn24,
            this.gridColumn25,
            this.gridColumn9,
            this.gridColumn26});
            this.gridViewFirst.GridControl = this.gridControlFirst;
            this.gridViewFirst.Name = "gridViewFirst";
            this.gridViewFirst.OptionsBehavior.Editable = false;
            this.gridViewFirst.OptionsDetail.EnableMasterViewMode = false;
            this.gridViewFirst.OptionsView.ShowDetailButtons = false;
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "������";
            this.gridColumn19.FieldName = "Person.Degree";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.Tag = "������#r";
            this.gridColumn19.Visible = true;
            this.gridColumn19.VisibleIndex = 0;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "���";
            this.gridColumn1.FieldName = "Person.Name";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Tag = "���#r";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 1;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "���������";
            this.gridColumn14.FieldName = "Person.Position";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.Tag = "���������#r";
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 2;
            // 
            // gridColumn28
            // 
            this.gridColumn28.Caption = "�������������";
            this.gridColumn28.FieldName = "Debts";
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.Tag = "�������������#r";
            this.gridColumn28.Visible = true;
            this.gridColumn28.VisibleIndex = 3;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "���������� �����";
            this.gridColumn7.FieldName = "Person.TripCount";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Tag = "���������� �����#r";
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 4;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "���� ������ ��������";
            this.gridColumn3.FieldName = "Person.TeachingStartDate";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Tag = "���� ������ ��������#r";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 5;
            // 
            // gridColumnShip
            // 
            this.gridColumnShip.Caption = "�������";
            this.gridColumnShip.FieldName = "Person.Ship";
            this.gridColumnShip.Name = "gridColumnShip";
            this.gridColumnShip.Tag = "�������#r";
            this.gridColumnShip.Visible = true;
            this.gridColumnShip.VisibleIndex = 6;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "�������";
            this.gridColumn16.FieldName = "Person.Ship.Condition";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.Tag = "�������#r";
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 7;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "��������";
            this.gridColumn13.FieldName = "ShipTool";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.Tag = "��������#r";
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 8;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "���� ��������";
            this.gridColumn8.FieldName = "Person.ExaminationDate";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Tag = "���� ��������#r";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 9;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "��������";
            this.gridColumn15.FieldName = "One";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.Tag = "��������#r";
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 10;
            // 
            // gridColumn27
            // 
            this.gridColumn27.Caption = "������";
            this.gridColumn27.FieldName = "Request";
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.Tag = "������#r";
            this.gridColumn27.Visible = true;
            this.gridColumn27.VisibleIndex = 11;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "�������";
            this.gridColumn17.FieldName = "Person.AppointmentOrderDate";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.Tag = "�������#r";
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 12;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "�������������";
            this.gridColumn18.FieldName = "Two";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.Tag = "�������������#r";
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 13;
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "���. � �/����";
            this.gridColumn20.FieldName = "Three";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.Tag = "���. � �.����#r";
            this.gridColumn20.Visible = true;
            this.gridColumn20.VisibleIndex = 14;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "��-����";
            this.gridColumn21.FieldName = "Four";
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.Tag = "��-����#r";
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 15;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "�������";
            this.gridColumn22.FieldName = "Five";
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.Tag = "�������#r";
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 16;
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "����";
            this.gridColumn23.FieldName = "Six";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.Tag = "����#r";
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 17;
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "������";
            this.gridColumn24.FieldName = "Seven";
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.Tag = "������#r";
            this.gridColumn24.Visible = true;
            this.gridColumn24.VisibleIndex = 18;
            // 
            // gridColumn25
            // 
            this.gridColumn25.Caption = "������";
            this.gridColumn25.FieldName = "HasLetters";
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.Tag = "������#r";
            this.gridColumn25.Visible = true;
            this.gridColumn25.VisibleIndex = 19;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "���� ������";
            this.gridColumn9.FieldName = "Person.DepartureDate";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.Tag = "���� ������#r";
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 20;
            // 
            // gridColumn26
            // 
            this.gridColumn26.Caption = "�������";
            this.gridColumn26.FieldName = "Notes";
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.Tag = "�������#r";
            this.gridColumn26.Visible = true;
            this.gridColumn26.VisibleIndex = 21;
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridViewSecond;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(727, 226);
            this.gridControl1.TabIndex = 17;
            this.gridControl1.Tag = "������� ���� �������� ����������� ��� ��� ������#r";
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewSecond});
            // 
            // gridViewSecond
            // 
            this.gridViewSecond.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn2,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn29,
            this.gridColumn30,
            this.gridColumn31,
            this.gridColumn32,
            this.gridColumn33,
            this.gridColumn34,
            this.gridColumn35,
            this.gridColumn36,
            this.gridColumn37,
            this.gridColumn38,
            this.gridColumn39,
            this.gridColumn40,
            this.gridColumn41,
            this.gridColumn42,
            this.gridColumn43});
            this.gridViewSecond.GridControl = this.gridControl1;
            this.gridViewSecond.Name = "gridViewSecond";
            this.gridViewSecond.OptionsBehavior.Editable = false;
            this.gridViewSecond.OptionsDetail.EnableMasterViewMode = false;
            this.gridViewSecond.OptionsView.ShowDetailButtons = false;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "������";
            this.gridColumn2.FieldName = "Person.Degree";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Tag = "������#r";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "���";
            this.gridColumn4.FieldName = "Person.Name";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Tag = "���#r";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 1;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "���������";
            this.gridColumn5.FieldName = "Person.Position";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Tag = "���������#r";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 2;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "�������������";
            this.gridColumn6.FieldName = "Debts";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Tag = "�������������#r";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 3;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "���������� �����";
            this.gridColumn10.FieldName = "Person.TripCount";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.Tag = "���������� �����#r";
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 4;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "���� ������ ��������";
            this.gridColumn11.FieldName = "Person.TeachingStartDate";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.Tag = "���� ������ ��������#r";
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 5;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "�������";
            this.gridColumn12.FieldName = "Person.Ship";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.Tag = "�������#r";
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 6;
            // 
            // gridColumn29
            // 
            this.gridColumn29.Caption = "�������";
            this.gridColumn29.FieldName = "Person.Ship.Condition";
            this.gridColumn29.Name = "gridColumn29";
            this.gridColumn29.Tag = "�������#r";
            this.gridColumn29.Visible = true;
            this.gridColumn29.VisibleIndex = 7;
            // 
            // gridColumn30
            // 
            this.gridColumn30.Caption = "��������";
            this.gridColumn30.FieldName = "ShipTool";
            this.gridColumn30.Name = "gridColumn30";
            this.gridColumn30.Tag = "��������#r";
            this.gridColumn30.Visible = true;
            this.gridColumn30.VisibleIndex = 8;
            // 
            // gridColumn31
            // 
            this.gridColumn31.Caption = "���� ��������";
            this.gridColumn31.FieldName = "Person.ExaminationDate";
            this.gridColumn31.Name = "gridColumn31";
            this.gridColumn31.Tag = "���� ��������#r";
            this.gridColumn31.Visible = true;
            this.gridColumn31.VisibleIndex = 9;
            // 
            // gridColumn32
            // 
            this.gridColumn32.Caption = "��������";
            this.gridColumn32.FieldName = "One";
            this.gridColumn32.Name = "gridColumn32";
            this.gridColumn32.Tag = "��������#r";
            this.gridColumn32.Visible = true;
            this.gridColumn32.VisibleIndex = 10;
            // 
            // gridColumn33
            // 
            this.gridColumn33.Caption = "������";
            this.gridColumn33.FieldName = "Request";
            this.gridColumn33.Name = "gridColumn33";
            this.gridColumn33.Tag = "������#r";
            this.gridColumn33.Visible = true;
            this.gridColumn33.VisibleIndex = 11;
            // 
            // gridColumn34
            // 
            this.gridColumn34.Caption = "�������";
            this.gridColumn34.FieldName = "Person.AppointmentOrderDate";
            this.gridColumn34.Name = "gridColumn34";
            this.gridColumn34.Tag = "�������#r";
            this.gridColumn34.Visible = true;
            this.gridColumn34.VisibleIndex = 12;
            // 
            // gridColumn35
            // 
            this.gridColumn35.Caption = "�������������";
            this.gridColumn35.FieldName = "Two";
            this.gridColumn35.Name = "gridColumn35";
            this.gridColumn35.Tag = "�������������#r";
            this.gridColumn35.Visible = true;
            this.gridColumn35.VisibleIndex = 13;
            // 
            // gridColumn36
            // 
            this.gridColumn36.Caption = "���. � �/����";
            this.gridColumn36.FieldName = "Three";
            this.gridColumn36.Name = "gridColumn36";
            this.gridColumn36.Tag = "���. � �.����#r";
            this.gridColumn36.Visible = true;
            this.gridColumn36.VisibleIndex = 14;
            // 
            // gridColumn37
            // 
            this.gridColumn37.Caption = "��-����";
            this.gridColumn37.FieldName = "Four";
            this.gridColumn37.Name = "gridColumn37";
            this.gridColumn37.Tag = "��-����#r";
            this.gridColumn37.Visible = true;
            this.gridColumn37.VisibleIndex = 15;
            // 
            // gridColumn38
            // 
            this.gridColumn38.Caption = "�������";
            this.gridColumn38.FieldName = "Five";
            this.gridColumn38.Name = "gridColumn38";
            this.gridColumn38.Tag = "�������#r";
            this.gridColumn38.Visible = true;
            this.gridColumn38.VisibleIndex = 16;
            // 
            // gridColumn39
            // 
            this.gridColumn39.Caption = "����";
            this.gridColumn39.FieldName = "Six";
            this.gridColumn39.Name = "gridColumn39";
            this.gridColumn39.Tag = "����#r";
            this.gridColumn39.Visible = true;
            this.gridColumn39.VisibleIndex = 17;
            // 
            // gridColumn40
            // 
            this.gridColumn40.Caption = "������";
            this.gridColumn40.FieldName = "Seven";
            this.gridColumn40.Name = "gridColumn40";
            this.gridColumn40.Tag = "������#r";
            this.gridColumn40.Visible = true;
            this.gridColumn40.VisibleIndex = 18;
            // 
            // gridColumn41
            // 
            this.gridColumn41.Caption = "������";
            this.gridColumn41.FieldName = "HasLetters";
            this.gridColumn41.Name = "gridColumn41";
            this.gridColumn41.Tag = "������#r";
            this.gridColumn41.Visible = true;
            this.gridColumn41.VisibleIndex = 19;
            // 
            // gridColumn42
            // 
            this.gridColumn42.Caption = "���� ������";
            this.gridColumn42.FieldName = "Person.DepartureDate";
            this.gridColumn42.Name = "gridColumn42";
            this.gridColumn42.Tag = "���� ������#r";
            this.gridColumn42.Visible = true;
            this.gridColumn42.VisibleIndex = 20;
            // 
            // gridColumn43
            // 
            this.gridColumn43.Caption = "�������";
            this.gridColumn43.FieldName = "Notes";
            this.gridColumn43.Name = "gridColumn43";
            this.gridColumn43.Tag = "�������#r";
            this.gridColumn43.Visible = true;
            this.gridColumn43.VisibleIndex = 21;
            // 
            // CrewForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(731, 574);
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this._editPersonPanelControl);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CrewForm";
            this.Tag = "������ ����������\\����������\\���� �������#r";
            this.Text = "���� �������";
            ((System.ComponentModel.ISupportInitialize)(this._editPersonPanelControl)).EndInit();
            this._editPersonPanelControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlFirst)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewFirst)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSecond)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl _editPersonPanelControl;
        private DevExpress.XtraEditors.SimpleButton _reportSimpleButton;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraGrid.GridControl gridControlFirst;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewFirst;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnShip;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraEditors.SimpleButton _refreshSimpleButton;
        private DevExpress.XtraEditors.SimpleButton _personelSimpleButton;
        private DevExpress.XtraEditors.SimpleButton simpleButtonToShips;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewSecond;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn30;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn31;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn32;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn33;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn34;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn35;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn36;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn37;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn38;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn39;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn40;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn41;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn42;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn43;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButtonToShips1;
        private DevExpress.XtraEditors.SimpleButton simpleButtonToPersonalFirst;
    }
}

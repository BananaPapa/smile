﻿using System;
using System.Threading;
using DevExpress.XtraEditors;

namespace Curriculum
{
    /// <summary>
    /// Represents simple marquee progress form with message text.
    /// </summary>
    public partial class MarqueeProgressForm : XtraForm
    {
        /// <summary>
        /// Encapsulates method that represents action to perform.
        /// </summary>
        private readonly Action<MarqueeProgressForm> _action;

        /// <summary>
        /// Action thread priority. 
        /// </summary>
        private readonly ThreadPriority _actionThreadPriority;

        /// <summary>
        /// Initializes new instance of marquee progress form.
        /// </summary>
        /// <param name="message">Message describing process.</param>
        /// <param name="action">Action to perform.</param>
        public MarqueeProgressForm(string message, Action<MarqueeProgressForm> action)
        {
            InitializeComponent();

            _action = action;
            _actionThreadPriority = ThreadPriority.Normal;

            Message = message;
        }

        /// <summary>
        /// Gets or sets progress message.
        /// </summary>
        public string Message
        {
            get { return GetMessage(); }
            set { SetMessage(value ?? String.Empty); }
        }

        /// <summary>
        /// Stops progress animation.
        /// </summary>
        public void StopAnimation()
        {
            if (InvokeRequired)
            {
                Invoke(new Action(StopAnimation));
            }
            else
            {
                _marqueeProgressBarControl.Properties.Stopped = true;
            }
        }

        /// <summary>
        /// Starts progress animation.
        /// </summary>
        public void StartAnimation()
        {
            if (InvokeRequired)
            {
                Invoke(new Action(StartAnimation));
            }
            else
            {
                _marqueeProgressBarControl.Properties.Stopped = false;
            }
        }

        /// <summary>
        /// Gets message.
        /// </summary>
        /// <returns></returns>
        private string GetMessage()
        {
            if (InvokeRequired)
            {
                return (string) Invoke(new Func<string>(GetMessage));
            }

            return _marqueeProgressBarControl.Text;
        }

        /// <summary>
        /// Sets message.
        /// </summary>
        /// <param name="message">Progress message.</param>
        private void SetMessage(string message)
        {
            if (InvokeRequired)
            {
                Invoke(new Action<string>(SetMessage), message);
            }
            else
            {
                _marqueeProgressBarControl.Text = message;
            }
        }

        /// <summary>
        /// Closes form.
        /// </summary>
        private void CloseForm()
        {
            if (InvokeRequired)
            {
                Invoke(new Action(CloseForm));
            }
            else
            {
                if (!IsDisposed)
                {
                    try
                    {
                        Close();
                    }
                    catch (InvalidOperationException)
                    {
                    }
                }
            }
        }

        /// <summary>
        /// Invokes action.
        /// </summary>
        private void InvokeAction()
        {
            if (_action != null)
            {
                _action.Invoke(this);
            }

            CloseForm();
        }

        /// <summary>
        /// Handles marquee progress form load event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void MarqueeProgressFormLoad(object sender, EventArgs e)
        {
            var actionThread = new Thread(InvokeAction) {Priority = _actionThreadPriority};
            actionThread.Start();
        }
    }
}
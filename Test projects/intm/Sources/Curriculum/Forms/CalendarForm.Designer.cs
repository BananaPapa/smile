using DevExpress.XtraBars;

namespace Curriculum
{
    partial class CalendarForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraScheduler.TimeRuler timeRuler1 = new DevExpress.XtraScheduler.TimeRuler();
            DevExpress.XtraScheduler.TimeRuler timeRuler2 = new DevExpress.XtraScheduler.TimeRuler();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CalendarForm));
            this._schedulerControl = new DevExpress.XtraScheduler.SchedulerControl();
            this._barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.navigatorBar1 = new DevExpress.XtraScheduler.UI.NavigatorBar();
            this._refreshBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.gotoTodayItem1 = new DevExpress.XtraScheduler.UI.GotoTodayItem();
            this.viewZoomInItem1 = new DevExpress.XtraScheduler.UI.ViewZoomInItem();
            this.viewZoomOutItem1 = new DevExpress.XtraScheduler.UI.ViewZoomOutItem();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this._barButtonItemAddEvent = new DevExpress.XtraBars.BarButtonItem();
            this._barButtonItemRemoveEvent = new DevExpress.XtraBars.BarButtonItem();
            this.activeViewBar1 = new DevExpress.XtraScheduler.UI.ActiveViewBar();
            this.switchToDayViewItem1 = new DevExpress.XtraScheduler.UI.SwitchToDayViewItem();
            this.switchToWorkWeekViewItem1 = new DevExpress.XtraScheduler.UI.SwitchToWorkWeekViewItem();
            this.switchToWeekViewItem1 = new DevExpress.XtraScheduler.UI.SwitchToWeekViewItem();
            this.switchToMonthViewItem1 = new DevExpress.XtraScheduler.UI.SwitchToMonthViewItem();
            this.switchToTimelineViewItem1 = new DevExpress.XtraScheduler.UI.SwitchToTimelineViewItem();
            this.switchToGanttViewItem1 = new DevExpress.XtraScheduler.UI.SwitchToGanttViewItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.newAppointmentItem1 = new DevExpress.XtraScheduler.UI.NewAppointmentItem();
            this.newRecurringAppointmentItem1 = new DevExpress.XtraScheduler.UI.NewRecurringAppointmentItem();
            this.groupByResourceItem1 = new DevExpress.XtraScheduler.UI.GroupByResourceItem();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this._schedulerStorage = new DevExpress.XtraScheduler.SchedulerStorage(this.components);
            this._toolTipController = new DevExpress.Utils.ToolTipController(this.components);
            this._veticalSplitContainerControl = new DevExpress.XtraEditors.SplitContainerControl();
            this._dateNavigator = new DevExpress.XtraScheduler.DateNavigator();
            this._personAppointmentContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._personsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._lessonsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.schedulerBarController1 = new DevExpress.XtraScheduler.UI.SchedulerBarController();
            ((System.ComponentModel.ISupportInitialize)(this._schedulerControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._schedulerStorage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._veticalSplitContainerControl)).BeginInit();
            this._veticalSplitContainerControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._dateNavigator)).BeginInit();
            this._personAppointmentContextMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerBarController1)).BeginInit();
            this.SuspendLayout();
            // 
            // _schedulerControl
            // 
            this._schedulerControl.ActiveViewType = DevExpress.XtraScheduler.SchedulerViewType.WorkWeek;
            this._schedulerControl.AllowDrop = false;
            this._schedulerControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._schedulerControl.Location = new System.Drawing.Point(0, 0);
            this._schedulerControl.MenuManager = this._barManager;
            this._schedulerControl.Name = "_schedulerControl";
            this._schedulerControl.OptionsBehavior.RemindersFormDefaultAction = DevExpress.XtraScheduler.RemindersFormDefaultAction.Custom;
            this._schedulerControl.OptionsCustomization.AllowAppointmentCopy = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this._schedulerControl.OptionsCustomization.AllowAppointmentCreate = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this._schedulerControl.OptionsCustomization.AllowAppointmentDelete = DevExpress.XtraScheduler.UsedAppointmentType.Custom;
            this._schedulerControl.OptionsCustomization.AllowAppointmentDrag = DevExpress.XtraScheduler.UsedAppointmentType.Custom;
            this._schedulerControl.OptionsCustomization.AllowAppointmentDragBetweenResources = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this._schedulerControl.OptionsCustomization.AllowAppointmentResize = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this._schedulerControl.OptionsCustomization.AllowInplaceEditor = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this._schedulerControl.OptionsRangeControl.RangeMaximum = new System.DateTime(2014, 8, 1, 0, 0, 0, 0);
            this._schedulerControl.OptionsRangeControl.RangeMinimum = new System.DateTime(2014, 5, 1, 0, 0, 0, 0);
            this._schedulerControl.OptionsView.ToolTipVisibility = DevExpress.XtraScheduler.ToolTipVisibility.Always;
            this._schedulerControl.Size = new System.Drawing.Size(1091, 752);
            this._schedulerControl.Start = new System.DateTime(2009, 9, 28, 0, 0, 0, 0);
            this._schedulerControl.Storage = this._schedulerStorage;
            this._schedulerControl.TabIndex = 0;
            this._schedulerControl.Tag = "���������#r";
            this._schedulerControl.Text = "schedulerControl1";
            this._schedulerControl.ToolTipController = this._toolTipController;
            this._schedulerControl.Views.DayView.ResourcesPerPage = 3;
            timeRuler1.TimeZone.Id = "Russian Standard Time";
            timeRuler1.UseClientTimeZone = false;
            this._schedulerControl.Views.DayView.TimeRulers.Add(timeRuler1);
            this._schedulerControl.Views.DayView.TimeScale = System.TimeSpan.Parse("01:00:00");
            this._schedulerControl.Views.WorkWeekView.ResourcesPerPage = 3;
            timeRuler2.TimeZone.Id = "Russian Standard Time";
            timeRuler2.UseClientTimeZone = false;
            this._schedulerControl.Views.WorkWeekView.TimeRulers.Add(timeRuler2);
            this._schedulerControl.AllowAppointmentDrag += new DevExpress.XtraScheduler.AppointmentOperationEventHandler(this.SchedulerControlAllowAppointmentEdit);
            this._schedulerControl.AllowAppointmentResize += new DevExpress.XtraScheduler.AppointmentOperationEventHandler(this.SchedulerControlAllowAppointmentEdit);
            this._schedulerControl.AllowAppointmentDelete += new DevExpress.XtraScheduler.AppointmentOperationEventHandler(this.SchedulerControlAllowAppointmentEdit);
            this._schedulerControl.AppointmentDrop += new DevExpress.XtraScheduler.AppointmentDragEventHandler(this.SchedulerControlAppointmentDrop);
            this._schedulerControl.AppointmentResized += new DevExpress.XtraScheduler.AppointmentResizeEventHandler(this.SchedulerControlAppointmentResized);
            this._schedulerControl.PopupMenuShowing += new DevExpress.XtraScheduler.PopupMenuShowingEventHandler(this.SchedulerControlPreparePopupMenu);
            this._schedulerControl.EditAppointmentFormShowing += new DevExpress.XtraScheduler.AppointmentFormEventHandler(this.SchedulerControlEditAppointmentFormShowing);
            this._schedulerControl.RemindersFormShowing += new DevExpress.XtraScheduler.RemindersFormEventHandler(this.SchedulerControlRemindersFormShowing);
            this._schedulerControl.MouseClick += new System.Windows.Forms.MouseEventHandler(this.SchedulerControlMouseClick);
            // 
            // _barManager
            // 
            this._barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.navigatorBar1,
            this.bar2,
            this.activeViewBar1});
            this._barManager.DockControls.Add(this.barDockControlTop);
            this._barManager.DockControls.Add(this.barDockControlBottom);
            this._barManager.DockControls.Add(this.barDockControlLeft);
            this._barManager.DockControls.Add(this.barDockControlRight);
            this._barManager.Form = this;
            this._barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.newAppointmentItem1,
            this.newRecurringAppointmentItem1,
            this.gotoTodayItem1,
            this.viewZoomInItem1,
            this.viewZoomOutItem1,
            this.switchToDayViewItem1,
            this.switchToWorkWeekViewItem1,
            this.switchToWeekViewItem1,
            this.switchToMonthViewItem1,
            this.groupByResourceItem1,
            this._refreshBarButtonItem,
            this._barButtonItemAddEvent,
            this._barButtonItemRemoveEvent,
            this.switchToTimelineViewItem1,
            this.switchToGanttViewItem1});
            this._barManager.MaxItemId = 42;
            this._barManager.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemSpinEdit1});
            // 
            // navigatorBar1
            // 
            this.navigatorBar1.Control = this._schedulerControl;
            this.navigatorBar1.DockCol = 0;
            this.navigatorBar1.DockRow = 0;
            this.navigatorBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.navigatorBar1.FloatLocation = new System.Drawing.Point(118, 163);
            this.navigatorBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this._refreshBarButtonItem),
            new DevExpress.XtraBars.LinkPersistInfo(this.gotoTodayItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.viewZoomInItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.viewZoomOutItem1)});
            this.navigatorBar1.OptionsBar.AllowQuickCustomization = false;
            this.navigatorBar1.OptionsBar.DrawDragBorder = false;
            // 
            // _refreshBarButtonItem
            // 
            this._refreshBarButtonItem.Caption = "��������";
            this._refreshBarButtonItem.Glyph = global::Curriculum.Properties.Resources.Refresh16;
            this._refreshBarButtonItem.Hint = "��� ������� �� ������, ���������� ���������� ���������.";
            this._refreshBarButtonItem.Id = 25;
            this._refreshBarButtonItem.Name = "_refreshBarButtonItem";
            this._refreshBarButtonItem.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this._refreshBarButtonItem.Tag = "���������#r";
            this._refreshBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.RefreshBarButtonItemItemClick);
            // 
            // gotoTodayItem1
            // 
            this.gotoTodayItem1.Id = 6;
            this.gotoTodayItem1.Name = "gotoTodayItem1";
            this.gotoTodayItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.gotoTodayItem1.Tag = "���������#r";
            // 
            // viewZoomInItem1
            // 
            this.viewZoomInItem1.Id = 4;
            this.viewZoomInItem1.Name = "viewZoomInItem1";
            this.viewZoomInItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.viewZoomInItem1.Tag = "���������#r";
            // 
            // viewZoomOutItem1
            // 
            this.viewZoomOutItem1.Id = 5;
            this.viewZoomOutItem1.Name = "viewZoomOutItem1";
            this.viewZoomOutItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.viewZoomOutItem1.Tag = "���������#r";
            // 
            // bar2
            // 
            this.bar2.BarName = "Custom 3";
            this.bar2.DockCol = 2;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this._barButtonItemAddEvent, true),
            new DevExpress.XtraBars.LinkPersistInfo(this._barButtonItemRemoveEvent)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.Text = "Custom 3";
            // 
            // _barButtonItemAddEvent
            // 
            this._barButtonItemAddEvent.Caption = "�������� �������";
            this._barButtonItemAddEvent.Glyph = global::Curriculum.Properties.Resources.Add16;
            this._barButtonItemAddEvent.Hint = "�������� ������� � ���������� �������� ������������.";
            this._barButtonItemAddEvent.Id = 28;
            this._barButtonItemAddEvent.Name = "_barButtonItemAddEvent";
            this._barButtonItemAddEvent.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this._barButtonItemAddEvent.Tag = "���������#ra";
            this._barButtonItemAddEvent.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BarButtonItemAddEventItemClick);
            // 
            // _barButtonItemRemoveEvent
            // 
            this._barButtonItemRemoveEvent.Caption = "������� �������";
            this._barButtonItemRemoveEvent.Glyph = global::Curriculum.Properties.Resources.Delete16;
            this._barButtonItemRemoveEvent.Hint = "������� ��������� ������� �� ����������� �������� ������������.";
            this._barButtonItemRemoveEvent.Id = 31;
            this._barButtonItemRemoveEvent.Name = "_barButtonItemRemoveEvent";
            this._barButtonItemRemoveEvent.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this._barButtonItemRemoveEvent.Tag = "���������#rd";
            this._barButtonItemRemoveEvent.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BarButtonItemRemoveEventItemClick);
            // 
            // activeViewBar1
            // 
            this.activeViewBar1.Control = this._schedulerControl;
            this.activeViewBar1.DockCol = 1;
            this.activeViewBar1.DockRow = 0;
            this.activeViewBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.activeViewBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.switchToDayViewItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.switchToWorkWeekViewItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.switchToWeekViewItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.switchToMonthViewItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.switchToTimelineViewItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.switchToGanttViewItem1)});
            this.activeViewBar1.Offset = 264;
            this.activeViewBar1.OptionsBar.AllowQuickCustomization = false;
            this.activeViewBar1.OptionsBar.DrawDragBorder = false;
            // 
            // switchToDayViewItem1
            // 
            this.switchToDayViewItem1.Id = 7;
            this.switchToDayViewItem1.Name = "switchToDayViewItem1";
            this.switchToDayViewItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.switchToDayViewItem1.Tag = "���������#r";
            // 
            // switchToWorkWeekViewItem1
            // 
            this.switchToWorkWeekViewItem1.Id = 8;
            this.switchToWorkWeekViewItem1.Name = "switchToWorkWeekViewItem1";
            this.switchToWorkWeekViewItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.switchToWorkWeekViewItem1.Tag = "���������#r";
            // 
            // switchToWeekViewItem1
            // 
            this.switchToWeekViewItem1.Id = 9;
            this.switchToWeekViewItem1.Name = "switchToWeekViewItem1";
            this.switchToWeekViewItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.switchToWeekViewItem1.Tag = "���������#r";
            // 
            // switchToMonthViewItem1
            // 
            this.switchToMonthViewItem1.Id = 10;
            this.switchToMonthViewItem1.Name = "switchToMonthViewItem1";
            this.switchToMonthViewItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.switchToMonthViewItem1.Tag = "���������#r";
            // 
            // switchToTimelineViewItem1
            // 
            this.switchToTimelineViewItem1.Id = 39;
            this.switchToTimelineViewItem1.Name = "switchToTimelineViewItem1";
            this.switchToTimelineViewItem1.Tag = "���������#r";
            // 
            // switchToGanttViewItem1
            // 
            this.switchToGanttViewItem1.Id = 40;
            this.switchToGanttViewItem1.Name = "switchToGanttViewItem1";
            this.switchToGanttViewItem1.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(1280, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 778);
            this.barDockControlBottom.Size = new System.Drawing.Size(1280, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 752);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1280, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 752);
            // 
            // newAppointmentItem1
            // 
            this.newAppointmentItem1.Id = 11;
            this.newAppointmentItem1.Name = "newAppointmentItem1";
            // 
            // newRecurringAppointmentItem1
            // 
            this.newRecurringAppointmentItem1.Id = 12;
            this.newRecurringAppointmentItem1.Name = "newRecurringAppointmentItem1";
            // 
            // groupByResourceItem1
            // 
            this.groupByResourceItem1.Id = 41;
            this.groupByResourceItem1.Name = "groupByResourceItem1";
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit1.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.repositoryItemSpinEdit1.MaxValue = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.MinValue = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // _schedulerStorage
            // 
            this._schedulerStorage.AppointmentDeleting += new DevExpress.XtraScheduler.PersistentObjectCancelEventHandler(this.SchedulerStorageAppointmentDeleting);
            // 
            // _toolTipController
            // 
            this._toolTipController.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(221)))), ((int)(((byte)(233)))));
            this._toolTipController.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(242)))), ((int)(((byte)(245)))));
            this._toolTipController.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(199)))), ((int)(((byte)(223)))));
            this._toolTipController.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(120)))), ((int)(((byte)(127)))));
            this._toolTipController.Appearance.Options.UseBackColor = true;
            this._toolTipController.Appearance.Options.UseBorderColor = true;
            this._toolTipController.Appearance.Options.UseFont = true;
            this._toolTipController.Appearance.Options.UseForeColor = true;
            this._toolTipController.AppearanceTitle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(221)))), ((int)(((byte)(233)))));
            this._toolTipController.AppearanceTitle.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(199)))), ((int)(((byte)(223)))));
            this._toolTipController.AppearanceTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(53)))), ((int)(((byte)(60)))));
            this._toolTipController.AppearanceTitle.Options.UseBackColor = true;
            this._toolTipController.AppearanceTitle.Options.UseBorderColor = true;
            this._toolTipController.AppearanceTitle.Options.UseFont = true;
            this._toolTipController.AppearanceTitle.Options.UseForeColor = true;
            this._toolTipController.AutoPopDelay = 60000;
            this._toolTipController.Rounded = true;
            this._toolTipController.ShowBeak = true;
            this._toolTipController.BeforeShow += new DevExpress.Utils.ToolTipControllerBeforeShowEventHandler(this.ToolTipControllerBeforeShow);
            // 
            // _veticalSplitContainerControl
            // 
            this._veticalSplitContainerControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._veticalSplitContainerControl.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this._veticalSplitContainerControl.Location = new System.Drawing.Point(0, 26);
            this._veticalSplitContainerControl.Name = "_veticalSplitContainerControl";
            this._veticalSplitContainerControl.Panel1.Controls.Add(this._schedulerControl);
            this._veticalSplitContainerControl.Panel1.Text = "Panel1";
            this._veticalSplitContainerControl.Panel2.Controls.Add(this._dateNavigator);
            this._veticalSplitContainerControl.Panel2.Text = "Panel2";
            this._veticalSplitContainerControl.Size = new System.Drawing.Size(1280, 752);
            this._veticalSplitContainerControl.SplitterPosition = 183;
            this._veticalSplitContainerControl.TabIndex = 1;
            this._veticalSplitContainerControl.Text = "splitContainerControl1";
            // 
            // _dateNavigator
            // 
            this._dateNavigator.Dock = System.Windows.Forms.DockStyle.Fill;
            this._dateNavigator.HotDate = null;
            this._dateNavigator.Location = new System.Drawing.Point(0, 0);
            this._dateNavigator.Name = "_dateNavigator";
            this._dateNavigator.SchedulerControl = this._schedulerControl;
            this._dateNavigator.Size = new System.Drawing.Size(183, 752);
            this._dateNavigator.TabIndex = 0;
            this._dateNavigator.Tag = "���������#r";
            // 
            // _personAppointmentContextMenuStrip
            // 
            this._personAppointmentContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._personsToolStripMenuItem,
            this._lessonsToolStripMenuItem});
            this._personAppointmentContextMenuStrip.Name = "contextMenuStrip1";
            this._personAppointmentContextMenuStrip.Size = new System.Drawing.Size(155, 48);
            // 
            // _personsToolStripMenuItem
            // 
            this._personsToolStripMenuItem.Image = global::Curriculum.Properties.Resources.User16;
            this._personsToolStripMenuItem.Name = "_personsToolStripMenuItem";
            this._personsToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this._personsToolStripMenuItem.Text = "��������";
            this._personsToolStripMenuItem.Click += new System.EventHandler(this.PersonelMenuItemClick);
            // 
            // _lessonsToolStripMenuItem
            // 
            this._lessonsToolStripMenuItem.Image = global::Curriculum.Properties.Resources.Lessons16;
            this._lessonsToolStripMenuItem.Name = "_lessonsToolStripMenuItem";
            this._lessonsToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this._lessonsToolStripMenuItem.Text = "������� ����";
            this._lessonsToolStripMenuItem.Click += new System.EventHandler(this.LessonsMenuItemClick);
            // 
            // schedulerBarController1
            // 
            this.schedulerBarController1.BarItems.Add(this.newAppointmentItem1);
            this.schedulerBarController1.BarItems.Add(this.newRecurringAppointmentItem1);
            this.schedulerBarController1.BarItems.Add(this.gotoTodayItem1);
            this.schedulerBarController1.BarItems.Add(this.viewZoomInItem1);
            this.schedulerBarController1.BarItems.Add(this.viewZoomOutItem1);
            this.schedulerBarController1.BarItems.Add(this.switchToDayViewItem1);
            this.schedulerBarController1.BarItems.Add(this.switchToWorkWeekViewItem1);
            this.schedulerBarController1.BarItems.Add(this.switchToWeekViewItem1);
            this.schedulerBarController1.BarItems.Add(this.switchToMonthViewItem1);
            this.schedulerBarController1.BarItems.Add(this.groupByResourceItem1);
            this.schedulerBarController1.BarItems.Add(this.switchToTimelineViewItem1);
            this.schedulerBarController1.BarItems.Add(this.switchToGanttViewItem1);
            this.schedulerBarController1.Control = this._schedulerControl;
            // 
            // CalendarForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1280, 778);
            this.Controls.Add(this._veticalSplitContainerControl);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CalendarForm";
            this.Tag = "������ ����������\\������� ����\\���������#r";
            this.Text = "���������";
            this.Deactivate += new System.EventHandler(this.CalendarFormDeactivate);
            this.Load += new System.EventHandler(this.CalendarFormLoad);
            ((System.ComponentModel.ISupportInitialize)(this._schedulerControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._schedulerStorage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._veticalSplitContainerControl)).EndInit();
            this._veticalSplitContainerControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._dateNavigator)).EndInit();
            this._personAppointmentContextMenuStrip.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.schedulerBarController1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraScheduler.SchedulerControl _schedulerControl;
        private DevExpress.XtraScheduler.SchedulerStorage _schedulerStorage;
        private DevExpress.XtraEditors.SplitContainerControl _veticalSplitContainerControl;
        private DevExpress.XtraScheduler.DateNavigator _dateNavigator;
        private DevExpress.Utils.ToolTipController _toolTipController;
        private System.Windows.Forms.ContextMenuStrip _personAppointmentContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem _personsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _lessonsToolStripMenuItem;
        private DevExpress.XtraScheduler.UI.SchedulerBarController schedulerBarController1;
        private BarManager _barManager;
        private DevExpress.XtraScheduler.UI.NavigatorBar navigatorBar1;
        private DevExpress.XtraScheduler.UI.ViewZoomInItem viewZoomInItem1;
        private DevExpress.XtraScheduler.UI.ViewZoomOutItem viewZoomOutItem1;
        private DevExpress.XtraScheduler.UI.GotoTodayItem gotoTodayItem1;
        private DevExpress.XtraScheduler.UI.SwitchToDayViewItem switchToDayViewItem1;
        private DevExpress.XtraScheduler.UI.SwitchToWorkWeekViewItem switchToWorkWeekViewItem1;
        private DevExpress.XtraScheduler.UI.SwitchToWeekViewItem switchToWeekViewItem1;
        private DevExpress.XtraScheduler.UI.SwitchToMonthViewItem switchToMonthViewItem1;
        private BarDockControl barDockControlTop;
        private BarDockControl barDockControlBottom;
        private BarDockControl barDockControlLeft;
        private BarDockControl barDockControlRight;
        private DevExpress.XtraScheduler.UI.NewAppointmentItem newAppointmentItem1;
        private DevExpress.XtraScheduler.UI.NewRecurringAppointmentItem newRecurringAppointmentItem1;
        private DevExpress.XtraScheduler.UI.GroupByResourceItem groupByResourceItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraBars.BarButtonItem _refreshBarButtonItem;
        private DevExpress.XtraBars.BarButtonItem _barButtonItemAddEvent;
        private DevExpress.XtraBars.BarButtonItem _barButtonItemRemoveEvent;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraScheduler.UI.ActiveViewBar activeViewBar1;
        private DevExpress.XtraScheduler.UI.SwitchToTimelineViewItem switchToTimelineViewItem1;
        private DevExpress.XtraScheduler.UI.SwitchToGanttViewItem switchToGanttViewItem1;
    }
}
namespace Curriculum
{
    partial class AuthForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AuthForm));
            this._credentialsLayoutControl = new DevExpress.XtraLayout.LayoutControl();
            this._passwordTextEdit = new DevExpress.XtraEditors.TextEdit();
            this._user�omboBoxEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this._layoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this._userLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this._passwordLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this._signInSimpleButton = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this._credentialsLayoutControl)).BeginInit();
            this._credentialsLayoutControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._passwordTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._user�omboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._layoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._userLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._passwordLayoutControlItem)).BeginInit();
            this.SuspendLayout();
            // 
            // _credentialsLayoutControl
            // 
            this._credentialsLayoutControl.Controls.Add(this._passwordTextEdit);
            this._credentialsLayoutControl.Controls.Add(this._user�omboBoxEdit);
            this._credentialsLayoutControl.Dock = System.Windows.Forms.DockStyle.Top;
            this._credentialsLayoutControl.Location = new System.Drawing.Point(0, 0);
            this._credentialsLayoutControl.Name = "_credentialsLayoutControl";
            this._credentialsLayoutControl.Root = this._layoutControlGroup;
            this._credentialsLayoutControl.Size = new System.Drawing.Size(294, 70);
            this._credentialsLayoutControl.TabIndex = 0;
            this._credentialsLayoutControl.Text = "�����������";
            // 
            // _passwordTextEdit
            // 
            this._passwordTextEdit.Location = new System.Drawing.Point(90, 36);
            this._passwordTextEdit.Name = "_passwordTextEdit";
            this._passwordTextEdit.Properties.PasswordChar = '*';
            this._passwordTextEdit.Size = new System.Drawing.Size(192, 20);
            this._passwordTextEdit.StyleController = this._credentialsLayoutControl;
            this._passwordTextEdit.TabIndex = 5;
            this._passwordTextEdit.EditValueChanged += new System.EventHandler(this.PasswordTextEdit_EditValueChanged);
            // 
            // _user�omboBoxEdit
            // 
            this._user�omboBoxEdit.Location = new System.Drawing.Point(90, 12);
            this._user�omboBoxEdit.Name = "_user�omboBoxEdit";
            this._user�omboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._user�omboBoxEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this._user�omboBoxEdit.Size = new System.Drawing.Size(192, 20);
            this._user�omboBoxEdit.StyleController = this._credentialsLayoutControl;
            this._user�omboBoxEdit.TabIndex = 4;
            this._user�omboBoxEdit.SelectedIndexChanged += new System.EventHandler(this.User�omboBoxEdit_SelectedIndexChanged);
            // 
            // _layoutControlGroup
            // 
            this._layoutControlGroup.CustomizationFormText = "layoutControlGroup1";
            this._layoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this._userLayoutControlItem,
            this._passwordLayoutControlItem});
            this._layoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this._layoutControlGroup.Name = "_layoutControlGroup";
            this._layoutControlGroup.OptionsItemText.TextToControlDistance = 6;
            this._layoutControlGroup.Size = new System.Drawing.Size(294, 70);
            this._layoutControlGroup.Text = "_layoutControlGroup";
            this._layoutControlGroup.TextVisible = false;
            // 
            // _userLayoutControlItem
            // 
            this._userLayoutControlItem.Control = this._user�omboBoxEdit;
            this._userLayoutControlItem.CustomizationFormText = "������������";
            this._userLayoutControlItem.Location = new System.Drawing.Point(0, 0);
            this._userLayoutControlItem.Name = "_userLayoutControlItem";
            this._userLayoutControlItem.Size = new System.Drawing.Size(274, 24);
            this._userLayoutControlItem.Text = "������������";
            this._userLayoutControlItem.TextSize = new System.Drawing.Size(72, 13);
            this._userLayoutControlItem.TextToControlDistance = 6;
            // 
            // _passwordLayoutControlItem
            // 
            this._passwordLayoutControlItem.Control = this._passwordTextEdit;
            this._passwordLayoutControlItem.CustomizationFormText = "������";
            this._passwordLayoutControlItem.Location = new System.Drawing.Point(0, 24);
            this._passwordLayoutControlItem.Name = "_passwordLayoutControlItem";
            this._passwordLayoutControlItem.Size = new System.Drawing.Size(274, 26);
            this._passwordLayoutControlItem.Text = "������";
            this._passwordLayoutControlItem.TextSize = new System.Drawing.Size(72, 13);
            this._passwordLayoutControlItem.TextToControlDistance = 6;
            // 
            // _signInSimpleButton
            // 
            this._signInSimpleButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this._signInSimpleButton.Enabled = false;
            this._signInSimpleButton.Location = new System.Drawing.Point(115, 76);
            this._signInSimpleButton.Name = "_signInSimpleButton";
            this._signInSimpleButton.Size = new System.Drawing.Size(64, 23);
            this._signInSimpleButton.TabIndex = 1;
            this._signInSimpleButton.Text = "�����";
            this._signInSimpleButton.Click += new System.EventHandler(this.SignInSimpleButton_Click);
            // 
            // AuthForm
            // 
            this.AcceptButton = this._signInSimpleButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(294, 104);
            this.Controls.Add(this._signInSimpleButton);
            this.Controls.Add(this._credentialsLayoutControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AuthForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "�����������";
            this.Load += new System.EventHandler(this.AuthForm_Load);
            this.Shown += new System.EventHandler(this.AuthForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this._credentialsLayoutControl)).EndInit();
            this._credentialsLayoutControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._passwordTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._user�omboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._layoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._userLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._passwordLayoutControlItem)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl _credentialsLayoutControl;
        private DevExpress.XtraEditors.TextEdit _passwordTextEdit;
        private DevExpress.XtraEditors.ComboBoxEdit _user�omboBoxEdit;
        private DevExpress.XtraLayout.LayoutControlGroup _layoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlItem _userLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem _passwordLayoutControlItem;
        private DevExpress.XtraEditors.SimpleButton _signInSimpleButton;
    }
}
﻿namespace Curriculum
{
    partial class TemplatesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TemplatesForm));
            this._editTemplateCoursesPanelControl = new DevExpress.XtraEditors.PanelControl();
            this._editTemplateCoursesSimpleButton = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this._editTemplateCoursesPanelControl)).BeginInit();
            this._editTemplateCoursesPanelControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // _editTemplateCoursesPanelControl
            // 
            this._editTemplateCoursesPanelControl.Controls.Add(this._editTemplateCoursesSimpleButton);
            this._editTemplateCoursesPanelControl.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._editTemplateCoursesPanelControl.Location = new System.Drawing.Point(0, 534);
            this._editTemplateCoursesPanelControl.Name = "_editTemplateCoursesPanelControl";
            this._editTemplateCoursesPanelControl.Size = new System.Drawing.Size(649, 30);
            this._editTemplateCoursesPanelControl.TabIndex = 6;
            // 
            // _editTemplateCoursesSimpleButton
            // 
            this._editTemplateCoursesSimpleButton.Dock = System.Windows.Forms.DockStyle.Right;
            this._editTemplateCoursesSimpleButton.Enabled = false;
            this._editTemplateCoursesSimpleButton.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this._editTemplateCoursesSimpleButton.Location = new System.Drawing.Point(537, 2);
            this._editTemplateCoursesSimpleButton.Name = "_editTemplateCoursesSimpleButton";
            this._editTemplateCoursesSimpleButton.Size = new System.Drawing.Size(110, 26);
            this._editTemplateCoursesSimpleButton.TabIndex = 2;
            this._editTemplateCoursesSimpleButton.Tag = "Шаблоны курсов#rc";
            this._editTemplateCoursesSimpleButton.Text = "Редактировать";
            this._editTemplateCoursesSimpleButton.ToolTip = "Изменить содержание шаблона курсов";
            this._editTemplateCoursesSimpleButton.Click += new System.EventHandler(this.EditTemplateCoursesSimpleButtonClick);
            // 
            // TemplatesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(649, 564);
            this.Controls.Add(this._editTemplateCoursesPanelControl);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "TemplatesForm";
            this.Tag = "Панель управления\\Учебный план\\Шаблоны курсов#r";
            this.Text = "Шаблоны курсов";
            this.Load += new System.EventHandler(this.TemplatesFormLoad);
            ((System.ComponentModel.ISupportInitialize)(this._editTemplateCoursesPanelControl)).EndInit();
            this._editTemplateCoursesPanelControl.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl _editTemplateCoursesPanelControl;
        private DevExpress.XtraEditors.SimpleButton _editTemplateCoursesSimpleButton;
    }
}
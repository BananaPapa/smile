using System;
using System.Collections.Generic;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using Curriculum.Reports;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using Eureca.Integrator.Common;
using File = System.IO.File;

namespace Curriculum
{
    /// <summary>
    /// Represent form for ships.
    /// </summary>
    public partial class ShipsForm : FormWithAccessAreas, IAliasesHolder
    {
        /// <summary>
        /// Report key.
        /// </summary>
        private const string ReportKey = "Ships";

        /// <summary>
        /// Persons report key.
        /// </summary>
        private const string ReportKeyPersons = "Persons";

        /// <summary>
        /// Previously focused ship.
        /// </summary>
        private Ship _previousShip;

        /// <summary>
        /// Whether displayed data is read-only.
        /// </summary>
        /// <see cref="ReadOnly"/>
        private bool _readOnly;

        /// <summary>
        /// Initializes new instance of ships form.
        /// </summary>
        public ShipsForm()
        {
            InitializeComponent();

            ApplyAliases();
        }

        /// <summary>
        /// Get or sets whether displayed data is read-only.
        /// </summary>
        private bool ReadOnly
        {
            get { return _readOnly; }
            set
            {
                var gridControls = new[]
                                       {
                                           _shipsGridControl,
                                           _shipToolsGridControl,
                                           _referencesGridControl,
                                           _phonesGridControl
                                       };

                foreach (GridControl control in gridControls)
                {
                    PermissionType permission = PermissionType.None ; 
                    if(!_permissionsDic.TryGetValue(control.Name, out permission))
                        permission = PermissionType.Add| PermissionType.Change | PermissionType.Delete | PermissionType.ReadOnly;
                        // Set readonly property of all in-place editors.
                    foreach (RepositoryItem item in control.RepositoryItems)
                    {
                        item.ReadOnly = value || !permission.HasFlag(PermissionType.Change);
                    }

                    // Enable/disable emdedded navigator edit buttons.
                    control.EmbeddedNavigator.Buttons.Edit.Enabled = !value && permission.HasFlag(PermissionType.Change);
                    control.EmbeddedNavigator.Buttons.Append.Enabled = !value && permission.HasFlag(PermissionType.Add);
                    control.EmbeddedNavigator.Buttons.Remove.Enabled = !value && permission.HasFlag(PermissionType.Delete);
                }

                PermissionType personPermission = PermissionType.None ; 
                if(!_permissionsDic.TryGetValue(_personsGridControl.Name, out personPermission))
                    personPermission = PermissionType.Add| PermissionType.Change | PermissionType.Delete | PermissionType.ReadOnly;

                // Notes RTF editors.
                _shipNotesRtfEditControl.ReadOnly =
                _responsibilityZoneNotesRtfEditControl.ReadOnly = value || !personPermission.HasFlag(PermissionType.Change);

                // Responsibility zone controls.
                _responsibilityZoneNTextEdit.Properties.ReadOnly =
                    _responsibilityZoneOYTextEdit.Properties.ReadOnly =
                    _responsibilityZoneNameTextEdit.Properties.ReadOnly =
                    _responsibilityZoneBuildingTextEdit.Properties.ReadOnly =
                    _responsibilityZonePorchTextEdit.Properties.ReadOnly =
                    _responsibilityZoneRoomTextEdit.Properties.ReadOnly = value || !personPermission.HasFlag( PermissionType.Change );
                _saveResponsibilityZoneSimpleButton.Enabled = !value && personPermission.HasFlag( PermissionType.Change );

                // Toggle edit lock button image.
                _shipsGridControl.EmbeddedNavigator.CustomButtons[0].ImageIndex = value || personPermission == PermissionType.ReadOnly ? 1 : 0;

                _readOnly = value;
            }
        }

        /// <summary>
        /// Gets or sets currently focused ship.
        /// </summary>
        public Ship FocusedShip
        {
            get { return _shipsLayoutView.GetFocusedRow() as Ship; }
            set
            {
                if (value != null && _shipsLayoutView.RowCount > 0)
                {
                    int rowHandle = FindShipRowHandle(value);

                    if (_shipsLayoutView.IsDataRow(rowHandle))
                    {
                        // Focus found row.
                        _shipsLayoutView.FocusedRowHandle = rowHandle;
                    }
                    else
                    {
                        // Clear filter, sorting and selection.
                        _shipsLayoutView.ClearColumnsFilter();
                        _shipsLayoutView.ClearSorting();
                        _shipsLayoutView.ClearSelection();

                        rowHandle = FindShipRowHandle(value);

                        if (_shipsLayoutView.IsDataRow(rowHandle))
                        {
                            // Focus found row.
                            _shipsLayoutView.FocusedRowHandle = rowHandle;
                        }
                    }
                }
            }
        }

        #region IAliasesHolder Members

        /// <summary>
        /// Applies aliases for the current form.
        /// </summary>
        public void ApplyAliases()
        {
            try
            {
                Aliase.ApplyAlias( "Ships", a => _shipsLayoutView.ViewCaption = a );
                Aliase.ApplyAlias( "Ships", a => Text = a );
                Aliase.ApplyAlias( "River", a => _shipRiverLayoutViewColumn.Caption = a );
                Aliase.ApplyAlias( "ShipLine", a => _shipLineLayoutViewColumn.Caption = a );
                Aliase.ApplyAlias( "N", a => _responsibilityZoneNLayoutControlItem.Text = a );
                Aliase.ApplyAlias( "OY", a => _responsibilityZoneOYLayoutControlItem.Text = a );
                Aliase.ApplyAlias( "Position", a => _personPositionGridColumn.Caption = a );
                Aliase.ApplyAlias( "Status", a => _personStatusGridColumn.Caption = a );
            }
            catch (DbException ex)
            {
                Program.HandleDbException(ex);
            }
        }

        #endregion

        /// <summary>
        /// Finds person's row handle.
        /// </summary>
        /// <param name="ship">Ship to find row for.</param>
        /// <returns>Row handle of found row or invalid row handle.</returns>
        private int FindShipRowHandle(Ship ship)
        {
            int firstRowHandle = _shipsLayoutView.GetRowHandle(0);

            // Search row by person id starting from first row.
            return _shipsLayoutView.LocateByValue(firstRowHandle, _shipIdLayoutViewColumn,
                                                  ship.ShipId);
        }

        /// <summary>
        /// Initializes ships grid control data source.
        /// </summary>
        private void InitShipsDataSource()
        {
            _shipsGridControl.DataSource = from ship in Program.DbSingletone.GetDataSource(typeof (Ship)).OfType<Ship>()
                                           orderby ship.Name
                                           select ship;
        }

        /// <summary>
        /// Refreshes data source.
        /// </summary>
        private void RefreshShipDataSource()
        {
            Program.DbSingletone.RefreshDataSource(typeof (Tool), typeof (ResponsibilityZonePhone), typeof (ResponsibilityZone),
                                         typeof (Reference), typeof (Person), typeof (Course), typeof (Ship));

            _shipsLayoutView.RefreshData();
        }

        /// <summary>
        /// Fills all combo box editors with corresponding values from database.
        /// </summary>
        private void FillComboBoxEditors()
        {
            try
            {
                NameOwnerTools.FillComboBox<River>(_shipRiverComboBox);
                NameOwnerTools.FillComboBox<Condition>(_shipConditionComboBox);
                NameOwnerTools.FillComboBox<ShipLine>(_shipLineComboBox);
                NameOwnerTools.FillComboBox<Tool>(_shipToolComboBox);
                NameOwnerTools.FillComboBox<ToolCategory>(_shipToolCategoryComboBox);
                NameOwnerTools.FillComboBox<ToolSubCategory>(_shipToolSubCategoryComboBox);
                NameOwnerTools.FillComboBox<PhoneType>(_phoneTypeComboBox);
            }
            catch (DbException ex)
            {
                Program.HandleDbException(ex);
            }
        }

        /// <summary>
        /// Updates content of controls placed on details panel corresponding to the currently focused ship.
        /// </summary>
        /// <param name="ship">Ship to fill its details.</param>
        private void UpdateShipDetails(Ship ship)
        {
            if (ship != null)
            {
                // Change grids' data source.
                _shipToolsGridControl.DataSource = from shipTool in Program.DbSingletone.ShipTools
                                                   where shipTool.ShipId == ship.ShipId
                                                   select shipTool;

                _referencesGridControl.DataSource = from reference in Program.DbSingletone.References
                                                    where reference.ShipId == ship.ShipId
                                                    select reference;

                _personsGridControl.DataSource = from person in Program.DbSingletone.Persons
                                                 where person.ShipId == ship.ShipId
                                                 select person;

                if (ship.ResponsibilityZone != null)
                {
                    // Fill responsibility zone details.
                    _responsibilityZoneNTextEdit.Text = ship.ResponsibilityZone.N;
                    _responsibilityZoneOYTextEdit.Text = ship.ResponsibilityZone.OY;
                    _responsibilityZoneNameTextEdit.Text = ship.ResponsibilityZone.Name;
                    _responsibilityZoneBuildingTextEdit.Text = ship.ResponsibilityZone.Building;
                    _responsibilityZonePorchTextEdit.Text = ship.ResponsibilityZone.Porch;
                    _responsibilityZoneRoomTextEdit.Text = ship.ResponsibilityZone.Room;
                    _responsibilityZoneNotesRtfEditControl.Rtf = ship.ResponsibilityZone.Notes;

                    _phonesGridControl.DataSource = from phone in Program.DbSingletone.ResponsibilityZonePhones
                                                    where
                                                        phone.ResponsibilityZoneId ==
                                                        ship.ResponsibilityZone.ResponsibilityZoneId
                                                    select phone;
                }
                else
                {
                    (new List<TextEdit>(new[]
                                            {
                                                _responsibilityZoneNTextEdit,
                                                _responsibilityZoneOYTextEdit,
                                                _responsibilityZoneNameTextEdit,
                                                _responsibilityZoneBuildingTextEdit,
                                                _responsibilityZonePorchTextEdit,
                                                _responsibilityZoneRoomTextEdit
                                            }
                        )).ForEach(t => t.Text = String.Empty);

                    _responsibilityZoneNotesRtfEditControl.Rtf = null;

                    _phonesGridControl.DataSource = null;
                }

                // Select first person row.
                if (_personsGridView.RowCount > 0)
                {
                    _personsGridView.FocusedRowHandle = _personsGridView.GetRowHandle(0);
                }

                // Set ship notes RTF.
                _shipNotesRtfEditControl.Rtf = ship.Notes;

                // Hide lessons control with label.
                _showCourcesLinkLabel.BringToFront();

                // Show details if they are hidden.
                if (_shipsSplitContainerControl.PanelVisibility != SplitPanelVisibility.Both)
                {
                    _shipsSplitContainerControl.PanelVisibility = SplitPanelVisibility.Both;
                }
            }
            else
            {
                _shipToolsGridControl.DataSource =
                    _referencesGridControl.DataSource =
                    _personsGridControl.DataSource = null;

                _responsibilityZoneNTextEdit.Text =
                    _responsibilityZoneOYTextEdit.Text =
                    _responsibilityZoneNameTextEdit.Text =
                    _responsibilityZoneBuildingTextEdit.Text =
                    _responsibilityZonePorchTextEdit.Text =
                    _responsibilityZoneRoomTextEdit.Text = String.Empty;

                _phonesGridControl.DataSource = null;
                _responsibilityZoneNotesRtfEditControl.Rtf = String.Empty;

                _shipNotesRtfEditControl.Rtf = String.Empty;

                _coursesControl.Ship = null;
                _showCourcesLinkLabel.BringToFront();

                // Hide details if they are shown.
                if (_shipsSplitContainerControl.PanelVisibility != SplitPanelVisibility.Panel1)
                {
                    _shipsSplitContainerControl.PanelVisibility = SplitPanelVisibility.Panel1;
                }
            }
        }

        /// <summary>
        /// Offers to save details changes.
        /// </summary>
        /// <param name="ship">Ship to save details for.</param>
        private void OfferToSaveShipDetailsChanges(Ship ship)
        {
            if (ship != null)
            {
                // Check if ship notes was modified but not saved.
                if (_shipNotesRtfEditControl.Modified)
                {
                    if (XtraMessageBox.Show(
                        "� ������� ���� ������� ���������." + Environment.NewLine +
                        "��������� ���������?", "�������������", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                        == DialogResult.Yes)
                    {
                        SaveShipNotes(ship);
                    }
                    else
                    {
                        _shipNotesRtfEditControl.Rtf = ship.Notes;
                    }
                }

                ResponsibilityZone zone = ship.ResponsibilityZone;

                if (zone != null)
                {
                    // Check if responsibility zone details was modified but not saved.
                    string n = _responsibilityZoneNTextEdit.Text;
                    string oy = _responsibilityZoneOYTextEdit.Text;
                    string name = _responsibilityZoneNameTextEdit.Text;
                    string building = _responsibilityZoneBuildingTextEdit.Text;
                    string porch = _responsibilityZonePorchTextEdit.Text;
                    string room = _responsibilityZoneRoomTextEdit.Text;

                    if (
                        n != zone.N && !(String.IsNullOrEmpty(n) && String.IsNullOrEmpty(zone.N)) ||
                        oy != zone.OY && !(String.IsNullOrEmpty(oy) && String.IsNullOrEmpty(zone.OY)) ||
                        name != zone.Name && !(String.IsNullOrEmpty(name) && String.IsNullOrEmpty(zone.Name)) ||
                        building != zone.Building &&
                        !(String.IsNullOrEmpty(building) && String.IsNullOrEmpty(zone.Building)) ||
                        porch != zone.Porch && !(String.IsNullOrEmpty(porch) && String.IsNullOrEmpty(zone.Porch)) ||
                        room != zone.Room && !(String.IsNullOrEmpty(room) && String.IsNullOrEmpty(zone.Room))
                        )
                    {
                        if (XtraMessageBox.Show(
                            "� ���� �������������� ���� ������� ���������." + Environment.NewLine +
                            "��������� ���������?", "�������������", MessageBoxButtons.YesNo,
                            MessageBoxIcon.Question)
                            == DialogResult.Yes)
                        {
                            SaveResponsibilityZoneDetails(ship);
                        }
                    }

                    // Check if responsibility zone notes was modified but not saved.
                    if (_responsibilityZoneNotesRtfEditControl.Modified)
                    {
                        if (XtraMessageBox.Show(
                            "� ������� � ���� �������������� ���� ������� ���������." + Environment.NewLine +
                            "��������� ���������?", "�������������", MessageBoxButtons.YesNo,
                            MessageBoxIcon.Question)
                            == DialogResult.Yes)
                        {
                            SaveResponsibilityZoneNotes(zone);
                        }
                        else
                        {
                            _responsibilityZoneNotesRtfEditControl.Rtf = zone.Notes;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Saves notes for the specified ship.
        /// </summary>
        /// <param name="ship">Ship to save notes for.</param>
        private void SaveShipNotes(Ship ship)
        {
            if (ship != null)
            {
                ship.Notes = _shipNotesRtfEditControl.Rtf;

                try
                {
                    Program.DbSingletone.SubmitChanges();

                    _shipNotesRtfEditControl.ResetModifiedProperty();
                }
                catch (DbException ex)
                {
                    Program.HandleDbException(ex);
                }
            }
        }

        /// <summary>
        /// Saves notes for the specified responsibility zone.
        /// </summary>
        /// <param name="responsibilityZone">Responsibility zone to save notes for.</param>
        private void SaveResponsibilityZoneNotes(ResponsibilityZone responsibilityZone)
        {
            if (responsibilityZone != null)
            {
                responsibilityZone.Notes = _responsibilityZoneNotesRtfEditControl.Rtf;

                try
                {
                    Program.DbSingletone.SubmitChanges();

                    _responsibilityZoneNotesRtfEditControl.ResetModifiedProperty();
                }
                catch (DbException ex)
                {
                    Program.HandleDbException(ex);
                }
            }
        }

        /// <summary>
        /// Saves details for the specified responsibility zone.
        /// </summary>
        /// <param name="responsibilityZone">Responsibility zone to save details for.</param>
        /// <param name="ship">Responsibility zone owner.</param>
        private void SaveResponsibilityZoneDetails(Ship ship)
        {
            if (ship.ResponsibilityZone == null)
            {
                ship.ResponsibilityZone = new ResponsibilityZone();
            }

            ship.ResponsibilityZone.N = _responsibilityZoneNTextEdit.Text;
            ship.ResponsibilityZone.OY = _responsibilityZoneOYTextEdit.Text;
            ship.ResponsibilityZone.Name = _responsibilityZoneNameTextEdit.Text;
            ship.ResponsibilityZone.Building = _responsibilityZoneBuildingTextEdit.Text;
            ship.ResponsibilityZone.Porch = _responsibilityZonePorchTextEdit.Text;
            ship.ResponsibilityZone.Room = _responsibilityZoneRoomTextEdit.Text;

            UpdateShipDetails( ship );

            try
            {
                Program.DbSingletone.SubmitChanges();

                XtraMessageBox.Show(this, "��������� ���������!", String.Empty, MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
            }
            catch (DbException ex)
            {
                Program.HandleDbException(ex);
            }
        }

        /// <summary>
        /// Handles form load event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void ShipsForm_Load(object sender, EventArgs e)
        {
            // Data is not initially editable.
            ReadOnly = true;
            FillComboBoxEditors();
            InitShipsDataSource();

            // Bind save notes save buttons to modified property of RTF edit control.
            _saveShipNotesSimpleButton.DataBindings.Add("Enabled", _shipNotesRtfEditControl, "Modified");
            _saveResponsibilityZoneNotesSimpleButton.DataBindings.Add("Enabled", _responsibilityZoneNotesRtfEditControl,
                                                                      "Modified");
        }

        /// <summary>
        /// Handles save ship notes button click event
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void SaveShipNotesSimpleButton_Click(object sender, EventArgs e)
        {
            SaveShipNotes(FocusedShip);
        }

        /// <summary>
        /// Handles save responsibility zone button click event
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void SaveResponsibilityZoneSimpleButton_Click(object sender, EventArgs e)
        {
            SaveResponsibilityZoneDetails(FocusedShip);
        }

        /// <summary>
        /// Handles save responsibility zone notes button click event
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void SaveResponsibilityZoneNotesSimpleButton_Click(object sender, EventArgs e)
        {
            SaveResponsibilityZoneNotes(FocusedShip.ResponsibilityZone);
        }

        /// <summary>
        /// Handles edit person simple button click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void EditPersonSimpleButton_Click(object sender, EventArgs e)
        {
            // Check if the parent form is main form.
            if (ParentForm != null && ParentForm is StartForm)
            {
                var person = _personsGridView.GetFocusedRow() as Person;

                // Chick if there is a person to edit.
                if (person != null)
                {
                    // Open or activate persons form and focus desired person.
                    ((PersonsForm)
                     ((StartForm) ParentForm).OpenOrActivateMdiChild(typeof (PersonsForm), () => new PersonsForm())).
                        FocusedPerson = person;
                }
            }
        }

        /// <summary>
        /// Handles edit courses simple button click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void EditCoursesSimpleButton_Click(object sender, EventArgs e)
        {
            // Check if the parent form is main form.
            if (ParentForm != null && ParentForm is StartForm)
            {
                // Save current ship changes.
                _shipsLayoutView.UpdateCurrentRow();

                Ship ship = FocusedShip;

                if (ship != null)
                {
                    // Open or activate courses form for desired ship.
                    ((StartForm) ParentForm).OpenOrActivateMdiChild(typeof (CoursesForm),
                                                                   () => new CoursesForm(ship){HierarchyParent = this},
                                                                   args: ship);
                }
            }
            //if (_coursesControl != null)
            //    _coursesControl.RefreshCoursesView( );

        }

        /// <summary>
        /// Handles show lessons link lable link click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void ShowCourcesLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Ship ship = FocusedShip;

            if (ship != null)
            {
                _coursesControl.Ship = ship;
                _coursesControl.BringToFront();
            }
        }

        /// <summary>
        /// Handles produce report bar button item click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void ProduceReportBarButtonItemItemClick(object sender, ItemClickEventArgs e)
        {
            ProduceReport(false);
        }

        private void ProduceReportWithPersonsBarButtonItemItemClick(object sender, ItemClickEventArgs e)
        {
            ProduceReport(true);
        }


        /// <summary>
        /// Produces common report of ships.
        /// </summary>
        private void ProduceReport(bool includePersons)
        {
            var ships = new List<Ship>();

            for (int i = 0; i < _shipsLayoutView.RowCount; ++i)
            {
                var ship = _shipsLayoutView.GetRow(i) as Ship;

                if (ship != null)
                {
                    ships.Add(ship);
                }
            }

            // Create report.
            try
            {
                string reportPath = Reporter.GetTemplateDocument(ReportKey, this);
                if (!string.IsNullOrEmpty(reportPath))
                {
                    var wordManager = new WordManager();
                    var personsWordManager = new WordManager();

                    try
                    {
                        string reportPathWithPersons = string.Empty;
                        if (includePersons)
                        {
                            reportPathWithPersons = Reporter.GetTemplateDocument(ReportKeyPersons, this);
                        }

                        new MarqueeProgressForm(
                            "������������ ������",
                            progressForm =>
                                {
                                    if (includePersons)
                                    {
                                        if (string.IsNullOrEmpty(reportPathWithPersons))
                                        {
                                            return;
                                        }

                                        personsWordManager.Open(reportPathWithPersons);
                                        Thread.Sleep(1000);
                                        personsWordManager.CopyTable(1);
                                        Thread.Sleep(1000);
                                        personsWordManager.KillWinWord();
                                        Thread.Sleep(2000);
                                    }

                                    wordManager.Open(reportPath);
                                    Thread.Sleep(2000);

                                    // Fill ships table.
                                    if (includePersons)
                                    {
                                        wordManager.CreateShipsPersonsTable(progressForm, ships);
                                    }
                                    else
                                    {
                                        wordManager.CreateShipsTable(progressForm, ships);
                                    }


                                    wordManager.SaveAs(reportPath);
                                }).ShowDialog();
                    }
                    finally
                    {
                        wordManager.KillWinWord();
                        personsWordManager.KillWinWord();
                    }


                    Application.DoEvents();

                    // Save and open template.

                    Reporter.SaveAndOpenReport(fileName =>
                                                   {
                                                       try
                                                       {
                                                           if (System.IO.File.Exists(fileName))
                                                               System.IO.File.Delete( fileName );
                                                           System.IO.File.Move( reportPath, fileName );
                                                           return null;
                                                       }
                                                       catch (Exception ex)
                                                       {
                                                           return ex;
                                                       }
                                                   });
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "����������");
            }
        }

        #region Focused Row Changed Event

        /// <summary>
        /// Handles ships layout view focused row changed event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void ShipsLayoutView_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            Ship ship = FocusedShip;

            string errorText;
            if (_previousShip != null && !ValidateShip(_previousShip, out errorText))
            {
                //If _previous responsibility zone is not validated then discard data context changes.
                Program.DiscardDataChanges();
            }

            if (_previousShip != null && !_previousShip.Equals(ship))
            {
                OfferToSaveShipDetailsChanges(_previousShip);
            }

            UpdateShipDetails(ship);

            _previousShip = ship;
        }

        /// <summary>
        /// Handles persons grid view focused row changed event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void PersonsGridView_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            // Enable / disable edit person button.
            _editPersonSimpleButton.Enabled = _personsGridView.GetRow(e.FocusedRowHandle) as Person != null;
        }

        #endregion

        #region Init New Row Events

        /// <summary>
        /// Handles ship tools grid view focused row changed event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void ShipToolsGridView_InitNewRow(object sender, InitNewRowEventArgs e)
        {
            var shipTool = _shipToolsGridView.GetRow(e.RowHandle) as ShipTool;

            if (shipTool != null)
            {
                // Link the ship tool being added to the currently focused ship.
                shipTool.Ship = FocusedShip;
            }
        }

        /// <summary>
        /// Handles references grid view focused row changed event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void ReferencesGridView_InitNewRow(object sender, InitNewRowEventArgs e)
        {
            var reference = _referencesGridView.GetRow(e.RowHandle) as Reference;

            if (reference != null)
            {
                // Link the reference being added to the currently focused ship.
                reference.Ship = FocusedShip;
            }
        }

        /// <summary>
        /// Handles phones grid view focused row changed event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void PhonesGridView_InitNewRow(object sender, InitNewRowEventArgs e)
        {
            var phone = _phonesGridView.GetRow(e.RowHandle) as ResponsibilityZonePhone;

            if (phone != null)
            {
                // Link phone being added to the currently focused responsibility zone.
                phone.ResponsibilityZone = FocusedShip.ResponsibilityZone;
            }
        }

        #endregion

        #region Validation Events

        /// <summary>
        /// Checks whether ship name is available.
        /// </summary>
        /// <param name="ship">Ship to check it's name.</param>
        /// <param name="errorText">Error text if name is not available.</param>
        /// <returns>true if available; otherwise, false.</returns>
        private static bool ShipNameIsAvailable(Ship ship, out string errorText, string newName = null)
        {
            if (ship != null && Program.DbSingletone.Ships.Any(s => s.Name == (newName ?? ship.Name) && s.ShipId != ship.ShipId))
            {
                errorText = "����� �������� ��� ������������.";
                return false;
            }

            errorText = null;
            return true;
        }

        /// <summary>
        /// Checks whether ship index is available.
        /// </summary>
        /// <param name="ship">Ship to check it's index.</param>
        /// <param name="errorText">Error text if index is not available.</param>
        /// <returns>true if available; otherwise, false.</returns>
        private static bool ShipIndexIsAvailable(Ship ship, out string errorText,string newIndex = null)
        {
            if (ship != null && !String.IsNullOrEmpty(ship.Index) &&
                Program.DbSingletone.Ships.Any(s => s.Index == (newIndex ?? ship.Index) && s.ShipId != ship.ShipId))
            {
                errorText = "����� ������ ��� ������������.";
                return false;
            }

            errorText = null;
            return true;
        }

        #region Editor Level

        /// <summary>
        /// Handles ships layout view validating editor event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void ShipsLayoutView_ValidatingEditor(object sender, BaseContainerValidateEditorEventArgs e)
        {
            var text = e.Value as string;
            if (text == null) return;

            // Trim string.
            text = text.Trim();

            if (text.Length == 0)
            {
                // If string is empty set value is null.
                e.Value = null;
            }
            else
            {
                // Get field name.
                string fieldName;
                try
                {
                    fieldName = _shipsLayoutView.FocusedColumn.FieldName;
                }
                catch (NullReferenceException)
                {
                    return;
                }

                try
                {
                    string errorText;

                    switch (fieldName)
                    {
                            // Check Unique fields.

                        case "Name":

                            if (!ShipNameIsAvailable(FocusedShip, out errorText,(string)e.Value))
                            {
                                e.Valid = false;
                                e.ErrorText = errorText;
                            }

                            break;

                        case "Index":

                            if (!ShipIndexIsAvailable( FocusedShip, out errorText, (string)e.Value ))
                            {
                                e.Valid = false;
                                e.ErrorText = errorText;
                            }

                            break;

                        case "River":
                            try
                            {
                                e.Value = Program.DbSingletone.Rivers.Single(r => r.Name == text);
                            }
                            catch (InvalidOperationException)
                            {
                                e.Value = new River {Name = text};
                            }
                            break;

                        case "ShipLine":
                            try
                            {
                                e.Value = Program.DbSingletone.ShipLines.Single(s => s.Name == text);
                            }
                            catch (InvalidOperationException)
                            {
                                e.Value = new ShipLine {Name = text};
                            }
                            break;

                        case "Condition":
                            try
                            {
                                e.Value = Program.DbSingletone.Conditions.Single(c => c.Name == text);
                            }
                            catch (InvalidOperationException)
                            {
                                e.Value = new Condition {Name = text};
                            }
                            break;
                    }
                }
                catch (DbException ex)
                {
                    Program.HandleDbException(ex);
                }
            }
        }

        /// <summary>
        /// Handles ship tools grid view validating editor event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void ShipToolsGridView_ValidatingEditor(object sender, BaseContainerValidateEditorEventArgs e)
        {
            var text = e.Value as string;

            // If value is not string it was selected from combo box.
            if (text == null) return;

            // Trim string.
            text = text.Trim();

            if (text.Length == 0)
            {
                // If string is empty set value is null.
                e.Value = null;
            }
            else
            {
                // Get field name.
                string fieldName;
                try
                {
                    fieldName = _shipToolsGridView.FocusedColumn.FieldName;
                }
                catch (NullReferenceException)
                {
                    return;
                }

                try
                {
                    // Get name owner objects by name or create new instances.
                    switch (fieldName)
                    {
                        case "Tool":
                            try
                            {
                                e.Value = Program.DbSingletone.Tools.Single(s => s.Name == text);
                            }
                            catch (InvalidOperationException)
                            {
                                e.Value = new Tool {Name = text};
                            }
                            break;

                        case "ToolCategory":
                            try
                            {
                                e.Value = Program.DbSingletone.ToolCategories.Single(s => s.Name == text);
                            }
                            catch (InvalidOperationException)
                            {
                                e.Value = new ToolCategory {Name = text};
                            }
                            break;

                        case "ToolSubCategory":
                            try
                            {
                                e.Value = Program.DbSingletone.ToolSubCategories.Single(s => s.Name == text);
                            }
                            catch (InvalidOperationException)
                            {
                                e.Value = new ToolSubCategory {Name = text};
                            }
                            break;
                    }
                }
                catch (DbException ex)
                {
                    Program.HandleDbException(ex);
                }
            }
        }

        /// <summary>
        /// Handles phones grid view validating editor event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void PhonesGridView_ValidatingEditor(object sender, BaseContainerValidateEditorEventArgs e)
        {
            // Only string value typed be user is to be validated.
            var text = e.Value as string;
            if (text == null) return;

            // Get field name.
            string fieldName;
            try
            {
                fieldName = _phonesGridView.FocusedColumn.FieldName;
            }
            catch (NullReferenceException)
            {
                return;
            }

            if (text.Length == 0)
            {
                e.Value = null;
            }
            else
            {
                try
                {
                    switch (fieldName)
                    {
                            // Get name owner objects by name or create new instances.
                        case "PhoneType":
                            try
                            {
                                e.Value = Program.DbSingletone.PhoneTypes.Single(pt => pt.Name == text);
                            }
                            catch (InvalidOperationException)
                            {
                                e.Value = new PhoneType {Name = text};
                            }
                            break;
                    }
                }
                catch (DbException ex)
                {
                    Program.HandleDbException(ex);
                }
            }
        }

        #endregion

        #region Row Level

        /// <summary>
        /// Handles ships layout view validate row event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void ShipsLayoutView_ValidateRow(object sender, ValidateRowEventArgs e)
        {
            var ship = e.Row as Ship;
            if (ship == null) return;

            try
            {
                string errorText;

                // Validate ship attributes.
                if (String.IsNullOrEmpty(ship.Name))
                {
                    e.ErrorText = "�������� �� ������ ���� ������.";
                }
                else if (!ShipNameIsAvailable(ship, out errorText))
                {
                    e.ErrorText = errorText;
                }
                else if (!ShipIndexIsAvailable(ship, out errorText))
                {
                    e.ErrorText = errorText;
                }
            }
            catch (DbException ex)
            {
                Program.HandleDbException(ex);
            }

            // If there is an error show message.
            if (!String.IsNullOrEmpty(e.ErrorText))
            {
                e.ErrorText += Environment.NewLine;
                e.Valid = false;
            }
        }

        /// <summary>
        /// Handles ship tools grid view validate row event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void ShipToolsGridView_ValidateRow(object sender, ValidateRowEventArgs e)
        {
            var shipTool = e.Row as ShipTool;
            if (shipTool == null) return;

            try
            {
                Ship ship = FocusedShip;
                if (ship == null)
                {
                    e.ErrorText = "�������� �� ����� ���� ���������.";
                }
                else
                {
                    // Validate ship tool attributes.
                    if (shipTool.Tool == null)
                    {
                        e.ErrorText = "������� ��� �������� ��������.";
                    }
                    else if (
                        Program.DbSingletone.ShipTools.Any(
                            st =>
                            st.Tool.Equals(shipTool.Tool) && st.ShipId == ship.ShipId &&
                            st.ShipToolId != shipTool.ShipToolId))
                    {
                        e.ErrorText = "����� �������� ��� ���������.";
                    }
                    else if (shipTool.ToolCategory == null)
                    {
                        e.ErrorText = "������� ��� �������� ���������.";
                    }
                }
            }
            catch (DbException ex)
            {
                Program.HandleDbException(ex);
            }

            // If there is an error show message.
            if (!String.IsNullOrEmpty(e.ErrorText))
            {
                e.ErrorText += Environment.NewLine;
                e.Valid = false;
            }
        }

        /// <summary>
        /// Handles references grid view validate row event..
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void ReferencesGridView_ValidateRow(object sender, ValidateRowEventArgs e)
        {
            var reference = e.Row as Reference;
            if (reference == null) return;

            try
            {
                Ship ship = FocusedShip;
                if (ship == null)
                {
                    e.ErrorText = "������� �� ����� ���� ���������.";
                }
                else
                {
                    // Validate reference attributes.
                    if (String.IsNullOrEmpty(reference.Item))
                    {
                        e.ErrorText = "������� �������.";
                    }
                    else if (
                        Program.DbSingletone.References.Any(
                            r =>
                            r.Item == reference.Item && r.ShipId == ship.ShipId &&
                            r.ReferenceId != reference.ReferenceId))
                    {
                        e.ErrorText = "����� ������� ��� ��������.";
                    }
                    else if (String.IsNullOrEmpty(reference.Value))
                    {
                        e.ErrorText = "������� ��������.";
                    }
                }
            }
            catch (DbException ex)
            {
                Program.HandleDbException(ex);
            }

            // If there is an error show message.
            if (!String.IsNullOrEmpty(e.ErrorText))
            {
                e.Valid = false;
                e.ErrorText += Environment.NewLine;
            }
        }

        /// <summary>
        /// Handles phones grid view validate row event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void PhonesGridView_ValidateRow(object sender, ValidateRowEventArgs e)
        {
            var phone = e.Row as ResponsibilityZonePhone;
            if (phone == null) return;

            if (String.IsNullOrEmpty(phone.Number))
            {
                e.ErrorText = "����� �� ������ ���� ������.";
            }

            // If there is an error show message.
            if (!String.IsNullOrEmpty(e.ErrorText))
            {
                e.ErrorText += Environment.NewLine;
                e.Valid = false;
            }
        }

        #endregion

        #endregion

        #region Row Update Events

        /// <summary>
        /// Handles ships layout view row updated event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void ShipsLayoutView_RowUpdated(object sender, RowObjectEventArgs e)
        {
            var ship = e.Row as Ship;

            if (ship != null)
            {
                try
                {
                    // Submit changes.
                    Program.DbSingletone.SubmitChanges();

                    // And update combo boxes with rivers, ship lines and conditions if new items were added.
                    NameOwnerTools.UpdateComboBox(ship.River, _shipRiverComboBox);
                    NameOwnerTools.UpdateComboBox(ship.ShipLine, _shipLineComboBox);
                    NameOwnerTools.UpdateComboBox(ship.Condition, _shipConditionComboBox);

                    UpdateShipDetails(ship);
                }
                catch (DbException ex)
                {
                    Program.HandleDbException(ex);
                }
            }
        }

        /// <summary>
        /// Handles ship tools grid view row updated event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void ShipToolsGridView_RowUpdated(object sender, RowObjectEventArgs e)
        {
            var shipTool = e.Row as ShipTool;

            if (shipTool != null)
            {
                try
                {
                    // Submit changes.
                    Program.DbSingletone.SubmitChanges();

                    // And update combo boxes with tools, tool categories and subcategories if new items were added.
                    NameOwnerTools.UpdateComboBox(shipTool.Tool, _shipToolComboBox);
                    NameOwnerTools.UpdateComboBox(shipTool.ToolCategory, _shipToolCategoryComboBox);
                    NameOwnerTools.UpdateComboBox(shipTool.ToolSubCategory, _shipToolSubCategoryComboBox);
                }
                catch (DbException ex)
                {
                    Program.HandleDbException(ex);
                }
            }
        }

        /// <summary>
        /// Handles references grid view row updated event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void ReferencesGridView_RowUpdated(object sender, RowObjectEventArgs e)
        {
            try
            {
                Program.DbSingletone.SubmitChanges();
            }
            catch (DbException ex)
            {
                Program.HandleDbException(ex);
            }
        }

        /// <summary>
        /// Handles phones grid view row updated event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void PhonesGridView_RowUpdated(object sender, RowObjectEventArgs e)
        {
            var phone = e.Row as ResponsibilityZonePhone;

            if (phone != null)
            {
                try
                {
                    Program.DbSingletone.SubmitChanges();
                }
                catch (DbException ex)
                {
                    Program.HandleDbException(ex);
                }

                NameOwnerTools.UpdateComboBox(phone.PhoneType, _phoneTypeComboBox);
            }
        }

        #endregion

        #region Embedded Navigator Events

        /// <summary>
        /// Gets user's confirmation for deletion.
        /// </summary>
        /// <returns>true if deletion is confirmed; otherwise, false.</returns>
        private static bool ConfirmDeletion()
        {
            // Show question message box and return result.
            return XtraMessageBox.Show(
                "�� �������, ��� ������ ������� ���������� ������?",
                "������������� ��������",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Question,
                MessageBoxDefaultButton.Button2) == DialogResult.Yes;
        }

        /// <summary>
        /// Handles ships grid control embedded navigator button click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void ShipsGridControl_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Remove:

                    // Selected row will not be removed.
                    e.Handled = true;

                    Ship ship = FocusedShip;

                    if (ship != null && ConfirmDeletion())
                    {
                        if (ship.ResponsibilityZone != null)
                        {
                            // Delete ship responsibility zone.
                            Program.DbSingletone.ResponsibilityZones.DeleteOnSubmit(ship.ResponsibilityZone);
                        }

                        Program.DbSingletone.Ships.DeleteOnSubmit(ship);

                        try
                        {
                            Program.DbSingletone.SubmitChanges();

                            // Selected row will be removed.
                            e.Handled = false;
                        }
                        catch (DbException ex)
                        {
                            Program.HandleDbException(ex);
                        }
                    }
                    break;
                case NavigatorButtonType.Custom:

                    var tag = e.Button.Tag as string;

                    if (tag != null)
                    {
                        tag = tag.Split( '#' )[0];
                        switch (tag)
                        {
                            case "EditLock":
                                // Toggle read-only property.
                                ReadOnly = !ReadOnly;
                                break;

                            case "Refresh":

                                Ship focusedShip = FocusedShip;

                                try
                                {
                                    Program.DbSingletone.SubmitChanges();
                                }
                                catch (DbException ex)
                                {
                                    Program.HandleDbException(ex);
                                }
                                finally
                                {
                                    FillComboBoxEditors();

                                    RefreshShipDataSource();

                                    FocusedShip = focusedShip;

                                    UpdateShipDetails(FocusedShip);
                                }
                                break;
                            case "Report":

                                _popupMenu.ShowPopup(MousePosition);

                                break;
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// Handles ship tools grid control embedded navigator button click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void ShipToolsGridControl_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Append:

                    e.Handled = true;

                    if (FocusedShip != null)
                    {
                        int currentRow = _shipToolsGridView.FocusedRowHandle;
                        if (currentRow < 0)
                        {
                            currentRow = _shipToolsGridView.GetDataRowHandleByGroupRowHandle(currentRow);
                        }

                        _shipToolsGridView.AddNewRow();

                        if (_shipToolsGridView.SortInfo.GroupCount == 0) return;

                        // Initialize group values.
                        foreach (GridColumn groupColumn in _shipToolsGridView.GroupedColumns)
                        {
                            object val = _shipToolsGridView.GetRowCellValue(currentRow, groupColumn);
                            _shipToolsGridView.SetRowCellValue(_shipToolsGridView.FocusedRowHandle, groupColumn, val);
                        }

                        _shipToolsGridView.ShowEditor();
                    }

                    break;

                case NavigatorButtonType.Remove:

                    // Selected row will not be removed.
                    e.Handled = true;

                    var shipTool = _shipToolsGridView.GetFocusedRow() as ShipTool;

                    if (shipTool != null && ConfirmDeletion())
                    {
                        Program.DbSingletone.ShipTools.DeleteOnSubmit(shipTool);

                        try
                        {
                            Program.DbSingletone.SubmitChanges();

                            // Selected row will be removed.
                            e.Handled = false;
                        }
                        catch (DbException ex)
                        {
                            Program.HandleDbException(ex);
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// Handles references grid control embedded navigator button click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void ReferencesGridControl_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Append:

                    e.Handled = true;

                    if (FocusedShip != null)
                    {
                        _referencesGridView.AddNewRow();
                        _referencesGridView.ShowEditor();
                    }

                    break;

                case NavigatorButtonType.Remove:

                    // Selected row will not be removed.
                    e.Handled = true;

                    var reference = _referencesGridView.GetFocusedRow() as Reference;

                    if (reference != null && ConfirmDeletion())
                    {
                        Program.DbSingletone.References.DeleteOnSubmit(reference);

                        try
                        {
                            Program.DbSingletone.SubmitChanges();

                            // Selected row will be removed.
                            e.Handled = false;
                        }
                        catch (DbException ex)
                        {
                            Program.HandleDbException(ex);
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// Handles phones grid control embedded navigator button click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void PhonesGridControl_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Append:

                    e.Handled = true;

                    if (FocusedShip != null)
                    {
                        int currentRow = _phonesGridView.FocusedRowHandle;
                        if (currentRow < 0)
                        {
                            currentRow = _phonesGridView.GetDataRowHandleByGroupRowHandle(currentRow);
                        }

                        _phonesGridView.AddNewRow();

                        if (_phonesGridView.SortInfo.GroupCount == 0) return;

                        // Initialize group values.
                        foreach (GridColumn groupColumn in _phonesGridView.GroupedColumns)
                        {
                            object val = _phonesGridView.GetRowCellValue(currentRow, groupColumn);
                            _phonesGridView.SetRowCellValue(_phonesGridView.FocusedRowHandle, groupColumn, val);
                        }

                        _phonesGridView.ShowEditor();
                    }
                    break;

                case NavigatorButtonType.Remove:

                    // Selected row will not be removed.
                    e.Handled = true;

                    var phone = _phonesGridView.GetFocusedRow() as ResponsibilityZonePhone;

                    if (phone != null && ConfirmDeletion())
                    {
                        Program.DbSingletone.ResponsibilityZonePhones.DeleteOnSubmit(phone);

                        try
                        {
                            Program.DbSingletone.SubmitChanges();

                            // Selected row will be removed.
                            e.Handled = false;
                        }
                        catch (DbException ex)
                        {
                            Program.HandleDbException(ex);
                        }
                    }
                    break;
            }
        }

        #endregion

        #region Code was added by V.Kovalev

        /// <summary>
        /// Previous selected phone in the phones grid view.
        /// </summary>
        private ResponsibilityZonePhone _prevPhone;

        /// <summary>
        /// Previous selected reference in the references grid view.
        /// </summary>
        private Reference _prevReference;

        /// <summary>
        /// Previous selected ship tool in the ship tools view grid.
        /// </summary>
        private ShipTool _prevShipTool;

        /// <summary>
        /// Validate the ship.
        /// </summary>
        /// <param name="ship">Ship to validate</param>
        /// <param name="errorText">Error if occured.</param>
        /// <returns>True if ship is valid; otherwise, false.</returns>
        private static bool ValidateShip(Ship ship, out string errorText)
        {
            bool isValid = true;
            errorText = null;

            try
            {
                string errText;

                // Validate ship attributes.
                if (String.IsNullOrEmpty(ship.Name))
                {
                    errorText = "�������� �� ������ ���� ������.";
                    isValid = false;
                }
                else if (!ShipNameIsAvailable(ship, out errText))
                {
                    errorText += errText;
                    isValid = false;
                }
                else if (!ShipIndexIsAvailable(ship, out errText))
                {
                    errorText += errText;
                    isValid = false;
                }
            }
            catch (DbException ex)
            {
                Program.HandleDbException(ex);
                isValid = false;
            }

            return isValid;
        }

        /// <summary>
        /// Validates ship tool.
        /// </summary>
        /// <param name="shipTool">Ship tool to validate</param>
        /// <param name="errorText">Error if occured.</param>
        /// <returns>True if ship tool is valid; otherwise, false.</returns>
        private bool ValidateShipTool(ShipTool shipTool, out string errorText)
        {
            bool isValid = true;
            errorText = null;

            try
            {
                Ship ship = FocusedShip;
                if (ship == null)
                {
                    errorText = "�������� �� ����� ���� ���������." + Environment.NewLine;
                }
                else
                {
                    // Validate ship tool attributes.
                    if (shipTool.Tool == null)
                    {
                        errorText = "������� ��� �������� ��������." + Environment.NewLine;
                    }
                    else if (
                        Program.DbSingletone.ShipTools.Any(
                            st =>
                            st.Tool.Equals(shipTool.Tool) && st.ShipId == ship.ShipId &&
                            st.ShipToolId != shipTool.ShipToolId))
                    {
                        errorText = "����� �������� ��� ���������." + Environment.NewLine;
                    }
                    else if (shipTool.ToolCategory == null)
                    {
                        errorText = "������� ��� �������� ���������." + Environment.NewLine;
                    }
                }
            }
            catch (DbException ex)
            {
                Program.HandleDbException(ex);
            }

            if (!string.IsNullOrEmpty(errorText))
            {
                isValid = false;
            }

            return isValid;
        }

        /// <summary>
        /// Validate the phone.
        /// </summary>
        /// <param name="phone">Phone to validate</param>
        /// <param name="errorText">Error if occured.</param>
        /// <returns>True if phone is valid; otherwise, false.</returns>
        private static bool ValidatePhone(ResponsibilityZonePhone phone, out string errorText)
        {
            bool isValid = true;
            errorText = null;

            if (phone == null)
            {
                errorText = "������ ����� �� ����� ���� ��������." + Environment.NewLine;
                isValid = false;
            }
            else
            {
                if (String.IsNullOrEmpty(phone.Number))
                {
                    errorText = "����� �� ������ ���� ������." + Environment.NewLine;
                    isValid = false;
                }
            }

            return isValid;
        }

        /// <summary>
        /// Validate the reference.
        /// </summary>
        /// <param name="reference">Reference to validate</param>
        /// <param name="errorText">Error if occured.</param>
        /// <returns>True if reference is valid; otherwise, false.</returns>
        private bool ValidateReference(Reference reference, out string errorText)
        {
            bool isValid = true;
            errorText = null;

            try
            {
                Ship ship = FocusedShip;
                if (ship == null)
                {
                    errorText = "������� �� ����� ���� ���������.";
                    isValid = false;
                }
                else
                {
                    // Validate reference attributes.
                    if (String.IsNullOrEmpty(reference.Item))
                    {
                        errorText = "������� �������.";
                        isValid = false;
                    }
                    else if (
                        Program.DbSingletone.References.Any(
                            r =>
                            r.Item == reference.Item && r.ShipId == ship.ShipId &&
                            r.ReferenceId != reference.ReferenceId))
                    {
                        errorText = "����� ������� ��� ��������.";
                        isValid = false;
                    }
                    else if (String.IsNullOrEmpty(reference.Value))
                    {
                        errorText = "������� ��������.";
                        isValid = false;
                    }
                }
            }
            catch (DbException ex)
            {
                Program.HandleDbException(ex);
                isValid = false;
            }

            return isValid;
        }

        /// <summary>
        /// Handles ship tools view grid focused row changed event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void ShipToolsGridView_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            string errorText;
            if (_prevShipTool != null && !ValidateShipTool(_prevShipTool, out errorText))
            {
                //If _previous ship tool is not validated then discard data context changes.
                Program.DiscardDataChanges();
            }

            if (_shipToolsGridView.IsGroupRow(_shipToolsGridView.FocusedRowHandle))
            {
                _prevShipTool = null;
            }
            else
            {
                _prevShipTool = _shipToolsGridView.GetRow(_shipToolsGridView.FocusedRowHandle) as ShipTool;
            }
        }

        /// <summary>
        /// Handles phones grid view focused view changed event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void PhonesGridView_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            string errorText;
            if (_prevPhone != null && !ValidatePhone(_prevPhone, out errorText))
            {
                //If _previous phone is not validated then discard data context changes.
                Program.DiscardDataChanges();
            }

            if (_phonesGridView.IsGroupRow(_phonesGridView.FocusedRowHandle))
            {
                _prevPhone = null;
            }
            else
            {
                _prevPhone = _phonesGridView.GetRow(_phonesGridView.FocusedRowHandle) as ResponsibilityZonePhone;
            }
        }

        /// <summary>
        /// Handles references grid view focused row changed event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void ReferencesGridView_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            string errorText;
            if (_prevReference != null && !ValidateReference(_prevReference, out errorText))
            {
                //If _previous phone is not validated then discard data context changes.
                Program.DiscardDataChanges();
            }

            if (_referencesGridView.IsGroupRow(_referencesGridView.FocusedRowHandle))
            {
                _prevReference = null;
            }
            else
            {
                _prevReference = _referencesGridView.GetRow(_referencesGridView.FocusedRowHandle) as Reference;
            }
        }

        public override IEnumerable<ISKOCBCTDNBTIDED> GetHiddenChilds( bool withLazy )
        {
            var hidden = new List<ISKOCBCTDNBTIDED>();
            if (withLazy)
            {
                var reportTemplateDummyForm = Reporter.GetReportTemplateDummyForm( );
                var supportAccessAreasForm = reportTemplateDummyForm as ISupportAccessAreas;
                supportAccessAreasForm.HierarchyParent = Program.StartForm;
                hidden.Add( new ControlInterfaceImplementation( reportTemplateDummyForm ) );
                var coursesForm = new CoursesForm( null ) { HierarchyParent = this };
                hidden.Add( new ControlInterfaceImplementation( coursesForm ) );
            }
            return hidden;
        }

        #endregion
    }
}
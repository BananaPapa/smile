using System;
using System.Configuration;
using System.Data.Common;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace Curriculum
{
    /// <summary>
    /// Connection form.
    /// </summary>
    public partial class ConnectionForm : XtraForm
    {
        public ConnectionForm()
        {
            InitializeComponent();
        }

        private void ConnectionForm_Load(object sender, EventArgs e)
        {
            var builder = new DbConnectionStringBuilder();
            builder.ConnectionString = Program.DbSingletone.Connection.ConnectionString;
            _textEditDataSource.Text = (string) builder["Data Source"];
            _textEditDatabase.Text = (string) builder["Initial Catalog"];
            _textEditUser.Text = (string) builder["User ID"];
        }

        private void _simpleButtonConnect_Click(object sender, EventArgs e)
        {
            var builder = new DbConnectionStringBuilder();
            builder["Persist Security Info"] = true;
            builder["Initial Catalog"] = _textEditDatabase.Text;
            builder["Data Source"] = _textEditDataSource.Text;
            builder["User ID"] = _textEditUser.Text;
            builder["Password"] = _textEditPassword.Text;

            Program.DbSingletone.Connection.ConnectionString = builder.ConnectionString;
            if (Program.DbSingletone.DatabaseExists())
            {
                DialogResult = DialogResult.OK;

                ConnectionStringSettingsCollection connections = ConfigurationManager.ConnectionStrings;
                try
                {
                    Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                    config.ConnectionStrings.ConnectionStrings[
                        "Curriculum.Properties.Settings.CurriculumConnectionString"].ConnectionString =
                        builder.ConnectionString;
                    config.Save(ConfigurationSaveMode.Modified, true);
                    ConfigurationManager.RefreshSection("connectionStrings");
                }
                catch (NullReferenceException)
                {
                }

                Close();
            }
            else
            {
                Program.HandleDbConnectionError();
            }
        }
    }
}
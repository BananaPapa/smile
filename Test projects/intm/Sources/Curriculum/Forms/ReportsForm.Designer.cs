namespace Curriculum
{
    partial class ReportsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportsForm));
            this._navBarControl = new DevExpress.XtraNavBar.NavBarControl();
            this._staticNavBarGroup = new DevExpress.XtraNavBar.NavBarGroup();
            this._dailyRoutineNavBarItem = new DevExpress.XtraNavBar.NavBarItem();
            this._birthDatesNavBarItem = new DevExpress.XtraNavBar.NavBarItem();
            this._teachingConclusionNavBarItem = new DevExpress.XtraNavBar.NavBarItem();
            this._workDirectionNavBarItem = new DevExpress.XtraNavBar.NavBarItem();
            this.splitterControl1 = new DevExpress.XtraEditors.SplitterControl();
            this._panelControl = new DevExpress.XtraEditors.PanelControl();
            this._hintLabelControl = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this._navBarControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._panelControl)).BeginInit();
            this._panelControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // _navBarControl
            // 
            this._navBarControl.ActiveGroup = this._staticNavBarGroup;
            this._navBarControl.ContentButtonHint = null;
            this._navBarControl.Dock = System.Windows.Forms.DockStyle.Left;
            this._navBarControl.Groups.AddRange(new DevExpress.XtraNavBar.NavBarGroup[] {
            this._staticNavBarGroup});
            this._navBarControl.Items.AddRange(new DevExpress.XtraNavBar.NavBarItem[] {
            this._birthDatesNavBarItem,
            this._teachingConclusionNavBarItem,
            this._dailyRoutineNavBarItem,
            this._workDirectionNavBarItem});
            this._navBarControl.LinkSelectionMode = DevExpress.XtraNavBar.LinkSelectionModeType.OneInControl;
            this._navBarControl.Location = new System.Drawing.Point(0, 0);
            this._navBarControl.Name = "_navBarControl";
            this._navBarControl.OptionsNavPane.ExpandedWidth = 134;
            this._navBarControl.Size = new System.Drawing.Size(190, 501);
            this._navBarControl.TabIndex = 0;
            this._navBarControl.Text = "������";
            this._navBarControl.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.NavBarControl_LinkClicked);
            // 
            // _staticNavBarGroup
            // 
            this._staticNavBarGroup.Caption = "�����������";
            this._staticNavBarGroup.Expanded = true;
            this._staticNavBarGroup.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this._dailyRoutineNavBarItem),
            new DevExpress.XtraNavBar.NavBarItemLink(this._birthDatesNavBarItem),
            new DevExpress.XtraNavBar.NavBarItemLink(this._teachingConclusionNavBarItem),
            new DevExpress.XtraNavBar.NavBarItemLink(this._workDirectionNavBarItem)});
            this._staticNavBarGroup.LargeImageIndex = 1;
            this._staticNavBarGroup.Name = "_staticNavBarGroup";
            this._staticNavBarGroup.SelectedLinkIndex = 5;
            this._staticNavBarGroup.Tag = "�����������#r";
            // 
            // _dailyRoutineNavBarItem
            // 
            this._dailyRoutineNavBarItem.Caption = "���������� ���";
            this._dailyRoutineNavBarItem.Name = "_dailyRoutineNavBarItem";
            this._dailyRoutineNavBarItem.SmallImage = global::Curriculum.Properties.Resources.Bullet16;
            this._dailyRoutineNavBarItem.Tag = "���������� ���#r";
            // 
            // _birthDatesNavBarItem
            // 
            this._birthDatesNavBarItem.Caption = "��� ��������";
            this._birthDatesNavBarItem.Name = "_birthDatesNavBarItem";
            this._birthDatesNavBarItem.SmallImage = ((System.Drawing.Image)(resources.GetObject("_birthDatesNavBarItem.SmallImage")));
            this._birthDatesNavBarItem.SmallImageIndex = 0;
            this._birthDatesNavBarItem.Tag = "��� ��������#r";
            // 
            // _teachingConclusionNavBarItem
            // 
            this._teachingConclusionNavBarItem.Caption = "���������� �� ��������";
            this._teachingConclusionNavBarItem.Name = "_teachingConclusionNavBarItem";
            this._teachingConclusionNavBarItem.SmallImage = ((System.Drawing.Image)(resources.GetObject("_teachingConclusionNavBarItem.SmallImage")));
            this._teachingConclusionNavBarItem.Tag = "���������� �� ��������#r";
            // 
            // _workDirectionNavBarItem
            // 
            this._workDirectionNavBarItem.Caption = "����������� �� ������";
            this._workDirectionNavBarItem.Name = "_workDirectionNavBarItem";
            this._workDirectionNavBarItem.SmallImage = global::Curriculum.Properties.Resources.Bullet16;
            this._workDirectionNavBarItem.Tag = "����������� �� ������#r";
            // 
            // splitterControl1
            // 
            this.splitterControl1.Location = new System.Drawing.Point(190, 0);
            this.splitterControl1.Name = "splitterControl1";
            this.splitterControl1.Size = new System.Drawing.Size(6, 501);
            this.splitterControl1.TabIndex = 1;
            this.splitterControl1.TabStop = false;
            // 
            // _panelControl
            // 
            this._panelControl.Controls.Add(this._hintLabelControl);
            this._panelControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._panelControl.Location = new System.Drawing.Point(196, 0);
            this._panelControl.Name = "_panelControl";
            this._panelControl.Size = new System.Drawing.Size(493, 501);
            this._panelControl.TabIndex = 2;
            // 
            // _hintLabelControl
            // 
            this._hintLabelControl.Appearance.Image = global::Curriculum.Properties.Resources.Left24;
            this._hintLabelControl.Appearance.ImageIndex = 2;
            this._hintLabelControl.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this._hintLabelControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._hintLabelControl.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this._hintLabelControl.Location = new System.Drawing.Point(2, 2);
            this._hintLabelControl.Name = "_hintLabelControl";
            this._hintLabelControl.Padding = new System.Windows.Forms.Padding(20);
            this._hintLabelControl.Size = new System.Drawing.Size(489, 497);
            this._hintLabelControl.TabIndex = 0;
            this._hintLabelControl.Text = "�������� ����������� �����";
            // 
            // ReportsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(689, 501);
            this.Controls.Add(this._panelControl);
            this.Controls.Add(this.splitterControl1);
            this.Controls.Add(this._navBarControl);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ReportsForm";
            this.Tag = "������ ����������\\����������\\������#r";
            this.Text = "������";
            ((System.ComponentModel.ISupportInitialize)(this._navBarControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._panelControl)).EndInit();
            this._panelControl.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraNavBar.NavBarControl _navBarControl;
        private DevExpress.XtraNavBar.NavBarGroup _staticNavBarGroup;
        private DevExpress.XtraEditors.SplitterControl splitterControl1;
        private DevExpress.XtraNavBar.NavBarItem _birthDatesNavBarItem;
        private DevExpress.XtraEditors.PanelControl _panelControl;
        private DevExpress.XtraEditors.LabelControl _hintLabelControl;
        private DevExpress.XtraNavBar.NavBarItem _teachingConclusionNavBarItem;
        private DevExpress.XtraNavBar.NavBarItem _dailyRoutineNavBarItem;
        private DevExpress.XtraNavBar.NavBarItem _workDirectionNavBarItem;
    }
}
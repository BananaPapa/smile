using System;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraScheduler;
using DevExpress.XtraScheduler.UI;
using Eureca.Integrator.Common;

namespace Curriculum
{
    public partial class EventForm : FormWithAccessAreas
    {
        private readonly SchedulerControl _control;
        private readonly EventAppointmentFormController _controller;
        private bool _canClose = true;
        private int _suspendUpdateCount;

        public EventForm(Appointment apt, SchedulerControl control)
        {
            _control = control;
            _controller = new EventAppointmentFormController(control, apt);

            SuspendUpdate();
            InitializeComponent();
            ResumeUpdate();

            UpdateForm();
        }

        private EventForm()
        {
            InitializeComponent();
        }

        public static EventForm GetDummy()
        {
            return new EventForm();
        }

        protected AppointmentStorage Appointments
        {
            get { return _control.Storage.Appointments; }
        }

        protected bool IsUpdateSuspended
        {
            get { return _suspendUpdateCount > 0; }
        }

        protected void SuspendUpdate()
        {
            _suspendUpdateCount++;
        }

        protected void ResumeUpdate()
        {
            if (_suspendUpdateCount > 0)
                _suspendUpdateCount--;
        }

        private void UpdateForm()
        {
            SuspendUpdate();
            try
            {
                _eventNameTextEdit.Text = _controller.Subject;
                _dateStart.DateTime = _controller.Start.Date;
                _dateEnd.DateTime = _controller.End.Date;
                if (_controller.TimeBeforeStart != null)
                {
                    _checkEditHasReminder.Checked = true;
                    _durationEditTimeBeforeStart.EditValue = _controller.TimeBeforeStart;
                }
                else
                {
                    _checkEditHasReminder.Checked = false;
                }

                _timeStart.Time = DateTime.MinValue.AddTicks(_controller.Start.TimeOfDay.Ticks);
                _timeEnd.Time = DateTime.MinValue.AddTicks(_controller.End.TimeOfDay.Ticks);

                _descriptionMemoEdit.Text = _controller.Description;
                _isAllDayCheckEdit.Checked = _controller.AllDay;
            }
            finally
            {
                ResumeUpdate();
            }
        }

        private void BtnOkClick(object sender, EventArgs e)
        {
            string errorText;
            var errorMsg = new StringBuilder();

            if (!EventNameValidate(_eventNameTextEdit.Text, out errorText))
            {
                errorMsg.Append(errorText);
                errorMsg.Append(Environment.NewLine);
            }

            var start = new DateTime(_dateStart.DateTime.Year, _dateStart.DateTime.Month,
                                     _dateStart.DateTime.Day, _timeStart.Time.Hour, _timeStart.Time.Minute,
                                     _timeStart.Time.Second);
            var end = new DateTime(_dateEnd.DateTime.Year, _dateEnd.DateTime.Month,
                                   _dateEnd.DateTime.Day, _timeEnd.Time.Hour, _timeEnd.Time.Minute, _timeEnd.Time.Second);

            if (!_isAllDayCheckEdit.Checked && start > end)
            {
                errorMsg.Append("����� ������ �� ����� ���� ������ ������� ���������!");
            }

            if (!string.IsNullOrEmpty(errorMsg.ToString()))
            {
                XtraMessageBox.Show(errorMsg.ToString(),
                                    "������", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _canClose = false;
            }
            else
            {
                _controller.Subject = _eventNameTextEdit.Text;
                _controller.Description = _descriptionMemoEdit.Text;
                _controller.AllDay = _isAllDayCheckEdit.Checked;
                if (_isAllDayCheckEdit.Checked)
                {
                    _controller.Start = start;
                }
                else
                {
                    _controller.Start = start;
                    _controller.End = end;
                }
                if (_checkEditHasReminder.Checked)
                {
                    _controller.TimeBeforeStart = (TimeSpan) _durationEditTimeBeforeStart.EditValue;
                }
                else
                {
                    _controller.TimeBeforeStart = null;
                }

                _controller.ApplyChanges();
                _canClose = true;
            }
        }

        private void EventFormFormClosing(object sender, FormClosingEventArgs e)
        {
            if (!_canClose)
            {
                e.Cancel = true;
            }
        }

        private void BtnCancelClick(object sender, EventArgs e)
        {
            _canClose = true;
        }

        private void IsAllDayCheckEditCheckedChanged(object sender, EventArgs e)
        {
            _timeStart.Enabled = _dateEnd.Enabled = _timeEnd.Enabled = !_isAllDayCheckEdit.Checked;
        }

        private void CheckEditHasReminderCheckedChanged(object sender, EventArgs e)
        {
            _durationEditTimeBeforeStart.Enabled = _checkEditHasReminder.Checked;
            if (_durationEditTimeBeforeStart.Enabled && _durationEditTimeBeforeStart.EditValue == null)
            {
                _durationEditTimeBeforeStart.EditValue = new TimeSpan(0, 15, 0);
            }
        }

        private static bool EventNameValidate(string name, out string errorText)
        {
            bool isValidate = true;
            errorText = null;

            if (string.IsNullOrEmpty(name))
            {
                isValidate = false;
                errorText = @"��� �� ������ ���� ������!";
            }

            return isValidate;
        }
    }

    public class EventAppointmentFormController : AppointmentFormController
    {
        #region Public properties

        public TimeSpan? TimeBeforeStart
        {
            get
            {
                TimeSpan? ts = null;
                if (EditedAppointmentCopy.CustomFields["Event"] != null)
                {
                    int? seconds = (EditedAppointmentCopy.CustomFields["Event"] as Event).ReminderTime;
                    ts = (seconds.HasValue) ? (TimeSpan?) TimeSpan.FromSeconds(seconds.Value) : null;
                }
                return ts;
            }
            set
            {
                if (EditedAppointmentCopy.CustomFields["Event"] != null)
                {
                    (EditedAppointmentCopy.CustomFields["Event"] as Event).ReminderTime = (value.HasValue)
                                                                                              ? (int?)
                                                                                                value.Value.TotalSeconds
                                                                                              : null;
                }
            }
        }

        public TimeSpan? TimeBeforeStartSource
        {
            get
            {
                TimeSpan? ts = null;
                if (SourceAppointment.CustomFields["Event"] != null)
                {
                    int? seconds = (SourceAppointment.CustomFields["Event"] as Event).ReminderTime;
                    ts = (seconds.HasValue) ? (TimeSpan?) TimeSpan.FromSeconds(seconds.Value) : null;
                }
                return ts;
            }
            set
            {
                if (SourceAppointment.CustomFields["Event"] != null)
                {
                    (SourceAppointment.CustomFields["Event"] as Event).ReminderTime = (value.HasValue)
                                                                                          ? (int?)
                                                                                            value.Value.TotalSeconds
                                                                                          : null;
                }
            }
        }

        #endregion

        public EventAppointmentFormController(SchedulerControl control, Appointment apt) : base(control, apt)
        {
        }

        public override bool IsAppointmentChanged()
        {
            if (base.IsAppointmentChanged())
            {
                return true;
            }
            return (TimeBeforeStart != TimeBeforeStartSource);
        }

        protected override void ApplyCustomFieldsValues()
        {
            TimeBeforeStartSource = TimeBeforeStart;
        }
    }
}
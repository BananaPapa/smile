using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Common;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraScheduler;
using DevExpress.XtraScheduler.Drawing;
using Eureca.Integrator.Common;

namespace Curriculum
{
    /// <summary>
    /// Represents calendar form.
    /// </summary>
    public partial class CalendarForm : FormWithAccessAreas
    {
        private const int BirthdaySuspendDays = 30;

        #region Constructors

        /// <summary>
        /// Initializes new instance of calendar form.
        /// </summary>
        public CalendarForm()
        {
            InitializeComponent();

            _schedulerControl.Start = DateTime.Today;

            _schedulerStorage.Appointments.CustomFieldMappings.Add(new AppointmentCustomFieldMapping("Event",
                "Event"));
            _schedulerStorage.Appointments.CustomFieldMappings.Add(new AppointmentCustomFieldMapping("Person",
                "Person"));
            _schedulerStorage.Appointments.CustomFieldMappings.Add(new AppointmentCustomFieldMapping("CommentText",
                "CommentText"));
            _schedulerStorage.Appointments.CustomFieldMappings.Add(new AppointmentCustomFieldMapping("IsImportant",
                "IsImportant"));
            _schedulerStorage.Appointments.CustomFieldMappings.Add(new AppointmentCustomFieldMapping("Title",
                "Title"));
            _schedulerStorage.Appointments.CustomFieldMappings.Add(new AppointmentCustomFieldMapping("Duration",
                "Duration"));
            _schedulerStorage.Appointments.CustomFieldMappings.Add(new AppointmentCustomFieldMapping("TimeBeforeStart",
                "TimeBeforeStart"));

            _schedulerControl.ActiveView.AppointmentDisplayOptions.SnapToCellsMode = AppointmentSnapToCellsMode.Disabled;
        }

        #endregion

        #region Fields

        /// <summary>
        /// List of persons to show in calendar.
        /// </summary>
        private readonly List<Person> _persons = new List<Person>();

        /// <summary>
        /// Form with event's reminders.
        /// </summary>
        private ReminderForm _reminderForm;

        #endregion

        #region Public methods

        /// <summary>
        /// Sets current date for calendar.
        /// </summary>
        public void GoToToday()
        {
            _schedulerControl.GoToToday();
        }

        /// <summary>
        /// Shows appointment edit form.
        /// </summary>
        /// <param name="apt">Appointment to show.</param>
        public void ShowAppointment(Appointment apt)
        {
            Debug.Assert(apt != null, "Appointment must be not null.");

            if (apt == null)
            {
                throw new ArgumentNullException("apt", "Appointment must be not null.");
            }

            _schedulerControl.ShowEditAppointmentForm(apt);
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Fills label collection.
        /// </summary>
        private void FillLabels()
        {
            _schedulerStorage.Appointments.Labels.Clear();

            _schedulerStorage.Appointments.Labels.AddRange
                (new[]
                {
                    new AppointmentLabel(Color.Tomato, SchedulerTools.BirthdayDateType),
                    new AppointmentLabel(Color.Snow, SchedulerTools.KnowndateDateType),
                    new AppointmentLabel(Color.DarkOrange, SchedulerTools.EventDateType),
                    new AppointmentLabel(Color.Firebrick, SchedulerTools.HolidayDateType),
                    new AppointmentLabel(Color.LightSlateGray, SchedulerTools.BirthdayWithGreetingDateType)
                }
                );
        }

        /// <summary>
        /// Loads persons list.
        /// </summary>
        private void LoadPersons()
        {
            _persons.Clear();

            int? excludeStatusId = Setting.GetExtclusionStatusId();

            IQueryable<Person> persons = from p in Program.DbSingletone.Persons select p;

            if (excludeStatusId != null)
            {
                persons = from p in persons
                    where p.Statuse == null || p.Statuse.StatusId != excludeStatusId
                    select p;
            }

            _persons.AddRange(persons);
        }

        /// <summary>
        /// Creates appointment for the event.
        /// </summary>
        /// <param name="evt">event</param>
        /// <returns>new appointment</returns>
        private Appointment CreateEventAppointment(Event evt)
        {
            #region Debug.Assert()

            Debug.Assert(evt != null, "Event must be not null.");

            #endregion

            #region Checking input

            if (evt == null)
            {
                throw new ArgumentNullException("evt", "Event must be not null");
            }

            #endregion

            Appointment apt = _schedulerStorage.CreateAppointment(AppointmentType.Normal);
            apt.Subject = evt.Name;
            apt.Description = evt.Description;
            try
            {
                AppointmentLabel label =
                    (AppointmentLabel)
                        _schedulerStorage.Appointments.Labels.Find(
                            item => item.DisplayName == SchedulerTools.EventDateType);
                apt.LabelId = _schedulerStorage.Appointments.Labels.IndexOf(label);
            }
            catch (Exception)
            {
            }
            apt.Start = evt.StartDateTime;
            apt.End = evt.EndDateTime;
            apt.AllDay = evt.AllDay;
            apt.CustomFields["Event"] = evt;

            AppointmentStatus status = _schedulerStorage.Appointments.Statuses[AppointmentStatusType.OutOfOffice];
            apt.StatusId = _schedulerStorage.Appointments.Statuses.IndexOf(status);

            if (evt.ReminderTime != null && evt.EndDateTime > DateTime.Now)
            {
                apt.HasReminder = true;
                if (apt.Reminder != null)
                {
                    apt.Reminder.TimeBeforeStart = TimeSpan.FromSeconds(evt.ReminderTime.Value);
                }
            }

            return apt;
        }

        /// <summary>
        /// Creates appointments for person birthday.
        /// </summary>
        /// <param name="date">birthday date</param>
        /// <param name="person">person</param>
        /// <returns>apointments</returns>
        private List<Appointment> CreateBirthdayAppointments(DateTime date, Person person,
            IEnumerable<Greeting> greetings)
        {
            #region Debug.Assert()

            Debug.Assert(person != null, "Person must be not null.");

            #endregion

            #region Checking input

            if (person == null)
            {
                throw new ArgumentNullException("Person must be not null");
            }

            #endregion

            var apts = new List<Appointment>();
            int year = DateTime.Today.Year;
            int endYear = DateTime.Today.Year + 1;

            while (year <= endYear)
            {
                var sb = new StringBuilder();
                sb.Append("���� ��������");
                sb.Append(" (");
                sb.Append(year - person.BirthDate.Year);
                sb.Append(")");

                DateTime checkedDate = SchedulerTools.CheckDateTime(year, date.Month, date.Day);

                if (checkedDate + TimeSpan.FromDays(BirthdaySuspendDays) >= DateTime.Today)
                {
                    bool greetingExists = greetings != null &&
                                          greetings.Any(g => g.PersonId == person.PersonId && g.Date == checkedDate);

                    Appointment apt =
                        CreateKnownDateAppointment(
                            SchedulerTools.CheckDateTime(year, date.Month, date.Day),
                            sb.ToString(),
                            greetingExists
                                ? SchedulerTools.BirthdayWithGreetingDateType
                                : SchedulerTools.BirthdayDateType, person);
                    if (apt != null)
                    {
                        apts.Add(apt);
                        if (apt.Start.Date >= DateTime.Now.Date && !greetingExists)
                        {
                            apt.HasReminder = true;
                            if (apt.Reminder != null)
                            {
                                apt.Reminder.TimeBeforeStart = new TimeSpan(2, 0, 0, 0);
                            }
                        }
                    }

                    break;
                }

                year++;
            }
            return apts;
        }

        /// <summary>
        /// Creates person's known date appointment.
        /// </summary>
        /// <param name="date">known date</param>
        /// <param name="subject">appointment subject</param>
        /// <param name="dateType">kind of known date</param>
        /// <param name="person">person</param>
        /// <returns>appointment</returns>
        private Appointment CreateKnownDateAppointment(DateTime date, string subject, string dateType, Person person)
        {
            Appointment apt = _schedulerStorage.CreateAppointment(AppointmentType.Normal);

            apt.AllDay = true;
            apt.Start = date;
            try
            {
                AppointmentLabel label =
                    (AppointmentLabel) _schedulerStorage.Appointments.Labels.Find(item => item.DisplayName == dateType);
                apt.LabelId = _schedulerStorage.Appointments.Labels.IndexOf(label);
            }
            catch (Exception)
            {
            }

            apt.Subject = String.Format("{0} - ", person.Name) + subject;
            apt.CustomFields["Person"] = person;
            apt.CustomFields["Title"] = subject;

            return apt;
        }

        /// <summary>
        /// Refreshes calendar.
        /// </summary>
        public void RefreshCalendar()
        {
            Cursor = Cursors.WaitCursor;
            _schedulerStorage.Appointments.Clear();

            FillLabels();
            LoadPersons();

            Application.DoEvents();

            SchedulerTools.SetTimeRange(_schedulerControl);
            SchedulerTools.FillHolidays(_schedulerControl);

            //Adds event appointments for current user.
            if (Program.CurrentUser != null)
            {
                IQueryable<Event> events = from evt in Program.DbSingletone.Events
                    where evt.User == Program.CurrentUser
                    select evt;
                foreach (Event evt in events)
                {
                    Appointment apt = CreateEventAppointment(evt);
                    if (apt != null)
                    {
                        _schedulerStorage.Appointments.Add(apt);
                    }
                }
            }

            Application.DoEvents();

            int[] affiliationIds = Setting.GetBirthAffiliationIds();

            DateTime startDate = DateTime.Today - TimeSpan.FromDays(BirthdaySuspendDays);
            DateTime stopDate = SchedulerTools.CheckDateTime(startDate.Year + 1, startDate.Month, startDate.Day);
            var greetings = Program.DbSingletone.Greetings.Where(g => g.Date >= startDate && g.Date <= stopDate);

            //Adds selected person's appointments.
            foreach (Person person in _persons)
            {
                // Add person's main dates.
                if ((person.Statuse != null && person.Statuse.CreateProbationer) || affiliationIds == null ||
                    person.Affiliation == null ||
                    affiliationIds.Contains(person.Affiliation.AffiliationId))
                {
                    _schedulerStorage.Appointments.Items.AddRange(CreateBirthdayAppointments(person.BirthDate, person,
                        greetings));
                }

                if (person.Statuse != null && person.Statuse.CreateProbationer)
                {
                    if (person.TeachingStartDate != null)
                    {
                        Appointment apt = CreateKnownDateAppointment((DateTime) person.TeachingStartDate,
                            "������ ��������", SchedulerTools.KnowndateDateType,
                            person);
                        if (apt != null)
                        {
                            _schedulerStorage.Appointments.Add(apt);
                        }
                    }
                    if (person.ExaminationDate != null)
                    {
                        Appointment apt = CreateKnownDateAppointment((DateTime) person.ExaminationDate,
                            "���� ��������", SchedulerTools.KnowndateDateType,
                            person);
                        if (apt != null)
                        {
                            _schedulerStorage.Appointments.Add(apt);
                        }
                    }
                    if (person.DepartureDate != null)
                    {
                        Appointment apt = CreateKnownDateAppointment((DateTime) person.DepartureDate,
                            "���� ������", SchedulerTools.KnowndateDateType,
                            person);
                        if (apt != null)
                        {
                            _schedulerStorage.Appointments.Add(apt);
                        }
                    }
                }
            }

            Cursor = Cursors.Default;
        }

        /// <summary>
        /// Submit changes in database.
        /// </summary>
        /// <returns></returns>
        private static bool SubmitChanges()
        {
            try
            {
                Program.DbSingletone.SubmitChanges();
                return true;
            }
            catch (DbException ex)
            {
                Program.HandleDbException(ex);
            }
            catch (Exception)
            {
                var sb = new StringBuilder();
                sb.Append("��� ������� ��������� ��������� � ���� ������");
                sb.Append(Environment.NewLine);
                sb.Append("��������� ������. ��� ��������� ���� ��������.");
                sb.Append(Environment.NewLine);
                sb.Append("������������� ���������.");
                XtraMessageBox.Show(sb.ToString(), "����������� ������", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }

            return false;
        }

        #region Event Handlers

        // ReSharper disable MemberCanBeMadeStatic.Local

        /// <summary>
        /// Handles calendar form load event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CalendarFormLoad(object sender, EventArgs e)
        {
            //Random rand = new Random();          

            //for (int i = 0; i < 400; ++i)
            //{
            //    double randDouble = rand.NextDouble();
            //    var teachingStart = new DateTime(rand.Next(DateTime.Today.Year - 1, DateTime.Today.Year),
            //                                          rand.Next(1, 12), rand.Next(1, 28));
            //    DateTime examDate = teachingStart + TimeSpan.FromDays(rand.Next(60, 100));
            //    DateTime departDate = examDate + TimeSpan.FromDays(rand.Next(10, 20));

            //    var person = new Person
            //                        {
            //                            Name = "������ ϸ�� ����������" + i,
            //                            BirthDate =
            //                                new DateTime(rand.Next(1950, 1990), rand.Next(1, 12), rand.Next(1, 28)),
            //                            Status =
            //                                departDate > DateTime.Today
            //                                    ? Program.Db.Statuses.First(s => s.Name == "�")
            //                                    : (randDouble > 0.1
            //                                           ? Program.Db.Statuses.First(s => s.Name == "�")
            //                                           : Program.Db.Statuses.First(s => s.Name == "�")),
            //                            TeachingStartDate = teachingStart,
            //                            ExaminationDate = examDate,
            //                            DepartureDate = departDate,
            //                        };

            //    Program.Db.Persons.InsertOnSubmit(person);
            //}

            //Program.Db.SubmitChanges();

            RefreshCalendar();
        }

        /// <summary>
        /// Customizes the appointment editing form.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void SchedulerControlEditAppointmentFormShowing(object sender, AppointmentFormEventArgs e)
        {
            Appointment apt = e.Appointment;

            #region Event appointment

            if (apt.CustomFields["Event"] != null)
            {
                var frm = new EventForm(apt, (SchedulerControl) sender) {Icon = Icon, HierarchyParent = this};
                frm.LookAndFeel.ParentLookAndFeel = LookAndFeel.ParentLookAndFeel;
                e.DialogResult = frm.ShowDialog();
                if (e.DialogResult == DialogResult.OK)
                {
                    var evt = apt.CustomFields["Event"] as Event;
                    if (evt != null)
                    {
                        evt.Name = apt.Subject;
                        evt.Description = apt.Description;
                        evt.AllDay = apt.AllDay;
                        evt.StartDateTime = apt.Start;
                        evt.EndDateTime = apt.End;

                        if (evt.ReminderTime != null)
                        {
                            apt.HasReminder = true;
                            if (apt.Reminder != null)
                            {
                                apt.Reminder.TimeBeforeStart = TimeSpan.FromSeconds(evt.ReminderTime.Value);
                            }
                        }
                        else
                        {
                            evt.ReminderTime = null;
                            apt.HasReminder = false;
                        }

                        SubmitChanges();
                    }
                }
            }

            #endregion

            e.Handled = true;
        }

        /// <summary>
        /// Forms the appointment tool tip.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>

        private void ToolTipControllerBeforeShow(object sender, ToolTipControllerShowEventArgs e)

        {
            var controller = sender as ToolTipController;
            var aptViewInfo = controller.ActiveObject as AppointmentViewInfo;
            if (aptViewInfo == null) return;
            Appointment apt = aptViewInfo.Appointment;
            if (apt == null) return;
            e.IconType = ToolTipIconType.Information;
            var sb = new StringBuilder();

            if (!apt.AllDay)
            {
                sb.Append(apt.Start.ToShortTimeString());
                sb.Append(" - ");
                sb.Append(apt.End.ToShortTimeString());
                sb.Append(" (");
                var evt = apt.CustomFields["Event"] as Event;
                if (evt != null)
                {
                    sb.Append("�������");
                }
                sb.Append(") ");
            }
            sb.Append(e.ToolTip);

            if (_schedulerStorage.Appointments.Labels[apt.LabelId].DisplayName ==
                SchedulerTools.BirthdayWithGreetingDateType)
            {
                sb.Append(". ������������ ������������!");
            }

            e.Title = sb.ToString();
            e.ToolTip = apt.Description;
        }

        /// <summary>
        /// Handles refresh bar button item click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void RefreshBarButtonItemItemClick(object sender, ItemClickEventArgs e)
        {
            UseWaitCursor = true;
            Application.DoEvents();

            Program.DbSingletone.RefreshDataSource(typeof (Schedule), typeof (Holiday), typeof (Person), typeof (Event));
            Application.DoEvents();

            RefreshCalendar();
            UseWaitCursor = false;
        }

        /// <summary>
        /// Adds new event for current user.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void BarButtonItemAddEventItemClick(object sender, ItemClickEventArgs e)
        {
            var evt = new Event();
            if (_schedulerControl.SelectedInterval != TimeInterval.Empty)
            {
                evt.StartDateTime = _schedulerControl.SelectedInterval.Start;
                evt.EndDateTime = _schedulerControl.SelectedInterval.End;
            }
            else
            {
                evt.StartDateTime = _dateNavigator.SelectionStart;
                evt.EndDateTime = _dateNavigator.SelectionStart;
            }
            evt.User = Program.CurrentUser;

            Appointment apt = CreateEventAppointment(evt);
            Program.DbSingletone.Events.InsertOnSubmit( evt );
            var frm = new EventForm(apt, _schedulerControl) {Icon = Icon, HierarchyParent = this};
            if (frm.ShowDialog() == DialogResult.OK)
            {
                evt.Name = apt.Subject;
                evt.Description = apt.Description;
                evt.AllDay = apt.AllDay;
                evt.StartDateTime = apt.Start;
                evt.EndDateTime = apt.End;

                //Program.Db.Events.InsertOnSubmit(evt);
                if (SubmitChanges())
                {
                    if (evt.ReminderTime != null)
                    {
                        apt.HasReminder = true;
                        if (apt.Reminder != null)
                        {
                            apt.Reminder.TimeBeforeStart = TimeSpan.FromSeconds(evt.ReminderTime.Value);
                        }
                    }
                    _schedulerStorage.Appointments.Add(apt);
                }

                if (Program.StartForm != null)
                {
                    Program.StartForm.CountImportantEvents(ImportantEvent.UserEvent);
                }
            }
            else
            {
                Program.DbSingletone.Events.DeleteOnSubmit( evt );
            }
        }

        /// <summary>
        /// Handles allow appointment edit event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void SchedulerControlAllowAppointmentEdit(object sender, AppointmentOperationEventArgs e)
        {
            if ((e.Appointment.CustomFields["Event"] as Event) == null)
            {
                e.Allow = false;
            }
        }

        /// <summary>
        /// Handles appointment deleting event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void SchedulerStorageAppointmentDeleting(object sender, PersistentObjectCancelEventArgs e)
        {
            var apt = e.Object as Appointment;
            if (apt != null)
            {
                var evt = apt.CustomFields["Event"] as Event;
                if (evt != null)
                {
                    Program.DbSingletone.Events.DeleteOnSubmit(evt);
                    SubmitChanges();
                }
                else
                {
                    e.Cancel = true;
                }
            }

            if (Program.StartForm != null)
            {
                Program.StartForm.CountImportantEvents(ImportantEvent.UserEvent);
            }
        }

        /// <summary>
        /// Handles apointment resize event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void SchedulerControlAppointmentResized(object sender, AppointmentResizeEventArgs e)
        {
            var evt = e.EditedAppointment.CustomFields["Event"] as Event;
            if (evt != null)
            {
                evt.StartDateTime = e.EditedAppointment.Start;
                evt.EndDateTime = e.EditedAppointment.End;

                SubmitChanges();
            }
        }

        /// <summary>
        /// Handles appointment drop event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void SchedulerControlAppointmentDrop(object sender, AppointmentDragEventArgs e)
        {
            var evt = e.EditedAppointment.CustomFields["Event"] as Event;
            if (evt != null)
            {
                evt.StartDateTime = e.EditedAppointment.Start;
                evt.EndDateTime = e.EditedAppointment.End;

                SubmitChanges();
            }
        }

        /// <summary>
        /// Handles remove event button click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void BarButtonItemRemoveEventItemClick(object sender, ItemClickEventArgs e)
        {
            if (_schedulerControl.SelectedAppointments.Count > 0)
            {
                if (XtraMessageBox.Show("������� ��������� �������?", "�������������", MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question) == DialogResult.No)
                {
                    return;
                }

                List<Appointment> apts = _schedulerControl.SelectedAppointments.ToList();
                foreach (Appointment apt in apts)
                {
                    _schedulerStorage.Appointments.Remove(apt);
                }
            }
        }

        /// <summary>
        /// Handles remainders form showing event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void SchedulerControlRemindersFormShowing(object sender, RemindersFormEventArgs e)
        {
            if (_reminderForm == null || _reminderForm.IsDisposed)
            {
                _reminderForm = new ReminderForm(this);
                _reminderForm.AddAllertNotifications(e.AlertNotifications);
                _reminderForm.Show();
            }
            else
            {
                _reminderForm.AddAllertNotifications(e.AlertNotifications);
            }
            e.Handled = true;
        }

        /// <summary>
        /// Disables the popup menu.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void SchedulerControlPreparePopupMenu(object sender, PopupMenuShowingEventArgs e)
        {
            e.Menu = null;
        }


        /// <summary>
        /// Handles lessons menu item click.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void LessonsMenuItemClick(object sender, EventArgs e)
        {
            // Check if the parent form is main form.
            if (ParentForm != null && ParentForm is StartForm)
            {
                var item = sender as ToolStripItem;

                // Chick if there is a person to edit.
                if (item != null && item.Tag != null && item.Tag is Person)
                {
                    var startForm = ( (StartForm)ParentForm );

                    var personForm = startForm.OpenOrActivateMdiChild( typeof( PersonsForm ), ( ) => new PersonsForm( ),
                      false ) as PersonsForm;

                    // Open or activate lessons form for desired person.
                    startForm.OpenOrActivateMdiChild(typeof (LessonsForm),
                        () =>
                            new LessonsForm((Person) item.Tag)
                            {CurrentDate = _dateNavigator.DateTime,HierarchyParent = personForm},
                        args: (Person) item.Tag);
                }
            }
        }

        /// <summary>
        /// Handles personel menu item click.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void PersonelMenuItemClick(object sender, EventArgs e)
        {
            // Check if the parent form is main form.
            if (ParentForm != null && ParentForm is StartForm)
            {
                var item = sender as ToolStripItem;

                // Chick if there is a person to edit.
                if (item != null && item.Tag != null && item.Tag is Person)
                {
                    // Open or activate persons form and focus desired person.
                    ((PersonsForm)
                        ((StartForm) ParentForm).OpenOrActivateMdiChild(typeof (PersonsForm), () => new PersonsForm())).
                        FocusedPerson = (Person) item.Tag;
                }
            }
        }

        /// <summary>
        /// Handles calendar form diactivate event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CalendarFormDeactivate(object sender, EventArgs e)
        {
            SubmitChanges();
        }

        /// <summary>
        /// Handles calendar form closing event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        //private void CalendarFormFormClosing( object sender, FormClosingEventArgs e )
        //{
            
        //    if (e.CloseReason != CloseReason.MdiFormClosing && e.CloseReason != CloseReason.WindowsShutDown)
        //    {
        //        var sb = new StringBuilder( );
        //        sb.Append( "����� �������� ���������" );
        //        sb.Append( Environment.NewLine );
        //        sb.Append( "�� ����� �������� �����������." );
        //        sb.Append( Environment.NewLine );
        //        sb.Append( "������� ���������?" );
        //        if (XtraMessageBox.Show( sb.ToString( ), "�������������", MessageBoxButtons.YesNo,
        //            MessageBoxIcon.Question ) == DialogResult.No)
        //        {
        //            e.Cancel = true;
        //        }
        //    }
        //}

        /// <summary>
        /// Handles scheduler control mouse click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void SchedulerControlMouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Right) return;

            if (_schedulerControl.SelectedAppointments.Count > 0)
            {
                foreach (Appointment apt in _schedulerControl.SelectedAppointments)
                {
                    var person = apt.CustomFields["Person"] as Person;

                    if (person != null)
                    {
                        foreach (ToolStripItem item in _personAppointmentContextMenuStrip.Items)
                        {
                            item.Tag = person;
                        }

                        _personAppointmentContextMenuStrip.Show(_schedulerControl, e.X, e.Y);
                    }
                }
            }
        }

        // ReSharper restore MemberCanBeMadeStatic.Local

        #endregion

        #endregion

        public override IEnumerable<ISKOCBCTDNBTIDED> GetHiddenChilds(bool withLazy)
        {
            var hiddenControls = new List<ISKOCBCTDNBTIDED>( );
            hiddenControls.Add( new BarManagerBase( _barManager ) );
            if (withLazy)
            {
                var eventForm = EventForm.GetDummy();
                eventForm.HierarchyParent = this;
                hiddenControls.Add( new ControlInterfaceImplementation(eventForm));
            }
            return hiddenControls;
        }

    }
}
using System;
using System.Linq;
using DevExpress.XtraEditors;
using DevExpress.XtraScheduler;
using DevExpress.XtraScheduler.UI;
using Eureca.Integrator.Common;

namespace Curriculum
{
    public partial class LessonEditForm : FormWithAccessAreas
    {
        private readonly SchedulerControl _control;
        private readonly LessonAppointmentFormController _controller;
        private int _suspendUpdateCount;

        public LessonEditForm(SchedulerControl control, Appointment apt)
            : this(control, apt, true)
        {
        }

        public LessonEditForm(SchedulerControl control, Appointment apt, bool editable)
        {
            _control = control;
            _controller = new LessonAppointmentFormController(control, apt);

            SuspendUpdate();
            InitializeComponent();
            ResumeUpdate();

            UpdateForm(editable);
        }

        private LessonEditForm( )
        {
             InitializeComponent();
        }

        public static LessonEditForm GetDummyForm( )
        {
            return new LessonEditForm();
        }

        protected AppointmentStorage Appointments
        {
            get { return _control.Storage.Appointments; }
        }

        protected bool IsUpdateSuspended
        {
            get { return _suspendUpdateCount > 0; }
        }

        protected void SuspendUpdate()
        {
            _suspendUpdateCount++;
        }

        protected void ResumeUpdate()
        {
            if (_suspendUpdateCount > 0)
                _suspendUpdateCount--;
        }

        private void BtnOkClick(object sender, EventArgs e)
        {
            _controller.Comment = _descriptionMemoEdit.Text;
            _controller.IsImportant = _isImportantCheckEdit.Checked;
            _controller.Duration = (int) _spinEditDuration.Value;
            _controller.Start = new DateTime(dtStart.DateTime.Year, dtStart.DateTime.Month,
                                            dtStart.DateTime.Day, timeStart.Time.Hour, 0, 0);
            _controller.LabelId = Appointments.Labels.IndexOf(_lessonTypeAppointmentLabelEdit.Label);
            if (_isImportantCheckEdit.Checked && _checkEditHasReminder.Checked)
            {
                _controller.TimeBeforeStart = (TimeSpan) _durationEditTimeBeforeStart.EditValue;
            }
            else
            {
                _controller.TimeBeforeStart = null;
            }

            // Save all changes made to the appointment edited in a form.
            _controller.ApplyChanges();
        }

        private void UpdateForm(bool editable)
        {
            SuspendUpdate();
            try
            {
                _disciplineTextEdit.Text = _controller.Subject;

                dtStart.DateTime = _controller.Start.Date;

                timeStart.Time = DateTime.MinValue.AddTicks(_controller.Start.TimeOfDay.Ticks);

                _lessonTypeAppointmentLabelEdit.Storage = new SchedulerStorage();
                _lessonTypeAppointmentLabelEdit.Storage.Appointments.Labels.Clear();
                foreach (LessonType lessonType in Program.DbSingletone.LessonTypes)
                {
                    _lessonTypeAppointmentLabelEdit.Storage.Appointments.Labels.AddRange(
                        _control.Storage.Appointments.Labels.Where(items => items.DisplayName == lessonType.Name).ToArray
                            ());
                }

                _lessonTypeAppointmentLabelEdit.Label = Appointments.Labels[_controller.LabelId];

                // _lessonTypeAppointmentLabelEdit.Storage = control.Storage;

                _descriptionMemoEdit.Text = _controller.Comment;
                _isImportantCheckEdit.Checked = _controller.IsImportant;
                _spinEditDuration.Value = _controller.Duration;

                _checkEditHasReminder.Checked = (_controller.TimeBeforeStart != null);
                _durationEditTimeBeforeStart.EditValue = _controller.TimeBeforeStart;

                // sets visible of controls
                _checkEditHasReminder.Visible = _durationEditTimeBeforeStart.Visible =
                                                _isImportantCheckEdit.Checked;

                // sets readonly mode of controls
                _lessonTypeAppointmentLabelEdit.Properties.ReadOnly = !editable;
                _descriptionMemoEdit.Properties.ReadOnly = !editable;
                dtStart.Properties.ReadOnly = !editable;
                timeStart.Properties.ReadOnly = !editable;
                _spinEditDuration.Properties.ReadOnly = !editable;
            }
            finally
            {
                ResumeUpdate();
            }
        }

        /// <summary>
        /// Handles event [CheckedChanged] from IsImportantCheckEdit.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">event args</param>
        private void IsImportantCheckEditCheckedChanged(object sender, EventArgs e)
        {
            // changes controls state
            _checkEditHasReminder.Visible = _durationEditTimeBeforeStart.Visible =
                                            _isImportantCheckEdit.Checked;
        }

        /// <summary>
        /// Handles event [CheckedChanged] from CheckEditHasReminder.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">event args</param>
        private void CheckEditHasReminderCheckedChanged(object sender, EventArgs e)
        {
            _durationEditTimeBeforeStart.Enabled = _checkEditHasReminder.Checked;
            if (_durationEditTimeBeforeStart.Enabled && _durationEditTimeBeforeStart.EditValue == null)
            {
                _durationEditTimeBeforeStart.EditValue = new TimeSpan(0, 15, 0);
            }
        }
    }

    public class LessonAppointmentFormController : AppointmentFormController
    {
        #region Public properties

        public bool IsImportant
        {
            get { return (bool) EditedAppointmentCopy.CustomFields["IsImportant"]; }
            set { EditedAppointmentCopy.CustomFields["IsImportant"] = value; }
        }

        public bool SourceIsImportant
        {
            get { return (bool) SourceAppointment.CustomFields["IsImportant"]; }
            set { SourceAppointment.CustomFields["IsImportant"] = value; }
        }

        public string Comment
        {
            get { return (string) EditedAppointmentCopy.CustomFields["CommentText"]; }
            set { EditedAppointmentCopy.CustomFields["CommentText"] = value; }
        }

        public string SourceComment
        {
            get { return (string) SourceAppointment.CustomFields["CommentText"]; }
            set { SourceAppointment.CustomFields["CommentText"] = value; }
        }

        /// <summary>
        /// Gets or sets lesson duration.
        /// </summary>
        public int Duration
        {
            get
            {
                if (EditedAppointmentCopy.CustomFields["Duration"] == null)
                {
                    EditedAppointmentCopy.CustomFields["Duration"] = 1;
                }
                return (int) EditedAppointmentCopy.CustomFields["Duration"];
            }
            set { EditedAppointmentCopy.CustomFields["Duration"] = value; }
        }

        /// <summary>
        /// Gets or sets lesson duration of source appointment.
        /// </summary>
        public int SourceDuration
        {
            get
            {
                if (SourceAppointment.CustomFields["Duration"] == null)
                {
                    SourceAppointment.CustomFields["Duration"] = 1;
                }
                return (int) SourceAppointment.CustomFields["Duration"];
            }
            set { SourceAppointment.CustomFields["Duration"] = value; }
        }

        /// <summary>
        /// Gets or sets starting time of reminder.
        /// </summary>
        public TimeSpan? TimeBeforeStart
        {
            get { return (TimeSpan?) EditedAppointmentCopy.CustomFields["TimeBeforeStart"]; }
            set { EditedAppointmentCopy.CustomFields["TimeBeforeStart"] = value; }
        }

        /// <summary>
        /// Gets or sets starting time of reminder of source appointment.
        /// </summary>
        public TimeSpan? SourceTimeBeforeStart
        {
            get { return (TimeSpan?) SourceAppointment.CustomFields["TimeBeforeStart"]; }
            set { SourceAppointment.CustomFields["TimeBeforeStart"] = value; }
        }

        #endregion

        public LessonAppointmentFormController(SchedulerControl control, Appointment apt) : base(control, apt)
        {
        }

        public override bool IsAppointmentChanged()
        {
            if (base.IsAppointmentChanged())
            {
                return true;
            }
            return (Comment != SourceComment) || (IsImportant != SourceIsImportant) ||
                   (SourceDuration != Duration) || (TimeBeforeStart != SourceTimeBeforeStart);
        }

        protected override void ApplyCustomFieldsValues()
        {
            SourceComment = Comment;
            SourceIsImportant = IsImportant;
            SourceDuration = Duration;
            SourceAppointment.Start = EditedAppointmentCopy.Start;
            SourceAppointment.LabelId = EditedAppointmentCopy.LabelId;
            SourceTimeBeforeStart = TimeBeforeStart;
        }
    }
}
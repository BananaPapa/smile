namespace Curriculum
{
    partial class EventForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._descriptionMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.label1 = new System.Windows.Forms.Label();
            this._timeEnd = new DevExpress.XtraEditors.TimeEdit();
            this._timeStart = new DevExpress.XtraEditors.TimeEdit();
            this._dateEnd = new DevExpress.XtraEditors.DateEdit();
            this._dateStart = new DevExpress.XtraEditors.DateEdit();
            this.lblEnd = new System.Windows.Forms.Label();
            this.lblStart = new System.Windows.Forms.Label();
            this._eventNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.lblSubject = new System.Windows.Forms.Label();
            this._btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this._btnOK = new DevExpress.XtraEditors.SimpleButton();
            this._isAllDayCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this._durationEditTimeBeforeStart = new DevExpress.XtraScheduler.UI.DurationEdit();
            this._checkEditHasReminder = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this._descriptionMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._timeEnd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._timeStart.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._dateEnd.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._dateEnd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._dateStart.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._dateStart.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._eventNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._isAllDayCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._durationEditTimeBeforeStart.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._checkEditHasReminder.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // _descriptionMemoEdit
            // 
            this._descriptionMemoEdit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._descriptionMemoEdit.EditValue = "����������";
            this._descriptionMemoEdit.Location = new System.Drawing.Point(102, 143);
            this._descriptionMemoEdit.Name = "_descriptionMemoEdit";
            this._descriptionMemoEdit.Size = new System.Drawing.Size(327, 107);
            this._descriptionMemoEdit.TabIndex = 40;
            this._descriptionMemoEdit.Tag = "����������#r";
            this._descriptionMemoEdit.UseOptimizedRendering = true;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(14, 145);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 18);
            this.label1.TabIndex = 39;
            this.label1.Tag = "����������#r";
            this.label1.Text = "����������:";
            // 
            // _timeEnd
            // 
            this._timeEnd.EditValue = new System.DateTime(2006, 3, 28, 0, 0, 0, 0);
            this._timeEnd.Location = new System.Drawing.Point(204, 64);
            this._timeEnd.Name = "_timeEnd";
            this._timeEnd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this._timeEnd.Size = new System.Drawing.Size(80, 20);
            this._timeEnd.TabIndex = 34;
            this._timeEnd.Tag = "���������#r";
            // 
            // _timeStart
            // 
            this._timeStart.EditValue = new System.DateTime(2006, 3, 28, 0, 0, 0, 0);
            this._timeStart.Location = new System.Drawing.Point(204, 38);
            this._timeStart.Name = "_timeStart";
            this._timeStart.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this._timeStart.Size = new System.Drawing.Size(80, 20);
            this._timeStart.TabIndex = 31;
            this._timeStart.Tag = "������#r";
            // 
            // _dateEnd
            // 
            this._dateEnd.EditValue = new System.DateTime(2008, 6, 27, 0, 0, 0, 0);
            this._dateEnd.Location = new System.Drawing.Point(102, 64);
            this._dateEnd.Name = "_dateEnd";
            this._dateEnd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._dateEnd.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this._dateEnd.Size = new System.Drawing.Size(96, 20);
            this._dateEnd.TabIndex = 32;
            this._dateEnd.Tag = "���������#r";
            // 
            // _dateStart
            // 
            this._dateStart.EditValue = new System.DateTime(2008, 6, 27, 0, 0, 0, 0);
            this._dateStart.Location = new System.Drawing.Point(102, 38);
            this._dateStart.Name = "_dateStart";
            this._dateStart.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._dateStart.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this._dateStart.Size = new System.Drawing.Size(96, 20);
            this._dateStart.TabIndex = 30;
            this._dateStart.Tag = "������#r";
            // 
            // lblEnd
            // 
            this.lblEnd.Location = new System.Drawing.Point(12, 67);
            this.lblEnd.Name = "lblEnd";
            this.lblEnd.Size = new System.Drawing.Size(73, 18);
            this.lblEnd.TabIndex = 38;
            this.lblEnd.Tag = "���������#r";
            this.lblEnd.Text = "���������:";
            // 
            // lblStart
            // 
            this.lblStart.Location = new System.Drawing.Point(12, 41);
            this.lblStart.Name = "lblStart";
            this.lblStart.Size = new System.Drawing.Size(56, 18);
            this.lblStart.TabIndex = 37;
            this.lblStart.Tag = "������#r";
            this.lblStart.Text = "������:";
            // 
            // _eventNameTextEdit
            // 
            this._eventNameTextEdit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._eventNameTextEdit.EditValue = "";
            this._eventNameTextEdit.Location = new System.Drawing.Point(102, 12);
            this._eventNameTextEdit.Name = "_eventNameTextEdit";
            this._eventNameTextEdit.Size = new System.Drawing.Size(327, 20);
            this._eventNameTextEdit.TabIndex = 29;
            this._eventNameTextEdit.Tag = "������������#r";
            // 
            // lblSubject
            // 
            this.lblSubject.Location = new System.Drawing.Point(12, 14);
            this.lblSubject.Name = "lblSubject";
            this.lblSubject.Size = new System.Drawing.Size(84, 18);
            this.lblSubject.TabIndex = 33;
            this.lblSubject.Tag = "������������#r";
            this.lblSubject.Text = "������������:";
            // 
            // _btnCancel
            // 
            this._btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._btnCancel.Location = new System.Drawing.Point(354, 264);
            this._btnCancel.Name = "_btnCancel";
            this._btnCancel.Size = new System.Drawing.Size(75, 27);
            this._btnCancel.TabIndex = 36;
            this._btnCancel.Tag = "������#r";
            this._btnCancel.Text = "������";
            this._btnCancel.Click += new System.EventHandler(this.BtnCancelClick);
            // 
            // _btnOK
            // 
            this._btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this._btnOK.Location = new System.Drawing.Point(273, 264);
            this._btnOK.Name = "_btnOK";
            this._btnOK.Size = new System.Drawing.Size(75, 27);
            this._btnOK.TabIndex = 35;
            this._btnOK.Tag = "���������#rc";
            this._btnOK.Text = "���������";
            this._btnOK.Click += new System.EventHandler(this.BtnOkClick);
            // 
            // _isAllDayCheckEdit
            // 
            this._isAllDayCheckEdit.Location = new System.Drawing.Point(100, 90);
            this._isAllDayCheckEdit.Name = "_isAllDayCheckEdit";
            this._isAllDayCheckEdit.Properties.Caption = "���� ����";
            this._isAllDayCheckEdit.Size = new System.Drawing.Size(176, 19);
            this._isAllDayCheckEdit.TabIndex = 41;
            this._isAllDayCheckEdit.Tag = "���� ���� ����#r";
            this._isAllDayCheckEdit.CheckedChanged += new System.EventHandler(this.IsAllDayCheckEditCheckedChanged);
            // 
            // _durationEditTimeBeforeStart
            // 
            this._durationEditTimeBeforeStart.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._durationEditTimeBeforeStart.Enabled = false;
            this._durationEditTimeBeforeStart.Location = new System.Drawing.Point(204, 115);
            this._durationEditTimeBeforeStart.Name = "_durationEditTimeBeforeStart";
            this._durationEditTimeBeforeStart.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._durationEditTimeBeforeStart.Properties.ShowEmptyItem = false;
            this._durationEditTimeBeforeStart.Size = new System.Drawing.Size(225, 20);
            this._durationEditTimeBeforeStart.TabIndex = 42;
            this._durationEditTimeBeforeStart.Tag = "�����������#r";
            // 
            // _checkEditHasReminder
            // 
            this._checkEditHasReminder.Location = new System.Drawing.Point(100, 115);
            this._checkEditHasReminder.Name = "_checkEditHasReminder";
            this._checkEditHasReminder.Properties.Caption = "��������� ��";
            this._checkEditHasReminder.Size = new System.Drawing.Size(98, 19);
            this._checkEditHasReminder.TabIndex = 43;
            this._checkEditHasReminder.Tag = "�����������#r";
            this._checkEditHasReminder.CheckedChanged += new System.EventHandler(this.CheckEditHasReminderCheckedChanged);
            // 
            // EventForm
            // 
            this.AcceptButton = this._btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._btnCancel;
            this.ClientSize = new System.Drawing.Size(441, 303);
            this.Controls.Add(this._checkEditHasReminder);
            this.Controls.Add(this._durationEditTimeBeforeStart);
            this.Controls.Add(this._isAllDayCheckEdit);
            this.Controls.Add(this._descriptionMemoEdit);
            this.Controls.Add(this.label1);
            this.Controls.Add(this._timeEnd);
            this.Controls.Add(this._timeStart);
            this.Controls.Add(this._dateEnd);
            this.Controls.Add(this._dateStart);
            this.Controls.Add(this.lblEnd);
            this.Controls.Add(this.lblStart);
            this.Controls.Add(this._eventNameTextEdit);
            this.Controls.Add(this.lblSubject);
            this.Controls.Add(this._btnCancel);
            this.Controls.Add(this._btnOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EventForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "�������#r";
            this.Text = "�������";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.EventFormFormClosing);
            ((System.ComponentModel.ISupportInitialize)(this._descriptionMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._timeEnd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._timeStart.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._dateEnd.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._dateEnd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._dateStart.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._dateStart.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._eventNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._isAllDayCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._durationEditTimeBeforeStart.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._checkEditHasReminder.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.MemoEdit _descriptionMemoEdit;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TimeEdit _timeEnd;
        private DevExpress.XtraEditors.TimeEdit _timeStart;
        private DevExpress.XtraEditors.DateEdit _dateEnd;
        private DevExpress.XtraEditors.DateEdit _dateStart;
        private System.Windows.Forms.Label lblEnd;
        private System.Windows.Forms.Label lblStart;
        private DevExpress.XtraEditors.TextEdit _eventNameTextEdit;
        private System.Windows.Forms.Label lblSubject;
        private DevExpress.XtraEditors.SimpleButton _btnCancel;
        private DevExpress.XtraEditors.SimpleButton _btnOK;
        private DevExpress.XtraEditors.CheckEdit _isAllDayCheckEdit;
        private DevExpress.XtraScheduler.UI.DurationEdit _durationEditTimeBeforeStart;
        private DevExpress.XtraEditors.CheckEdit _checkEditHasReminder;
    }
}
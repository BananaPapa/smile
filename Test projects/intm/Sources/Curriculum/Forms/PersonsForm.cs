using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Common;
using System.Data.Linq;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Curriculum.Reports;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using Eureca.Integrator.Common;
using log4net;

namespace Curriculum
{
    /// <summary>
    /// Represents form for persons.
    /// </summary>
    public partial class PersonsForm : FormWithAccessAreas, IAliasesHolder
    {
        private ILog Log = LogManager.GetLogger(typeof (PersonsForm));

        /// <summary>
        /// Persons report key.
        /// </summary>
        private const string ReportKey = "Persons";

        /// <summary>
        /// Previously focused person.
        /// </summary>
        private Person _previousPerson;

        /// <summary>
        /// Whether displayed data is read-only.
        /// </summary>
        /// <see cref="ReadOnly"/>
        private bool _readonly;

        /// <summary>
        /// Initializes new instance of persons form.
        /// </summary>
        public PersonsForm()
        {
            InitializeComponent();
            
            Load+=PersonsForm_Load;
            
        }

        /// <summary>
        /// Get or sets whether displayed data is read-only.
        /// </summary>
        private bool ReadOnly
        {
            get { return _readonly; }
            set
            {
                var gridControls = new[]
                                       {
                                           _personsGridControl,
                                           _phonesGridControl,
                                           _lettersGridControl
                                       };

                foreach (GridControl control in gridControls)
                {
                    PermissionType gridPermission = PermissionType.None;
                    if (!_permissionsDic.TryGetValue(control.Name, out gridPermission))
                        gridPermission = PermissionType.Add | PermissionType.Change | PermissionType.Delete | PermissionType.ReadOnly;
                // Set readonly property of all in-place editors.
                    foreach (RepositoryItem item in control.RepositoryItems)
                    {
                        item.ReadOnly = value || !gridPermission.HasFlag(PermissionType.Change);
                    }

                    // Enable/disable emdedded navigator edit buttons.
                    control.EmbeddedNavigator.Buttons.Edit.Enabled = !value && gridPermission.HasFlag(PermissionType.Change);
                    control.EmbeddedNavigator.Buttons.Append.Enabled = !value && gridPermission.HasFlag(PermissionType.Add);
                    control.EmbeddedNavigator.Buttons.Remove.Enabled = !value && gridPermission.HasFlag(PermissionType.Delete);
                }

                PermissionType personGridPermission = PermissionType.None;
                if (!_permissionsDic.TryGetValue( _personsGridControl.Name, out personGridPermission ))
                    personGridPermission = PermissionType.Add | PermissionType.Change | PermissionType.Delete |
                                     PermissionType.ReadOnly;

                // Enable/disable person ship remove appointment button.
                _personShipGridLookUpEdit.Buttons[0].Enabled = _personShipGridLookUpEdit.Buttons[1].Enabled = !value && personGridPermission.HasFlag( PermissionType.Change );

                // Enable/disable person notes RTF edit control.
                _personNotesRtfEditControl.ReadOnly = value || !personGridPermission.HasFlag( PermissionType.Change );

                Person focusedPerson = FocusedPerson;

                // Enable/disable professional credentials controls.
                _professionalLoginTextEdit.Properties.ReadOnly =
                    _professionalPasswordTextEdit.Properties.ReadOnly =
                    value || ( focusedPerson != null && focusedPerson.ProfessionalUserUniqueId.HasValue ) || !personGridPermission.HasFlag( PermissionType.Change );
                _saveProfessionalCredentialsButton.Enabled = !value && personGridPermission.HasFlag( PermissionType.Change );

                // Toggle edit lock button image.
                _personsGridControl.EmbeddedNavigator.CustomButtons[0].ImageIndex = value || personGridPermission == PermissionType.ReadOnly ? 1 : 0;

                _readonly = value;
            }
        }

        /// <summary>
        /// Gets or sets currently focused person.
        /// </summary>
        [Browsable(false)]
        public Person FocusedPerson
        {
            get { return _personsLayoutView.GetFocusedRow() as Person; }
            set
            {
                if (value != null && _personsLayoutView.RowCount > 0)
                {
                    int rowHandle = FindPersonRowHandle(value);

                    if (_personsLayoutView.IsDataRow(rowHandle))
                    {
                        // Focus found row.
                        _personsLayoutView.FocusedRowHandle = rowHandle;
                    }
                    else
                    {
                        // Clear filter, sorting and selection.
                        _personsLayoutView.ClearColumnsFilter();
                        _personsLayoutView.ClearSorting();
                        _personsLayoutView.ClearSelection();

                        rowHandle = FindPersonRowHandle(value);

                        if (_personsLayoutView.IsDataRow(rowHandle))
                        {
                            // Focus found row.
                            _personsLayoutView.FocusedRowHandle = rowHandle;
                        }
                    }
                }
            }
        }

        #region IAliasesHolder Members

        /// <summary>
        /// Applies aliases for the current form.
        /// </summary>
        public void ApplyAliases()
        {
            try
            {
                Aliase.ApplyAlias( "Ships", a => _personShipGridLookUpEditView.ViewCaption = a );
                Aliase.ApplyAlias( "ContactInfo",
                                 a => _contactInfoGroup.Text = _contactInfoGroup.CustomizationFormText = a);
                Aliase.ApplyAlias( "QualInfo",
                                 a => _qualificationInfoGroup.Text = _qualificationInfoGroup.CustomizationFormText = a);
                Aliase.ApplyAlias( "CadreInfo", a => _qadreInfoGroup.Text = _qadreInfoGroup.CustomizationFormText = a );
                Aliase.ApplyAlias( "Appointment",
                                 a => _appointmentGroup.Text = _appointmentGroup.CustomizationFormText = a);
                Aliase.ApplyAlias( "AppointmentOrder",
                                 a => _appointmentOrderItem.Text = _appointmentOrderItem.CustomizationFormText = a);
                Aliase.ApplyAlias( "RequestOrder", a => _requestOrderItem.Text = _requestOrderItem.CustomizationFormText = a );
                Aliase.ApplyAlias( "Ship", a => _personShipLayoutViewColumn.Caption = a );
                Aliase.ApplyAlias( "River", a => _shipRiverGridColumn.Caption = a );
                Aliase.ApplyAlias( "ShipLine", a => _shipLineGridColumn.Caption = a );
                Aliase.ApplyAlias( "ChildNickName", a => _personChildNickNameLayoutViewColumn.Caption = a );
                Aliase.ApplyAlias( "Status", a => _personStatusLayoutViewColumn.Caption = a );
                Aliase.ApplyAlias( "Qualification", a => _personQualificationLayoutViewColumn.Caption = a );
                Aliase.ApplyAlias( "Education", a => _personEducationLayoutViewColumn.Caption = a );
                Aliase.ApplyAlias( "Degree", a => _personDegreeLayoutViewColumn.Caption = a );
                Aliase.ApplyAlias( "Position", a => _personPositionLayoutViewColumn.Caption = a );
                Aliase.ApplyAlias( "Department", a => _personDepartmentLayoutViewColumn.Caption = a );
                Aliase.ApplyAlias( "Affiliation", a => _personAffiliationLayoutViewColumn.Caption = a );
                Aliase.ApplyAlias( "TripCount", a => _personTripCountLayoutViewColumn.Caption = a );
                Aliase.ApplyAlias( "Period", a => _personPeriodLayoutViewColumn.Caption = a );
                //Program.FindAndHideControls(this);
            }
            catch (DbException ex)
            {
                Program.HandleDbException(ex);
            }
        }

        #endregion

        /// <summary>
        /// Finds person's row handle.
        /// </summary>
        /// <param name="person">Person to find row for.</param>
        /// <returns>Row handle of found row or invalid row handle.</returns>
        private int FindPersonRowHandle(Person person)
        {
            int firstRowHandle = _personsLayoutView.GetRowHandle(0);

            // Search row by ship id starting from first row.
            return _personsLayoutView.LocateByValue(firstRowHandle, _personIdLayoutViewColumn,
                                                    person.PersonId);
        }

        /// <summary>
        /// Initializes persons grid control data source.
        /// </summary>
        private void InitPersonsDataSource()
        {
            _personsGridControl.DataSource = from person in Program.DbSingletone.GetDataSource(typeof (Person)).OfType<Person>()
                                             orderby person.Name
                                             select person;
        }

        /// <summary>
        /// Refreshes data source.
        /// </summary>
        private void RefreshPersonsDataSource()
        {
            Program.DbSingletone.RefreshDataSource(typeof (Ship), typeof (Letter), typeof (PersonPhone), typeof (Person));

            _personsLayoutView.RefreshData();
        }

        /// <summary>
        /// Initializes ships grid control data source.
        /// </summary>
        private void InitShipsDataSource()
        {
            _personShipGridLookUpEdit.DataSource = from ship in Program.DbSingletone.GetDataSource(typeof (Ship)).OfType<Ship>()
                                                   orderby ship.Name
                                                   select ship;
        }

        /// <summary>
        /// Refreshes data source.
        /// </summary>
        private void RefreshShipDataSource()
        {
            Program.DbSingletone.RefreshDataSource(typeof (Ship));
            _personShipGridLookUpEditView.RefreshData();
        }

        /// <summary>
        /// Fills all combo box editors with corresponding values from database.
        /// </summary>
        private void FillComboBoxEditors()
        {
            try
            {
                NameOwnerTools.FillComboBox<Statuse>(_personStatusComboBox);
                NameOwnerTools.FillComboBox<Qualification>(_personQualificationComboBox);
                NameOwnerTools.FillComboBox<Education>(_personEducationComboBox);
                NameOwnerTools.FillComboBox<Degree>(_personDegreeComboBox);
                NameOwnerTools.FillComboBox<Position>(_personPositionComboBox);
                NameOwnerTools.FillComboBox<Department>(_personDepartmentComboBox);
                NameOwnerTools.FillComboBox<Affiliation>(_personAffiliationComboBox);
                NameOwnerTools.FillComboBox<Category>(_personCategoryComboBox);
                NameOwnerTools.FillComboBox<PhoneType>(_phoneTypeComboBox);
                NameOwnerTools.FillComboBox<Addressee>(_letterAddresseeComboBox);
            }
            catch (DbException ex)
            {
                Program.HandleDbException(ex);
            }
        }

        /// <summary>
        /// Updates content of controls placed on details panel corresponding to the currently focused person.
        /// </summary>
        /// <param name="person">Person to fill its details.</param>
        private void UpdateDetailsPanel(Person person)
        {
            if (person != null)
            {
                _phonesGridControl.DataSource = from personPhone in Program.DbSingletone.PersonPhones
                                                where personPhone.PersonId == person.PersonId
                                                select personPhone;

                _lettersGridControl.DataSource = from letter in Program.DbSingletone.Letters
                                                 where letter.PersonId == person.PersonId
                                                 select letter;

                _personPropertiesGridControl.DataSource = (from result in Program.DbSingletone.GetPersonProperties(person.PersonId)
                                                          select result).ToList();

                // Set professional credentials.
                _professionalLoginTextEdit.Text = person.ProfessionalLogin;
                _professionalPasswordTextEdit.Text = person.ProfessionalPassword;
                _showProfessionalPasswordCheckButton.Checked = false;

                _professionalLoginTextEdit.Properties.ReadOnly =
                    _professionalPasswordTextEdit.Properties.ReadOnly =
                    person.ProfessionalUserUniqueId.HasValue || ReadOnly;
                _createProfessionalUserSimpleButton.Enabled =
                    _saveProfessionalCredentialsButton.Enabled = !person.ProfessionalUserUniqueId.HasValue;
                _deleteProfessionalUserSimpleButton.Enabled = !_createProfessionalUserSimpleButton.Enabled;

                // Set person notes RTF.
                _personNotesRtfEditControl.Rtf = person.Notes;

                // Hide lessons control with label.
                _showLessonsLinkLabel.BringToFront();

                // Show details if they are hidden.
                if (_splitContainerControl.PanelVisibility != SplitPanelVisibility.Both)
                {
                    _splitContainerControl.PanelVisibility = SplitPanelVisibility.Both;
                }
            }
            else
            {
                _phonesGridControl.DataSource =
                    _lettersGridControl.DataSource = null;

                _professionalLoginTextEdit.Text =
                    _professionalPasswordTextEdit.Text =
                    _personNotesRtfEditControl.Rtf = String.Empty;

                _lessonsControl.Person = null;
                _showLessonsLinkLabel.BringToFront();

                // Hide details if they are shown.
                if (_splitContainerControl.PanelVisibility != SplitPanelVisibility.Panel1)
                {
                    _splitContainerControl.PanelVisibility = SplitPanelVisibility.Panel1;
                }
            }
        }

        /// <summary>
        /// Recount important dates on main form.
        /// </summary>
        private void RecountImportantDates()
        {
            var form = ParentForm as StartForm;
            if (form != null)
            {
                form.CountImportantEvents(ImportantEvent.PersonDate);
            }
        }

        /// <summary>
        /// Offers to save details changes.
        /// </summary>
        /// <param name="person">Person to save details for.</param>
        private void OfferToSaveDetailsChanges(Person person)
        {
            if (person != null)
            {
                // Check if person notes was modified but not saved.
                if (_personNotesRtfEditControl.Modified)
                {
                    if (XtraMessageBox.Show(
                        "� ������� ���� ������� ���������." + Environment.NewLine +
                        "��������� ���������?", "�������������", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                        == DialogResult.Yes)
                    {
                        SavePersonNotes(person);
                    }
                    else
                    {
                        _personNotesRtfEditControl.Rtf = person.Notes;
                    }
                }

                // Check if professional credentials were modified but not saved.
                if ((person.ProfessionalLogin != _professionalLoginTextEdit.Text &&
                     !(String.IsNullOrEmpty(person.ProfessionalLogin) &&
                       String.IsNullOrEmpty(_professionalLoginTextEdit.Text)
                      )
                    )
                    ||
                    (person.ProfessionalPassword != _professionalPasswordTextEdit.Text &&
                     !(String.IsNullOrEmpty(person.ProfessionalPassword) &&
                       String.IsNullOrEmpty(_professionalPasswordTextEdit.Text)
                      )
                    ))
                {
                    if (XtraMessageBox.Show(
                        "��������� ������� ������ ��� \"������������\" ���� ��������." + Environment.NewLine +
                        "��������� ���������?", "�������������", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                        == DialogResult.Yes)
                    {
                        SaveProfessionalCredentials(person);
                    }
                    else
                    {
                        _professionalLoginTextEdit.Text = person.ProfessionalLogin;
                        _professionalPasswordTextEdit.Text = person.ProfessionalPassword;
                    }
                }
            }
        }

        /// <summary>
        /// Saves professional credentials for the specified person.
        /// </summary>
        /// <param name="person">Person to save credetials for.</param>
        private void SaveProfessionalCredentials(Person person)
        {
            string login = _professionalLoginTextEdit.Text.Trim();
            string password = _professionalPasswordTextEdit.Text.Trim();

            person.ProfessionalLogin = String.IsNullOrEmpty(login) ? null : login;
            person.ProfessionalPassword = String.IsNullOrEmpty(password) ? null : password;

            try
            {
                Program.DbSingletone.SubmitChanges();
            }
            catch (DbException ex)
            {
                Program.HandleDbException(ex);
            }
        }

        /// <summary>
        /// Saves notes for the specified person.
        /// </summary>
        /// <param name="person">Person to save notes for.</param>
        private void SavePersonNotes(Person person)
        {
            person.Notes = _personNotesRtfEditControl.Rtf;

            try
            {
                Program.DbSingletone.SubmitChanges();

                _personNotesRtfEditControl.ResetModifiedProperty();
            }
            catch (DbException ex)
            {
                Program.HandleDbException(ex);
            }
        }

        /// <summary>
        /// Creates professional user.
        /// </summary>
        /// <param name="person">Corresponding person.</param>
        /// <returns>True if successfully; otherwise, false.</returns>
        private bool CreateProfessionalUser(Person person, string password)
        {
            if (!person.ProfessionalUserUniqueId.HasValue)
            {
                try
                {
                    Guid? guid = null;

                    Program.ProfessionalDb.CreateUser(person.FirstName, person.MiddleName, person.LastName,
                                                      person.BirthDate,
                                                      password, true, null, ref guid);
                    string login = _professionalLoginTextEdit.Text.Trim( );


                    if (guid.HasValue && guid != Guid.Empty)
                    {
                        person.ProfessionalUserUniqueId = guid;
                        if( String.IsNullOrEmpty(login) )
                        {
                             XtraMessageBox.Show(this, "� �������� ������ ������� ������ ����� ������������ ������ ��� (�.�.�.)!",  "�������� ������� ������",  MessageBoxButtons.OK, MessageBoxIcon.Information);
                             person.ProfessionalLogin = person.Name;  
                        }
                        else
                            person.ProfessionalLogin = login;  
                            
                        person.ProfessionalPassword = password;
                        Program.DbSingletone.SubmitChanges();

                        return true;
                    }
                }
                catch (DbException ex)
                {
                    Program.HandleDbException(ex);
                }
            }

            return false;
        }

        /// <summary>
        /// Deletes professional user.
        /// </summary>
        /// <param name="person">Corresponding person.</param>
        /// <param name="exception">Thrown exception.</param>
        /// <returns>True if successfully; otherwise, false.</returns>
        private static bool DeleteProfessionalUser(Person person, out DbException exception)
        {
            try
            {
                exception = null;
                return Program.ProfessionalDb.DeleteUser(person.ProfessionalUserUniqueId) == 1;
            }
            catch (DbException ex)
            {
                exception = ex;
                return false;
            }
        }

        /// <summary>
        /// Handles form load event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void PersonsForm_Load(object sender, EventArgs e)
        {
            if(Site!= null && Site.DesignMode)
                return;

            ApplyAliases( );
            FillComboBoxEditors();
            InitShipsDataSource();
            InitPersonsDataSource();
            // Bind save person notes button to modified property of RTF edit control.
            _savePersonNotesSimpleButton.DataBindings.Add("Enabled", _personNotesRtfEditControl, "Modified");
            
            // Data is not initially editable.
            ReadOnly = true;
        }

        /// <summary>
        /// Handles persons layout view focused row changed event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void PersonsLayoutView_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            Person person = FocusedPerson;

            string errorText;
            if (_previousPerson != null && !ValidatePerson(_previousPerson, out errorText))
            {
                //If _previous person is not validated then discard data context changes.
                Program.DiscardDataChanges();
            }

            if (_previousPerson != null && !_previousPerson.Equals( person ))
            {
                OfferToSaveDetailsChanges(_previousPerson);
            }

            UpdateDetailsPanel(person);

            _previousPerson = person;
        }

        /// <summary>
        /// Handles person ship grid look ip edit button click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void PersonShipGridLookUpEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            Person person = FocusedPerson;

            if (person != null)
            {
                // Remove person appointment.
                person.Ship = null;
                _personsLayoutView.RefreshEditor(true);
            }
        }

        /// <summary>
        /// Handles save person notes simple button click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void SavePersonNotesSimpleButton_Click(object sender, EventArgs e)
        {
            Person person = FocusedPerson;

            if (person != null)
            {
                SavePersonNotes(person);
            }
        }

        /// <summary>
        /// Handles show professional password check button checked changed event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void ShowProfessionalPasswordCheckButton_CheckedChanged(object sender, EventArgs e)
        {
            // Hide or show password.
            //_professionalPasswordTextEdit.MaskBox.UseSystemPasswordChar = !_showProfessionalPasswordCheckButton.Checked;
            _professionalPasswordTextEdit.Properties.PasswordChar = _showProfessionalPasswordCheckButton.Checked
                ? '\0'
                : '*';
        }

        /// <summary>
        /// Handles save professional credentials button click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void SaveProfessionalCredentialsButton_Click(object sender, EventArgs e)
        {
            Person person = FocusedPerson;

            if (person != null)
            {
                SaveProfessionalCredentials(person);
            }
        }

        /// <summary>
        /// Handles show lessons link lable link click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void ShowLessonsLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Person person = FocusedPerson;

            if (person != null)
            {
                _lessonsControl.Person = person;
                _lessonsControl.BringToFront();
            }
        }

        /// <summary>
        /// Handles edit lessons simple button click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void EditLessonsSimpleButton_Click(object sender, EventArgs e)
        {
            // Save current person changes.
            _personsLayoutView.UpdateCurrentRow();

            Person person = FocusedPerson;

            if (person != null)
            {
                // Recomend to fill category of person, teaching start and examination dates.
                if (person.Category == null || person.TeachingStartDate == null)
                {
                    DialogResult result =
                        XtraMessageBox.Show(
                            "����� �������������� �������� ����� ������������� ��������� ��������� ����������, � ����� ���� ������ ��������." +
                            Environment.NewLine + "������ ���������?", "��������������", MessageBoxButtons.YesNoCancel,
                            MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);

                    switch (result)
                    {
                        case DialogResult.Yes:

                            // Switch off read-only mode.
                            if (ReadOnly)
                            {
                                ReadOnly = false;
                            }

                            // Focus first empty value column.                                        
                            _personsLayoutView.FocusedColumn = person.Category == null
                                                                   ? _personCategoryLayoutViewColumn
                                                                   : _personTeachingStartDateLayoutViewColumn;
                            ;

                            // Show editor.
                            _personsLayoutView.ShowEditor();

                            return;

                        case DialogResult.Cancel:
                            return;
                    }
                }

                if (ParentForm != null && ParentForm is StartForm)
                {
                    // Open or activate lessons form for desired person.
                    ((StartForm) ParentForm).OpenOrActivateMdiChild(typeof (LessonsForm),
                                                                   () => new LessonsForm(person){HierarchyParent = this},
                                                                   args: person);
                }
            }
        }

        /// <summary>
        /// Handles letters grid view focused row changed event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void LettersGridView_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            string errorText;
            if (_prevLetter != null && !ValidateLetter(_prevLetter, out errorText))
            {
                //If _previous letter is not validated then discard data context changes.
                Program.DiscardDataChanges();
            }

            if (_lettersGridView.IsGroupRow(_lettersGridView.FocusedRowHandle))
            {
                _prevLetter = null;
            }
            else
            {
                _prevLetter = _lettersGridView.GetRow(_lettersGridView.FocusedRowHandle) as Letter;
            }
        }

        /// <summary>
        /// Handles produce report bar button item click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void ProduceReportBarButtonItemItemClick(object sender, ItemClickEventArgs e)
        {
            ProduceReport();
        }

        /// <summary>
        /// Produces common report of persons.
        /// </summary>
        private void ProduceReport()
        {
            var persons = new List<Person>();

            for (int i = 0; i < _personsLayoutView.RowCount; ++i)
            {
                var person = _personsLayoutView.GetRow(i) as Person;

                if (person != null)
                {
                    persons.Add(person);
                }
            }

            // Create report.
            try
            {
                string reportPath = Reporter.GetTemplateDocument(ReportKey, this);
                if (!string.IsNullOrEmpty(reportPath))
                {
                    new MarqueeProgressForm(
                        "������������ ������",
                        progressForm =>
                            {
                                var wordManager = new WordManager();

                                try
                                {
                                    wordManager.Open(reportPath);

                                    wordManager.CreatePersonsTable(progressForm, persons, 1);

                                    wordManager.SaveAs(reportPath);
                                }
                                finally
                                {
                                    wordManager.KillWinWord();
                                }
                            }).ShowDialog();

                    Application.DoEvents();


                    // Save and open template.
                    Reporter.SaveAndOpenReport(fileName =>
                                                   {
                                                       try
                                                       {
                                                           if (System.IO.File.Exists( fileName ))
                                                               System.IO.File.Delete( fileName );
                                                           System.IO.File.Move( reportPath, fileName );
                                                           return null;
                                                       }
                                                       catch (Exception ex)
                                                       {
                                                           return ex;
                                                       }
                                                   });
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                XtraMessageBox.Show(ex.Message, "����������");
            }
        }

        /// <summary>
        /// Formats status for message.
        /// </summary>
        /// <param name="person">Person to pick status.</param>
        /// <returns></returns>
        private static string FormatStatusForMessage(Person person)
        {
            return String.Format("������: {0}!", person.Statuse != null ? person.Statuse.ToString() : "�� ������");
        }

        /// <summary>
        /// Handles create professional user simple button click.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void CreateProfessionalUserSimpleButtonClick(object sender, EventArgs e)
        {
            Person person = FocusedPerson;

            if (!person.ProfessionalUserUniqueId.HasValue)
            {
                string password = _professionalPasswordTextEdit.Text.Trim();

                if (!String.IsNullOrEmpty(password))
                {
                    if (person.Statuse == null || !person.Statuse.CreateProbationer)
                    {
                        if (XtraMessageBox.Show(this,
                                                "�� �������, ��� ������ ������� ������� ������ � ��� \"������������\"?" +
                                                Environment.NewLine + FormatStatusForMessage(person),
                                                "�������������",
                                                MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                        {
                            return;
                        }
                    }

                    if (CreateProfessionalUser(person, password))
                    {
                        UpdateDetailsPanel(person);

                        XtraMessageBox.Show(this, "������� ������ ������� �������!", "�������� ������� ������",
                                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    XtraMessageBox.Show(this, "������� ������!", "�������� ������� ������",
                                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    _professionalPasswordTextEdit.Focus();
                }
            }
            else
            {
                XtraMessageBox.Show(this, "������� ������ ��� ������������!", "�������� ������� ������",
                                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        /// <summary>
        /// Handles delete professional user simple button click.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void DeleteProfessionalUserSimpleButtonClick(object sender, EventArgs e)
        {
            Person person = FocusedPerson;

            if (XtraMessageBox.Show(this,
                                    "�� �������, ��� ������ ������� ������� ������ � ��� \"������������\"?" +
                                    Environment.NewLine + "����� ������� ����� � ��� ���������� ������������!" +
                                    ((person.Statuse != null && person.Statuse.CreateProbationer)
                                         ? Environment.NewLine + FormatStatusForMessage(person)
                                         : String.Empty),
                                    "�������������",
                                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
            {
                return;
            }


            if (person.ProfessionalUserUniqueId.HasValue)
            {
                PerformProfessionalUserDeletion(person);
            }
            else
            {
                XtraMessageBox.Show(this, "������� ������ �� ������������!", "�������� ������� ������",
                                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        /// <summary>
        /// Performs professional user deletion.
        /// </summary>
        /// <param name="person">Corresponding person.</param>
        private void PerformProfessionalUserDeletion(Person person)
        {
            try
            {
                DbException exception;

                bool success = DeleteProfessionalUser(person, out exception);

                if (success || exception == null)
                {
                    person.ProfessionalUserUniqueId = null;
                    person.ProfessionalLogin = null;
                    person.ProfessionalPassword = null;

                    Program.DbSingletone.SubmitChanges();

                    XtraMessageBox.Show(this,
                                        success
                                            ? "������� ������ ������� �������!"
                                            : "�� ������� ��������������� ������� ������� ������, ��� ��� ��� ����������� � �� \"������������\".",
                                        "�������� ������� ������",
                                        MessageBoxButtons.OK, MessageBoxIcon.Information);

                    UpdateDetailsPanel(person);
                }
                else
                {
                    throw exception;
                }
            }
            catch (DbException ex)
            {
                Program.HandleDbException(ex);

                RefreshData();
            }
        }

        /// <summary>
        /// Performs person lessons deletion.
        /// </summary>
        /// <param name="person">Corresponding person.</param>
        private void PerformPersonLessonsDeletion(Person person)
        {
            try
            {
                Program.DbSingletone.Lessons.DeleteAllOnSubmit(from lesson in Program.DbSingletone.Lessons
                                                     where lesson.PersonId == person.PersonId
                                                     select lesson);

                Program.DbSingletone.SubmitChanges();

                XtraMessageBox.Show(this,
                                    "��� ������� �������� ����� ������� ���������� ������� �������!",
                                    "�������� �������� �����",
                                    MessageBoxButtons.OK, MessageBoxIcon.Information);

                UpdateDetailsPanel(person);
            }
            catch (DbException ex)
            {
                Program.HandleDbException(ex);

                RefreshData();
            }
        }

        #region Init New Row Events

        /// <summary>
        /// Handles persons layout view init new row event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void PersonsLayoutView_InitNewRow(object sender, InitNewRowEventArgs e)
        {
            var person = _personsLayoutView.GetRow(e.RowHandle) as Person;

            if (person != null)
            {
                // Set today as default person birth date.
                person.BirthDate = DateTime.Today;
            }
        }

        /// <summary>
        /// Handles phones grid view init new row event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void PhonesGridView_InitNewRow(object sender, InitNewRowEventArgs e)
        {
            var phone = _phonesGridView.GetRow(e.RowHandle) as PersonPhone;

            if (phone != null)
            {
                // Link phone being added to the currently focused person.
                phone.Person = FocusedPerson;
            }
        }

        /// <summary>
        /// Handles letters grid view init new row event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void LettersGridView_InitNewRow(object sender, InitNewRowEventArgs e)
        {
            var letter = _lettersGridView.GetRow(e.RowHandle) as Letter;

            if (letter != null)
            {
                // Link letter being added to the currently focused person.
                letter.Person = FocusedPerson;

                // Set today as default letter date.
                letter.Date = DateTime.Today;
            }
        }

        #endregion

        #region Validation Events

        #region Editor Level

        /// <summary>
        /// Handles persons layout view validating editor event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void PersonsLayoutView_ValidatingEditor(object sender,
                                                        BaseContainerValidateEditorEventArgs e)
        {
            // Get field name.
            string fieldName;
            try
            {
                fieldName = _personsLayoutView.FocusedColumn.FieldName;
            }
            catch (NullReferenceException)
            {
                return;
            }

            // Check date values.
            var date = e.Value as DateTime?;

            Person person;
            if (date != null && (person = FocusedPerson) != null)
            {
                try
                {
                    switch (fieldName)
                    {
                        case "TeachingStartDate":
                            if (date >= person.ExaminationDate || date >= person.DepartureDate)
                            {
                                e.Valid = false;
                                e.ErrorText =
                                    "���� ������ �������� �� ����� ���� ����� ��� ��������� � ������ �������� � ������.";
                            }
                            else if (Program.DbSingletone.Lessons.Any(l => l.PersonId == person.PersonId &&
                                                                 l.Datetime.Date < date))
                            {
                                e.Valid = false;
                                e.ErrorText = "� ������� ����� ��� ��������� ������� �� ��������� ����.";
                            }
                            return;

                        case "ExaminationDate":

                            if (date <= person.TeachingStartDate || date >= person.DepartureDate)
                            {
                                e.Valid = false;
                                e.ErrorText =
                                    "���� �������� �� ����� ���� ������ ��� ��������� � ����� ������ �������� � ���� ����� ��� ��������� � ����� ������.";
                            }
                            else if (Program.DbSingletone.Lessons.Any(l => l.PersonId == person.PersonId &&
                                                                 l.Datetime.Date > date))
                            {
                                e.Valid = false;
                                e.ErrorText = "� ������� ����� ��� ��������� ������� ����� ��������� ����.";
                            }
                            return;

                        case "DepartureDate":

                            if (date <= person.TeachingStartDate || date <= person.ExaminationDate)
                            {
                                e.Valid = false;
                                e.ErrorText =
                                    "���� ������ �� ����� ���� ������ ��� ��������� � ������ ������ �������� � ��������.";
                            }
                            return;
                    }
                }
                catch (DbException ex)
                {
                    Program.HandleDbException(ex);
                }
            }

            // Check string values.
            var text = e.Value as string;
            if (text == null) return;

            if (text.Length == 0)
            {
                e.Value = null;
            }
            else
            {
                try
                {
                    // Get name owner objects by name or create new instances.
                    switch (fieldName)
                    {
                        case "Qualification":
                            try
                            {
                                e.Value = Program.DbSingletone.Qualifications.Single(q => q.Name == text);
                            }
                            catch (InvalidOperationException)
                            {
                                e.Value = new Qualification {Name = text};
                            }
                            break;

                        case "Education":
                            try
                            {
                                e.Value = Program.DbSingletone.Educations.Single(ed => ed.Name == text);
                            }
                            catch (InvalidOperationException)
                            {
                                e.Value = new Education {Name = text};
                            }
                            break;

                        case "Degree":
                            try
                            {
                                e.Value = Program.DbSingletone.Degrees.Single(d => d.Name == text);
                            }
                            catch (InvalidOperationException)
                            {
                                e.Value = new Degree {Name = text};
                            }
                            break;

                        case "Position":
                            try
                            {
                                e.Value = Program.DbSingletone.Positions.Single(p => p.Name == text);
                            }
                            catch (InvalidOperationException)
                            {
                                e.Value = new Position {Name = text};
                            }
                            break;

                        case "Department":
                            try
                            {
                                e.Value = Program.DbSingletone.Departments.Single(d => d.Name == text);
                            }
                            catch (InvalidOperationException)
                            {
                                e.Value = new Department {Name = text};
                            }
                            break;

                        case "Affiliation":
                            try
                            {
                                e.Value = Program.DbSingletone.Affiliations.Single(a => a.Name == text);
                            }
                            catch (InvalidOperationException)
                            {
                                e.Value = new Affiliation {Name = text};
                            }
                            break;

                        case "Status":
                            try
                            {
                                e.Value = Program.DbSingletone.Statuses.Single(s => s.Name == text);
                            }
                            catch (InvalidOperationException)
                            {
                                e.Value = new Statuse {Name = text};
                            }
                            break;

                        case "Category":
                            try
                            {
                                e.Value = Program.DbSingletone.Categories.Single(c => c.Name == text);
                            }
                            catch (InvalidOperationException)
                            {
                                e.Value = new Category {Name = text};
                            }
                            break;
                    }
                }
                catch (DbException ex)
                {
                    Program.HandleDbException(ex);
                }
            }
        }

        /// <summary>
        /// Handles phones grid view validating editor event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void PhonesGridView_ValidatingEditor(object sender, BaseContainerValidateEditorEventArgs e)
        {
            // Only string value typed be user is to be validated.
            var text = e.Value as string;
            if (text == null) return;

            // Get field name.
            string fieldName;
            try
            {
                fieldName = _phonesGridView.FocusedColumn.FieldName;
            }
            catch (NullReferenceException)
            {
                return;
            }

            if (text.Length == 0)
            {
                e.Value = null;
            }
            else
            {
                try
                {
                    switch (fieldName)
                    {
                            // Get name owner objects by name or create new instances.
                        case "PhoneType":
                            try
                            {
                                e.Value = Program.DbSingletone.PhoneTypes.Single(pt => pt.Name == text);
                            }
                            catch (InvalidOperationException)
                            {
                                e.Value = new PhoneType {Name = text};
                            }
                            break;
                    }
                }
                catch (DbException ex)
                {
                    Program.HandleDbException(ex);
                }
            }
        }

        /// <summary>
        /// Handles letters grid view validating editor event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void LettersGridView_ValidatingEditor(object sender, BaseContainerValidateEditorEventArgs e)
        {
            // Only string value typed be user is to be validated.
            var text = e.Value as string;
            if (text == null) return;

            // Get field name.
            string fieldName;
            try
            {
                fieldName = _lettersGridView.FocusedColumn.FieldName;
            }
            catch (NullReferenceException)
            {
                return;
            }

            if (text.Length == 0)
            {
                e.Value = null;
            }
            else
            {
                try
                {
                    switch (fieldName)
                    {
                            // Get name owner objects by name or create new instances.
                        case "Addressee":
                            try
                            {
                                e.Value = Program.DbSingletone.Addressees.Single(a => a.Name == text);
                            }
                            catch (InvalidOperationException)
                            {
                                e.Value = new Addressee {Name = text};
                            }
                            break;
                    }
                }
                catch (DbException ex)
                {
                    Program.HandleDbException(ex);
                }
            }
        }

        #endregion

        #region Row Level

        /// <summary>
        /// Handles persons layout view validate row event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void PersonsLayoutView_ValidateRow(object sender, ValidateRowEventArgs e)
        {
            var person = e.Row as Person;
            if (person == null) return;

            try
            {
                if (String.IsNullOrEmpty(person.Name))
                {
                    e.ErrorText = "��� �� ������ ���� ������.";
                }
                else if (
                    Program.DbSingletone.Persons.Any(
                        p => p.Name == person.Name && p.BirthDate == person.BirthDate && p.PersonId != person.PersonId))
                {
                    e.ErrorText = "������ � ����� ��� � ����� �������� ��� ����������.";
                }
            }
            catch (DbException ex)
            {
                Program.HandleDbException(ex);
            }

            // If there is an error show message.
            if (!String.IsNullOrEmpty(e.ErrorText))
            {
                e.ErrorText += Environment.NewLine;
                e.Valid = false;
            }
        }

        /// <summary>
        /// Handles phones grid view validate row event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void PhonesGridView_ValidateRow(object sender, ValidateRowEventArgs e)
        {
            var phone = e.Row as PersonPhone;
            if (phone == null) return;

            if (String.IsNullOrEmpty(phone.Number))
            {
                e.ErrorText = "����� �� ������ ���� ������.";
            }

            // If there is an error show message.
            if (!String.IsNullOrEmpty(e.ErrorText))
            {
                e.ErrorText += Environment.NewLine;
                e.Valid = false;
            }
        }

        /// <summary>
        /// Handles letters grid view validate row event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void LettersGridView_ValidateRow(object sender, ValidateRowEventArgs e)
        {
            var letter = e.Row as Letter;
            if (letter == null) return;

            if (letter.Addressee == null)
            {
                e.ErrorText = "������� ������ ���� ������.";
            }
            else if (String.IsNullOrEmpty(letter.Code))
            {
                e.ErrorText = "����� �� ������ ���� ������.";
            }

            // If there is an error show message.
            if (!String.IsNullOrEmpty(e.ErrorText))
            {
                e.ErrorText += Environment.NewLine;
                e.Valid = false;
            }
        }

        #endregion

        #endregion

        #region Row Update Events

        /// <summary>
        /// Handles persons layout view row updated event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void PersonsLayoutView_RowUpdated(object sender, RowObjectEventArgs e)
        {
            var person = e.Row as Person;

            if (person != null)
            {
                try
                {
                    Statuse other = null;

                    try
                    {
                        other = ( from p in Program.DbSingletone.Persons
                                  where p.PersonId == person.PersonId
                                  select p.Statuse ).Single( );
                    }
                    catch (Exception exception)
                    {
                        Log.Error( exception.ToString( ) );
                    }
                    // New status with CreateProbationer = false
                    if (person.ProfessionalUserUniqueId.HasValue && person.Statuse != null &&
                        !person.Statuse.CreateProbationer
                        && other != null && !other.CreateProbationer
                        )
                    {
                        if (XtraMessageBox.Show( this,
                                                "������ ��������� �� " + person.Statuse + "." + Environment.NewLine +
                                                "������� �� �������������� ������� ������ � ��� \"������������\", � ����� ��� ������� �������� ����� ������� ����������?" +
                                                Environment.NewLine +
                                                "����� ������� ����� ��� ���������� ������������!",
                                                "�������������",
                                                MessageBoxButtons.YesNo, MessageBoxIcon.Question ) ==
                            DialogResult.Yes)
                        {
                            PerformProfessionalUserDeletion( person );
                            PerformPersonLessonsDeletion( person );
                        }
                    }

                    Program.DbSingletone.SubmitChanges( );
                }
                catch (DbException ex)
                {
                    Program.HandleDbException( ex );
                }

                NameOwnerTools.UpdateComboBox(person.Statuse, _personStatusComboBox);
                NameOwnerTools.UpdateComboBox(person.Qualification, _personQualificationComboBox);
                NameOwnerTools.UpdateComboBox(person.Education, _personEducationComboBox);
                NameOwnerTools.UpdateComboBox(person.Degree, _personDegreeComboBox);
                NameOwnerTools.UpdateComboBox(person.Position, _personPositionComboBox);
                NameOwnerTools.UpdateComboBox(person.Department, _personDepartmentComboBox);
                NameOwnerTools.UpdateComboBox(person.Affiliation, _personAffiliationComboBox);
                NameOwnerTools.UpdateComboBox(person.Category, _personCategoryComboBox);

                RecountImportantDates();
            }
        }

        /// <summary>
        /// Handles phones grid view row updated event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void PhonesGridView_RowUpdated(object sender, RowObjectEventArgs e)
        {
            var phone = e.Row as PersonPhone;

            if (phone != null)
            {
                try
                {
                    Program.DbSingletone.SubmitChanges();
                }
                catch (DbException ex)
                {
                    Program.HandleDbException(ex);
                }

                NameOwnerTools.UpdateComboBox(phone.PhoneType, _phoneTypeComboBox);
            }
        }

        /// <summary>
        /// Handles letters grid view row updated event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void LettersGridView_RowUpdated(object sender, RowObjectEventArgs e)
        {
            var letter = e.Row as Letter;

            if (letter != null)
            {
                try
                {
                    Program.DbSingletone.SubmitChanges();
                }
                catch (DbException ex)
                {
                    Program.HandleDbException(ex);
                }

                NameOwnerTools.UpdateComboBox(letter.Addressee, _letterAddresseeComboBox);
            }
        }

        #endregion

        #region Embedded Navigators' Events

        /// <summary>
        /// Gets user's confirmation for deletion.
        /// </summary>
        /// <returns>true if deletion is confirmed; otherwise, false.</returns>
        private static bool ConfirmDeletion()
        {
            // Show question message box and return result.
            return XtraMessageBox.Show(
                "�� �������, ��� ������ ������� ���������� ������?",
                "������������� ��������",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Question,
                MessageBoxDefaultButton.Button2
                       ) == DialogResult.Yes;
        }

        /// <summary>
        /// Handles persons grid control embedded navigator button click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void PersonsGridControl_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Remove:

                    // Selected row will not be removed.
                    e.Handled = true;

                    Person person = FocusedPerson;

                    if (person != null && ConfirmDeletion())
                    {
                        if (person.ProfessionalUserUniqueId.HasValue)
                        {
                            if (XtraMessageBox.Show(this,
                                                    "������� �� � �������������� ������� ������ � ��� \"������������\"?" +
                                                    Environment.NewLine +
                                                    "����� ������� ����� � ��� ���������� ������������!",
                                                    "�������������",
                                                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==
                                DialogResult.Yes)
                            {
                                PerformProfessionalUserDeletion(person);
                            }
                        }

                        Program.DbSingletone.Persons.DeleteOnSubmit(person);

                        try
                        {
                            Program.DbSingletone.SubmitChanges();

                            RecountImportantDates();

                            // Selected row will be removed.
                            e.Handled = false;
                        }
                        catch (DbException ex)
                        {
                            Program.HandleDbException(ex);
                        }
                    }
                    break;
                case NavigatorButtonType.Custom:

                    var tag = e.Button.Tag as string;

                    if (tag != null)
                    {
                        tag = tag.Split( '#' )[0];
                        switch (tag)
                        {
                            case "EditLock":
                                // Toggle read-only property.
                                ReadOnly = !ReadOnly;
                                break;

                            case "Refresh":
                                RefreshData();
                                break;

                            case "Report":

                                _popupMenu.ShowPopup(MousePosition);

                                break;
                        }
                    }
                    break;
            }
        }

        private void RefreshData()
        {
            try
            {
                Program.DbSingletone.SubmitChanges();
                Program.DbSingletone.Refresh(RefreshMode.OverwriteCurrentValues, Program.DbSingletone.Persons);
            }
            catch (DbException ex)
            {
                Program.HandleDbException(ex);
            }
            finally
            {
                Person focusedPerson = FocusedPerson;

                FillComboBoxEditors();
                RefreshShipDataSource();
                RefreshPersonsDataSource();

                FocusedPerson = focusedPerson;

                UpdateDetailsPanel(FocusedPerson);
                RecountImportantDates();
            }
        }

        /// <summary>
        /// Handles phones grid control embedded navigator button click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void PhonesGridControl_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Append:

                    e.Handled = true;

                    if (FocusedPerson != null)
                    {
                        int currentRow = _phonesGridView.FocusedRowHandle;
                        if (currentRow < 0)
                        {
                            currentRow = _phonesGridView.GetDataRowHandleByGroupRowHandle(currentRow);
                        }

                        _phonesGridView.AddNewRow();

                        if (_phonesGridView.SortInfo.GroupCount == 0) return;

                        // Initialize group values.
                        foreach (GridColumn groupColumn in _phonesGridView.GroupedColumns)
                        {
                            object val = _phonesGridView.GetRowCellValue(currentRow, groupColumn);
                            _phonesGridView.SetRowCellValue(_phonesGridView.FocusedRowHandle, groupColumn, val);
                        }

                        _phonesGridView.ShowEditor();
                    }
                    break;

                case NavigatorButtonType.Remove:

                    // Selected row will not be removed.
                    e.Handled = true;

                    var phone = _phonesGridView.GetFocusedRow() as PersonPhone;

                    if (phone != null && ConfirmDeletion())
                    {
                        Program.DbSingletone.PersonPhones.DeleteOnSubmit(phone);

                        try
                        {
                            Program.DbSingletone.SubmitChanges();

                            // Selected row will be removed.
                            e.Handled = false;
                        }
                        catch (DbException ex)
                        {
                            Program.HandleDbException(ex);
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// Handles letters grid control embedded navigator button click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void LettersGridControl_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Append:

                    e.Handled = true;

                    if (FocusedPerson != null)
                    {
                        int currentRow = _lettersGridView.FocusedRowHandle;
                        if (currentRow < 0)
                        {
                            currentRow = _lettersGridView.GetDataRowHandleByGroupRowHandle(currentRow);
                        }

                        _lettersGridView.AddNewRow();

                        if (_lettersGridView.SortInfo.GroupCount == 0) return;

                        // Initialize group values.
                        foreach (GridColumn groupColumn in _lettersGridView.GroupedColumns)
                        {
                            object val = _lettersGridView.GetRowCellValue(currentRow, groupColumn);
                            _lettersGridView.SetRowCellValue(_lettersGridView.FocusedRowHandle, groupColumn, val);
                        }

                        _lettersGridView.ShowEditor();
                    }
                    break;

                case NavigatorButtonType.Remove:

                    // Selected row will not be removed.
                    e.Handled = true;

                    var letter = _lettersGridView.GetFocusedRow() as Letter;

                    if (letter != null && ConfirmDeletion())
                    {
                        Program.DbSingletone.Letters.DeleteOnSubmit(letter);

                        try
                        {
                            Program.DbSingletone.SubmitChanges();

                            // Selected row will be removed.
                            e.Handled = false;
                        }
                        catch (DbException ex)
                        {
                            Program.HandleDbException(ex);
                        }
                    }
                    break;
            }
        }

        #endregion

        #region Code was added by V.Kovalev

        /// <summary>
        /// Previous selected letter in the letters grid view.
        /// </summary>
        private Letter _prevLetter;

        /// <summary>
        /// Previous selected phone in the phones grid view.
        /// </summary>
        private PersonPhone _prevPhone;

        /// <summary>
        /// Validate person.
        /// </summary>
        /// <param name="person">Person to validate.</param>
        /// <param name="errorText">Error text if occured.</param>
        /// <returns>True if person is valid; otherwise, false.</returns>
        private static bool ValidatePerson(Person person, out string errorText)
        {
            bool isValid = true;
            errorText = null;

            if (person == null)
            {
                errorText = "������ ��������� �� ����� ���� ��������." + Environment.NewLine;
                isValid = false;
            }
            else
            {
                try
                {
                    if (String.IsNullOrEmpty(person.Name))
                    {
                        errorText = "��� �� ������ ���� ������." + Environment.NewLine;
                        isValid = false;
                    }
                    else if (
                        Program.DbSingletone.Persons.Any(
                            p =>
                            p.Name == person.Name && p.BirthDate == person.BirthDate && p.PersonId != person.PersonId))
                    {
                        errorText = "������ � ����� ��� � ����� �������� ��� ����������." + Environment.NewLine;
                        isValid = false;
                    }
                }
                catch (DbException ex)
                {
                    Program.HandleDbException(ex);
                    isValid = false;
                }
            }

            return isValid;
        }

        /// <summary>
        /// Validate the phone.
        /// </summary>
        /// <param name="phone">Phone to validate.</param>
        /// <param name="errorText">Error text if occured.</param>
        /// <returns>True if phone is valid; otherwise, false.</returns>
        private static bool ValidatePhone(PersonPhone phone, out string errorText)
        {
            bool isValid = true;
            errorText = null;

            if (phone == null)
            {
                errorText += "������ ����� �� ����� ���� ��������." + Environment.NewLine;
                isValid = false;
            }
            else
            {
                if (String.IsNullOrEmpty(phone.Number))
                {
                    errorText += "����� �� ������ ���� ������." + Environment.NewLine;
                    isValid = false;
                }
            }

            return isValid;
        }

        /// <summary>
        /// Validate the letter.
        /// </summary>
        /// <param name="letter">Letter to validate.</param>
        /// <param name="errorText">Error text if occured.</param>
        /// <returns>True if letter is valid; otherwise, false.</returns>
        private static bool ValidateLetter(Letter letter, out string errorText)
        {
            bool isValid = true;
            errorText = null;

            if (letter == null)
            {
                errorText += "������ �� ����� ���� ���������." + Environment.NewLine;
                isValid = false;
            }
            else
            {
                if (letter.Addressee == null)
                {
                    errorText = "������� ������ ���� ������.";
                    isValid = false;
                }
                else if (String.IsNullOrEmpty(letter.Code))
                {
                    errorText = "����� �� ������ ���� ������.";
                    isValid = false;
                }
            }

            return isValid;
        }

        /// <summary>
        /// Handles phones grid view focused row changed event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void PhonesGridView_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            string errorText;
            if (_prevPhone != null && !ValidatePhone(_prevPhone, out errorText))
            {
                //If _previous phone is not validated then discard data context changes.
                Program.DiscardDataChanges();
            }

            if (_phonesGridView.IsGroupRow(_phonesGridView.FocusedRowHandle))
            {
                _prevPhone = null;
            }
            else
            {
                _prevPhone = _phonesGridView.GetRow(_phonesGridView.FocusedRowHandle) as PersonPhone;
            }
        }

        #endregion

        private bool GetPropertyValueDate(object row, out GetPersonPropertiesResult propertyValue, out Type valueType)
        {
            valueType = null;

            propertyValue = row as GetPersonPropertiesResult;
            if (propertyValue == null) return false;

            try
            {
                valueType = Type.GetType(propertyValue.Type);
            }
            catch
            {
                return false;
            }

            return true;
        }

        private void PersonPropertiesGridViewCustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.Column.Name != _personPropertyValueGridColumn.Name) return;

            GetPersonPropertiesResult propertyValue;
            Type valueType;

            if(!GetPropertyValueDate(e.RowHandle, out propertyValue, out valueType)) return;

            if (valueType == typeof(bool))
            {
                e.RepositoryItem = new RepositoryItemCheckEdit();
            }
        }

        private void PersonPropertiesGridViewCustomUnboundColumnData(object sender, CustomColumnDataEventArgs e)
        {
            if (e.Column.Name != _personPropertyValueGridColumn.Name) return;

            GetPersonPropertiesResult propertyValue;
            Type valueType;

            if (!GetPropertyValueDate(e.Row, out propertyValue, out valueType)) return;

            if(e.IsGetData)
            {
                if(valueType == typeof(Boolean))
                {
                    bool booleanValue = false;

                    if(!String.IsNullOrEmpty(propertyValue.Value))
                    {
                        Boolean.TryParse(propertyValue.Value, out booleanValue);
                    }

                    e.Value = booleanValue;
                }
                else
                {
                    e.Value = propertyValue.Value;
                }
            }
            else if(e.IsSetData)
            {
                if(valueType == typeof(bool))
                {
                    propertyValue.Value = e.Value == null || (e.Value is bool && !((bool) e.Value))
                                              ? null
                                              : Boolean.TrueString;
                }
                else
                {
                    string stringValue = e.Value == null ? null : e.Value.ToString();

                    propertyValue.Value = !String.IsNullOrEmpty(stringValue) ? stringValue : null;
                }

                var person = FocusedPerson;

                var existingValue =
                    Program.DbSingletone.PersonPropertyValues.FirstOrDefault(
                        p => p.PersonId == person.PersonId && p.PropertyId == propertyValue.PropertyId);

                if (propertyValue.Value == null)
                {
                    if (existingValue != null)
                    {
                        Program.DbSingletone.PersonPropertyValues.DeleteOnSubmit(existingValue);
                        Program.DbSingletone.SubmitChanges();
                    }
                }
                else
                {
                    if(existingValue != null)
                    {
                        existingValue.Value = propertyValue.Value;
                    }
                    else
                    {
                        Program.DbSingletone.PersonPropertyValues.InsertOnSubmit(new PersonPropertyValue
                                                                           {
                                                                               PersonId = person.PersonId,
                                                                               PropertyId = propertyValue.PropertyId,
                                                                               Value = propertyValue.Value
                                                                           });
                    }

                    Program.DbSingletone.SubmitChanges();
                }
            }
        }

        public override IEnumerable<ISKOCBCTDNBTIDED> GetHiddenChilds(bool withLazy)
        {
            var hidden = new List<ISKOCBCTDNBTIDED>( );

            if (withLazy)
            {
                var reportTemplateDummyForm = Reporter.GetReportTemplateDummyForm( );
                var supportAccessAreasForm = reportTemplateDummyForm as ISupportAccessAreas;
                supportAccessAreasForm.HierarchyParent = Program.StartForm;
                hidden.Add( new ControlInterfaceImplementation( reportTemplateDummyForm ) );

                var lessonsForm = new LessonsForm( null ) { HierarchyParent = this };
                hidden.Add( new ControlInterfaceImplementation( lessonsForm ) );
            }

            return hidden;
        }
    }
}
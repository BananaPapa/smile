using System;
using System.Data.Common;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace Curriculum
{
    /// <summary>
    /// Represents authorization form.
    /// </summary>
    internal partial class AuthForm : XtraForm
    {
        /// <summary>
        /// Previously authorized user before form was opened.
        /// </summary>
        private readonly User _previousUser;

        /// <summary>
        /// Initializes new instance of authorization form.
        /// </summary>
        public AuthForm(User currentUser)
        {
            InitializeComponent();

            _previousUser = currentUser;
        }

        /// <summary>
        /// Gets currently typed password.
        /// </summary>
        private string Password
        {
            get { return _passwordTextEdit.Text.Trim(); }
        }

        /// <summary>
        /// Gets currently selected user.
        /// </summary>
        public User User
        {
            get { return _user�omboBoxEdit.SelectedItem as User; }
        }

        /// <summary>
        /// Checks if the form can submit.
        /// </summary>
        private void CheckTheFormCanSubmit()
        {
            _signInSimpleButton.Enabled = User != null && Password.Length > 0;
        }

        /// <summary>
        /// Handles form load event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void AuthForm_Load(object sender, EventArgs e)
        {
            _passwordTextEdit.MaskBox.UseSystemPasswordChar = true;

            try
            {
                NameOwnerTools.FillComboBox<User>(_user�omboBoxEdit);
            }
            catch (DbException)
            {
                XtraMessageBox.Show("��� ��������� � ���� ������ �������� ������." + Environment.NewLine +
                                    "��������� ����������� ������� ��� ������ � ��������� ����������.",
                                    "������ ���� ������",
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            if (_user�omboBoxEdit.Properties.Items.Count > 0)
            {
                if (_previousUser != null)
                {
                    if (_user�omboBoxEdit.Properties.Items.Contains(_previousUser))
                    {
                        _user�omboBoxEdit.SelectedItem = _previousUser;
                    }
                }
                else
                {
                    _user�omboBoxEdit.SelectedIndex = 0;
                    _user�omboBoxEdit.DeselectAll();
                }
            }
        }

        /// <summary>
        /// Handles form shown event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void AuthForm_Shown(object sender, EventArgs e)
        {
            Activate();

            _passwordTextEdit.Focus();
        }

        /// <summary>
        /// Handles sign in simple button click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void SignInSimpleButton_Click(object sender, EventArgs e)
        {
            User user = User;
            string password = Password;

            if (user != null && password.Length > 0)
            {
                if (user.PasswordHash.ToArray().IsIdenticalTo(password.ComputeSHA1Hash(Encoding.UTF8)))
                {
                    DialogResult = DialogResult.OK;
                    Close();
                }
                else
                {
                    XtraMessageBox.Show("�������� ������.", "������ �����������", MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);

                    _passwordTextEdit.Text = String.Empty;
                    _passwordTextEdit.Focus();
                }
            }
        }

        /// <summary>
        /// Handles user combo box edit selected index changed event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void User�omboBoxEdit_SelectedIndexChanged(object sender, EventArgs e)
        {
            CheckTheFormCanSubmit();
        }

        /// <summary>
        /// Handles password text edit edit value changed event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void PasswordTextEdit_EditValueChanged(object sender, EventArgs e)
        {
            CheckTheFormCanSubmit();
        }
    }
}
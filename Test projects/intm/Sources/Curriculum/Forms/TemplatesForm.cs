﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Curriculum.Settings;
using DevExpress.XtraEditors;
using Eureca.Integrator.Common;
using NHibernate.Linq.Functions;

namespace Curriculum
{
    public partial class TemplatesForm : FormWithAccessAreas
    {
        private readonly NameOwnersControl _nameOwnersControl = new NameOwnersControl(typeof (Template)) { Dock = DockStyle.Fill, Tag = "..\\Шаблоны курсов#r"};

        public TemplatesForm()
        {
            InitializeComponent();
            _nameOwnersControl.HierarchyParent = this;
        }

        private void TemplatesFormLoad(object sender, EventArgs e)
        {
            _nameOwnersControl.FocusedObjectChanged += FocusedObjectChanged;
            Controls.Add(_nameOwnersControl);
            _nameOwnersControl.BringToFront();
            _editTemplateCoursesPanelControl.SendToBack();
        }

        private void FocusedObjectChanged(object sender, EventArgs e)
        {
            _editTemplateCoursesSimpleButton.Enabled = _nameOwnersControl.FocusedObject as Template != null;
        }

        private void EditTemplateCoursesSimpleButtonClick(object sender, EventArgs e)
        {
            var template = _nameOwnersControl.FocusedObject as Template;

            if (template != null)
            {
                // Check if the parent form is main form.
                if (MdiParent != null && MdiParent is StartForm)
                {
                    // Open or activate courses form for desired ship.
                    ((StartForm) MdiParent).OpenOrActivateMdiChild(typeof (TemplateCoursesForm),
                                                                  () => new TemplateCoursesForm(template){HierarchyParent = this},
                                                                 args: template);
                }
            }
        }

        public override IEnumerable<ISKOCBCTDNBTIDED> GetHiddenChilds(bool withLazy)
        {
            var hidden = new List<ISKOCBCTDNBTIDED>();

            hidden.Add(new ControlInterfaceImplementation(_nameOwnersControl));
            if (withLazy)
            {
                var templateCoursesForm = new TemplateCoursesForm( null ){HierarchyParent = this};
                hidden.Add(new ControlInterfaceImplementation(templateCoursesForm));
            }
            return hidden;
        }
    }
}
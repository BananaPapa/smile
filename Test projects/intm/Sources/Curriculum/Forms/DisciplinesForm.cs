using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Linq;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using Eureca.Integrator.Common;
using log4net;
using log4net.Config;

namespace Curriculum
{
    /// <summary>
    /// Represents form for disciplines viewing and editing.
    /// </summary>
    public partial class DisciplinesForm : FormWithAccessAreas
    {
        private ILog Log = LogManager.GetLogger(typeof (DisciplinesForm));

        #region Fields

        /// <summary>
        /// Current grouped columns values.
        /// </summary>
        private readonly Dictionary<GridColumn, object> _groupColumnValues = new Dictionary<GridColumn, object>();

        /// <summary>
        /// Previous discipline.
        /// </summary>
        private Discipline _prevDiscipline;

        #endregion

        #region Private properties

        /// <summary>
        /// The discipline focused in the _disciplineGridView
        /// </summary>
        private Discipline CurrentDiscipline
        {
            get
            {
                if (_disciplinesGridView.IsGroupRow(_disciplinesGridView.FocusedRowHandle))
                {
                    return null;
                }
                return _disciplinesGridView.GetRow(_disciplinesGridView.FocusedRowHandle) as Discipline;
            }
        }

        #endregion

        #region Private methods

        #region Fills data directory comboboxes

        private void FillUnitsComboBox()
        {
            _unitsComboBox.Items.Clear();
            NameOwnerTools.FillComboBox<Unit>(_unitsComboBox);
        }

        private void FillPartsComboBox()
        {
            _partsComboBox.Items.Clear();
            var discipline = _disciplinesGridView.GetRow(_disciplinesGridView.FocusedRowHandle) as Discipline;
            if (discipline != null && discipline.Unit != null)
            {
                Program.DbSingletone.Refresh(RefreshMode.OverwriteCurrentValues, Program.DbSingletone.Parts);
                IQueryable<Part> parts = from part in Program.DbSingletone.Parts
                                         where part.Unit == discipline.Unit
                                         select part;
                foreach (Part item in parts)
                {
                    _partsComboBox.Items.Add(item);
                }
            }
        }

        private void FillTestsLookupEdit()
        {
            _testsLookUpEdit.DataSource = null;
            IQueryable<vAllTest> tests = Program.ProfessionalDb.vAllTests
                .OrderBy(x => x.MethodName)
                .Select(x => x);
            _testsLookUpEdit.DataSource = tests;
        }

        #endregion

        #region Updates data directory comboboxes

        private void UpdateUnitsComboBox(Unit item)
        {
            NameOwnerTools.UpdateComboBox(item, _unitsComboBox);
        }

        private void UpdatePartsComboBox(Part item)
        {
            NameOwnerTools.UpdateComboBox(item, _partsComboBox);
        }

        #endregion

        #region Forms event handlers

        /// <summary>
        /// Handles disciplines from load event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DisciplinesFormLoad(object sender, EventArgs e)
        {
            FillPartsComboBox();
            FillUnitsComboBox();
            FillTestsLookupEdit();

            RefreshDisciplinesGridView();

            _saveDescriptionButton.DataBindings.Add("Enabled", _descriptionRtfEditControl, "Modified");

            _disciplinesGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = _disciplinesGridView.Editable;
            _disciplinesGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = _disciplinesGridView.Editable;
            _disciplinesGridControl.EmbeddedNavigator.Buttons.Append.Enabled = _disciplinesGridView.Editable;

            // Designer doesn't save further settings.
            _attachmentsFolderButtonEdit.Buttons[0].ToolTip = "�������";
            _attachmentsFolderButtonEdit.Buttons[1].ToolTip = "���������";
            _attachmentsFolderButtonEdit.Buttons[1].Kind = ButtonPredefines.Delete;

            _testsLookUpEdit.Buttons[0].ToolTip = "�������";
            _testsLookUpEdit.Buttons[1].ToolTip = "���������";
            _testsLookUpEdit.Buttons[1].Kind = ButtonPredefines.Delete;
        }

        /// <summary>
        /// Handles disciplines form diactivate event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DisciplinesForm_Deactivate(object sender, EventArgs e)
        {
            //Discard all non-submitted changes.
            Program.DiscardDataChanges();
        }

        /// <summary>
        /// Handles disciplines form closing event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DisciplinesFormFormClosing(object sender, FormClosingEventArgs e)
        {
            OfferToSaveDetailsChanges(CurrentDiscipline);
        }

        #endregion

        #region Disciplines grid control and view event handlers

        /// <summary>
        /// Handles discipline view focused column changed event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void DisciplineGridViewFocusedColumnChanged(object sender, FocusedColumnChangedEventArgs e)
        {
            if (e.PrevFocusedColumn == _disciplinesGridView.Columns["Unit"])
            {
                FillPartsComboBox();
            }
        }

        /// <summary>
        /// Handles disciplines grid view row updated event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void DisciplinesGridViewRowUpdated(object sender, RowObjectEventArgs e)
        {
            var discipline = e.Row as Discipline;
            if (discipline != null)
            {
                SubmitChanges();

                UpdatePartsComboBox(discipline.Part);
                UpdateUnitsComboBox(discipline.Unit);

                FillPartsComboBox();
                FillUnitsComboBox();
                FillTestsLookupEdit();
            }
        }

        /// <summary>
        /// Handles disciplines grid view row validate event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void DisciplinesGridView_ValidateRow(object sender, ValidateRowEventArgs e)
        {
            var discipline = e.Row as Discipline;
            if (discipline == null)
            {
                return;
            }

            string errorText;

            if (!(e.Valid = ValidateDiscipline(discipline, out errorText)))
            {
                e.Valid = false;
                e.ErrorText += errorText;
            }
            if (discipline.Unit == null)
            {
                e.Valid = false;
                e.ErrorText += "�� ������ ������ ����." + Environment.NewLine;
            }
        }

        /// <summary>
        /// Handles disciplines grid view validating editor event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void DisciplinesGridViewValidatingEditor(object sender, BaseContainerValidateEditorEventArgs e)
        {
            if (sender != _disciplinesGridView)
            {
                return;
            }

            if (e.Value is Unit && CurrentDiscipline != null)
            {
                CurrentDiscipline.Part = null;
            }

            var text = e.Value as string;
            if (text == null) return;

            // Gets disciplines grid view focused column name.
            string fieldName = _disciplinesGridView.FocusedColumn.FieldName;

            switch (fieldName)
            {
                case "Name":
                    string errorText;
                    if (CurrentDiscipline != null)
                    {
                        if (!(e.Valid = ValidateDisciplineName(text, out errorText)))
                        {
                            e.ErrorText = errorText;
                        }
                    }
                    else
                    {
                        if (!(e.Valid = ValidateDisciplineName(text, out errorText)))
                        {
                            e.ErrorText = errorText;
                        }
                    }
                    break;
                case "Part":
                    if (CurrentDiscipline != null)
                    {
                        try
                        {
                            //Look for part with same name in db.
                            e.Value =
                                Program.DbSingletone.Parts.Single(part => part.Name == text && part.Unit == CurrentDiscipline.Unit);
                        }
                        catch (InvalidOperationException)
                        {
                            if (CurrentDiscipline.Unit != null && !string.IsNullOrEmpty(text))
                            {
                                e.Value = new Part {Name = text, Unit = CurrentDiscipline.Unit};
                            }
                            else
                            {
                                e.Value = null;
                            }
                        }
                        catch (DbException ex)
                        {
                            Program.HandleDbException(ex);
                        }
                    }
                    else
                    {
                        e.Value = null;
                    }
                    break;
                case "Unit":
                    try
                    {
                        if (string.IsNullOrEmpty(text))
                        {
                            e.ErrorText = "������ �� ����� ���� ������!";
                            e.Value = null;
                            e.Valid = false;
                        }
                        else
                        {
                            //Look for unit with same name in db.
                            e.Value = Program.DbSingletone.Units.Single(unit => unit.Name == text);
                        }
                    }
                    catch (InvalidOperationException)
                    {
                        // If there no such part in db, then create the new part.
                        e.Value = new Unit {Name = text};
                    }
                    catch (DbException ex)
                    {
                        Program.HandleDbException(ex);
                    }
                    break;
            }
        }

        /// <summary>
        /// Handles disciplines grid view focused row changed event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DisciplinesGridViewFocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            string errorText;
            if (_prevDiscipline != null && !ValidateDiscipline(_prevDiscipline, out errorText))
            {
                //If _previous discipline is not validated then discard data context changes.
                Program.DiscardDataChanges();
            }
            if (_prevDiscipline != null && !_prevDiscipline.Equals(CurrentDiscipline))
            {
                OfferToSaveDetailsChanges(_prevDiscipline);
            }

            if (CurrentDiscipline == null)
            {
                _descriptionRtfEditControl.Rtf = null;
            }
            else if (!CurrentDiscipline.Equals(_prevDiscipline))
            {
                _descriptionRtfEditControl.Rtf = CurrentDiscipline.Description;
            }

            _prevDiscipline = CurrentDiscipline;
        }

        /// <summary>
        /// Handles disciplines grid view initialization of new row event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DisciplinesGridViewInitNewRow(object sender, InitNewRowEventArgs e)
        {
            var discipline = _disciplinesGridView.GetRow(e.RowHandle) as Discipline;
            if (discipline != null)
            {
                foreach (GridColumn groupColumn in _disciplinesGridView.GroupedColumns)
                {
                    if (_disciplinesGridView.GetFocusedRowCellValue(groupColumn) == null)
                    {
                        _disciplinesGridView.SetRowCellValue(_disciplinesGridView.FocusedRowHandle,
                                                             groupColumn, _groupColumnValues[groupColumn]);
                    }
                }
            }
        }

        /// <summary>
        /// Handles disciplines grid view invalid row exception.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DisciplinesGridView_InvalidRowException(object sender, InvalidRowExceptionEventArgs e)
        {
            e.ErrorText += "������ �������������� ������?";
            e.ExceptionMode =
                XtraMessageBox.Show(e.ErrorText, "������", MessageBoxButtons.YesNo, MessageBoxIcon.Error) ==
                DialogResult.No
                    ? ExceptionMode.Ignore
                    : ExceptionMode.NoAction;
        }

        /// <summary>
        /// Re-fill part's combobox when current column is part's column.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DisciplinesGridViewCustomRowCellEditForEditing(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.Column == _disciplinesGridView.Columns["Part"])
            {
                FillPartsComboBox();
            }
        }

        private bool _editLockFlag = true;

        /// <summary>
        /// Handles discipline grid control embeded navigator buttons click event. 
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void DisciplinesGridControlEmbeddedNavigatorButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            Discipline discipline;
            if (e != null && e.Button != null)
            {
                switch (e.Button.ButtonType)
                {
                    case NavigatorButtonType.Append:
                        _groupColumnValues.Clear();
                        int currentRow = _disciplinesGridView.FocusedRowHandle;

                        if (currentRow < 0)
                        {
                            currentRow = _disciplinesGridView.GetDataRowHandleByGroupRowHandle(currentRow);
                        }

                        if (_disciplinesGridView.SortInfo.GroupCount == 0)
                        {
                            return;
                        }
                        //Remember current grouped columns values.
                        foreach (GridColumn groupColumn in _disciplinesGridView.GroupedColumns)
                        {
                            object val = _disciplinesGridView.GetRowCellValue(currentRow, groupColumn);
                            _groupColumnValues.Add(groupColumn, val);
                        }

                        break;
                    case NavigatorButtonType.Remove:

                        // TODO: Confirmation.

                        int[] rowHandles = _disciplinesGridView.GetSelectedRows();

                        if (rowHandles.Length > 0)
                        {
                            object row = _disciplinesGridView.GetRow(rowHandles[0]);

                            discipline = row as Discipline;

                            if (discipline != null && (discipline.Courses.Count > 0
                                                       || discipline.Lessons.Count > 0))
                            {
                                if ((XtraMessageBox.Show("� ���� ������ ���� ��������� ������ c ���� �����." +
                                                         Environment.NewLine + "������� ����?",
                                                         "�������������", MessageBoxButtons.YesNo,
                                                         MessageBoxIcon.Question) == DialogResult.No))
                                {
                                    e.Handled = true;
                                    return;
                                }
                            }

                            if (discipline != null) Program.DbSingletone.Disciplines.DeleteOnSubmit(discipline);

                            _disciplinesGridView.BeginDataUpdate();

                            if (!SubmitChanges())
                            {
                                XtraMessageBox.Show("������ ��� �������� ����." +
                                                    Environment.NewLine + "� ���� ������ ���� ��������� ������.",
                                                    "������", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }

                            _disciplinesGridView.EndDataUpdate();
                        }
                        break;
                    case NavigatorButtonType.Custom:
                        var tag = e.Button.Tag as string;
                        if (tag != null)
                        {
                            tag = tag.Split( '#' )[0];
                            switch (tag)
                            {
                                case "EditLock":
                                    
                                    var disciplineGridPermission = PermissionType.None;
                                    if (
                                        !_permissionsDic.TryGetValue(_disciplinesGridControl.Name,
                                            out disciplineGridPermission))
                                    disciplineGridPermission = PermissionType.Add | PermissionType.Change | PermissionType.Delete | PermissionType.ReadOnly;
                                    _editLockFlag = !_editLockFlag ||
                                                disciplineGridPermission == PermissionType.ReadOnly;
                                    bool allowEdit = !_editLockFlag && disciplineGridPermission.HasFlag(PermissionType.Change);
                                    _disciplinesGridView.OptionsBehavior.Editable = disciplineGridPermission != PermissionType.ReadOnly;
                                    _descriptionRtfEditControl.ReadOnly = !allowEdit;
                                    _disciplinesGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = allowEdit;
                                    _disciplinesGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = !_editLockFlag && disciplineGridPermission.HasFlag(PermissionType.Delete);
                                    _disciplinesGridControl.EmbeddedNavigator.Buttons.Append.Enabled = !_editLockFlag && disciplineGridPermission.HasFlag( PermissionType.Add );

                                    e.Button.ImageIndex = _editLockFlag || disciplineGridPermission == PermissionType.ReadOnly ? 1 : 0;
                                    break;
                                case "Refresh":
                                    RefreshDisciplinesGridView();
                                    break;
                            }
                        }
                        break;
                }
            }
        }

        #endregion

        /// <summary>
        /// Refreshes disciplines grid view.
        /// </summary>
        private void RefreshDisciplinesGridView()
        {
            FillUnitsComboBox();

            IQueryable<Discipline> disciplines = from d in Program.DbSingletone.Disciplines
                                                 select d;
            _disciplinesGridControl.DataSource = disciplines;
        }

        /// <summary>
        /// Validates new discipline name.
        /// </summary>
        /// <param name="name">name</param>
        /// <param name="errorText">error's text</param>
        /// <returns>validation result</returns>
        private static bool ValidateDisciplineName(string name, out string errorText)
        {
            bool isValidate = true;
            errorText = null;

            if (string.IsNullOrEmpty(name))
            {
                isValidate = false;
                errorText = "���� �� ������� ���� ������." + Environment.NewLine;
            }

            return isValidate;
        }

        /// <summary>
        /// Validate the discipline.
        /// </summary>
        /// <param name="discipline">discipline</param>
        /// <param name="errorText">error's text</param>
        /// <returns>validation result</returns>
        private static bool ValidateDiscipline(Discipline discipline, out string errorText)
        {
            bool isValidate = ValidateDisciplineName(discipline.Name, out errorText);

            if (Program.DbSingletone.Disciplines.Any(d => d.Name == discipline.Name &&
                                                d.DisciplineId != discipline.DisciplineId &&
                                                d.Unit == discipline.Unit && d.Part == discipline.Part
                ))
            {
                isValidate = false;
                errorText += "����� ���� ��� ����������." + Environment.NewLine;
            }

            return isValidate;
        }

        /// <summary>
        /// Offers to save and saves discipline description changes.
        /// </summary>
        /// <param name="discipline"></param>
        private void OfferToSaveDetailsChanges(Discipline discipline)
        {
            if (_descriptionRtfEditControl.Modified && discipline != null)
            {
                if (XtraMessageBox.Show("����� �������� ��� �������" + Environment.NewLine + "��������� ���������?",
                                        "�������������", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    discipline.Description = _descriptionRtfEditControl.Rtf;

                    SubmitChanges();
                }
            }
        }

        /// <summary>
        /// Handles save discipline description button click event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void SaveDescriptionButtonClick(object sender, EventArgs e)
        {
            if (_descriptionRtfEditControl.Modified)
            {
                var discipline = _disciplinesGridView.GetRow(_disciplinesGridView.FocusedRowHandle) as Discipline;
                if (discipline != null)
                {
                    discipline.Description = _descriptionRtfEditControl.Rtf;

                    SubmitChanges();
                }

                _descriptionRtfEditControl.ResetModifiedProperty();
            }
        }

        /// <summary>
        /// Commit changes in db.
        /// </summary>
        /// <returns>result</returns>
        private static bool SubmitChanges()
        {
            try
            {
                Program.DbSingletone.SubmitChanges();
                return true;
            }
            catch (DbException ex)
            {
                Program.HandleDbException(ex);
            }
            catch (Exception)
            {
                var sb = new StringBuilder();
                sb.Append("��� ������� ��������� ��������� � ���� ������");
                sb.Append(Environment.NewLine);
                sb.Append("��������� ������. ��� ��������� ���� ��������.");
                sb.Append(Environment.NewLine);
                sb.Append("������������� ���������.");
                XtraMessageBox.Show(sb.ToString(), "����������� ������", MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
            }

            return false;
        }

        #endregion

        #region Constructors

        public DisciplinesForm()
        {
            
            InitializeComponent();
        }

        #endregion

        private void AttachmentsFolderButtonEditButtonClick(object sender, ButtonPressedEventArgs e)
        {
            switch (e.Button.Index)
            {
                case 0:

                    Setting setting =
                        Program.DbSingletone.Settings.FirstOrDefault(
                            s => s.Key == Properties.Settings.Default.AttachmentsBaseFolderSettingKey);

                    string basePath = setting != null ? setting.Value : String.Empty;

                    if (String.IsNullOrEmpty(basePath) || !Directory.Exists(basePath))
                    {
                        XtraMessageBox.Show(this,
                                            "�� ������� � ����������, ���� �� ����� ���� ������� �������� ����� � �������� �����������.",
                                            "������", MessageBoxButtons.OK);

                        break;
                    }

                    string relativePath = _disciplinesGridView.ActiveEditor.EditValue != null
                                              ? _disciplinesGridView.ActiveEditor.EditValue.ToString()
                                              : String.Empty;

                    string selectedPath = basePath;

                    if (!String.IsNullOrEmpty(relativePath))
                    {
                        try
                        {
                            selectedPath = Path.Combine(basePath, relativePath);
                        }
                        catch (ArgumentException)
                        {
                        }
                    }

                    var dialog = new FolderBrowserDialog
                                     {
                                         SelectedPath = selectedPath,
                                         ShowNewFolderButton = false
                                     };

                    if (dialog.ShowDialog(this) == DialogResult.OK)
                    {
                        if (!dialog.SelectedPath.StartsWith(basePath))
                        {
                            XtraMessageBox.Show(this,
                                                "����� ������ ���������� � �������� ����� � �������� �����������.",
                                                "������", MessageBoxButtons.OK);
                        }
                        else
                        {
                            _disciplinesGridView.ActiveEditor.EditValue = !dialog.SelectedPath.Equals(basePath)
                                                                              ? dialog.SelectedPath.Substring(
                                                                                  basePath.Length + 1)
                                                                              : String.Empty;
                        }
                    }

                    break;

                case 1:

                    _disciplinesGridView.ActiveEditor.EditValue = null;

                    break;
            }
        }

        private void TestsLookUpEditButtonClick(object sender, ButtonPressedEventArgs e)
        {
            switch (e.Button.Index)
            {
                case 1:

                    _disciplinesGridView.ActiveEditor.EditValue = null;

                    break;
            }
        }

        private void DisciplinesGridViewCustomColumnSort(object sender, CustomColumnSortEventArgs e)
        {
            if (e.Column.FieldName == disciplineNameGridColumn.FieldName)
            {
                var discipline1 = e.RowObject1 as Discipline;
                var discipline2 = e.RowObject2 as Discipline;

                if (discipline1 != null && discipline2 != null)
                {
                    e.Result = discipline1.CompareTo(discipline2);
                    e.Handled = true;

                    return;
                }
            }

            e.Handled = false;
        }
    }
}
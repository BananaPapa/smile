namespace Curriculum
{
    partial class ConnectionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this._textEditDatabase = new DevExpress.XtraEditors.TextEdit();
            this._simpleButtonConnect = new DevExpress.XtraEditors.SimpleButton();
            this._simpleButtonExit = new DevExpress.XtraEditors.SimpleButton();
            this._textEditPassword = new DevExpress.XtraEditors.TextEdit();
            this._textEditUser = new DevExpress.XtraEditors.TextEdit();
            this._textEditDataSource = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._textEditDatabase.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._textEditPassword.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._textEditUser.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._textEditDataSource.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this._textEditDatabase);
            this.layoutControl1.Controls.Add(this._simpleButtonConnect);
            this.layoutControl1.Controls.Add(this._simpleButtonExit);
            this.layoutControl1.Controls.Add(this._textEditPassword);
            this.layoutControl1.Controls.Add(this._textEditUser);
            this.layoutControl1.Controls.Add(this._textEditDataSource);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(372, 160);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // _textEditDatabase
            // 
            this._textEditDatabase.Location = new System.Drawing.Point(118, 36);
            this._textEditDatabase.Name = "_textEditDatabase";
            this._textEditDatabase.Size = new System.Drawing.Size(242, 20);
            this._textEditDatabase.StyleController = this.layoutControl1;
            this._textEditDatabase.TabIndex = 9;
            // 
            // _simpleButtonConnect
            // 
            this._simpleButtonConnect.Location = new System.Drawing.Point(216, 108);
            this._simpleButtonConnect.Name = "_simpleButtonConnect";
            this._simpleButtonConnect.Size = new System.Drawing.Size(82, 22);
            this._simpleButtonConnect.StyleController = this.layoutControl1;
            this._simpleButtonConnect.TabIndex = 8;
            this._simpleButtonConnect.Text = "�����������";
            this._simpleButtonConnect.Click += new System.EventHandler(this._simpleButtonConnect_Click);
            // 
            // _simpleButtonExit
            // 
            this._simpleButtonExit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._simpleButtonExit.Location = new System.Drawing.Point(302, 108);
            this._simpleButtonExit.Name = "_simpleButtonExit";
            this._simpleButtonExit.Size = new System.Drawing.Size(58, 22);
            this._simpleButtonExit.StyleController = this.layoutControl1;
            this._simpleButtonExit.TabIndex = 7;
            this._simpleButtonExit.Text = "�����";
            // 
            // _textEditPassword
            // 
            this._textEditPassword.Location = new System.Drawing.Point(118, 84);
            this._textEditPassword.Name = "_textEditPassword";
            this._textEditPassword.Properties.PasswordChar = '*';
            this._textEditPassword.Size = new System.Drawing.Size(242, 20);
            this._textEditPassword.StyleController = this.layoutControl1;
            this._textEditPassword.TabIndex = 6;
            // 
            // _textEditUser
            // 
            this._textEditUser.Location = new System.Drawing.Point(118, 60);
            this._textEditUser.Name = "_textEditUser";
            this._textEditUser.Size = new System.Drawing.Size(242, 20);
            this._textEditUser.StyleController = this.layoutControl1;
            this._textEditUser.TabIndex = 5;
            // 
            // _textEditDataSource
            // 
            this._textEditDataSource.Location = new System.Drawing.Point(118, 12);
            this._textEditDataSource.Name = "_textEditDataSource";
            this._textEditDataSource.Size = new System.Drawing.Size(242, 20);
            this._textEditDataSource.StyleController = this.layoutControl1;
            this._textEditDataSource.TabIndex = 4;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.emptySpaceItem1,
            this.layoutControlItem6});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(372, 160);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this._textEditDataSource;
            this.layoutControlItem1.CustomizationFormText = "������ ��� ������:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(352, 24);
            this.layoutControlItem1.Text = "������ ��� ������:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(103, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this._textEditUser;
            this.layoutControlItem2.CustomizationFormText = "������������:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(352, 24);
            this.layoutControlItem2.Text = "������������:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(103, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this._textEditPassword;
            this.layoutControlItem3.CustomizationFormText = "������:";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(352, 24);
            this.layoutControlItem3.Text = "������:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(103, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this._simpleButtonExit;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(290, 96);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(62, 44);
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this._simpleButtonConnect;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(204, 96);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(86, 44);
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 96);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(204, 44);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this._textEditDatabase;
            this.layoutControlItem6.CustomizationFormText = "���� ������:";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(352, 24);
            this.layoutControlItem6.Text = "���� ������:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ConnectionForm
            // 
            this.AcceptButton = this._simpleButtonConnect;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._simpleButtonExit;
            this.ClientSize = new System.Drawing.Size(372, 160);
            this.Controls.Add(this.layoutControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ConnectionForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "����������� � ������� ��� ������#r";
            this.Text = "����������� � ������� ��� ������";
            this.Load += new System.EventHandler(this.ConnectionForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._textEditDatabase.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._textEditPassword.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._textEditUser.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._textEditDataSource.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.SimpleButton _simpleButtonConnect;
        private DevExpress.XtraEditors.SimpleButton _simpleButtonExit;
        private DevExpress.XtraEditors.TextEdit _textEditPassword;
        private DevExpress.XtraEditors.TextEdit _textEditUser;
        private DevExpress.XtraEditors.TextEdit _textEditDataSource;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.TextEdit _textEditDatabase;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
    }
}
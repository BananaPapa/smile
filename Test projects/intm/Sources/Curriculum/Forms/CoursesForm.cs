using System.ComponentModel;
using DevExpress.XtraEditors;
using Eureca.Integrator.Common;

namespace Curriculum
{
    public partial class CoursesForm : FormWithAccessAreas
    {
        #region Fields

        private Ship _ship;

        #endregion

        #region Public properties

        /// <summary>
        /// Gets or sets current ship.
        /// </summary>
        [Browsable(false)]
        public Ship Ship
        {
            get { return _ship; }
            set
            {
                _ship = value;
                string name = (_ship != null) ? _ship.Name : "";
                Text = string.Format("����� [{0}]", name);
                _coursesControl.Ship = _ship;
            }
        }

        #endregion

        #region Constructors

        public CoursesForm(Ship ship)
        {
            InitializeComponent();

            Ship = ship;
        }

        #endregion
    }
}
using DevExpress.XtraEditors.Controls;

namespace Curriculum
{
    partial class DisciplinesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( DisciplinesForm ) );
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this._disciplinesGridControl = new DevExpress.XtraGrid.GridControl();
            this._imageList = new System.Windows.Forms.ImageList(this.components);
            this._disciplinesGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.disciplineUnitGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._unitsComboBox = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.disciplinePartGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._partsComboBox = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.disciplineNameGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.testGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._testsLookUpEdit = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this._attachmentsFolderGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._attachmentsFolderButtonEdit = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this._descriptionXtraTabControl = new DevExpress.XtraTab.XtraTabControl();
            this._descriptionXtraTabPage = new DevExpress.XtraTab.XtraTabPage();
            this._descriptionRtfEditControl = new Curriculum.RtfEditControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this._saveDescriptionButton = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._disciplinesGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._disciplinesGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._unitsComboBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._partsComboBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._testsLookUpEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._attachmentsFolderButtonEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._descriptionXtraTabControl)).BeginInit();
            this._descriptionXtraTabControl.SuspendLayout();
            this._descriptionXtraTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this._disciplinesGridControl);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this._descriptionXtraTabControl);
            this.splitContainerControl1.Panel2.Text = "��������";
            this.splitContainerControl1.Size = new System.Drawing.Size(939, 595);
            this.splitContainerControl1.SplitterPosition = 329;
            this.splitContainerControl1.TabIndex = 1;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // _disciplinesGridControl
            // 
            this._disciplinesGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._disciplinesGridControl.EmbeddedNavigator.Buttons.Append.Tag = "������� ����������#ra";
            this._disciplinesGridControl.EmbeddedNavigator.Buttons.Edit.Tag = "������� ����������#rc";
            this._disciplinesGridControl.EmbeddedNavigator.Buttons.ImageList = this._imageList;
            this._disciplinesGridControl.EmbeddedNavigator.Buttons.Remove.Tag = "������� ����������#rd";
            this._disciplinesGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "���������/��������� ��������������", "EditLock#r"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "", "Refresh")});
            this._disciplinesGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.DisciplinesGridControlEmbeddedNavigatorButtonClick);
            this._disciplinesGridControl.Location = new System.Drawing.Point(0, 0);
            this._disciplinesGridControl.MainView = this._disciplinesGridView;
            this._disciplinesGridControl.Name = "_disciplinesGridControl";
            this._disciplinesGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this._partsComboBox,
            this._unitsComboBox,
            this._testsLookUpEdit,
            this._attachmentsFolderButtonEdit});
            this._disciplinesGridControl.Size = new System.Drawing.Size(939, 329);
            this._disciplinesGridControl.TabIndex = 1;
            this._disciplinesGridControl.Tag = "������� ����������#r";
            this._disciplinesGridControl.UseEmbeddedNavigator = true;
            this._disciplinesGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this._disciplinesGridView});
            // 
            // _imageList
            // 
            this._imageList.ImageStream = ( (System.Windows.Forms.ImageListStreamer)( resources.GetObject( "_imageList.ImageStream" ) ) );
            this._imageList.TransparentColor = System.Drawing.Color.White;
            this._imageList.Images.SetKeyName(0, "EditLockOpen16.png");
            this._imageList.Images.SetKeyName(1, "EditLock16.png");
            this._imageList.Images.SetKeyName(2, "Refresh16.png");
            // 
            // _disciplinesGridView
            // 
            this._disciplinesGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.disciplineUnitGridColumn,
            this.disciplinePartGridColumn,
            this.disciplineNameGridColumn,
            this.testGridColumn,
            this._attachmentsFolderGridColumn});
            this._disciplinesGridView.GridControl = this._disciplinesGridControl;
            this._disciplinesGridView.GroupCount = 2;
            this._disciplinesGridView.Name = "_disciplinesGridView";
            this._disciplinesGridView.OptionsBehavior.Editable = false;
            this._disciplinesGridView.OptionsDetail.EnableMasterViewMode = false;
            this._disciplinesGridView.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.disciplineUnitGridColumn, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.disciplinePartGridColumn, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.disciplineNameGridColumn, DevExpress.Data.ColumnSortOrder.Ascending)});
            this._disciplinesGridView.CustomRowCellEditForEditing += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.DisciplinesGridViewCustomRowCellEditForEditing);
            this._disciplinesGridView.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.DisciplinesGridViewInitNewRow);
            this._disciplinesGridView.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.DisciplinesGridViewFocusedRowChanged);
            this._disciplinesGridView.FocusedColumnChanged += new DevExpress.XtraGrid.Views.Base.FocusedColumnChangedEventHandler(this.DisciplineGridViewFocusedColumnChanged);
            this._disciplinesGridView.InvalidRowException += new DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventHandler(this.DisciplinesGridView_InvalidRowException);
            this._disciplinesGridView.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.DisciplinesGridView_ValidateRow);
            this._disciplinesGridView.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.DisciplinesGridViewRowUpdated);
            this._disciplinesGridView.CustomColumnSort += new DevExpress.XtraGrid.Views.Base.CustomColumnSortEventHandler(this.DisciplinesGridViewCustomColumnSort);
            this._disciplinesGridView.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.DisciplinesGridViewValidatingEditor);
            // 
            // disciplineUnitGridColumn
            // 
            this.disciplineUnitGridColumn.AppearanceHeader.Options.UseTextOptions = true;
            this.disciplineUnitGridColumn.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.disciplineUnitGridColumn.Caption = "������";
            this.disciplineUnitGridColumn.ColumnEdit = this._unitsComboBox;
            this.disciplineUnitGridColumn.FieldName = "Unit";
            this.disciplineUnitGridColumn.Name = "disciplineUnitGridColumn";
            this.disciplineUnitGridColumn.Tag = "������#r";
            // 
            // _unitsComboBox
            // 
            this._unitsComboBox.AutoHeight = false;
            this._unitsComboBox.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this._unitsComboBox.Name = "_unitsComboBox";
            this._unitsComboBox.NullText = "�� ������";
            // 
            // disciplinePartGridColumn
            // 
            this.disciplinePartGridColumn.AppearanceHeader.Options.UseTextOptions = true;
            this.disciplinePartGridColumn.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.disciplinePartGridColumn.Caption = "�����";
            this.disciplinePartGridColumn.ColumnEdit = this._partsComboBox;
            this.disciplinePartGridColumn.FieldName = "Part";
            this.disciplinePartGridColumn.Name = "disciplinePartGridColumn";
            this.disciplinePartGridColumn.Tag = "�����#r";
            // 
            // _partsComboBox
            // 
            this._partsComboBox.AutoHeight = false;
            this._partsComboBox.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(),
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this._partsComboBox.Name = "_partsComboBox";
            this._partsComboBox.NullText = "�� �������";
            // 
            // disciplineNameGridColumn
            // 
            this.disciplineNameGridColumn.AppearanceHeader.Options.UseTextOptions = true;
            this.disciplineNameGridColumn.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.disciplineNameGridColumn.Caption = "����";
            this.disciplineNameGridColumn.FieldName = "Name";
            this.disciplineNameGridColumn.Name = "disciplineNameGridColumn";
            this.disciplineNameGridColumn.SortMode = DevExpress.XtraGrid.ColumnSortMode.Custom;
            this.disciplineNameGridColumn.Tag = "����#r";
            this.disciplineNameGridColumn.Visible = true;
            this.disciplineNameGridColumn.VisibleIndex = 0;
            // 
            // testGridColumn
            // 
            this.testGridColumn.Caption = "����";
            this.testGridColumn.ColumnEdit = this._testsLookUpEdit;
            this.testGridColumn.FieldName = "TestGuid";
            this.testGridColumn.Name = "testGridColumn";
            this.testGridColumn.Tag = "����#r";
            this.testGridColumn.Visible = true;
            this.testGridColumn.VisibleIndex = 1;
            // 
            // _testsLookUpEdit
            // 
            this._testsLookUpEdit.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this._testsLookUpEdit.AutoHeight = false;
            this._testsLookUpEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(),
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this._testsLookUpEdit.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TestName", "����")});
            this._testsLookUpEdit.DisplayMember = "TestName";
            this._testsLookUpEdit.Name = "_testsLookUpEdit";
            this._testsLookUpEdit.NullText = "�� �����";
            this._testsLookUpEdit.ValueMember = "guid";
            this._testsLookUpEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.TestsLookUpEditButtonClick);
            // 
            // _attachmentsFolderGridColumn
            // 
            this._attachmentsFolderGridColumn.Caption = "����� � �����������";
            this._attachmentsFolderGridColumn.ColumnEdit = this._attachmentsFolderButtonEdit;
            this._attachmentsFolderGridColumn.FieldName = "AttachmentsFolder";
            this._attachmentsFolderGridColumn.Name = "_attachmentsFolderGridColumn";
            this._attachmentsFolderGridColumn.Tag = "����� � �����������#r";
            this._attachmentsFolderGridColumn.Visible = true;
            this._attachmentsFolderGridColumn.VisibleIndex = 2;
            // 
            // _attachmentsFolderButtonEdit
            // 
            this._attachmentsFolderButtonEdit.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this._attachmentsFolderButtonEdit.AutoHeight = false;
            this._attachmentsFolderButtonEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(),
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this._attachmentsFolderButtonEdit.Name = "_attachmentsFolderButtonEdit";
            this._attachmentsFolderButtonEdit.NullText = "�� ������";
            this._attachmentsFolderButtonEdit.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this._attachmentsFolderButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.AttachmentsFolderButtonEditButtonClick);
            // 
            // _descriptionXtraTabControl
            // 
            this._descriptionXtraTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._descriptionXtraTabControl.Location = new System.Drawing.Point(0, 0);
            this._descriptionXtraTabControl.Name = "_descriptionXtraTabControl";
            this._descriptionXtraTabControl.SelectedTabPage = this._descriptionXtraTabPage;
            this._descriptionXtraTabControl.Size = new System.Drawing.Size(939, 260);
            this._descriptionXtraTabControl.TabIndex = 0;
            this._descriptionXtraTabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this._descriptionXtraTabPage});
            // 
            // _descriptionXtraTabPage
            // 
            this._descriptionXtraTabPage.Controls.Add(this._descriptionRtfEditControl);
            this._descriptionXtraTabPage.Controls.Add(this.panelControl2);
            this._descriptionXtraTabPage.Name = "_descriptionXtraTabPage";
            this._descriptionXtraTabPage.Size = new System.Drawing.Size(934, 234);
            this._descriptionXtraTabPage.Tag = "��������#r";
            this._descriptionXtraTabPage.Text = "��������";
            // 
            // _descriptionRtfEditControl
            // 
            this._descriptionRtfEditControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._descriptionRtfEditControl.Location = new System.Drawing.Point(0, 0);
            this._descriptionRtfEditControl.MinimumSize = new System.Drawing.Size(420, 100);
            this._descriptionRtfEditControl.Name = "_descriptionRtfEditControl";
            this._descriptionRtfEditControl.ReadOnly = true;
            this._descriptionRtfEditControl.Rtf = "{\\rtf1\\ansi\\ansicpg1251\\deff0\\deflang1049{\\fonttbl{\\f0\\fnil\\fcharset204 Microsoft" +
    " Sans Serif;}}\r\n\\viewkind4\\uc1\\pard\\f0\\fs17\\par\r\n}\r\n";
            this._descriptionRtfEditControl.Size = new System.Drawing.Size(934, 204);
            this._descriptionRtfEditControl.TabIndex = 2;
            this._descriptionRtfEditControl.Tag = "..\\��������#r";
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this._saveDescriptionButton);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl2.Location = new System.Drawing.Point(0, 204);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(934, 30);
            this.panelControl2.TabIndex = 3;
            // 
            // _saveDescriptionButton
            // 
            this._saveDescriptionButton.Dock = System.Windows.Forms.DockStyle.Right;
            this._saveDescriptionButton.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this._saveDescriptionButton.Location = new System.Drawing.Point(845, 2);
            this._saveDescriptionButton.Name = "_saveDescriptionButton";
            this._saveDescriptionButton.Size = new System.Drawing.Size(87, 26);
            this._saveDescriptionButton.TabIndex = 2;
            this._saveDescriptionButton.Tag = "..\\��������#rc";
            this._saveDescriptionButton.Text = "���������";
            this._saveDescriptionButton.ToolTip = "��������� ���������";
            this._saveDescriptionButton.Click += new System.EventHandler(this.SaveDescriptionButtonClick);
            // 
            // DisciplinesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(939, 595);
            this.Controls.Add(this.splitContainerControl1);
            this.HasInnerHideLogic = true;
            this.Name = "DisciplinesForm";
            this.Tag = "������ ����������\\������� ����\\����#r";
            this.Text = "����";
            this.Deactivate += new System.EventHandler(this.DisciplinesForm_Deactivate);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DisciplinesFormFormClosing);
            this.Load += new System.EventHandler(this.DisciplinesFormLoad);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._disciplinesGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._disciplinesGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._unitsComboBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._partsComboBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._testsLookUpEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._attachmentsFolderButtonEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._descriptionXtraTabControl)).EndInit();
            this._descriptionXtraTabControl.ResumeLayout(false);
            this._descriptionXtraTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraGrid.GridControl _disciplinesGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView _disciplinesGridView;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox _unitsComboBox;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox _partsComboBox;
 private DevExpress.XtraTab.XtraTabControl _descriptionXtraTabControl;
        private DevExpress.XtraTab.XtraTabPage _descriptionXtraTabPage;
        private RtfEditControl _descriptionRtfEditControl;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton _saveDescriptionButton;
        private System.Windows.Forms.ImageList _imageList;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit _testsLookUpEdit;
        private DevExpress.XtraGrid.Columns.GridColumn _attachmentsFolderGridColumn;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit _attachmentsFolderButtonEdit;
        private DevExpress.XtraGrid.Columns.GridColumn testGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn disciplineUnitGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn disciplinePartGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn disciplineNameGridColumn;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Curriculum;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;

namespace Eureca.Professional.FillingFormsTest.Forms
{
    public partial class UncheckedBlancs : XtraForm
    {
        private Curriculum.ProfessionalDataContext db;
        public UncheckedBlancs( )
        {
            db = Program.ProfessionalDb;
            InitializeComponent( );
            _uncheckedBlancList.DataSource = db.vUnverifiedAssignments;
        }

        private void _uncheckedBlancView_DoubleClick(object sender, EventArgs e)
        {
            GridView view = (GridView) sender;

            Point pt = view.GridControl.PointToClient(Control.MousePosition);

            GridHitInfo info = view.CalcHitInfo(pt);

            if (info.InRow || info.InRowCell)
            {
                var row = (vUnverifiedAssignment) view.GetRow(info.RowHandle);

                VerifyTestPassing(row.tpId);

               
            }
        }

        private void VerifyTestPassing( int p )
        {

            _uncheckedBlancList.RefreshDataSource( );
            if (_uncheckedBlancList.MainView.RowCount == 0)
                DialogResult = DialogResult.OK;
        }

        private void _okButton_Click( object sender, EventArgs e )
        {
            DialogResult = DialogResult.OK;
        }

        private void _verifyCurrentButton_Click( object sender, EventArgs e )
        {
            GridView view = (GridView) _uncheckedBlancList.MainView;
            var rowHandles = view.GetSelectedRows();
            if(rowHandles == null || rowHandles.Length == 0)
                return;
            var row = (vUnverifiedAssignment)view.GetRow(rowHandles[0]);
            VerifyTestPassing(row.tpId);
        }
    }
}

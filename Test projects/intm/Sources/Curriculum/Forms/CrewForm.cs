using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using Curriculum.Reports;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using Eureca.Integrator.Common;

namespace Curriculum
{
    public partial class CrewForm : FormWithAccessAreas, IAliasesHolder
    {
        /// <summary>
        /// Report key.
        /// </summary>
        private const string ReportKey = "CoursesPassing";


        /// <summary>
        /// ������� � ���������� � ������� �� ��������� ���� ��������
        /// </summary>
        private readonly List<CoursesPassingPerson> _first = new List<CoursesPassingPerson>();

        /// <summary>
        /// ������� � ���������� � ������� ���� �������� ����������� ��� ��� ���������
        /// </summary>
        private readonly List<CoursesPassingPerson> _second = new List<CoursesPassingPerson>();


        public CrewForm()
        {
            InitializeComponent();
            InsertData();
        }

        #region IAliasesHolder Members

        public void ApplyAliases()
        {
            try
            {
                Aliase.ApplyAlias("Ship", a => gridColumnShip.Caption = a);
                //FindAndHide();
            }
            catch (DbException)
            {
                //TODO: Handle DB errors.
                throw;
            }
        }

        #endregion

        private void InsertData()
        {
            _first.Clear();
            _second.Clear();
            List<Person> first =
                Program.DbSingletone.Persons.Where(
                    items => items.ExaminationDate.HasValue && items.ExaminationDate >= DateTime.Now)
                    .OrderBy(items => items.ExaminationDate).ToList();

            foreach (Person person in first)
            {
                _first.Add(GetPerson(person));
            }
            List<Person> second =
                Program.DbSingletone.Persons.Where(items => items.ExaminationDate.HasValue && items.ExaminationDate < DateTime.Now
                                                  && items.DepartureDate.HasValue && items.DepartureDate > DateTime.Now)
                    .OrderBy(items => items.DepartureDate).ToList();
            foreach (Person person in second)
            {
                _second.Add(GetPerson(person));
            }

            gridControlFirst.DataSource = _first;
            gridViewFirst.BestFitColumns();
            gridControl1.DataSource = _second;
            gridViewSecond.BestFitColumns();
        }

        private static CoursesPassingPerson GetPerson(Person person)
        {
            var coursePerson = new CoursesPassingPerson
                                   {
                                       Person = person,
                                   };
            return coursePerson;
        }

        private void _reportSimpleButton_Click(object sender, EventArgs e)
        {
            //Person person = Person;

            try
            {
                string reportPath = Reporter.GetTemplateDocument(ReportKey, this);
                if (string.IsNullOrEmpty(reportPath))
                {
                    //MessageBox.Show("�� ������ ������ ��� ������");
                    return;
                }

                new MarqueeProgressForm(
                    "������������ ������",
                    progressForm =>
                        {
                            //�������� ������� ���������� �����
                            Dictionary<string, string> dict =
                                WordManager.GetCommonFields(_first.FirstOrDefault().Person);
                            var wordManager = new WordManager();
                            try
                            {
                                wordManager.Open(reportPath);
                                Thread.Sleep(2000);
                                try
                                {
                                    wordManager.CreateCoursesPassing(progressForm, _first, _second);
                                }
                                catch (Exception exception)
                                {
                                    MessageBox.Show(exception.Message);
                                }
                                //�������� 
                                wordManager.ReplaceTemplateCommonFields(dict);
                                wordManager.SaveAs(reportPath);
                            }
                            finally
                            {
                                wordManager.KillWinWord();
                            }
                        }).ShowDialog();


                Application.DoEvents();
                // ��������� � ��������� �����....
                Reporter.SaveAndOpenReport(fileName =>
                                               {
                                                   try
                                                   {
                                                       if (System.IO.File.Exists(fileName))
                                                           System.IO.File.Delete( fileName );
                                                       System.IO.File.Move( reportPath, fileName );
                                                       return null;
                                                   }
                                                   catch (Exception ex)
                                                   {
                                                       return ex;
                                                   }
                                               });
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "����������");
            }
        }

        private void _refreshSimpleButton_Click(object sender, EventArgs e)
        {
            Program.DbSingletone.RefreshDataSource(typeof (Statuse), typeof (Discipline), typeof (Person), typeof (Lesson));

            InsertData();
        }

        private void _personelSimpleButton_Click(object sender, EventArgs e)
        {
            // Check if the parent form is main form.
            if (ParentForm != null && ParentForm is StartForm)
            {
                var state = gridViewSecond.GetFocusedRow() as CoursesPassingPerson;

                // Chick if there is a person to edit.
                if (state != null)
                {
                    // Open or activate persons form and focus desired person.
                    ((PersonsForm)
                     ((StartForm) ParentForm).OpenOrActivateMdiChild(typeof (PersonsForm), () => new PersonsForm())).
                        FocusedPerson = state.Person;
                }
            }
        }

        private void simpleButtonToShips_Click(object sender, EventArgs e)
        {
            // Check if the parent form is main form.
            if (ParentForm != null && ParentForm is StartForm)
            {
                var state = gridViewSecond.GetFocusedRow() as CoursesPassingPerson;

                // Chick if there is a person to edit.
                if (state != null)
                {
                    // Open or activate persons form and focus desired person.
                    ((ShipsForm)
                     ((StartForm) ParentForm).OpenOrActivateMdiChild(typeof (ShipsForm), () => new ShipsForm())).
                        FocusedShip = state.Person.Ship;
                }
            }
        }

        private void simpleButtonToShips1_Click(object sender, EventArgs e)
        {
            // Check if the parent form is main form.
            if (ParentForm != null && ParentForm is StartForm)
            {
                var state = gridViewFirst.GetFocusedRow() as CoursesPassingPerson;

                // Chick if there is a person to edit.
                if (state != null)
                {
                    // Open or activate persons form and focus desired person.
                    ((ShipsForm)
                     ((StartForm)ParentForm).OpenOrActivateMdiChild(typeof(ShipsForm), () => new ShipsForm())).
                        FocusedShip = state.Person.Ship;
                }
            }
        }

        private void simpleButtonToPersonalFirst_Click(object sender, EventArgs e)
        {
            // Check if the parent form is main form.
            if (ParentForm != null && ParentForm is StartForm)
            {
                var state = gridViewFirst.GetFocusedRow() as CoursesPassingPerson;

                // Chick if there is a person to edit.
                if (state != null)
                {
                    // Open or activate persons form and focus desired person.
                    ((PersonsForm)
                     ((StartForm)ParentForm).OpenOrActivateMdiChild(typeof(PersonsForm), () => new PersonsForm())).
                        FocusedPerson = state.Person;
                }
            }
        }

        public override IEnumerable<ISKOCBCTDNBTIDED> GetHiddenChilds(bool withLazy)
        {
            var hidden = new List<ISKOCBCTDNBTIDED>();
            if (withLazy)
            {
                var reportTemplateDummyForm = Reporter.GetReportTemplateDummyForm( );
                var supportAccessAreasForm = reportTemplateDummyForm as ISupportAccessAreas;
                supportAccessAreasForm.HierarchyParent = Program.StartForm;
                hidden.Add( new ControlInterfaceImplementation( reportTemplateDummyForm ) );

                var templateForm = Reporter.GetReportTemplateDummyForm() as ISupportAccessAreas;
                templateForm.HierarchyParent = this;
                hidden.Add(new ControlInterfaceImplementation(templateForm.Control));
            }
            return hidden;
        }
    }

    public class CoursesPassingPerson
    {
        private Person _person;
        public Person Person { get { return _person; } 
            set { _person = value;
                One = bool.Parse(GetProperty("��������",One.GetType()));
                Two = bool.Parse(GetProperty("�������������", Two.GetType()));
                Three = bool.Parse(GetProperty("���. � �/����", Three.GetType()));
                Four = GetProperty("��-����", typeof(string));
                Five = bool.Parse(GetProperty("�������", Five.GetType()));
                Six = bool.Parse(GetProperty("����", Six.GetType()));
                Seven = bool.Parse(GetProperty("������", Seven.GetType()));
            }
        }
        public string FIO { get; set; }

        public string ShipTool
        {
            get
            {
                if (Person.Ship != null)
                    return String.Join(", ", (from shipTool in Person.Ship.ShipTools
                                              select shipTool.Tool.ToString()).ToArray());
                return string.Empty;
            }
        }

        public bool HasLetters
        {
            get { return Person.Letters.Count != 0; }
        }

        public string Request
        {
            get
            {
                string code = Person.RequestCode;
                if (!string.IsNullOrEmpty(code))
                    code += ",";
                var data = Person.RequestDate.HasValue ? Person.RequestDate.Value.ToShortDateString() : string.Empty;
                return code + data;

            }
        }

      public string Notes
        {
            get
            {
                var rtf = new RichTextBox {Rtf = Person.Notes};
                return rtf.Text;
            }
        }

        public string Debts
        {
            get
            {
                IQueryable<Lesson> items = from lesson in Program.DbSingletone.Lessons
                                           where lesson.Person.Statuse.CreateProbationer &&
                                                 (!lesson.Person.ExaminationDate.HasValue ||
                                                  lesson.Person.ExaminationDate.Value >= DateTime.Now) &&
                                                 lesson.Person == Person &&
                                                 lesson.Discipline.TestGuid != null &&
                                                 lesson.LessonType.IsTest
                                           select lesson;
                int count = 0;
                foreach (Lesson item in items)
                {
                    //��������� �������
                    int testCompleted = Program.ProfessionalDb.GetTestStatus(item.Discipline.TestGuid,
                                                                             item.Person.ProfessionalUserUniqueId);
                    if (testCompleted == 0 && item.Datetime < DateTime.Now)
                    {
                        count++;
                    }
                }
                return count != 0 ? count.ToString() : "-";
            }
        }

        
               

        public bool One { get; private set; }
        public bool Two { get; private set; }
        public bool Three { get; private set; }
        public string Four { get; private set; }
        public bool Five { get; private set; }
        public bool Six { get; private set; }
        public bool Seven { get; private set; }

        private string GetProperty(string name,Type type)
        {
            var prop = Program.DbSingletone.GetPersonProperties(_person.PersonId);
            var temp = prop.Where(item => item.Name == name).FirstOrDefault();
            if (temp == null)
            {
                if (type == typeof (Boolean))
                    return false.ToString();
                return string.Empty;
            }
            if (temp.Value == null)
            {
                  if(type == typeof (Boolean))
                        return "false";
                return string.Empty;
            }
            if (type == typeof(Boolean))
            {
                bool res;
                if (!bool.TryParse(temp.Value, out res))
                    temp.Value = "false";
            }
            return temp.Value;
        }

    }

}
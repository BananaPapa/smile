﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eureca.Integrator.Utility;

namespace Curriculum
{
    [Serializable]
    public class DocumentMetadata
    {
        public SerializableDictionary<string, bool> ConfirmationList { get; set; }
    }
}

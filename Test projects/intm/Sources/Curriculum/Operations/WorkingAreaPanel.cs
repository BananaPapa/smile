﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace Curriculum.Operations
{

    [Designer( typeof( WorkingAreaDesigner ) )]
    public partial class WorkingAreaControl : Panel
    {
        public WorkingAreaControl( )
        {
            InitializeComponent( );
            base.Dock = DockStyle.Fill;
        }
    }

    public class WorkingAreaDesigner : ScrollableControlDesigner
    {
        protected override void PreFilterProperties(
                  System.Collections.IDictionary properties )
        {
            properties.Remove( "Dock" );

            base.PreFilterProperties( properties );
        }
    }
}

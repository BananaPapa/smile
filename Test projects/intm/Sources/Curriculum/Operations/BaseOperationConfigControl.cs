﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace Curriculum.Operations
{

    /// <span class="code-SummaryComment"><summary></span>
    /// A test Control to demonstrate allowing nested Controls
    /// to accept child controls at design time.
    /// <span class="code-SummaryComment"></summary></span>
    [Designer( typeof( BaseOperationConfigControlDesigner ) )]
    public partial class BaseOperationConfigControl : UserControl
    {
        public BaseOperationConfigControl( )
        {
            InitializeComponent( );
        }

        #region TestControl PROPERTIES ..........................
        /// <span class="code-SummaryComment"><summary></span>
        /// Surface the Caption to allow the user to 
        /// change it
        /// <span class="code-SummaryComment"></summary></span>
        //[Category( "Appearance" ),
        //DefaultValue( typeof( String ), "Test Control" )]
        //public string Caption
        //{
        //    get
        //    {
        //        return this.lblCaption.Text;
        //    }

        //    set
        //    {
        //        this.lblCaption.Text = value;
        //    }
        //}

        [
        Category( "Appearance" ),
        DesignerSerializationVisibility( DesignerSerializationVisibility.Content )
        ]
        /// <span class="code-SummaryComment"><summary></span>
        /// This property is essential to allowing the designer to work and
        /// the DesignerSerializationVisibility Attribute (above) is essential
        /// in allowing serialization to take place at design time.
        /// <span class="code-SummaryComment"></summary></span>
        public WorkingAreaControl WorkingArea
        {
            get
            {
                return _workingAreaPanel;
            }
        }
        #endregion
    }

    public class BaseOperationConfigControlDesigner : ParentControlDesigner
    {
        public override void Initialize( System.ComponentModel.IComponent component )
        {
            base.Initialize( component );

            if (this.Control is BaseOperationConfigControl)
            {
                this.EnableDesignMode( (
                   (BaseOperationConfigControl)this.Control ).WorkingArea, "WorkingArea" );
            }
        }
    }

    
}

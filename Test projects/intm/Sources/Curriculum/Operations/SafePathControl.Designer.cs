﻿namespace Curriculum.Operations
{
    partial class SafePathControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if (disposing && ( components != null ))
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            this._selectPathControl = new DevExpress.XtraEditors.ButtonEdit();
            ((System.ComponentModel.ISupportInitialize)(this._selectPathControl.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // _selectPathControl
            // 
            this._selectPathControl.Location = new System.Drawing.Point(0, 0);
            this._selectPathControl.Margin = new System.Windows.Forms.Padding(0);
            this._selectPathControl.Name = "_selectPathControl";
            this._selectPathControl.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this._selectPathControl.Size = new System.Drawing.Size(115, 20);
            this._selectPathControl.TabIndex = 0;
            // 
            // SafePathControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this._selectPathControl);
            this.Name = "SafePathControl";
            this.Size = new System.Drawing.Size(115, 20);
            ((System.ComponentModel.ISupportInitialize)(this._selectPathControl.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.ButtonEdit _selectPathControl;
    }
}

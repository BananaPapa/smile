﻿namespace Curriculum.Operations.Operation_Config
{
    partial class SendDocumentOperationConfigControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.baseOperationConfigControl1 = new Curriculum.Operations.BaseOperationConfigControl();
            this.label1 = new System.Windows.Forms.Label();
            this._recieverCombo = new System.Windows.Forms.ComboBox();
            this.baseOperationConfigControl1.WorkingArea.SuspendLayout();
            this.baseOperationConfigControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // baseOperationConfigControl1
            // 
            this.baseOperationConfigControl1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.baseOperationConfigControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.baseOperationConfigControl1.Location = new System.Drawing.Point(0, 0);
            this.baseOperationConfigControl1.MinimumSize = new System.Drawing.Size(200, 26);
            this.baseOperationConfigControl1.Name = "baseOperationConfigControl1";
            this.baseOperationConfigControl1.Size = new System.Drawing.Size(595, 37);
            this.baseOperationConfigControl1.TabIndex = 0;
            // 
            // baseOperationConfigControl1.WorkingArea
            // 
            this.baseOperationConfigControl1.WorkingArea.Controls.Add(this._recieverCombo);
            this.baseOperationConfigControl1.WorkingArea.Controls.Add(this.label1);
            this.baseOperationConfigControl1.WorkingArea.Location = new System.Drawing.Point(105, 0);
            this.baseOperationConfigControl1.WorkingArea.Name = "WorkingArea";
            this.baseOperationConfigControl1.WorkingArea.Size = new System.Drawing.Size(434, 37);
            this.baseOperationConfigControl1.WorkingArea.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Получатель";
            // 
            // _recieverCombo
            // 
            this._recieverCombo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._recieverCombo.FormattingEnabled = true;
            this._recieverCombo.Location = new System.Drawing.Point(82, 2);
            this._recieverCombo.Name = "_recieverCombo";
            this._recieverCombo.Size = new System.Drawing.Size(347, 21);
            this._recieverCombo.TabIndex = 1;
            // 
            // SendDocumentOperationConfigControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.baseOperationConfigControl1);
            this.Name = "SendDocumentOperationConfigControl";
            this.Size = new System.Drawing.Size(595, 37);
            this.baseOperationConfigControl1.WorkingArea.ResumeLayout(false);
            this.baseOperationConfigControl1.WorkingArea.PerformLayout();
            this.baseOperationConfigControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private BaseOperationConfigControl baseOperationConfigControl1;
        private System.Windows.Forms.ComboBox _recieverCombo;
        private System.Windows.Forms.Label label1;

    }
}

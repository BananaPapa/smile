﻿namespace Curriculum.Operations
{
    partial class BaseOperationConfigControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if (disposing && ( components != null ))
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            this._rightPanel = new System.Windows.Forms.Panel();
            this._deleteNode = new DevExpress.XtraEditors.SimpleButton();
            this._leftPanel = new System.Windows.Forms.Panel();
            this._operationCombo = new System.Windows.Forms.ComboBox();
            this._workingAreaPanel = new Curriculum.Operations.WorkingAreaControl();
            this._rightPanel.SuspendLayout();
            this._leftPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // _rightPanel
            // 
            this._rightPanel.Controls.Add(this._deleteNode);
            this._rightPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this._rightPanel.Location = new System.Drawing.Point(253, 0);
            this._rightPanel.MinimumSize = new System.Drawing.Size(56, 24);
            this._rightPanel.Name = "_rightPanel";
            this._rightPanel.Padding = new System.Windows.Forms.Padding(3);
            this._rightPanel.Size = new System.Drawing.Size(65, 116);
            this._rightPanel.TabIndex = 3;
            // 
            // _deleteNode
            // 
            this._deleteNode.Dock = System.Windows.Forms.DockStyle.Top;
            this._deleteNode.Location = new System.Drawing.Point(3, 3);
            this._deleteNode.Name = "_deleteNode";
            this._deleteNode.Size = new System.Drawing.Size(59, 23);
            this._deleteNode.TabIndex = 0;
            this._deleteNode.Text = "Удалить";
            // 
            // _leftPanel
            // 
            this._leftPanel.Controls.Add(this._operationCombo);
            this._leftPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this._leftPanel.Location = new System.Drawing.Point(0, 0);
            this._leftPanel.MinimumSize = new System.Drawing.Size(105, 21);
            this._leftPanel.Name = "_leftPanel";
            this._leftPanel.Padding = new System.Windows.Forms.Padding(3);
            this._leftPanel.Size = new System.Drawing.Size(105, 116);
            this._leftPanel.TabIndex = 2;
            // 
            // _operationCombo
            // 
            this._operationCombo.Dock = System.Windows.Forms.DockStyle.Top;
            this._operationCombo.FormattingEnabled = true;
            this._operationCombo.Location = new System.Drawing.Point(3, 3);
            this._operationCombo.Name = "_operationCombo";
            this._operationCombo.Size = new System.Drawing.Size(99, 21);
            this._operationCombo.TabIndex = 0;
            this._operationCombo.Text = "Добавить этап";
            // 
            // _workingAreaPanel
            // 
            this._workingAreaPanel.Location = new System.Drawing.Point(105, 0);
            this._workingAreaPanel.Name = "_workingAreaPanel";
            this._workingAreaPanel.Size = new System.Drawing.Size(148, 116);
            this._workingAreaPanel.TabIndex = 4;
            // 
            // BaseOperationConfigControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this._workingAreaPanel);
            this.Controls.Add(this._rightPanel);
            this.Controls.Add(this._leftPanel);
            this.MinimumSize = new System.Drawing.Size(200, 26);
            this.Name = "BaseOperationConfigControl";
            this.Size = new System.Drawing.Size(318, 116);
            this._rightPanel.ResumeLayout(false);
            this._leftPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel _rightPanel;
        private DevExpress.XtraEditors.SimpleButton _deleteNode;
        private System.Windows.Forms.Panel _leftPanel;
        private System.Windows.Forms.ComboBox _operationCombo;
        private WorkingAreaControl _workingAreaPanel;


    }
}

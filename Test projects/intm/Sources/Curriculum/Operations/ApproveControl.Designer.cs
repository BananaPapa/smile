﻿namespace Curriculum.Operations
{
    partial class ApproveControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if (disposing && ( components != null ))
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            this._confirmToggleControl = new DevExpress.XtraEditors.ToggleSwitch();
            ((System.ComponentModel.ISupportInitialize)(this._confirmToggleControl.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // _confirmToggleControl
            // 
            this._confirmToggleControl.Location = new System.Drawing.Point(0, 0);
            this._confirmToggleControl.Name = "_confirmToggleControl";
            this._confirmToggleControl.Properties.OffText = "Отменить";
            this._confirmToggleControl.Properties.OnText = "Подтвердить";
            this._confirmToggleControl.Size = new System.Drawing.Size(95, 24);
            this._confirmToggleControl.TabIndex = 0;
            // 
            // ApproveControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this._confirmToggleControl);
            this.Name = "ApproveControl";
            this.Size = new System.Drawing.Size(98, 27);
            ((System.ComponentModel.ISupportInitialize)(this._confirmToggleControl.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.ToggleSwitch _confirmToggleControl;
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Curriculum.Operations
{
    public partial class SafePathControl : UserControl
    {
        public string Path { get; set; }        

        public SafePathControl( )
        {
            InitializeComponent( );
            _selectPathControl.DataBindings.Add(new Binding("Text", this, "Path"));
        }

    }
}

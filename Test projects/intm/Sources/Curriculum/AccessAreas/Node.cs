﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Eureca.Utility.Extensions;

namespace Curriculum
{
    public abstract class TreeNode<T>
    {
        private string _path = null;

        public TreeNode()
        {
        }

        public TreeNode(bool isBypassed, T value, TreeNode<T> owner = null)
        {
            this.IsBypassed = isBypassed;
            this.Value = value;
            this.Owner = owner;
        }

        public TreeNode<T> Owner { get; private set; }
        public bool IsBypassed { get; set; }
        public T Value { get; set; }

        public abstract string Name { get; set; }

        public string Path
        {
            get
            {
                if (_path == null)
                {
                    _path = this.Name;
                    var parent = this.Owner;
                    while (parent != null)
                    {
                        _path = System.IO.Path.Combine(parent.Name, _path);
                        parent = parent.Owner;
                    }
                }
                return _path;
            }
            set
            {
                _path = value;
            }
        }

        public static string GetDefaultNode(object obj)
        {
            //return "{{{0}}}".FormatString( obj.GetType( ) );
            return "#";
        }

    }

    public class TreeNodeControls : TreeNode<ISKOCBCTDNBTIDED>
    {
        public TreeNodeControls( bool isBypassed, ISKOCBCTDNBTIDED value, TreeNode<ISKOCBCTDNBTIDED> owner = null )
            : base( isBypassed, value, owner )
        {
                
        }
        public override string Name
        {
            get
            {
                return Value.Tag??GetDefaultNode(Value); 
            }
            set
            {
                throw new NotImplementedException( );
                //Value.Tag = value;
            }
        }
    }

    public class NamedTreeNodeControls : TreeNode<ISKOCBCTDNBTIDED>
    {
        private string _tag;
        public NamedTreeNodeControls( bool isBypassed, ISKOCBCTDNBTIDED value, string tag, TreeNode<ISKOCBCTDNBTIDED> owner = null )
            : base( isBypassed, value, owner )
        {
            _tag = tag;
        }

        public override string Name
        {
            get
            {
                return ( _tag ) ?? GetDefaultNode( Value );
            }
            set
            {
                throw new NotImplementedException();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Layout;
using DevExpress.XtraLayout;
using DevExpress.XtraLayout.Utils;
using DevExpress.XtraNavBar;
using Eureca.Integrator.Common;
using Eureca.Professional.CommonClasses;
using NHibernate.Util;

namespace Curriculum
{
    /// <summary>
    /// Some kind of base class "Control" that does not bother to implement DevExpress Developers
    /// </summary>
    public interface ISKOCBCTDNBTIDED
    {
        Type Type { get; }
        string Tag { get; }

        bool IsGroup { get; }
        bool Visible { get; set; }

        object Model { get; }

        List<ISKOCBCTDNBTIDED> GetItems();
    }


    public class BarItemBase : ISKOCBCTDNBTIDED
    {
        private BarItem _model;


        public BarItemBase( BarItem model)
        {
            _model = model;
        }

        #region ISKOCBCTDNBTIDED Members

        public Type Type
        {
            get
            {
                return _model.GetType();
            }
        }

        public string Tag
        {
            get { return _model.Tag as string; }
        }

        public bool IsGroup
        {
            get { return _model is BarButtonGroup; }
        }

        public bool Visible
        {
            get { return _model.Visibility == BarItemVisibility.Always; }
            set
            {
                _model.Visibility = value ? BarItemVisibility.Always : BarItemVisibility.Never;
            }
        }

        public List<ISKOCBCTDNBTIDED> GetItems()
        {
            var list = new List<ISKOCBCTDNBTIDED>();
            var group = _model as BarButtonGroup;
            if(group == null)
                return list;
            foreach (BarItemLink itemLink in group.ItemLinks)
            {
                list.Add(new BarItemBase(itemLink.Item));
            }
            return list;
        }
       
        public object Model
        {
            get { return  _model; }
        }

        #endregion

    }

    public class RibbonPageBase : ISKOCBCTDNBTIDED
    {
        private RibbonPage _model;


        public RibbonPageBase( RibbonPage model )
        {
            _model = model;
        }

        #region ISKOCBCTDNBTIDED Members

        public Type Type
        {
            get
            {
                return _model.GetType( );
            }
        }

        public string Tag
        {
            get { return _model.Tag as string; }
        }

        public bool IsGroup
        {
            get { return true; }
        }

        public bool Visible
        {
            get { return _model.Visible; }
            set
            {
                _model.Visible = value;
            }
        }

        public List<ISKOCBCTDNBTIDED> GetItems( )
        {
            var list = new List<ISKOCBCTDNBTIDED>( );
            var group = _model;
            if (group == null)
                return list;
            foreach (RibbonPageGroup itemLink in group.Groups)
            {
                list.Add( new RibbonPageGroupBase( itemLink ) );
            }
            return list;
        }


        public object Model
        {
            get { return _model; }
        }

        #endregion
    }

    public class LayoutViewColumnBase : ISKOCBCTDNBTIDED
    {
        private LayoutViewColumn _model;


        public LayoutViewColumnBase( LayoutViewColumn model )
        {
            _model = model;
        }

        #region ISKOCBCTDNBTIDED Members

        public Type Type
        {
            get
            {
                return _model.GetType( );
            }
        }

        public string Tag
        {
            get { return _model.Tag as string; }
        }

        public bool IsGroup
        {
            get { return false; }
        }

        public bool Visible
        {
            get { return _model.Visible; }
            set
            {
                _model.Visible = value;
            }
        }

        public List<ISKOCBCTDNBTIDED> GetItems( )
        {
            return null;
        }


        public object Model
        {
            get { return _model; }
        }

        #endregion
    }

    public class NavElementBase : ISKOCBCTDNBTIDED
    {
        private NavElement _model;

        public NavElementBase(NavElement model)
        {
            _model = model;
        }

        public Type Type
        {
            get { return _model.GetType(); }
        }

        public string Tag
        {
            get { return _model.Tag as string; }
        }

        public bool IsGroup
        {
            get { return _model is NavBarGroup; }
        }

        public bool Visible
        {
            get
            {
               return _model.Visible;
            }
            set
            {
                _model.Visible = value;
            }
        }

        public List<ISKOCBCTDNBTIDED> GetItems( )
        {
            List<ISKOCBCTDNBTIDED> list = new List<ISKOCBCTDNBTIDED>();
            var group = _model as NavBarGroup;
            if (group == null)
                return null;
            foreach (NavBarItemLink itemLink in group.ItemLinks)
            {
                list.Add( new NavElementBase(itemLink.Item) );
            }
            return list;
        }


        public object Model
        {
            get { return _model; }
        }

    } 
    
    public class LayoutControlGroupBase : ISKOCBCTDNBTIDED
    {
        private LayoutControlGroup _model;

        public LayoutControlGroupBase( LayoutControlGroup model )
        {
            _model = model;
        }

        public Type Type
        {
            get { return _model.GetType(); }
        }

        public string Tag
        {
            get { return _model.Tag as string; }
        }

        public bool IsGroup
        {
            get { return true; }
        }

        public bool Visible
        {
            get
            {
               return _model.Visible;
            }
            set
            {
                _model.Visibility = value ? LayoutVisibility.Always : LayoutVisibility.Never;
            }
        }

        public List<ISKOCBCTDNBTIDED> GetItems( )
        {
            var list = new List<ISKOCBCTDNBTIDED>();
            foreach (LayoutViewField itemLink in _model.Items.OfType<LayoutViewField>( ))
            {
                list.Add( new LayoutViewColumnBase( itemLink.Column ) );
            }
            foreach (LayoutControlGroup itemLink in _model.Items.OfType<LayoutControlGroup>( ))
            {
                list.Add( new LayoutControlGroupBase( itemLink ) );
            }
            return list;
        }


        public object Model
        {
            get { return _model; }
        }

    }

     public class RibbonPageGroupBase : ISKOCBCTDNBTIDED
     {
         private RibbonPageGroup _model;


         public RibbonPageGroupBase( RibbonPageGroup model )
         {
             _model = model;
         }

         #region ISKOCBCTDNBTIDED Members

         public Type Type
         {
             get
             {
                 return _model.GetType( );
             }
         }

         public string Tag
         {
             get { return _model.Tag as string; }
         }

         public bool IsGroup
         {
             get { return true; }
         }

         public bool Visible
         {
             get { return _model.Visible; }
             set
             {
                 _model.Visible = value;
             }
         }

         public List<ISKOCBCTDNBTIDED> GetItems( )
         {
             var list = new List<ISKOCBCTDNBTIDED>( );
             var group = _model;
             if (group == null)
                 return list;
             foreach (BarItemLink itemLink in group.ItemLinks)
             {
                 list.Add( new BarItemBase( itemLink.Item ) );
             }
             return list;
         }


         public object Model
         {
             get { return _model; }
         }

         #endregion
     }  

    public class BaseLayoutItemBase : ISKOCBCTDNBTIDED
     {
        private BaseLayoutItem _model;


        public BaseLayoutItemBase( BaseLayoutItem model )
         {
             _model = model;
         }

         #region ISKOCBCTDNBTIDED Members

         public Type Type
         {
             get
             {
                 return _model.GetType( );
             }
         }

         public string Tag
         {
             get { return _model.Tag as string; }
         }

         public bool IsGroup
         {
             get { return _model.IsGroup; }
         }

         public bool Visible
         {
             get { return _model.Visible; }
             set
             {
                 _model.Visibility = value? LayoutVisibility.Always : LayoutVisibility.Never;
             }
         }

         public List<ISKOCBCTDNBTIDED> GetItems( )
         {
             var list = new List<ISKOCBCTDNBTIDED>( );
             var group = _model as LayoutGroup;
             if (group == null)
                 return list;
             foreach (BaseLayoutItem baseLayoutItem in group.Items)
             {
                 list.Add( new BaseLayoutItemBase( baseLayoutItem ) );
             }
             return list;
         }


         public object Model
         {
             get { return _model; }
         }

         #endregion
     }

    public class ControlInterfaceImplementation : ISKOCBCTDNBTIDED
    {
        private Control _model;

        public ControlInterfaceImplementation(Control model)
        {
            _model = model;
        }

        public Type Type
        {
            get { return _model.GetType(); }
        }

        public string Tag
        {
            get { return _model.Tag as string; }
        }

        public bool IsGroup
        {
            get { return _model.Controls.Any(); }
        }

        public bool Visible
        {
            get
            {
                return _model.Visible;
            }
            set
            {
                _model.Visible = value;
            }
        }

        public List<ISKOCBCTDNBTIDED> GetItems( )
        {
            return _model.Controls.Cast<Control>( ).Select( control => new ControlInterfaceImplementation( control ) ).Cast<ISKOCBCTDNBTIDED>( ).ToList( );
        }


        public object Model
        {
            get { return _model; }
        }

    }

    public class BarManagerBase : ISKOCBCTDNBTIDED
    {
        private readonly BarManager _model;


        public BarManagerBase( BarManager model )
        {
            _model = model;
        }

        #region ISKOCBCTDNBTIDED Members

        public Type Type
        {
            get
            {
                return _model.GetType( );
            }
        }

        public string Tag
        {
            get { return null; }
        }

        public bool IsGroup
        {
            get { return true; }
        }

        public bool Visible
        {
            get { return false; }
            set
            {
                throw new NotImplementedException();
            }
        }

        public List<ISKOCBCTDNBTIDED> GetItems( )
        {
            var list = new List<ISKOCBCTDNBTIDED>( );
            foreach (BarItem item in _model.Items.Cast<BarItem>())
            {
                list.Add( new BarItemBase( item ) );
            }
            return list;
        }


        public object Model
        {
            get { return _model; }
        }

        #endregion
    }

    public class BarBase : ISKOCBCTDNBTIDED
    {
        private readonly Bar _model;


        public BarBase( Bar model )
        {
            _model = model;
        }

        #region ISKOCBCTDNBTIDED Members

        public Type Type
        {
            get
            {
                return _model.GetType( );
            }
        }

        public string Tag
        {
            get { return null; }
        }

        public bool IsGroup
        {
            get { return true; }
        }

        public bool Visible
        {
            get { return _model.Visible; }
            set
            {
                _model.Visible = value;
            }
        }

        public List<ISKOCBCTDNBTIDED> GetItems( )
        {
            var list = new List<ISKOCBCTDNBTIDED>( );
            foreach (BarItemLink itemLink in _model.ItemLinks.Cast<BarItemLink>( ))
            {
                list.Add( new BarItemBase( itemLink.Item ) );
            }
            return list;
        }


        public object Model
        {
            get { return _model; }
        }

        #endregion
    }

    public class TabPageBase : ISKOCBCTDNBTIDED
    {
        private readonly TabPage _model;


        public TabPageBase( TabPage model )
        {
            _model = model;
        }

        #region ISKOCBCTDNBTIDED Members

        public Type Type
        {
            get
            {
                return _model.GetType( );
            }
        }

        public string Tag
        {
            get { return _model.Tag as string; }
        }

        public bool IsGroup
        {
            get { return true; }
        }

        public bool Visible
        {
            get { return _model.Visible; }
            set
            {
                _model.Visible = value;
            }
        }

        public List<ISKOCBCTDNBTIDED> GetItems( )
        {
            return new List<ISKOCBCTDNBTIDED>( _model.Controls.Cast<Control>().Select(control=> new ControlInterfaceImplementation(control)) );
        }


        public object Model
        {
            get { return _model; }
        }

        #endregion
    }

}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Management;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.UI.Design.WebControls;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using Aga.Controls.Tree;
using Curriculum.AccessAreas;
using DevExpress.XtraBars;
using DevExpress.XtraBars.Docking2010.Views.WindowsUI;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraBars.Ribbon.Handler;
using DevExpress.XtraBars.Ribbon.Internal;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Layout;
using DevExpress.XtraLayout;
using DevExpress.XtraLayout.Utils;
using DevExpress.XtraNavBar;
using DevExpress.XtraTab;
using Eureca.Integrator.Common;
using Eureca.Professional.CommonClasses;
using Eureca.Utility;
using Eureca.Utility.Extensions;
using Eureca.Utility.Helper;
using NHibernate.Type;

namespace Curriculum
{
    public class PermissionManager
    {
        private bool _isAdmin;

        private bool? _allowVerifyTests = null;
        private bool? _allowReceiveMessages = null;

        //private IEnumerable<RolesPermission> _permissionsContext;
        private Func<IEnumerable<RolesPermission>> _getContextMethod;
        private Dictionary<Role, IEnumerable<Permission>> _cash = new Dictionary<Role, IEnumerable<Permission>>();

        private Role _currentRole;

        public event EventHandler<AccessAreaControlArgs<GridControl>> OnGridFiltering;
        public event EventHandler<AccessAreaControlArgs<GridColumn>> OnGridColumnFiltering;
        public event EventHandler<AccessAreaControlArgs<LayoutViewColumn>> OnLayoutViewColumnFiltering;


        public bool AllowUncheckedTestNatification 
        {
            get
            {
                if (_isAdmin)
                    return true;
                if (_allowVerifyTests == null)
                {
                    _allowVerifyTests =
                        GetRolePermissions(_currentRole).Any(
                            pair =>
                                pair.NormalisedTag.EndsWith(@"\Непроверенные тесты") &&
                                pair.PermissionType.HasFlag(PermissionType.Change));
                }
                return _allowVerifyTests.Value;
            }
        }

        public bool AllowMessages
        {
            get
            {
                if (_isAdmin)
                    return true;
                if (_allowReceiveMessages == null)
                {
                    _allowReceiveMessages =
                        GetRolePermissions( _currentRole ).Any(
                            pair =>
                                pair.NormalisedTag.EndsWith( @"Личный кабинет\Сообщения" ) &&
                                pair.PermissionType.HasFlag( PermissionType.ReadOnly ) );
                }
                return _allowReceiveMessages.Value;
            }
        }

        public void OnRoleChanged()
        {
            _allowVerifyTests = null;

            if (_currentRole == null)
            {
                _isAdmin = false;
                _cash.Clear();
            }
            else
            {
                _isAdmin = false;
                var parent = _currentRole;
                while (true)
                {
                    if (parent.Admin)
                        _isAdmin = true;
                    parent = parent.ParentRole;
                    if (parent == null)
                        break;
                }
            }

            //_permissionsContext = createContext( );
        }

        public Role CurrentRole 
        {
            get
            {
                return _currentRole;
            }
            set
            {
                _currentRole = value;
                OnRoleChanged();
            }
        }

        public bool IsAdmin
        {
            get { return _isAdmin; }
        }

        List<Permission> GetRolePermissions(Role role)
        {
            var permissions = new List<Permission>();
            var node = role;
            do
            {
                var someShit = node;
                if (_cash.ContainsKey(node))
                    _cash.Remove(node);
                var loadedPermissions = _getContextMethod().Where( permission => permission.RoleId == someShit.RoleId );
                _cash.Add( someShit, loadedPermissions.Select( permission => new Permission( permission ) ).ToList() );
                permissions.AddRange( _cash[someShit] );
                node = node.ParentRole;
            } while (node != null);
            return permissions;
        }

        //private IEnumerable<string> _globalKeys; 

        public PermissionManager( Func<IEnumerable<RolesPermission>> createContext )
        {
            this._getContextMethod = createContext;
            //createContext
            //_permissionsContext = createContext();
        }

        public void ApplyFilter( ISupportAccessAreas control )
        {
            var tree = FindAndHideControls( control, true, false );
            Application.DoEvents();
        }

        public List<string> GenerateTree(ISupportAccessAreas control)
        {
            var keys = FindAndHideControls( control, false, true );
            Trace.WriteLine( string.Join("\n" ,keys) );
            return keys.Select( NormalizePath).Distinct().ToList( );
        }

        public string NormalizePath(string path)
        {
            var res = Permission.RemoveUnmarkedNodes(path);
            res = Permission.SolveRelativePath(res);
            //upper folder
            
            return res;
        }

        private List<string> FindAndHideControls( ISupportAccessAreas owner, bool isHiding = true, bool withLazy = false )
        {
            List<string> nodes = new List<string>( );
            var root = CreateNode( owner);
            Stack<TreeNode<ISKOCBCTDNBTIDED>> controlTreeStack = new Stack<TreeNode<ISKOCBCTDNBTIDED>>( );

            IEnumerable<Permission> filteredItems = null;
            if (isHiding && !_isAdmin)
            {
                filteredItems = FilterByHeader( root.Path);
            }
            controlTreeStack.Push( root );
            while (true)
            {
                if (controlTreeStack.Count == 0)
                    break;
                var head = controlTreeStack.Peek( );
                var control = head.Value;
                if (!head.IsBypassed && !( isHiding && control.Tag != null && !CheckAllow( head.Path, filteredItems ) ))
                {
                    #region Обход элементов управления.
                    
                    //getting filtered permissions
                    var permissions = isHiding ? FilterByHeader( head.Path, filteredItems ) : null;


                    //put nodes childs to stack
                    if (control != null && control.GetItems().Count != 0)
                    {
                        foreach (var childs in control.GetItems())
                        {
                            AddChildInStack( childs, head, controlTreeStack );
                        }
                    }

                    //Check additional childs passings for devexpress controls and ISupportAccess areas controls
                    do
                    {
                        var grid = head.Value.Model as GridControl;
                        if (grid != null)
                        {
                            //Add permissions to ISupportAccessAreasControls
                            if (isHiding)
                            {
                                var accessAreaOwner =
                                    controlTreeStack.FirstOrDefault(node => node.Value.Model is ISupportAccessAreas);
                                if (accessAreaOwner != null)
                                {
                                    var supportAccessAreas = (accessAreaOwner.Value.Model as ISupportAccessAreas);
                                    if (supportAccessAreas.HasInnerHideLogic)
                                        supportAccessAreas.AddControlRights(grid.Name,
                                            GetPermissionForNode(head.Path, permissions));
                                }
                            }   
                         
                            nodes.AddRange( FindAndHideGridViews( head.Path, grid, isHiding, permissions ) );
                            break;
                        }

                        var ribbon = head.Value.Model as RibbonControl;
                        if (ribbon != null)
                        {
                            nodes.AddRange(FindAndHideRibbonControlItems( head.Path, ribbon, isHiding, permissions ));
                            break;
                        }

                        var navBar = head.Value.Model as NavBarControl;
                        if (navBar != null)
                        {
                            nodes.AddRange(FindAndHideNavbarElements( head.Path, navBar, isHiding, permissions ));
                            break;
                        }

                        var barManager = head.Value.Model as BarManager;
                        if(barManager!=null)
                        {
                            nodes.AddRange( FindAndHideBarManager( head.Path, barManager, isHiding, permissions ) );
                            break;
                        }

                        var supportAccessAreasControl = control.Model as ISupportAccessAreas;
                        if (supportAccessAreasControl != null)
                        {
                            foreach (var hiddenChild in supportAccessAreasControl.GetHiddenChilds( withLazy ))
                            {
                                var accessAreasControlChild = hiddenChild.Model as ISupportAccessAreas;
                                if ( accessAreasControlChild != null && accessAreasControlChild.HierarchyParent != null )
                                {
                                    var supportAccessAreasNode = CreateNode(accessAreasControlChild);
                                    controlTreeStack.Push(supportAccessAreasNode);
                                }
                                else
                                {
                                    AddChildInStack( hiddenChild, head, controlTreeStack );
                                }
                            }
                            break;
                        }
                        else
                        {
                            var hadHiddenChildrens = control.Model as IHasHiddenChildren;
                            if (hadHiddenChildrens != null)
                                foreach (var hiddenChild in hadHiddenChildrens.GetHiddenChilds( withLazy ))
                                {
                                    AddChildInStack( hiddenChild, head, controlTreeStack );
                                }
                            break;
                        }

                    } while (false);
                    #endregion
                 
                    head.IsBypassed = true;
                }
                else
                {
                    controlTreeStack.Pop( );
                    if (!string.IsNullOrEmpty( head.Value.Tag as string ))
                    {
                        nodes.Add( head.Path );
                        if (isHiding)
                            ApplyFilter(head.Path, control, filteredItems);
                        //control.Visible = CheckAllow( head.Path, filteredItems );
                    }
                   
                }
            }
            return nodes;
        }

        private IEnumerable<string> FindAndHideTabPages( TreeNode<ISKOCBCTDNBTIDED> parent, TabControl tabControl, bool isHiding, List<Permission> permissions,Stack<TreeNode<ISKOCBCTDNBTIDED>> stack  )
        {
            var tabControlNode = new TreeNodeControls(true, new ControlInterfaceImplementation(tabControl), parent);
            stack.Push(tabControlNode);
            var nodes = new List<string>();
            var tabControlTag = (tabControl.Tag as string) ?? TreeNodeControls.GetDefaultNode(tabControl);
            foreach (var tabPage in tabControl.TabPages.Cast<TabPage>())
            {
                var tabTag = tabPage.Tag as string;
                var tagPath = Path.Combine( tabControlTag,parent.Path, tabTag??TreeNodeControls.GetDefaultNode(tabPage) );
                if(tabTag == null)
                {
                    var checkAllow = CheckAllow(tagPath, permissions);
                    nodes.Add(tagPath);
                    if (isHiding)
                    {
                        tabPage.Visible = false;
                        continue;
                    }
                }
                AddChildInStack(new TabPageBase(tabPage),tabControlNode,stack);
            }
            return nodes;
        }

        private void ApplyFilter( string path, ISKOCBCTDNBTIDED control, IEnumerable<Permission> permissions = null )
        {          
            var checkAllow = CheckAllow(path, permissions);
            if(!control.Visible && checkAllow)
                return;
            var permission = GetPermissionForNode(path,permissions);
            control.Visible = checkAllow;
            
            TypeSwitch.Do( control.Model,TypeSwitch.Case<XtraTabPage>(() =>(control.Model as XtraTabPage).PageVisible = checkAllow));

            if (permission == PermissionType.ReadOnly)
            {
                TypeSwitch.Do(control.Model,
                    TypeSwitch.Case<BaseEdit>( ( c ) => ( c as BaseEdit ).Properties.ReadOnly = true ),
                    TypeSwitch.Case<TextBox>( ( c ) => ( c as TextBox ).ReadOnly = true ));
                var actualControl = control.Model as Control;
                if (actualControl != null)
                    actualControl.AllowDrop = false;

            }
        }

        /// <summary>
        /// Filter tags for current path
        /// </summary>
        /// <param name="header">Path start part</param>
        /// <returns>Tags which starts for this path</returns>
        List<Permission> FilterByHeader( string header, IEnumerable<Permission> permissions = null )
        {
            var headerPermission = Permission.Parse( header );
            return ( permissions ?? GetRolePermissions( _currentRole ) ).Where( permission => permission.Tag.StartsWith( headerPermission.NormalisedTag ) ).ToList( );
        }

        private static void AddChildInStack( ISKOCBCTDNBTIDED child, TreeNode<ISKOCBCTDNBTIDED> head, Stack<TreeNode<ISKOCBCTDNBTIDED>> controlTreeStack)
        {
            var treeNodeControls = new TreeNodeControls(false, child, head);
            treeNodeControls.Path = Path.Combine(head.Path, treeNodeControls.Name);
            controlTreeStack.Push(treeNodeControls);
        }

    
        public bool CheckAllow( string nodePath , IEnumerable<Permission> filteredList = null)
        {
            if (IsAdmin)
                return true;
            var permission = Permission.Parse(nodePath);
            var searchArea = filteredList == null || permission.IsRelative
                ? GetRolePermissions(_currentRole)
                : filteredList;
            var checkAllowRes = searchArea.Any( rolePermission => rolePermission.CheckAllow( permission ) );
            if (permission.NormalisedTag.Contains( "Учётная" ))
                Trace.WriteLine( permission.NormalisedTag );
            Trace.WriteLine( "node: {0} with min allows {1} visible: {2}".FormatString(permission.NormalisedTag , permission.PermissionType, checkAllowRes ) );
            return checkAllowRes;

        }

        public bool CheckAllow( ISupportAccessAreas nodePath, IEnumerable<Permission> filteredList = null )
        {
            if (IsAdmin)
                return true;
            var path = CreateNode(nodePath).Path;
            var permission = Permission.Parse(path);
            var searchArea = filteredList == null || permission.IsRelative
                ? GetRolePermissions( _currentRole )
                : filteredList;
            var checkAllowRes = searchArea.Any( rolePermission => rolePermission.CheckAllow( permission ) );
            Trace.WriteLine( "node: {0} with min allows {1} visible: {2}".FormatString( permission.NormalisedTag, permission.PermissionType, checkAllowRes ) );
            return checkAllowRes;

        }

        TreeNode<ISKOCBCTDNBTIDED> CreateNode( ISupportAccessAreas supportAccessAreasControl )
        {
            Stack<ISupportAccessAreas> parentStack = new Stack<ISupportAccessAreas>();
            parentStack.Push( supportAccessAreasControl );
            NamedTreeNodeControls nodeWithParentTree = null;
            var parent = supportAccessAreasControl.HierarchyParent;
            while (parent != null)
            {
                ISupportAccessAreas copy = parent;
                parentStack.Push(copy);
                parent = parent.HierarchyParent;
            }
            while (parentStack.Count >0)
            {
                var node = parentStack.Pop();
                var control = node.Control;
                nodeWithParentTree = new NamedTreeNodeControls( false, new ControlInterfaceImplementation( control ), node.NodeTag, nodeWithParentTree );
            }
            Trace.WriteLine( nodeWithParentTree.Path ); 
            return nodeWithParentTree;
        }

        #region DevExpressSerchers
        
        private IEnumerable<string> FindAndHideBarManager( string parentPath, BarManager barManager, bool isHiding, List<Permission> permissions )
        {
            var nodes = new List<string>();
            var bypassedBarItems = new List<BarItem>();
            foreach (var bar in barManager.Bars.Cast<Bar>())
            {
                var barBase = new BarBase( bar );
                var barPath = Path.Combine( parentPath, (bar.Tag as string)??TreeNodeControls.GetDefaultNode( bar ) );
                foreach (var baseItem in bar.ItemLinks.Cast<BarItemLink>().Select(barItemLink=>barItemLink.Item))
                {
                    var itemTag = FindAndHideSomeItem( barPath, isHiding, new BarItemBase( baseItem ), permissions );
                    if (itemTag!=null)
                    {
                        nodes.Add(itemTag);
                    }
                    bypassedBarItems.Add(baseItem);
                }
                nodes.Add( barPath ); 
                if (isHiding)
                {
                    FindAndHideSomeItem( parentPath, isHiding, barBase, permissions );
                }
            }

            foreach (var missedItem in barManager.Items.Cast<BarItem>().Where(link => !bypassedBarItems.Contains(link)))
            {
                var itemTag = FindAndHideSomeItem( parentPath, isHiding, new BarItemBase( missedItem), permissions );
                if (itemTag != null)
                {
                    nodes.Add( itemTag );
                }
            }

            return nodes;
        }

        private List<string> FindAndHideGridViews( string gridPath, GridControl grid, bool hiding,
            IEnumerable<Permission> permissions )
        {
            var bypassedColumns = new List<LayoutViewColumn>();
            if(hiding)
                CallOnFilterEvent( permissions, gridPath, grid );
            var tree = new List<string>( );
            foreach (var view in grid.Views.Cast<ColumnView>( ))
            {
                //Trace.WriteLine( gridPath + "\\views:" );
                string viewPath = Path.Combine( gridPath, ( view.Tag as string ) ?? TreeNodeControls.GetDefaultNode( view ) );
                var layoutView = view as LayoutView;
                if (layoutView != null)
                {
                    var groups = layoutView.Columns.Cast<LayoutViewColumn>( ).Where(column => column.LayoutViewField!=null && column.LayoutViewField.Parent !=null).Select( column => column.LayoutViewField.Parent as LayoutControlGroup );
                    foreach (var layoutControlGroup in groups)
                    {
                        var items = FindAndHideLayoutGroup(hiding, permissions, viewPath, layoutControlGroup, bypassedColumns);
                        tree.AddRange(items);
                    }
                    foreach (
                        var column in
                            layoutView.Columns.Cast<LayoutViewColumn>()
                                .Where(column => !bypassedColumns.Contains(column)))
                    {
                        var columnTag = FindAndHideSomeItem(viewPath, hiding, new LayoutViewColumnBase(column),
                            permissions);
                        if (columnTag != null)
                        {
                            tree.Add( columnTag );
                            if (hiding)
                                CallOnFilterEvent( permissions, columnTag, column );
                        }
                    }
                }
                else
                {
                    foreach (var column in view.Columns.Cast<GridColumn>( ))
                    {
                        var nodePath = Path.Combine( viewPath,
                            ( column.Tag as string ) ?? ( TreeNodeControls.GetDefaultNode( column ) ) );
                        if (column.Tag != null)
                        {
                            tree.Add( nodePath );
                            if (hiding)
                            {
                                CallOnFilterEvent( permissions, nodePath, column );
                                if(column.Visible)
                                    column.Visible = CheckAllow( nodePath , permissions );
                            }
                        }
                    }
                }
            }
            return tree;
        }

        private List<string> FindAndHideLayoutGroup( bool hiding, IEnumerable<Permission> permissions, string viewPath,
            LayoutControlGroup layoutControlGroup, List<LayoutViewColumn> bypassedColumns)
        {
            List<string> tree = new List<string>();
            var groupTag = Path.Combine( viewPath, layoutControlGroup.Tag as string ?? TreeNodeControls.GetDefaultNode( layoutControlGroup ) );
            foreach (var viewField in layoutControlGroup.Items.OfType<LayoutViewField>())
            {
                var layoutViewColumn = viewField.Column;
                var columnTag = FindAndHideSomeItem(groupTag, hiding, new LayoutViewColumnBase(layoutViewColumn), permissions);
                bypassedColumns.Add(layoutViewColumn);
                if (columnTag != null)
                {
                    tree.Add(columnTag);
                    if(hiding)
                        CallOnFilterEvent( permissions, columnTag, layoutViewColumn );
                }
            }

            foreach (var viewField in layoutControlGroup.Items.OfType<BaseLayoutItem>())
            {
                var columnTag = FindAndHideSomeItem(groupTag, hiding, new BaseLayoutItemBase(viewField), permissions);
                if (columnTag != null)
                    tree.Add(columnTag);
            }

            foreach (var group in layoutControlGroup.Items.OfType<LayoutControlGroup>())
            {
                var items = FindAndHideLayoutGroup(hiding,permissions,viewPath,group,bypassedColumns);
                tree.AddRange(items);
            }

            var groupBaseTag = FindAndHideSomeItem(viewPath, hiding, new LayoutControlGroupBase(layoutControlGroup), permissions);
            if (groupBaseTag != null)
                tree.Add(groupBaseTag);
            return tree;
        }

        private List<string> FindAndHideRibbonControlItems( string _ribbonPath, RibbonControl ribbonControl, bool hiding,
            IEnumerable<Permission> permissions )
        {
            List<string> treeList = new List<string>( );
            List<BarItem> bypassedItems = new List<BarItem>( );

            foreach (RibbonPage page in ribbonControl.Pages)
            {
                
                string pagePath = Path.Combine( _ribbonPath,
                    ( page.Tag as string ) ?? ( TreeNodeControls.GetDefaultNode( page ) ) );
                foreach (RibbonPageGroup group in page.Groups)
                {
                    string groupPath = Path.Combine( pagePath,
                        ( group.Tag as string ) ?? ( TreeNodeControls.GetDefaultNode( group ) ) );
                    foreach (BarItemLink itemLink in group.ItemLinks)
                    {
                        var barItem = itemLink.Item;
                        var findAndSomeItem = FindAndHideSomeItem( groupPath, hiding, new BarItemBase( barItem ),
                            permissions );
                        if (findAndSomeItem != null)
                        {
                            treeList.Add( findAndSomeItem );
                        }
                        bypassedItems.Add( barItem );
                    }
                    FindAndHideSomeItem( pagePath, hiding, new RibbonPageGroupBase( group ), permissions );
                    if(group.Tag!=null)
                    {
                        treeList.Add(groupPath);
                    }
                }
                FindAndHideSomeItem( _ribbonPath, hiding, new RibbonPageBase( page ), permissions );
                if(page.Tag!=null)
                {
                    treeList.Add(pagePath);
                }
            }
            //tool bar and other
            foreach (
                BarItem ribbonBarItem in
                    ribbonControl.Items.Cast<BarItem>( ).Where( item => !bypassedItems.Contains( item ) ))
            {
                var findAndSomeItem = FindAndHideSomeItem( _ribbonPath, hiding, new BarItemBase( ribbonBarItem ),
                    permissions );
                if (findAndSomeItem != null)
                {
                    treeList.Add( findAndSomeItem );
                }
            }

            return treeList;
        }

        private List<string> FindAndHideNavbarElements( string navBarPath, NavBarControl navBarControl, bool hiding,
            IEnumerable<Permission> permissions )
        {
            List<string> treeList = new List<string>( );
            List<NavElement> byPassedElements = new List<NavElement>( );

            foreach (NavBarGroup @group in navBarControl.Groups)
            {
                var groupTag = Path.Combine(navBarPath, (@group.Tag as string)??  TreeNodeControls.GetDefaultNode( @group ));
                foreach (NavBarItemLink navBarItemLink in @group.ItemLinks)
                {
                    NavElement element = navBarItemLink.Item;
                    var findAndSomeItem = FindAndHideSomeItem( groupTag, hiding, new NavElementBase( element ), permissions );
                    if (findAndSomeItem != null)
                        treeList.Add( findAndSomeItem );
                    byPassedElements.Add( element );
                }

                FindAndHideSomeItem( navBarPath, hiding, new NavElementBase( @group ), permissions );
                if (@group.Tag != null)
                    treeList.Add( groupTag );
                byPassedElements.Add( @group );
            }

            foreach (
                NavElement element in
                    navBarControl.Items.Cast<NavElement>( ).Where( element => !byPassedElements.Contains( element ) ))
            {
                var findAndSomeItem = FindAndHideSomeItem( navBarPath, hiding, new NavElementBase( element ), permissions );
                if (findAndSomeItem != null)
                    treeList.Add( findAndSomeItem );
            }
            return treeList;
        }

        private string FindAndHideSomeItem( string parentPath, bool hiding, ISKOCBCTDNBTIDED baseItem
            , IEnumerable<Permission> permissions )
        {
            if (baseItem.Tag == null)
            {
                if (baseItem.IsGroup && hiding)
                {
                    baseItem.Visible = baseItem.GetItems( ).Where( skocbctdnbtided => skocbctdnbtided.Tag != null ).Any( skocbctdnbtided => skocbctdnbtided.Visible );
                }
                return null;
            }
            //string nodeName = baseItem.Tag ?? ( TreeNodeControls.GetDefaultNode( baseItem ) );
            string nodePath = Path.Combine( parentPath, baseItem.Tag );

            if (hiding)
            {
                var checkAllow = CheckAllow( nodePath, permissions );
                baseItem.Visible = checkAllow;

                if (baseItem.IsGroup)
                {
                    var visibleTagedChilds = baseItem.GetItems( ).Where( skocbctdnbtided => skocbctdnbtided.Tag != null ).Any( skocbctdnbtided => skocbctdnbtided.Visible );

                    if (!checkAllow)
                    {
                        if (!visibleTagedChilds)
                            baseItem.GetItems().ForEach(skocbctdnbtided => skocbctdnbtided.Visible = false);
                    }
                    else
                    {
                        if(baseItem.Visible)
                            baseItem.Visible = visibleTagedChilds;
                    }
                }
            }

            return baseItem.Tag == null ? null : nodePath;
        }

        private PermissionType GetPermissionForNode( string tag, IEnumerable<Permission> permissions )
        {
            PermissionType rolePermissions = PermissionType.None;
            if (_isAdmin)
                rolePermissions = PermissionType.Add | PermissionType.ReadOnly | PermissionType.Change |
                                  PermissionType.Delete;
            else
            {
                var permission = FilterByHeader( tag, permissions ).FirstOrDefault( );
                rolePermissions = permission == null ? PermissionType.None : permission.PermissionType;
            }
            return rolePermissions;
        }

        #endregion

        private void CallOnFilterEvent<T>( IEnumerable<Permission> permissions, string nodePath, T control )
        {
            var LayoutColumnPermission = Permission.Parse( nodePath );
            var rolePermissions = GetPermissionForNode( LayoutColumnPermission.NormalisedTag, permissions );
            if (typeof( T ) == typeof( GridColumn ))
                RaiseEvent( OnGridColumnFiltering,
                    new AccessAreaControlArgs<GridColumn>( LayoutColumnPermission,
                        rolePermissions, control as GridColumn ) );
            else if (typeof( T ) == typeof( LayoutViewColumn ))
                RaiseEvent( OnLayoutViewColumnFiltering,
                    new AccessAreaControlArgs<LayoutViewColumn>( LayoutColumnPermission,
                        rolePermissions, control as LayoutViewColumn ) );
            else if (typeof( T ) == typeof( GridControl ))
                RaiseEvent( OnGridFiltering,
                    new AccessAreaControlArgs<GridControl>( LayoutColumnPermission,
                        rolePermissions, control as GridControl ) );
            else
                throw new NotImplementedException( );
        }


        private void RaiseEvent<T>( EventHandler<AccessAreaControlArgs<T>> @event, AccessAreaControlArgs<T> args )
        {
            if (@event != null)
                @event( this, args);
        }
    }
    
    public class AccessAreaControlArgs<T> : EventArgs
    {
        public Permission Permission { get; private set; }

        public PermissionType RolePermission { get; private set; }

        public T Control { get; private set; }

        public AccessAreaControlArgs( Permission permission, PermissionType rolePermission, T control )
        {
            Permission = permission;
            RolePermission = rolePermission;
            Control = control;
        }

    }
}

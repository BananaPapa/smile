
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Aga.Controls.Tree;
using System.IO;
using System.Drawing;
using System.ComponentModel;
using System.Threading;
using Curriculum.AccessAreas;
using Eureca.Common.Settings;
using Eureca.Integrator.Common;
using Eureca.Utility.Helper;
using log4net;
using NHibernate.Util;

namespace Curriculum.Controls.AccessAreasTree
{
	public class AppControlItemsModel: ITreeModel
	{
	    private ILog Log = LogManager.GetLogger(typeof (AppControlItemsModel));

        List<ControlItemNode> roots = new List<ControlItemNode>( );
        Dictionary<string, ControlItemNode> _nodes = new Dictionary<string, ControlItemNode>( );
	    private BindingListWithRemoveEvent<Permission> _changes = new BindingListWithRemoveEvent<Permission>() ;

        public BindingListWithRemoveEvent<Permission> Changes
        {
            get { return _changes; }
        }

	    public IEnumerable<string> Keys
	    {
	        get { return _nodes.Keys; }
	    }

	    public TreePath GetPath( ControlItemNode item )
		{
			if (item == null)
				return TreePath.Empty;
			else
			{
				Stack<object> stack = new Stack<object>();
				while (item != null)
				{
					stack.Push(item);
					item = item.Parent;
				}
				return new TreePath(stack.ToArray());
			}
		}

		public System.Collections.IEnumerable GetChildren(TreePath treePath)
		{
			if (treePath.IsEmpty())
			{
			    return roots;
			}

            ControlItemNode parent = treePath.LastNode as ControlItemNode;
            return parent==null? new List<ControlItemNode>() : GetChildByPath(parent.ItemPath);
		}

	    public ControlItemNode GetNode(string path)
	    {
	        if (_nodes.ContainsKey(path))
	            return _nodes[path];
	        return null;
	    }

        public IEnumerable<ControlItemNode> GetChildByPath( string path , bool withNested = false )
	    {
	        List<ControlItemNode> items = new List<ControlItemNode>();
	        foreach (
	            var childKeys in
	                _nodes.Keys.Where(
	                    s =>
                            s.StartsWith( path ) && s != path &&
                            (withNested || Path.GetDirectoryName( s ) == path )))
	        {
	            items.Add(_nodes[childKeys]);
	        }
	        return items;
	    }

	    public bool IsLeaf(TreePath treePath)
		{
            return !GetChildren(treePath).Any();
		}

		public event EventHandler<TreeModelEventArgs> NodesChanged;
        
        internal void OnNodesChanged( ControlItemNode item )
        {
			if (NodesChanged != null)
			{
				TreePath path = GetPath(item.Parent);
				NodesChanged(this, new TreeModelEventArgs(path, new object[] { item }));
			}
		}

	    public AppControlItemsModel()
	    {
	    }

        public void LoadTree( IEnumerable<Tag> tags )
	    {
            foreach (var tag in tags.OrderBy( tag => tag.Tag1 ))
            {
                AddNode( tag );
            }
	    }

	    private void AddNode(Tag tag)
	    {
            var permission = new Permission(tag);
                //Permission.Parse( tag );
            string permissionKey = permission.Tag;
	        string normolisedPermissionKey = permission.NormalisedTag;
	        if (_nodes.ContainsKey( normolisedPermissionKey ))
            {
                _nodes[normolisedPermissionKey].SetFlag( permission.PermissionType ,true);
                return;
            }
            var parentPemissionKey = Permission.GetTag(Path.GetDirectoryName( permissionKey ));
	        var normolisedParentPermissionKey = Permission.RemoveTagsFromPath(parentPemissionKey);
	        try
	        {
	            ControlItemNode controlItem = null;
	            if (string.IsNullOrEmpty(parentPemissionKey) )
	            {
                    if (roots.All( node =>
                    {
                        return node.ItemPath != normolisedPermissionKey;
                    }))
                    {
                        controlItem = new ControlItemNode(permission,this);
                        roots.Add( controlItem );
                    }
                    else return;
	            }
	            else
	            {
                    if (_nodes.ContainsKey( normolisedParentPermissionKey ))
	                {
                        controlItem = new ControlItemNode( permission, this ,_nodes[normolisedParentPermissionKey] );
	                }
	                else
	                {
                        throw  new Exception("WTF");
	                }
	            }
                _nodes.Add( normolisedPermissionKey, controlItem );
                //Trace.WriteLine( "###" + normolisedPermissionKey );
	        }
	        catch (Exception e)
	        {
	            Log.Error(e.ToString());
	        }
	    }

	    public void LoadProjection(IEnumerable<Permission> permissions,bool withLock = false)
	    {
            ControlItemNode.DisableUpdateRelatives = true;
	        foreach (var permission in permissions)
	        {
	            var targetNode = _nodes[permission.Tag];
	            if (withLock)
                    targetNode.LockPermission( permission.PermissionType );
	            targetNode.SetFlag(permission.PermissionType, true);
	        }
            _changes.ClearHistory( );
	        ControlItemNode.DisableUpdateRelatives = false;

	    }

        public void ResetView()
        {
            ControlItemNode.DisableUpdateRelatives = true;
            foreach (var paths in _nodes.Keys)
            {
                _nodes[paths].Reset();
            }
            _changes.ClearHistory( );
            _changes.Clear( );
            ControlItemNode.DisableUpdateRelatives = false;
        }

		public event EventHandler<TreeModelEventArgs> NodesInserted;
		public event EventHandler<TreeModelEventArgs> NodesRemoved;
		public event EventHandler<TreePathEventArgs> StructureChanged;
		
        public void OnStructureChanged()
		{
			if (StructureChanged != null)
				StructureChanged(this, new TreePathEventArgs());
		}
	}
}

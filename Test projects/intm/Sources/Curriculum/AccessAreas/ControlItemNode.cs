using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Permissions;
using System.Xml.Serialization;
using Aga.Controls.Tree;
using Curriculum.AccessAreas;
using Eureca.Integrator.Common;

namespace Curriculum.Controls.AccessAreasTree
{
	public class ControlItemNode
	{
        protected static Dictionary<string,ControlItemNode> nodesDirectory = new Dictionary<string, ControlItemNode>();

	    private Permission _model;

	    private static bool _disableUpdateRelatives = false;
        //private TreePath _treePath;

        //public TreePath TreePath 
        //{
        //    get
        //    {
        //        if (_treePath == null)
        //            _treePath = _owner.GetPath(this);
        //        return _treePath;
        //    }
        //}

	    private string _name;
		public string ItemPath
		{
			get { return _model.NormalisedTag; }
		}

		private Image _icon;
		public Image Icon
		{
			get { return _icon; }
			set { _icon = value; }
		}

		public string Name
		{
			get { return _name; }
		}

        private ControlItemNode _parent;
        public ControlItemNode Parent
		{
			get { return _parent; }
			set { _parent = value; }
		}

        public bool IsAllowRead
        {
            get { return _model.PermissionType.HasFlag(PermissionType.ReadOnly); }
            set
            {
                if(!value && (_model.LockedPermissionType.HasFlag(PermissionType.ReadOnly)))
                    return;
                SetFlag( PermissionType.ReadOnly, value );
            }
        }

        public bool IsAllowAdd
        {
            get { return _model.PermissionType.HasFlag(PermissionType.Add); }
            set
            {
                if (!value && (_model.LockedPermissionType.HasFlag(PermissionType.Add)))
                    return;
                SetFlag( PermissionType.Add, value );
            }
        }

	    public bool IsAllowEdit
        {
            get { return _model.PermissionType.HasFlag( PermissionType.Change ); }
            set
            {
                if (!value && (_model.LockedPermissionType.HasFlag(PermissionType.Change)))
                    return;
                SetFlag( PermissionType.Change, value );
            }
        }

        public bool IsAllowRemove
        {
            get { return _model.PermissionType.HasFlag( PermissionType.Delete ); }
            set
            {
                if (!value && ( _model.LockedPermissionType.HasFlag( PermissionType.Delete ) ))
                    return;
                SetFlag(PermissionType.Delete, value);
            }
        }

	    private void NotifyNodeChanged( )
        {
            if (Owner != null)
                Owner.OnNodesChanged( this );
        }

	    public PermissionType ItemPermissionType
	    {
	        get { return _model.PermissionType; }

	    }

	    public PermissionType LockedItemPermissionType
	    {
            get { return _model.LockedPermissionType; }	        
	    }

	    public void SetFlag(PermissionType flag, bool setter)
	    {
            var oldFlags = _model.PermissionType;
            if (setter)
            {
                if (_model.PermissionType.HasFlag( flag ))
                    return;
                _model.PermissionType |= flag;
                if (flag != PermissionType.None && flag != PermissionType.ReadOnly )
                    _model.PermissionType |= PermissionType.ReadOnly;
                if (flag == PermissionType.Add && !_model.PermissionType.HasFlag(PermissionType.Change))
                    _model.PermissionType |= PermissionType.Change;
            }
            else
            {
                if (_model.LockedPermissionType.HasFlag(flag))
                    return;

                if (flag == PermissionType.Change)
                    _model.PermissionType &= ~PermissionType.Add;

                if(flag == PermissionType.ReadOnly)
                    _model.PermissionType = PermissionType.None;
                else
                    _model.PermissionType &= ~flag;
            }

            //if nothing changed
            if(oldFlags == _model.PermissionType)
                return;
            
            MarkChanges(oldFlags);
            
            NotifyNodeChanged();

            if (_disableUpdateRelatives)
                return;

            DisableUpdateRelatives = true;
            UpdateChilds(flag,setter);
	        UpdateParent(!setter);
            DisableUpdateRelatives = false;

	    }

	    public void Reset()
	    {
	        _model.PermissionType = PermissionType.None;
	        _model.LockedPermissionType = PermissionType.None;
            Owner.OnNodesChanged(this);
	    }

	    private void MarkChanges(PermissionType oldFlags)
	    {
            if(oldFlags == _model.PermissionType)
                return;
	        PermissionType startPermissionState = LockedItemPermissionType | PermissionType.None;
            if (_model.PermissionType == startPermissionState )
	        {
	            Owner.Changes.Remove(this._model);
	        }
            else if ( oldFlags == startPermissionState && _model.PermissionType != startPermissionState ) 
	        {
	            Owner.Changes.Add(_model);
	        }
	        else
	        {
	            Owner.Changes.MarkAsChanged(_model);
	        }
	    }

	    public void LockPermission(PermissionType permission)
	    {
	        _model.LockedPermissionType = permission;
	    }

	    public static bool DisableUpdateRelatives
        {
            get { return _disableUpdateRelatives; }
            set { _disableUpdateRelatives = value; }
        }


	    private void UpdateParent(bool isConstriction = true)
	    {
	        ControlItemNode parent = this;
	        do
	        {
	            parent = parent.Parent;// Owner.GetNode(Path.GetDirectoryName(this.ItemPath));
                
                if(parent == null)
                    break;
                
                parent.SetFlag(this.ItemPermissionType, true);

                var childs = _owner.GetChildByPath(parent.ItemPath );

                if (isConstriction)
	            {
	                var filter = parent.ItemPermissionType;
	                PermissionType permissionSum = PermissionType.None;
	                var shouldUpdate = true;
	                foreach (var child in childs)
	                {
	                    permissionSum |= child.ItemPermissionType;
	                    if (permissionSum.HasFlag(filter))
	                    {
	                        shouldUpdate = false;
	                        break;
	                    }
	                }
	                if (shouldUpdate)
	                    parent.SetFlag(~permissionSum, false);
	            }
	        } while (true);
	    }

	    private void UpdateChilds(PermissionType flag, bool setter)
	    {
	        var childs = _owner.GetChildByPath(ItemPath,true);
	        foreach (var child in childs)
	        {
	            if (flag == PermissionType.ReadOnly && !setter)
	            {
                    child.SetFlag( ~PermissionType.None,false ); 
	            }
	            else
	            {
                    child.SetFlag(flag, setter);
	            }
            }
	    }

	    private AppControlItemsModel _owner;
        public AppControlItemsModel Owner
		{
			get { return _owner; }
			set { _owner = value; }
		}

        public ControlItemNode( Permission model,AppControlItemsModel owner, ControlItemNode parentControlItem = null )
	    {
	        _model = model;
	        _name = Path.GetFileName(_model.Tag);
	        _parent = parentControlItem;
            _owner = owner;
	    }

		public override string ToString()
		{
			return _name;
		}
	}
}

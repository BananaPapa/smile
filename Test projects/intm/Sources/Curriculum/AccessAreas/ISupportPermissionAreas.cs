﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Curriculum;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Filtering;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Layout;
using DevExpress.XtraLayout;
using MessageBox = Eureca.Integrator.Utility.Forms.MessageBox;

namespace Eureca.Integrator.Common
{
    public interface ISupportAccessAreas : IHasHiddenChildren
    {
        Control Control { get; }

        ISupportAccessAreas HierarchyParent { get; set; }

        string PrefixTag { get; set; }

        bool HasInnerHideLogic { get; set; }

        void AddControlRights( string controlName, PermissionType permission );

        string NodeTag { get;}
    }

    public interface IHasHiddenChildren
    {
        IEnumerable<ISKOCBCTDNBTIDED> GetHiddenChilds( bool withLazy );
    }

    public class FormWithAccessAreas : XtraForm, ISupportAccessAreas
    {
        protected Dictionary<string, PermissionType> _permissionsDic;

        private bool? isAllowed = null;
        
        protected bool IsAllowed
        {
            get
            {
                return isAllowed ?? Program.PermissionManager.CheckAllow(this);
            }
        }


        private bool _hasInnerHideLogic = false;

        public bool HasInnerHideLogic
        {
            get
            {
                return _hasInnerHideLogic;
            }
            set
            {
                if (value == _hasInnerHideLogic)
                    return;
                _hasInnerHideLogic = value;
                if (value)
                {
                    _permissionsDic = _permissionsDic ?? new Dictionary<string, PermissionType>( );
                }
                else
                {
                    _permissionsDic = null;
                }
            }
        }

        public void AddControlRights( string controlName, PermissionType permission )
        {
            if (!_permissionsDic.ContainsKey( controlName ))
            {
                _permissionsDic[controlName] = permission;
            }
        }

        public bool ManualFilter
        {
            set;
            get;
        }

        protected FormWithAccessAreas( )
        {
            this.Load += FormWithAccessAreas_Load;
        }

        void FormWithAccessAreas_Load( object sender, EventArgs e )
        {
            if(Site != null && Site.DesignMode)
                return;
            if(!ManualFilter)
            {
                if (!IsAllowed)
                {
                    this.Visible = false;
                    this.DialogResult = DialogResult.Cancel;
                    this.Close( );
                    MessageBox.ShowValidationError("Недостаточно прав для просмотра формы.",this.Parent);
                }
                else
                    Program.PermissionManager.ApplyFilter(this);
            }
        }

        public Control Control
        {
            get { return this; }
        }

        public ISupportAccessAreas HierarchyParent
        {
            get;
            set;
        }

        public virtual IEnumerable<ISKOCBCTDNBTIDED> GetHiddenChilds( bool withLazy )
        {
            return new ISKOCBCTDNBTIDED[0];
        }

        public string PrefixTag
        {
            get;
            set;
        }

        public string NodeTag
        {
            get
            {
                var controlTag = Control.Tag as string;
                return PrefixTag == null ? controlTag : Path.Combine( PrefixTag, controlTag );
            }
        }

    }

    public class ControlWithAccessAreas : XtraUserControl, ISupportAccessAreas
    {

        private Dictionary<string, PermissionType> _permissionsDic;


        private bool _hasInnerHideLogic = false;

        public bool HasInnerHideLogic
        {
            get
            {
                return _hasInnerHideLogic;
            }
            set
            {
                if (value == _hasInnerHideLogic)
                    return;
                _hasInnerHideLogic = value;
                if (value)
                {
                    _permissionsDic = _permissionsDic ?? new Dictionary<string, PermissionType>( );
                }
                else
                {
                    _permissionsDic = null;
                }

            }
        }

        public void AddControlRights( string controlName, PermissionType permission )
        {
            if (!_permissionsDic.ContainsKey( controlName ))
            {
                _permissionsDic[controlName] = permission;
            }
        }
        public bool ManualFilter { set; get; }

        public ControlWithAccessAreas()
        {
            this.Load += ControlWithAccessAreas_Load;
        }

        private void ControlWithAccessAreas_Load(object sender, EventArgs e)
        {
            if (Site != null && Site.DesignMode)
                return;
            if (!ManualFilter)
                Program.PermissionManager.ApplyFilter(this);
        }

        public Control Control
        {
            get { return this; }
        }

        public ISupportAccessAreas HierarchyParent { get; set; }

        public virtual IEnumerable<ISKOCBCTDNBTIDED> GetHiddenChilds(bool withLazy)
        {
            return new ISKOCBCTDNBTIDED[0];
        }

        public string PrefixTag { get; set; }

        public string NodeTag
        {
            get
            {
                var controlTag = Control.Tag as string;
                if(controlTag == null)
                    throw new Exception("Every node of control tree must have a name/tag");
                return PrefixTag == null ? controlTag : Path.Combine(PrefixTag, controlTag);
            }
        }
    }

}

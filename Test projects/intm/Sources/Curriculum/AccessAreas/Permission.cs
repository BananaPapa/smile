﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using DevExpress.XtraScheduler.UI;
using Eureca.Integrator.Common;
using Eureca.Utility.Extensions;
using Integrator.Curriculum.Constants;

namespace Curriculum.AccessAreas
{

    public class Permission
    {
        private int _id = -1;

        
        static readonly Dictionary<PermissionType, string> _permssionTags = new Dictionary<PermissionType, string>( );

        public int TagId
        {
            get { return _id; }
        }

        public bool IsRelative { get; set; }        

        static Permission( )
        {
            foreach (var value in Enum.GetValues( typeof( PermissionType ) ).Cast<PermissionType>( ))
            {
                _permssionTags.Add( value, value.GetEnumAttribute<TagAttribute>( ).First( ).Tag );
            }
        }

        private string _tag;

        public string Tag 
        {
            get { return _tag; }
            private set
            {
                _tag = value;
                NormalisedTag = Normalise(value);
            } 
        }

        public static string Normalise(string value)
        {
            return SolveRelativePath( RemoveTagsFromPath( RemoveUnmarkedNodes( value ) ) );
        }

        public string NormalisedTag { get; private set; }

        private PermissionType _permissionType = PermissionType.None;

        private PermissionType _lockedPermissionType = PermissionType.None;

        public PermissionType PermissionType
        {
            get { return _permissionType; }
            set { _permissionType = value; }
        }

        public PermissionType LockedPermissionType
        {
            get { return _lockedPermissionType; }
            set { _lockedPermissionType = value; }
        }

        public Permission( string tag, PermissionType permissionType , int tagId = -1 )
        {
            Tag = tag;
            IsRelative = tag.Contains("..");
            _id = tagId;
            _permissionType = permissionType;
        }

        public Permission( RolesPermission rolePermission )
        {
            Tag tag = rolePermission.Tag;
            Tag = tag.Tag1;
            _id = tag.TagId;
            _permissionType = (PermissionType)rolePermission.Permission;
        }

        public override string ToString( )
        {
            return GetTagString( );
        }

        public string GetTagString( )
        {
            StringBuilder sb = new StringBuilder( );
            sb.Append( NormalisedTag );
            sb.Append( Constants.splitString );
            foreach (var pairs in _permssionTags)
            {
                if (_permissionType.HasFlag( pairs.Key ))
                {
                    sb.Append( pairs.Value );
                }
            }
            return sb.ToString( ).TrimEnd( Constants.splitCharapter );
        }

        public bool CheckAllow( Permission permission )
        {
            if (this.NormalisedTag != permission.NormalisedTag)
                return false;
            return _permissionType.HasFlag( permission._permissionType ) && _permissionType.HasFlag( _permissionType );
        }

        #region Static methods
        public static Permission Parse(string permissionString)
        {
            var parts = SplitTag(permissionString);
            if (parts.Length == 0)
                return null;
            PermissionType permissions = PermissionType.None;
            if (parts.Length > 1)
            {
                var tags = parts[1];
                foreach (var pairs in _permssionTags)
                {
                    if (tags.Contains(pairs.Value))
                        permissions |= pairs.Key;
                }
            }
            return new Permission(parts[0], permissions);
        }

        private static string[] SplitTag(string permissionString)
        {
            int tagStartIndex = permissionString.LastIndexOf(Constants.splitCharapter);
            int separatorStartIndex = permissionString.LastIndexOf(Path.DirectorySeparatorChar);
            if (tagStartIndex > 0 && tagStartIndex > separatorStartIndex)
                return new[]
                {permissionString.Substring(0, tagStartIndex), permissionString.Substring(tagStartIndex + 1)};
            return new[] {permissionString};
        }

        public static string GetTag(string permissionString)
        {
            return SplitTag(permissionString)[0];
        }

        public static string RemoveTagsFromPath(string permissionString)
        {
            return Regex.Replace(permissionString, @"#\w+", "");
        }

        public static string RemoveUnmarkedNodes(string permissionString)
        {
            return permissionString.Replace("\\#", "");
        }

        public static string SolveRelativePath(string permissionPath)
        {
            var path = String.Copy( permissionPath );
            while (path.Contains( ".." ))
            {
                path = Regex.Replace( path, @"(([\w\s#]\\)*)[\w\s#]+\\\.\.\\([\w\s\\#]*)", "$2$3" );
            }
            return path;
        }

        public Permission(Tag tag):this(tag.Tag1,PermissionType.None)
        {
            _id = tag.TagId;
        }

        #endregion

    }

}

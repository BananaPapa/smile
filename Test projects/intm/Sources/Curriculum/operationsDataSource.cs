using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Linq;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Management;
using System.Reflection;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Aga.Controls.Tree.NodeControls;
using Curriculum.AccessAreas;
using Curriculum.Controls;
using Curriculum.Forms;
using DevExpress.Skins;
using DevExpress.UserSkins;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Layout;
using DevExpress.XtraLayout;
using DevExpress.XtraRichEdit.API.Word;
using Eureca.Integrator.Common;
using Eureca.Integrator.Utility;
using Eureca.Integrator.Utility.Settings;
using Eureca.Professional.BaseComponents;
using Eureca.Professional.CommonClasses;
using Eureca.Utility;
using Eureca.Utility.Extensions;
using Microsoft.Office.Interop.Word;
using MessageBox = Eureca.Integrator.Utility.Forms.MessageBox;
using log4net;
using Application = System.Windows.Forms.Application;

namespace Curriculum
{
    /// <summary>
    /// Represents program with it's entry point.
    /// </summary>
    public static class Program
    {
        private static TestPassingDispatcher _testPassingDispatcher ;
        private static MessageDispatcher _messageDispatcher;

        private static readonly ILog Log = LogManager.GetLogger( typeof( Program ) );
        /// <summary>
        /// Object to lock data context instance.
        /// </summary>
        private static readonly object InstanceLock = new object( );

        /// <summary>
        /// Object to lock professional data context instance.
        /// </summary>
        private static readonly object ProfessionalInstanceLock = new object( );

        /// <summary>
        /// Application settings.
        /// </summary>
        private static readonly Properties.Settings ApplicationSettings = new Properties.Settings( );

        /// <summary>
        /// Instance of data context.
        /// </summary>
        private static CurriculumDataContext _dataContextInstance;

        /// <summary>
        /// Instance of professional data context.
        /// </summary>
        private static ProfessionalDataContext _professionalDataContextInstance;

        /// <summary>
        /// Gets data context instance.
        /// </summary>
        public static CurriculumDataContext DbSingletone
        {
            get
            {
                lock (InstanceLock)
                {
                    if (_dataContextInstance == null)
                    {
                        string connString =ConnectionStringManager.GetProviderConnectionString(ConnectionStringManager.GetLocalConnectionString( IntegratorDb.Curriculum ));
                        if (String.IsNullOrEmpty(connString))
                        {
                            connString =
                                ConnectionStringManager.GetProviderConnectionString(
                                    CommonConfig.Load().ProfessionalConnectionString);
                            if (String.IsNullOrEmpty(connString))
                                throw new Exception(
                                    "������ ����������� � �� ������������. ����������� � ������� �� �� ���������");
                        }
                        _dataContextInstance = new CurriculumDataContext( connString );

                    }
                    return _dataContextInstance;
                }
            }
        }

        public static CurriculumDataContext Db
        {
            get
            {
                string connString = ConnectionStringManager.GetProviderConnectionString( ConnectionStringManager.GetLocalConnectionString( IntegratorDb.Curriculum ) );
                if (String.IsNullOrEmpty( connString ))
                {
                    connString =
                        ConnectionStringManager.GetProviderConnectionString(
                            CommonConfig.Load( ).ProfessionalConnectionString );
                    if (String.IsNullOrEmpty( connString ))
                        throw new Exception(
                            "������ ����������� � �� ������������. ����������� � ������� �� �� ���������" );
                }
                return new CurriculumDataContext( connString );
            }

        }


        private static PermissionManager _permissionManager;

        public static PermissionManager PermissionManager
        {
            get
            {
                if (_permissionManager == null)
                {
                    _permissionManager = new PermissionManager( ( ) => Program.DbSingletone.RolesPermissions );
                }
                return _permissionManager;
            }
        }


        /// <summary>
        /// Gets professional data context instance.
        /// </summary>
        public static ProfessionalDataContext ProfessionalDb
        {
            get
            {
                lock (ProfessionalInstanceLock)
                {
                    if (_professionalDataContextInstance == null)
                    {
                        string connString =ConnectionStringManager.GetProviderConnectionString(ConnectionStringManager.GetLocalConnectionString( IntegratorDb.Professional ));

                        if (String.IsNullOrEmpty(connString))
                        {
                            connString = ConnectionStringManager.GetProviderConnectionString(CommonConfig.Load().ProfessionalConnectionString);
                            if (String.IsNullOrEmpty(connString))
                                throw new Exception(
                                    "������ ����������� � �� ������������. ����������� � ������� �� �� ���������");
                        }

                        _professionalDataContextInstance = new ProfessionalDataContext( connString );
                    }
                    return _professionalDataContextInstance;
                }
            }
        }

        private static User _currentUser;

        /// <summary>
        /// Gets or sets current user.
        /// </summary>
        public static User CurrentUser
        {
            get
            {
                return _currentUser;
            }
            set
            {
                _currentUser = value;
                var currentRole = DbSingletone.UsersRoles.SingleOrDefault( role => role.UserId == _currentUser.UserId );
                if (currentRole == null)
                    throw new InvalidOperationException( "unknown user!!!" );
                PermissionManager.CurrentRole = DbSingletone.Roles.Single( role => role.RoleId == currentRole.RoleId );
            }
        }

        /// <summary>
        /// Gets application settings.
        /// </summary>
        internal static Properties.Settings Settings
        {
            get { return ApplicationSettings; }
        }

        /// <summary>
        /// Gets application main form.
        /// </summary>
        public static StartForm StartForm { get; private set; }

        public static TestPassingDispatcher TestPassingDispatcher
        {
            get { return _testPassingDispatcher; }
        }

        public static MessageDispatcher MessageDispatcher
        {
            get { return _messageDispatcher; }
        }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main( )
        {
            Log.Info( "Curriculum Application Start" );

            Trace.WriteLine(MethodBase.GetCurrentMethod().DeclaringType.Name);
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            Application.ThreadException += Application_ThreadException;

            InitLocalization( );
            InitSkins( );
            ConnectionStringManager.SynchronizeConnectionString( IntegratorDb.Curriculum );
            ConnectionStringManager.SynchronizeConnectionString( IntegratorDb.Professional );

            Application.EnableVisualStyles( );
            Application.SetCompatibleTextRenderingDefault( false );

            if (!AppActivation.IsActivation( Application.StartupPath )
                && !AppActivation.Activation( Application.StartupPath ))
            {
                return;
            }

            if (!( CheckDatabase( IntegratorDb.Professional ) && CheckDatabase( IntegratorDb.Curriculum ) && DbSingletone.DatabaseExists( ) ))
            {
                Application.Exit( );
                return;
            }

            RunnerApplication.RunUpdater( true,false );
                    
            // Remove expired events.
            Setting expirationPeriodSetting =
                Setting.GetSetting( Properties.Settings.Default.EventExpirationPeriodSettingKey );

            int result;
            if (expirationPeriodSetting != null && !String.IsNullOrEmpty( expirationPeriodSetting.Value ) &&
                Int32.TryParse( expirationPeriodSetting.Value, out result ))
            {
                DbSingletone.Events.DeleteAllOnSubmit( from evnt in DbSingletone.Events
                                             where
                                                 evnt.EndDateTime.Date < DateTime.Today &&
                                                 ( DateTime.Today - evnt.EndDateTime ).TotalDays > result
                                             select evnt );

                try
                {
                    DbSingletone.SubmitChanges( );
                }
                catch (DbException ex)
                {
                    HandleDbException( ex );
                }
            }

            // Remove expired exception schedules.
            DbSingletone.ExceptionSchedules.DeleteAllOnSubmit( from exceptionSchedule in DbSingletone.ExceptionSchedules
                                                     where
                                                         exceptionSchedule.Date.Date < DateTime.Today &&
                                                         ( DateTime.Today - exceptionSchedule.Date.Date ).TotalDays > 365
                                                     select exceptionSchedule );

            try
            {
                DbSingletone.SubmitChanges( );
            }
            catch (DbException ex)
            {
                HandleDbException( ex );
            }

            PermissionManager.OnGridColumnFiltering += PermissionManager_OnGridColumnFiltering;
            PermissionManager.OnGridFiltering += PermissionManager_OnGridFiltering;
            PermissionManager.OnLayoutViewColumnFiltering += PermissionManager_OnLayoutViewColumnFiltering;
            StartForm = new StartForm( );
            StartForm.ManualFilter = true;
            
            if (!DbSingletone.Tags.Any())
            {
                var tree = PermissionManager.GenerateTree( StartForm as ISupportAccessAreas );
                foreach (var node in tree.Select( Permission.Normalise ).Distinct())
                {
                    DbSingletone.Tags.InsertOnSubmit( new Tag( ) { Tag1 = Permission.Normalise( node ) } );   
                }
                DbSingletone.SubmitChanges();
            }

            _testPassingDispatcher = new TestPassingDispatcher( Program.ProfessionalDb );
            _messageDispatcher = new MessageDispatcher();
            
            _testPassingDispatcher.Timeout = TimeSpan.FromSeconds( 10 );
            Application.Run( StartForm );
        }

        #region Permissions

        private static void PermissionManager_OnLayoutViewColumnFiltering(object sender,
            AccessAreaControlArgs<LayoutViewColumn> e)
        {
            if (!e.Control.ReadOnly && e.RolePermission.HasFlag(PermissionType.Change))
            {
                e.Control.OptionsColumn.ReadOnly = false;
            }
            if (e.Control.OptionsColumn.AllowEdit)
            {
                e.Control.OptionsColumn.AllowEdit = e.RolePermission.HasFlag(PermissionType.Change);
            }
        }

        private static void PermissionManager_OnGridFiltering(object sender, AccessAreaControlArgs<GridControl> e)
        {

            if (e.Control.UseEmbeddedNavigator)
            {
                var controlNavigatorButtons = e.Control.EmbeddedNavigator.Buttons;
                HideNavigationButtons(e, PermissionType.Change, controlNavigatorButtons.Edit);
                HideNavigationButtons(e, PermissionType.Change, controlNavigatorButtons.EndEdit );
                HideNavigationButtons(e, PermissionType.Change, controlNavigatorButtons.CancelEdit );
                HideNavigationButtons(e, PermissionType.Add, controlNavigatorButtons.Append);
                HideNavigationButtons(e, PermissionType.Delete, controlNavigatorButtons.Remove);
                foreach ( var navigatorCustomButton in controlNavigatorButtons.CustomButtons.Cast<NavigatorCustomButton>())
                {
                    HideCustomNavigationButtons(e, navigatorCustomButton);
                }
            }
            if (e.RolePermission == PermissionType.ReadOnly)
            {
                e.Control.AllowDrop = false;
            }
        }

        private static void HideNavigationButtons(AccessAreaControlArgs<GridControl> e, PermissionType flag,
            NavigatorButton navButton)
        {
            if (navButton.Visible &&
                !e.RolePermission.HasFlag(flag))
                navButton.Visible = false;
        }

        private static void HideCustomNavigationButtons(AccessAreaControlArgs<GridControl> e,
            NavigatorCustomButton navButton)
        {
            var permissionString = navButton.Tag as string;
            if (permissionString == null)
                return;
            var requeriments = Permission.Parse(permissionString).PermissionType;
            if (navButton.Visible &&
                !e.RolePermission.HasFlag(requeriments))
                navButton.Visible = false;
        }

        private static void PermissionManager_OnGridColumnFiltering(object sender, AccessAreaControlArgs<GridColumn> e)
        {
            if (e.Control.OptionsColumn.AllowEdit  && !e.RolePermission.HasFlag(PermissionType.Change))
            {
                e.Control.OptionsColumn.ReadOnly = true;
                e.Control.OptionsColumn.AllowEdit = false;
            }
        }

        #endregion


        private static void ShowTree( )
        {
            ShowTree(DbSingletone.Tags.ToList());
            //var form = new EditRoleForm( Program.Db.Roles.First( ), Program.Db.Roles );
            //form.ShowDialog( );
        }


        private static void ShowTree(IEnumerable<Tag> tree)
        {
            var accessAreasTree = new AccessAreasTreeControl() {Dock = DockStyle.Fill};
            accessAreasTree.Init(tree);
            var form = new Form() {WindowState = FormWindowState.Maximized};
            form.Controls.Add(accessAreasTree);
            form.ShowDialog();
        }

        private static string CreateScript(IEnumerable<string> tree)
        {
            StringBuilder sb = new StringBuilder();
            string script = null;
            sb.AppendLine("Insert into [Tags]  ([Tag]) values");
            foreach (var key in tree.Select(Permission.RemoveTagsFromPath).Distinct().OrderBy(s => s))
            {
                sb.AppendLine("('{0}'), ".FormatString(key));
            }
            try
            {
                script = sb.ToString();
                var index = script.LastIndexOf( ',' );
                if (index >= 0)
                    script = script.Remove( index, 1 );
               
            }
            catch (Exception e)
            {
                Trace.WriteLine(e.ToString());
            }
            return script;
        }

        private static bool CheckDatabase( IntegratorDb integratorDb, bool saveConnectionString = true )
        {
            string dbName = integratorDb.GetEnumDescription( ),dbConnString = null;

            if (DatabaseHelper.CheckConnection(integratorDb)) return true;

            if (MessageBox.Confirm( String.Format( "���� ������ '{0}' �� �������. ������� ���� ������?", dbName ) ))
            {
                #region create database
                Eureca.Integrator.Utility.Forms.MessageBox.ShowInfo( "������������ � �� 'Master'" );
                if (DatabaseHelper.ChangeConnection( integratorDb,out dbConnString, new Func<string, bool>( el => el.Contains( "master" ) ) ) !=
                    DialogResult.OK)
                {
                    return false;
                }
                //string currentConnectionString = DatabaseHelper.LastSuccessfulConnectionString( integratorDb );
                try
                {
                    Eureca.Integrator.Utility.Forms.MarqueeProgressForm.Show( String.Format( "�������� ���� ������ '{0}'...", dbName ), ( form ) =>
                    {
                        var scriptFilePath = Path.Combine( Environment.CurrentDirectory, integratorDb.GetEnumDescription( ) + "2008.sql" );
                        Exception ex = null;
                        if (!System.IO.File.Exists( scriptFilePath ))
                             throw new Exception( "�� ������ ������ �������� ��", ex );
                        if (!DatabaseHelper.ExecFile( scriptFilePath, dbConnString, out ex ))
                        {
                            MessageBox.ShowError( "������ ��������� ��. �������� ��������������." );
                            Log.Error(ex.ToString());
                        }
                    } );
                }
                catch (Exception ex)
                {
                    Log.Error( ex is Eureca.Integrator.Utility.Forms.MarqueeProgressFormException ? ex.InnerException.Message : ex.Message );
                    MessageBox.ShowError( ex is Eureca.Integrator.Utility.Forms.MarqueeProgressFormException ? ex.InnerException.Message : ex.Message );
                    return false;
                }
                #endregion
            }
            MessageBox.ShowInfo( String.Format( @"������������ � �� '{0}'", dbName ) );
            if (DatabaseHelper.ChangeConnection( integratorDb,out dbConnString, new Func<string, bool>( el => el.Contains( dbName ) ), saveConnectionString ) == DialogResult.Cancel 
                || !DatabaseHelper.CheckConnection( integratorDb ))
                return false;
            if (integratorDb == IntegratorDb.Curriculum)
            {
                if (RunnerApplication.UpdateSettingsContext == null)
                    RunnerApplication.InitProfessionalContext( ConnectionStringManager.GetProviderConnectionString( ConnectionStringManager.GetLocalConnectionString( IntegratorDb.Professional ) ) );
                RunnerApplication.UpdateSettingsContext.CurriculumConnectionString = dbConnString;
            }
            return true;
        }

        private static void Application_ThreadException( object sender, ThreadExceptionEventArgs e )
        {
            HandleUnhandledException( e.Exception );
        }

        private static void CurrentDomain_UnhandledException( object sender, UnhandledExceptionEventArgs e )
        {
            HandleUnhandledException( e.ExceptionObject as Exception );
        }

        /// <summary>
        /// Handles unhandled exception.
        /// </summary>
        /// <param name="e">DB exception.</param>
        public static void HandleUnhandledException( Exception e )
        {
            if (e != null)
            {
                Log.Error( e.ToString( ) );
            }

            XtraMessageBox.Show(
                "��� ������ ���������� �������� ������!" + Environment.NewLine +
                "���������� � ������ ����������� ���������.",
                "������ ����������", MessageBoxButtons.OK, MessageBoxIcon.Error );
        }

        /// <summary>
        /// Initializes program localization.
        /// </summary>
        private static void InitLocalization( )
        {
            Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo( "ru-RU" );
            Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo( "ru-RU" );
        }

        /// <summary>
        /// Initializes program skins.
        /// </summary>
        private static void InitSkins( )
        {
            OfficeSkins.Register( );
            BonusSkins.Register( );
            SkinManager.EnableFormSkins( );
            SkinManager.EnableMdiFormSkins( );
        }


        /// <summary>
        /// Reinitializes Curriculum data context using specified connection string.
        /// </summary>
        /// <param name="connecionString">Custom connection string.</param>
        public static void SetCustomConnectionString( string connecionString )
        {
            lock (InstanceLock)
            {
                _dataContextInstance = new CurriculumDataContext( connecionString );
            }
        }

        /// <summary>
        /// Discards all data changes in data context.
        /// </summary>
        public static void DiscardDataChanges( )
        {
            lock (InstanceLock)
            {
                if (_dataContextInstance != null)
                {
                    try
                    {
                        ChangeSet changes = _dataContextInstance.GetChangeSet( );

                        // Discard all inserts.
                        foreach (object insertion in changes.Inserts)
                        {
                            _dataContextInstance.GetTable( insertion.GetType( ) ).DeleteOnSubmit( insertion );
                        }

                        // Discard all deletes.
                        foreach (object deletion in changes.Deletes)
                        {
                            _dataContextInstance.GetTable( deletion.GetType( ) ).InsertOnSubmit( deletion );
                        }

                        // Discard all updates.
                        var updatedTables = new List<ITable>( );

                        foreach (object update in changes.Updates)
                        {
                            ITable tbl = _dataContextInstance.GetTable( update.GetType( ) );

                            // Make sure not to refresh the same table twice.
                            if (updatedTables.Contains( tbl ))
                            {
                                continue;
                            }

                            updatedTables.Add( tbl );

                            _dataContextInstance.Refresh( RefreshMode.OverwriteCurrentValues, tbl );
                        }
                    }
                    catch (InvalidOperationException ex)
                    {
                        Log.Error( ex.ToString( ) );
                        // TODO: Handle the exception that occurs when editing the ships tools.
                    }
                }
            }
        }

        /// <summary>
        /// Handles DB exceptions.
        /// </summary>
        /// <param name="e">DB exception.</param>
        public static void HandleDbException( DbException e )
        {
            Log.Error( e.ToString( ) );

            XtraMessageBox.Show( "��� ������ � ����� ������ �������� ������!" + Environment.NewLine +
                                "��������� ��������� ��������� �� �������. �������� ���������� �� �����.",
                                "������ ���� ������", MessageBoxButtons.OK, MessageBoxIcon.Error );
        }

        /// <summary>
        /// Handles DB connection error.
        /// </summary>
        public static void HandleDbConnectionError( )
        {
            XtraMessageBox.Show( "��� ��������� � ���� ������ �������� ������." + Environment.NewLine +
                                "��������� ����������� ������� ��� ������ � ��������� ����������.",
                                "������ ���� ������", MessageBoxButtons.OK, MessageBoxIcon.Error );
        }

    }

}
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Linq;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace Curriculum
{
    /// <summary>
    /// Curriculum database data context.
    /// </summary>
    partial class CurriculumDataContext
    {
        /// <summary>
        /// Sends changes that were made to retrieved objects to the underlying database, and specifies the action to be taken if submission fails.
        /// </summary>
        /// <param name="failureMode">The action to be taken if submission fails.</param>
        public override void SubmitChanges(ConflictMode failureMode)
        {
            try
            {
                base.SubmitChanges(failureMode);
            }
            catch
            {
                Program.DiscardDataChanges();
                throw;
            }
        }

        public IQueryable GetDataSource(Type entityType)
        {
            try
            {
                return GetTable(entityType).AsQueryable();
            }
            catch (DbException e)
            {
                Program.HandleDbException(e);
            }

            return null;
        }

        public void RefreshDataSource(params Type[] entityTypes)
        {
            try
            {
                if (entityTypes != null && entityTypes.Length > 0)
                {
                    foreach (Type type in entityTypes)
                    {
                        Refresh(RefreshMode.OverwriteCurrentValues, GetTable(type));
                    }
                }
            }
            catch (DbException e)
            {
                Program.HandleDbException(e);
            }
        }
    }

    /// <summary>
    /// Alias entity.
    /// </summary>
    partial class Aliase
    {
        #region Delegates

        /// <summary>
        /// Delegate used to apply alias to objects.
        /// </summary>
        /// <param name="aliasValue">Alias value to be applied.</param>
        public delegate void ApplyAliasDelegate(string aliasValue);

        #endregion

        #region Aliases' Default Values

        /// <summary>
        /// Aliases default values.
        /// </summary>
        private static readonly Dictionary<string, string> DefaultValues
            = new Dictionary<string, string>
                  {
                      {"Ship", "�������"},
                      {"Ships", "�������"},
                      {"River", "����"},
                      {"Rivers", "����"},
                      {"ShipLine", "�����������"},
                      {"ShipLines", "�����������"},
                      {"ChildNickName", "������� ��������"},
                      {"Status", "������"},
                      {"Statuses", "�������"},
                      {"QualInfo", "���������������� ������"},
                      {"Qualification", "������������"},
                      {"Qualifications", "������������"},
                      {"Education", "�����������"},
                      {"Educations", "�����������"},
                      {"Degree", "������ �������"},
                      {"Degrees", "������ �������"},
                      {"CadreInfo", "�������� ����������"},
                      {"Position", "���������"},
                      {"Positions", "���������"},
                      {"Department", "���������"},
                      {"Departments", "���������"},
                      {"Affiliation", "��������������"},
                      {"Affiliations", "��������������"},
                      {"Appointment", "����������"},
                      {"AppointmentOrder", "������ � ����������"},
                      {"RequestOrder", "������"},
                      {"TripCount", "���������� �����"},
                      {"Period", "����"},
                      {"ContactInfo", "���������� ����������"},
                      {"Roles", "����"},
                      {"Tags", "����"},
                  };

        #endregion

        /// <summary>
        /// Gets alias by key.
        /// </summary>
        /// <param name="key">Key of alias.</param>
        /// <returns>Alias value or null.</returns>
        private static string GetByKey(string key)
        {
            string value = null;

            try
            {
                value = Program.DbSingletone.Aliases.Single(a => a.AliasKey == key).Value;
            }
            catch (InvalidOperationException)
            {
                // No such alias.
            }
            finally
            {
                if (value == null && DefaultValues.ContainsKey(key))
                {
                    value = DefaultValues[key];
                }
            }

            return value;
        }

        /// <summary>
        /// Gets default value of alias.
        /// </summary>
        /// <param name="key">Key of alias.</param>
        /// <returns>Default value of alias.</returns>
        public static string GetDefaultValue(string key)
        {
            if (!String.IsNullOrEmpty(key) && DefaultValues.ContainsKey(key))
            {
                return DefaultValues[key];
            }

            return key;
        }

        /// <summary>
        /// Applies alias to objects using external delegate.
        /// </summary>
        /// <param name="aliasKey">Key of alias.</param>
        /// <param name="applyAliasDelegate">Delegate with required actions.</param>
        public static void ApplyAlias(string aliasKey, ApplyAliasDelegate applyAliasDelegate)
        {
            string alias = GetByKey(aliasKey);

            if (!String.IsNullOrEmpty(alias) && applyAliasDelegate != null)
            {
                applyAliasDelegate.Invoke(alias);
            }
        }
    }

    partial class Schedule
    {
        public override string ToString()
        {
            return Name;
        }

        public static void GetTimeRange(out TimeSpan? startTime, out TimeSpan? finishTime)
        {
            startTime = Program.DbSingletone.Schedules.Min(s => s.Start);
            finishTime = Program.DbSingletone.Schedules.Max(s => s.Finish);
        }
    }

    partial class ExceptionSchedule
    {
        public static ExceptionSchedule GetException(DateTime date)
        {
            return Program.DbSingletone.ExceptionSchedules.FirstOrDefault(es => es.Date == date);
        }
    }

    partial class DaySchedule
    {
        public static readonly Dictionary<DayOfWeek, string> DaysOfWeek = new Dictionary<DayOfWeek, string>
                                                                              {
                                                                                  {
                                                                                      System.DayOfWeek.Monday,
                                                                                      "�����������"
                                                                                      },
                                                                                  {System.DayOfWeek.Tuesday, "�������"},
                                                                                  {System.DayOfWeek.Wednesday, "�����"},
                                                                                  {System.DayOfWeek.Thursday, "�������"},
                                                                                  {System.DayOfWeek.Friday, "�������"},
                                                                                  {System.DayOfWeek.Saturday, "�������"},
                                                                                  {
                                                                                      System.DayOfWeek.Sunday,
                                                                                      "�����������"
                                                                                      },
                                                                              };

        public static string GetDayOfWeekName(int index)
        {
            try
            {
                return DaysOfWeek[(DayOfWeek)index];
            }
            catch
            {
                return index.ToString();
            }
        }

        public static Schedule GetSchedule(DateTime dateTime)
        {
            return Program.DbSingletone.DaySchedules.First(s => s.DayOfWeek == (int)dateTime.DayOfWeek).Schedule;
        }
    }

    partial class Holiday
    {
        private static DateTime GetStartDate(DateTime holidayStartDate, int year)
        {
            return SchedulerTools.CheckDateTime(year, holidayStartDate.Month, holidayStartDate.Day);
        }

        private static DateTime GetFinishDate(DateTime holidayStartDate, DateTime holidayFinishDate, int year)
        {
            return SchedulerTools.CheckDateTime(holidayStartDate.Month <= holidayFinishDate.Month
                                                    ? year
                                                    : year + 1, holidayFinishDate.Month, holidayFinishDate.Day);
        }

        public static Holiday GetCurrentHoliday(DateTime date)
        {
            Holiday holiday = Program.DbSingletone.Holidays.ToList().Where(
                h =>
                (h.Finish.HasValue &&
                 date >= GetStartDate(h.Start, date.Year) &&
                 date <= GetFinishDate(h.Start, h.Finish.Value, date.Year))
                ||
                date.Equals(GetStartDate(h.Start, date.Year))
                ).OrderByDescending(h => h.Finish).FirstOrDefault();

            if (holiday != null)
            {
                MoveHoliday(date.Year, holiday);
            }

            return holiday;
        }

        private static void MoveHoliday(int year, Holiday holiday)
        {
            holiday.Start = GetStartDate(holiday.Start, year);

            if (holiday.Finish.HasValue)
            {
                holiday.Finish = GetFinishDate(holiday.Start, holiday.Finish.Value, year);
            }
        }

        public void MoveTo(int year)
        {
            MoveHoliday(year, this);
        }
    }

    /// <summary>
    /// Course entity.
    /// </summary>
    partial class Course : ICourse
    {
    }

    /// <summary>
    /// Template course entity.
    /// </summary>
    partial class TemplateCourse : ICourse
    {
    }

    partial class Template : INameOwner, IComparable
    {
        #region IComparable Members

        /// <summary>
        /// Compares the current instance with another object of the same type.
        /// </summary>
        /// <param name="obj">An object to compare with this instance. </param>
        /// <returns>A 32-bit signed integer that indicates the relative order of the objects being compared.</returns>
        public int CompareTo(object obj)
        {
            if (obj == null || !(obj is Template))
            {
                return 1;
            }

            return String.Compare(Name, ((Template)obj).Name);
        }

        #endregion

        /// <summary>
        /// Returns template name.
        /// </summary>
        /// <returns>Template name.</returns>
        public override string ToString()
        {
            return Name;
        }
    }

    /// <summary>
    /// Phone type entity.
    /// </summary>
    partial class PhoneType : INameOwner, IComparable
    {
        #region IComparable Members

        /// <summary>
        /// Compares the current instance with another object of the same type.
        /// </summary>
        /// <param name="obj">An object to compare with this instance. </param>
        /// <returns>A 32-bit signed integer that indicates the relative order of the objects being compared.</returns>
        public int CompareTo(object obj)
        {
            if (obj == null || !(obj is PhoneType))
            {
                return 1;
            }

            return String.Compare(Name, ((PhoneType)obj).Name);
        }

        #endregion

        /// <summary>
        /// Returns phone type name.
        /// </summary>
        /// <returns>Phone type name</returns>
        public override string ToString()
        {
            return Name;
        }
    }

    /// <summary>
    /// Ship entity.
    /// </summary>
    partial class Ship : INameOwner, IComparable
    {
        #region IComparable Members

        /// <summary>
        /// Compares the current instance with another object of the same type.
        /// </summary>
        /// <param name="obj">An object to compare with this instance. </param>
        /// <returns>A 32-bit signed integer that indicates the relative order of the objects being compared.</returns>
        public int CompareTo(object obj)
        {
            return NameOwnerTools.CompareNameOwnerWithObject(this, obj);
        }

        #endregion

        /// <summary>
        /// Returns ship name.
        /// </summary>
        /// <returns>Ship name</returns>
        public override string ToString()
        {
            return Name;
        }
    }

    /// <summary>
    /// River entity.
    /// </summary>
    partial class River : INameOwner, IComparable
    {
        #region IComparable Members

        /// <summary>
        /// Compares the current instance with another object of the same type.
        /// </summary>
        /// <param name="obj">An object to compare with this instance. </param>
        /// <returns>A 32-bit signed integer that indicates the relative order of the objects being compared.</returns>
        public int CompareTo(object obj)
        {
            return NameOwnerTools.CompareNameOwnerWithObject(this, obj);
        }

        #endregion

        /// <summary>
        /// Returns river name.
        /// </summary>
        /// <returns>River name</returns>
        public override string ToString()
        {
            return Name;
        }
    }

    /// <summary>
    /// Condition entity.
    /// </summary>
    partial class Condition : INameOwner, IComparable
    {
        #region IComparable Members

        /// <summary>
        /// Compares the current instance with another object of the same type.
        /// </summary>
        /// <param name="obj">An object to compare with this instance. </param>
        /// <returns>A 32-bit signed integer that indicates the relative order of the objects being compared.</returns>
        public int CompareTo(object obj)
        {
            return NameOwnerTools.CompareNameOwnerWithObject(this, obj);
        }

        #endregion

        /// <summary>
        /// Returns condition name.
        /// </summary>
        /// <returns>Condition name</returns>
        public override string ToString()
        {
            return Name;
        }
    }

    /// <summary>
    /// Ship line entity.
    /// </summary>
    partial class ShipLine : INameOwner, IComparable
    {
        #region IComparable Members

        /// <summary>
        /// Compares the current instance with another object of the same type.
        /// </summary>
        /// <param name="obj">An object to compare with this instance. </param>
        /// <returns>A 32-bit signed integer that indicates the relative order of the objects being compared.</returns>
        public int CompareTo(object obj)
        {
            return NameOwnerTools.CompareNameOwnerWithObject(this, obj);
        }

        #endregion

        /// <summary>
        /// Returns ship line name.
        /// </summary>
        /// <returns>Ship line name</returns>
        public override string ToString()
        {
            return Name;
        }
    }

    /// <summary>
    /// Status entity.
    /// </summary>
    partial class Statuse : INameOwner, IComparable
    {
        #region IComparable Members

        /// <summary>
        /// Compares the current instance with another object of the same type.
        /// </summary>
        /// <param name="obj">An object to compare with this instance. </param>
        /// <returns>A 32-bit signed integer that indicates the relative order of the objects being compared.</returns>
        public int CompareTo(object obj)
        {
            return NameOwnerTools.CompareNameOwnerWithObject(this, obj);
        }

        #endregion

        /// <summary>
        /// Returns status name.
        /// </summary>
        /// <returns>Status name</returns>
        public override string ToString()
        {
            return Name;
        }
    }

    /// <summary>
    /// Qualification entity.
    /// </summary>
    partial class Qualification : INameOwner, IComparable
    {
        #region IComparable Members

        /// <summary>
        /// Compares the current instance with another object of the same type.
        /// </summary>
        /// <param name="obj">An object to compare with this instance. </param>
        /// <returns>A 32-bit signed integer that indicates the relative order of the objects being compared.</returns>
        public int CompareTo(object obj)
        {
            return NameOwnerTools.CompareNameOwnerWithObject(this, obj);
        }

        #endregion

        /// <summary>
        /// Returns qualification name.
        /// </summary>
        /// <returns>Qualification name</returns>
        public override string ToString()
        {
            return Name;
        }
    }

    /// <summary>
    /// Education entity.
    /// </summary>
    partial class Education : INameOwner, IComparable
    {
        #region IComparable Members

        /// <summary>
        /// Compares the current instance with another object of the same type.
        /// </summary>
        /// <param name="obj">An object to compare with this instance. </param>
        /// <returns>A 32-bit signed integer that indicates the relative order of the objects being compared.</returns>
        public int CompareTo(object obj)
        {
            return NameOwnerTools.CompareNameOwnerWithObject(this, obj);
        }

        #endregion

        /// <summary>
        /// Returns education name.
        /// </summary>
        /// <returns>Education name</returns>
        public override string ToString()
        {
            return Name;
        }
    }

    /// <summary>
    /// Degree entity.
    /// </summary>
    partial class Degree : INameOwner, IComparable
    {
        #region IComparable Members

        /// <summary>
        /// Compares the current instance with another object of the same type.
        /// </summary>
        /// <param name="obj">An object to compare with this instance. </param>
        /// <returns>A 32-bit signed integer that indicates the relative order of the objects being compared.</returns>
        public int CompareTo(object obj)
        {
            return NameOwnerTools.CompareNameOwnerWithObject(this, obj);
        }

        #endregion

        /// <summary>
        /// Returns degree name.
        /// </summary>
        /// <returns>Degree name</returns>
        public override string ToString()
        {
            return Name;
        }
    }

    /// <summary>
    /// Position entity.
    /// </summary>
    partial class Position : INameOwner, IComparable
    {
        #region IComparable Members

        /// <summary>
        /// Compares the current instance with another object of the same type.
        /// </summary>
        /// <param name="obj">An object to compare with this instance. </param>
        /// <returns>A 32-bit signed integer that indicates the relative order of the objects being compared.</returns>
        public int CompareTo(object obj)
        {
            return NameOwnerTools.CompareNameOwnerWithObject(this, obj);
        }

        #endregion

        /// <summary>
        /// Returns position name.
        /// </summary>
        /// <returns>Position name</returns>
        public override string ToString()
        {
            return Name;
        }
    }

    /// <summary>
    /// Department entity.
    /// </summary>
    partial class Department : INameOwner, IComparable
    {
        #region IComparable Members

        /// <summary>
        /// Compares the current instance with another object of the same type.
        /// </summary>
        /// <param name="obj">An object to compare with this instance. </param>
        /// <returns>A 32-bit signed integer that indicates the relative order of the objects being compared.</returns>
        public int CompareTo(object obj)
        {
            return NameOwnerTools.CompareNameOwnerWithObject(this, obj);
        }

        #endregion

        /// <summary>
        /// Returns department name.
        /// </summary>
        /// <returns>Department name</returns>
        public override string ToString()
        {
            return Name;
        }
    }

    /// <summary>
    /// Affliction entity.
    /// </summary>
    partial class Affiliation : INameOwner, IComparable
    {
        #region IComparable Members

        /// <summary>
        /// Compares the current instance with another object of the same type.
        /// </summary>
        /// <param name="obj">An object to compare with this instance. </param>
        /// <returns>A 32-bit signed integer that indicates the relative order of the objects being compared.</returns>
        public int CompareTo(object obj)
        {
            return NameOwnerTools.CompareNameOwnerWithObject(this, obj);
        }

        #endregion

        /// <summary>
        /// Returns affiliation name.
        /// </summary>
        /// <returns>Affiliation name</returns>
        public override string ToString()
        {
            return Name;
        }
    }

    /// <summary>
    /// Category entity.
    /// </summary>
    partial class Category : INameOwner, IComparable
    {
        #region IComparable Members

        /// <summary>
        /// Compares the current instance with another object of the same type.
        /// </summary>
        /// <param name="obj">An object to compare with this instance. </param>
        /// <returns>A 32-bit signed integer that indicates the relative order of the objects being compared.</returns>
        public int CompareTo(object obj)
        {
            return NameOwnerTools.CompareNameOwnerWithObject(this, obj);
        }

        #endregion

        /// <summary>
        /// Returns category name.
        /// </summary>
        /// <returns>Category name</returns>
        public override string ToString()
        {
            return Name;
        }
    }

    /// <summary>
    /// Addressee entity.
    /// </summary>
    partial class Addressee : INameOwner, IComparable
    {
        #region IComparable Members

        /// <summary>
        /// Compares the current instance with another object of the same type.
        /// </summary>
        /// <param name="obj">An object to compare with this instance. </param>
        /// <returns>A 32-bit signed integer that indicates the relative order of the objects being compared.</returns>
        public int CompareTo(object obj)
        {
            return NameOwnerTools.CompareNameOwnerWithObject(this, obj);
        }

        #endregion

        /// <summary>
        /// Returns addressee name.
        /// </summary>
        /// <returns>Addressee name</returns>
        public override string ToString()
        {
            return Name;
        }
    }

    /// <summary>
    /// Tool entity.
    /// </summary>
    partial class Tool : INameOwner, IComparable
    {
        #region IComparable Members

        /// <summary>
        /// Compares the current instance with another object of the same type.
        /// </summary>
        /// <param name="obj">An object to compare with this instance. </param>
        /// <returns>A 32-bit signed integer that indicates the relative order of the objects being compared.</returns>
        public int CompareTo(object obj)
        {
            return NameOwnerTools.CompareNameOwnerWithObject(this, obj);
        }

        #endregion

        /// <summary>
        /// Returns tool name.
        /// </summary>
        /// <returns>Tool name</returns>
        public override string ToString()
        {
            return Name;
        }
    }

    /// <summary>
    /// Tool category entity.
    /// </summary>
    partial class ToolCategory : INameOwner, IComparable
    {
        #region IComparable Members

        /// <summary>
        /// Compares the current instance with another object of the same type.
        /// </summary>
        /// <param name="obj">An object to compare with this instance. </param>
        /// <returns>A 32-bit signed integer that indicates the relative order of the objects being compared.</returns>
        public int CompareTo(object obj)
        {
            return NameOwnerTools.CompareNameOwnerWithObject(this, obj);
        }

        #endregion

        /// <summary>
        /// Returns tool category name.
        /// </summary>
        /// <returns>Tool category name</returns>
        public override string ToString()
        {
            return Name;
        }
    }

    /// <summary>
    /// Tool subcategory entity.
    /// </summary>
    partial class ToolSubCategory : INameOwner, IComparable
    {
        #region IComparable Members

        public int CompareTo(object obj)
        {
            return NameOwnerTools.CompareNameOwnerWithObject(this, obj);
        }

        #endregion

        /// <summary>
        /// Returns tool subcategory name.
        /// </summary>
        /// <returns>Tool subcategory name</returns>
        public override string ToString()
        {
            return Name;
        }
    }

    /// <summary>
    /// Unit entity.
    /// </summary>
    partial class Unit : INameOwner, IComparable
    {
        #region IComparable Members

        /// <summary>
        /// Compares the current instance with another object of the same type.
        /// </summary>
        /// <param name="obj">An object to compare with this instance. </param>
        /// <returns>A 32-bit signed integer that indicates the relative order of the objects being compared.</returns>
        public int CompareTo(object obj)
        {
            return NameOwnerTools.CompareNameOwnerWithObject(this, obj);
        }

        #endregion

        /// <summary>
        /// Returns unit name.
        /// </summary>
        /// <returns>Unit name</returns>
        public override string ToString()
        {
            return Name;
        }
    }

    /// <summary>
    /// Part entity.
    /// </summary>
    partial class Part : INameOwner, IComparable
    {
        #region IComparable Members

        /// <summary>
        /// Compares the current instance with another object of the same type.
        /// </summary>
        /// <param name="obj">An object to compare with this instance. </param>
        /// <returns>A 32-bit signed integer that indicates the relative order of the objects being compared.</returns>
        public int CompareTo(object obj)
        {
            return NameOwnerTools.CompareNameOwnerWithObject(this, obj);
        }

        #endregion

        /// <summary>
        /// Returns part name.
        /// </summary>
        /// <returns>Part name</returns>
        public override string ToString()
        {
            return Name;
        }
    }

    /// <summary>
    /// Discipline entity.
    /// </summary>
    partial class Discipline : INameOwner, IComparable
    {
        /// <summary>
        /// Gets unit number.
        /// </summary>
        public int UnitNumber { get; private set; }

        /// <summary>
        /// Gets part number.
        /// </summary>
        public int PartNumber { get; private set; }

        /// <summary>
        /// Gets discipline number.
        /// </summary>
        public int DisciplineNumber { get; private set; }

        #region IComparable Members

        /// <summary>
        /// Compares the current instance with another object of the same type.
        /// </summary>
        /// <param name="obj">An object to compare with this instance. </param>
        /// <returns>A 32-bit signed integer that indicates the relative order of the objects being compared.</returns>
        public int CompareTo(object obj)
        {
            if (obj == null || !(obj is Discipline))
            {
                return 0;
            }

            var otherObject = obj as Discipline;

            if (UnitNumber != -1 && otherObject.UnitNumber != -1 && UnitNumber != otherObject.UnitNumber)
            {
                return UnitNumber.CompareTo(otherObject.UnitNumber);
            }
            if (PartNumber != -1 && otherObject.PartNumber != -1 && PartNumber != otherObject.PartNumber)
            {
                return PartNumber.CompareTo(otherObject.PartNumber);
            }
            if (DisciplineNumber != -1 && otherObject.DisciplineNumber != -1 &&
                DisciplineNumber != otherObject.DisciplineNumber)
            {
                return DisciplineNumber.CompareTo(otherObject.DisciplineNumber);
            }

            return NameOwnerTools.CompareNameOwnerWithObject(this, obj);
        }

        #endregion

        /// <summary>
        /// Handles entity loading.
        /// </summary>
        // ReSharper disable UnusedMember.Local
        partial void OnLoaded()
        // ReSharper restore UnusedMember.Local
        {
            ParseNumbers(Name);
        }

        /// <summary>
        /// Handles name changing.
        /// </summary>
        /// <param name="value">New value of name.</param>
        partial void OnNameChanging(string value)
        {
            ParseNumbers(value);
        }

        private static string RemoveStartingZeros(string text)
        {
            while (text.StartsWith("0"))
            {
                text = text.Remove(0);
            }

            return text;
        }

        /// <summary>
        /// Parses unit, part and discipline numbers from discipline name.
        /// </summary>
        /// <param name="name">Value of name.</param>
        private void ParseNumbers(string name)
        {
            UnitNumber = -1;
            PartNumber = -1;
            DisciplineNumber = -1;

            MatchCollection matches =
                new Regex(@"(?<UnitNumber>[0-9]+)\.(?<PartNumber>[0-9]+)\.(?<DisciplineNumber>[0-9]+)").Matches(name);

            if (matches.Count > 0)
            {
                int unitNumber;
                int partNumber;
                int disciplineNumber;

                if (Int32.TryParse(RemoveStartingZeros(matches[0].Result("${UnitNumber}")), out unitNumber))
                {
                    UnitNumber = unitNumber;
                }
                if (Int32.TryParse(RemoveStartingZeros(matches[0].Result("${PartNumber}")), out partNumber))
                {
                    PartNumber = partNumber;
                }
                if (!Int32.TryParse(RemoveStartingZeros(matches[0].Result("${DisciplineNumber}")), out disciplineNumber))
                {
                    DisciplineNumber = disciplineNumber;
                }
            }
        }

        /// <summary>
        /// Returns discipline name.
        /// </summary>
        /// <returns>Discipline name</returns>
        public override string ToString()
        {
            return Name;
        }
    }

    /// <summary>
    /// Person entity.
    /// </summary>
    partial class Person : INameOwner, IComparable
    {
        /// <summary>
        /// Gets last name of person.
        /// </summary>
        public string LastName
        {
            get { return NameOwnerTools.GetLastName(Name); }
        }

        /// <summary>
        /// Gets first name of person.
        /// </summary>
        public string FirstName
        {
            get { return NameOwnerTools.GetFirstName(Name); }
        }

        /// <summary>
        /// Gets middle name of person.
        /// </summary>
        public string MiddleName
        {
            get { return NameOwnerTools.GetMiddleName(Name); }
        }

        public byte[] PhotoData
        {
            get { return Photo != null ? Photo.ToArray() : null; }
            set
            {
                // Throws strange exception if value == null.
                Photo = value ?? new byte[0];
            }
        }

        public Image PhotoImage
        {
            get
            {
                byte[] data = PhotoData;

                try
                {
                    return data != null ? Image.FromStream(new MemoryStream(data)) : null;
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public bool GreetingExists
        {
            get { return Greeting.Exists(this, GetNextBirthday()); }
        }

        public string PhonesString
        {
            get { return string.Join("; ", PersonPhones.Select(item => item.PhoneType + ": " + item.Number).ToArray()); }
        }


        #region IComparable Members

        /// <summary>
        /// Compares the current instance with another object of the same type.
        /// </summary>
        /// <param name="obj">An object to compare with this instance. </param>
        /// <returns>A 32-bit signed integer that indicates the relative order of the objects being compared.</returns>
        public int CompareTo(object obj)
        {
            return NameOwnerTools.CompareNameOwnerWithObject(this, obj);
        }

        #endregion

        public DateTime GetNextBirthday()
        {
            DateTime today = DateTime.Today;

            int year = (BirthDate.Month < today.Month ||
                        BirthDate.Month == today.Month && BirthDate.Day < today.Day)
                           ? today.Year + 1
                           : today.Year;
            return SchedulerTools.CheckDateTime(year, BirthDate.Month, BirthDate.Day);
        }

        /// <summary>
        /// Returns person name.
        /// </summary>
        /// <returns>Person name</returns>
        public override string ToString()
        {
            return Name;
        }

        public static IQueryable<Person> FilterPersons(IQueryable<Person> persons, int? excludeStatusId,
                                                       IEnumerable<int> affiliationIds)
        {
            if (excludeStatusId.HasValue)
            {
                persons = persons.Where(person => person.Statuse == null || person.Statuse.StatusId != excludeStatusId);
            }
            if (affiliationIds != null)
            {
                persons =
                    persons.Where(
                        person =>
                        person.Affiliation == null || affiliationIds.Contains(person.Affiliation.AffiliationId));
            }

            return persons;
        }

        public static IQueryable<Person> SelectWithBirthdayAtDate(DateTime date, int? exclusionStatusId,
                                                                  IEnumerable<int> affiliationIds)
        {
            IQueryable<Person> birthPersons = Program.DbSingletone.Persons.Where(person =>
                                                                       ((person.BirthDate.Day == date.Day ||
                                                                         (!DateTime.IsLeapYear(date.Year) &&
                                                                          date.Month == 2 &&
                                                                          person.BirthDate.Day == 29 && date.Day == 28))
                                                                        && person.BirthDate.Month == date.Month));

            return FilterPersons(birthPersons, exclusionStatusId, affiliationIds);
        }

        /// <summary>
        /// Selects persons that are having birthdays next several days.
        /// </summary>
        /// <param name="daysBefore">Days before birthday.</param>
        /// <param name="excludeStatusId">Exclude persons with this status id.</param>
        /// <param name="affiliationIds">Select only persons with those education ids.</param>
        /// <returns>Queryable data source with persons.</returns>
        public static IQueryable<Person> SelectByComingBirthdays(int daysBefore, int? excludeStatusId,
                                                                 IEnumerable<int> affiliationIds)
        {
            DateTime today = DateTime.Today;
            DateTime finishDay = today + TimeSpan.FromDays(daysBefore);
            if (finishDay.Year > today.Year &&
                (finishDay.Month > today.Month || finishDay.Month == today.Month && finishDay.Day > today.Day))
            {
                finishDay = SchedulerTools.CheckDateTime(today.Year, finishDay.Month, finishDay.Day);
            }

            IEnumerable<Person> personList =
                FilterPersons(Program.DbSingletone.Persons, excludeStatusId, affiliationIds).AsEnumerable();

            return (from person in personList
                    let nextBirthday = person.GetNextBirthday()
                    where nextBirthday >= today && nextBirthday <= finishDay
                    select person).AsQueryable();

        }

        /// <summary>
        /// Gets if Professional probationer has correspoding Curriculum person.
        /// </summary>
        /// <param name="professionalUserUniqueId">Unique id of professional user.</param>
        /// <returns>True if exists; otherwise, false.</returns>
        public static bool Exists(Guid professionalUserUniqueId)
        {
            return Program.DbSingletone.Persons.Any(p => p.ProfessionalUserUniqueId == professionalUserUniqueId);
        }


    }

    partial class Setting
    {
        internal static Setting GetSetting(string key)
        {
            try
            {
                return Program.DbSingletone.Settings.FirstOrDefault(s => s.Key == key);
            }
            catch (DbException ex)
            {
                Program.HandleDbException(ex);
                return null;
            }
        }

        public static int[] GetBirthAffiliationIds()
        {
            Setting affiliationSetting = GetSetting(Properties.Settings.Default.BirthDayAffiliationSettingKey);
            return affiliationSetting != null && !String.IsNullOrEmpty(affiliationSetting.Value)
                       ? Array.ConvertAll(affiliationSetting.Value.Split(','), s => Convert.ToInt32(s))
                       : null;
        }

        public static int? GetExtclusionStatusId()
        {
            Setting exclusionStatus = GetSetting(Properties.Settings.Default.ExclusionStatusSettingKey);

            return exclusionStatus != null && !String.IsNullOrEmpty(exclusionStatus.Value)
                       ? (int?)Convert.ToInt32(exclusionStatus.Value)
                       : null;
        }
    }

    /// <summary>
    /// Commission member entity.
    /// </summary>
    partial class CommissionMember : INameOwner, IComparable
    {
        /// <summary>
        /// Gets last name of commission member.
        /// </summary>
        public string LastName
        {
            get { return NameOwnerTools.GetLastName(Name); }
        }

        /// <summary>
        /// Gets first name of commission member.
        /// </summary>
        public string FirstName
        {
            get { return NameOwnerTools.GetFirstName(Name); }
        }

        /// <summary>
        /// Gets middle name of commission member.
        /// </summary>
        public string MiddleName
        {
            get { return NameOwnerTools.GetMiddleName(Name); }
        }

        #region IComparable Members

        /// <summary>
        /// Compares the current instance with another object of the same type.
        /// </summary>
        /// <param name="obj">An object to compare with this instance. </param>
        /// <returns>A 32-bit signed integer that indicates the relative order of the objects being compared.</returns>
        public int CompareTo(object obj)
        {
            return NameOwnerTools.CompareNameOwnerWithObject(this, obj);
        }

        #endregion

        /// <summary>
        /// Returns commission member name.
        /// </summary>
        /// <returns>Commission member name</returns>
        public override string ToString()
        {
            return Name;
        }
    }

    /// <summary>
    /// Lesson type entity.
    /// </summary>
    partial class LessonType : INameOwner, IComparable
    {
        /// <summary>
        /// Gets or sets fill color of lesson boxes on the calendar.
        /// </summary>
        public Color FillColor
        {
            get { return Color.FromArgb(FillColorArgb); }
            set
            {
                SendPropertyChanging();
                FillColorArgb = value.ToArgb();
                SendPropertyChanged("FillColor");
            }
        }

        /// <summary>
        /// Gets or sets border color of lesson boxes on the calendar.
        /// </summary>
        public Color BorderColor
        {
            get { return Color.FromArgb(BorderColorArgb); }
            set
            {
                SendPropertyChanging();
                BorderColorArgb = value.ToArgb();
                SendPropertyChanged("BorderColor");
            }
        }

        #region IComparable Members

        /// <summary>
        /// Compares the current instance with another object of the same type.
        /// </summary>
        /// <param name="obj">An object to compare with this instance. </param>
        /// <returns>A 32-bit signed integer that indicates the relative order of the objects being compared.</returns>
        public int CompareTo(object obj)
        {
            if (obj == null || !(obj is LessonType))
            {
                return 0;
            }

            var otherLessonType = obj as LessonType;

            if (IsTest != otherLessonType.IsTest)
            {
                return IsTest ? 1 : -1;
            }

            return NameOwnerTools.CompareNameOwnerWithObject(this, obj);
        }

        #endregion

        /// <summary>
        /// Returns person name.
        /// </summary>
        /// <returns>Person name</returns>
        public override string ToString()
        {
            return Name;
        }

        /// <summary>
        /// Handles fill color ARGB changed event.
        /// </summary>
        partial void OnFillColorArgbChanged()
        {
            SendPropertyChanged("FillColor");
        }

        /// <summary>
        /// Handles border color ARGB changed event.
        /// </summary>
        partial void OnBorderColorArgbChanged()
        {
            SendPropertyChanged("BorderColor");
        }
    }

    /// <summary>
    /// User entity.
    /// </summary>
    partial class User : INameOwner, IComparable
    {
        #region IComparable Members

        /// <summary>
        /// Compares the current instance with another object of the same type.
        /// </summary>
        /// <param name="obj">An object to compare with this instance. </param>
        /// <returns>A 32-bit signed integer that indicates the relative order of the objects being compared.</returns>
        public int CompareTo(object obj)
        {
            return NameOwnerTools.CompareNameOwnerWithObject(this, obj);
        }

        #endregion
        /// <summary>
        /// Returns user name.
        /// </summary>
        /// <returns>User name</returns>
        public override string ToString()
        {
            return Name;
        }
    }

    partial class Greeting
    {
        public static bool Exists(Person person, DateTime date)
        {
            Greeting greeting =
                Program.DbSingletone.Greetings.FirstOrDefault(g => g.Person.PersonId == person.PersonId && g.Date == date.Date);

            return greeting != null;
        }

        public static void Save(Person person, DateTime date)
        {
            if (!Exists(person, date))
            {
                Program.DbSingletone.Greetings.InsertOnSubmit(new Greeting { Person = person, Date = date.Date });
                Program.DbSingletone.SubmitChanges();
            }
        }
    }
}
using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;
using System.Security;
using Eureca.Integrator.Utility;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

//[assembly: AllowPartiallyTrustedCallers]
[assembly: AssemblyTitle("Curriculum")]
[assembly:
    AssemblyDescription(
        "��� ������������ �������� ��������, ������� ���� ������ ��������� � ���������� ��������� ���������� �� �������� ��������"
        )]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("EURECA")]
[assembly: AssemblyProduct("����������-2")]
[assembly: AssemblyCopyright("Copyright � EURECA 2009")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.

[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM

[assembly: Guid("060a81ba-c826-4b67-ab71-2466c2bac96c")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]

[assembly: AssemblyVersion(AppVersion.ApplicationVersionString)]
[assembly: AssemblyFileVersion( AppVersion.ApplicationVersionString )]
[assembly: log4net.Config.XmlConfigurator( Watch = true )]
[assembly: NeutralResourcesLanguage("ru")]
﻿using System;
using System.Collections.Generic;
using System.Linq;
using DevExpress.XtraScheduler;

namespace Curriculum
{
    internal static class SchedulerTools
    {
        public const string BirthdayDateType = "Birthday";
        public const string EventDateType = "Event";
        public const string KnowndateDateType = "Knowndate";
        public const string HolidayDateType = "Holiday";
        public const string BirthdayWithGreetingDateType = "BirthdayWithGreeting";

        public static void SetTimeRange(SchedulerControl control)
        {
            TimeSpan? startTime;
            TimeSpan? finishTime;

            Schedule.GetTimeRange(out startTime, out finishTime);

            if (!startTime.HasValue) startTime = TimeSpan.Zero;
            if (!finishTime.HasValue) finishTime = TimeSpan.FromDays(1) - startTime;

            control.Views.DayView.VisibleTime.Start =
                control.Views.WorkWeekView.VisibleTime.Start =
                control.Views.TimelineView.WorkTime.Start = startTime.Value;
            control.Views.DayView.VisibleTime.Duration =
                control.Views.WorkWeekView.VisibleTime.Duration =
                control.Views.TimelineView.WorkTime.Duration = finishTime.Value - startTime.Value;
        }

        public static DateTime IncrementDate(DateTime date)
        {
            return date.Date + TimeSpan.FromDays(1);
        }

        private static void AddHoliday(SchedulerControl control, DateTime date, string name)
        {
            control.WorkDays.AddHoliday(date, name);
        }

        private static void AddHolidayAppointment(SchedulerStorage storage, DateTime start, DateTime? finish,
                                                  string subject)
        {
            var apt = new Appointment(AppointmentType.Normal) {AllDay = true, Start = start, Subject = subject};

            if (finish.HasValue)
            {
                apt.Duration = IncrementDate(finish.Value) - start;
            }

            try
            {
                AppointmentLabel label = (AppointmentLabel)storage.Appointments.Labels.Find( item => item.DisplayName == HolidayDateType );
                apt.LabelId = storage.Appointments.Labels.IndexOf(label);
            }
            catch (Exception)
            {
            }

            storage.Appointments.Add(apt);
            apt.CustomFields["Title"] = subject;
        }

        public static void FillHolidays(SchedulerControl control)
        {
            foreach (
                DevExpress.Schedule.Holiday holiday in
                    control.WorkDays.OfType<DevExpress.Schedule.Holiday>( ).ToArray( ))
            {
                control.WorkDays.Remove(holiday);
            }

            List<Holiday> holidays = Program.DbSingletone.Holidays.ToList();

            for (int year = DateTime.Today.Year - 1; year <= DateTime.Today.Year + 1; ++year)
            {
                foreach (Holiday holiday in holidays)
                {
                    holiday.MoveTo(year);

                    AddHoliday(control, holiday.Start, holiday.Name);

                    if (holiday.Finish.HasValue && holiday.Finish.Value > holiday.Start)
                    {
                        DateTime date = holiday.Start;

                        while (date < holiday.Finish.Value)
                        {
                            date = IncrementDate(date);

                            AddHoliday(control, date, holiday.Name);
                        }
                    }

                    AddHolidayAppointment(control.Storage, holiday.Start, holiday.Finish, holiday.Name);
                }

                List<ExceptionSchedule> exceptions =
                    Program.DbSingletone.ExceptionSchedules.Where(es => es.Date.Year == year && es.Schedule.IsHoliday).ToList();

                foreach (ExceptionSchedule exception in exceptions)
                {
                    AddHoliday(control, exception.Date, exception.Schedule.Name);
                    AddHolidayAppointment(control.Storage, exception.Date, null, exception.Schedule.Name);
                }
            }
        }

        public static DateTime CheckDateTime(int year, int month, int day)
        {
            return new DateTime(year, month, (DateTime.IsLeapYear(year) || month != 2 || day != 29) ? day : 28);
        }
    }
}
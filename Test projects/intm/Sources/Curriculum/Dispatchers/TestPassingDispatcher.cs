﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Curriculum;
using Eureca.Utility.Timers;

namespace Eureca.Utility
{
    public class NewTestPassingEventArgs : EventArgs
    {
        public List<int> TestPassingsIds { get; private set; }

        public NewTestPassingEventArgs(List<int> testPassingsIds)
        {
            TestPassingsIds = testPassingsIds;  
        }
    }

    public class TestPassingDispatcher: PeriodicInvoker
    {
        private readonly ProfessionalDataContext _dataContext;
        
        private int _lastTestPassingsCount = 0;

        public event EventHandler<NewTestPassingEventArgs> OnNewTestPassings; 
        
        protected override void PeriodicActionMethod( )
        {
            var newTestPassings = _dataContext.vStatistics.Where( item => !item.Verified ).Select( item => item.TestPassingId ).ToList( );
            if (_lastTestPassingsCount < newTestPassings.Count)
                if (OnNewTestPassings != null)
                {
                    Task.Factory.StartNew( ( ) => OnNewTestPassings( this, new NewTestPassingEventArgs( newTestPassings ) ) );
                    _lastTestPassingsCount = newTestPassings.Count;

                }
        }

        public TestPassingDispatcher(ProfessionalDataContext dataContext)
        {
            _dataContext = dataContext;
        }

    }
}

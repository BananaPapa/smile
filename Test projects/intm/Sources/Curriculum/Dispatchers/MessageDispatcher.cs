﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DevExpress.XtraRichEdit.Commands.Internal;
using DevExpress.XtraScheduler.Drawing;
using Eureca.Utility;
using Eureca.Utility.Extensions;
using Eureca.Utility.Helper;
using Eureca.Utility.Timers;
using NHibernate.Criterion;

namespace Curriculum
{
    public interface IMessageListener
    {
        void OnNewMessage(IEnumerable<ReceiveMessageArgs> args);
        void OnMessageHasBeenHandled(ReceiveMessageArgs args);
    }


    public class MessageDispatcher:PeriodicInvoker
    {
        private readonly ReaderWriterLock _readerWriterLock = new ReaderWriterLock( );
        
        private List<ReceiveMessageArgs> _unHandledMessages = new List<ReceiveMessageArgs>();
        private PriorityInvoker<IMessageListener> _priorityInvoker = new PriorityInvoker<IMessageListener>();
        private DateTime _lastCreationDate = DateTime.MinValue;
        private User _currentUser;
       // private CurriculumDataContext _dataContext;

        public EventHandler<ReceiveMessageArgs> NewMessage;

        public MessageDispatcher( )
        {
            //_dataContext = dataContext;
             this.Timeout = TimeSpan.FromSeconds(5);
        }

        public IEnumerable<Message> UnHandledMessages
        {
            get { return _unHandledMessages.Select(args => args.Message); }
        }

        public User CurrentUser
        {
            get { return _currentUser; }
            set
            {
                if(_currentUser == value)
                    return;
                Reset();
                _currentUser = value;
            }
        }

        private void Reset( )
        {
            _unHandledMessages.Clear();
            _priorityInvoker.ClearListeners();
            _lastCreationDate = SqlDateTime.MinValue.Value;
        }

        public void Subscribe(IMessageListener listener, int order = 0)
        {
           _priorityInvoker.Addlistener(order,listener);
           if (_unHandledMessages.Count>0)
            listener.OnNewMessage( _unHandledMessages );
        }

        protected override void PeriodicActionMethod( )
        {
            CurriculumDataContext curriculumDataContext = Program.Db;
            var Messages = curriculumDataContext.Messages.Where( message => !message.Delivered && message.ReceiverUserId == _currentUser.UserId && message.CreationDate > _lastCreationDate ).ToList();
            var messagesArgs = new List<ReceiveMessageArgs>();
            foreach (var newMessage in Messages)
            {
                if (newMessage.CreationDate > _lastCreationDate)
                    _lastCreationDate = newMessage.CreationDate;
                if(_unHandledMessages.Any(args => args.Message == newMessage))
                    continue;
                var newMessageArg = new ReceiveMessageArgs(newMessage);
                _readerWriterLock.CallInWriterLock( ( ) => _unHandledMessages.Add( newMessageArg ));
                newMessageArg.MessageHandled += newMessageArgs_MessageHandled;
                messagesArgs.Add(newMessageArg);
            }
            if (messagesArgs.Count > 0)
                Task.Factory.StartNew( ( ) => _priorityInvoker.Invoke( listener => listener.OnNewMessage( messagesArgs ) ) );
        }

        void newMessageArgs_MessageHandled( object sender, ItemEventArgs<Message> e )
        {
            var receiverMessageArgs = sender as ReceiveMessageArgs;
            if (!e.Item.Delivered)
            {
                var curriculumDataContext = Program.Db;
                var syncMessage =
                    curriculumDataContext.Messages.SingleOrDefault(message => message.MessageId == e.Item.MessageId);
                syncMessage.Delivered = true;
                curriculumDataContext.SubmitChanges();
            }
            _readerWriterLock.CallInWriterLock(() => _unHandledMessages.Remove(receiverMessageArgs));
            Task.Factory.StartNew( ( ) => _priorityInvoker.Invoke( listener => listener.OnMessageHasBeenHandled( receiverMessageArgs ) ) );
        }

       
    }

    public class ReceiveMessageArgs : EventArgs
    {
        private bool _handled = false;


        public Message Message { get; private set; }

        public bool Handled
        {
            get { return _handled; }
            set
            {
                if(_handled)
                    return;
                _handled = value;
                if (_handled == true && MessageHandled != null)
                {
                    MessageHandled(this, new ItemEventArgs<Message>(Message));
                }
            }
        }

        //public CurriculumDataContext DataContext { get; private set; }

        public ReceiveMessageArgs( Message message  )
        {
            Message = message;
            //DataContext = context;
        }

        public event EventHandler<ItemEventArgs<Message>> MessageHandled;
    }

}

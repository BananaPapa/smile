﻿namespace Curriculum.Forms
{
    partial class ChatForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if (disposing && ( components != null ))
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChatForm));
            this._usersListGrid = new DevExpress.XtraGrid.GridControl();
            this._imageList = new System.Windows.Forms.ImageList(this.components);
            this._usersView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._usersNameColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._dialogsTabControl = new DevExpress.XtraTab.XtraTabControl();
            ((System.ComponentModel.ISupportInitialize)(this._usersListGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._usersView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._dialogsTabControl)).BeginInit();
            this.SuspendLayout();
            // 
            // _usersListGrid
            // 
            resources.ApplyResources(this._usersListGrid, "_usersListGrid");
            this._usersListGrid.EmbeddedNavigator.Buttons.Append.Visible = false;
            this._usersListGrid.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this._usersListGrid.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this._usersListGrid.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this._usersListGrid.EmbeddedNavigator.Buttons.ImageList = this._imageList;
            this._usersListGrid.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this._usersListGrid.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(((int)(resources.GetObject("_usersListGrid.EmbeddedNavigator.CustomButtons"))), ((int)(resources.GetObject("_usersListGrid.EmbeddedNavigator.CustomButtons1"))), ((bool)(resources.GetObject("_usersListGrid.EmbeddedNavigator.CustomButtons2"))), ((bool)(resources.GetObject("_usersListGrid.EmbeddedNavigator.CustomButtons3"))), resources.GetString("_usersListGrid.EmbeddedNavigator.CustomButtons4"), resources.GetString("_usersListGrid.EmbeddedNavigator.CustomButtons5"))});
            this._usersListGrid.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this._usersListGrid_EmbeddedNavigator_ButtonClick);
            this._usersListGrid.MainView = this._usersView;
            this._usersListGrid.Name = "_usersListGrid";
            this._usersListGrid.Tag = "";
            this._usersListGrid.UseEmbeddedNavigator = true;
            this._usersListGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this._usersView});
            // 
            // _imageList
            // 
            this._imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("_imageList.ImageStream")));
            this._imageList.TransparentColor = System.Drawing.Color.Transparent;
            this._imageList.Images.SetKeyName(0, "mail_send16.png");
            // 
            // _usersView
            // 
            this._usersView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._usersNameColumn});
            this._usersView.GridControl = this._usersListGrid;
            this._usersView.Name = "_usersView";
            this._usersView.OptionsBehavior.Editable = false;
            this._usersView.OptionsSelection.MultiSelect = true;
            this._usersView.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this._usersView.OptionsView.ShowDetailButtons = false;
            this._usersView.OptionsView.ShowGroupPanel = false;
            this._usersView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this._usersView_SelectionChanged);
            // 
            // _usersNameColumn
            // 
            resources.ApplyResources(this._usersNameColumn, "_usersNameColumn");
            this._usersNameColumn.FieldName = "Name";
            this._usersNameColumn.Name = "_usersNameColumn";
            // 
            // _dialogsTabControl
            // 
            resources.ApplyResources(this._dialogsTabControl, "_dialogsTabControl");
            this._dialogsTabControl.Name = "_dialogsTabControl";
            this._dialogsTabControl.Tag = "";
            // 
            // ChatForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._dialogsTabControl);
            this.Controls.Add(this._usersListGrid);
            this.Icon = Properties.Resources.Messages_ico;
            this.Name = "ChatForm";
            this.Tag = "Панель управления\\Личный кабинет\\Сообщения#r";
            ((System.ComponentModel.ISupportInitialize)(this._usersListGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._usersView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._dialogsTabControl)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl _usersListGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView _usersView;
        private DevExpress.XtraTab.XtraTabControl _dialogsTabControl;
        private DevExpress.XtraGrid.Columns.GridColumn _usersNameColumn;
        private System.Windows.Forms.ImageList _imageList;
    }
}
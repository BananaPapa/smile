﻿using Curriculum.Properties;
using Eureca.Integrator.Common;

namespace Curriculum.Forms
{
    partial class FilesForm : FormWithAccessAreas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if (disposing && ( components != null ))
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FilesForm));
            this._tabControl = new System.Windows.Forms.TabControl();
            this._ownFilesTab = new System.Windows.Forms.TabPage();
            this._popupGridContainerControl = new DevExpress.XtraEditors.PopupContainerControl();
            this._usersGroupGrid = new DevExpress.XtraGrid.GridControl();
            this._groupsView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._groupName = new DevExpress.XtraGrid.Columns.GridColumn();
            this._ownFilesGrid = new DevExpress.XtraGrid.GridControl();
            this._ownFilesGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._documentNameColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._uploadDateColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._fileNameColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._fileVisionColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemPopupContainerEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this._obtainedFilesTab = new System.Windows.Forms.TabPage();
            this._obtainedFilesGrid = new DevExpress.XtraGrid.GridControl();
            this._imageList = new System.Windows.Forms.ImageList(this.components);
            this._obtainedFilesView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._obtainedDocumentNameColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._obtainedfileNameColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._senderColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._tabControl.SuspendLayout();
            this._ownFilesTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._popupGridContainerControl)).BeginInit();
            this._popupGridContainerControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._usersGroupGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._groupsView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._ownFilesGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._ownFilesGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).BeginInit();
            this._obtainedFilesTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._obtainedFilesGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._obtainedFilesView)).BeginInit();
            this.SuspendLayout();
            // 
            // _tabControl
            // 
            this._tabControl.Controls.Add(this._ownFilesTab);
            this._tabControl.Controls.Add(this._obtainedFilesTab);
            this._tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tabControl.Location = new System.Drawing.Point(0, 0);
            this._tabControl.Name = "_tabControl";
            this._tabControl.SelectedIndex = 0;
            this._tabControl.Size = new System.Drawing.Size(959, 644);
            this._tabControl.TabIndex = 0;
            // 
            // _ownFilesTab
            // 
            this._ownFilesTab.Controls.Add(this._popupGridContainerControl);
            this._ownFilesTab.Controls.Add(this._ownFilesGrid);
            this._ownFilesTab.Location = new System.Drawing.Point(4, 22);
            this._ownFilesTab.Name = "_ownFilesTab";
            this._ownFilesTab.Padding = new System.Windows.Forms.Padding(3);
            this._ownFilesTab.Size = new System.Drawing.Size(951, 618);
            this._ownFilesTab.TabIndex = 0;
            this._ownFilesTab.Tag = "Свои файлы#r";
            this._ownFilesTab.Text = "Свои файлы";
            this._ownFilesTab.UseVisualStyleBackColor = true;
            // 
            // _popupGridContainerControl
            // 
            this._popupGridContainerControl.Controls.Add(this._usersGroupGrid);
            this._popupGridContainerControl.Location = new System.Drawing.Point(133, 160);
            this._popupGridContainerControl.Name = "_popupGridContainerControl";
            this._popupGridContainerControl.Size = new System.Drawing.Size(402, 272);
            this._popupGridContainerControl.TabIndex = 1;
            // 
            // _usersGroupGrid
            // 
            this._usersGroupGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._usersGroupGrid.Location = new System.Drawing.Point(0, 0);
            this._usersGroupGrid.MainView = this._groupsView;
            this._usersGroupGrid.Name = "_usersGroupGrid";
            this._usersGroupGrid.Size = new System.Drawing.Size(402, 272);
            this._usersGroupGrid.TabIndex = 0;
            this._usersGroupGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this._groupsView});
            // 
            // _groupsView
            // 
            this._groupsView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._groupName});
            this._groupsView.GridControl = this._usersGroupGrid;
            this._groupsView.Name = "_groupsView";
            this._groupsView.OptionsSelection.MultiSelect = true;
            this._groupsView.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this._groupsView.OptionsView.ShowDetailButtons = false;
            this._groupsView.OptionsView.ShowGroupPanel = false;
            this._groupsView.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this._groupName, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // _groupName
            // 
            this._groupName.Caption = "Группы";
            this._groupName.FieldName = "UserGroupName";
            this._groupName.Name = "_groupName";
            this._groupName.Visible = true;
            this._groupName.VisibleIndex = 1;
            // 
            // _ownFilesGrid
            // 
            this._ownFilesGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._ownFilesGrid.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.ownFilesGrid_EmbeddedNavigator_ButtonClick);
            this._ownFilesGrid.Location = new System.Drawing.Point(3, 3);
            this._ownFilesGrid.MainView = this._ownFilesGridView;
            this._ownFilesGrid.Name = "_ownFilesGrid";
            this._ownFilesGrid.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPopupContainerEdit1});
            this._ownFilesGrid.Size = new System.Drawing.Size(945, 612);
            this._ownFilesGrid.TabIndex = 0;
            this._ownFilesGrid.UseEmbeddedNavigator = true;
            this._ownFilesGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this._ownFilesGridView});
            // 
            // _ownFilesGridView
            // 
            this._ownFilesGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._documentNameColumn,
            this._uploadDateColumn,
            this._fileNameColumn,
            this._fileVisionColumn});
            this._ownFilesGridView.GridControl = this._ownFilesGrid;
            this._ownFilesGridView.Name = "_ownFilesGridView";
            this._ownFilesGridView.OptionsView.ShowDetailButtons = false;
            this._ownFilesGridView.OptionsView.ShowGroupPanel = false;
            this._ownFilesGridView.CustomColumnDisplayText += new DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventHandler(this._ownFilesGridView_CustomColumnDisplayText);
            // 
            // _documentNameColumn
            // 
            this._documentNameColumn.Caption = "Название";
            this._documentNameColumn.FieldName = "Name";
            this._documentNameColumn.Name = "_documentNameColumn";
            this._documentNameColumn.Tag = "Название#r";
            this._documentNameColumn.Visible = true;
            this._documentNameColumn.VisibleIndex = 0;
            // 
            // _uploadDateColumn
            // 
            this._uploadDateColumn.Caption = "Дата создание";
            this._uploadDateColumn.FieldName = "_uploadDateColumn";
            this._uploadDateColumn.Name = "_uploadDateColumn";
            this._uploadDateColumn.Tag = "Дата создание#r";
            // 
            // _fileNameColumn
            // 
            this._fileNameColumn.Caption = "Имя файла";
            this._fileNameColumn.FieldName = "FileName";
            this._fileNameColumn.Name = "_fileNameColumn";
            this._fileNameColumn.OptionsColumn.AllowEdit = false;
            this._fileNameColumn.OptionsColumn.ReadOnly = true;
            this._fileNameColumn.Tag = "Имя файла#r";
            this._fileNameColumn.Visible = true;
            this._fileNameColumn.VisibleIndex = 1;
            // 
            // _fileVisionColumn
            // 
            this._fileVisionColumn.Caption = "Область видимости";
            this._fileVisionColumn.ColumnEdit = this.repositoryItemPopupContainerEdit1;
            this._fileVisionColumn.FieldName = "FilesUsersGroups";
            this._fileVisionColumn.Name = "_fileVisionColumn";
            this._fileVisionColumn.Visible = true;
            this._fileVisionColumn.VisibleIndex = 2;
            // 
            // repositoryItemPopupContainerEdit1
            // 
            this.repositoryItemPopupContainerEdit1.AutoHeight = false;
            this.repositoryItemPopupContainerEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit1.Name = "repositoryItemPopupContainerEdit1";
            this.repositoryItemPopupContainerEdit1.PopupControl = this._popupGridContainerControl;
            this.repositoryItemPopupContainerEdit1.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEdit1_QueryResultValue);
            this.repositoryItemPopupContainerEdit1.QueryDisplayText += new DevExpress.XtraEditors.Controls.QueryDisplayTextEventHandler(this.repositoryItemPopupContainerEdit1_QueryDisplayText);
            this.repositoryItemPopupContainerEdit1.QueryPopUp += new System.ComponentModel.CancelEventHandler(this.repositoryItemPopupContainerEdit1_QueryPopUp);
            // 
            // _obtainedFilesTab
            // 
            this._obtainedFilesTab.Controls.Add(this._obtainedFilesGrid);
            this._obtainedFilesTab.Location = new System.Drawing.Point(4, 22);
            this._obtainedFilesTab.Name = "_obtainedFilesTab";
            this._obtainedFilesTab.Padding = new System.Windows.Forms.Padding(3);
            this._obtainedFilesTab.Size = new System.Drawing.Size(951, 618);
            this._obtainedFilesTab.TabIndex = 1;
            this._obtainedFilesTab.Tag = "Полученные файлы#r";
            this._obtainedFilesTab.Text = "Полученные файлы";
            this._obtainedFilesTab.UseVisualStyleBackColor = true;
            // 
            // _obtainedFilesGrid
            // 
            this._obtainedFilesGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._obtainedFilesGrid.EmbeddedNavigator.Buttons.Append.Visible = false;
            this._obtainedFilesGrid.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this._obtainedFilesGrid.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this._obtainedFilesGrid.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this._obtainedFilesGrid.EmbeddedNavigator.Buttons.ImageList = this._imageList;
            this._obtainedFilesGrid.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this._obtainedFilesGrid.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Скачать файл", "Save#r"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Обновить", "Refresh#r")});
            this._obtainedFilesGrid.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this._obtainedFilesGrid_EmbeddedNavigator_ButtonClick);
            this._obtainedFilesGrid.Location = new System.Drawing.Point(3, 3);
            this._obtainedFilesGrid.MainView = this._obtainedFilesView;
            this._obtainedFilesGrid.Name = "_obtainedFilesGrid";
            this._obtainedFilesGrid.Size = new System.Drawing.Size(945, 612);
            this._obtainedFilesGrid.TabIndex = 1;
            this._obtainedFilesGrid.UseEmbeddedNavigator = true;
            this._obtainedFilesGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this._obtainedFilesView});
            // 
            // _imageList
            // 
            this._imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("_imageList.ImageStream")));
            this._imageList.TransparentColor = System.Drawing.Color.Transparent;
            this._imageList.Images.SetKeyName(0, "download16.png");
            this._imageList.Images.SetKeyName(1, "Refresh16.png");
            // 
            // _obtainedFilesView
            // 
            this._obtainedFilesView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._obtainedDocumentNameColumn,
            this._obtainedfileNameColumn,
            this._senderColumn});
            this._obtainedFilesView.GridControl = this._obtainedFilesGrid;
            this._obtainedFilesView.Name = "_obtainedFilesView";
            this._obtainedFilesView.OptionsView.ShowDetailButtons = false;
            this._obtainedFilesView.OptionsView.ShowGroupPanel = false;
            // 
            // _obtainedDocumentNameColumn
            // 
            this._obtainedDocumentNameColumn.Caption = "Название";
            this._obtainedDocumentNameColumn.FieldName = "Name";
            this._obtainedDocumentNameColumn.Name = "_obtainedDocumentNameColumn";
            this._obtainedDocumentNameColumn.OptionsColumn.AllowEdit = false;
            this._obtainedDocumentNameColumn.OptionsColumn.ReadOnly = true;
            this._obtainedDocumentNameColumn.Tag = "Название#r";
            this._obtainedDocumentNameColumn.Visible = true;
            this._obtainedDocumentNameColumn.VisibleIndex = 0;
            // 
            // _obtainedfileNameColumn
            // 
            this._obtainedfileNameColumn.Caption = "Имя файла";
            this._obtainedfileNameColumn.FieldName = "FileName";
            this._obtainedfileNameColumn.Name = "_obtainedfileNameColumn";
            this._obtainedfileNameColumn.OptionsColumn.AllowEdit = false;
            this._obtainedfileNameColumn.OptionsColumn.ReadOnly = true;
            this._obtainedfileNameColumn.Visible = true;
            this._obtainedfileNameColumn.VisibleIndex = 2;
            // 
            // _senderColumn
            // 
            this._senderColumn.Caption = "Отправитель";
            this._senderColumn.FieldName = "OwnerUser.Name";
            this._senderColumn.Name = "_senderColumn";
            this._senderColumn.OptionsColumn.AllowEdit = false;
            this._senderColumn.OptionsColumn.ReadOnly = true;
            this._senderColumn.Tag = "Отправитель#r";
            this._senderColumn.Visible = true;
            this._senderColumn.VisibleIndex = 1;
            // 
            // FilesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(959, 644);
            this.Controls.Add(this._tabControl);
            this.Name = "FilesForm";
            this.Tag = "Панель управления\\Личный кабинет\\Файлы#r";
            this.Text = "Файлы";
            this._tabControl.ResumeLayout(false);
            this._ownFilesTab.ResumeLayout(false);
            this.Icon = Properties.Resources.Files_ico;
            ((System.ComponentModel.ISupportInitialize)(this._popupGridContainerControl)).EndInit();
            this._popupGridContainerControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._usersGroupGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._groupsView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._ownFilesGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._ownFilesGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).EndInit();
            this._obtainedFilesTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._obtainedFilesGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._obtainedFilesView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl _tabControl;
        private System.Windows.Forms.TabPage _ownFilesTab;
        private System.Windows.Forms.TabPage _obtainedFilesTab;
        private DevExpress.XtraGrid.GridControl _ownFilesGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView _ownFilesGridView;
        private DevExpress.XtraGrid.GridControl _obtainedFilesGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView _obtainedFilesView;
        private DevExpress.XtraGrid.Columns.GridColumn _documentNameColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _uploadDateColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _fileNameColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _obtainedDocumentNameColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _senderColumn;
        private DevExpress.XtraEditors.PopupContainerControl _popupGridContainerControl;
        private DevExpress.XtraGrid.GridControl _usersGroupGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView _groupsView;
        private DevExpress.XtraGrid.Columns.GridColumn _groupName;
        private DevExpress.XtraGrid.Columns.GridColumn _fileVisionColumn;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit1;
        private System.Windows.Forms.ImageList _imageList;
        private DevExpress.XtraGrid.Columns.GridColumn _obtainedfileNameColumn;
    }
}
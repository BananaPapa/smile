﻿namespace Curriculum.PersonPage
{
    partial class RouteEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if (disposing && ( components != null ))
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RouteEditForm));
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this._routeNameTextBox = new System.Windows.Forms.TextBox();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this._routeTargetsGrid = new DevExpress.XtraGrid.GridControl();
            this._imageList = new System.Windows.Forms.ImageList(this.components);
            this._routeTargetsGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._receiverName = new DevExpress.XtraGrid.Columns.GridColumn();
            this._receiversComboEdit = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this._allowedOperationsColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._allowedOperationsPopupContainerEdit = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this._operationsContainerControl = new DevExpress.XtraEditors.PopupContainerControl();
            this._operationsGrid = new DevExpress.XtraGrid.GridControl();
            this._operationGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._operationNameColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._orderColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this._routeTargetsGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._routeTargetsGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._receiversComboEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._allowedOperationsPopupContainerEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._operationsContainerControl)).BeginInit();
            this._operationsContainerControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._operationsGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._operationGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(33, 28);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(52, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Название:";
            // 
            // _routeNameTextBox
            // 
            this._routeNameTextBox.Location = new System.Drawing.Point(106, 25);
            this._routeNameTextBox.Name = "_routeNameTextBox";
            this._routeNameTextBox.Size = new System.Drawing.Size(756, 20);
            this._routeNameTextBox.TabIndex = 1;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButton1.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.simpleButton1.Location = new System.Drawing.Point(743, 482);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(119, 23);
            this.simpleButton1.TabIndex = 0;
            this.simpleButton1.Text = "Ок";
            // 
            // simpleButton2
            // 
            this.simpleButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButton2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButton2.Location = new System.Drawing.Point(595, 482);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(119, 23);
            this.simpleButton2.TabIndex = 3;
            this.simpleButton2.Text = "Отмена";
            // 
            // _routeTargetsGrid
            // 
            this._routeTargetsGrid.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this._routeTargetsGrid.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this._routeTargetsGrid.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this._routeTargetsGrid.EmbeddedNavigator.Buttons.ImageList = this._imageList;
            this._routeTargetsGrid.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "", "RaiseBtn"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "", "LowerBtn")});
            this._routeTargetsGrid.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this._routeTargetsGrid_EmbeddedNavigator_ButtonClick);
            this._routeTargetsGrid.Location = new System.Drawing.Point(33, 69);
            this._routeTargetsGrid.MainView = this._routeTargetsGridView;
            this._routeTargetsGrid.Name = "_routeTargetsGrid";
            this._routeTargetsGrid.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this._allowedOperationsPopupContainerEdit,
            this._receiversComboEdit});
            this._routeTargetsGrid.Size = new System.Drawing.Size(829, 395);
            this._routeTargetsGrid.TabIndex = 4;
            this._routeTargetsGrid.UseEmbeddedNavigator = true;
            this._routeTargetsGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this._routeTargetsGridView});
            // 
            // _imageList
            // 
            this._imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("_imageList.ImageStream")));
            this._imageList.TransparentColor = System.Drawing.Color.Transparent;
            this._imageList.Images.SetKeyName(0, "arrow_down.png");
            this._imageList.Images.SetKeyName(1, "arrow_up.png");
            // 
            // _routeTargetsGridView
            // 
            this._routeTargetsGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._receiverName,
            this._allowedOperationsColumn,
            this._orderColumn});
            this._routeTargetsGridView.GridControl = this._routeTargetsGrid;
            this._routeTargetsGridView.Name = "_routeTargetsGridView";
            this._routeTargetsGridView.OptionsView.ShowGroupPanel = false;
            // 
            // _receiverName
            // 
            this._receiverName.Caption = "Получатель";
            this._receiverName.ColumnEdit = this._receiversComboEdit;
            this._receiverName.FieldName = "Receiver.Name";
            this._receiverName.Name = "_receiverName";
            this._receiverName.Visible = true;
            this._receiverName.VisibleIndex = 0;
            // 
            // _receiversComboEdit
            // 
            this._receiversComboEdit.AutoHeight = false;
            this._receiversComboEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._receiversComboEdit.Name = "_receiversComboEdit";
            // 
            // _allowedOperationsColumn
            // 
            this._allowedOperationsColumn.Caption = "Доступные операции";
            this._allowedOperationsColumn.ColumnEdit = this._allowedOperationsPopupContainerEdit;
            this._allowedOperationsColumn.FieldName = "Operations";
            this._allowedOperationsColumn.Name = "_allowedOperationsColumn";
            this._allowedOperationsColumn.Visible = true;
            this._allowedOperationsColumn.VisibleIndex = 1;
            // 
            // _allowedOperationsPopupContainerEdit
            // 
            this._allowedOperationsPopupContainerEdit.AutoHeight = false;
            this._allowedOperationsPopupContainerEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._allowedOperationsPopupContainerEdit.Name = "_allowedOperationsPopupContainerEdit";
            this._allowedOperationsPopupContainerEdit.PopupControl = this._operationsContainerControl;
            this._allowedOperationsPopupContainerEdit.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this._allowedOperationsPopupContainerEdit_QueryResultValue);
            this._allowedOperationsPopupContainerEdit.QueryDisplayText += new DevExpress.XtraEditors.Controls.QueryDisplayTextEventHandler(this._allowedOperationsPopupContainerEdit_QueryDisplayText);
            this._allowedOperationsPopupContainerEdit.QueryPopUp += new System.ComponentModel.CancelEventHandler(this._allowedOperationsPopupContainerEdit_QueryPopUp);
            // 
            // _operationsContainerControl
            // 
            this._operationsContainerControl.Controls.Add(this._operationsGrid);
            this._operationsContainerControl.Location = new System.Drawing.Point(677, 51);
            this._operationsContainerControl.Name = "_operationsContainerControl";
            this._operationsContainerControl.Size = new System.Drawing.Size(200, 176);
            this._operationsContainerControl.TabIndex = 5;
            // 
            // _operationsGrid
            // 
            this._operationsGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._operationsGrid.Location = new System.Drawing.Point(0, 0);
            this._operationsGrid.MainView = this._operationGridView;
            this._operationsGrid.Name = "_operationsGrid";
            this._operationsGrid.Size = new System.Drawing.Size(200, 176);
            this._operationsGrid.TabIndex = 0;
            this._operationsGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this._operationGridView});
            // 
            // _operationGridView
            // 
            this._operationGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._operationNameColumn});
            this._operationGridView.GridControl = this._operationsGrid;
            this._operationGridView.Name = "_operationGridView";
            this._operationGridView.OptionsSelection.MultiSelect = true;
            this._operationGridView.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this._operationGridView.OptionsView.ShowGroupPanel = false;
            // 
            // _operationNameColumn
            // 
            this._operationNameColumn.Caption = "Операция";
            this._operationNameColumn.FieldName = "Name";
            this._operationNameColumn.Name = "_operationNameColumn";
            this._operationNameColumn.Visible = true;
            this._operationNameColumn.VisibleIndex = 1;
            // 
            // _orderColumn
            // 
            this._orderColumn.FieldName = "Order";
            this._orderColumn.Name = "_orderColumn";
            // 
            // RouteEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(889, 517);
            this.Controls.Add(this._operationsContainerControl);
            this.Controls.Add(this._routeTargetsGrid);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this._routeNameTextBox);
            this.Controls.Add(this.labelControl1);
            this.Name = "RouteEditForm";
            this.Text = "RouteEditForm";
            ((System.ComponentModel.ISupportInitialize)(this._routeTargetsGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._routeTargetsGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._receiversComboEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._allowedOperationsPopupContainerEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._operationsContainerControl)).EndInit();
            this._operationsContainerControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._operationsGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._operationGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private System.Windows.Forms.TextBox _routeNameTextBox;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraGrid.GridControl _routeTargetsGrid;
        private System.Windows.Forms.ImageList _imageList;
        private DevExpress.XtraGrid.Views.Grid.GridView _routeTargetsGridView;
        private DevExpress.XtraGrid.Columns.GridColumn _receiverName;
        private DevExpress.XtraGrid.Columns.GridColumn _allowedOperationsColumn;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox _receiversComboEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit _allowedOperationsPopupContainerEdit;
        private DevExpress.XtraEditors.PopupContainerControl _operationsContainerControl;
        private DevExpress.XtraGrid.GridControl _operationsGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView _operationGridView;
        private DevExpress.XtraGrid.Columns.GridColumn _operationNameColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _orderColumn;

    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Linq;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraRichEdit;
using DevExpress.XtraRichEdit.Services.Implementation;
using Eureca.Integrator.Common;
using Eureca.Integrator.Extensions;
using log4net;
using NHibernate.Hql.Ast.ANTLR;
using MessageBox = Eureca.Integrator.Utility.Forms.MessageBox;

namespace Curriculum.Forms
{
    public partial class FilesForm : FormWithAccessAreas
    {
        private ILog Log = LogManager.GetLogger(typeof (FilesForm));

        readonly BindingList<File> _obtainFiles = new BindingList<File>( );
        readonly BindingList<File> _ownFiles = new BindingList<File>( );
        readonly RichEditControl _giantParserWithBlackJackAndHookers = new RichEditControl( );

        public FilesForm( )
        {
            InitializeComponent( );
            Load += FilesForm_Load;
        }

        void FilesForm_Load( object sender, EventArgs e )
        {
            FillObtainedFiles();
            FillOwnedFiles();
            _obtainedFilesGrid.DataSource = _obtainFiles;
            _ownFilesGrid.DataSource = _ownFiles;
        }

        private void ownFilesGrid_EmbeddedNavigator_ButtonClick( object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e )
        {
            switch (e.Button.ButtonType)
            {
                    case NavigatorButtonType.Append:
                    e.Handled = true;
                        var newDocument = LoadNewFile();
                        if(newDocument==null)
                            return;
                        _ownFiles.Add( newDocument );
                        _ownFilesGridView.SelectRow(_ownFilesGridView.GetRowHandle(_ownFiles.IndexOf(newDocument)));
                    break;


                    case NavigatorButtonType.Remove:
                    e.Handled = true;
                        var selectedItem =_ownFilesGridView.GetSelectedItem() as File;
                        if(selectedItem ==null)
                            return;
                        Program.DbSingletone.Files.DeleteOnSubmit(selectedItem);
                        Program.DbSingletone.SubmitChanges();
                        //if(CheckRefs(selectedItem))
                        //{
                        //    Program.Db.Documents.DeleteOnSubmit(selectedItem);
                        //    Program.Db.SubmitChanges();
                        //}
                        FillOwnedFiles( );

                    break;
            }

        }

        //private bool CheckRefs( Document selectedItem )
        //{
        //    var topicalMessages =
        //        Program.Db.Attachments.Where( attachment => attachment.Document == selectedItem )
        //            .Select( attachment => attachment.Document );
        //    if (topicalMessages.Any( message => message.State != completedState ))
        //        return false;
        //    return true;
        //}

        public File LoadNewFile()
        {
            File newDocument = null;
            var openFileDialog = new OpenFileDialog();
            var res = openFileDialog.ShowDialog(this);
            if (res == DialogResult.OK)
                newDocument = UploadTextToDataBase(openFileDialog.FileName);
            return newDocument;
        }

        private File UploadTextToDataBase(string filePath )
        {
            File newDocument = null;
            string fileExtension = Path.GetExtension( filePath );
            byte[] buffer = null;
            if((new []{"doc","rtf","docx"}).Contains(fileExtension))
            {
                try
                {
                
                    DocumentFormat docFormat = DocumentFormat.Undefined;
                    if(fileExtension=="doc" || fileExtension=="docx")
                        docFormat = DocumentFormat.Doc;
                    else if(fileExtension=="rtf")
                        docFormat = DocumentFormat.Rtf;
                    _giantParserWithBlackJackAndHookers.Document.LoadDocument(filePath,docFormat);
                    var rtfStream = _giantParserWithBlackJackAndHookers.SaveRtf( );
                    buffer = rtfStream.GetBuffer();
                
                }
                catch (Exception e)
                {
                    Log.Error(e.ToString());
                }
            }
            else
            {
                FileStream fileStream = null;
                try
                {
                    fileStream = System.IO.File.OpenRead(filePath);
                    buffer = new byte[fileStream.Length];
                    fileStream.Read(buffer, 0, buffer.Length);
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                }
                finally
                {
                    if (fileStream != null)
                        fileStream.Close();
                }
            }
            if (buffer != null)
            {
                newDocument = new File
                {
                    Content = buffer,
                    Name = Path.GetFileNameWithoutExtension( filePath ),
                    OwnerUser = Program.CurrentUser,
                    FileName = Path.GetFileName( filePath )
                };
                Program.DbSingletone.SubmitChanges( );
            }
            return newDocument;
        }

        void FillObtainedFiles()
        {
            _obtainFiles.Clear();
            var groupsMember = Program.CurrentUser.UsersGroupsUsers.Select(usu => usu.UsersGroup);
            var files = from file in Program.DbSingletone.Files
                join filesUsersGroups in Program.DbSingletone.FilesUsersGroups on file.FileId equals filesUsersGroups.FileId
                where groupsMember.Contains(filesUsersGroups.UsersGroup) && file.OwnerUser!=Program.CurrentUser
                select file;
            foreach (var file in files)
            {
                _obtainFiles.Add(file);
            }

        }

        void FillOwnedFiles()
        {
            _ownFiles.Clear();
            var ownedFiles = Program.DbSingletone.Files.Where(file => file.OwnerUser == Program.CurrentUser);
            foreach (var ownedFile in ownedFiles)
            {
                _ownFiles.Add(ownedFile);
            }
        }

        private void _ownFilesGridView_RowUpdated( object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e )
        {
            //Program.Db.SubmitChanges();
        }

        private void repositoryItemPopupContainerEdit1_QueryPopUp( object sender, CancelEventArgs e )
        {
            e.Cancel = true;

            GridColumn column = _ownFilesGridView.FocusedColumn;
            if (column != _fileVisionColumn)
                return;

            File file = _ownFilesGridView.GetFocusedRow( ) as File;
            if (file == null ) return;


            _usersGroupGrid.DataSource = null;
            var usersGroups = Program.DbSingletone.UsersGroups.ToList(); 
            _usersGroupGrid.DataSource = usersGroups.Where(@group => true);
            _groupsView.ClearSelection();
            foreach (var selectedGroup in file.FilesUsersGroups.Select( @group => @group.UsersGroup ))
            {
                _groupsView.SelectRow(_groupsView.GetRowHandle(usersGroups.IndexOf(selectedGroup)));
            }

            e.Cancel = false;
        }

        private void repositoryItemPopupContainerEdit1_QueryDisplayText( object sender, DevExpress.XtraEditors.Controls.QueryDisplayTextEventArgs e )
        {
            if (e.EditValue == null)
                return;
            GridColumn column = _ownFilesGridView.FocusedColumn;
            if (column != _fileVisionColumn)
                return;

            var filesUsersGroups = e.EditValue as IEnumerable<FilesUsersGroup>;
            
            e.DisplayText =filesUsersGroups.Any()? String.Join(", ",
                filesUsersGroups.Select(@group => @group.UsersGroup.UserGroupName)): "";
            //Trace.WriteLine( "_ " + e.DisplayText + " / " + (filesUsersGroups.Any( ) ? String.Join( ", ",
            //    filesUsersGroups.Select( @group => @group.UsersGroup.UserGroupName ) ) : "" ));
            //if (String.IsNullOrEmpty(e.DisplayText))
            //    e.DisplayText = "!!!";
        }

        private void repositoryItemPopupContainerEdit1_QueryResultValue( object sender, DevExpress.XtraEditors.Controls.QueryResultValueEventArgs e )
        {
            e.Value = null;

            GridColumn column = _ownFilesGridView.FocusedColumn;
            if (column != _fileVisionColumn)
                return;

            File file = _ownFilesGridView.GetFocusedRow( ) as File;

            if (file == null) return;

            var selectedGroups = _groupsView.GetSelectedRows( ).Select( rowHandle => _groupsView.GetRow( rowHandle ) as UsersGroup );
            var currentGroups = new List<FilesUsersGroup>(file.FilesUsersGroups);
            foreach (var userGroup in selectedGroups)
            {
                var condemned = currentGroups.SingleOrDefault(@group => @group.UsersGroup == userGroup);
                if (condemned!=null)
                    currentGroups.Remove( condemned );
                else
                    Program.DbSingletone.FilesUsersGroups.InsertOnSubmit(new FilesUsersGroup(){File = file,UsersGroup = userGroup});
            }
            foreach (var currentGroup in currentGroups)
            {
                Program.DbSingletone.FilesUsersGroups.DeleteOnSubmit(currentGroup);
            }
            Program.DbSingletone.SubmitChanges();
            e.Value = file.FilesUsersGroups;
        }

        private void _obtainedFilesGrid_EmbeddedNavigator_ButtonClick( object sender, NavigatorButtonClickEventArgs e )
        {
            switch (e.Button.ButtonType)
            {
                    case NavigatorButtonType.Custom:
                    var selectedItem = _obtainedFilesView.GetSelectedItem() as File;
                        var tag = (e.Button.Tag as string).Split(new[] {'#'})[0];
                        switch (tag)
                        {
                            case "Save":
                                if(selectedItem == null)
                                    return;
                                DownloadFile(selectedItem);
                                e.Handled = true;
                                break;
                            case "Refresh":
                                FillObtainedFiles();
                                break;
                        }
                    break;
            }
        }

        private void DownloadFile( File selectedItem )
        {
            var saveFileDialog = new SaveFileDialog();
            saveFileDialog.FileName = selectedItem.FileName;
            saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                FileStream fileStream = null;
                try
                {
                    fileStream = System.IO.File.OpenWrite(saveFileDialog.FileName);
                    byte[] array = selectedItem.Content.ToArray();
                    fileStream.Write(array, 0, array.Length);
                }
                catch (UnauthorizedAccessException ex)
                {
                    MessageBox.ShowError("Недостаточно прав для записи файла в данную папку",this);
                }
                catch (Exception ex)
                {
                    MessageBox.ShowError( "Произошла неизвестная ошибка, возможно файл повреждён.", this );
                    Log.Error(ex);
                }
                finally
                {
                    if (fileStream != null)
                        fileStream.Close();
                }
            }
        }

        private void _ownFilesGridView_CustomColumnDisplayText( object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e )
        {
            if (e.Column == _fileVisionColumn)
            {
                var filesUsersGroups = (_ownFilesGridView.GetRow(e.RowHandle) as File).FilesUsersGroups;

                e.DisplayText = filesUsersGroups != null && filesUsersGroups.Any( ) ? String.Join( ", ",
                    filesUsersGroups.Select( @group => @group.UsersGroup.UserGroupName ) ) : "";
            }
        }

    }
}

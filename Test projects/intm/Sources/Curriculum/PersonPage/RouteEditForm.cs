﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Eureca.Integrator.Extensions;
using Eureca.Utility.Helper;

namespace Curriculum.PersonPage
{
    public partial class RouteEditForm : Form
    {
        BindingListWithRemoveEvent<TargetView> _targets = new BindingListWithRemoveEvent<TargetView>( );

        private readonly List<Operation> _dataSource = Program.DbSingletone.Operations.Where(
            operation => operation != Integrator.Curriculum.Constants.Operations.Return).ToList();

        public RouteEditForm(IEnumerable<TargetView> targets )
        {
            InitializeComponent();

            _receiversComboEdit.Items.AddRange(Program.DbSingletone.Users.ToArray());
            _operationsGrid.DataSource = _dataSource;
            _targets.AddingNew += _targets_AddingNew;
            foreach (var targetView in targets)
            {
                _targets.Add(targetView);
            }
            _targets.ClearHistory();
        }

        public IListChangesHistory<TargetView> Targets
        {
            get { return _targets; }
        }
        w
        void _targets_AddingNew( object sender, AddingNewEventArgs e )
        {
            ( e.NewObject as TargetView ).PropertyChanged += RouteEditForm_PropertyChanged;
        }

        void RouteEditForm_PropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            _targets.MarkAsChanged(sender as TargetView);
        }

        private void _routeTargetsGrid_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    e.Handled = true;
                    var tag = e.Button.Tag as string;
                    var selectedItem = _routeTargetsGridView.GetSelectedItem() as TargetView;
                    if(selectedItem == null)
                        return;
                    switch (tag)
                    {
                        case "RaiseBtn":
                            var itemPosition = selectedItem.Order;
                            var swapedTarget = _targets.Single(view => view.Order == ++selectedItem.Order);
                            swapedTarget.Order--;
                            _routeTargetsGridView.RefreshData();//Row( _routeTargetsGridView.GetRowHandle());
                            break;

                        case "LowerBtn":
                            itemPosition = selectedItem.Order;
                            swapedTarget = _targets.Single(view => view.Order == --selectedItem.Order);
                            swapedTarget.Order++;
                            _routeTargetsGridView.RefreshData();//Row( _routeTargetsGridView.GetRowHandle());
                            break;
                    }
                    break;
            }
        }

        private void _allowedOperationsPopupContainerEdit_QueryPopUp( object sender, CancelEventArgs e )
        {
            e.Cancel = true;
            var selectedItem = _routeTargetsGridView.GetSelectedItem() as TargetView; 
            if(selectedItem == null)
                return;
            _operationGridView.ClearSelection();
            if (selectedItem.Operations.Any())
            {
                selectedItem.Operations.ForEach(operation => _operationGridView.SelectRow(_dataSource.IndexOf(operation)));
            }
            e.Cancel = false;
        }

        private void _allowedOperationsPopupContainerEdit_QueryDisplayText( object sender, DevExpress.XtraEditors.Controls.QueryDisplayTextEventArgs e )
        {
            e.DisplayText = "";
            var selectedOperation = e.EditValue as List<Operation>;
            if(selectedOperation == null)
                return;
            e.DisplayText = string.Join( ", ", selectedOperation.Select(operation => operation.Name) );
        }

        private void _allowedOperationsPopupContainerEdit_QueryResultValue( object sender, DevExpress.XtraEditors.Controls.QueryResultValueEventArgs e )
        {
            var selectedItem = _routeTargetsGridView.GetSelectedItem( ) as TargetView;
            if (selectedItem == null)
                return;
            var selectedOperations = _routeTargetsGridView.GetSelectedRows( ).Select( handle => _routeTargetsGridView.GetRow(handle) as Operation ).ToList();
            selectedItem.Operations = selectedOperations;
            e.Value = selectedOperations;
        }
    }

    public class TargetView :INotifyPropertyChanged
    {
        private List<Operation> _operations;

        public List<Operation> Operations
        {
            get { return _operations; }
            set { _operations = value; NotifyPropertyChanged("Operations"); }
        }

        private User _receiver;

        public User Receiver
        {
            get { return _receiver; }
            set { _receiver = value; NotifyPropertyChanged( "Receiver" ); }
        }
        
        private int _order;

        public int Order
        {
            get { return _order; }
            set { _order = value; NotifyPropertyChanged( "Order" ); }
        }

        public TargetView(IEnumerable<Operation> operations, User receiver, int order)
        {
            _operations = new List<Operation>(operations);
            _receiver = receiver;
            _order = order; 
        }

        public int TargetId { get; set; }   

        public TargetView()
        {
          _operations = new List<Operation>(); 
        }

        #region INotifyPropertyChanged Members

        private void NotifyPropertyChanged(string props)
        {
            if(PropertyChanged == null)
                return;
            if(string.IsNullOrEmpty(props))
                PropertyChanged(this,new PropertyChangedEventArgs(""));
            else
                foreach (var prop in props.Split(new []{','},StringSplitOptions.RemoveEmptyEntries))
                {
                    PropertyChanged( this, new PropertyChangedEventArgs( prop ) );
                }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
    }

}
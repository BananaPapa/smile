﻿
namespace Curriculum.Forms
{
    partial class DocumentsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if (disposing && ( components != null ))
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DocumentsForm));
            this._mainTabControl = new DevExpress.XtraTab.XtraTabControl();
            this._recievedDocuments = new DevExpress.XtraTab.XtraTabPage();
            this._recievedDocumentsGrid = new DevExpress.XtraGrid.GridControl();
            this._imageList = new System.Windows.Forms.ImageList(this.components);
            this._recievedDocumentsView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._fileNameColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._senderNameColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._messageColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._operationNameColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this._popupFileGridContainer = new DevExpress.XtraEditors.PopupContainerControl();
            this._popupFilesGrid = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._gridFileName = new DevExpress.XtraGrid.Columns.GridColumn();
            this._gridOwnerName = new DevExpress.XtraGrid.Columns.GridColumn();
            this._filePopupSelector = new DevExpress.XtraEditors.PopupContainerEdit();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this._sendBtn = new DevExpress.XtraEditors.SimpleButton();
            this._autoWidthLayoutPanelControl = new Eureca.Integrator.Common.Controls.AutoWidthLayoutPanel();
            this._messageMemo = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this._mainTabControl)).BeginInit();
            this._mainTabControl.SuspendLayout();
            this._recievedDocuments.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._recievedDocumentsGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._recievedDocumentsView)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._popupFileGridContainer)).BeginInit();
            this._popupFileGridContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._popupFilesGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._filePopupSelector.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._messageMemo.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // _mainTabControl
            // 
            this._mainTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._mainTabControl.Location = new System.Drawing.Point(0, 0);
            this._mainTabControl.Name = "_mainTabControl";
            this._mainTabControl.SelectedTabPage = this._recievedDocuments;
            this._mainTabControl.Size = new System.Drawing.Size(991, 762);
            this._mainTabControl.TabIndex = 0;
            this._mainTabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this._recievedDocuments,
            this.xtraTabPage2});
            // 
            // _recievedDocuments
            // 
            this._recievedDocuments.Controls.Add(this._recievedDocumentsGrid);
            this._recievedDocuments.Name = "_recievedDocuments";
            this._recievedDocuments.Size = new System.Drawing.Size(986, 736);
            this._recievedDocuments.Text = "Полученные документы";
            // 
            // _recievedDocumentsGrid
            // 
            this._recievedDocumentsGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._recievedDocumentsGrid.EmbeddedNavigator.Buttons.Append.Visible = false;
            this._recievedDocumentsGrid.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this._recievedDocumentsGrid.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this._recievedDocumentsGrid.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this._recievedDocumentsGrid.EmbeddedNavigator.Buttons.ImageList = this._imageList;
            this._recievedDocumentsGrid.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this._recievedDocumentsGrid.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Ответить", "Handle")});
            this._recievedDocumentsGrid.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this._recievedDocumentsGrid_EmbeddedNavigator_ButtonClick);
            this._recievedDocumentsGrid.Location = new System.Drawing.Point(0, 0);
            this._recievedDocumentsGrid.MainView = this._recievedDocumentsView;
            this._recievedDocumentsGrid.Name = "_recievedDocumentsGrid";
            this._recievedDocumentsGrid.Size = new System.Drawing.Size(986, 736);
            this._recievedDocumentsGrid.TabIndex = 0;
            this._recievedDocumentsGrid.UseEmbeddedNavigator = true;
            this._recievedDocumentsGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this._recievedDocumentsView});
            // 
            // _imageList
            // 
            this._imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("_imageList.ImageStream")));
            this._imageList.TransparentColor = System.Drawing.Color.Transparent;
            this._imageList.Images.SetKeyName(0, "new-24-128.png");
            // 
            // _recievedDocumentsView
            // 
            this._recievedDocumentsView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._fileNameColumn,
            this._senderNameColumn,
            this._messageColumn,
            this._operationNameColumn});
            this._recievedDocumentsView.GridControl = this._recievedDocumentsGrid;
            this._recievedDocumentsView.Images = this._imageList;
            this._recievedDocumentsView.Name = "_recievedDocumentsView";
            this._recievedDocumentsView.OptionsView.ShowGroupPanel = false;
            // 
            // _fileNameColumn
            // 
            this._fileNameColumn.Caption = "Файл";
            this._fileNameColumn.FieldName = "FileName";
            this._fileNameColumn.Name = "_fileNameColumn";
            this._fileNameColumn.OptionsColumn.AllowEdit = false;
            this._fileNameColumn.OptionsColumn.ReadOnly = true;
            this._fileNameColumn.Visible = true;
            this._fileNameColumn.VisibleIndex = 0;
            // 
            // _senderNameColumn
            // 
            this._senderNameColumn.Caption = "Отправитель";
            this._senderNameColumn.FieldName = "SenderName";
            this._senderNameColumn.Name = "_senderNameColumn";
            this._senderNameColumn.OptionsColumn.AllowEdit = false;
            this._senderNameColumn.OptionsColumn.ReadOnly = true;
            this._senderNameColumn.Visible = true;
            this._senderNameColumn.VisibleIndex = 1;
            // 
            // _messageColumn
            // 
            this._messageColumn.Caption = "Сообщения";
            this._messageColumn.FieldName = "Message";
            this._messageColumn.Name = "_messageColumn";
            this._messageColumn.OptionsColumn.AllowEdit = false;
            this._messageColumn.OptionsColumn.ReadOnly = true;
            this._messageColumn.Visible = true;
            this._messageColumn.VisibleIndex = 2;
            // 
            // _operationNameColumn
            // 
            this._operationNameColumn.Caption = "Операция";
            this._operationNameColumn.FieldName = "OperationName";
            this._operationNameColumn.Name = "_operationNameColumn";
            this._operationNameColumn.OptionsColumn.AllowEdit = false;
            this._operationNameColumn.OptionsColumn.ReadOnly = true;
            this._operationNameColumn.Visible = true;
            this._operationNameColumn.VisibleIndex = 3;
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this._popupFileGridContainer);
            this.xtraTabPage2.Controls.Add(this._filePopupSelector);
            this.xtraTabPage2.Controls.Add(this.simpleButton1);
            this.xtraTabPage2.Controls.Add(this._sendBtn);
            this.xtraTabPage2.Controls.Add(this._autoWidthLayoutPanelControl);
            this.xtraTabPage2.Controls.Add(this._messageMemo);
            this.xtraTabPage2.Controls.Add(this.labelControl2);
            this.xtraTabPage2.Controls.Add(this.labelControl1);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(986, 736);
            this.xtraTabPage2.Text = "Создать документ";
            // 
            // _popupFileGridContainer
            // 
            this._popupFileGridContainer.Controls.Add(this._popupFilesGrid);
            this._popupFileGridContainer.Location = new System.Drawing.Point(683, 34);
            this._popupFileGridContainer.Name = "_popupFileGridContainer";
            this._popupFileGridContainer.Size = new System.Drawing.Size(270, 211);
            this._popupFileGridContainer.TabIndex = 9;
            // 
            // _popupFilesGrid
            // 
            this._popupFilesGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._popupFilesGrid.Location = new System.Drawing.Point(0, 0);
            this._popupFilesGrid.MainView = this.gridView1;
            this._popupFilesGrid.Name = "_popupFilesGrid";
            this._popupFilesGrid.Size = new System.Drawing.Size(270, 211);
            this._popupFilesGrid.TabIndex = 0;
            this._popupFilesGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._gridFileName,
            this._gridOwnerName});
            this.gridView1.GridControl = this._popupFilesGrid;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // _gridFileName
            // 
            this._gridFileName.Caption = "Имя файла";
            this._gridFileName.FieldName = "Name";
            this._gridFileName.Name = "_gridFileName";
            this._gridFileName.Visible = true;
            this._gridFileName.VisibleIndex = 0;
            // 
            // _gridOwnerName
            // 
            this._gridOwnerName.Caption = "Владелец";
            this._gridOwnerName.FieldName = "Owner.Name";
            this._gridOwnerName.Name = "_gridOwnerName";
            this._gridOwnerName.Visible = true;
            this._gridOwnerName.VisibleIndex = 1;
            // 
            // _filePopupSelector
            // 
            this._filePopupSelector.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._filePopupSelector.EditValue = "Выберите файл";
            this._filePopupSelector.Location = new System.Drawing.Point(126, 18);
            this._filePopupSelector.Name = "_filePopupSelector";
            this._filePopupSelector.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._filePopupSelector.Properties.PopupControl = this._popupFileGridContainer;
            this._filePopupSelector.Size = new System.Drawing.Size(719, 20);
            this._filePopupSelector.TabIndex = 8;
            this._filePopupSelector.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this._filePopupSelector_QueryResultValue);
            this._filePopupSelector.QueryDisplayText += new DevExpress.XtraEditors.Controls.QueryDisplayTextEventHandler(this._filePopupSelector_QueryDisplayText);
            this._filePopupSelector.QueryPopUp += new System.ComponentModel.CancelEventHandler(this._filePopupSelector_QueryPopUp);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(868, 16);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(96, 23);
            this.simpleButton1.TabIndex = 7;
            this.simpleButton1.Text = "Добавить файл";
            // 
            // _sendBtn
            // 
            this._sendBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._sendBtn.Location = new System.Drawing.Point(890, 699);
            this._sendBtn.Name = "_sendBtn";
            this._sendBtn.Size = new System.Drawing.Size(75, 23);
            this._sendBtn.TabIndex = 6;
            this._sendBtn.Text = "Отправить";
            this._sendBtn.Click += new System.EventHandler(this._sendBtn_Click);
            // 
            // _autoWidthLayoutPanelControl
            // 
            this._autoWidthLayoutPanelControl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._autoWidthLayoutPanelControl.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this._autoWidthLayoutPanelControl.Location = new System.Drawing.Point(29, 205);
            this._autoWidthLayoutPanelControl.Name = "_autoWidthLayoutPanelControl";
            this._autoWidthLayoutPanelControl.Size = new System.Drawing.Size(936, 479);
            this._autoWidthLayoutPanelControl.TabIndex = 5;
            // 
            // _messageMemo
            // 
            this._messageMemo.Location = new System.Drawing.Point(29, 64);
            this._messageMemo.Name = "_messageMemo";
            this._messageMemo.Size = new System.Drawing.Size(935, 112);
            this._messageMemo.TabIndex = 4;
            this._messageMemo.UseOptimizedRendering = true;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(29, 44);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(58, 13);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "Сообщение";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(29, 21);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(30, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Файл:";
            // 
            // DocumentsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(991, 762);
            this.Controls.Add(this._mainTabControl);
            this.Icon = global::Curriculum.Properties.Resources.Documents_ico;
            this.Name = "DocumentsForm";
            this.Text = "Документы";
            ((System.ComponentModel.ISupportInitialize)(this._mainTabControl)).EndInit();
            this._mainTabControl.ResumeLayout(false);
            this._recievedDocuments.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._recievedDocumentsGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._recievedDocumentsView)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            this.xtraTabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._popupFileGridContainer)).EndInit();
            this._popupFileGridContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._popupFilesGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._filePopupSelector.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._messageMemo.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTab.XtraTabControl _mainTabControl;
        private DevExpress.XtraTab.XtraTabPage _recievedDocuments;
        private DevExpress.XtraGrid.GridControl _recievedDocumentsGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView _recievedDocumentsView;
        private DevExpress.XtraGrid.Columns.GridColumn _fileNameColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _senderNameColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _messageColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _operationNameColumn;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraEditors.MemoEdit _messageMemo;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton _sendBtn;
        private Eureca.Integrator.Common.Controls.AutoWidthLayoutPanel _autoWidthLayoutPanelControl;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private System.Windows.Forms.ImageList _imageList;
        private DevExpress.XtraEditors.PopupContainerControl _popupFileGridContainer;
        private DevExpress.XtraGrid.GridControl _popupFilesGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn _gridFileName;
        private DevExpress.XtraGrid.Columns.GridColumn _gridOwnerName;
        private DevExpress.XtraEditors.PopupContainerEdit _filePopupSelector;
    }
}
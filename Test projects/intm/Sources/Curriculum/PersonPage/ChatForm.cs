﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Curriculum.Controls;
using Curriculum.PersonPage;
using DevExpress.Charts.Native;
using DevExpress.XtraEditors;
using DevExpress.XtraTab;
using Eureca.Integrator.Common;
using Eureca.Utility.Extensions;

namespace Curriculum.Forms
{
    public partial class ChatForm : FormWithAccessAreas, IMessageListener
    {
        Dictionary<int,ChatControl> _dialogs = new Dictionary<int, ChatControl>();
        Dictionary<ChatControl, XtraTabPage> _dialogsPages = new Dictionary<ChatControl, XtraTabPage>( ); 
        List<User> _selectedUsers = new List<User>(); 
        
        public ChatForm( )
        {
            InitializeComponent( );
            _usersListGrid.EmbeddedNavigator.CustomButtons[0].Enabled = false;
            Load += ChatForm_Load;
        }

        void ChatForm_Load( object sender, EventArgs e )
        {
            if (!Program.PermissionManager.CurrentRole.Admin)
            {
                var usersGroup =
                    Program.DbSingletone.UsersVisions.Where( ugu => ugu.User == Program.CurrentUser )
                        .Select( usu => usu.UsersGroup ).SelectMany( @group => @group.UsersGroupsUsers );
                _usersListGrid.DataSource =
                    usersGroup.Select( usu => usu.User ).Distinct( ).Where( user => user != Program.CurrentUser );
            }
            else
                _usersListGrid.DataSource = Program.DbSingletone.Users.Where(user => user!=Program.CurrentUser);
        }

        #region IMessageListener Members

        public void OnNewMessage(IEnumerable<ReceiveMessageArgs> args)
        {
            foreach (var receiveMessageArg in args)
            {
                var newMessage = receiveMessageArg.Message;
                var sender = newMessage.SenderUser;
                if (_dialogs.ContainsKey(sender.UserId))
                {
                    var chatControl = _dialogs[sender.UserId];
                    chatControl.ReceiveMessage(receiveMessageArg);
                }
                else
                {
                    var chatControl = CreateDialog(sender);
                    chatControl.ReceiveMessage(receiveMessageArg);

                }
            }
        }

        private ChatControl CreateDialog(User sender)
        {
            ChatControl chatControl = null;
            this.Invoke((Action)(()=>
            {
                XtraTabPage xtraTabPage = new XtraTabPage();
                _dialogsTabControl.TabPages.Add(xtraTabPage);
                chatControl = new ChatControl(sender) {Dock = DockStyle.Fill};
                chatControl.EndOfConversation += chatControl_EndOfConversation;
                chatControl.MessagesChanged += chatControl_MessagesChanged;
                xtraTabPage.Controls.Add(chatControl);
                xtraTabPage.Text = sender.Name;
                _dialogs.Add(sender.UserId, chatControl);
                _dialogsPages.Add(chatControl, xtraTabPage);
            }));
            return chatControl;
        }

        void chatControl_MessagesChanged( object sender, MessagesChangedArgs e )
        {
            var chatControl = sender as ChatControl;
            UpdateTabHeader(_dialogsPages[chatControl],e.MessageCount, chatControl.Interlacutor);
        }

        private void UpdateTabHeader(XtraTabPage page,int messageCount ,User user)
        {
            if (_dialogsTabControl.InvokeRequired)
                _dialogsTabControl.Invoke((Action<XtraTabPage,int, User>) UpdateTabHeader, new object[] {page,messageCount, user});
            else
            {
                var userName = user.Name;

                if (messageCount == 0)
                {
                    page.Text = userName;
                    page.Appearance.Header.ForeColor = Color.Black;
                }
                else
                {
                    page.Text = "{0} ({1})".FormatString( userName, messageCount );
                    page.Appearance.Header.ForeColor = Color.DarkGreen;
                }
            }
        }

        void chatControl_EndOfConversation( object sender, EventArgs e )
        {
            var chatControl = sender as ChatControl;
            var pair = _dialogs.Single(valuePair => valuePair.Value == chatControl);
            _dialogs.Remove(pair.Key);
            _dialogsTabControl.TabPages.Remove(_dialogsPages[pair.Value]);
        }

        public void OnMessageHasBeenHandled( ReceiveMessageArgs args )
        {
            
        }

        #endregion

        private void _usersListGrid_EmbeddedNavigator_ButtonClick( object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e )
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    var buttonTag = (e.Button.Tag as string).Split('#')[0];
                    switch (buttonTag)
                    {
                        case "SendMessage":
                            SendMulticast(_selectedUsers);
                        break;
                    }

                    e.Handled = true;
                break;
            }
        }

        void SendMulticast(IEnumerable<User> users)
        {
            var requestMessageForm = new RequestMessageForm(users){Receivers = _selectedUsers};
            if (requestMessageForm.ShowDialog(this) == DialogResult.OK)
            {
                string messageText = requestMessageForm.Message;
                var curriculumDataContext = Program.Db;
                var sender = curriculumDataContext.Users.FirstOrDefault( user => user.UserId == Program.CurrentUser.UserId );
                foreach (var receiver in requestMessageForm.Receivers)
                {
                    var message = new Message
                    {
                        ReceiverUser = curriculumDataContext.Users.FirstOrDefault( user => user.UserId == receiver.UserId ),
                        SenderUser = sender,
                        CreationDate = DateTime.Now,
                        MessageText = messageText
                    };
                    if (_dialogs.ContainsKey(receiver.UserId))
                    {
                        _dialogs[receiver.UserId].ShowMessage(message);
                    }
                    else
                    {
                        var dialog = CreateDialog(receiver);
                        dialog.ShowMessage(message);
                    }
                    curriculumDataContext.Messages.InsertOnSubmit(message);
                }
                curriculumDataContext.SubmitChanges();
            }
        }

        private void _usersView_SelectionChanged( object sender, DevExpress.Data.SelectionChangedEventArgs e )
        {
            _selectedUsers.Clear();
            var selectedRowsHandles = _usersView.GetSelectedRows();
            _usersListGrid.EmbeddedNavigator.CustomButtons[0].Enabled = selectedRowsHandles.Length != 0;
            foreach (var selectedRowsHandle in selectedRowsHandles)
            {
                _selectedUsers.Add(_usersView.GetRow(selectedRowsHandle) as User);    
            }
        }
    }

}

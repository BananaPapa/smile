﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Integrator.Curriculum.Constants;

namespace Curriculum.Forms
{
    public partial class DocumentHandleForm : Form
    {
        private Document _document;
        private Target _target;

        public DocumentHandleForm( Document document , Target target )
        {
            _document = document;
            _target = target;
            InitializeComponent( );
        }

        private void ReturnDocument(Document document, string message)
        {
            if (_target.TargetsType == TargetsTypes.Route)
            {
                var route = Program.DbSingletone.Routes.Single(route1 => route1.RouteId == _target.TargetObjectId);
                var currentTarget =
                    Program.DbSingletone.RoutesSteps.Single(
                        step => step.RouteId == route.RouteId && step.Order == route.CurrentStep);
            }
            else if (_target.TargetsType == TargetsTypes.Person)
            {
                

            }
            //return null;
        }
    }
}

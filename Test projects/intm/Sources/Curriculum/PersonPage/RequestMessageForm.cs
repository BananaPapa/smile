﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Curriculum.Controls;
using DevExpress.XtraGrid;
using Eureca.Utility.Extensions;
using MessageBox = Eureca.Integrator.Utility.Forms.MessageBox;

namespace Curriculum.PersonPage
{
    public partial class RequestMessageForm : Form
    {
        private const string _formTitleTemplate = "Сообщение для: {0}";

        //private IEnumerable<User> allUsers;
        private UsersSelectionControl _usersSelectionControl = new UsersSelectionControl();
        
        public IEnumerable<User> Receivers
        {
            get
            {
                return _usersSelectionControl.SelectedUsers;
            }
            set
            {
                _usersSelectionControl.SelectedUsers = value;
                Text = _formTitleTemplate.FormatString(String.Join(", ",
                    _usersSelectionControl.SelectedUsers.Select(user => user.Name)));
            }
        }

        public string Message
        {
            get;
            set;
        }

        public RequestMessageForm(IEnumerable<User> allUsers)
        {
            InitializeComponent( );
            _textMessage.DataBindings.Add("Text", this, "Message");
            _usersSelectionControl.Init(allUsers);
        }

        private void simpleButton2_Click( object sender, EventArgs e )
        {
            if(ValidateMessage())
                DialogResult = DialogResult.OK;
        }

        private bool ValidateMessage()
        {
            if(String.IsNullOrEmpty(Message))
            {
                MessageBox.ShowValidationError("Сообщение не может быть пустым",this);
                return false;
            }
            return true;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPrinting;
using Eureca.Integrator.Common;
using Eureca.Integrator.Extensions;
using Integrator.Curriculum.Constants;
using NHibernate.Hql.Ast.ANTLR;

namespace Curriculum.Forms
{
    public partial class DocumentsForm : FormWithAccessAreas
    {
        BindingList<DocumentView> _documentViews = new BindingList<DocumentView>();
        private User _currentUser;
        private File _fileToSend;

        public DocumentsForm( User user)
        {
            _currentUser = user;
            InitializeComponent( );
            //_selectFileGridLookUp.Click += _selectFileGridLookUp_Click;
        }

        //void _selectFileGridLookUp_Click( object sender, EventArgs e )
        //{
        //    var ownFiles = Program.DbSingletone.Files.Where(file => file.OwnerUser == _currentUser);
        //    var groupsMember = Program.CurrentUser.UsersGroupsUsers.Select(usu => usu.UsersGroup);
        //    var obtainedFiles = from file in Program.DbSingletone.Files
        //        join filesUsersGroups in Program.DbSingletone.FilesUsersGroups on file.FileId equals filesUsersGroups.FileId
        //        where groupsMember.Contains(filesUsersGroups.UsersGroup) && file.OwnerUser!=Program.CurrentUser
        //        select file;

        //    _selectFileGridLookUp.Properties.DataSource = ownFiles.Concat(obtainedFiles);
        //}

        private void FillDocumentViews()
        {
            var condidates = Program.DbSingletone.Documents.Where(document => document.State != States.Complete);
            foreach (var condidate in condidates)
            {
                var route = condidate.Route;
                var routesStep =
                Program.DbSingletone.RoutesSteps.Single(
                    step => step.RouteId == route.RouteId && step.Order == route.CurrentStep );
                var target = Program.DbSingletone.Targets.Single( target1 => target1.TargetId == routesStep.RouteStepId );
                DistributeTarget(target,condidate);
            }
        }

        private void DistributeTarget(Target documentTarget, Document document)
        {
            if (documentTarget.TargetsType == TargetsTypes.Person)
            {
                var targetPerson =
                    Program.DbSingletone.Users.Single( user => user.UserId == documentTarget.TargetObjectId );
                if (targetPerson.UserId == _currentUser.UserId)
                    _documentViews.Add( new DocumentView( document, documentTarget ) );
            }
            else
            {
                throw new NotImplementedException("В качестве целей для документов поддерживаются только пользователи");
            }
            //DistributeTarget(Program.DbSingletone.Targets.Single(target => target.TargetId == routesStep.RouteStepId),condidate); // recursive call
        }

        private void _recievedDocumentsGrid_EmbeddedNavigator_ButtonClick( object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e )
        {
            switch (e.Button.ButtonType)
            {
                    case NavigatorButtonType.Custom:
                    var tag = e.Button.Tag as string;
                    if (tag == "Handle")
                    {
                        var selectedItem = _recievedDocumentsView.GetSelectedItem();
                        if (selectedItem != null)
                        {
                            //var newHandleDocumentForm = new DocumentHandleForm()
                        }
                        e.Handled = true;

                    }
                    break;
            }
        }

        private void HandleDocument(Document document)
        {
        }

        private void _sendBtn_Click( object sender, EventArgs e )
        {
            //if(_selectFileGridLookUp.sele)
        }

        private void _filePopupSelector_QueryDisplayText( object sender, DevExpress.XtraEditors.Controls.QueryDisplayTextEventArgs e )
        {
            var file = e.EditValue as File;
            if (file != null)
                e.DisplayText = "";
            e.DisplayText = file.Name;
        }

        private void _filePopupSelector_QueryResultValue( object sender, DevExpress.XtraEditors.Controls.QueryResultValueEventArgs e )
        {
            var selectedItem = (_popupFilesGrid.MainView as GridView).GetSelectedItem() as File;
            e.Value = selectedItem;
            _fileToSend = selectedItem;
        }

        private void _filePopupSelector_QueryPopUp( object sender, CancelEventArgs e )
        {
            e.Cancel = true;

            var ownFiles = Program.DbSingletone.Files.Where(file => file.OwnerUser == _currentUser);
            var groupsMember = Program.CurrentUser.UsersGroupsUsers.Select(usu => usu.UsersGroup);
            var obtainedFiles = from file in Program.DbSingletone.Files
                join filesUsersGroups in Program.DbSingletone.FilesUsersGroups on file.FileId equals filesUsersGroups.FileId
                where groupsMember.Contains(filesUsersGroups.UsersGroup) && file.OwnerUser!=Program.CurrentUser
                select file;

            var dataSource = ownFiles.Concat(obtainedFiles).Where(file => true).ToList();// i really need index of my file
            _popupFilesGrid.DataSource = dataSource;

            if (_fileToSend != null)
            {
                int selectedFileIndex = dataSource.IndexOf(_fileToSend);
                if (selectedFileIndex >0)
                {
                    var gridView = (_popupFilesGrid.MainView as GridView);
                    gridView.SelectRow(gridView.GetRowHandle(selectedFileIndex));
                }
            }
            e.Cancel = false;
        }
    }

    internal class DocumentView
    {
        private Document _documentModel = null;
        private Target _targetModel = null;

        public DocumentView(Document documentModel, Target targetModel)
        {
            _documentModel = documentModel;
            _targetModel = targetModel;
        }

        public string DocumentName { get { return _documentModel.Attachments.First().File.Name; }
    }

        public string OperationName {
            get
            {
                return ""; 
                //_targetModel.Operation.Name;
            }
        }

        public string Message { get
        {
            return ""; //_targetModel.Message; 
        } }

        public string SenderName { get { return GetSenderName(); } }

        private string GetSenderName()
        {
            if (_targetModel.TargetsType.Name == "Person")
            {
                var sender = Program.DbSingletone.Users.Single(user => user.UserId == _targetModel.TargetObjectId);
                return sender.Name;
            }
            else
            {
                throw new NotImplementedException("Unsupported target type");
            }
        }


        private void Init()
        {
            //DocumentName = _documentModel.
        }
    }
}

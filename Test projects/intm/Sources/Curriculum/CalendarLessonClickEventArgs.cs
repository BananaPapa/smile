﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Curriculum
{
    /// <summary>
    /// Provides data for the Lesson click events of the CalendarForm.
    /// </summary>
    public class CalendarLessonClickEventArgs : LessonClickEventArgs
    {
        public CalendarLessonClickEventArgs(Guid personId, Lesson lesson)
            : base(lesson)
        {
            PersonId = personId;
        }

        /// <summary>
        /// Gets or sets person unique identifier.
        /// </summary>
        public Guid PersonId { get; private set; }
    }
}

﻿using System;

namespace Curriculum
{
    /// <summary>
    /// Provides data for the Lesson click events of the LessonControl.
    /// </summary>
    public class LessonClickEventArgs : EventArgs
    {
        public LessonClickEventArgs(Lesson lesson)
        {
            if (lesson != null)
            {
                if (lesson.Discipline.TestGuid.HasValue)
                    TestGuid = lesson.Discipline.TestGuid.Value;
                AttachmentsFolder = lesson.Discipline.AttachmentsFolder;
            }
        }

        /// <summary>
        /// Gets or sets test unique identifier.
        /// </summary>
        public Guid TestGuid { get; set; }

        /// <summary>
        /// Gets or sets attachments folder
        /// </summary>
        public string AttachmentsFolder { get; set; }
    }

}
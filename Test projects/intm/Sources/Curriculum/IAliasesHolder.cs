namespace Curriculum
{
    /// <summary>
    /// Interface for controls that hold aliases.
    /// </summary>
    internal interface IAliasesHolder
    {
        /// <summary>
        /// Applies aliases for control components.
        /// </summary>
        void ApplyAliases();
    }
}
﻿

using Eureca.Professional.BaseComponents.Data;

namespace Eureca.Professional.ProfessionalTeacher
{
    partial class XtraReportCommonProfiles
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.labelFIO = new DevExpress.XtraReports.UI.XRLabel();
            this.labelBirthDay = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelBirthday = new DevExpress.XtraReports.UI.XRLabel();
            this.labelProfileName = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelTime = new DevExpress.XtraReports.UI.XRLabel();
            this.labelTime = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelProfileName = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelStatistic = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableMethodTitle = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableTestTitle = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableScaleName = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableMaxEff = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableResult = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTablePercent = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableValuation = new DevExpress.XtraReports.UI.XRTableCell();
            this.winControlContainer1 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.xrLabelCaption = new DevExpress.XtraEditors.LabelControl();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.dataSetProfessional1 = new DataSetProfessional();
            this.xrControlStyle1 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrControlStyle2 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.GroupHeader2 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSetProfessional1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.Detail.Height = 25;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable1
            // 
            this.xrTable1.BackColor = System.Drawing.Color.Empty;
            this.xrTable1.BorderColor = System.Drawing.Color.Empty;
            this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable1.BorderWidth = 0;
            this.xrTable1.EvenStyleName = "xrControlStyle1";
            this.xrTable1.Location = new System.Drawing.Point(267, 0);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.OddStyleName = "xrControlStyle2";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable1.Size = new System.Drawing.Size(392, 22);
            this.xrTable1.StylePriority.UseBorderWidth = false;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCell5,
            this.xrTableCell9,
            this.xrTableCell10,
            this.xrTableCell6});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Size = new System.Drawing.Size(392, 22);
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "vCommonTests.ScaleName", "")});
            this.xrTableCell4.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell4.Multiline = true;
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell4.Size = new System.Drawing.Size(133, 22);
            this.xrTableCell4.Text = "xrTableCell4";
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "vCommonTests.MaxEff", "")});
            this.xrTableCell5.Location = new System.Drawing.Point(133, 0);
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell5.Size = new System.Drawing.Size(92, 22);
            this.xrTableCell5.Text = "xrTableCell5";
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "vCommonTests.Result", "")});
            this.xrTableCell9.Location = new System.Drawing.Point(225, 0);
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell9.Size = new System.Drawing.Size(66, 22);
            this.xrTableCell9.Text = "xrTableCell9";
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "vCommonTests.Perc", "")});
            this.xrTableCell10.Location = new System.Drawing.Point(291, 0);
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell10.Size = new System.Drawing.Size(58, 22);
            this.xrTableCell10.Text = "xrTableCell10";
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "vCommonTests.Valuation", "")});
            this.xrTableCell6.Location = new System.Drawing.Point(349, 0);
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell6.Size = new System.Drawing.Size(43, 22);
            this.xrTableCell6.Text = "xrTableCell6";
            // 
            // xrTable3
            // 
            this.xrTable3.Location = new System.Drawing.Point(17, 0);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable3.Size = new System.Drawing.Size(250, 25);
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell7,
            this.xrTableCell3});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Size = new System.Drawing.Size(250, 25);
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(220)))), ((int)(((byte)(255)))));
            this.xrTableCell7.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "vCommonTests.MethodTitle", "")});
            this.xrTableCell7.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell7.Multiline = true;
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell7.Size = new System.Drawing.Size(117, 25);
            this.xrTableCell7.StylePriority.UseBackColor = false;
            this.xrTableCell7.Text = "xrTableCell7";
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(220)))), ((int)(((byte)(255)))));
            this.xrTableCell3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "vCommonTests.TestTitle", "")});
            this.xrTableCell3.Location = new System.Drawing.Point(117, 0);
            this.xrTableCell3.Multiline = true;
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell3.Size = new System.Drawing.Size(133, 25);
            this.xrTableCell3.StylePriority.UseBackColor = false;
            this.xrTableCell3.Text = "xrTableCell3";
            // 
            // labelFIO
            // 
            this.labelFIO.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelFIO.Location = new System.Drawing.Point(75, 67);
            this.labelFIO.Name = "labelFIO";
            this.labelFIO.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.labelFIO.Size = new System.Drawing.Size(517, 25);
            this.labelFIO.StylePriority.UseFont = false;
            this.labelFIO.StylePriority.UseTextAlignment = false;
            this.labelFIO.Text = "labelFIO";
            this.labelFIO.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // labelBirthDay
            // 
            this.labelBirthDay.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelBirthDay.Location = new System.Drawing.Point(133, 100);
            this.labelBirthDay.Name = "labelBirthDay";
            this.labelBirthDay.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.labelBirthDay.Size = new System.Drawing.Size(150, 25);
            this.labelBirthDay.StylePriority.UseFont = false;
            this.labelBirthDay.StylePriority.UseTextAlignment = false;
            this.labelBirthDay.Text = "labelBirthDay";
            this.labelBirthDay.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabelBirthday
            // 
            this.xrLabelBirthday.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.xrLabelBirthday.Location = new System.Drawing.Point(16, 100);
            this.xrLabelBirthday.Name = "xrLabelBirthday";
            this.xrLabelBirthday.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelBirthday.Size = new System.Drawing.Size(92, 25);
            this.xrLabelBirthday.StylePriority.UseFont = false;
            this.xrLabelBirthday.StylePriority.UseTextAlignment = false;
            this.xrLabelBirthday.Text = "Дата рождения: ";
            this.xrLabelBirthday.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // labelProfileName
            // 
            this.labelProfileName.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelProfileName.Location = new System.Drawing.Point(133, 133);
            this.labelProfileName.Name = "labelProfileName";
            this.labelProfileName.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.labelProfileName.Size = new System.Drawing.Size(525, 25);
            this.labelProfileName.StylePriority.UseFont = false;
            this.labelProfileName.StylePriority.UseTextAlignment = false;
            this.labelProfileName.Text = "labelProfileName";
            this.labelProfileName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabelTime
            // 
            this.xrLabelTime.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.xrLabelTime.Location = new System.Drawing.Point(17, 167);
            this.xrLabelTime.Name = "xrLabelTime";
            this.xrLabelTime.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelTime.Size = new System.Drawing.Size(108, 25);
            this.xrLabelTime.StylePriority.UseFont = false;
            this.xrLabelTime.StylePriority.UseTextAlignment = false;
            this.xrLabelTime.Text = "Время проведения:";
            this.xrLabelTime.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // labelTime
            // 
            this.labelTime.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelTime.Location = new System.Drawing.Point(133, 166);
            this.labelTime.Name = "labelTime";
            this.labelTime.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.labelTime.Size = new System.Drawing.Size(525, 25);
            this.labelTime.StylePriority.UseFont = false;
            this.labelTime.StylePriority.UseTextAlignment = false;
            this.labelTime.Text = "labelTime";
            this.labelTime.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabelProfileName
            // 
            this.xrLabelProfileName.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.xrLabelProfileName.Location = new System.Drawing.Point(16, 133);
            this.xrLabelProfileName.Name = "xrLabelProfileName";
            this.xrLabelProfileName.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelProfileName.Size = new System.Drawing.Size(92, 25);
            this.xrLabelProfileName.StylePriority.UseFont = false;
            this.xrLabelProfileName.StylePriority.UseTextAlignment = false;
            this.xrLabelProfileName.Text = "Профиль:";
            this.xrLabelProfileName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabelStatistic
            // 
            this.xrLabelStatistic.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.xrLabelStatistic.Location = new System.Drawing.Point(17, 300);
            this.xrLabelStatistic.Name = "xrLabelStatistic";
            this.xrLabelStatistic.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelStatistic.Size = new System.Drawing.Size(274, 25);
            this.xrLabelStatistic.StylePriority.UseFont = false;
            this.xrLabelStatistic.StylePriority.UseTextAlignment = false;
            this.xrLabelStatistic.Text = "Статистика прохождения тестов";
            this.xrLabelStatistic.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable2
            // 
            this.xrTable2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(214)))), ((int)(((byte)(237)))));
            this.xrTable2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(190)))), ((int)(((byte)(216)))));
            this.xrTable2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable2.Location = new System.Drawing.Point(17, 0);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable2.Size = new System.Drawing.Size(642, 67);
            this.xrTable2.StylePriority.UseBackColor = false;
            this.xrTable2.StylePriority.UseBorderColor = false;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableMethodTitle,
            this.xrTableTestTitle,
            this.xrTableScaleName,
            this.xrTableMaxEff,
            this.xrTableResult,
            this.xrTablePercent,
            this.xrTableValuation});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Size = new System.Drawing.Size(642, 67);
            // 
            // xrTableMethodTitle
            // 
            this.xrTableMethodTitle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.xrTableMethodTitle.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrTableMethodTitle.Location = new System.Drawing.Point(0, 0);
            this.xrTableMethodTitle.Multiline = true;
            this.xrTableMethodTitle.Name = "xrTableMethodTitle";
            this.xrTableMethodTitle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableMethodTitle.Size = new System.Drawing.Size(116, 67);
            this.xrTableMethodTitle.StylePriority.UseBackColor = false;
            this.xrTableMethodTitle.StylePriority.UseFont = false;
            this.xrTableMethodTitle.Text = "Тематика\r\n";
            // 
            // xrTableTestTitle
            // 
            this.xrTableTestTitle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.xrTableTestTitle.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrTableTestTitle.Location = new System.Drawing.Point(116, 0);
            this.xrTableTestTitle.Name = "xrTableTestTitle";
            this.xrTableTestTitle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableTestTitle.Size = new System.Drawing.Size(132, 67);
            this.xrTableTestTitle.StylePriority.UseBackColor = false;
            this.xrTableTestTitle.StylePriority.UseFont = false;
            this.xrTableTestTitle.Text = "Тест";
            // 
            // xrTableScaleName
            // 
            this.xrTableScaleName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.xrTableScaleName.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrTableScaleName.Location = new System.Drawing.Point(248, 0);
            this.xrTableScaleName.Name = "xrTableScaleName";
            this.xrTableScaleName.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableScaleName.Size = new System.Drawing.Size(72, 67);
            this.xrTableScaleName.StylePriority.UseBackColor = false;
            this.xrTableScaleName.StylePriority.UseFont = false;
            this.xrTableScaleName.Text = "Шкала";
            // 
            // xrTableMaxEff
            // 
            this.xrTableMaxEff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.xrTableMaxEff.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrTableMaxEff.Location = new System.Drawing.Point(320, 0);
            this.xrTableMaxEff.Multiline = true;
            this.xrTableMaxEff.Name = "xrTableMaxEff";
            this.xrTableMaxEff.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableMaxEff.Size = new System.Drawing.Size(99, 67);
            this.xrTableMaxEff.StylePriority.UseBackColor = false;
            this.xrTableMaxEff.StylePriority.UseFont = false;
            this.xrTableMaxEff.Text = "Макс.\r\nвозможный\r\nбалл";
            // 
            // xrTableResult
            // 
            this.xrTableResult.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.xrTableResult.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrTableResult.Location = new System.Drawing.Point(419, 0);
            this.xrTableResult.Multiline = true;
            this.xrTableResult.Name = "xrTableResult";
            this.xrTableResult.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableResult.Size = new System.Drawing.Size(92, 67);
            this.xrTableResult.StylePriority.UseBackColor = false;
            this.xrTableResult.StylePriority.UseFont = false;
            this.xrTableResult.Text = "Набраный\r\nбалл";
            // 
            // xrTablePercent
            // 
            this.xrTablePercent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.xrTablePercent.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrTablePercent.Location = new System.Drawing.Point(511, 0);
            this.xrTablePercent.Multiline = true;
            this.xrTablePercent.Name = "xrTablePercent";
            this.xrTablePercent.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTablePercent.Size = new System.Drawing.Size(64, 67);
            this.xrTablePercent.StylePriority.UseBackColor = false;
            this.xrTablePercent.StylePriority.UseFont = false;
            this.xrTablePercent.Text = "%\r\nот макс.";
            // 
            // xrTableValuation
            // 
            this.xrTableValuation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.xrTableValuation.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrTableValuation.Location = new System.Drawing.Point(575, 0);
            this.xrTableValuation.Name = "xrTableValuation";
            this.xrTableValuation.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableValuation.Size = new System.Drawing.Size(67, 67);
            this.xrTableValuation.StylePriority.UseBackColor = false;
            this.xrTableValuation.StylePriority.UseFont = false;
            this.xrTableValuation.Text = "Оценка";
            // 
            // winControlContainer1
            // 
            this.winControlContainer1.Location = new System.Drawing.Point(158, 17);
            this.winControlContainer1.Name = "winControlContainer1";
            this.winControlContainer1.Size = new System.Drawing.Size(414, 42);
            this.winControlContainer1.WinControl = this.xrLabelCaption;
            // 
            // xrLabelCaption
            // 
            this.xrLabelCaption.Appearance.Font = new System.Drawing.Font("Tahoma", 25F);
            this.xrLabelCaption.Appearance.Options.UseFont = true;
            this.xrLabelCaption.Location = new System.Drawing.Point(0, 0);
            this.xrLabelCaption.Name = "xrLabelCaption";
            this.xrLabelCaption.Size = new System.Drawing.Size(397, 40);
            this.xrLabelCaption.TabIndex = 0;
            this.xrLabelCaption.Text = "Общий отчет по профилю";
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo1});
            this.PageFooter.Height = 42;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Format = "Страница {0}";
            this.xrPageInfo1.Location = new System.Drawing.Point(558, 8);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.Size = new System.Drawing.Size(92, 25);
            // 
            // dataSetProfessional1
            // 
            this.dataSetProfessional1.DataSetName = "DataSetProfessional";
            this.dataSetProfessional1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // xrControlStyle1
            // 
            this.xrControlStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(227)))), ((int)(((byte)(214)))));
            this.xrControlStyle1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.xrControlStyle1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrControlStyle1.Name = "xrControlStyle1";
            this.xrControlStyle1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo2,
            this.labelProfileName,
            this.labelBirthDay,
            this.xrLabelBirthday,
            this.xrLabelStatistic,
            this.xrLabelTime,
            this.labelTime,
            this.xrLabelProfileName,
            this.labelFIO,
            this.winControlContainer1});
            this.ReportHeader.Height = 325;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // xrPageInfo2
            // 
            this.xrPageInfo2.BackColor = System.Drawing.Color.Empty;
            this.xrPageInfo2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.xrPageInfo2.Format = "Дата создания {0}";
            this.xrPageInfo2.Location = new System.Drawing.Point(450, 300);
            this.xrPageInfo2.Name = "xrPageInfo2";
            this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo2.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo2.Size = new System.Drawing.Size(208, 25);
            this.xrPageInfo2.StylePriority.UseBackColor = false;
            this.xrPageInfo2.StylePriority.UseForeColor = false;
            // 
            // xrControlStyle2
            // 
            this.xrControlStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(214)))), ((int)(((byte)(237)))));
            this.xrControlStyle2.BorderColor = System.Drawing.Color.Lime;
            this.xrControlStyle2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrControlStyle2.Name = "xrControlStyle2";
            this.xrControlStyle2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.PageHeader.Height = 67;
            this.PageHeader.Name = "PageHeader";
            // 
            // GroupHeader2
            // 
            this.GroupHeader2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3});
            this.GroupHeader2.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("MethodId", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending),
            new DevExpress.XtraReports.UI.GroupField("TestId", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.GroupHeader2.Height = 30;
            this.GroupHeader2.Name = "GroupHeader2";
            // 
            // XtraReportCommonProfiles
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageFooter,
            this.ReportHeader,
            this.PageHeader,
            this.GroupHeader2});
            this.DataMember = "vCommonTests";
            this.DataSource = this.dataSetProfessional1;
            this.Margins = new System.Drawing.Printing.Margins(100, 84, 100, 100);
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.xrControlStyle1,
            this.xrControlStyle2});
            this.Version = "8.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSetProfessional1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer1;
        public DevExpress.XtraReports.UI.XRLabel labelFIO;
        public DevExpress.XtraReports.UI.XRLabel labelBirthDay;
        public DevExpress.XtraReports.UI.XRLabel labelProfileName;
        public DevExpress.XtraReports.UI.XRLabel labelTime;
        public DataSetProfessional dataSetProfessional1;
        public DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.XRControlStyle xrControlStyle1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo2;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRControlStyle xrControlStyle2;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        public DevExpress.XtraEditors.LabelControl xrLabelCaption;
        public DevExpress.XtraReports.UI.XRLabel xrLabelBirthday;
        public DevExpress.XtraReports.UI.XRLabel xrLabelProfileName;
        public DevExpress.XtraReports.UI.XRLabel xrLabelTime;
        public DevExpress.XtraReports.UI.XRLabel xrLabelStatistic;
        public DevExpress.XtraReports.UI.XRTableCell xrTableMethodTitle;
        public DevExpress.XtraReports.UI.XRTableCell xrTableTestTitle;
        public DevExpress.XtraReports.UI.XRTableCell xrTableScaleName;
        public DevExpress.XtraReports.UI.XRTableCell xrTableMaxEff;
        public DevExpress.XtraReports.UI.XRTableCell xrTableResult;
        public DevExpress.XtraReports.UI.XRTableCell xrTablePercent;
        public DevExpress.XtraReports.UI.XRTableCell xrTableValuation;
        
    }
}

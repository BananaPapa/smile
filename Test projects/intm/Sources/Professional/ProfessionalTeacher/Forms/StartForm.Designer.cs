﻿namespace Eureca.Professional.ProfessionalTeacher
{
    partial class StartForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StartForm));
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barButtonItemTests = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemProfile = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemUsers = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemReport = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemEvents = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemChangeUser = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemExit = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemDB = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemDBCurriculum = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemSerSupervise = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemUpdate = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemInfo = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemJournals = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPageWork = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroupTestts = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup5 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageSettings = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            ((System.ComponentModel.ISupportInitialize)(this.workPanel)).BeginInit();
            this.workPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            this.SuspendLayout();
            // 
            // workPanel
            // 
            this.workPanel.Appearance.ForeColor = System.Drawing.Color.Transparent;
            this.workPanel.Appearance.Options.UseForeColor = true;
            this.workPanel.Controls.Add(this.ribbonControl1);
            this.workPanel.LookAndFeel.SkinName = "Blue";
            this.workPanel.Size = new System.Drawing.Size(679, 119);
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.barButtonItemTests,
            this.barButtonItemProfile,
            this.barButtonItemUsers,
            this.barButtonItemReport,
            this.barButtonItemEvents,
            this.barButtonItemChangeUser,
            this.barButtonItemExit,
            this.barButtonItemDB,
            this.barButtonItemDBCurriculum,
            this.barButtonItemSerSupervise,
            this.barButtonItemUpdate,
            this.barButtonItemInfo,
            this.barButtonItemJournals});
            this.ribbonControl1.Location = new System.Drawing.Point(2, 2);
            this.ribbonControl1.MaxItemId = 25;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPageWork,
            this.ribbonPage1});
            this.ribbonControl1.ShowToolbarCustomizeItem = false;
            this.ribbonControl1.Size = new System.Drawing.Size(675, 115);
            this.ribbonControl1.Toolbar.ShowCustomizeItem = false;
            this.ribbonControl1.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden;
            // 
            // barButtonItemTests
            // 
            this.barButtonItemTests.Caption = "Тематики и тесты";
            this.barButtonItemTests.Id = 1;
            this.barButtonItemTests.LargeGlyph = global::Eureca.Professional.ProfessionalTeacher.Properties.Resources.ChangeTests;
            this.barButtonItemTests.Name = "barButtonItemTests";
            this.barButtonItemTests.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemTests_ItemClick);
            // 
            // barButtonItemProfile
            // 
            this.barButtonItemProfile.Caption = "Профили";
            this.barButtonItemProfile.Id = 3;
            this.barButtonItemProfile.LargeGlyph = global::Eureca.Professional.ProfessionalTeacher.Properties.Resources.Profiles;
            this.barButtonItemProfile.Name = "barButtonItemProfile";
            this.barButtonItemProfile.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemProfile_ItemClick);
            // 
            // barButtonItemUsers
            // 
            this.barButtonItemUsers.Caption = "Пользователи";
            this.barButtonItemUsers.Id = 4;
            this.barButtonItemUsers.LargeGlyph = global::Eureca.Professional.ProfessionalTeacher.Properties.Resources.users;
            this.barButtonItemUsers.Name = "barButtonItemUsers";
            this.barButtonItemUsers.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemUsers_ItemClick);
            // 
            // barButtonItemReport
            // 
            this.barButtonItemReport.Caption = "Отчеты";
            this.barButtonItemReport.Id = 8;
            this.barButtonItemReport.LargeGlyph = global::Eureca.Professional.ProfessionalTeacher.Properties.Resources.Report;
            this.barButtonItemReport.Name = "barButtonItemReport";
            this.barButtonItemReport.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemReport_ItemClick);
            // 
            // barButtonItemEvents
            // 
            this.barButtonItemEvents.Caption = "События";
            this.barButtonItemEvents.Id = 9;
            this.barButtonItemEvents.LargeGlyph = global::Eureca.Professional.ProfessionalTeacher.Properties.Resources.ViewEvents;
            this.barButtonItemEvents.Name = "barButtonItemEvents";
            this.barButtonItemEvents.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemEvents_ItemClick);
            // 
            // barButtonItemChangeUser
            // 
            this.barButtonItemChangeUser.Caption = "Смена пользователя";
            this.barButtonItemChangeUser.Id = 10;
            this.barButtonItemChangeUser.LargeGlyph = global::Eureca.Professional.ProfessionalTeacher.Properties.Resources.ChangeUsers;
            this.barButtonItemChangeUser.Name = "barButtonItemChangeUser";
            this.barButtonItemChangeUser.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemChangeUser_ItemClick);
            // 
            // barButtonItemExit
            // 
            this.barButtonItemExit.Caption = "Завершение работы";
            this.barButtonItemExit.Id = 12;
            this.barButtonItemExit.LargeGlyph = global::Eureca.Professional.ProfessionalTeacher.Properties.Resources.exit;
            this.barButtonItemExit.Name = "barButtonItemExit";
            this.barButtonItemExit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemExit_ItemClick);
            // 
            // barButtonItemDB
            // 
            this.barButtonItemDB.Caption = "Подключение к БД Профессионал";
            this.barButtonItemDB.Id = 15;
            this.barButtonItemDB.LargeGlyph = global::Eureca.Professional.ProfessionalTeacher.Properties.Resources.network_server_database32;
            this.barButtonItemDB.Name = "barButtonItemDB";
            this.barButtonItemDB.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemDB_ItemClick);
            // 
            // barButtonItemDBCurriculum
            // 
            this.barButtonItemDBCurriculum.Caption = "Подключение к БД Планирования";
            this.barButtonItemDBCurriculum.Id = 16;
            this.barButtonItemDBCurriculum.LargeGlyph = global::Eureca.Professional.ProfessionalTeacher.Properties.Resources.network_server_database32;
            this.barButtonItemDBCurriculum.Name = "barButtonItemDBCurriculum";
            this.barButtonItemDBCurriculum.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemDBCurriculum_ItemClick);
            // 
            // barButtonItemSerSupervise
            // 
            this.barButtonItemSerSupervise.Caption = "Контроль";
            this.barButtonItemSerSupervise.Id = 18;
            this.barButtonItemSerSupervise.LargeGlyph = global::Eureca.Professional.ProfessionalTeacher.Properties.Resources.off;
            this.barButtonItemSerSupervise.Name = "barButtonItemSerSupervise";
            this.barButtonItemSerSupervise.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemSerSupervise_ItemClick);
            // 
            // barButtonItemUpdate
            // 
            this.barButtonItemUpdate.Caption = "Обновления";
            this.barButtonItemUpdate.Id = 21;
            this.barButtonItemUpdate.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemUpdate.LargeGlyph")));
            this.barButtonItemUpdate.Name = "barButtonItemUpdate";
            this.barButtonItemUpdate.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemUpdate_ItemClick);
            // 
            // barButtonItemInfo
            // 
            this.barButtonItemInfo.Caption = "Справка";
            this.barButtonItemInfo.Id = 23;
            this.barButtonItemInfo.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemInfo.LargeGlyph")));
            this.barButtonItemInfo.Name = "barButtonItemInfo";
            this.barButtonItemInfo.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemInfo_ItemClick);
            // 
            // barButtonItemJournals
            // 
            this.barButtonItemJournals.Caption = "Журналы";
            this.barButtonItemJournals.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.barButtonItemJournals.Id = 24;
            this.barButtonItemJournals.LargeGlyph = global::Eureca.Professional.ProfessionalTeacher.Properties.Resources.Profile;
            this.barButtonItemJournals.Name = "barButtonItemJournals";
            this.barButtonItemJournals.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemJournals_ItemClick);
            // 
            // ribbonPageWork
            // 
            this.ribbonPageWork.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroupTestts,
            this.ribbonPageGroup3,
            this.ribbonPageGroup2,
            this.ribbonPageGroup5});
            this.ribbonPageWork.Name = "ribbonPageWork";
            this.ribbonPageWork.Text = "Действия пользователя";
            // 
            // ribbonPageGroupTestts
            // 
            this.ribbonPageGroupTestts.ItemLinks.Add(this.barButtonItemTests);
            this.ribbonPageGroupTestts.ItemLinks.Add(this.barButtonItemProfile);
            this.ribbonPageGroupTestts.ItemLinks.Add(this.barButtonItemJournals);
            this.ribbonPageGroupTestts.Name = "ribbonPageGroupTestts";
            this.ribbonPageGroupTestts.ShowCaptionButton = false;
            this.ribbonPageGroupTestts.Text = "Тесты";
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.ItemLinks.Add(this.barButtonItemUsers);
            this.ribbonPageGroup3.ItemLinks.Add(this.barButtonItemReport);
            this.ribbonPageGroup3.ItemLinks.Add(this.barButtonItemEvents);
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            this.ribbonPageGroup3.Text = "Обучение";
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.AllowMinimize = false;
            this.ribbonPageGroup2.AllowTextClipping = false;
            this.ribbonPageGroup2.ItemLinks.Add(this.barButtonItemSerSupervise);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.Text = "Тестирование";
            // 
            // ribbonPageGroup5
            // 
            this.ribbonPageGroup5.ItemLinks.Add(this.barButtonItemChangeUser);
            this.ribbonPageGroup5.ItemLinks.Add(this.barButtonItemExit);
            this.ribbonPageGroup5.Name = "ribbonPageGroup5";
            this.ribbonPageGroup5.ShowCaptionButton = false;
            this.ribbonPageGroup5.Text = "Завершение работы";
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageSettings,
            this.ribbonPageGroup1});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "Настройки";
            // 
            // ribbonPageSettings
            // 
            this.ribbonPageSettings.ItemLinks.Add(this.barButtonItemDB);
            this.ribbonPageSettings.ItemLinks.Add(this.barButtonItemDBCurriculum);
            this.ribbonPageSettings.ItemLinks.Add(this.barButtonItemUpdate);
            this.ribbonPageSettings.Name = "ribbonPageSettings";
            this.ribbonPageSettings.ShowCaptionButton = false;
            this.ribbonPageSettings.Text = "Настройки";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.AllowMinimize = false;
            this.ribbonPageGroup1.AllowTextClipping = false;
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItemInfo);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.Text = "Информация";
            // 
            // StartForm
            // 
            this.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.Appearance.Options.UseForeColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(679, 119);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.LookAndFeel.SkinName = "Blue";
            this.MaximizeBox = false;
            this.Name = "StartForm";
            this.Text = "СПО «Преподаватель»";
            ((System.ComponentModel.ISupportInitialize)(this.workPanel)).EndInit();
            this.workPanel.ResumeLayout(false);
            this.workPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPageWork;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupTestts;
        private DevExpress.XtraBars.BarButtonItem barButtonItemTests;
        private DevExpress.XtraBars.BarButtonItem barButtonItemProfile;
        private DevExpress.XtraBars.BarButtonItem barButtonItemUsers;
        private DevExpress.XtraBars.BarButtonItem barButtonItemReport;
        private DevExpress.XtraBars.BarButtonItem barButtonItemEvents;
        private DevExpress.XtraBars.BarButtonItem barButtonItemChangeUser;
        private DevExpress.XtraBars.BarButtonItem barButtonItemExit;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageSettings;
        private DevExpress.XtraBars.BarButtonItem barButtonItemDB;
        private DevExpress.XtraBars.BarButtonItem barButtonItemDBCurriculum;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup5;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraBars.BarButtonItem barButtonItemSerSupervise;
        private DevExpress.XtraBars.BarButtonItem barButtonItemUpdate;
        private DevExpress.XtraBars.BarButtonItem barButtonItemInfo;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.BarButtonItem barButtonItemJournals;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
    }
}
﻿using System.Diagnostics;
using Castle.ActiveRecord;
using DevExpress.XtraBars;
using Eureca.Professional.ActiveRecords;
using Eureca.Professional.BaseComponents;
using Eureca.Professional.TestingPlugInStandard;
using Eureca.Professional.TestingPlugInStandard.Controls.CreateTest;
using Eureca.Updater.UpdaterClient;

namespace Eureca.Professional.ProfessionalTeacher
{
    public partial class StartForm : ProfessionalStartForm
    {
        private bool _enableSupervise;
        public StartForm()
        {
            InitializeComponent();
           // GetSuperviseStatus();
        }

        private void barButtonItemDB_ItemClick(object sender, ItemClickEventArgs e)
        {
            ConnectDbClick();
        }

        private void barButtonItemChangeUser_ItemClick(object sender, ItemClickEventArgs e)
        {
            ChangeUserClick();
        }

        private void barButtonItemSettings_ItemClick(object sender, ItemClickEventArgs e)
        {
            ShowSettingsClick();
        }

        private void barButtonItemExit_ItemClick(object sender, ItemClickEventArgs e)
        {
            Close();
        }

        private void barButtonItemProfile_ItemClick(object sender, ItemClickEventArgs e)
        {
            using (new SessionScope())
            {
                var uc = new ViewProfiles();
                uc.InitControl();
                uc.ShowInForm();
            }
            TestDBExecutor.ClearDataBase( );
        }

        private void barButtonItemUsers_ItemClick(object sender, ItemClickEventArgs e)
        {
            var uc = new ViewUsers(RunnerApplication.CurrentUser);
            uc.InitControl();
            uc.ShowInForm();
            RunnerApplication.CurrentUser.Refresh();
        }

        private void barButtonItemEvents_ItemClick(object sender, ItemClickEventArgs e)
        {
            var uc = new ViewEvents();
            uc.InitControl();
            uc.ShowInForm();
        }

        private void barButtonItemReport_ItemClick(object sender, ItemClickEventArgs e)
        {
            using (new SessionScope())
            {
                var uc = new ReportManager();
                uc.InitControl();
                uc.ShowInForm();
            }
            TestDBExecutor.ClearDataBase( );
        }

        private void barButtonItemTests_ItemClick(object sender, ItemClickEventArgs e)
        {
            RunCreateTest();
        }

        private static void RunCreateTest()
        {
            //using ( new SessionScope())
            //{
                var lt = new ListTests(RunnerApplication.CurrentUser);
                lt.InitControl();
                lt.ShowInForm();
                //Trace.WriteLine("in scope");
                //foreach (var test in Test.FindAll( ))
                //{
                //    Trace.WriteLine( test.Title );
                //}
            //}
            //Trace.WriteLine( "after scope" );
            //foreach (var test in Test.FindAll())
            //{
            //   Trace.WriteLine(test.Title);
            //}
            TestDBExecutor.ClearDataBase( );
            //Trace.WriteLine( "after clear" );
            //foreach (var test in Test.FindAll( ))
            //{
            //    Trace.WriteLine( test.Title );
            //}
            //Trace.WriteLine( "\n\n\n" );
        }

        private void barButtonItemDBCurriculum_ItemClick(object sender, ItemClickEventArgs e)
        {
            ConnectCurriculumDb();
        }

        private void barButtonItemSerSupervise_ItemClick(object sender, ItemClickEventArgs e)
        {
            var uc = new ViewSuperviseAdresses();
            uc.InitControl();
            uc.ShowInForm(true);
        }

        private void barButtonItemUpdate_ItemClick( object sender, ItemClickEventArgs e )
        {
            ChangeUpdateSettings( (IUpdateSettingStorage)RunnerApplication.UpdateSettingsContext );
        }

        private void barButtonItemInfo_ItemClick( object sender, ItemClickEventArgs e )
        {
            RunnerApplication.ShowInfoPage( );
        }

        private void barButtonItemJournals_ItemClick( object sender, ItemClickEventArgs e )
        {
            RunnerApplication.JournalManager.EditJournals();
        }

    }
}
﻿namespace Eureca.Professional.ProfessionalTeacher
{
    partial class ReportManager
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if (disposing && ( components != null ))
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            this.comboBoxEdit1 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.gridControlProfiles = new DevExpress.XtraGrid.GridControl();
            this.gridViewProfiles = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTimeEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.simpleButtonCreateProfileReport = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.gridControlTests = new DevExpress.XtraGrid.GridControl();
            this.gridViewTests = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTimeEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.comboBoxEditReports = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButtonCreateReport = new DevExpress.XtraEditors.SimpleButton();
            this.panelControlReportProcess = new DevExpress.XtraEditors.PanelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.marqueeProgressBarControl1 = new DevExpress.XtraEditors.MarqueeProgressBarControl();
            this.barManager1 = new DevExpress.XtraBars.BarManager();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemStatus = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonDeleteProfile = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonDeleteAll = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlProfiles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewProfiles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlTests)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTests)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditReports.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlReportProcess)).BeginInit();
            this.panelControlReportProcess.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.marqueeProgressBarControl1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBoxEdit1
            // 
            this.comboBoxEdit1.Location = new System.Drawing.Point(15, 24);
            this.comboBoxEdit1.Name = "comboBoxEdit1";
            this.comboBoxEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit1.Size = new System.Drawing.Size(510, 20);
            this.comboBoxEdit1.TabIndex = 0;
            this.comboBoxEdit1.SelectedIndexChanged += new System.EventHandler(this.ComboBoxEdit1SelectedIndexChanged);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(15, 5);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(182, 13);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "Выберите обучаемого специалиста ";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(15, 50);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(149, 13);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "Выполненные наборы тестов";
            // 
            // gridControlProfiles
            // 
            this.gridControlProfiles.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControlProfiles.Location = new System.Drawing.Point(15, 69);
            this.gridControlProfiles.MainView = this.gridViewProfiles;
            this.gridControlProfiles.Name = "gridControlProfiles";
            this.gridControlProfiles.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTimeEdit1});
            this.gridControlProfiles.Size = new System.Drawing.Size(785, 205);
            this.gridControlProfiles.TabIndex = 3;
            this.gridControlProfiles.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewProfiles,
            this.gridView2});
            // 
            // gridViewProfiles
            // 
            this.gridViewProfiles.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn7,
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn16,
            this.gridColumn3});
            this.gridViewProfiles.GridControl = this.gridControlProfiles;
            this.gridViewProfiles.Name = "gridViewProfiles";
            this.gridViewProfiles.OptionsBehavior.Editable = false;
            this.gridViewProfiles.OptionsView.ShowAutoFilterRow = true;
            this.gridViewProfiles.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.GridViewProfilesFocusedRowChanged);
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Profileid";
            this.gridColumn7.FieldName = "Profileid";
            this.gridColumn7.Name = "gridColumn7";
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "ProfilePassingid";
            this.gridColumn1.FieldName = "ProfilePassingid";
            this.gridColumn1.Name = "gridColumn1";
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Наименование";
            this.gridColumn2.FieldName = "Title";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Дата прохождения";
            this.gridColumn4.FieldName = "StartTime";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 1;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Время начала";
            this.gridColumn5.ColumnEdit = this.repositoryItemTimeEdit1;
            this.gridColumn5.FieldName = "StartTime";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 2;
            // 
            // repositoryItemTimeEdit1
            // 
            this.repositoryItemTimeEdit1.AutoHeight = false;
            this.repositoryItemTimeEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemTimeEdit1.Name = "repositoryItemTimeEdit1";
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Время окончания";
            this.gridColumn6.ColumnEdit = this.repositoryItemTimeEdit1;
            this.gridColumn6.FieldName = "FinishTime";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 3;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "Длительность(сек)";
            this.gridColumn16.FieldName = "TimeDuration";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 4;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Описание";
            this.gridColumn3.FieldName = "Description";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 5;
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.gridControlProfiles;
            this.gridView2.Name = "gridView2";
            // 
            // simpleButtonCreateProfileReport
            // 
            this.simpleButtonCreateProfileReport.Location = new System.Drawing.Point(15, 280);
            this.simpleButtonCreateProfileReport.Name = "simpleButtonCreateProfileReport";
            this.simpleButtonCreateProfileReport.Size = new System.Drawing.Size(156, 23);
            this.simpleButtonCreateProfileReport.TabIndex = 4;
            this.simpleButtonCreateProfileReport.Text = "Общий отчет по профилю";
            this.simpleButtonCreateProfileReport.Click += new System.EventHandler(this.SimpleButtonCreateProfileReportClick);
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(15, 309);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(104, 13);
            this.labelControl3.TabIndex = 6;
            this.labelControl3.Text = "Выполненные тесты";
            // 
            // gridControlTests
            // 
            this.gridControlTests.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControlTests.Location = new System.Drawing.Point(15, 328);
            this.gridControlTests.MainView = this.gridViewTests;
            this.gridControlTests.Name = "gridControlTests";
            this.gridControlTests.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTimeEdit3});
            this.gridControlTests.Size = new System.Drawing.Size(785, 208);
            this.gridControlTests.TabIndex = 7;
            this.gridControlTests.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewTests});
            // 
            // gridViewTests
            // 
            this.gridViewTests.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn15,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn13,
            this.gridColumn14});
            this.gridViewTests.GridControl = this.gridControlTests;
            this.gridViewTests.Name = "gridViewTests";
            this.gridViewTests.OptionsBehavior.Editable = false;
            this.gridViewTests.OptionsView.ShowAutoFilterRow = true;
            this.gridViewTests.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.GridViewTestsFocusedRowChanged);
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Дата начала";
            this.gridColumn8.FieldName = "sTime";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 1;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Время начала";
            this.gridColumn9.ColumnEdit = this.repositoryItemTimeEdit3;
            this.gridColumn9.FieldName = "sTime";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 2;
            // 
            // repositoryItemTimeEdit3
            // 
            this.repositoryItemTimeEdit3.AutoHeight = false;
            this.repositoryItemTimeEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemTimeEdit3.Name = "repositoryItemTimeEdit3";
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Время окончания";
            this.gridColumn10.ColumnEdit = this.repositoryItemTimeEdit3;
            this.gridColumn10.FieldName = "eTime";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 3;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Длительность(сек)";
            this.gridColumn15.FieldName = "TimeDuration";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 4;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Наименование теста";
            this.gridColumn11.FieldName = "Title";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 0;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Результат(%)";
            this.gridColumn12.FieldName = "Perc";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 5;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Оценка";
            this.gridColumn13.FieldName = "Valuation";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 6;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "testid";
            this.gridColumn14.FieldName = "testid";
            this.gridColumn14.Name = "gridColumn14";
            // 
            // comboBoxEditReports
            // 
            this.comboBoxEditReports.Location = new System.Drawing.Point(15, 560);
            this.comboBoxEditReports.Name = "comboBoxEditReports";
            this.comboBoxEditReports.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditReports.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEditReports.Size = new System.Drawing.Size(227, 20);
            this.comboBoxEditReports.TabIndex = 8;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(15, 541);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(76, 13);
            this.labelControl4.TabIndex = 9;
            this.labelControl4.Text = "Виды отчетов:";
            // 
            // simpleButtonCreateReport
            // 
            this.simpleButtonCreateReport.Location = new System.Drawing.Point(262, 557);
            this.simpleButtonCreateReport.Name = "simpleButtonCreateReport";
            this.simpleButtonCreateReport.Size = new System.Drawing.Size(95, 23);
            this.simpleButtonCreateReport.TabIndex = 10;
            this.simpleButtonCreateReport.Text = "Создать отчет";
            this.simpleButtonCreateReport.Click += new System.EventHandler(this.SimpleButtonCreateReportClick);
            // 
            // panelControlReportProcess
            // 
            this.panelControlReportProcess.Controls.Add(this.labelControl5);
            this.panelControlReportProcess.Controls.Add(this.marqueeProgressBarControl1);
            this.panelControlReportProcess.Location = new System.Drawing.Point(507, 541);
            this.panelControlReportProcess.Name = "panelControlReportProcess";
            this.panelControlReportProcess.Size = new System.Drawing.Size(284, 48);
            this.panelControlReportProcess.TabIndex = 12;
            this.panelControlReportProcess.Visible = false;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(5, 5);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(158, 13);
            this.labelControl5.TabIndex = 11;
            this.labelControl5.Text = "Идет процесс создания отчета";
            // 
            // marqueeProgressBarControl1
            // 
            this.marqueeProgressBarControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.marqueeProgressBarControl1.EditValue = 0;
            this.marqueeProgressBarControl1.Location = new System.Drawing.Point(5, 25);
            this.marqueeProgressBarControl1.Name = "marqueeProgressBarControl1";
            this.marqueeProgressBarControl1.Size = new System.Drawing.Size(274, 18);
            this.marqueeProgressBarControl1.TabIndex = 10;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar3});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barStaticItemStatus});
            this.barManager1.MaxItemId = 1;
            this.barManager1.StatusBar = this.bar3;
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemStatus)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemStatus
            // 
            this.barStaticItemStatus.Id = 0;
            this.barStaticItemStatus.Name = "barStaticItemStatus";
            this.barStaticItemStatus.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(803, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 585);
            this.barDockControlBottom.Size = new System.Drawing.Size(803, 28);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 585);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(803, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 585);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(363, 557);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(125, 23);
            this.simpleButton1.TabIndex = 13;
            this.simpleButton1.Text = "Редактировать отчет";
            this.simpleButton1.Visible = false;
            // 
            // simpleButtonDeleteProfile
            // 
            this.simpleButtonDeleteProfile.Location = new System.Drawing.Point(177, 280);
            this.simpleButtonDeleteProfile.Name = "simpleButtonDeleteProfile";
            this.simpleButtonDeleteProfile.Size = new System.Drawing.Size(156, 23);
            this.simpleButtonDeleteProfile.TabIndex = 14;
            this.simpleButtonDeleteProfile.Text = "Удалить";
            this.simpleButtonDeleteProfile.Click += new System.EventHandler(this.SimpleButtonDeleteProfileClick);
            // 
            // simpleButtonDeleteAll
            // 
            this.simpleButtonDeleteAll.Location = new System.Drawing.Point(339, 280);
            this.simpleButtonDeleteAll.Name = "simpleButtonDeleteAll";
            this.simpleButtonDeleteAll.Size = new System.Drawing.Size(156, 23);
            this.simpleButtonDeleteAll.TabIndex = 15;
            this.simpleButtonDeleteAll.Text = "Удалить все";
            this.simpleButtonDeleteAll.Click += new System.EventHandler(this.SimpleButtonDeleteAllClick);
            // 
            // ReportManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.simpleButtonDeleteAll);
            this.Controls.Add(this.simpleButtonDeleteProfile);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.panelControlReportProcess);
            this.Controls.Add(this.simpleButtonCreateReport);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.comboBoxEditReports);
            this.Controls.Add(this.gridControlTests);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.simpleButtonCreateProfileReport);
            this.Controls.Add(this.gridControlProfiles);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.comboBoxEdit1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.MinimumSize = new System.Drawing.Size(492, 608);
            this.Name = "ReportManager";
            this.Size = new System.Drawing.Size(803, 613);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlProfiles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewProfiles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlTests)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTests)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditReports.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlReportProcess)).EndInit();
            this.panelControlReportProcess.ResumeLayout(false);
            this.panelControlReportProcess.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.marqueeProgressBarControl1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraGrid.GridControl gridControlProfiles;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewProfiles;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit repositoryItemTimeEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCreateProfileReport;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraGrid.GridControl gridControlTests;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewTests;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit repositoryItemTimeEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditReports;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCreateReport;
        private DevExpress.XtraEditors.PanelControl panelControlReportProcess;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.MarqueeProgressBarControl marqueeProgressBarControl1;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemStatus;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton simpleButtonDeleteAll;
        private DevExpress.XtraEditors.SimpleButton simpleButtonDeleteProfile;
    }
}

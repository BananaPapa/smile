﻿namespace Eureca.Professional.ProfessionalTeacher
{
    partial class ViewSuperviseAdresses
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components  = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.simpleButtonClose = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonPrint = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this._manualSettingTagControl = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this._probationerControlGrid = new DevExpress.XtraGrid.GridControl();
            this.gridViewSimple = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._adressColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._isSupervise = new DevExpress.XtraGrid.Columns.GridColumn();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this._disableControlSBtn = new DevExpress.XtraEditors.SimpleButton();
            this._enableControlSBtn = new DevExpress.XtraEditors.SimpleButton();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this._winUsersControlGrid = new DevExpress.XtraGrid.GridControl();
            this._winUsersView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._winUserControl = new DevExpress.XtraGrid.Columns.GridColumn();
            this._superviseEnabled = new DevExpress.XtraGrid.Columns.GridColumn();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this._disableWinUserControl = new DevExpress.XtraEditors.SimpleButton();
            this._enableWinUserControl = new DevExpress.XtraEditors.SimpleButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._manualControlSettingRadioButton = new System.Windows.Forms.RadioButton();
            this._globalControlCheckRadioButton = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this._manualSettingTagControl.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._probationerControlGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSimple)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._winUsersControlGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._winUsersView)).BeginInit();
            this.flowLayoutPanel2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // simpleButtonClose
            // 
            this.simpleButtonClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonClose.Location = new System.Drawing.Point(924, 715);
            this.simpleButtonClose.Name = "simpleButtonClose";
            this.simpleButtonClose.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonClose.TabIndex = 4;
            this.simpleButtonClose.Text = "Закрыть";
            this.simpleButtonClose.Click += new System.EventHandler(this.simpleButtonClose_Click);
            // 
            // simpleButtonPrint
            // 
            this.simpleButtonPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonPrint.Location = new System.Drawing.Point(843, 715);
            this.simpleButtonPrint.Name = "simpleButtonPrint";
            this.simpleButtonPrint.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonPrint.TabIndex = 5;
            this.simpleButtonPrint.Text = "Печать";
            this.simpleButtonPrint.Click += new System.EventHandler(this.simpleButtonPrint_Click);
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this._manualSettingTagControl);
            this.groupControl1.Controls.Add(this.groupBox1);
            this.groupControl1.Location = new System.Drawing.Point(14, 14);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(985, 695);
            this.groupControl1.TabIndex = 8;
            this.groupControl1.Text = "Контроль";
            // 
            // _manualSettingTagControl
            // 
            this._manualSettingTagControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._manualSettingTagControl.Controls.Add(this.tabPage1);
            this._manualSettingTagControl.Controls.Add(this.tabPage2);
            this._manualSettingTagControl.Location = new System.Drawing.Point(18, 114);
            this._manualSettingTagControl.Name = "_manualSettingTagControl";
            this._manualSettingTagControl.SelectedIndex = 0;
            this._manualSettingTagControl.Size = new System.Drawing.Size(949, 566);
            this._manualSettingTagControl.TabIndex = 14;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this._probationerControlGrid);
            this.tabPage1.Controls.Add(this.flowLayoutPanel1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(941, 540);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Сетевой контроль";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // _probationerControlGrid
            // 
            this._probationerControlGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._probationerControlGrid.Location = new System.Drawing.Point(3, 3);
            this._probationerControlGrid.MainView = this.gridViewSimple;
            this._probationerControlGrid.Name = "_probationerControlGrid";
            this._probationerControlGrid.Size = new System.Drawing.Size(935, 501);
            this._probationerControlGrid.TabIndex = 9;
            this._probationerControlGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewSimple});
            // 
            // gridViewSimple
            // 
            this.gridViewSimple.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._adressColumn,
            this._isSupervise});
            this.gridViewSimple.GridControl = this._probationerControlGrid;
            this.gridViewSimple.Name = "gridViewSimple";
            this.gridViewSimple.OptionsBehavior.Editable = false;
            this.gridViewSimple.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewSimple.OptionsSelection.MultiSelect = true;
            this.gridViewSimple.OptionsView.ShowAutoFilterRow = true;
            // 
            // _adressColumn
            // 
            this._adressColumn.Caption = "Сетевой адрес";
            this._adressColumn.FieldName = "Adress";
            this._adressColumn.Name = "_adressColumn";
            this._adressColumn.Visible = true;
            this._adressColumn.VisibleIndex = 0;
            // 
            // _isSupervise
            // 
            this._isSupervise.Caption = "Контроль";
            this._isSupervise.FieldName = "IsSupervise";
            this._isSupervise.Name = "_isSupervise";
            this._isSupervise.Visible = true;
            this._isSupervise.VisibleIndex = 1;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.Controls.Add(this._disableControlSBtn);
            this.flowLayoutPanel1.Controls.Add(this._enableControlSBtn);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 504);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(935, 33);
            this.flowLayoutPanel1.TabIndex = 12;
            // 
            // _disableControlSBtn
            // 
            this._disableControlSBtn.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this._disableControlSBtn.Location = new System.Drawing.Point(3, 3);
            this._disableControlSBtn.Name = "_disableControlSBtn";
            this._disableControlSBtn.Size = new System.Drawing.Size(75, 27);
            this._disableControlSBtn.TabIndex = 10;
            this._disableControlSBtn.Text = "Отключить";
            this._disableControlSBtn.Click += new System.EventHandler(this.simpleButtonDisable_Click);
            // 
            // _enableControlSBtn
            // 
            this._enableControlSBtn.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this._enableControlSBtn.Location = new System.Drawing.Point(84, 3);
            this._enableControlSBtn.Name = "_enableControlSBtn";
            this._enableControlSBtn.Size = new System.Drawing.Size(75, 27);
            this._enableControlSBtn.TabIndex = 8;
            this._enableControlSBtn.Text = "Включить";
            this._enableControlSBtn.Click += new System.EventHandler(this.simpleButtonEnable_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this._winUsersControlGrid);
            this.tabPage2.Controls.Add(this.flowLayoutPanel2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(941, 540);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Пользовательский контроль";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // _winUsersControlGrid
            // 
            this._winUsersControlGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._winUsersControlGrid.Location = new System.Drawing.Point(3, 3);
            this._winUsersControlGrid.MainView = this._winUsersView;
            this._winUsersControlGrid.Name = "_winUsersControlGrid";
            this._winUsersControlGrid.Size = new System.Drawing.Size(935, 501);
            this._winUsersControlGrid.TabIndex = 0;
            this._winUsersControlGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this._winUsersView});
            // 
            // _winUsersView
            // 
            this._winUsersView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._winUserControl,
            this._superviseEnabled});
            this._winUsersView.GridControl = this._winUsersControlGrid;
            this._winUsersView.Name = "_winUsersView";
            this._winUsersView.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this._winUserControl, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // _winUserControl
            // 
            this._winUserControl.Caption = "Пользователь";
            this._winUserControl.FieldName = "Item";
            this._winUserControl.Name = "_winUserControl";
            this._winUserControl.OptionsColumn.AllowEdit = false;
            this._winUserControl.OptionsColumn.ReadOnly = true;
            this._winUserControl.Visible = true;
            this._winUserControl.VisibleIndex = 0;
            // 
            // _superviseEnabled
            // 
            this._superviseEnabled.Caption = "Контроль";
            this._superviseEnabled.FieldName = "IsSelect";
            this._superviseEnabled.Name = "_superviseEnabled";
            this._superviseEnabled.OptionsColumn.AllowEdit = false;
            this._superviseEnabled.OptionsColumn.ReadOnly = true;
            this._superviseEnabled.Visible = true;
            this._superviseEnabled.VisibleIndex = 1;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.AutoSize = true;
            this.flowLayoutPanel2.Controls.Add(this._disableWinUserControl);
            this.flowLayoutPanel2.Controls.Add(this._enableWinUserControl);
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(3, 504);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(935, 33);
            this.flowLayoutPanel2.TabIndex = 13;
            // 
            // _disableWinUserControl
            // 
            this._disableWinUserControl.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this._disableWinUserControl.Location = new System.Drawing.Point(3, 3);
            this._disableWinUserControl.Name = "_disableWinUserControl";
            this._disableWinUserControl.Size = new System.Drawing.Size(75, 27);
            this._disableWinUserControl.TabIndex = 10;
            this._disableWinUserControl.Text = "Отключить";
            this._disableWinUserControl.Click += new System.EventHandler(this._disableWinUserControl_Click);
            // 
            // _enableWinUserControl
            // 
            this._enableWinUserControl.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this._enableWinUserControl.Location = new System.Drawing.Point(84, 3);
            this._enableWinUserControl.Name = "_enableWinUserControl";
            this._enableWinUserControl.Size = new System.Drawing.Size(75, 27);
            this._enableWinUserControl.TabIndex = 8;
            this._enableWinUserControl.Text = "Включить";
            this._enableWinUserControl.Click += new System.EventHandler(this._enableWinUserControl_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._manualControlSettingRadioButton);
            this.groupBox1.Controls.Add(this._globalControlCheckRadioButton);
            this.groupBox1.Location = new System.Drawing.Point(18, 24);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(949, 74);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Глобальные настройки";
            // 
            // _manualControlSettingRadioButton
            // 
            this._manualControlSettingRadioButton.AutoSize = true;
            this._manualControlSettingRadioButton.Location = new System.Drawing.Point(7, 44);
            this._manualControlSettingRadioButton.Name = "_manualControlSettingRadioButton";
            this._manualControlSettingRadioButton.Size = new System.Drawing.Size(137, 17);
            this._manualControlSettingRadioButton.TabIndex = 1;
            this._manualControlSettingRadioButton.TabStop = true;
            this._manualControlSettingRadioButton.Text = "Ручная конфигурация";
            this._manualControlSettingRadioButton.UseVisualStyleBackColor = true;
            // 
            // _globalControlCheckRadioButton
            // 
            this._globalControlCheckRadioButton.AutoSize = true;
            this._globalControlCheckRadioButton.Location = new System.Drawing.Point(7, 21);
            this._globalControlCheckRadioButton.Name = "_globalControlCheckRadioButton";
            this._globalControlCheckRadioButton.Size = new System.Drawing.Size(138, 17);
            this._globalControlCheckRadioButton.TabIndex = 0;
            this._globalControlCheckRadioButton.TabStop = true;
            this._globalControlCheckRadioButton.Text = "Глобальный контроль";
            this._globalControlCheckRadioButton.UseVisualStyleBackColor = true;
            this._globalControlCheckRadioButton.CheckedChanged += new System.EventHandler(this.globalControlCheckBtn_CheckedChanged);
            // 
            // ViewSuperviseAdresses
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.simpleButtonPrint);
            this.Controls.Add(this.simpleButtonClose);
            this.MinimumSize = new System.Drawing.Size(571, 303);
            this.Name = "ViewSuperviseAdresses";
            this.Size = new System.Drawing.Size(1014, 746);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this._manualSettingTagControl.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._probationerControlGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSimple)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._winUsersControlGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._winUsersView)).EndInit();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton simpleButtonClose;
        private DevExpress.XtraEditors.SimpleButton simpleButtonPrint;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraGrid.GridControl _probationerControlGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewSimple;
        private DevExpress.XtraGrid.Columns.GridColumn _adressColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _isSupervise;
        private DevExpress.XtraEditors.SimpleButton _disableControlSBtn;
        private DevExpress.XtraEditors.SimpleButton _enableControlSBtn;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton _manualControlSettingRadioButton;
        private System.Windows.Forms.RadioButton _globalControlCheckRadioButton;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.TabControl _manualSettingTagControl;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private DevExpress.XtraGrid.GridControl _winUsersControlGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView _winUsersView;
        private DevExpress.XtraGrid.Columns.GridColumn _winUserControl;
        private DevExpress.XtraGrid.Columns.GridColumn _superviseEnabled;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private DevExpress.XtraEditors.SimpleButton _disableWinUserControl;
        private DevExpress.XtraEditors.SimpleButton _enableWinUserControl;
    }
}

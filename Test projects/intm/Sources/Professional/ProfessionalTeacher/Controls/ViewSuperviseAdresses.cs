﻿using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Castle.ActiveRecord;
using DevExpress.XtraEditors;
using Eureca.Integrator.Extensions;
using Eureca.Professional.CommonClasses;
using Eureca.Professional.CommonClasses.Data;
using Eureca.Utility;
using NHibernate.Criterion;
using Eureca.Professional.ActiveRecords;
using System.Management;
using NHibernate.Dialect.Function;
using System.Collections;


namespace Eureca.Professional.ProfessionalTeacher
{
    /// <summary>
    /// контрол для просмотра списка профилей
    /// </summary>
    public partial class ViewSuperviseAdresses : BaseTestingControl,IDisposable
    {
        #region поля

        private AutoResetEvent _waitLoadHandle = new AutoResetEvent(false);

        private readonly List<SuperviseAdresses> _users = new List<SuperviseAdresses>();

        private List<string> _winUsers = new List<string>();

        #endregion

        #region конструктор

        public ViewSuperviseAdresses()
        {
            Task.Factory.StartNew( ( ) => Eureca.Integrator.Utility.Forms.MarqueeProgressForm.Show( "Подготовка формы редактирования.",
               ( form ) => { _waitLoadHandle.WaitOne( ); }, this.ParentForm ) );

            SelectQuery query = new SelectQuery( "Win32_UserAccount" );
            ManagementObjectSearcher searcher = new ManagementObjectSearcher( query );
            _winUsers = searcher.Get( ).Cast<ManagementObject>( ).ToList( ).Select( o => o["Name"].ToString( ) ).Distinct().ToList( );

            //_winUsers.Add( "я" );
            //_winUsers.Add( "забыл" );
            //_winUsers.Add( "включить фактический поиск." );
            //_winUsers.Add( "просто 1000 пользователей он грузит с минуту где-то" );
            InitializeComponent();

            _waitLoadHandle.Set();
            _waitLoadHandle.Dispose();
        }

        #endregion

        private bool IsGlobalControl 
        {
            get
            {
                var setting = SystemProperties.GetItem( SysPropEnum.IsGlobalControl );
                if (setting != null)
                    return int.Parse( setting.Value ) == 1;
                return false;
            }
            set
            {
                var setting = SystemProperties.GetItem( SysPropEnum.IsGlobalControl );
                var newValue = value ? "1" : "0";
                
                if (setting != null)
                {
                    setting.Value = newValue;
                    setting.UpdateAndFlush();
                }
                else
                {
                    var newSetting = new SystemProperties(SysPropEnum.IsGlobalControl, newValue);
                    newSetting.CreateAndFlush();
                }
            }
        }

        #region методы

        public override void InitControl()
        {
            base.InitControl();
            CaptionControl = "Просмотр контроля тестирований";
            InsertData();
        }

        /// <summary>
        /// метод вставки данных из БД
        /// </summary>
        private void InsertData()
        {
            _users.Clear();
            _users.AddRange(ActiveRecordBase<SuperviseAdresses>.FindAll());
            _globalControlCheckRadioButton.Checked = IsGlobalControl;
            _manualControlSettingRadioButton.Checked = !IsGlobalControl;
            RefreshData();
            RefreshWinUsersData(1);
        }

        /// <summary>
        /// метод обновления тестов
        /// </summary>
        private void RefreshWinUsersData(int rowHandle)
        {
            _winUsersControlGrid.DataSource = GetWindowsUserAccauntsNames();
            _winUsersView.SelectRow(rowHandle);
            _winUsersView.FocusedRowHandle = rowHandle;

        }

        private void RefreshData(  )
        {
            _probationerControlGrid.DataSource = _users;
            gridViewSimple.BestFitColumns( );
        }

        /// <summary>
        /// метод изменения профиля
        /// </summary>
        private void ChangeSuperviseAdress(bool enable)
        {
            var selected = gridViewSimple.GetSelectedRows();
            if (selected.Length > 0)
            {
                foreach (var itemid in gridViewSimple.GetSelectedRows())
                {
                    var item = gridViewSimple.GetRow(itemid) as SuperviseAdresses;

                    if (item != null)
                    {
                        item.IsSupervise = enable;
                        item.SaveAndFlush();
                        RefreshData();
                    }
                }
            }
            else
            {
                MessageForm.ShowErrorMsg("Необходимо выбрать адрес из таблицы.");
                return;
            }
        }

        #endregion

     
        #region обработчики событий стандартных компонент

        private void simpleButtonClose_Click(object sender, EventArgs e)
        {
            if (ParentForm != null)
                ParentForm.Close();
        }

        private void simpleButtonEnable_Click(object sender, EventArgs e)
        {
            ChangeSuperviseAdress(true);
        }

        private void simpleButtonDisable_Click(object sender, EventArgs e)
        {
            ChangeSuperviseAdress(false);
        }
      
        private void simpleButtonPrint_Click(object sender, EventArgs e)
        {
            _probationerControlGrid.ShowPrintPreview();
        }

        private void globalControlCheckBtn_CheckedChanged( object sender, EventArgs e )
        {
            var checkButtonSender = ( sender as RadioButton );
            _enableControlSBtn.Enabled =
                _disableControlSBtn.Enabled = _manualSettingTagControl.Enabled = !checkButtonSender.Checked;
            IsGlobalControl = checkButtonSender.Checked;
        }

        private IEnumerable<SelectableItem<string>> GetWindowsUserAccauntsNames( )
        {
            List<SelectableItem<string>> res = new List<SelectableItem<string>>( );
            var setting = SystemProperties.GetItem( SysPropEnum.WinUsersSupervisAccounts );
            var supervisedUsers = setting == null? new string[0] : setting.Value.Split(new[]{'|'},StringSplitOptions.RemoveEmptyEntries);
            
            foreach (var user in _winUsers) {  res.Add ( new SelectableItem<string>(user, supervisedUsers.Contains( user )) ); }
            return res;
        }


        private void _enableWinUserControl_Click( object sender, EventArgs e )
        {
            SelectableItem<string> selectedItem = _winUsersView.GetSelectedItem( ) as SelectableItem<string>;
            if(selectedItem == null || selectedItem.IsSelect)
                return;
            var rowHandle = _winUsersView.GetSelectedRows( )[0];
            selectedItem.IsSelect = true;
            UpdateSupervisedList();
            RefreshWinUsersData( rowHandle );
        }

        private void _disableWinUserControl_Click(object sender, EventArgs e)
        {
            SelectableItem<string> selectedItem = _winUsersView.GetSelectedItem() as SelectableItem<string>;
            if (selectedItem == null || !selectedItem.IsSelect)
                return;
            var selectedIndexes = _winUsersView.GetSelectedRows( )[0];
            selectedItem.IsSelect = false;
            UpdateSupervisedList( );
            RefreshWinUsersData( selectedIndexes );
        }

        #endregion

        private void UpdateSupervisedList( )
        {
            List<string> supervisedUsers = new List<string>();
            for (int i = 0; i < _winUsersView.RowCount; i++)
            {
                int rowHandle = _winUsersView.GetVisibleRowHandle( i );
                var selectableItem = _winUsersView.GetRow( rowHandle ) as SelectableItem<string>;   
                if(selectableItem.IsSelect)
                    supervisedUsers.Add( selectableItem.Item );
            }

            var setting = SystemProperties.GetItem( SysPropEnum.WinUsersSupervisAccounts );
            var newValue = string.Join("|",supervisedUsers);

            if (setting != null)
            {
                setting.Value = newValue;
                setting.UpdateAndFlush( );
            }
            else
            {
                var newSetting = new SystemProperties( SysPropEnum.WinUsersSupervisAccounts, newValue );
                newSetting.CreateAndFlush( );
            }
        }

        void IDisposable.Dispose( )
        {
            if(_waitLoadHandle != null)
                _waitLoadHandle.Dispose();
        }

    }
}
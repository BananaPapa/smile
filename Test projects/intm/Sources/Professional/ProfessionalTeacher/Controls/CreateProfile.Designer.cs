﻿namespace Eureca.Professional.ProfessionalTeacher
{
    partial class CreateProfile
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.textEditTitle = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.memoEditDescription = new DevExpress.XtraEditors.MemoEdit();
            this.listBoxControlAllTests = new DevExpress.XtraEditors.ListBoxControl();
            this.listBoxControlSelectedTest = new DevExpress.XtraEditors.ListBoxControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButtonOk = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonToAllTests = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonToSelectedTestsAll = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonToAllTestsAll = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonToSelectedTests = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTitle.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxControlAllTests)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxControlSelectedTest)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(23, 15);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(73, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Наименование";
            // 
            // textEditTitle
            // 
            this.textEditTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textEditTitle.Location = new System.Drawing.Point(121, 12);
            this.textEditTitle.Name = "textEditTitle";
            this.textEditTitle.Properties.MaxLength = 255;
            this.textEditTitle.Size = new System.Drawing.Size(441, 20);
            this.textEditTitle.TabIndex = 0;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(23, 41);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(49, 13);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "Описание";
            // 
            // memoEditDescription
            // 
            this.memoEditDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.memoEditDescription.Location = new System.Drawing.Point(121, 38);
            this.memoEditDescription.Name = "memoEditDescription";
            this.memoEditDescription.Properties.MaxLength = 4000;
            this.memoEditDescription.Size = new System.Drawing.Size(441, 96);
            this.memoEditDescription.TabIndex = 1;
            this.memoEditDescription.UseOptimizedRendering = true;
            // 
            // listBoxControlAllTests
            // 
            this.listBoxControlAllTests.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.listBoxControlAllTests.Location = new System.Drawing.Point(12, 161);
            this.listBoxControlAllTests.MultiColumn = true;
            this.listBoxControlAllTests.Name = "listBoxControlAllTests";
            this.listBoxControlAllTests.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.listBoxControlAllTests.Size = new System.Drawing.Size(262, 284);
            this.listBoxControlAllTests.TabIndex = 2;
            // 
            // listBoxControlSelectedTest
            // 
            this.listBoxControlSelectedTest.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listBoxControlSelectedTest.Location = new System.Drawing.Point(303, 161);
            this.listBoxControlSelectedTest.MultiColumn = true;
            this.listBoxControlSelectedTest.Name = "listBoxControlSelectedTest";
            this.listBoxControlSelectedTest.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.listBoxControlSelectedTest.Size = new System.Drawing.Size(268, 284);
            this.listBoxControlSelectedTest.TabIndex = 3;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(12, 142);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(100, 13);
            this.labelControl3.TabIndex = 6;
            this.labelControl3.Text = "Список всех тестов";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(292, 142);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(135, 13);
            this.labelControl4.TabIndex = 7;
            this.labelControl4.Text = "Список выбранных тестов";
            // 
            // simpleButtonOk
            // 
            this.simpleButtonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonOk.Location = new System.Drawing.Point(405, 451);
            this.simpleButtonOk.Name = "simpleButtonOk";
            this.simpleButtonOk.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonOk.TabIndex = 8;
            this.simpleButtonOk.Text = "Ок";
            this.simpleButtonOk.Click += new System.EventHandler(this.simpleButtonOk_Click);
            // 
            // simpleButtonCancel
            // 
            this.simpleButtonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonCancel.Location = new System.Drawing.Point(500, 451);
            this.simpleButtonCancel.Name = "simpleButtonCancel";
            this.simpleButtonCancel.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonCancel.TabIndex = 9;
            this.simpleButtonCancel.Text = "Отмена";
            this.simpleButtonCancel.Click += new System.EventHandler(this.simpleButtonCancel_Click);
            // 
            // simpleButtonToAllTests
            // 
            this.simpleButtonToAllTests.Location = new System.Drawing.Point(276, 264);
            this.simpleButtonToAllTests.Name = "simpleButtonToAllTests";
            this.simpleButtonToAllTests.Size = new System.Drawing.Size(26, 23);
            this.simpleButtonToAllTests.TabIndex = 5;
            this.simpleButtonToAllTests.Text = "<";
            this.simpleButtonToAllTests.Click += new System.EventHandler(this.simpleButtonToAllTests_Click);
            // 
            // simpleButtonToSelectedTestsAll
            // 
            this.simpleButtonToSelectedTestsAll.Location = new System.Drawing.Point(276, 316);
            this.simpleButtonToSelectedTestsAll.Name = "simpleButtonToSelectedTestsAll";
            this.simpleButtonToSelectedTestsAll.Size = new System.Drawing.Size(26, 23);
            this.simpleButtonToSelectedTestsAll.TabIndex = 6;
            this.simpleButtonToSelectedTestsAll.Text = ">>";
            this.simpleButtonToSelectedTestsAll.Click += new System.EventHandler(this.simpleButtonToSelectedTestsAll_Click);
            // 
            // simpleButtonToAllTestsAll
            // 
            this.simpleButtonToAllTestsAll.Location = new System.Drawing.Point(276, 345);
            this.simpleButtonToAllTestsAll.Name = "simpleButtonToAllTestsAll";
            this.simpleButtonToAllTestsAll.Size = new System.Drawing.Size(26, 23);
            this.simpleButtonToAllTestsAll.TabIndex = 7;
            this.simpleButtonToAllTestsAll.Text = "<<";
            this.simpleButtonToAllTestsAll.Click += new System.EventHandler(this.simpleButtonToAllTestsAll_Click);
            // 
            // simpleButtonToSelectedTests
            // 
            this.simpleButtonToSelectedTests.Location = new System.Drawing.Point(276, 235);
            this.simpleButtonToSelectedTests.Name = "simpleButtonToSelectedTests";
            this.simpleButtonToSelectedTests.Size = new System.Drawing.Size(26, 23);
            this.simpleButtonToSelectedTests.TabIndex = 4;
            this.simpleButtonToSelectedTests.Text = ">";
            this.simpleButtonToSelectedTests.Click += new System.EventHandler(this.simpleButtonToSelectedTests_Click);
            // 
            // CreateProfile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.simpleButtonToAllTests);
            this.Controls.Add(this.simpleButtonToSelectedTestsAll);
            this.Controls.Add(this.simpleButtonToAllTestsAll);
            this.Controls.Add(this.simpleButtonToSelectedTests);
            this.Controls.Add(this.simpleButtonCancel);
            this.Controls.Add(this.simpleButtonOk);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.listBoxControlSelectedTest);
            this.Controls.Add(this.listBoxControlAllTests);
            this.Controls.Add(this.memoEditDescription);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.textEditTitle);
            this.Controls.Add(this.labelControl1);
            this.MinimumSize = new System.Drawing.Size(514, 410);
            this.Name = "CreateProfile";
            this.Size = new System.Drawing.Size(583, 479);
            ((System.ComponentModel.ISupportInitialize)(this.textEditTitle.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxControlAllTests)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxControlSelectedTest)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit textEditTitle;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.MemoEdit memoEditDescription;
        private DevExpress.XtraEditors.ListBoxControl listBoxControlAllTests;
        private DevExpress.XtraEditors.ListBoxControl listBoxControlSelectedTest;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.SimpleButton simpleButtonOk;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCancel;
        private DevExpress.XtraEditors.SimpleButton simpleButtonToAllTests;
        private DevExpress.XtraEditors.SimpleButton simpleButtonToSelectedTestsAll;
        private DevExpress.XtraEditors.SimpleButton simpleButtonToAllTestsAll;
        private DevExpress.XtraEditors.SimpleButton simpleButtonToSelectedTests;
    }
}

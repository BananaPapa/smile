﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using Castle.ActiveRecord;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraReports.UI;
using Eureca.Professional.ActiveRecords;
using Eureca.Professional.BaseComponents.Data;
using Eureca.Professional.CommonClasses;
using Eureca.Professional.TestingPlugInStandard.Controls.Reports;
using log4net;
using NHibernate.Criterion;
using System.Linq;


namespace Eureca.Professional.ProfessionalTeacher
{
    public partial class ReportManager : BaseTestingControl
    {
        private static readonly ILog Log = LogManager.GetLogger( typeof( ReportManager ) );

        #region Fields
        private Probationer _probationeer;
        private ProfilePassing _profilePassing;
        private Test _test;
        private TestPassing _testPassing;

        protected XtraReport FReport;
        protected string FileName;
        #endregion

        #region properties

        //public XtraReport Report
        //{
        //    get { return fReport; }
        //    set
        //    {
        //        if (fReport != value)
        //        {
        //            if (fReport != null)
        //                fReport.Dispose();
        //            fReport = value;
        //            if (fReport == null)
        //                return;
        //            //printingSystem.ClearContent();
        //            Invalidate();
        //            Update();
        //            fileName = GetReportPath(fReport, "repx");
        //            //fReport.PrintingSystem = printingSystem;
        //            fReport.CreateDocument();
        //            //printControl.ExecCommand(PrintingSystemCommand.DocumentMap, new object[] {false});
        //            //previewBar.Buttons[0].Pushed = false;
        //        }
        //    }
        //}	

        #endregion

        #region Constructor

        public ReportManager()
        {
            InitializeComponent();
        }

        #endregion

        #region Methods
        public override void InitControl()
        {
            base.InitControl();
            CaptionControl = "Создание отчетов";
            InitData();
        }

        private void InitData()
        {
            comboBoxEdit1.Properties.Items.Clear();
            //using (new SessionScope())
            //{
                var probationers = ActiveRecordBase<Probationer>.FindAll().
                    OrderBy(item => item.User.LastName);
                foreach (Probationer p in probationers)
                {
                    if (!p.User.IsArchive)
                        comboBoxEdit1.Properties.Items.Add(p);
                }
            //}
            if (comboBoxEdit1.Properties.Items.Count > 0)
                comboBoxEdit1.SelectedIndex = 0;
        }

        private void GetProfileByProbationer()
        {
            if (gridViewProfiles.FocusedRowHandle < 0)
            {
                gridControlTests.DataSource = null;
                gridViewTests.FocusedRowHandle = -1;
                return;
            }
            object profilePassinId = gridViewProfiles.GetRowCellValue(gridViewProfiles.FocusedRowHandle,
                                                                      "ProfilePassingId");
            _profilePassing = ActiveRecordBase<ProfilePassing>.Find(profilePassinId);
            gridControlTests.DataSource = ProfessionalDbExecutor.GetStatistic(_probationeer.User, _profilePassing);
            gridViewTests.BestFitColumns();
        }

        private void DeleteProfilePassings(IEnumerable<ProfilePassing> profilePassings)
        {
            try
            {
                foreach (var profilePassing in (profilePassings))
                {
                    profilePassing.DeleteAndFlush();
                }
                RefreshProfilePassings();
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                MessageForm.ShowErrorMsg(ex.Message);
            }
        }

        private void RefreshProfilePassings()
        {
            if (comboBoxEdit1.SelectedIndex == -1)
                return;
            _probationeer = (Probationer)comboBoxEdit1.SelectedItem;
            gridControlProfiles.DataSource = ProfessionalDbExecutor.GetProfilePassins(_probationeer.User);
            GetProfileByProbationer();
            GetReportsByTest();
        }


        private void GetReportsByTest()
        {
            comboBoxEditReports.Properties.Items.Clear();
            comboBoxEditReports.SelectedItem = null;
            if (gridViewTests.FocusedRowHandle < 0)
            {
                return;
            }

            object testid = gridViewTests.GetRowCellValue(gridViewTests.FocusedRowHandle, "testId");
            _test = ActiveRecordBase<Test>.Find(testid);

            object testPassingId = gridViewTests.GetRowCellValue(gridViewTests.FocusedRowHandle, "TestPassingId");
            _testPassing = ActiveRecordBase<TestPassing>.Find(testPassingId);
            foreach (Report report in _test.Reports)
            {
                comboBoxEditReports.Properties.Items.Add(report);
            }

            if (comboBoxEditReports.SelectedIndex == -1
                && comboBoxEditReports.Properties.Items.Count > 0)
            {
                comboBoxEditReports.SelectedIndex = 0;
            }
        }

        /// <summary>
        /// метод включения- отключения формы
        /// </summary>
        /// <param name="enable"></param>
        private void EnableForm(bool enable)
        {
            barStaticItemStatus.Caption = !enable ? "Идёт формирование отчёта, ждите..." : "";
            simpleButtonCreateProfileReport.Enabled = enable;
            simpleButtonCreateReport.Enabled = enable;
            Application.DoEvents();
        }

        private void CreateCommontTestReport(Probationer probationer, ProfilePassing profilePassing, Test test,
                                           TestPassing testpassing)
        {
            try
            {
                var sm = new StandardReportManager();
                sm.GenerateCommonReport(probationer, profilePassing, test, testpassing);
                Refresh();
                EnableForm(true);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                MessageForm.ShowErrorMsg(ex.Message);
            }
            finally
            {
                EnableForm(true);
            }
        }

        /// <summary>
        /// метод создания детального отчета по тесту
        /// </summary>
        /// <param name="probationer"></param>
        /// <param name="profilePassing"></param>
        /// <param name="test"></param>
        /// <param name="testpassing"></param>
        public void CreateDetailTestReport(Probationer probationer, ProfilePassing profilePassing, Test test,
                                            TestPassing testpassing)
        {
            try
            {
                var sm = new StandardReportManager();
                sm.CreateDetailTestReport(probationer, profilePassing, test, testpassing);
                Refresh();
                EnableForm(true);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                MessageForm.ShowErrorMsg(ex.Message);
            }
            finally
            {
                EnableForm(true);
            }
        }

        private void CreateCommontProfileReport(Probationer probationer, ProfilePassing profilePassing)
        {
            try
            {
                if (profilePassing == null)
                {
                    MessageForm.ShowErrorMsg("Необходимо выбрать профиль");
                    return;
                }
                var repCommonTests = new XtraReportCommonProfiles
                {
                    labelFIO = { Text = probationer.User.FullName },
                    labelBirthDay = { Text = probationer.User.BirthDate.ToShortDateString() },
                    labelProfileName = { Text = profilePassing.Profile.ToString() },
                    labelTime =
                    {
                        Text = string.Format("{0}       с {1}  -  по {2}",
                                             profilePassing.TimeInterval.StartTime.
                                                 ToShortDateString(),
                                             profilePassing.TimeInterval.StartTime.
                                                 ToLongTimeString(),
                                             profilePassing.TimeInterval.FinishTime.
                                                 ToLongTimeString())
                    }
                };

                #region постановка пользовательских наименований

                //repCommonTests.xrLabelCaption.Text = GetUserField(rep, "xrLabelCaption");
                //repCommonTests.xrLabelBirthday.Text = GetUserField(rep, "xrLabelBirthday");
                //repCommonTests.xrLabelProfileName.Text = GetUserField(rep, "xrLabelProfileName");
                //repCommonTests.xrLabelTime.Text = GetUserField(rep, "xrLabelTime");
                //repCommonTests.xrLabelStatistic.Text = GetUserField(rep, "xrLabelStatistic");
                //repCommonTests.xrTableMethodTitle.Text = GetUserField(rep, "xrTableMethodTitle");
                //repCommonTests.xrTableTestTitle.Text = GetUserField(rep, "xrTableTestTitle");
                //repCommonTests.xrTableScaleName.Text = GetUserField(rep, "xrTableScaleName");
                //repCommonTests.xrTableMaxEff.Text = GetUserField(rep, "xrTableMaxEff");
                //repCommonTests.xrTableResult.Text = GetUserField(rep, "xrTableResult");
                //repCommonTests.xrTablePercent.Text = GetUserField(rep, "xrTablePercent");
                //repCommonTests.xrTableValuation.Text = GetUserField(rep, "xrTableValuation");

                #endregion

                int ret = ProfessionalDbExecutor.GetCommonProfile(repCommonTests.dataSetProfessional1, probationer,
                                                                  profilePassing);
                if (ret == 0)
                {
                    MessageForm.ShowErrorMsg("Для данного прохождения профиля отсутствует информация.");
                    EnableForm(true);
                    return;
                }
                EnableForm(true);

                //var repTool = new ReportPrintTool( repCommonTests );
                repCommonTests.ShowPreviewDialog();
                
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                MessageForm.ShowErrorMsg(ex.Message);
            }
            finally
            {
                EnableForm(true);
            }
        }

        //private void simpleButton1_Click(object sender, EventArgs e)
        //{
        //    OpenFileDialog fd = new OpenFileDialog();
        //    //fd.Filter = "Формы отчетов |(*.repx;*.REPX)";
        //    XtraReport r;
        //    if (fd.ShowDialog() == DialogResult.OK)
        //    {
        //        r = (XtraReport)XtraReport.FromFile(fd.FileName, true);
        //    }
        //    else
        //        r = new XtraReportCommonTests();
        //    PreviewControl p = new PreviewControl();
        //    p.Report = r;
        //    BaseTestingControl.ShowInForm(p, true, true, "");

        //}
        public static string GetReportPath(XtraReport fReport, string ext)
        {
            Assembly asm = Assembly.GetExecutingAssembly();
            string repName = fReport.Name;
            if (repName.Length == 0) repName = fReport.GetType().Name;
            string dirName = Path.GetDirectoryName(asm.Location);
            if (dirName != null) return Path.Combine(dirName, repName + "." + ext);
            return string.Empty;
        }


        private static string GetUserField(Report rep, string caption)
        {
            try
            {
                return
                    ActiveRecordBase<ReportField>.FindOne(Expression.Eq("Report", rep),
                                                          Expression.Eq("FieldName", caption)).UserValue;
            }
            catch
            {
                return "";
            }
        }


        #endregion

        #region Event Handlers

        private void ComboBoxEdit1SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshProfilePassings();
        }

        /// <summary>
        /// выбор профиля
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridViewProfilesFocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            if (e.FocusedRowHandle < 0)
            {
                gridControlTests.DataSource = null;
                gridViewTests.FocusedRowHandle = -1;
                return;
            }
            //using (new SessionScope())
            //{
            object profilePassinId = gridViewProfiles.GetRowCellValue(e.FocusedRowHandle, "ProfilePassingId");
            if(profilePassinId == null)
                return;
            _profilePassing = ActiveRecordBase<ProfilePassing>.Find(profilePassinId);
            gridControlTests.DataSource = ProfessionalDbExecutor.GetStatistic(_probationeer.User, _profilePassing);
            gridViewTests.BestFitColumns();
            //}

            GetReportsByTest();
        }


        /// <summary>
        /// выбор теста
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridViewTestsFocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            GetReportsByTest();
        }

        /// <summary>
        /// создание теста
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SimpleButtonCreateReportClick(object sender, EventArgs e)
        {
            if (comboBoxEditReports.SelectedIndex == -1)
            {
                MessageForm.ShowErrorMsg("Необходимо выбрать вид отчета");
                return;
            }
            if (_test.Type.PlugInFileName != "TestingPlugInStandard")
            {
                MessageForm.ShowErrorMsg("Для данного теста невозможно построить отчет");
                return;
            }

            var rep = (Report)comboBoxEditReports.SelectedItem;
            
            //отключаем форму
            EnableForm(false);
            if (rep.Id == 1)
            {
                //общие отчеты

                CreateCommontTestReport(_probationeer, _profilePassing, _test, _testPassing);
            }
            if (rep.Id == 2)
            {
                //подробный отчет по тесту
                CreateDetailTestReport(_probationeer, _profilePassing, _test, _testPassing);
            }
        }

        private void SimpleButtonCreateProfileReportClick(object sender, EventArgs e)
        {
            CreateCommontProfileReport(_probationeer, _profilePassing);
        }

        private void SimpleButtonDeleteProfileClick(object sender, EventArgs e)
        {
            if (MessageForm.ShowYesNoQuestionMsg("Вы уверены, что хотите удалить тестирование?") == DialogResult.No)
                return;
            DeleteProfilePassings(new List<ProfilePassing> { _profilePassing });

        }

        private void SimpleButtonDeleteAllClick(object sender, EventArgs e)
        {
            if (MessageForm.ShowYesNoQuestionMsg("Вы уверены, что хотите удалить все тестирования?") == DialogResult.No)
                return;
            ProfilePassing[] profilePassings;

            //using (new SessionScope())
            //{
                profilePassings = ProfilePassing.FindAll(Expression.Eq("Probationer", _probationeer));
            //}

            DeleteProfilePassings(profilePassings);
        }
    }
        #endregion


}
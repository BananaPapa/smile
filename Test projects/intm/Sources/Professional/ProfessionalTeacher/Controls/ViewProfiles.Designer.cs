﻿namespace Eureca.Professional.ProfessionalTeacher
{
    partial class ViewProfiles
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBoxControlProfiles = new DevExpress.XtraEditors.ListBoxControl();
            this.listBoxControlTests = new DevExpress.XtraEditors.ListBoxControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButtonAdd = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonChange = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonDelete = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonClose = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxControlProfiles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxControlTests)).BeginInit();
            this.SuspendLayout();
            // 
            // listBoxControlProfiles
            // 
            this.listBoxControlProfiles.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.listBoxControlProfiles.HorizontalScrollbar = true;
            this.listBoxControlProfiles.Location = new System.Drawing.Point(15, 32);
            this.listBoxControlProfiles.Name = "listBoxControlProfiles";
            this.listBoxControlProfiles.Size = new System.Drawing.Size(237, 418);
            this.listBoxControlProfiles.TabIndex = 0;
            this.listBoxControlProfiles.SelectedIndexChanged += new System.EventHandler(this.listBoxControlProfiles_SelectedIndexChanged);
            this.listBoxControlProfiles.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listBoxControlProfiles_MouseDoubleClick);
            // 
            // listBoxControlTests
            // 
            this.listBoxControlTests.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listBoxControlTests.HorizontalScrollbar = true;
            this.listBoxControlTests.Location = new System.Drawing.Point(297, 32);
            this.listBoxControlTests.Name = "listBoxControlTests";
            this.listBoxControlTests.Size = new System.Drawing.Size(267, 418);
            this.listBoxControlTests.TabIndex = 1;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(15, 13);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(89, 13);
            this.labelControl1.TabIndex = 2;
            this.labelControl1.Text = "Список профилей";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(297, 13);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(130, 13);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "Список тестов в профиле";
            // 
            // simpleButtonAdd
            // 
            this.simpleButtonAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.simpleButtonAdd.Location = new System.Drawing.Point(15, 456);
            this.simpleButtonAdd.Name = "simpleButtonAdd";
            this.simpleButtonAdd.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonAdd.TabIndex = 4;
            this.simpleButtonAdd.Text = "Добавить";
            this.simpleButtonAdd.Click += new System.EventHandler(this.simpleButtonAdd_Click);
            // 
            // simpleButtonChange
            // 
            this.simpleButtonChange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.simpleButtonChange.Location = new System.Drawing.Point(96, 456);
            this.simpleButtonChange.Name = "simpleButtonChange";
            this.simpleButtonChange.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonChange.TabIndex = 5;
            this.simpleButtonChange.Text = "Изменить";
            this.simpleButtonChange.Click += new System.EventHandler(this.simpleButtonChange_Click);
            // 
            // simpleButtonDelete
            // 
            this.simpleButtonDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.simpleButtonDelete.Location = new System.Drawing.Point(177, 456);
            this.simpleButtonDelete.Name = "simpleButtonDelete";
            this.simpleButtonDelete.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonDelete.TabIndex = 6;
            this.simpleButtonDelete.Text = "Удалить";
            this.simpleButtonDelete.Click += new System.EventHandler(this.simpleButtonDelete_Click);
            // 
            // simpleButtonClose
            // 
            this.simpleButtonClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonClose.Location = new System.Drawing.Point(489, 456);
            this.simpleButtonClose.Name = "simpleButtonClose";
            this.simpleButtonClose.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonClose.TabIndex = 8;
            this.simpleButtonClose.Text = "Закрыть";
            this.simpleButtonClose.Click += new System.EventHandler(this.simpleButtonClose_Click);
            // 
            // ViewProfiles
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.simpleButtonClose);
            this.Controls.Add(this.simpleButtonDelete);
            this.Controls.Add(this.simpleButtonChange);
            this.Controls.Add(this.simpleButtonAdd);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.listBoxControlTests);
            this.Controls.Add(this.listBoxControlProfiles);
            this.MinimumSize = new System.Drawing.Size(523, 268);
            this.Name = "ViewProfiles";
            this.Size = new System.Drawing.Size(572, 491);
            ((System.ComponentModel.ISupportInitialize)(this.listBoxControlProfiles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxControlTests)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.ListBoxControl listBoxControlProfiles;
        private DevExpress.XtraEditors.ListBoxControl listBoxControlTests;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.SimpleButton simpleButtonAdd;
        private DevExpress.XtraEditors.SimpleButton simpleButtonChange;
        private DevExpress.XtraEditors.SimpleButton simpleButtonDelete;
        private DevExpress.XtraEditors.SimpleButton simpleButtonClose;
    }
}

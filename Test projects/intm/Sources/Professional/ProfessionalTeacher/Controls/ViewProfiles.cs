﻿using System;
using System.Windows.Forms;
using Castle.ActiveRecord;
using Eureca.Professional.ActiveRecords;
using Eureca.Professional.BaseComponents;
using Eureca.Professional.CommonClasses;
using NHibernate.Criterion;

namespace Eureca.Professional.ProfessionalTeacher
{
    /// <summary>
    /// контрол для просмотра списка профилей
    /// </summary>
    public partial class ViewProfiles : BaseTestingControl
    {
        #region конструктор

        public ViewProfiles()
        {
            InitializeComponent();
        }

        #endregion

        #region методы

        public override void InitControl()
        {
            base.InitControl();
            //CreateScope();
            CaptionControl = "Просмотр профилей";
            InsertData();
        }

        /// <summary>
        /// метод вставки данных из БД
        /// </summary>
        private void InsertData()
        {
            listBoxControlProfiles.Items.Clear();
            listBoxControlTests.Items.Clear();
            listBoxControlProfiles.Items.AddRange(ActiveRecordBase<Profile>.FindAll());
            if (listBoxControlProfiles.ItemCount > 0)
            {
                listBoxControlProfiles.SelectedIndex = 0;
            }
        }

        /// <summary>
        /// метод обновления тестов
        /// </summary>
        private void RefreshTests()
        {
            listBoxControlTests.Items.Clear();
            if (listBoxControlProfiles.SelectedIndex == -1)
            {
                return;
            }
            var prof = (Profile) listBoxControlProfiles.SelectedItem;
            foreach (Test test in prof.Tests)
                listBoxControlTests.Items.Add(test);
            if (listBoxControlTests.ItemCount > 0)
                listBoxControlTests.SelectedIndex = 0;
        }

        private void AddProfile()
        {
            var uc = new CreateProfile(ControlTypeAction.INSERT, -1);
            uc.AddProfile += uc_AddProfile;
            uc.InitControl();
            uc.ShowInForm();
            RefreshTests();
        }

        /// <summary>
        /// метод изменения профиля
        /// </summary>
        private void ChangeProfile()
        {
            if (listBoxControlProfiles.SelectedIndex == -1)
            {
                MessageForm.ShowErrorMsg("Необходимо выбрать профиль");
                return;
            }
            var uc = new CreateProfile(ControlTypeAction.UPDATE, ((Profile) listBoxControlProfiles.SelectedItem).Id);
            uc.ChangeProfile += uc_ChangeProfile;
            uc.InitControl();
            uc.ShowInForm();
        }

        /// <summary>
        /// метод удаления профиля
        /// </summary>
        private void DeleteProfile()
        {
            if (listBoxControlProfiles.SelectedIndex == -1)
            {
                MessageForm.ShowErrorMsg("Необходимо выбрать профиль");
                return;
            }
            if (MessageForm.ShowYesNoQuestionMsg("Вы уверены, что хотите удалить профиль?") == DialogResult.No)
                return;
            var prof = (Profile) listBoxControlProfiles.SelectedItem;

            //проверка прикреплен ли профиль
            if (Probationer.FindAll(Expression.Eq("Profile", prof)).Length > 0)
            {
                MessageForm.ShowErrorMsg(
                   string.Format(
                       "Невозможно удалить профиль. \nДля его удаления выберите у испытуемых другой профиль.")
                   );
                return;
            }
            prof.Tests.Clear();
            try
            {
                prof.DeleteAndFlush();
                listBoxControlProfiles.Items.Remove(prof);
            }
            catch (Exception ex)
            {
                MessageForm.ShowErrorMsg("Невозможно удалить данный профиль");
            }
        }

        #endregion

        #region обработчики событий пользовательских контролов

        /// <summary>
        /// обработчик событи добавления профиля
        /// </summary>
        /// <param name="profile"></param>
        private void uc_AddProfile(Profile profile)
        {
            listBoxControlProfiles.SelectedIndex = listBoxControlProfiles.Items.Add(profile);
            listBoxControlProfiles.Refresh();
            RefreshTests();
        }

        /// <summary>
        /// обработчик события изменения профиля
        /// </summary>
        /// <param name="profile"></param>
        private void uc_ChangeProfile(Profile profile)
        {
            foreach (Profile prof in listBoxControlProfiles.Items)
            {
                if (prof.Equals(profile))
                {
                    listBoxControlProfiles.Items[listBoxControlProfiles.SelectedIndex] = profile;
                    listBoxControlProfiles.Refresh();
                    RefreshTests();
                    break;
                }
            }
        }

        #endregion

        #region обработчики событий стандартных компонент

        private void simpleButtonClose_Click(object sender, EventArgs e)
        {
            if (ParentForm != null)
                ParentForm.Close();
        }

        private void listBoxControlProfiles_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshTests();
        }

        private void simpleButtonAdd_Click(object sender, EventArgs e)
        {
            AddProfile();
        }

        private void simpleButtonChange_Click(object sender, EventArgs e)
        {
            ChangeProfile();
        }

        private void listBoxControlProfiles_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (listBoxControlProfiles.SelectedIndex == -1)
                return;
            ChangeProfile();
        }

        private void simpleButtonDelete_Click(object sender, EventArgs e)
        {
            DeleteProfile();
        }
        #endregion

    }

}
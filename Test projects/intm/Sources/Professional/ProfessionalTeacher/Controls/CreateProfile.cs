﻿using System;
using System.Windows.Forms;
using Castle.ActiveRecord;
using Eureca.Professional.ActiveRecords;
using Eureca.Professional.BaseComponents;
using Eureca.Professional.CommonClasses;
using NHibernate.Criterion;

namespace Eureca.Professional.ProfessionalTeacher
{
    public delegate void CreateProfileDelegate(Profile profile);

    /// <summary>
    /// контрол для редактирования профиля
    /// </summary>
    public partial class CreateProfile : BaseTestingControl
    {
        #region поля

        /// <summary>
        /// тип действия формы
        /// </summary>
        private readonly ControlTypeAction _typeAction = ControlTypeAction.VIEW;

        /// <summary>
        /// текущий профиль 
        /// </summary>
        private Profile _currentProfile;

        #endregion

        #region события

        /// <summary>
        /// событие на добавления профиля
        /// </summary>
        public event CreateProfileDelegate AddProfile;

        /// <summary>
        /// событие на изменения профиля
        /// </summary>
        public event CreateProfileDelegate ChangeProfile;

        #endregion

        #region конструктор

        public CreateProfile(ControlTypeAction TypeAction, int profileId)
        {
            //ResetSessionScope();
            _typeAction = TypeAction;
            if (profileId != -1)
                _currentProfile = ActiveRecordBase<Profile>.Find(profileId);
            InitializeComponent();
        }

        #endregion

        #region методы

        public override void InitControl()
        {
            base.InitControl();
            InsertData();
            switch (_typeAction)
            {
                case ControlTypeAction.INSERT:
                    CaptionControl = "Добавление профиля";
                    break;
                case ControlTypeAction.UPDATE:
                    CaptionControl = "Изменение профиля";
                    break;
                default:
                    CaptionControl = "Просмотр профиля";
                    break;
            }
        }

        private void InsertData()
        {
            listBoxControlAllTests.Items.Clear();
            listBoxControlAllTests.Items.AddRange(ActiveRecordBase<Test>.FindAll());
            if (_currentProfile != null)
            {
                //вставка данных из профиля
                textEditTitle.Text = _currentProfile.Title;
                memoEditDescription.Text = _currentProfile.Description;
                foreach (Test test in _currentProfile.Tests)
                {
                    listBoxControlAllTests.Items.Remove(test);
                    listBoxControlSelectedTest.Items.Add(test);
                }
            }
        }

        /// <summary>Метод перемещения из списка всех отчетов в список выбраных отчетов</summary>
        /// <param name="All"></param>
        private void dragFromAllToSelectedOneTest(bool All)
        {
            if (!All)
            {
                for (int i = 0; i < listBoxControlAllTests.SelectedItems.Count; i++)
                {
                    var test = (Test) listBoxControlAllTests.SelectedItems[i];
                    listBoxControlSelectedTest.Items.Add(test);
                    listBoxControlAllTests.Items.Remove(test);
                }
            }
            else
            {
                foreach (object o in listBoxControlAllTests.Items)
                {
                    listBoxControlSelectedTest.Items.Add(o);
                }
                listBoxControlAllTests.Items.Clear();
            }
        }

        /// <summary>Метод перемещения из списка выбраных отчетов во все отчеты</summary>
        /// <param name="All"></param>
        private void dragFromSelectedToAllOneTest(bool All)
        {
            if (!All)
            {
                for (int i = 0; i < listBoxControlSelectedTest.SelectedItems.Count; i++)
                {
                    var test = (Test) listBoxControlSelectedTest.SelectedItems[i];
                    listBoxControlAllTests.Items.Add(test);
                    listBoxControlSelectedTest.Items.Remove(test);
                }
            }
            else
            {
                foreach (object o in listBoxControlSelectedTest.Items)
                {
                    listBoxControlAllTests.Items.Add(o);
                }
                listBoxControlSelectedTest.Items.Clear();
            }
        }

        /// <summary>
        /// метод добавления профиля
        /// </summary>
        private void AddNewProfile()
        {
            _currentProfile = new Profile(textEditTitle.Text.Trim(), memoEditDescription.Text);
            foreach (Test test in listBoxControlSelectedTest.Items)
            {
                _currentProfile.Tests.Add(test);
            }
            _currentProfile.Save();
            //ResetSessionScope();
            AddProfile(_currentProfile);
        }

        /// <summary>
        /// метод измениения профиля
        /// </summary>
        private void ChangeCurrentProfile()
        {
            _currentProfile.Tests.Clear();
            _currentProfile.Title = textEditTitle.Text.Trim();
            _currentProfile.Description = memoEditDescription.Text;
            foreach (Test test in listBoxControlSelectedTest.Items)
            {
                _currentProfile.Tests.Add(test);
            }
            _currentProfile.Save();
            //ResetSessionScope();
            ChangeProfile(_currentProfile);
        }

        /// <summary>
        /// метиод проверки полей
        /// </summary>
        /// <returns></returns>
        private bool CheckFields()
        {
            if (_typeAction != ControlTypeAction.VIEW && string.IsNullOrEmpty(textEditTitle.Text.Trim()))
            {
                MessageForm.ShowErrorMsg("Введите наименование");
                return false;
            }
            Profile p = ActiveRecordBase<Profile>.FindOne(Expression.Eq("Title", textEditTitle.Text.Trim()));
            if (p != null && !p.Equals(_currentProfile))
            {
                MessageForm.ShowErrorMsg("Профиль с данным наименованием уже существует");
                return false;
            }
            return true;
        }

        #endregion

        #region обработчики событий от стандартных компонент

        private void simpleButtonToSelectedTests_Click(object sender, EventArgs e)
        {
            dragFromAllToSelectedOneTest(false);
        }

        private void simpleButtonToAllTests_Click(object sender, EventArgs e)
        {
            dragFromSelectedToAllOneTest(false);
        }

        private void simpleButtonToSelectedTestsAll_Click(object sender, EventArgs e)
        {
            dragFromAllToSelectedOneTest(true);
        }

        private void simpleButtonToAllTestsAll_Click(object sender, EventArgs e)
        {
            dragFromSelectedToAllOneTest(true);
        }

        private void simpleButtonOk_Click(object sender, EventArgs e)
        {
            if (!CheckFields())
                return;
            if (_typeAction == ControlTypeAction.INSERT)
            {
                AddNewProfile();
            }
            if (_typeAction == ControlTypeAction.UPDATE)
            {
                ChangeCurrentProfile();
            }
            if (ParentForm != null)
                ParentForm.DialogResult = DialogResult.OK;
        }

        private void simpleButtonCancel_Click(object sender, EventArgs e)
        {
            if (ParentForm != null)
                ParentForm.Close();
        }

        #endregion
    }
}
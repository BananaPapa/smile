﻿using System.Reflection;
using System.Runtime.InteropServices;
using Eureca.Integrator.Utility;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

[assembly: AssemblyTitle("ProfessionalTeacher")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Эврика")]
[assembly: AssemblyProduct("ProfessionalTeacher")]
[assembly: AssemblyCopyright("Copyright © Эврика 2009")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.

[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM

[assembly: Guid("e9d22fae-2d1b-458a-901b-28c87325f9d4")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: log4net.Config.XmlConfigurator( Watch = true )]
[assembly: AssemblyVersion( AppVersion.ApplicationVersionString )]
[assembly: AssemblyFileVersion( AppVersion.ApplicationVersionString )]
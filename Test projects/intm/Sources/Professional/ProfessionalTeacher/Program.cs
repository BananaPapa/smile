﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using DevExpress.Skins;
using Eureca.Integrator.Utility;
using Eureca.Integrator.Utility.Settings;
using Eureca.Professional.ActiveRecords;
using Eureca.Professional.BaseComponents;
using Eureca.Professional.BaseComponents.Configuration;
using Eureca.Professional.CommonClasses;
using Eureca.Professional.CommonClasses.Configuration;
using Eureca.Utility.Extensions;
using log4net;
using MessageBox = Eureca.Integrator.Utility.Forms.MessageBox;
using DevExpress.XtraEditors;

namespace Eureca.Professional.ProfessionalTeacher
{
    internal static class Program 
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof (Program));
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            if (RunnerApplication.AlreadyRunning())
            {
                return;
            }
            SkinManager.EnableFormSkins();
            SkinManager.EnableMdiFormSkins();


            Thread.CurrentThread.CurrentCulture = new CultureInfo("ru-RU");
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("ru");

            Application.ThreadException += (ApplicationThreadException);
            AppDomain.CurrentDomain.UnhandledException += (CurrentDomainUnhandledException);


            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            if (!AppActivation.IsActivation(Application.StartupPath)
                && !AppActivation.Activation(Application.StartupPath))
            {
                return;
            }
            try
            {
                RunnerApplication.CurrentArm = ARMType.TEACHER;
                RunnerApplication.CurrentUser = null;

                //Синхронизация с глобальными настройками.
                ProfessionalStartForm.SyncProfSettings( );

                //загружаю настройки арма
                ARMSettingsSection armSettings = ARMSettingsSection.GetFromConfigSource();
                //выставляю локальные настройки
                armSettings.ARMType = RunnerApplication.CurrentArm;
                //создаю контейнер для параметров
                var param = new StartUpParameters {ARMSettings = armSettings, ApplicationIcon = Properties.Resources.TeacherIcon};

                //запускаю функцию базового класса для всех армов
                if (RunnerApplication.ConnectDb())
                {
                    //сохраняю настройки
                    Settings.Save( );
                    RunnerApplication.RunUpdater( );
              
                    if (RunnerApplication.Authotization( param ))
                    {
                        Application.Run( new StartForm( ) );
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                MessageForm.ShowErrorMsg(ex.Message);
            }
        }

        private static void CurrentDomainUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Log.Error("Необработанное исключение");
            Log.Error(e.ExceptionObject);
            if (
                MessageForm.ShowYesNoQuestionMsg(
                    "Выполненное действие вызвало ошибку.\n За информацией обратитесь в службу технической поддержки. Продолжить работу?") !=
                DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private static void ApplicationThreadException(object sender, ThreadExceptionEventArgs e)
        {
            Log.Error("Необработанное исключение");
            Log.Error(e.Exception);
            if (
                MessageForm.ShowYesNoQuestionMsg(
                    "Выполненное действие вызвало ошибку.\n За информацией обратитесь в службу технической поддержки. Продолжить работу?") !=
                DialogResult.Yes)
            {
                Application.Exit();
            }
        }
    }
}
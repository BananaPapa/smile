﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Eureca.Professional.ActiveRecords;

namespace TestPlugins
{
    public interface IJournalModuleManager
    {
        void EditJournals(Form owner = null );
        
        void ShowUnverifiedTests( Form owner = null );

        void EditTestQuestions( Test test, Form owner = null );

        void ShowTestResult( TestPassing testPassing, Form owner = null );

        bool PerformTest( TestPassing testPassing, Form owner = null );

        //void ShowResults( TestPassing testPassing, Form owner = null );

        bool InitDataBase(string connectionString);

        bool CheckTest(Test test,out string error);


    }
}

﻿using System;
using System.Collections;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using System.Linq;

namespace Eureca.Professional.CommonClasses
{
    /// <summary>
    /// Класс для представления файловой системы на удаленном сервере
    /// </summary>
    [Serializable]
    public class FileItem
    {
        static string[] imageExtensions = new[]
                                      {
                                          "jpg",
                                          "gif",
                                          "jpeg",
                                          "bmp",
                                          "raw",
                                          "png",
                                          "emf",
                                          "ico",
                                          "tif",
                                          "tiff",
                                          "jp2",
                                          "pcx",
                                          "tga",
                                          "wbmp"
                                      };

        static string[] dvdExtensions = new[]
                                      {
                                          "BUP",
                                          "IFO",
                                          "VOB",
                                      };

        public FileItem(FileInfo fileInfo)
        {
            if (fileInfo == null)
                throw new NullReferenceException("fileInfo = null");
            Icon icon = Icon.ExtractAssociatedIcon(fileInfo.FullName);
            if (icon != null)
            {
                var stream = new MemoryStream();
                icon.Save(stream);
                stream.ToArray();
            }
            ItemExtension = !string.IsNullOrEmpty(fileInfo.Extension) ? fileInfo.Extension.Replace(".", "") : "";
            ItemName = !string.IsNullOrEmpty(fileInfo.Extension) ?
                fileInfo.Name.Replace(fileInfo.Extension, "") : fileInfo.Name;
            ItemPath = fileInfo.FullName;
            Length = fileInfo.Length;
            ItemType = FileItemType.File;
        }

        public FileItem(DirectoryInfo directoryInfo, bool isRoot)
        {
            if (directoryInfo == null)
                throw new NullReferenceException("directoryInfo = null");
            ItemName = !isRoot ? directoryInfo.Name : "...";
            ItemExtension = "Папка";
            ItemPath = !isRoot ? directoryInfo.FullName : directoryInfo.Parent.FullName;
            ItemType = FileItemType.Folder;
        }

        /// <summary>
        /// Наименование
        /// </summary>
        public string ItemName { get; private set; }
        /// <summary>
        /// Расширение
        /// </summary>
        public string ItemExtension { get; private set; }
        /// <summary>
        /// Путь к файлу
        /// </summary>
        public string ItemPath { get; private set; }
        /// <summary>
        /// Длина файла
        /// </summary>
        public long Length { get; private set; }
        /// <summary>
        /// Иконка
        /// </summary>
        public Image ItemIcon
        {
            get { return RegisteredDocumentIcon.GetFileIcon(ItemPath); }
        }

        /// <summary>
        /// Размер файла в читабельном виде
        /// </summary>
        public string LengthView
        {
            get
            {
                if (ItemType == FileItemType.Folder)
                    return "";
                if (Length < 1024)
                    return string.Format("{0} Кб", 1);
                return string.Format("{0} Кб", (int) Length/1024);
            }
        }
        /// <summary>
        /// Тип элемента (папка или файл)
        /// </summary>
        public FileItemType ItemType { get; private set; }
        public bool IsImage()
        {
           if(imageExtensions.Where(item=>item.ToUpper().CompareTo(ItemExtension.ToUpper()) == 0).Any())
               return true;
            return false; 
        }
        public bool IsDvd()
        {
            if (dvdExtensions.Where(item => item.ToUpper().CompareTo(ItemExtension.ToUpper()) == 0).Count() > 0)
                return true;
            return false;
        }
    }

    public enum FileItemType
    {
        Folder = 1,
        File = 2
    }


    public class RegisteredDocumentIcon
    {
        private static readonly Hashtable _iconHash = new Hashtable();
        private static bool _useHash = true;

        public static bool UseHash
        {
            get { return _useHash; }
            set { _useHash = value; }
        }


        public static Bitmap GetFileIcon(string fileName)
        {
            Bitmap icon = null;
            if (_useHash)
            {
                string extension = Path.GetExtension(fileName);
                icon = (Bitmap) _iconHash[extension];
                if (icon == null)
                {
                    icon = GetFileIconFromSystem(fileName);
                    _iconHash[extension] = icon;
                }
            }
            else
            {
                icon = GetFileIconFromSystem(fileName);
            }
            return icon;
        }


        private static Bitmap GetFileIconFromSystem(string fileName)
        {
            //Getting the small Icon
            var a_shinfo = new ShellFileInfoStructure();
            SHGetFileInfo(fileName, 0x4000, out a_shinfo,
                          (uint) Marshal.SizeOf(a_shinfo),
                          Win32.SHGFI_ICON | Win32.FILE_ATTRIBUTE_DIRECTORY | Win32.SHGFI_SMALLICON);
            return Bitmap.FromHicon(a_shinfo.hIcon);
        }

        #region Import win32

        // SHFILEINFO structure

        [DllImport("shell32.dll")]
        private static extern IntPtr SHGetFileInfo(string pszPath, uint dwFileAttributes,
                                                   out ShellFileInfoStructure psfi, uint cbSizeFileInfo, uint uFlags);

        #region Nested type: ShellFileInfoStructure

        [StructLayout(LayoutKind.Sequential)]
        private struct ShellFileInfoStructure
        {
            public readonly IntPtr hIcon;
            public readonly IntPtr iIcon;
            public readonly uint dwAttributes;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 260)] public readonly string szDisplayName;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 80)] public readonly string szTypeName;
        } ;

        #endregion

        #region Nested type: Win32

        private class Win32
        {
            public const uint SHGFI_ICON = 0x100;
            public const uint FILE_ATTRIBUTE_DIRECTORY = 0x10;
            public const uint SHGFI_LARGEICON = 0x0; // 'Large icon
            public const uint SHGFI_SMALLICON = 0x1; // 'Small icon
        }

        #endregion

        #endregion
    }
}
﻿namespace Eureca.Professional.CommonClasses
{
    partial class WaitingForm
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControlReportProcess = new DevExpress.XtraEditors.PanelControl();
            this.simpleButtonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.labelControlMessage = new DevExpress.XtraEditors.LabelControl();
            this.marqueeProgressBarControl1 = new DevExpress.XtraEditors.MarqueeProgressBarControl();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlReportProcess)).BeginInit();
            this.panelControlReportProcess.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.marqueeProgressBarControl1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControlReportProcess
            // 
            this.panelControlReportProcess.Controls.Add(this.simpleButtonCancel);
            this.panelControlReportProcess.Controls.Add(this.labelControlMessage);
            this.panelControlReportProcess.Controls.Add(this.marqueeProgressBarControl1);
            this.panelControlReportProcess.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControlReportProcess.Location = new System.Drawing.Point(0, 0);
            this.panelControlReportProcess.Name = "panelControlReportProcess";
            this.panelControlReportProcess.Size = new System.Drawing.Size(438, 101);
            this.panelControlReportProcess.TabIndex = 13;
            // 
            // simpleButtonCancel
            // 
            this.simpleButtonCancel.Location = new System.Drawing.Point(351, 73);
            this.simpleButtonCancel.Name = "simpleButtonCancel";
            this.simpleButtonCancel.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonCancel.TabIndex = 12;
            this.simpleButtonCancel.Text = "Отменить";
            this.simpleButtonCancel.Click += new System.EventHandler(this.SimpleButtonCancelClick);
            // 
            // labelControlMessage
            // 
            this.labelControlMessage.Location = new System.Drawing.Point(5, 5);
            this.labelControlMessage.Name = "labelControlMessage";
            this.labelControlMessage.Size = new System.Drawing.Size(123, 13);
            this.labelControlMessage.TabIndex = 11;
            this.labelControlMessage.Text = "Идет процесс. Ждите...";
            // 
            // marqueeProgressBarControl1
            // 
            this.marqueeProgressBarControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.marqueeProgressBarControl1.EditValue = 0;
            this.marqueeProgressBarControl1.Location = new System.Drawing.Point(5, 36);
            this.marqueeProgressBarControl1.Name = "marqueeProgressBarControl1";
            this.marqueeProgressBarControl1.Size = new System.Drawing.Size(428, 31);
            this.marqueeProgressBarControl1.TabIndex = 10;
            // 
            // WaitingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(438, 101);
            this.Controls.Add(this.panelControlReportProcess);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "WaitingForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Выполнение операций";
            this.TopMost = true;
            ((System.ComponentModel.ISupportInitialize)(this.panelControlReportProcess)).EndInit();
            this.panelControlReportProcess.ResumeLayout(false);
            this.panelControlReportProcess.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.marqueeProgressBarControl1.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControlReportProcess;
        private DevExpress.XtraEditors.LabelControl labelControlMessage;
        private DevExpress.XtraEditors.MarqueeProgressBarControl marqueeProgressBarControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCancel;
    }
}

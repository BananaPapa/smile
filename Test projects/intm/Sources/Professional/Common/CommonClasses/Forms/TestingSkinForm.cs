﻿using System;
using System.Runtime.InteropServices;

namespace Eureca.Professional.CommonClasses
{
    public partial class TestingSkinForm : DevExpress.XtraEditors.XtraForm
    {
        #region поля для формы

        private const string DllUser32 = "User32.dll";
        private const int MF_BYCOMMAND = 0;
        private const int SC_CLOSE = 0xF060;
        private readonly bool _showClose;

        #endregion

        #region поля для наследуемых форм

        /// <summary>
        /// флаг. спрашивать ли перед закрытием формы
        /// </summary>
        protected static bool AskBeforeExit = true;

        #endregion

        /// <summary>
        /// конструктор
        /// </summary>
        public TestingSkinForm()
        {
            InitializeComponent();
            _showClose = true;
        }

        /// <summary>
        /// конструктор с убиранием кнопки закрытия
        /// </summary>
        /// <param name="showClose"></param>
        public TestingSkinForm(bool showClose)
            : this()
        {
            #region style initializing для simpleCoolForm

            //var xtl = new SimpleXmlThemeLoader();
            //StatusBar.BarItems.Add(new SimpleCoolForm.SimpleStatusBar.XBarItem(60));
            //StatusBar.BarItems.Add(new SimpleCoolForm.SimpleStatusBar.XBarItem(200, ""));
            //StatusBar.BarItems.Add(new SimpleCoolForm.SimpleStatusBar.XBarItem(80, ""));
            //StatusBar.EllipticalGlow = false;

            //TitleBar.TitleBarCaption = "SkinForm - проект \"Психолог\"";
            //TitleBar.TitleBarFill = SimpleTitleBar.SimpleTitleBarFill.AdvancedRendering;
            //TitleBar.TitleBarType = SimpleTitleBar.SimpleTitleBarType.Rounded;

            //TitleBar.TitleBarButtons[2].ButtonFillMode = SimpleTitleBarButton.SimpleButtonFillMode.FullFill;
            //TitleBar.TitleBarButtons[1].ButtonFillMode = SimpleTitleBarButton.SimpleButtonFillMode.FullFill;
            //TitleBar.TitleBarButtons[0].ButtonFillMode = SimpleTitleBarButton.SimpleButtonFillMode.FullFill;
            //MenuIcon = Testing.Properties.Resources.Health_care_shield_48x48.GetThumbnailImage(30, 30, null, IntPtr.Zero);

            //StatusBar.BarImageAlign = SimpleStatusBar.SimpleStatusBarBackImageAlign.Right;
            //xtl.ThemeForm = this;
            //xtl.ApplyTheme(System.Windows.Forms.Application.ExecutablePath + ".xml");
            ////xtl.ApplyTheme(Path.Combine(Application.StartupPath, @"..\..\Themes\BlueWinterTheme.xml"));
            ShowIcon = false;
            #endregion

            _showClose = showClose;
        }

        #region методы из сторонних длл

        [DllImport(DllUser32)]
        private static extern IntPtr GetSystemMenu(IntPtr hWnd, bool bRevert);

        [DllImport(DllUser32)]
        private static extern bool RemoveMenu(IntPtr hMenu, int uPosition, int uFlags);

        //На момент вызова этого метода у нашей формы  
        //уже сформирован ее Handle (HWND) 
        protected override void OnHandleCreated(EventArgs e)
        {
            base.OnHandleCreated(e);
            //Получаем Handle системного меню  
            IntPtr hMenu = GetSystemMenu(Handle, false);
            //и удаляем пункт "Close" 

            if (!_showClose)
                RemoveMenu(hMenu, SC_CLOSE, MF_BYCOMMAND);
        }

        #endregion
    }
}
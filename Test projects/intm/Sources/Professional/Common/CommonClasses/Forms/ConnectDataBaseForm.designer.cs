﻿

using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace Eureca.Professional.CommonClasses.Data
{
    partial class ConnectDataBaseForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.OKOKButton = new DevExpress.XtraEditors.SimpleButton();
            this.cancelcButton = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.workPanel)).BeginInit();
            this.workPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // workPanel
            // 
            this.workPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.workPanel.Appearance.ForeColor = System.Drawing.Color.Transparent;
            this.workPanel.Appearance.Options.UseForeColor = true;
            this.workPanel.Controls.Add(this.connDB1);
            this.workPanel.Dock = System.Windows.Forms.DockStyle.None;
            this.workPanel.LookAndFeel.SkinName = "Blue";
            this.workPanel.Size = new System.Drawing.Size(436, 422);
            // 
            // OKOKButton
            // 
            this.OKOKButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.OKOKButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.OKOKButton.Location = new System.Drawing.Point(248, 427);
            this.OKOKButton.Name = "OKOKButton";
            this.OKOKButton.Size = new System.Drawing.Size(75, 23);
            this.OKOKButton.TabIndex = 1;
            this.OKOKButton.Text = "OK";
            this.OKOKButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // cancelcButton
            // 
            this.cancelcButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelcButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelcButton.Location = new System.Drawing.Point(339, 427);
            this.cancelcButton.Name = "cancelcButton";
            this.cancelcButton.Size = new System.Drawing.Size(75, 23);
            this.cancelcButton.TabIndex = 2;
            this.cancelcButton.Text = "Отмена";
            this.cancelcButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // ConnectDataBaseForm
            // 
            this.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.Appearance.Options.UseForeColor = true;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(426, 450);
            this.Controls.Add(this.cancelcButton);
            this.Controls.Add(this.OKOKButton);
            this.LookAndFeel.SkinName = "Blue";
            this.MinimumSize = new System.Drawing.Size(442, 488);
            this.Name = "ConnectDataBaseForm";
            this.Text = "Настройки подключения";
            this.Controls.SetChildIndex(this.OKOKButton, 0);
            this.Controls.SetChildIndex(this.cancelcButton, 0);
            this.Controls.SetChildIndex(this.workPanel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.workPanel)).EndInit();
            this.workPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public ConnectDBLib.connDB connDB1;
        private SimpleButton OKOKButton;
        private SimpleButton cancelcButton;


    }
}
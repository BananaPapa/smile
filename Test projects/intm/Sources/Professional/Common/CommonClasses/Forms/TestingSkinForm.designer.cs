﻿namespace Eureca.Professional.CommonClasses
{
    partial class TestingSkinForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TestingSkinForm));
            this.workPanel = new DevExpress.XtraEditors.PanelControl();
            ((System.ComponentModel.ISupportInitialize)(this.workPanel)).BeginInit();
            this.SuspendLayout();
            // 
            // workPanel
            // 
            this.workPanel.Appearance.ForeColor = System.Drawing.Color.Transparent;
            this.workPanel.Appearance.Options.UseForeColor = true;
            this.workPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.workPanel.Location = new System.Drawing.Point(0, 0);
            this.workPanel.LookAndFeel.SkinName = "Blue";
            this.workPanel.Margin = new System.Windows.Forms.Padding(2);
            this.workPanel.Name = "workPanel";
            this.workPanel.Size = new System.Drawing.Size(518, 546);
            this.workPanel.TabIndex = 4;
            // 
            // TestingSkinForm
            // 
            this.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.Appearance.Options.UseForeColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(518, 546);
            this.Controls.Add(this.workPanel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "TestingSkinForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SkinForm";
            ((System.ComponentModel.ISupportInitialize)(this.workPanel)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public DevExpress.XtraEditors.PanelControl workPanel;


    }
}
﻿using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using log4net;

namespace Eureca.Professional.CommonClasses
{
    public delegate void WaitingControlEventHandler( );

    public partial class WaitingForm : XtraForm
    {
        private static readonly ILog Log = LogManager.GetLogger( typeof( WaitingForm ) );

        public event WaitingControlEventHandler WaitingCancel;


        public WaitingForm( string text )
        {
            InitializeComponent( );
            if (!string.IsNullOrEmpty( text ))
            {
                labelControlMessage.Text = text;
            }
        }

        public WaitingForm( string text, bool visibleProgress, bool showCancelButton )
            : this( text )
        {
            simpleButtonCancel.Visible = showCancelButton;
            marqueeProgressBarControl1.Visible = visibleProgress;
        }

        public void InvokeWaitingCancel( )
        {
            WaitingControlEventHandler handler = WaitingCancel;
            if (handler != null) handler( );
        }


        public void SetMessage( string text )
        {
            try
            {
                MethodInvoker method = delegate
                                           {
                                               {
                                                   try
                                                   {
                                                       labelControlMessage.Text = text;
                                                   }
                                                   catch (Exception ex)
                                                   {
                                                       Application.DoEvents( );
                                                       Log.Error( ex );
                                                   }
                                               }
                                               ;
                                           };
                Invoke( method );
            }
            catch (Exception ex)
            {
                Log.Error( ex );
            }
        }

        ///// <summary>
        ///// метод закрывает контрол
        ///// </summary>
        ///// <returns></returns>
        //public void Close()
        //{
        //    try
        //    {
        //        MethodInvoker method = delegate
        //                                   {
        //                                       {
        //                                           try
        //                                           {
        //                                              Close();
        //                                           }
        //                                           catch (Exception ex)
        //                                           {
        //                                               Log.Error(ex);
        //                                           }
        //                                       }
        //                                       ;
        //                                   };
        //        Invoke(method);
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //}

        private void SimpleButtonCancelClick( object sender, EventArgs e )
        {
            try
            {
                InvokeWaitingCancel( );
                CloseForm( );
            }
            catch (Exception ex) { Log.Error( ex ); }
        }
        public void CloseForm( )
        {
            MethodInvoker method = delegate
            {
                {
                    try
                    {
                        Close( );
                    }
                    catch (Exception ex)
                    {
                        Log.Error( ex );
                    }
                }
                ;
            };
            Invoke( method );
        }

        public void SetOkDialogResult( )
        {
            MethodInvoker method = delegate
            {
                {
                    try
                    {
                        DialogResult = DialogResult.OK;
                    }
                    catch (Exception ex)
                    {
                        Log.Error( ex );
                    }
                }
                ;
            };
            Invoke( method );
        }
    }
}
﻿using System;

namespace Eureca.Professional.CommonClasses.Data
{
    public partial class ConnectDataBaseForm : TestingSkinForm
    {
        //private readonly DatabaseSettingsSection _db;


        public ConnectDataBaseForm(string connectionString)
        {
            //_db = DatabaseSettingsSection.GetFromConfigSource();
            InitializeComponent();
            connDB1.Initialize(connectionString);

            #region style initializing

            #endregion
        }

        public string ConnectionString
        {
            get { return connDB1.ConnectionString; }
        }

        private void OKButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
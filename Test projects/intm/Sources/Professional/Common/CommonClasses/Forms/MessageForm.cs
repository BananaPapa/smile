﻿using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace Eureca.Professional.CommonClasses
{
    public partial class MessageForm : BaseTestingControl
    {
        public MessageForm()
        {
            InitializeComponent();
        }

        public static void ShowWarningMsg(string msg)
        {
            //XtraMessageBox.Show(msg, "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Warning,
            //                MessageBoxDefaultButton.Button1);
            var f = new TestingSkinForm();
            XtraMessageBox.Show(f.LookAndFeel, msg, "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Warning,
                                MessageBoxDefaultButton.Button1);
        }

        public static void ShowOkMsg(string msg)
        {
            //XtraMessageBox.Show(msg, "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Information,
            //                MessageBoxDefaultButton.Button1);
            var f = new TestingSkinForm();
            XtraMessageBox.Show(f.LookAndFeel, msg, "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Information,
                                MessageBoxDefaultButton.Button1);
        }

        public static void ShowErrorMsg(string msg)
        {
            //MessageBox.Show(msg, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            var f = new TestingSkinForm();
            XtraMessageBox.Show(f.LookAndFeel, msg, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error,
                                MessageBoxDefaultButton.Button1);
        }

        public static DialogResult ShowYesNoQuestionMsg(string msg)
        {
            var f = new TestingSkinForm();
            return XtraMessageBox.Show(f.LookAndFeel, msg, "Вопрос", MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                                       MessageBoxDefaultButton.Button1);
        }
    }
}
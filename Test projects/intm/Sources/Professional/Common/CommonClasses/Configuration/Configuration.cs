using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using EurecaCorp.SystemTools;
using log4net;


namespace Eureca.Professional.CommonClasses.Configuration
{
    public class ConfigurationSectionSaveEventArgs : EventArgs
    {
        private readonly ConfigurationSection _section;

        public ConfigurationSectionSaveEventArgs(ConfigurationSection section)
        {
            _section = section;
        }

        public ConfigurationSection Section
        {
            get { return _section; }
        }
    }

    [SuppressMessage("Microsoft.Design", "CA1003:UseGenericEventHandlerInstances")]
    public delegate void ConfigurationSectionSaveEventHandler(object sender, ConfigurationSectionSaveEventArgs e);

    [SuppressMessage("Microsoft.Design", "CA1053:StaticHolderTypesShouldNotHaveConstructors")]
    public class Settings
    {
        private static  readonly ILog Log = LogManager.GetLogger( typeof( Settings ) );
        [SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")] private const string
            _configFileName = "Application.config";

        [SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")] private const string
            _configRootRelativePath = @"\Configuration";

        private static readonly string _appConfigFileFullPath;
        private static readonly string _configFileFullPath;
        private static readonly Dictionary<Type, ConfigurationSectionSaveEventHandler> _configSaveHandlers;
        private static System.Configuration.Configuration _configSource;

        static Settings()
        {
            _appConfigFileFullPath = Application.ExecutablePath + ".config";

            _configFileFullPath = Application.StartupPath + "\\"
                                  + _configRootRelativePath + "\\" + _configFileName;

            _configSaveHandlers = new Dictionary<Type, ConfigurationSectionSaveEventHandler>();

            CreateConfigFilesIfNotExist();

            RecreateConfigurationSource();
        }

        public static string ConfigurationFileFullName
        {
            get { return _configFileFullPath; }
        }

        private static void RecreateConfigurationSource()
        {
            var map = new ExeConfigurationFileMap();
            map.ExeConfigFilename = _configFileFullPath;

            _configSource = ConfigurationManager.OpenMappedExeConfiguration(map, ConfigurationUserLevel.None);
        }

        public static void CreateNetConfigFile(string fileName)
        {
            string configDirectory = Path.GetDirectoryName(fileName);
            if (!Directory.Exists(configDirectory))
            {
                Directory.CreateDirectory(configDirectory);
            }

            using (FileStream fs = File.Create(fileName))
            {
                var xmlSettings = new XmlWriterSettings();
                xmlSettings.CloseOutput = true;
                xmlSettings.Encoding = Encoding.UTF8;
                xmlSettings.OmitXmlDeclaration = true;

                XmlWriter xmlWriter = XmlWriter.Create(fs, xmlSettings);
                xmlWriter.WriteStartElement("configuration");
                xmlWriter.Close();
            }
        }

        public static void CreateConfigFilesIfNotExist()
        {
            if (!File.Exists(_configFileFullPath))
                CreateNetConfigFile(_configFileFullPath);
        }

        public static void DeleteAndRecreateConfigFile()
        {
            File.Delete(_configFileFullPath);
            CreateNetConfigFile(_configFileFullPath);
        }

        public static void AddSectionSaveEventHandler(Type sectionType, ConfigurationSectionSaveEventHandler handler)
        {
            _configSaveHandlers.Add(sectionType, handler);
        }

        public static void Reset()
        {
            RecreateConfigurationSource();
        }

        [SuppressMessage("Microsoft.Usage", "CA2208:InstantiateArgumentExceptionsCorrectly")]
        public static ConfigurationSection GetSection(Type sectionType)
        {
            if (!ObjectExtention.TypesEqualOrSubclass(sectionType, typeof (ConfigurationSection)))
                throw new ArgumentOutOfRangeException("Type of sectionType must inherit from NamedConfigurationSection");

            ConfigurationSectionParamsAttribute attr = ConfigurationSectionParamsAttribute.Get(sectionType);

            ConfigurationSection section = null;

            try
            {
                section = _configSource.GetSection(attr.SectionName);
            }
            catch (ConfigurationErrorsException e)
            {
                Log.Error(e);
                section = null;
                File.Delete(_configSource.FilePath);
                CreateNetConfigFile(_configSource.FilePath);
                RecreateConfigurationSource();
            }

            if (null == section && attr.Mandatory)
            {
                section = (ConfigurationSection) Activator.CreateInstance(sectionType);
                _configSource.Sections.Add(attr.SectionName, section);
                _configSource.Save();
            }

            return section;
        }

        public static void Save(ConfigurationSection section)
        {
            try
            {
                _configSource.Save();
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }

        public static void Save()
        {
            try
            {
                _configSource.Save();

                foreach (Type sectionType in _configSaveHandlers.Keys)
                {
                    (_configSaveHandlers[sectionType])(_configSource,
                                                       new ConfigurationSectionSaveEventArgs(GetSection(sectionType)));
                }
            }
            catch (Exception e)
            {
                Log.Error(e);
            }
        }

        [SuppressMessage("Microsoft.Usage", "CA2208:InstantiateArgumentExceptionsCorrectly")]
        public static void AddSection(ConfigurationSection section)
        {
            if (null == section) throw new ArgumentNullException();
            _configSource.Sections.Add(ConfigurationSectionParamsAttribute.Get(section.GetType()).SectionName, section);
        }
    }

    [SuppressMessage("Microsoft.Naming", "CA1716:IdentifiersShouldNotMatchKeywords")]
    public class Global
    {
        public const string ConfigFileRelativePath = @"Configuration\Tmc.config";
    }

    /// <summary>
    /// �� ������� ������ RSDN: ���������������� .NET-����������.
    /// </summary>
    public class ConfigSectionTypeDescriptor : ICustomTypeDescriptor
    {
        private readonly object _obj;
        private readonly PropertyDescriptorCollection _propsCollection;

        public ConfigSectionTypeDescriptor(object obj)
        {
            _obj = obj;

            _propsCollection = new PropertyDescriptorCollection(null);
            PropertyDescriptorCollection pdc = TypeDescriptor.GetProperties(obj, true);

            foreach (PropertyDescriptor pd in pdc)
            {
                if (null != pd.Attributes[typeof (DisplayNameAttribute)])
                    _propsCollection.Add(new ConfigSectionPropertyDescriptor(pd));

                _propsCollection = _propsCollection.Sort(new ConfigurationSectionPropertyComparer());
            }
        }

        #region ICustomTypeDescriptor implementation

        AttributeCollection ICustomTypeDescriptor.GetAttributes()
        {
            return new AttributeCollection(null);
        }

        string ICustomTypeDescriptor.GetClassName()
        {
            return null;
        }

        string ICustomTypeDescriptor.GetComponentName()
        {
            return null;
        }

        TypeConverter ICustomTypeDescriptor.GetConverter()
        {
            return null;
        }

        EventDescriptor ICustomTypeDescriptor.GetDefaultEvent()
        {
            return null;
        }


        PropertyDescriptor ICustomTypeDescriptor.GetDefaultProperty()
        {
            return null;
        }

        object ICustomTypeDescriptor.GetEditor(Type editorBaseType)
        {
            return null;
        }

        EventDescriptorCollection ICustomTypeDescriptor.GetEvents()
        {
            return new EventDescriptorCollection(null);
        }

        EventDescriptorCollection ICustomTypeDescriptor.GetEvents
            (Attribute[] attributes)
        {
            return new EventDescriptorCollection(null);
        }

        PropertyDescriptorCollection ICustomTypeDescriptor.GetProperties()
        {
            return _propsCollection;
        }

        PropertyDescriptorCollection ICustomTypeDescriptor.GetProperties(
            Attribute[] attributes)
        {
            return _propsCollection;
        }

        object ICustomTypeDescriptor.GetPropertyOwner(PropertyDescriptor pd)
        {
            return this;
        }

        #endregion

        public object Unwrap
        {
            get { return _obj; }
        }
    }

    public class ConfigSectionPropertyDescriptor : PropertyDescriptor
    {
        private readonly PropertyDescriptor _propDesc;

        public ConfigSectionPropertyDescriptor(PropertyDescriptor propDesc) : base(propDesc)
        {
            _propDesc = propDesc;
        }

        public PropertyDescriptor PropertyDescriptor
        {
            get { return _propDesc; }
        }

        public override string Category
        {
            get { return _propDesc.Category; }
        }

        // ��� �������� ���������� �������� ��������, 
        // ������������ � propertyGrid
        public override string DisplayName
        {
            get
            {
                // �������� �������� ������� DisplayNameAttribute.
                // � ������ ������� ����� ��������� null.
                var mna =
                    _propDesc.Attributes[typeof (DisplayNameAttribute)] as DisplayNameAttribute;

                if (mna != null)
                    return mna.ToString();

                return _propDesc.Name;
            }
        }

        public override Type ComponentType
        {
            get { return _propDesc.ComponentType; }
        }

        public override bool IsReadOnly
        {
            get { return false; }
        }

        public override Type PropertyType
        {
            get { return _propDesc.PropertyType; }
        }

        [SuppressMessage("Microsoft.Design", "CA1062:ValidateArgumentsOfPublicMethods")]
        public override bool CanResetValue(object component)
        {
            return _propDesc.CanResetValue(((ConfigSectionTypeDescriptor) component).Unwrap);
        }

        [SuppressMessage("Microsoft.Design", "CA1062:ValidateArgumentsOfPublicMethods")]
        public override object GetValue(object component)
        {
            return _propDesc.GetValue(((ConfigSectionTypeDescriptor) component).Unwrap);
        }

        [SuppressMessage("Microsoft.Design", "CA1062:ValidateArgumentsOfPublicMethods")]
        public override void ResetValue(object component)
        {
            _propDesc.ResetValue(((ConfigSectionTypeDescriptor) component).Unwrap);
        }

        [SuppressMessage("Microsoft.Design", "CA1062:ValidateArgumentsOfPublicMethods")]
        public override void SetValue(object component, object value)
        {
            _propDesc.SetValue(((ConfigSectionTypeDescriptor) component).Unwrap, value);
        }

        [SuppressMessage("Microsoft.Design", "CA1062:ValidateArgumentsOfPublicMethods")]
        public override bool ShouldSerializeValue(object component)
        {
            return _propDesc.ShouldSerializeValue(((ConfigSectionTypeDescriptor) component).Unwrap);
        }
    }

    [SuppressMessage("Microsoft.Design", "CA1019:DefineAccessorsForAttributeArguments"),
     SuppressMessage("Microsoft.Performance", "CA1813:AvoidUnsealedAttributes"),
     AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    [Serializable]
    public class DisplayNameAttribute : Attribute
    {
        private readonly string _text;

        public DisplayNameAttribute(string text)
        {
            _text = text;
        }

        public override string ToString()
        {
            return _text;
        }
    }

    public interface INamedConfigurationSection
    {
        string SectionName { get; }
    }

    public class ConfigurationSectionPropertyComparer : IComparer
    {
        #region IComparer Members

        [SuppressMessage("Microsoft.Design", "CA1062:ValidateArgumentsOfPublicMethods"),
         SuppressMessage("Microsoft.Usage", "CA2208:InstantiateArgumentExceptionsCorrectly")]
        public int Compare(Object x, Object y)
        {
            var xIndexAttr = (PropertyIndexAttribute)
                             ((ConfigSectionPropertyDescriptor) x).Attributes[typeof (PropertyIndexAttribute)];

            var yIndexAttr = (PropertyIndexAttribute)
                             ((ConfigSectionPropertyDescriptor) y).Attributes[typeof (PropertyIndexAttribute)];

            if (null == xIndexAttr || null == xIndexAttr)
                throw new ArgumentOutOfRangeException("Both compared objects must have PropertyIndexAttribute applied.");

            return xIndexAttr.Index - yIndexAttr.Index;
        }

        #endregion
    }

    [SuppressMessage("Microsoft.Performance", "CA1813:AvoidUnsealedAttributes"),
     AttributeUsage(AttributeTargets.Property)]
    public class PropertyIndexAttribute : Attribute
    {
        private readonly int _index;

        public PropertyIndexAttribute(int index)
        {
            _index = index;
        }

        public int Index
        {
            get { return _index; }
        }

        [SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters")]
        public static PropertyIndexAttribute Get(Type type)
        {
            return (PropertyIndexAttribute) GetCustomAttribute(type,
                                                               typeof (PropertyIndexAttribute));
        }
    }

    public class ConfigurationSectionParamsAttribute : Attribute
    {
        [SuppressMessage("Microsoft.Design", "CA1051:DoNotDeclareVisibleInstanceFields")]
        public bool Mandatory { get; set; }
        [SuppressMessage("Microsoft.Design", "CA1051:DoNotDeclareVisibleInstanceFields")]
        public string SectionName { get; set; }

        public ConfigurationSectionParamsAttribute(string sectionName, bool mandatory)
        {
            SectionName = sectionName;
            Mandatory = mandatory;
        }

        [SuppressMessage("Microsoft.Usage", "CA2201:DoNotRaiseReservedExceptionTypes"),
         SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters")]
        public static ConfigurationSectionParamsAttribute Get(Type type)
        {
            var attr =
                (ConfigurationSectionParamsAttribute)
                GetCustomAttribute(type, typeof (ConfigurationSectionParamsAttribute));

            if (null == attr)
                throw new Exception(
                    "Failed to find configuration section params. ConfigurationSectionParamsAttribute attribute is missing.");

            return attr;
        }
    }
}
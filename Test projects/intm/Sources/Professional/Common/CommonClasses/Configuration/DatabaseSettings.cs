using System.ComponentModel;
using System.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;

namespace Eureca.Professional.CommonClasses.Configuration
{
    [ConfigurationSectionParams("DatabaseSettingsSection", true)]
    public class DatabaseSettingsSection : SerializableConfigurationSection
    {
        //[ConfigurationProperty("Server"),
        // DisplayName("��� �������"),
        // Description("������ ��� ��� IP-����� ������� ��."),
        // Category("1. ��������"),
        // PropertyIndex(0)
        //]
        //private string Server
        //{
        //    get { return (string) this["Server"]; }
        //    set { this["Server"] = value; }
        //}

        //[ConfigurationProperty("Database"),
        // DisplayName("��� ���� ������"),
        // Description("��� ������������ ���� ������ �� ��������� �������."),
        // Category("1. ��������"),
        // PropertyIndex(1)
        //]
        //private string Database
        //{
        //    get { return (string) this["Database"]; }
        //    set { this["Database"] = value; }
        //}

        //[ConfigurationProperty("ConnectionTimeout"),
        // DisplayName("�������"),
        // Description("������� �����������."),
        // Category("1. ��������"),
        // PropertyIndex(2)
        //]
        //private int ConnectionTimeout
        //{
        //    get { return (int) this["ConnectionTimeout"]; }
        //    set { this["ConnectionTimeout"] = value; }
        //}

        //[ConfigurationProperty("UserName"),
        // DisplayName("�����"),
        // Description("��� ������������ ��� ����� �� ��������� ������."),
        // Category("2. ������������ � ������ (�������� ����������)"),
        // PropertyIndex(0)
        //]
        //private string UserName
        //{
        //    get { return (string) this["UserName"]; }
        //    set { this["UserName"] = value; }
        //}

        //[ConfigurationProperty("Password"),
        // DisplayName("������"),
        // Description("������ ��� ������� � ��������� ���� ������."),
        // Category("2. ������������ � ������ (�������� ����������)"),
        // PropertyIndex(1)
        //]
        //private string Password
        //{
        //    get { return (string) this["Password"]; }
        //    set { this["Password"] = value; }
        //}

        [ConfigurationProperty("ConnectionString"),
         DisplayName("������ �����������"),
         Description("������ �����������"),
         Category("1. ��������"),
         PropertyIndex(1)
        ]
        public string ConnectionString
        {
            get { return (string) this["ConnectionString"]; }
            set { this["ConnectionString"] = value; }
        }


        [ConfigurationProperty("CurriculumConnectionString"),
         DisplayName("������ ����������� � ��� ������������"),
         Description("������ ����������� � ��� ������������"),
         Category("1. ��������"),
         PropertyIndex(1)
        ]
        public string CurriculumConnectionString
        {
            get { return (string) this["CurriculumConnectionString"]; }
            set { this["CurriculumConnectionString"] = value; }
        }


        public static DatabaseSettingsSection GetFromConfigSource()
        {
            var section = (DatabaseSettingsSection) Settings.GetSection(typeof (DatabaseSettingsSection));

            return section;
        }
    }
}
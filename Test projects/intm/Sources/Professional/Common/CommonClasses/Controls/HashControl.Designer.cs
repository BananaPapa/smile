﻿namespace Eureca.Professional.CommonClasses
{
    partial class HashControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.textEditSerialNumber = new DevExpress.XtraEditors.TextEdit();
            this.textEditKey = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButtonOk = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.textEditSerialNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditKey.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(17, 16);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(84, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Серийный номер";
            // 
            // textEditSerialNumber
            // 
            this.textEditSerialNumber.Location = new System.Drawing.Point(17, 36);
            this.textEditSerialNumber.Name = "textEditSerialNumber";
            this.textEditSerialNumber.Properties.ReadOnly = true;
            this.textEditSerialNumber.Size = new System.Drawing.Size(165, 20);
            this.textEditSerialNumber.TabIndex = 1;
            // 
            // textEditKey
            // 
            this.textEditKey.Location = new System.Drawing.Point(17, 88);
            this.textEditKey.Name = "textEditKey";
            this.textEditKey.Size = new System.Drawing.Size(165, 20);
            this.textEditKey.TabIndex = 3;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(17, 68);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(85, 13);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "Ключ активации";
            // 
            // simpleButtonOk
            // 
            this.simpleButtonOk.Location = new System.Drawing.Point(107, 126);
            this.simpleButtonOk.Name = "simpleButtonOk";
            this.simpleButtonOk.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonOk.TabIndex = 4;
            this.simpleButtonOk.Text = "Ок";
            this.simpleButtonOk.Click += new System.EventHandler(this.simpleButtonOk_Click);
            // 
            // HashControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.simpleButtonOk);
            this.Controls.Add(this.textEditKey);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.textEditSerialNumber);
            this.Controls.Add(this.labelControl1);
            this.Name = "HashControl";
            this.Size = new System.Drawing.Size(203, 169);
            ((System.ComponentModel.ISupportInitialize)(this.textEditSerialNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditKey.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit textEditSerialNumber;
        private DevExpress.XtraEditors.TextEdit textEditKey;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.SimpleButton simpleButtonOk;
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.IO;
using System.Linq;
using System.Management;
using System.Text;
using System.Windows.Forms;

namespace Eureca.Professional.CommonClasses
{
    public partial class HashControl : BaseTestingControl
    {
        public HashControl()
        {
            InitializeComponent();
        }

        public string Hash { get; private set; }

        public override void InitControl()
        {
            base.InitControl();
            CaptionControl = "Ключ активации";
            InitData();
        }

        private void InitData()
        {
            string drive = Environment.GetFolderPath(Environment.SpecialFolder.System).Substring(0, 1);
            ManagementObject disk = new ManagementObject("win32_logicaldisk.deviceid=\"" + drive + ":\"");
            disk.Get();
            string diskLetter = (disk["VolumeSerialNumber"].ToString());

            textEditSerialNumber.Text = diskLetter;
            
            
        }

        private void simpleButtonOk_Click(object sender, EventArgs e)
        {
            var serial = textEditSerialNumber.Text;
            if (AppActivation.Crypt(serial) == textEditKey.Text)
            {
                Hash = textEditKey.Text;
                if (ParentForm != null)
                    ParentForm.DialogResult = DialogResult.OK;
            }
            else
            {
                MessageForm.ShowErrorMsg("Некорректный ключ");
            }
        }

       
    }
}

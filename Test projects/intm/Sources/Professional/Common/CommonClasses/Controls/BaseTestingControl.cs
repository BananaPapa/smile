﻿using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace Eureca.Professional.CommonClasses
{
    /// <summary>
    /// базовый класс для контролов
    /// содержит общие элементы и методы
    /// </summary>
    public partial class BaseTestingControl : XtraUserControl
    {
        #region поля

        /// <summary>
        /// плюсовая высота к контролу
        /// </summary>
        private const int PlusHeight = 30;

        /// <summary>
        /// переменная инициализации контрола
        /// </summary>
        protected bool Initialize;

        ///// <summary>
        ///// сессия для соединения с БД для ActiveRecords
        ///// </summary>
        //protected static ISessionScope _iSessionScope;

        /// <summary>
        /// наименование контрола в шапке
        /// </summary>
        private string _captionControl = "Основной контрол тестирования";

        /// <summary>
        /// выходной запрос выдается при выходе из контрола
        /// </summary>
        private string _exitQuestion;

        #endregion

        #region свойства

        public string ExitQuestion
        {
            get { return _exitQuestion; }
            set { _exitQuestion = value; }
        }

        public string CaptionControl
        {
            get { return _captionControl; }
            set { _captionControl = value; }
        }

        public IButtonControl AcceptButton { get; set; }
        public IButtonControl CancelButton { get; set; }

        #endregion

        #region конструктор

        public BaseTestingControl()
        {
            InitializeComponent();
        }

        #endregion

        #region Статические методы

        /// <summary>
        /// метод показа любого контрола в форме
        /// </summary>
        /// <param name="control"></param>
        /// <param name="showClose"></param>
        /// <param name="canResize"></param>
        /// <param name="caption"></param>
        /// <returns></returns>
        public static DialogResult ShowInForm(Control control, bool showClose, bool canResize, string caption)
        {
            //создаю форму просмотра
            var form = new TestingSkinForm(showClose)
                           {
                               DialogResult = DialogResult.Cancel,
                               MinimumSize =
                                   new Size(control.MinimumSize.Width, control.MinimumSize.Height + PlusHeight)
                           };
            if (canResize == false)
            {
                form.MaximumSize =
                    form.MinimumSize = new Size(control.Size.Width + 6, control.Size.Height + PlusHeight);
                form.MaximizeBox = false;
            }

            form.Size = new Size(control.Size.Width + 6, control.Size.Height + PlusHeight);


            form.workPanel.Controls.Clear();
            form.workPanel.Controls.Add(control);
            control.Dock = DockStyle.Fill;
            form.Text = caption + " - \"Профессионал\"";

            return form.ShowDialog();
        }

        #endregion

        #region виртуальные(перегружаемые в дочерник классах методы)

        /// <summary> метод инициализации контрола </summary>
        public virtual void InitControl()
        {
            Initialize = true;
            Dock = DockStyle.Fill;
        }

        /// <summary> метод уничтожения контрола(перегружается в дочерних контролах) </summary>
        public virtual void DisposeControl()
        {
            //для наследования в дочерних контролах
        }

        /// <summary> перегружаемое событие щакрытие контрола </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void ParentFormFormClosing(object sender, FormClosingEventArgs e)
        {
            if (!string.IsNullOrEmpty(_exitQuestion))
            {
                if (XtraMessageBox.Show(_exitQuestion, "Предупреждение", MessageBoxButtons.YesNo)
                    == DialogResult.No)
                {
                    e.Cancel = true;
                    return;
                }
            }
            _disposing = true;
            DisposeControl();
        }

        /// <summary> метод возвращает контрол, который мы хотим отобразить </summary>
        /// <returns></returns>
        public Control ShowAsControl()
        {
            if (!Initialize)
            {
                XtraMessageBox.Show(string.Format("Контрол {0} не инициализирован", Name), "Предупреждение");
            }
            return this;
        }

        /// <summary>
        /// метод показа контрола в форме
        /// </summary>
        /// <param name="showClose"></param>
        /// <param name="canResize"></param>
        /// <param name="question"></param>
        /// <param name="formWindowState"></param>
        /// <returns></returns>
        public virtual DialogResult ShowInForm(bool showClose, bool canResize, string question,
                                               FormWindowState formWindowState)
        {
            _exitQuestion = question;
            //создаю форму просмотра
            var form = new TestingSkinForm(showClose)
                           {DialogResult = DialogResult.Cancel};
            if (canResize == false)
            {
                form.MaximumSize = form.MinimumSize = new Size(Size.Width + 6, Size.Height + PlusHeight);
                form.MaximizeBox = false;
            }

            form.MinimumSize = new Size(MinimumSize.Width + 6, MinimumSize.Height + PlusHeight);
            form.Size = new Size(Size.Width + 6, Size.Height + PlusHeight);


            form.workPanel.Controls.Clear();
            form.workPanel.Controls.Add(this);
            Dock = DockStyle.Fill;
            form.Text = CaptionControl + " - \"Профессионал\"";
            form.AcceptButton = AcceptButton;
            form.CancelButton = CancelButton;
            form.WindowState = formWindowState;

            //подписка на события
            if (ParentForm != null)
                ParentForm.FormClosing += ParentFormFormClosing;


            //проверка на инициализацию
            if (!Initialize)
            {
                XtraMessageBox.Show("Контрол не инициализирован", "Предупреждение");
            }
            //вывод формы
            return form.ShowDialog();
        }


        public virtual DialogResult ShowInForm(bool showClose, bool canResize, string question)
        {
            return ShowInForm(showClose, canResize, question, FormWindowState.Normal);
        }

        /// <summary>
        /// усеченный метод показа контрола в форме
        /// </summary>
        /// <returns></returns>
        public virtual DialogResult ShowInForm()
        {
            return ShowInForm(true, true, string.Empty, FormWindowState.Normal);
        }

        /// <summary>
        /// усеченный метод показа контрола в форме
        /// </summary>
        /// <param name="canResize"></param>
        /// <returns></returns>
        public virtual DialogResult ShowInForm(bool canResize)
        {
            return ShowInForm(canResize, FormWindowState.Normal);
        }

        /// <summary>
        /// показ формы во весь экран
        /// </summary>
        /// <param name="canResize"></param>
        /// <returns></returns>
        public virtual DialogResult ShowInForm(bool canResize, FormWindowState formWinwowState)
        {
            return ShowInForm(true, canResize, null, formWinwowState);
        }


        /// <summary>
        /// показ контрола без формы в немодальном режиме 
        /// </summary>
        /// <param name="showClose"></param>
        /// <param name="canResize"></param>
        public virtual void ShowWithoutForm(bool showClose, bool canResize)
        {
            //создаю форму просмотра
            var form = new TestingSkinForm(showClose);
            if (canResize == false)
            {
                form.MaximumSize = form.MinimumSize = new Size(Size.Width + 6, Size.Height + PlusHeight);
                form.MaximizeBox = false;
            }
            form.FormBorderStyle = FormBorderStyle.None;
            //form.TopMost = true;

            form.MinimumSize = new Size(MinimumSize.Width + 6, MinimumSize.Height + PlusHeight);
            form.Size = new Size(Size.Width + 6, Size.Height + PlusHeight);

            form.workPanel.Controls.Clear();
            form.workPanel.Controls.Add(this);
            Dock = DockStyle.Fill;
            form.Text = CaptionControl + " - \"Профессионал\"";


            //подписка на события
            if (ParentForm != null)
                ParentForm.FormClosing += ParentFormFormClosing;


            //проверка на инициализацию
            if (!Initialize)
            {
                XtraMessageBox.Show("Контрол не инициализирован", "Предупреждение");
            }
            //вывод формы
            form.TopLevel = true;
            form.Show();
        }

        /// <summary>
        /// показ контрола в модальном режиме
        /// </summary>
        /// <param name="showClose"></param>
        /// <param name="canResize"></param>
        /// <returns></returns>
        public virtual DialogResult ShowWithoutFormDialog(bool showClose, bool canResize)
        {
            //создаю форму просмотра
            var form = new TestingSkinForm(showClose);
            if (canResize == false)
            {
                form.MaximumSize = form.MinimumSize = new Size(Size.Width + 6, Size.Height + PlusHeight);
                form.MaximizeBox = false;
            }
            form.FormBorderStyle = FormBorderStyle.None;
            form.TopMost = true;

            form.Size = new Size(Size.Width + 6, Size.Height + PlusHeight);
            form.MinimumSize = new Size(MinimumSize.Width + 6, MinimumSize.Height + PlusHeight);

            form.workPanel.Controls.Clear();
            form.workPanel.Controls.Add(this);
            Dock = DockStyle.Fill;
            form.Text = CaptionControl + " - \"Профессионал\"";


            //подписка на события
            if (ParentForm != null)
                ParentForm.FormClosing += ParentFormFormClosing;


            //проверка на инициализацию
            if (!Initialize)
            {
                XtraMessageBox.Show("Контрол не инициализирован", "Предупреждение");
            }
            //вывод формы
            form.TopLevel = true;
            return form.ShowDialog();
        }

        #endregion
    }
}
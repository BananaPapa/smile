﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Management;
using System.Text;
using System.Windows.Forms;

namespace Eureca.Professional.CommonClasses
{
    public class AppActivation
    {
        public static string Crypt(string text)
        {
            string rtnStr = string.Empty;
            foreach (char c in text) // Цикл, которым мы и криптуем "текст"
            {
                rtnStr += (char)((int)c ^ 1); //Число можно взять любое.
            }
            return rtnStr; //Возвращаем уже закриптованную строку. 
        }
        public static string GetSerialNumber()
        {
            string drive = Environment.GetFolderPath(Environment.SpecialFolder.System).Substring(0, 1);
            var disk = new ManagementObject("win32_logicaldisk.deviceid=\"" + drive + ":\"");
            disk.Get();
            string diskLetter = (disk["VolumeSerialNumber"].ToString());
            return diskLetter;
        }
        public static bool IsActivation(string path)
        {
            //var hashFilePath = Path.Combine(path, "hash");
            //if (!File.Exists(hashFilePath) || Crypt(GetSerialNumber()) != File.ReadAllText(hashFilePath))
            //{
            //    return false;
            //}
            return true;
        }

        public static bool Activation(string path)
        {
            var hashFilePath = Path.Combine(path, "hash");

            if(!File.Exists(hashFilePath) || Crypt(GetSerialNumber()) != File.ReadAllText(hashFilePath))
            {
                var uc = new HashControl();
                uc.InitControl();
                if(uc.ShowInForm(false, FormWindowState.Normal) == DialogResult.OK)
                {
                    var hash = uc.Hash;
                    if (Crypt(GetSerialNumber()) == hash)
                    {
                        File.WriteAllText(hashFilePath, hash);
                        return true;
                    }
                    return false;
                }
                else
                {
                    return false;
                }
            }
            return true;
        }
    }
}

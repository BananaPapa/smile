﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using Eureca.Professional.CommonClasses.Configuration;
using log4net;

namespace Eureca.Professional.CommonClasses.Data
{
    public class DBExecutor
    {
        private static readonly ILog Log = LogManager.GetLogger( typeof(DBExecutor) );
        protected static SqlConnection Conection;
        /// <summary>
        /// транзакция
        /// </summary>
        protected static SqlTransaction Transaction;

        public static bool Exist(string connectionString)
        {
            var connect = new SqlConnection(connectionString);
            try
            {
                connect.Open();
                if (connect.State == ConnectionState.Open)
                {
                    connect.Close();
                    connect.Dispose();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return false;
            }
        }

        public static string GetConnectionString()
        {
            return DatabaseSettingsSection.GetFromConfigSource().ConnectionString;
        }
        public static void SetConnectionString(string connectionString)
        {
            DatabaseSettingsSection db = DatabaseSettingsSection.GetFromConfigSource();
            db.ConnectionString = connectionString;
            Settings.Save();
        }

        
        /// <summary>
        /// выборка из БД в таблицу
        /// </summary>
        /// <param name="aSelectStr"></param>
        /// <returns></returns>
        public static DataTable SelectQuerry(String aSelectStr)
        {
            var returnsDataTable = new DataTable();
            if (Conection == null || Conection.State != ConnectionState.Open)
            {
                //открываем новое подключение
                Conection = new SqlConnection(GetConnectionString());
                Conection.Open();
            }

            var adapter = new SqlDataAdapter(aSelectStr, Conection);

            String str = adapter.SelectCommand.CommandText;
            try
            {
                if (Transaction != null)
                    adapter.SelectCommand.Transaction = Transaction;
                adapter.SelectCommand.CommandTimeout = 500;
                adapter.Fill(returnsDataTable);
                //закрываем соединение
                if (Transaction == null)
                {
                    Conection.Close();
                    Conection = null;
                }
            }
            catch (Exception ex)
            {
                Log.Error("Try to call: " + str);
                Log.Error(ex);
                throw;
            }
            return returnsDataTable;
        }
        /// <summary>
        /// метод выполнение запроса
        /// </summary>
        /// <param name="aStringToExecute">строка с запросом</param>
        /// <returns>число затрагиваемых строк</returns>
        public static int ExecNonQuery(String aStringToExecute)
        {
            int ret;

            if (Conection == null || Conection.State != ConnectionState.Open)
            {
                //открываем новое подключение
                Conection = new SqlConnection(GetConnectionString());
                Conection.Open();
            }
            var cmd = new SqlCommand(aStringToExecute, Conection);
            try
            {
                if (Transaction != null)
                    cmd.Transaction = Transaction;

                cmd.CommandTimeout = 500;
                ret = cmd.ExecuteNonQuery();
                //закрываем соединение
                if (Transaction == null)
                {
                    Conection.Close();
                    Conection = null;
                }
            }
            catch (Exception ex)
            {
                if (Conection.State != ConnectionState.Closed)
                {
                    Conection.Close();
                }
                Log.Error("Try to call: " + aStringToExecute);
                Log.Error(ex);
                throw ex;
            }

            return ret;
        }
        /// <summary>
        /// Выполнение запроса и возвращение значения
        /// </summary>
        /// <param name="aStringToExecute"></param>
        /// <returns></returns>
        public static object ExecScalar(String aStringToExecute)
        {
            object ret;
            var cmd = new SqlCommand(aStringToExecute, Conection);
            if (Conection == null || Conection.State != ConnectionState.Open)
            {
                //открываем новое подключение
                Conection = new SqlConnection(GetConnectionString());
                Conection.Open();
            }
            try
            {
            if (Transaction != null)
            cmd.Transaction = Transaction;
            cmd.CommandTimeout = 500;
            ret = cmd.ExecuteScalar();

            if (Transaction == null)
            {
            Conection.Close();
            Conection = null;
            }
            }
            catch (Exception ex)
            {
            Log.Error("Try to call: " + aStringToExecute);
            Log.Error(ex);
            throw;
            }
            return ret;
        }

        
        //методы для работы с транзакцией
        public static void BeginTransaction()
        {
            if (Transaction != null)
                throw new Exception("Необходимо завершить предыдущую транзакцию");
            if (Conection == null || Conection.State != ConnectionState.Open)
            {
                //открываем новое подключение
                Conection = new SqlConnection(GetConnectionString());
                Conection.Open();
            }
            Transaction = Conection.BeginTransaction(IsolationLevel.RepeatableRead);
        }
        public static void CommitTransaction()
        {
            if (Transaction == null)
                throw new Exception("Отсутствуют транзакции для подтверждения");

            Transaction.Commit();
            Transaction.Dispose();
            Transaction = null;

            Conection.Close();
            Conection = null;
        }
        public static void RollbackTransaction()
        {
            if (Transaction == null)
                throw new Exception("Отсутствуют транзакции для отката");

            Transaction.Rollback();
            Transaction.Dispose();
            Transaction = null;

            Conection.Close();
            Conection = null;
        }
        public static void RollbackTransaction(String savepointName)
        {
            Transaction.Rollback(savepointName);
        }
        public static void CreateSavepoint(String savepointName)
        {
            Transaction.Save(savepointName);
        }


        /// <summary>
        /// метод конвертации данных в формат БД
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string SqlConvert(object value)
        {
            if (value == null || value == DBNull.Value)
            {
                value = "NULL";
            }
            else
            {
                Type type = value.GetType();

                if (type.Equals(typeof (string)))
                {
                    value = string.Format("'{0}'", ((string) value).Replace("'", "''"));
                }
                else if (type.Equals(typeof (Boolean)))
                {
                    var boolValue = (bool) value;

                    value = boolValue ? "1" : "0";
                }
                else if (type.Equals(typeof (Double)) || type.Equals(typeof (Single)) ||
                         type.Equals(typeof (Decimal)))
                {
                    return value.ToString().Replace(',', '.');
                }
                else if (type.Equals(typeof (DateTime)))
                {
                    return SqlConvert(((DateTime) value).ToString("yyyyMMdd HH:mm:ss:fff"));
                }
                else if (type.Equals(typeof (Guid)))
                {
                    return SqlConvert(((Guid) value).ToString());
                }
            }

            return value.ToString();
        }
    }
}
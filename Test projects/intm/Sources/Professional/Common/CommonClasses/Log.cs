//using System;
//using System.Collections;
//using System.Diagnostics;
//using System.IO;
//using System.Threading;
//using System.Windows.Forms;

//namespace Eureca.Professional.CommonClasses
//{
//    /// <summary>
//    /// ������� �� ������� � ���
//    /// </summary>
//    /// <param name="sender"></param>
//    /// <param name="e"></param>
//    public delegate void LogDelegate(object sender, LogEventArgs e);


//    /// <summary>
//    /// ����� Log ������� ���������� � ���� ����
//    /// </summary>
//    public class Log
//    {
//        /// <summary>
//        /// ������������ ������ ����� ���� � ������
//        /// </summary>
//        private const int MAX_LOG_SIZE = 1000000;

//        /// <summary>
//        /// ���� ������ �������
//        /// </summary>
//        private static bool _isDebugging = true;

//        /// <summary>
//        /// 
//        /// </summary>
//        private static Log _log;

//        private static String _pathString = "log.001";
//        private static String _workDir = "";
//        private Queue _logQueue;
//        private Thread _logThread;
//        private bool usualCall;

//        private Log()
//        {
//        }

//        public static bool IsDebugging
//        {
//            set { _isDebugging = value; }
//        }

//        /// <summary>
//        /// �������
//        /// </summary>
//        public static event LogDelegate LogUpdated;

//        /// <summary>
//        /// 
//        /// </summary>
//        /// <param name="aDir"></param>
//        public static void SetWorkDir(String aDir)
//        {
//            if (!Directory.Exists(aDir))
//            {
//                Directory.CreateDirectory(aDir);
//            }
//            _workDir = aDir;
//        }

//        /// <summary>
//        /// 
//        /// </summary>
//        /// <param name="aObjectToLog"></param>
//        public static void AddInfo(Object aObjectToLog)
//        {
//            if (_isDebugging)
//            {
//                Add(aObjectToLog);
//            }
//        }

//        /// <summary>
//        /// 
//        /// </summary>
//        /// <param name="aStringToLog"></param>
//        public static void AddInfo(String aStringToLog)
//        {
//            if (_isDebugging)
//            {
//                Add(aStringToLog);
//            }
//        }

//        /// <summary>
//        /// 
//        /// </summary>
//        /// <param name="aObjectToLog"></param>
//        public static void Add(Object aObjectToLog)
//        {
//            if (string.IsNullOrEmpty(_workDir))
//            {
//                SetWorkDir(Application.StartupPath + "\\Log\\");
//            }
//            if (aObjectToLog != null)
//            {
//                Add(aObjectToLog.ToString());
//            }
//            else
//            {
//                Add("null reference");
//            }
//        }


//        private String GetFileNameOfLastRecord(int maxQuant, bool Correct)
//        {
//            string mainPart = "";

//            int Aux = _pathString.Split('.').Length;
//            for (int i = 0; i < Aux - 1; i++) mainPart += _pathString.Split('.')[i];
//            string ext = _pathString.Split('.')[Aux - 1];

//            try
//            {
//                //*** Kopiev A.N. 2006.01.17 ��������� ������ ����� �� ����� ������ � ������, ������ �� ���� ���������� �����
//                DateTime maxCDT = DateTime.MinValue;

//                int indMaxCDT = 1;

//                for (int filNum = 1; filNum <= maxQuant; filNum++)
//                {
//                    string lext = filNum.ToString().PadLeft(3, '0');

//                    if (File.Exists(_workDir + mainPart + "." + lext))
//                    {
//                        var lfi = new FileInfo(_workDir + mainPart + "." + lext);

//                        if (maxCDT < lfi.LastWriteTime)
//                        {
//                            maxCDT = lfi.LastWriteTime;
//                            indMaxCDT = filNum;
//                        }

//                        if (Correct)
//                        {
//                            // ���� ����������� ���������� ���� �� ���������� ��� �������� ����� ��� ������� �� ������ �����:
//                            if (lfi.LastWriteTime > DateTime.Now)
//                            {
//                                Add("Wrong file \"" + mainPart + "." + lext +
//                                    "\" attributes were corrected to current date, which is set on STALT server.");
//                                try
//                                {
//                                    lfi.LastWriteTime = DateTime.Now.AddSeconds(-1);
//                                }
//                                catch (Exception ex)
//                                {
//                                    Add("Error while changing attribute LastWriteTime for file \"" + mainPart + "." +
//                                        lext + "\". Permission denied.\n" + ex);
//                                }
//                            }
//                        }
//                    }
//                }
//                //***
//                ext = indMaxCDT.ToString().PadLeft(3, '0');
//                return mainPart + "." + ext;
//            }
//            catch
//            {
//                Debug.WriteLine("Error while try to obtain last file. First file will be used instead: " + ext);
//                return _pathString;
//            }
//        }

//        private void IterFileName()
//        {
//            String ext;
//            int curNum;
//            const int maxQuant = 999;
////			if (Trace.Listeners.Count>0)
////			{
////				Trace.Listeners[0].Close();
////			}
//            if (!usualCall) // - ������ ��� ������ ������ ������.
//            {
//                usualCall = true;
//                _pathString = GetFileNameOfLastRecord(maxQuant, false);
//            }

//            if (File.Exists(_workDir + _pathString))
//            {
//                var fi = new FileInfo(_workDir + _pathString);
//                var length = (int) fi.Length;
//                if (length >= MAX_LOG_SIZE)
//                {
//                    ext = fi.Extension;
//                    String mainPart = fi.Name.TrimEnd(ext.ToCharArray());
//                    try
//                    {
//                        _pathString = GetFileNameOfLastRecord(maxQuant, true);
//                        curNum = Int32.Parse(_pathString.Split('.')[_pathString.Split('.').Length - 1].TrimStart('0'));

//                        //***
//                        ext = curNum.ToString().PadLeft(3, '0');
//                        fi = new FileInfo(_workDir + mainPart + "." + ext);
//                        if ((int) fi.Length >= MAX_LOG_SIZE)
//                        {
//                            if (curNum == maxQuant) curNum = 0;
//                            ext = (++curNum).ToString().PadLeft(3, '0');
//                            if (File.Exists(_workDir + mainPart + "." + ext))
//                            {
//                                File.Delete(_workDir + mainPart + "." + ext);
//                            }
//                        }
//                    }
//                    catch
//                    {
//                        Debug.WriteLine("Wrong log file extention: " + ext);
//                        ext = "001";
//                    }
//                    _pathString = mainPart + "." + ext;
//                }
//            }
////			TextWriterTraceListener myTextListener = new 
////				TextWriterTraceListener(_workDir+_pathString);
////			Trace.Listeners.Clear();
////			Trace.Listeners.Add(myTextListener);
//        }


//        private void Log_LogUpdated(object sender, LogEventArgs e)
//        {
//            lock (_logQueue.SyncRoot)
//            {
//                _logQueue.Enqueue(e.Message);
//            }
//        }

//        private void ReadFromInputBuffer()
//        {
//            while (true)
//            {
//                String str = "";
//                lock (_logQueue.SyncRoot)
//                {
//                    while (_logQueue.Count > 0)
//                    {
//                        str += "\n" + _logQueue.Dequeue() + "\r";
//                    }
//                }
//                if (str.Length > 0)
//                {
//                    AddToFile(str);
//                }
//                Thread.Sleep(1000);
//            }

//// ReSharper disable FunctionNeverReturns
//        }

//        private static void Add(String aLogMessage)
//        {
//            if (_log == null)
//            {
//                _log = new Log {_logQueue = new Queue()};

//                LogUpdated += _log.Log_LogUpdated;
//                _log._logThread = new Thread(_log.ReadFromInputBuffer) {IsBackground = true, Name = "LogThread"};
//                _log._logThread.Start();
//            }
//            if (LogUpdated != null)
//            {
//                LogUpdated(null,
//                           new LogEventArgs(DateTime.Now.ToLongTimeString() + "." + DateTime.Now.Millisecond + " " +
//                                            aLogMessage));
//            }
//        }

//        private void AddToFile(String aLogMessage)
//        {
//            IterFileName();


//            try
//            {
//                using (var myTextListener = new TextWriterTraceListener(_workDir + _pathString))
//                {
//                    Trace.Listeners.Add(myTextListener);
//                    Trace.WriteLine(DateTime.Now.ToShortDateString() + " " + aLogMessage);
//                    Trace.Flush();
//                    Trace.Listeners.Clear();
//                }
//            }
//            catch (Exception ex)
//            {
//                Debug.WriteLine(ex);
//            }
//        }
//    }

//    /// <summary>
//    /// ����� ���� �������
//    /// </summary>
//    public class LogEventArgs : EventArgs
//    {
//        private readonly String _message;

//        /// <summary>
//        /// �����������
//        /// </summary>
//        /// <param name="aMessage"></param>
//        public LogEventArgs(String aMessage)
//        {
//            _message = aMessage;
//        }

//        /// <summary>
//        /// �������� ���������
//        /// </summary>
//        public String Message
//        {
//            get { return _message; }
//        }
//    }
//}
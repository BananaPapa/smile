using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceProcess;
using log4net;
using Microsoft.Win32;

namespace Eureca.Professional.CommonClasses
{
    /// <summary>
    /// ����� ��� ��������� ������
    /// </summary>
    public class Command : MarshalByRefObject
    {
        private static readonly ILog Log = LogManager.GetLogger( typeof( Command ) );

        /// <summary>
        /// �������� ���������
        /// </summary>
        /// <param name="productName">�������� ���������</param>
        /// <param name="resource">������� ������, ������ ��������������� ���������</param>
        /// <param name="targetDir">���� ���������</param>
        /// <param name="login">����� ��� ������� � resource</param>
        /// <param name="password">������ ��� ������� � resource</param>
        /// <returns></returns>
        public int Uninstall(string productName, string resource, string targetDir, string login, string password)
        {
            try
            {
                var myProcess = new Process();
                myProcess.StartInfo.FileName = "net";
                myProcess.StartInfo.Arguments = " use \"" + @resource + "\" " + @password + " /USER:" + @login;
                myProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                myProcess.Start();
                myProcess.WaitForExit();
                string host = Dns.GetHostName();
                int ret = 0;
                RegistryKey rk =
                    Registry.LocalMachine.OpenSubKey("SOFTWARE").OpenSubKey("Microsoft").OpenSubKey("Windows").
                        OpenSubKey("CurrentVersion").OpenSubKey("Uninstall");

                String[] names = rk.GetSubKeyNames();
                foreach (String s in names)
                {
                    RegistryKey rsk = rk.OpenSubKey(s);
                    if ((rsk.GetValue("DisplayName") != null) && (rsk.GetValue("DisplayName").ToString() == productName))
                    {
                        string uninstallStr = rsk.GetValue("UninstallString").ToString();

                        myProcess.StartInfo.FileName = "msiexec.exe";
                        uninstallStr = uninstallStr.Remove(0, uninstallStr.IndexOf("{"));

                        msiAPI.INSTALLSTATE instState = msiAPI.MsiQueryProductState(uninstallStr);
                        if (instState == msiAPI.INSTALLSTATE.INSTALLSTATE_DEFAULT ||
                            instState == msiAPI.INSTALLSTATE.INSTALLSTATE_ABSENT)
                        {
                            myProcess.StartInfo.Arguments = " /x " + uninstallStr + " /l* \"" + @resource + "\\Logs\\" +
                                                            host + "." + productName.Replace(" ", "") +
                                                            ".log\" /quiet REMOVE=ALL";
                            myProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                            myProcess.Start();
                            myProcess.WaitForExit(30000);
                            ret = myProcess.ExitCode;
                        }
                    }
                }

                myProcess.StartInfo.FileName = "net";
                myProcess.StartInfo.Arguments = " use " + @resource + " /DELETE";
                //myProcess.StartInfo.WindowStyle=ProcessWindowStyle.Hidden;
                //myProcess.Start();
                //myProcess.WaitForExit();
                myProcess.Dispose();

                return ret;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return int.MinValue;
            }
        }

        /// <summary>
        /// �������� ���������
        /// </summary>
        /// <param name="productName">�������� ���������</param>
        /// <param name="resource">������� ������, ������ ��������������� ���������</param>
        /// <param name="targetDir">���� ��� ���������</param>
        /// <param name="login">����� ��� ������� � resource</param>
        /// <param name="password">������ ��� ������� � resource</param>
        /// <returns></returns>
        public int Install(string productName, string resource, string targetDir, string login, string password)
        {
            try
            {
                var myProcess = new Process
                                    {
                                        StartInfo =
                                            {
                                                FileName = "net",
                                                Arguments =
                                                    " use \"" + @resource + "\" " + @password + " /USER:" + @login,
                                                WindowStyle = ProcessWindowStyle.Hidden
                                            }
                                    };
                myProcess.Start();

                myProcess.WaitForExit();

                string host = Dns.GetHostName();

                int ret;
                myProcess.StartInfo.FileName = "msiexec.exe";
                RegistryKey rk =
                    Registry.LocalMachine.OpenSubKey("SOFTWARE").OpenSubKey("Microsoft").OpenSubKey("Windows").
                        OpenSubKey("CurrentVersion").OpenSubKey("Uninstall");

                String[] names = rk.GetSubKeyNames();
                foreach (String s in names)
                {
                    RegistryKey rsk = rk.OpenSubKey(s);
                    if ((rsk.GetValue("DisplayName") != null) && (rsk.GetValue("DisplayName").ToString() == productName))
                    {
                        string uninstallStr = rsk.GetValue("UninstallString").ToString();

                        myProcess.StartInfo.FileName = "msiexec.exe";
                        uninstallStr = uninstallStr.Remove(0, uninstallStr.IndexOf("{"));
                        myProcess.StartInfo.Arguments = " /x " + uninstallStr + " /l* \"" + @resource + "\\Logs\\" +
                                                        host + "." + productName.Replace(" ", "") + ".log\" /quiet";
                        myProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                        myProcess.Start();

                        myProcess.WaitForExit();
                    }
                }


                productName = productName.Replace(" ", "");
                myProcess.StartInfo.Arguments = " /i \"" + @resource + "\\" + productName + ".msi\"" + " /l*+ \"" +
                                                @resource + "\\Logs\\" + host + "." + productName.Replace(" ", "") +
                                                ".log\" /quiet ALLUSERS=2 TARGETDIR=\"" + targetDir + "\"";
                myProcess.Start();

                myProcess.WaitForExit();

                ret = myProcess.ExitCode;

                myProcess.StartInfo.FileName = "net";
                myProcess.StartInfo.Arguments = " use " + @resource + " /DELETE";
                //myProcess.StartInfo.WindowStyle=ProcessWindowStyle.Hidden;
                //myProcess.Start();
                //myProcess.WaitForExit();

                return ret;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                //MessageBox.Show(ex.Message, "������ ���������", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return int.MinValue;

                //this.Close();
            }
        }


        /// <summary>
        /// ����� ��� ����������� ����������� ������� �������
        /// </summary>
        /// <returns></returns>
        public bool IsEnabled()
        {
            return true;
        }

        /// <summary>
        /// ����������� �� ��������� � �������� ������
        /// </summary>
        /// <param name="productName"></param>
        /// <returns></returns>
        public bool IsProductInstalled(string productName)
        {
            bool ret = false;
            RegistryKey rk =
                Registry.LocalMachine.OpenSubKey("SOFTWARE").OpenSubKey("Microsoft").OpenSubKey("Windows").OpenSubKey(
                    "CurrentVersion").OpenSubKey("Uninstall");

            String[] names = rk.GetSubKeyNames();
            foreach (String s in names)
            {
                RegistryKey rsk = rk.OpenSubKey(s);
                if ((rsk.GetValue("DisplayName") != null) && (rsk.GetValue("DisplayName").ToString() == productName))
                {
                    string uninstallStr = rsk.GetValue("UninstallString").ToString();


                    uninstallStr = uninstallStr.Remove(0, uninstallStr.IndexOf("{"));

                    msiAPI.INSTALLSTATE instState = msiAPI.MsiQueryProductState(uninstallStr);
                    if (instState == msiAPI.INSTALLSTATE.INSTALLSTATE_DEFAULT ||
                        instState == msiAPI.INSTALLSTATE.INSTALLSTATE_ABSENT)
                    {
                        ret = true;
                    }
                }
            }
            return ret;
        }

        public string GetProductPath(string productName)
        {
            string ret = "";
            RegistryKey rk =
                Registry.LocalMachine.OpenSubKey("SOFTWARE").OpenSubKey("Microsoft").OpenSubKey("Windows").OpenSubKey(
                    "CurrentVersion").OpenSubKey("Uninstall");

            String[] names = rk.GetSubKeyNames();
            foreach (String s in names)
            {
                RegistryKey rsk = rk.OpenSubKey(s);
                if ((rsk.GetValue("DisplayName") != null) && (rsk.GetValue("DisplayName").ToString() == productName))
                {
                    string uninstallStr = rsk.GetValue("UninstallString").ToString();
                }
            }
            return ret;
        }

        /// <summary>
        /// ��������� ������� ������ � �������� ������
        /// </summary>
        /// <param name="servName"></param>
        /// <returns></returns>
        public ServiceControllerStatus CheckServiceStatus(string servName)
        {
            var service = new ServiceController(servName);
            return service.Status;
        }

        /// <summary>
        /// ������ ������ � �������� ������
        /// </summary>
        /// <param name="servName"></param>
        /// <returns></returns>
        public bool RunService(string servName)
        {
            using (var service = new ServiceController(servName))
            {
                if (service.Status == ServiceControllerStatus.Stopped)
                {
                    try
                    {
                        service.Start();
                        service.WaitForStatus(ServiceControllerStatus.Running, new TimeSpan(0, 0, 60));
                        return true;
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex);
                        return false;
                    }
                }

                if (service.Status == ServiceControllerStatus.Running)
                {
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// ������ ������ � �������� ������
        /// </summary>
        /// <param name="servName"></param>
        /// <returns></returns>
        public bool RunServiceAgent(string servName)
        {
            using (var service = new ServiceController(servName))
            {
                if (service.Status == ServiceControllerStatus.Stopped)
                {
                    try
                    {
                        service.Start();
                        service.WaitForStatus(ServiceControllerStatus.Running, new TimeSpan(0, 0, 30));
                        return true;
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex);
                        return false;
                    }
                }

                if (service.Status == ServiceControllerStatus.Running)
                {
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// ��������� ������ � �������� ������
        /// </summary>
        /// <param name="servName"></param>
        /// <returns></returns>
        public bool StopService(string servName)
        {
            using (var service = new ServiceController(servName))
            {
                if (service.Status == ServiceControllerStatus.Running)
                {
                    try
                    {
                        service.Stop();
                        service.WaitForStatus(ServiceControllerStatus.Stopped, new TimeSpan(0, 0, 30));
                        return true;
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex);
                        return false;
                    }
                }

                if (service.Status == ServiceControllerStatus.Stopped)
                {
                    return true;
                }
                return false;
            }
        }

        #region Work with remoting files

        /// <summary>
        /// ��������� ������� ������
        /// </summary>
        /// <returns></returns>
        public string[] GetLogicalDrives()
        {
            try
            {
                return Directory.GetLogicalDrives();
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return null;
            }
        }

        /// <summary>
        /// ��������� ������� ������������� ��� �������� ��������
        /// </summary>
        /// <param name="rootDir"></param>
        /// <returns></returns>
        public string[] GetDirectories(string rootDir)
        {
            try
            {
                return Directory.GetDirectories(rootDir);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return null;
            }
        }

        /// <summary>
        /// ��������� ������� ������������� ��� �������� ��������
        /// </summary>
        /// <param name="rootDir"></param>
        /// <returns></returns>
        public string[] GetFiles(string rootDir)
        {
            try
            {
                return Directory.GetFiles(rootDir);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return null;
            }
        }

        public List<FileItem> GetFileItems(string path, bool needRoot)
        {
            var files = GetFiles(path);
            var fileItems = files.Select(item => new FileItem(new FileInfo(item))).ToList();

            var directories = GetDirectories(path);
            fileItems.AddRange(directories.Select(item => new FileItem(new DirectoryInfo(item), false)));
            if (needRoot)
                fileItems.Add(new FileItem(new DirectoryInfo(path), true));
            return fileItems;
        }


        /// <summary>
        /// �������� ����������
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public bool CreateDirectory(string path)
        {
            try
            {
                Directory.CreateDirectory(path);
                return true;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return false;
            }
        }

        /// <summary>
        /// ����� ����������� ����� �� 
        /// </summary>
        /// <param name="filePath"></param>
        public FileStream GetFile(string filePath)
        {
            try
            {
                FileStream fs = File.OpenRead(filePath);
                Log.Error("�������� ����� ��� ������ " + filePath);
                return fs;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return null;
            }
        }
        #endregion

    }
}
using System.Runtime.InteropServices;

namespace Eureca.Professional.CommonClasses
{
    /// <summary>
    /// Summary description for msiAPI.
    /// </summary>
    public class msiAPI
    {
        #region INSTALLSTATE enum

        public enum INSTALLSTATE
        {
            INSTALLSTATE_UNKNOWN = -1,
            INSTALLSTATE_INVALIDARG = 0,
            INSTALLSTATE_ADVERTISED = 1,
            INSTALLSTATE_ABSENT = 2,
            INSTALLSTATE_DEFAULT = 5
        }

        #endregion

        [DllImport("Msi")]
        public static extern INSTALLSTATE MsiQueryProductState(string szProduct);

        [DllImport("Msi")]
        public static extern uint MsiInstallProduct(string szPackagePath, string szCommandLine);
    }
}
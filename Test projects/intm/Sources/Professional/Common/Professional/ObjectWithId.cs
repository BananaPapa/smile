﻿using System;

namespace Eureca.Professional.BaseComponents
{
    /// <summary>
    /// класс - контейнер.
    /// Содержит основные понятия(ID, Name)
    /// </summary>
    public class ObjectWithId : IComparable
    {
        private readonly int _id;
        private object _value;

        public ObjectWithId(int id, object value)
        {
            _id = id;
            _value = value;
        }

        public int ID
        {
            get { return _id; }
        }

        public object Value
        {
            get { return _value; }
        }

        #region IComparable Members

        public int CompareTo(object obj)
        {
            var other = (ObjectWithId) obj;
            return _id.CompareTo(other._id);
        }

        #endregion

        public override string ToString()
        {
            return string.Format("{0}", Value);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is ObjectWithId))
                return false;
            var newObj = (ObjectWithId) obj;
            if (newObj == null)
                return false;
            if (_id == newObj.ID)
                return true;

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public ObjectWithId Clone()
        {
            return new ObjectWithId(ID, Value);
        }

        public void SetValue(object value)
        {
            _value = value;
        }
    }
}
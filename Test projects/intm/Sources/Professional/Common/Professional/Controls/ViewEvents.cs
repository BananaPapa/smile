﻿using System;
using System.Collections.Generic;
using Eureca.Professional.CommonClasses;
using Eureca.Professional.ActiveRecords;

namespace Eureca.Professional.BaseComponents
{
    public partial class ViewEvents : BaseTestingControl
    {
        private readonly List<Event> _events = new List<Event>();

        public ViewEvents()
        {
            InitializeComponent();
        }

        public override void InitControl()
        {
            base.InitControl();
            CaptionControl = "Просмотр событий";
            InsertData();
        }

        private void InsertData()
        {
            dateEditBegin.DateTime = DateTime.Now;
            dateEditEnd.DateTime = DateTime.Now;
            timeEditEnd.Time = DateTime.Now;
            GetEvents(CommonMethods.SetTime(dateEditBegin.DateTime, timeEditBegin.Time),
                      CommonMethods.SetTime(dateEditEnd.DateTime, timeEditEnd.Time));
        }

        private void GetEvents(DateTime begin, DateTime end)
        {
            _events.Clear();
            _events.AddRange(Event.GetEventsFromInterval(begin, end));
            gridControl1.DataSource = _events;
            gridView1.BestFitColumns();
        }

        private void simpleButtonCancel_Click(object sender, EventArgs e)
        {
            if (ParentForm != null)
                ParentForm.Close();
        }

        private void simpleButtonFind_Click(object sender, EventArgs e)
        {
            GetEvents(CommonMethods.SetTime(dateEditBegin.DateTime, timeEditBegin.Time),
                      CommonMethods.SetTime(dateEditEnd.DateTime, timeEditEnd.Time));
        }

        private void simpleButtonPrint_Click(object sender, EventArgs e)
        {
            gridControl1.ShowPrintPreview();
        }
    }
}
﻿namespace Eureca.Professional.BaseComponents
{
    partial class CreateUser
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.textEditLastName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.textEditFirsName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.textEditMiddleName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.dateEditBirthDate = new DevExpress.XtraEditors.DateEdit();
            this.comboBoxEditType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.textEditPassword = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.textEditPassword1 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButtonAdd = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.comboBoxEditProfile = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.checkEditIsArchive = new DevExpress.XtraEditors.CheckEdit();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.textEditLastName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditFirsName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditMiddleName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditBirthDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditBirthDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPassword.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPassword1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditProfile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditIsArchive.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(5, 5);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(44, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Фамилия";
            // 
            // textEditLastName
            // 
            this.textEditLastName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textEditLastName.Location = new System.Drawing.Point(132, 2);
            this.textEditLastName.MinimumSize = new System.Drawing.Size(269, 20);
            this.textEditLastName.Name = "textEditLastName";
            this.textEditLastName.Properties.MaxLength = 256;
            this.textEditLastName.Size = new System.Drawing.Size(271, 20);
            this.textEditLastName.TabIndex = 0;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(4, 31);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(19, 13);
            this.labelControl6.TabIndex = 10;
            this.labelControl6.Text = "Имя";
            // 
            // textEditFirsName
            // 
            this.textEditFirsName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textEditFirsName.Location = new System.Drawing.Point(131, 28);
            this.textEditFirsName.MinimumSize = new System.Drawing.Size(269, 20);
            this.textEditFirsName.Name = "textEditFirsName";
            this.textEditFirsName.Properties.MaxLength = 256;
            this.textEditFirsName.Size = new System.Drawing.Size(272, 20);
            this.textEditFirsName.TabIndex = 1;
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(4, 57);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(49, 13);
            this.labelControl7.TabIndex = 12;
            this.labelControl7.Text = "Отчество";
            // 
            // textEditMiddleName
            // 
            this.textEditMiddleName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textEditMiddleName.Location = new System.Drawing.Point(131, 54);
            this.textEditMiddleName.MinimumSize = new System.Drawing.Size(269, 20);
            this.textEditMiddleName.Name = "textEditMiddleName";
            this.textEditMiddleName.Properties.MaxLength = 256;
            this.textEditMiddleName.Size = new System.Drawing.Size(272, 20);
            this.textEditMiddleName.TabIndex = 2;
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(4, 84);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(80, 13);
            this.labelControl9.TabIndex = 15;
            this.labelControl9.Text = "Дата рождения";
            // 
            // dateEditBirthDate
            // 
            this.dateEditBirthDate.EditValue = null;
            this.dateEditBirthDate.Location = new System.Drawing.Point(132, 81);
            this.dateEditBirthDate.Name = "dateEditBirthDate";
            this.dateEditBirthDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditBirthDate.Properties.MinValue = new System.DateTime(1900, 1, 13, 0, 0, 0, 0);
            this.dateEditBirthDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditBirthDate.Size = new System.Drawing.Size(100, 20);
            this.dateEditBirthDate.TabIndex = 3;
            // 
            // comboBoxEditType
            // 
            this.comboBoxEditType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxEditType.Location = new System.Drawing.Point(131, 159);
            this.comboBoxEditType.MinimumSize = new System.Drawing.Size(269, 20);
            this.comboBoxEditType.Name = "comboBoxEditType";
            this.comboBoxEditType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEditType.Size = new System.Drawing.Size(272, 20);
            this.comboBoxEditType.TabIndex = 8;
            this.comboBoxEditType.SelectedIndexChanged += new System.EventHandler(this.comboBoxEditType_SelectedIndexChanged);
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(5, 110);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(37, 13);
            this.labelControl3.TabIndex = 5;
            this.labelControl3.Text = "Пароль";
            // 
            // textEditPassword
            // 
            this.textEditPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textEditPassword.Location = new System.Drawing.Point(132, 107);
            this.textEditPassword.MinimumSize = new System.Drawing.Size(240, 20);
            this.textEditPassword.Name = "textEditPassword";
            this.textEditPassword.Properties.MaxLength = 256;
            this.textEditPassword.Properties.PasswordChar = '*';
            this.textEditPassword.Size = new System.Drawing.Size(240, 20);
            this.textEditPassword.TabIndex = 5;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(5, 136);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(122, 13);
            this.labelControl4.TabIndex = 7;
            this.labelControl4.Text = "Подтверждание пароля";
            // 
            // textEditPassword1
            // 
            this.textEditPassword1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textEditPassword1.Location = new System.Drawing.Point(132, 133);
            this.textEditPassword1.MinimumSize = new System.Drawing.Size(240, 20);
            this.textEditPassword1.Name = "textEditPassword1";
            this.textEditPassword1.Properties.MaxLength = 256;
            this.textEditPassword1.Properties.PasswordChar = '*';
            this.textEditPassword1.Size = new System.Drawing.Size(240, 20);
            this.textEditPassword1.TabIndex = 6;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(4, 162);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(24, 13);
            this.labelControl5.TabIndex = 9;
            this.labelControl5.Text = "Роль";
            // 
            // simpleButtonAdd
            // 
            this.simpleButtonAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonAdd.Location = new System.Drawing.Point(248, 217);
            this.simpleButtonAdd.Name = "simpleButtonAdd";
            this.simpleButtonAdd.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonAdd.TabIndex = 9;
            this.simpleButtonAdd.Text = "Ок";
            this.simpleButtonAdd.Click += new System.EventHandler(this.simpleButtonAdd_Click);
            // 
            // simpleButtonCancel
            // 
            this.simpleButtonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButtonCancel.Location = new System.Drawing.Point(329, 217);
            this.simpleButtonCancel.Name = "simpleButtonCancel";
            this.simpleButtonCancel.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonCancel.TabIndex = 10;
            this.simpleButtonCancel.Text = "Отмена";
            // 
            // comboBoxEditProfile
            // 
            this.comboBoxEditProfile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxEditProfile.Location = new System.Drawing.Point(131, 185);
            this.comboBoxEditProfile.MinimumSize = new System.Drawing.Size(269, 20);
            this.comboBoxEditProfile.Name = "comboBoxEditProfile";
            this.comboBoxEditProfile.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditProfile.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEditProfile.Size = new System.Drawing.Size(272, 20);
            this.comboBoxEditProfile.TabIndex = 7;
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(4, 188);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(45, 13);
            this.labelControl8.TabIndex = 14;
            this.labelControl8.Text = "Профиль";
            // 
            // checkEditIsArchive
            // 
            this.checkEditIsArchive.Location = new System.Drawing.Point(129, 220);
            this.checkEditIsArchive.Name = "checkEditIsArchive";
            this.checkEditIsArchive.Properties.Caption = "Архивировать";
            this.checkEditIsArchive.Size = new System.Drawing.Size(112, 18);
            this.checkEditIsArchive.TabIndex = 16;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButton1.Location = new System.Drawing.Point(373, 104);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(30, 23);
            this.simpleButton1.TabIndex = 17;
            this.simpleButton1.Text = "?";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // CreateUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.checkEditIsArchive);
            this.Controls.Add(this.dateEditBirthDate);
            this.Controls.Add(this.labelControl9);
            this.Controls.Add(this.labelControl8);
            this.Controls.Add(this.comboBoxEditProfile);
            this.Controls.Add(this.textEditMiddleName);
            this.Controls.Add(this.labelControl7);
            this.Controls.Add(this.textEditFirsName);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.simpleButtonCancel);
            this.Controls.Add(this.simpleButtonAdd);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.textEditPassword1);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.textEditPassword);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.comboBoxEditType);
            this.Controls.Add(this.textEditLastName);
            this.Controls.Add(this.labelControl1);
            this.MinimumSize = new System.Drawing.Size(415, 245);
            this.Name = "CreateUser";
            this.Size = new System.Drawing.Size(415, 245);
            ((System.ComponentModel.ISupportInitialize)(this.textEditLastName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditFirsName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditMiddleName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditBirthDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditBirthDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPassword.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPassword1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditProfile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditIsArchive.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit textEditLastName;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit textEditFirsName;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit textEditMiddleName;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.DateEdit dateEditBirthDate;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditType;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit textEditPassword;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit textEditPassword1;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.SimpleButton simpleButtonAdd;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCancel;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditProfile;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.CheckEdit checkEditIsArchive;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
    }
}

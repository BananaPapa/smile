﻿using System;
using System.Windows.Forms;
using Castle.ActiveRecord;
using Eureca.Professional.CommonClasses;
using Eureca.Professional.ActiveRecords;

namespace Eureca.Professional.BaseComponents
{
    public delegate void CreateUserDelegate(User user);

    public partial class CreateUser : BaseTestingControl
    {
        /// <summary>
        /// тип действия формы
        /// </summary>
        private readonly ControlTypeAction _typeAction = ControlTypeAction.VIEW;

        private User _user;

        #region события

        /// <summary>
        /// событие на добавления профиля
        /// </summary>
        public event CreateUserDelegate AddUser;

        /// <summary>
        /// событие на изменения профиля
        /// </summary>
        public event CreateUserDelegate ChangeUser;

        #endregion

        public CreateUser(ControlTypeAction TypeAction, int UserId)
        {
            //ResetSessionScope();
            if (UserId != -1)
                _user = ActiveRecordBase<User>.Find(UserId);
            _typeAction = TypeAction;
            InitializeComponent();
        }

        public override void InitControl()
        {
            base.InitControl();
            checkEditIsArchive.Enabled = false;
            if (_user == null)
            {
                CaptionControl = "Cоздание пользователя";
            }
            else
            {
                CaptionControl = string.Format("Сведения о пользователе");
                if (!_user.Equals(RunnerApplication.CurrentUser))
                {
                    checkEditIsArchive.Enabled = true;
                    checkEditIsArchive.Checked = _user.IsArchive;
                }
            }
            InsertData();
            dateEditBirthDate.Properties.MaxValue = DateTime.Now;
        }

        private void InsertData()
        {
            comboBoxEditType.Properties.Items.Clear();
            comboBoxEditProfile.Properties.Items.Clear();
            using (new SessionScope())
            {
                foreach (Profile profile in ActiveRecordBase<Profile>.FindAll())
                    comboBoxEditProfile.Properties.Items.Add(profile);
            }
            if (comboBoxEditProfile.Properties.Items.Count > 0)
                comboBoxEditProfile.SelectedIndex = 0;

            if (_user == null)
            {
                comboBoxEditType.Properties.Items.AddRange(new[]
                                                               {
                                                                   new ObjectWithId(1, "Испытуемый"),
                                                                   //new  ObjectWithId(2,"Специалист"),
                                                                   //new ObjectWithId(3,"Эксперт"),
                                                                   new ObjectWithId(2, "Администратор")
                                                                   //new ObjectWithId(5,"Администратор безопасности")
                                                               });
                ClearFields();
            }
            else
            {
                textEditLastName.Text = _user.LastName;
                textEditFirsName.Text = _user.FirstName;
                textEditMiddleName.Text = _user.MiddleName;
                textEditPassword.Text = _user.Password;
                textEditPassword1.Text = _user.Password;
                dateEditBirthDate.DateTime = _user.BirthDate;
                if (_user.Probationer == null)
                {
                    comboBoxEditType.Properties.Items.AddRange(new[]
                                                                   {
                                                                       new ObjectWithId(2, "Администратор")
                                                                   });
                    comboBoxEditProfile.SelectedIndex = -1;
                    labelControl8.Visible = comboBoxEditProfile.Visible = false;
                }
                else
                {
                    comboBoxEditType.Properties.Items.AddRange(new[]
                                                                   {
                                                                       new ObjectWithId(1, "Испытуемый")
                                                                   });
                    if (_user.Probationer.Profile != null) 
                    comboBoxEditProfile.SelectedItem = _user.Probationer.Profile;
                }
            }
            comboBoxEditType.SelectedIndex = 0;
        }

        /// <summary>
        /// очистка всех полей
        /// </summary>
        public void ClearFields()
        {
            textEditFirsName.Text = "";
            textEditMiddleName.Text = "";
            textEditLastName.Text = "";
            textEditPassword.Text = "";
            textEditPassword1.Text = "";
            comboBoxEditType.SelectedIndex = 0;
            dateEditBirthDate.DateTime = new DateTime(0);
        }

        /// <summary>
        /// метод проверки полей
        /// </summary>
        /// <returns></returns>
        private bool CheckFields()
        {
            if (_typeAction == ControlTypeAction.VIEW)
                return true;
            
            if (//string.IsNullOrEmpty(textEditFirsName.Text.Trim()) ||
                StringScan.NotAllowedSymbols(textEditFirsName.Text.Trim()))
            {
                MessageForm.ShowErrorMsg("Некорректное имя");
                return false;
            }
            if (string.IsNullOrEmpty(textEditLastName.Text.Trim()) ||
                StringScan.NotAllowedSymbols(textEditLastName.Text.Trim()))
            {
                MessageForm.ShowErrorMsg("Некорректная фамилия");
                return false;
            }
            if (//string.IsNullOrEmpty(textEditMiddleName.Text.Trim()) ||
                StringScan.NotAllowedSymbols(textEditMiddleName.Text.Trim()))
            {
                MessageForm.ShowErrorMsg("Некорректное отчество");
                return false;
            }
            if (string.IsNullOrEmpty(textEditPassword.Text))
            {
                MessageForm.ShowErrorMsg("Введите пароль");
                return false;
            }
            if (textEditPassword.Text.CompareTo(textEditPassword1.Text) != 0)
            {
                MessageForm.ShowErrorMsg("Пароли не совпадают");
                return false;
            }
            var rol = (ObjectWithId) comboBoxEditType.SelectedItem;
            if (rol.ID == (int) UserRole.Probationer)
            {
                if (comboBoxEditProfile.SelectedIndex == -1)
                {
                    MessageForm.ShowErrorMsg("Выберите профиль");
                    return false;
                }
            }


           
            return true;
        }

        /// <summary>
        /// метод добавления пользователя
        /// </summary>
        private void AddNewUser()
        {
            var rol = (ObjectWithId) comboBoxEditType.SelectedItem;
            _user = new User(textEditFirsName.Text.Trim(), textEditMiddleName.Text.Trim(), textEditLastName.Text.Trim(),
                             dateEditBirthDate.DateTime,
                              textEditPassword.Text);
            if (((ObjectWithId)comboBoxEditType.SelectedItem).ID == (int)UserRole.Probationer)
            {
                var p = new Probationer(_user, (Profile) comboBoxEditProfile.SelectedItem);
                p.CreateAndFlush();
                _user.Probationer = p;
            }
            else
            {
                _user.CreateAndFlush();
            }
            AddUser(_user);
        }

        /// <summary>
        /// метод измениения профиля
        /// </summary>
        private void ChangeCurrentUser()
        {
            _user.FirstName = textEditFirsName.Text.Trim();
            _user.LastName = textEditLastName.Text.Trim();
            _user.MiddleName = textEditMiddleName.Text.Trim();
            _user.Password = textEditPassword.Text;
                
            _user.BirthDate = dateEditBirthDate.DateTime;

            _user.IsArchive = checkEditIsArchive.Checked;
            if (_user.Probationer != null)
            {
                _user.Probationer.Profile = (Profile) comboBoxEditProfile.SelectedItem;
                _user.Probationer.UpdateAndFlush();
            }
            _user.UpdateAndFlush();
            ChangeUser(_user);
        }

        private void simpleButtonAdd_Click(object sender, EventArgs e)
        {
            if (!CheckFields())
                return;
            if (_typeAction == ControlTypeAction.INSERT)
            {
                AddNewUser();
            }
            if (_typeAction == ControlTypeAction.UPDATE)
            {
                ChangeCurrentUser();
            }
            if (ParentForm != null)
                ParentForm.DialogResult = DialogResult.OK;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (_user == null)
            {
                MessageForm.ShowErrorMsg("Пользователь не создан");
                return;
            }
            if (_user.Probationer == null)
            {
                MessageForm.ShowErrorMsg("Подсказка паролей возможна только для обучаемых специалистов");
                return;
            }
            MessageForm.ShowOkMsg(_user.Password);
        }

        private void comboBoxEditType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxEditType.SelectedIndex == -1)
                return;
            var item = (ObjectWithId) comboBoxEditType.SelectedItem;
            if (item.ID == 2)
            {
                //администратор
                comboBoxEditProfile.Enabled = false;
            }
            else
            {
                comboBoxEditProfile.Enabled = true;
            }
        }
    }
}
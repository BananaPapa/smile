﻿using System;
using System.Windows.Forms;
using Eureca.Professional.CommonClasses;

namespace Eureca.Professional.BaseComponents
{
    public partial class AskPasswordForm : BaseTestingControl
    {
        public AskPasswordForm()
        {
            InitializeComponent();
        }

        public override void InitControl()
        {
            base.InitControl();
            CaptionControl = "Подтверждение пароля";
            labelControlUser.Text = RunnerApplication.CurrentUser.ToString();
        }

        private void simpleButtonOk_Click(object sender, EventArgs e)
        {
            if (textEdit1.Text.CompareTo(RunnerApplication.CurrentUser.Password) == 0)
            {
                ParentForm.DialogResult = DialogResult.OK;
            }
            else
            {
                MessageForm.ShowErrorMsg("Некорректный пароль");
            }
        }
    }
}
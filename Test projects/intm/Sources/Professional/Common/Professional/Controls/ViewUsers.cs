﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Castle.ActiveRecord;
using Eureca.Professional.CommonClasses;
using Eureca.Professional.CommonClasses.Data;
using Eureca.Professional.ActiveRecords;
using log4net;


namespace Eureca.Professional.BaseComponents
{
    /// <summary>
    /// контрол для просмотра списка профилей
    /// </summary>
    public partial class ViewUsers : BaseTestingControl
    {
        private static readonly ILog Log = LogManager.GetLogger( typeof( ViewUsers ) );

        #region поля

        private readonly List<User> _users = new List<User>();
        private User _currentUser;

        #endregion

        #region конструктор

        public ViewUsers(User currentUser)
        {
            _currentUser = currentUser;
            InitializeComponent();
        }

        #endregion

        #region методы

        public override void InitControl()
        {
            base.InitControl();
            //CreateScope();
            CaptionControl = "Просмотр пользователей";
            InsertData();
        }

        /// <summary>
        /// метод вставки данных из БД
        /// </summary>
        private void InsertData()
        {
            _users.Clear();
            using (new SessionScope())
            {
                _users.AddRange(ActiveRecordBase<User>.FindAll());
            }
            RefreshData();
        }

        /// <summary>
        /// метод обновления тестов
        /// </summary>
        private void RefreshData()
        {
            gridControl1.DataSource = _users;
            gridViewSimple.BestFitColumns();
        }

        private void AddUser()
        {
            var uc = new CreateUser(ControlTypeAction.INSERT, -1);
            uc.AddUser += uc_AddUser;
            uc.InitControl();
            uc.ShowInForm();
            RefreshData();
        }

        /// <summary>
        /// метод изменения профиля
        /// </summary>
        private void ChangeUser()
        {
            int currentRow = gridViewSimple.FocusedRowHandle;
            object id = gridViewSimple.GetRowCellValue(currentRow, "Id");
            if (id != null)
            {
                var uc = new CreateUser(ControlTypeAction.UPDATE, (int) id);
                uc.InitControl();
                uc.ChangeUser += uc_ChangeUser;
                uc.InitControl();
                uc.ShowInForm();
            }
            else
            {
                MessageForm.ShowErrorMsg("Необходимо выбрать пользователя из таблицы.");
                return;
            }
        }

        /// <summary>
        /// метод удаления профиля
        /// </summary>
        private void DeleteUser()
        {
            int currentRow = gridViewSimple.FocusedRowHandle;
            object id = gridViewSimple.GetRowCellValue(currentRow, "Id");

            if (id != null)
            {
                try
                {
                    if ((int) id == _currentUser.Id)
                    {
                        MessageForm.ShowErrorMsg("Нельзя удалять текущего пользователя.");
                        return;
                    }
                    if (MessageForm.ShowYesNoQuestionMsg("Вы уверены, что хотите удалить пользователя?") ==
                        DialogResult.No)
                        return;
                    User user = ActiveRecordBase<User>.Find(id);

                    try
                    {
                        if (DBExecutor.ExecNonQuery("exec DeleteUser '" + user.UniqueId + "'") > 0)
                        {
                            MessageForm.ShowOkMsg("Пользователь " + user + " успешно удален.");
                        }
                        //открепление пользователя из СПО планирования
                        var curriculumConnStr = SystemProperties.GetItem(SysPropEnum.CurriculumConnString);
                        if (curriculumConnStr != null)
                        {
                            var professionalConnectionString = DBExecutor.GetConnectionString();
                            if (DBExecutor.Exist(curriculumConnStr.Value))
                            {
                                DBExecutor.SetConnectionString(curriculumConnStr.Value);
                                DBExecutor.ExecNonQuery("exec DetachProfessionalUser '" + user.UniqueId + "'");
                                DBExecutor.SetConnectionString(professionalConnectionString);
                            }
                        }

                        _users.Remove(user);
                        RefreshData();
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex);
                        MessageForm.ShowErrorMsg(
                            "Ошибка удаления пользователя. " + ex.Message);
                        return;
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                    MessageForm.ShowErrorMsg(ex.Message);
                }
            }
            else
            {
                MessageForm.ShowErrorMsg("Необходимо выбрать пользователя из таблицы.");
                return;
            }
        }

        #endregion

        #region обработчики событий пользовательских контролов

        /// <summary>
        /// обработчик событи добавления профиля
        /// </summary>
        /// <param name="User"></param>
        private void uc_AddUser(User User)
        {
            _users.Add(User);
            RefreshData();
        }

        /// <summary>
        /// обработчик события изменения профиля
        /// </summary>
        /// <param name="User"></param>
        private void uc_ChangeUser(User User)
        {
            if (_currentUser.Equals(User))
                _currentUser = User;
            for (int i = 0; i < _users.Count; i++)
            {
                if (_users[i].Equals(User))
                {
                    _users[i] = User;
                    RefreshData();
                    break;
                }
            }
        }

        #endregion

        #region обработчики событий стандартных компонент

        private void simpleButtonClose_Click(object sender, EventArgs e)
        {
            if (ParentForm != null)
                ParentForm.Close();
        }


        private void simpleButtonAdd_Click(object sender, EventArgs e)
        {
            AddUser();
        }

        private void simpleButtonChange_Click(object sender, EventArgs e)
        {
            ChangeUser();
        }


        private void simpleButtonDelete_Click(object sender, EventArgs e)
        {
            DeleteUser();
        }

        private void gridControl1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ChangeUser();
        }

        private void simpleButtonPrint_Click(object sender, EventArgs e)
        {
            gridControl1.ShowPrintPreview();
        }
        #endregion
    }

  
}
﻿namespace Eureca.Professional.BaseComponents.Controls
{
    partial class ApplicationInfoControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if (disposing && ( components != null ))
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this._dbVersionLabel = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this._appVersionLabel = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this._producerLabel = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this._appNameLabel = new DevExpress.XtraEditors.LabelControl();
            this._descriptionLabel = new DevExpress.XtraEditors.LabelControl();
            this._richTextBox = new DevExpress.XtraRichEdit.RichEditControl();
            this._documentationLabel = new DevExpress.XtraEditors.LabelControl();
            this.flowLayoutPanel2.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this._dbVersionLabel);
            this.flowLayoutPanel2.Controls.Add(this.labelControl3);
            this.flowLayoutPanel2.Controls.Add(this._appVersionLabel);
            this.flowLayoutPanel2.Controls.Add(this.labelControl2);
            this.flowLayoutPanel2.Controls.Add(this._producerLabel);
            this.flowLayoutPanel2.Controls.Add(this.labelControl4);
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.flowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(0, 779);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(1009, 25);
            this.flowLayoutPanel2.TabIndex = 1;
            // 
            // _dbVersionLabel
            // 
            this._dbVersionLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._dbVersionLabel.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._dbVersionLabel.Location = new System.Drawing.Point(999, 3);
            this._dbVersionLabel.Margin = new System.Windows.Forms.Padding(5, 3, 10, 3);
            this._dbVersionLabel.Name = "_dbVersionLabel";
            this._dbVersionLabel.Size = new System.Drawing.Size(0, 16);
            this._dbVersionLabel.TabIndex = 20;
            // 
            // labelControl3
            // 
            this.labelControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl3.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl3.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelControl3.Location = new System.Drawing.Point(862, 3);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(5, 3, 10, 3);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(122, 16);
            this.labelControl3.TabIndex = 18;
            this.labelControl3.Text = "Версия базы данных";
            // 
            // _appVersionLabel
            // 
            this._appVersionLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._appVersionLabel.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._appVersionLabel.Location = new System.Drawing.Point(847, 3);
            this._appVersionLabel.Margin = new System.Windows.Forms.Padding(5, 3, 10, 3);
            this._appVersionLabel.Name = "_appVersionLabel";
            this._appVersionLabel.Size = new System.Drawing.Size(0, 16);
            this._appVersionLabel.TabIndex = 19;
            // 
            // labelControl2
            // 
            this.labelControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelControl2.Location = new System.Drawing.Point(709, 3);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(5, 3, 10, 3);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(123, 16);
            this.labelControl2.TabIndex = 17;
            this.labelControl2.Text = "Версия приложения:";
            // 
            // _producerLabel
            // 
            this._producerLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._producerLabel.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._producerLabel.Appearance.ForeColor = System.Drawing.Color.Gray;
            this._producerLabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this._producerLabel.Location = new System.Drawing.Point(694, 3);
            this._producerLabel.Margin = new System.Windows.Forms.Padding(5, 3, 10, 3);
            this._producerLabel.Name = "_producerLabel";
            this._producerLabel.Size = new System.Drawing.Size(0, 16);
            this._producerLabel.TabIndex = 23;
            this._producerLabel.Click += new System.EventHandler(this._producer_Click);
            // 
            // labelControl4
            // 
            this.labelControl4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl4.Location = new System.Drawing.Point(584, 3);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(5, 3, 10, 3);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(95, 16);
            this.labelControl4.TabIndex = 22;
            this.labelControl4.Text = "Производитель:";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flowLayoutPanel1.BackColor = System.Drawing.Color.White;
            this.flowLayoutPanel1.Controls.Add(this._appNameLabel);
            this.flowLayoutPanel1.Controls.Add(this._descriptionLabel);
            this.flowLayoutPanel1.Controls.Add(this._richTextBox);
            this.flowLayoutPanel1.Controls.Add(this._documentationLabel);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Padding = new System.Windows.Forms.Padding(30);
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1009, 804);
            this.flowLayoutPanel1.TabIndex = 2;
            this.flowLayoutPanel1.WrapContents = false;
            // 
            // _appNameLabel
            // 
            this._appNameLabel.Appearance.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._appNameLabel.Appearance.ForeColor = System.Drawing.Color.Black;
            this.flowLayoutPanel1.SetFlowBreak(this._appNameLabel, true);
            this._appNameLabel.Location = new System.Drawing.Point(60, 50);
            this._appNameLabel.Margin = new System.Windows.Forms.Padding(30, 20, 0, 50);
            this._appNameLabel.Name = "_appNameLabel";
            this._appNameLabel.Size = new System.Drawing.Size(0, 29);
            this._appNameLabel.TabIndex = 16;
            // 
            // _descriptionLabel
            // 
            this._descriptionLabel.Appearance.BackColor = System.Drawing.Color.Transparent;
            this._descriptionLabel.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._descriptionLabel.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.flowLayoutPanel1.SetFlowBreak(this._descriptionLabel, true);
            this._descriptionLabel.Location = new System.Drawing.Point(30, 129);
            this._descriptionLabel.Margin = new System.Windows.Forms.Padding(0);
            this._descriptionLabel.Name = "_descriptionLabel";
            this._descriptionLabel.Size = new System.Drawing.Size(771, 0);
            this._descriptionLabel.TabIndex = 21;
            this._descriptionLabel.Visible = false;
            // 
            // _richTextBox
            // 
            this._richTextBox.Location = new System.Drawing.Point(33, 132);
            this._richTextBox.Name = "_richTextBox";
            this._richTextBox.Options.Fields.UseCurrentCultureDateTimeFormat = false;
            this._richTextBox.Options.HorizontalRuler.Visibility = DevExpress.XtraRichEdit.RichEditRulerVisibility.Hidden;
            this._richTextBox.Options.MailMerge.KeepLastParagraph = false;
            this._richTextBox.Options.VerticalRuler.Visibility = DevExpress.XtraRichEdit.RichEditRulerVisibility.Hidden;
            this._richTextBox.ReadOnly = true;
            this._richTextBox.Size = new System.Drawing.Size(944, 639);
            this._richTextBox.TabIndex = 24;
            // 
            // _documentationLabel
            // 
            this._documentationLabel.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._documentationLabel.Appearance.ForeColor = System.Drawing.Color.Gray;
            this._documentationLabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.flowLayoutPanel1.SetFlowBreak(this._documentationLabel, true);
            this._documentationLabel.Location = new System.Drawing.Point(30, 794);
            this._documentationLabel.Margin = new System.Windows.Forms.Padding(0, 20, 0, 20);
            this._documentationLabel.Name = "_documentationLabel";
            this._documentationLabel.Size = new System.Drawing.Size(99, 16);
            this._documentationLabel.TabIndex = 22;
            this._documentationLabel.Text = "Документация";
            this._documentationLabel.Click += new System.EventHandler(this._documentationLabel_Click);
            // 
            // ApplicationInfoControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.flowLayoutPanel1);
            this.MinimumSize = new System.Drawing.Size(640, 200);
            this.Name = "ApplicationInfoControl";
            this.Size = new System.Drawing.Size(1009, 804);
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private DevExpress.XtraEditors.LabelControl _dbVersionLabel;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl _appVersionLabel;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl _producerLabel;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.LabelControl _appNameLabel;
        private DevExpress.XtraEditors.LabelControl _descriptionLabel;
        private DevExpress.XtraEditors.LabelControl _documentationLabel;
        private DevExpress.XtraRichEdit.RichEditControl _richTextBox;


    }
}

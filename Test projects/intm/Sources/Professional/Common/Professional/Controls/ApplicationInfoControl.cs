﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.IO;

namespace Eureca.Professional.BaseComponents.Controls
{
    public partial class ApplicationInfoControl : XtraUserControl
    {
        private string _appVersion;

        public string AppVersion
        {
            get { return _appVersion; }
            set { _appVersion = value; }
        }

        private string _dbVersion;

        public string DbVersion
        {
            get { return _dbVersion; }
            set { _dbVersion = value;}
        }

        private string _description;

        public string AppDescription
        {
            get { return _description; }
            set { _description = value; }
        }

        private string _companyName;

        public string CompanyName
        {
            get { return _companyName; }
            set { _companyName = value; }
        }

        private string _companySiteAddress;

        public string CompanySiteAddress
        {
            get { return _companySiteAddress; }
            set { _companySiteAddress = value; }
        }

        private string _appName;

        public string ApplicationName
        {
            get { return _appName; }
            set { _appName = value; }
        }

        private string _documentationFile;

        public string DocumentationFile
        {
            get { return _documentationFile; }
            set { _documentationFile = value; }
        }

        public ApplicationInfoControl( )
        {
            InitializeComponent( );

            //this.ParentChanged += ApplicationInfoControl_ParentChanged;
            //flowLayoutPanel1.SizeChanged += flowLayoutPanel1_SizeChanged;
            _appVersionLabel.DataBindings.Add( new Binding( "Text", this, "AppVersion" ) );
            //_descriptionLabel.DataBindings.Add( new Binding( "Text", this, "AppDescription" ) );
            _richTextBox.DataBindings.Add( new Binding( "RtfText", this, "AppDescription" ) );
            _dbVersionLabel.DataBindings.Add( new Binding( "Text", this, "DbVersion" ) );
            _producerLabel.DataBindings.Add( new Binding( "Text", this, "CompanyName" ) );
            _appNameLabel.DataBindings.Add(new Binding( "Text", this, "ApplicationName" ) );
        }

        void UpdateLabelText(LabelControl control ,string text )
        {
            if (control.InvokeRequired)
                control.Invoke((Action)(( ) => control.Text = text ));
            control.Text = text;


        }

        private void _producer_Click(object sender, EventArgs e)
        {
           // if (_companySiteAddress != null && Uri.TryCreate(_companySiteAddress, UriKind.Absolute, out uriResult) && uriResult.Scheme == Uri.UriSchemeHttp)
            System.Diagnostics.Process.Start( _companySiteAddress );
        }

        private void _documentationLabel_Click( object sender, EventArgs e )
        {
            if(File.Exists(_documentationFile))
                System.Diagnostics.Process.Start( _documentationFile );
        }
    }
}

﻿using System;
using System.Drawing;
using Eureca.Professional.BaseComponents.Configuration;
using Eureca.Updater.Common;

namespace Eureca.Professional.BaseComponents
{
    /// <summary>
    /// класс контейнер для передачи информации для 
    /// </summary>
    public class StartUpParameters
    {
        private ARMSettingsSection _armSettings;

        public ARMSettingsSection ARMSettings
        {
            get { return _armSettings; }
            set { _armSettings = value; }
        }
        private Icon _applicationIcon;

        public Icon ApplicationIcon
        {
            get { return _applicationIcon; }
            set { _applicationIcon = value; }
        }
    }

    /// <summary>
    /// тип формы
    /// </summary>
    public enum ControlTypeAction
    {
        VIEW,
        INSERT,
        UPDATE
    } ;

    public class CommonMethods
    {
        /// <summary>
        /// метод установки времени
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="time"></param>
        /// <returns></returns>
        public static DateTime SetTime(DateTime dt, DateTime time)
        {
//создаем пустую дату и добавляем туда значения
            var NewDate = new DateTime();
            NewDate = NewDate.AddYears(dt.Year - 1);
            NewDate = NewDate.AddMonths(dt.Month - 1);
            NewDate = NewDate.AddDays(dt.Day - 1);
            NewDate = NewDate.AddHours(time.Hour);
            NewDate = NewDate.AddMinutes(time.Minute);
            NewDate = NewDate.AddSeconds(time.Second);
            return NewDate;
        }
    }
}
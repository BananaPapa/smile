using System.ComponentModel;
using System.Configuration;
using Eureca.Professional.CommonClasses.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Eureca.Professional.ActiveRecords;

namespace Eureca.Professional.BaseComponents.Configuration
{
    [ConfigurationSectionParams("ARMSettingsSection", true)]
    public class ARMSettingsSection 
        : SerializableConfigurationSection
    {
        public ARMSettingsSection()
        {
            UserID = -1;
            ARMType = ARMType.PROBATIONER;
        }

        [ConfigurationProperty("UserID"),
         CommonClasses.Configuration.DisplayName("������������� �������� ������������"),
         Description("������������� �������� ������������"),
         Category("1. ��������"),
         PropertyIndex(-1)]
        public int UserID
        {
            get { return (int) this["UserID"]; }
            set { this["UserID"] = value; }
        }

        [ConfigurationProperty("ARMType"),
         CommonClasses.Configuration.DisplayName("��� ����"),
         Description("��� ����"),
         Category("1. ��������"),
         PropertyIndex(-1)]
        public ARMType ARMType
        {
            get { return (ARMType) this["ARMType"]; }
            set { this["ARMType"] = value; }
        }

        ///// <summary>
        ///// ����� ��� ������� ������� � ��������� ������ � ��������� �������.
        ///// </summary>
        //[
        //    ConfigurationProperty("AttachmentsDirectory"),
        //    Category("1.���������� ��� ������������� ������"),
        //    DisplayName("������������"),
        //    Description("����� ��� ������������� ������ " +
        //                "���������� �� ��."),
        //    Editor(typeof (FolderNameEditor),
        //        typeof (UITypeEditor)),
        //    PropertyIndex(0)
        //]
        //public string AttachmentsDirectory
        //{
        //    get { return (string) this["AttachmentsDirectory"]; }
        //    set { this["AttachmentsDirectory"] = value; }
        //}


        public static ARMSettingsSection GetFromConfigSource()
        {
            var section =
                (ARMSettingsSection) Settings.GetSection(typeof (ARMSettingsSection));

            return section;
        }
    }
}
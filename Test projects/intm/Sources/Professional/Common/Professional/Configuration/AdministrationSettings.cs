using System;
using System.ComponentModel;
using System.Configuration;
using System.Drawing.Design;
using System.Windows.Forms.Design;
using Eureca.Professional.ActiveRecords;
using Eureca.Professional.CommonClasses.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using DisplayName = Eureca.Professional.CommonClasses.Configuration.DisplayNameAttribute;

namespace EurecaCorp.Professional.BaseComponents.Configuration
{
    [ConfigurationSectionParams("AdministrationSettingsSection", true)]
    public class AdministrationSettingsSection : SerializableConfigurationSection
    {
        public AdministrationSettingsSection()
        {
            DBFilesDirectory = @"C:\Professional\DBFiles";
            DBBackUpFilesDirectory = @"C:\Professional\BackUp";
            EnableBackUpTimer = false;
            BackUpTimerInterval = 24;
            BackUpTimerElapsedDate = DateTime.MinValue;
        }

        /// <summary>
        /// ����� ��� ������� ��� ������� ������������� ������ �� ��
        /// </summary>
        [
            ConfigurationProperty("DBFilesDirectory"),
            Category("1.���������� ��� ������ ��"),
            DisplayName("���������� ������ ��"),
            Description("����� ��� ������ ��"),
            Editor(typeof (FolderNameEditor),
                typeof (UITypeEditor)),
            PropertyIndex(0)
        ]
        public string DBFilesDirectory
        {
            get { return (string) this["DBFilesDirectory"]; }
            set { this["DBFilesDirectory"] = value; }
        }

        /// <summary>
        /// ����� ��� ������� ��� ������� ������������� ������ �� ��
        /// </summary>
        [
            ConfigurationProperty("DBBackUpFilesDirectory"),
            Category("1.���������� ��� ������ ��"),
            DisplayName("���������� BackUp ������"),
            Description("����� ��� BackUp ������ ��"),
            Editor(typeof (FolderNameEditor),
                typeof (UITypeEditor)),
            PropertyIndex(1)
        ]
        public string DBBackUpFilesDirectory
        {
            get { return (string) this["DBBackUpFilesDirectory"]; }
            set { this["DBBackUpFilesDirectory"] = value; }
        }

        /// <summary>
        /// ����� ��� ������������� ����������
        /// </summary>
        [
            ConfigurationProperty("AttachmentsFolder"),
            Category("1.���������� ��� ������������� ����������"),
            DisplayName("���������� ��� ������������� ����������"),
            Description("���������� ��� ������������� ����������"),
            Editor(typeof(FolderNameEditor),
                typeof(UITypeEditor)),
            PropertyIndex(1)
        ]
        public string AttachmentsFolder
        {
            get { return (string)this["AttachmentsFolder"]; }
            set
            {
                this["AttachmentsFolder"] = value;
                
                var item = SystemProperties.GetItem(SysPropEnum.AttachmentsFolder);
                if (item == null)
                {
                    item = new SystemProperties(SysPropEnum.AttachmentsFolder, string.Empty);
                    item.Create();
                }
                //���������� ����� ��������
                item.Value = value;
                item.UpdateAndFlush();

            }
        }


        /// <summary>
        /// ���� ������� ������� ��������� ������
        /// </summary>
        [
            ConfigurationProperty("EnableBackUpTimer"),
            Category("2.������������ ������ �������� ����� ��"),
            DisplayName("��������� ����������� ��"),
            //Description("����� ��� ������ ��"), 
            PropertyIndex(0)
        ]
        public bool EnableBackUpTimer
        {
            get { return (bool)this["EnableBackUpTimer"]; }
            set { this["EnableBackUpTimer"] = value; }
        }
       
        /// <summary>
        /// �������� �������� �����
        /// </summary>
        [
            ConfigurationProperty("BackUpTimerInterval"),
            Category("2.������������ ������ �������� ����� ��"),
            DisplayName("�������� ���������� �����������, ����"),
            //Description("����� ��� ������ ��"),
            PropertyIndex(1)
        ]
        public int BackUpTimerInterval
        {
            get { return (int)this["BackUpTimerInterval"]; }
            set { this["BackUpTimerInterval"] = value; }
        }

        /// <summary>
        /// ���� ���������� �������� �����
        /// </summary>
        [
            ConfigurationProperty("BackUpTimerElapsedDate"),
            Category("2.������������ ������ �������� ����� ��"),
            DisplayName("���� ���������� ���������� �����������"),
            //Description("����� ��� ������ ��"),
            PropertyIndex(1)
        ]
        public DateTime BackUpTimerElapsedDate
        {
            get { return (DateTime)this["BackUpTimerElapsedDate"]; }
            set { this["BackUpTimerElapsedDate"] = value; }
        }

        public static AdministrationSettingsSection GetFromConfigSource()
        {
            var section =
                (AdministrationSettingsSection) Settings.GetSection(typeof (AdministrationSettingsSection));

            return section;
        }
    }
}
using System;
using System.ComponentModel;
using System.Configuration;
using System.Drawing.Design;
using System.Net;
using System.Windows.Forms.Design;
using Eureca.Professional.CommonClasses;
using Eureca.Professional.CommonClasses.Configuration;
using log4net;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;

namespace Eureca.Professional.BaseComponents.Configuration
{
    [ConfigurationSectionParams("UserSettingsSection", true)]
    public class UserSettingsSection : SerializableConfigurationSection
    {
        private static readonly ILog Log = LogManager.GetLogger( typeof( UserSettingsSection ) );

        private string _defaultServerIp = "127.0.0.1";

        public UserSettingsSection()
        {
            AttachmentsDirectory = @"C:\Professional\Attachments";
            ServerIp = _defaultServerIp;
        }

        /// <summary>
        /// ����� ��� ������� ��� ������� ������������� ������ �� ��
        /// </summary>
        [
            ConfigurationProperty("AttachmentsDirectory"),
            Category("1.���������� ��� ������������� ������"),
            CommonClasses.Configuration.DisplayName("������������"),
            Description("����� ��� ������������� ������ " +
                        "���������� �� ��."),
            Editor(typeof (FolderNameEditor),
                typeof (UITypeEditor)),
            PropertyIndex(0)
        ]
        public string AttachmentsDirectory
        {
            get { return (string) this["AttachmentsDirectory"]; }
            set { this["AttachmentsDirectory"] = value; }
        }


        /// <summary>
        /// IP ����� �������
        /// </summary>
        [
            ConfigurationProperty("ServerIp"),
            Category("2.Ip ����� �������"),
            CommonClasses.Configuration.DisplayName("IP"),
            Description("Ip ����� �������"),
            Editor(typeof (FolderNameEditor),
                typeof (UITypeEditor)),
            PropertyIndex(0)
        ]
        public string ServerIp
        {
            get { return (string) this["ServerIp"]; }
            set { this["ServerIp"] = value; }
        }


        public static UserSettingsSection GetFromConfigSource()
        {
            var section =
                (UserSettingsSection) Settings.GetSection(typeof (UserSettingsSection));

            try
            {
                IPAddress.Parse(section.ServerIp);
                return section;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                section.ServerIp = section._defaultServerIp;
                Settings.Save(section);
                return section;
            }
        }
    }
}
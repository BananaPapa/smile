﻿using System;
using System.Collections.Generic;
using System.Data.EntityClient;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Castle.ActiveRecord;
using Castle.ActiveRecord.Framework.Config;
using DevExpress.XtraEditors;
using Eureca.Integrator.Common;
using Eureca.Integrator.Utility;
using Eureca.Integrator.Utility.Forms;
using Eureca.Integrator.Utility.Settings;
using Eureca.Professional.ActiveRecords;
using Eureca.Professional.BaseComponents.Controls;
using Eureca.Professional.CommonClasses;
using Eureca.Professional.CommonClasses.Configuration;
using Eureca.Professional.FillingFormsTest;
using Eureca.Updater.DbUpdater;
using Eureca.Updater.UpdaterClient;
using Eureca.Utility.Extensions;
using log4net;
using TestPlugins;
using Event = Eureca.Professional.ActiveRecords.Event;
using MessageBox = Eureca.Integrator.Utility.Forms.MessageBox;
using Report = Eureca.Professional.ActiveRecords.Report;
using User = Eureca.Professional.ActiveRecords.User;

namespace Eureca.Professional.BaseComponents
{
    /// <summary>
    /// класс для запуска приложений комплекса профессионал
    /// </summary>
    public static class RunnerApplication
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof (RunnerApplication));

        private static IJournalModuleManager _journalManager;

        private static LocalUserConfig _localUserConfig;

        private static string _processName = Process.GetCurrentProcess().ProcessName;

        const string checkBaseQuery = "Select GetDate()";
        /// <summary>
        /// текущий пользователь
        /// </summary>
        public static User CurrentUser { get; set; }

        public static ARMType CurrentArm { get; set; }
        public static bool ExitUser { get; set; }

        private static UpdaterClient _updaterClient = new UpdaterClient( AppVersion.ApplicationName, AppVersion.ApplicationVersion );

       
        private static DbUpdater _dbUpdater;

        public static UpdaterClient UpdaterClient
        {
            get { return _updaterClient; }
            set { _updaterClient = value; }
        }

        private static ProfessionalServerSettingsImpl _pContext;
        public static ProfessionalServerSettingsImpl UpdateSettingsContext
        {
            get { return _pContext; }
        }

        static Icon _helpIcon;
        static XtraForm _showInfoForm;
        static object _showInfoFormLock = new object( );
        public static Icon HelpIcon
        {
            get
            {
                if (_helpIcon == null)
                    _helpIcon = System.Drawing.Icon.FromHandle( BaseComponents.Properties.Resources.help.GetHicon( ) );
                return _helpIcon;
            }
        }

        public static IJournalModuleManager JournalManager
        {
            get
            {
                if (_journalManager == null)
                {
                    _journalManager = new JournalModuleManager();
                    if (_journalManager.InitDataBase(
                        ConnectionStringManager.GetProviderConnectionString(
                            CommonConfig.Load().ProfessionalConnectionString)))
                        throw new InvalidOperationException("Ошибка подключения к бд модуля ЗЖУ.");
                }
                return _journalManager;
                
            }
        }

        /// <summary>
        /// метод старта приложения
        /// </summary>
        /// <param name="connectionCheckFunc"> Функция проверки корректности выбранного подключения. </param>
        /// <returns></returns>
        public static bool ConnectDb( Func<string, bool> connectionCheckFunc = null )
        {
            var db = DatabaseSettingsSection.GetFromConfigSource();
            string connString = db.ConnectionString, profDbName = IntegratorDb.Professional.GetEnumDescription( );
            if (String.IsNullOrEmpty( connString ))
                connString = String.Format( "Server={0};DataBase={1};Trusted_Connection=True", ".\\sqlexpress2012", profDbName );
            string resultConnnectionString;
            //подключение к БД
            bool exit = false;

            if (!ConnectToDataBase( connString, connectionCheckFunc, out resultConnnectionString ))
                return false;
            while (!exit)
            {
                //инициализация ActiveRecords
                try
                {
                    InitActiveRecords(resultConnnectionString);
                    exit = true;
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                    if (MessageForm.ShowYesNoQuestionMsg( "Выбрана некорректная база данных.\n Повторить попытку подключения?") == DialogResult.No)
                    {
                        return false;
                    }
                    ResetInitActiveRecord();
                    ChangeConnectionToDataBase( connString, out resultConnnectionString, connectionCheckFunc );
                }
            }
            //сохраняю строку подключения
            db.ConnectionString = resultConnnectionString;
            Settings.Save();
            return true;
        }

        public static bool Authotization( StartUpParameters param )
        {
            if (param.ARMSettings == null)
                throw new Exception( "Пустой параметр конфигурирования" );
            //вызов контрола ввода логина
            if (!ChangeUser( param ))
            {
                ExitUser = true;
                return false;
            }
            return true;
        }

        /// <summary>
        /// метод перезапуска флага инициализации
        /// </summary>
        public static void ResetInitActiveRecord()
        {
            ActiveRecordStarter.ResetInitializationFlag();
        }

        /// <summary>
        /// метод смены подключения к БД
        /// показывается форма настроек и происходит подключение
        /// </summary>
        /// <returns></returns>
        public static bool ChangeConnectionToDataBase( string connectionString, out string resultConnectionString, Func<string, bool> connectionCheckFunc = null )
        {
            if(String.IsNullOrEmpty(connectionString))
                connectionString = "Server=.\\sqlexpress2012;Database=master;Trusted_Connection=True";
            var cndbForm = new SqlServerConnectionForm( connectionString, connectionCheckFunc );

            while (true)
            {
                try
                {
                    DialogResult rslt = cndbForm.ShowDialog();

                    if (rslt == DialogResult.OK )
                    {
                        //var db = DatabaseSettingsSection.GetFromConfigSource();
                        //db.ConnectionString = cndbForm.connDB1.ConnectionString;
                        //Settings.Save();
                        resultConnectionString = cndbForm.ConnectionString;
                        return true;
                    }
                    if (rslt == DialogResult.Cancel)
                    {
                        resultConnectionString = string.Empty;
                        return false;
                    }
                    //MessageForm.ShowErrorMsg(cndbForm.connDB1.Err);
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                    MessageForm.ShowErrorMsg(ex.Message);
                }
            }
        }

        /// <summary>
        /// метод подключения к БД
        /// Если подключение успешно, то форма не показывается
        /// </summary>
        /// <returns></returns>
        private static bool ConnectToDataBase(string connectionString, Func<string, bool> connectionCheckFunc, out string resultConnectionString)
        {
            try
            {
                if (String.IsNullOrEmpty(connectionString) || !DatabaseHelper.CheckConnection(connectionString,checkBaseQuery))
                {
                    return ChangeConnectionToDataBase( connectionString , out resultConnectionString, connectionCheckFunc );
                }
                //var db = DatabaseSettingsSection.GetFromConfigSource();
                //db.ConnectionString = cndbForm.connDB1.ConnectionString;
                //Settings.Save();
                resultConnectionString = connectionString;
                return true;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                MessageForm.ShowErrorMsg(ex.Message);
            }
            resultConnectionString = string.Empty;
            return true;
        }

        /// <summary>
        /// метод инициализации ActiveRecords
        /// </summary>
        public static void InitActiveRecords(string connectionString)
        {
            var properties = new Dictionary<string,string>();
            properties.Add(NHibernate.Cfg.Environment.ConnectionDriver, "NHibernate.Driver.SqlClientDriver");
            properties.Add(NHibernate.Cfg.Environment.Dialect, "NHibernate.Dialect.MsSql2008Dialect");
            properties.Add(NHibernate.Cfg.Environment.ConnectionProvider, "NHibernate.Connection.DriverConnectionProvider");
            properties.Add(NHibernate.Cfg.Environment.ConnectionString, connectionString);
            properties.Add(NHibernate.Cfg.Environment.ProxyFactoryFactoryClass,"NHibernate.ByteCode.Castle.ProxyFactoryFactory, NHibernate.ByteCode.Castle");
            
            var source = new InPlaceConfigurationSource();

            source.Add(typeof (ActiveRecordBase), properties);
            //source.Debug = true;
            //source.
            ActiveRecordStarter.Initialize(source,
                typeof (Test),
                typeof (TestType),
                typeof (Method),
                typeof (Content),
                typeof (Answer),
                typeof (Question),
                typeof (TestAnswer),
                typeof (TestQuestion),
                typeof (QuestionAnswer),
                typeof (ControlQuestion),
                typeof (MemorizationInfo),
                typeof (Probationer),
                typeof (Profile),
                typeof (ProfilePassing),
                typeof (RawPoint),
                typeof (Scale),
                typeof (Sten),
                typeof (TestPassing),
                typeof (TestResult),
                typeof (TestType),
                typeof (TimeInterval),
                typeof (User),
                typeof (EventType),
                typeof (Event),
                typeof (ArmApplication),
                typeof (Report),
                typeof (ReportField),
                typeof (SystemProperties),
                typeof (SuperviseAdresses),
                typeof (Comment),
                typeof (RtfForm),
                typeof (FormFilling),
                typeof (Journal),
                typeof (JournalView),
                typeof (JournalBranch),
                typeof (FormFilling ),
                typeof (FillingFormsTestResult),
                typeof (QuestionJournals),
                typeof ( vStatistic ),
                typeof (vUnverifiedAssignments)
                );
            //проверка на инициализацию именно этой БД
//#if DEBUG
            //TestQuestion question;
            //using (new SessionScope())
            //{
            //     question = TestQuestion.FindAll()[0];
            //}
//#endif
            //Trace.WriteLine(question.Question.Id);
            ActiveRecordBase<User>.FindAll();
        }

        public static void InitProfessionalContext( string profConnectionString )
        {
            _pContext = new ProfessionalServerSettingsImpl( profConnectionString );
        }

        public static int PreviousUserID
        {
            get
            {
                if (_localUserConfig == null)
                    _localUserConfig = LocalUserConfig.Load();
                
                if (_localUserConfig.PreviousUsers.ContainsKey(_processName))
                {
                    return _localUserConfig.PreviousUsers[_processName];
                }
                return -1;
            }

            set
            {
                if (_localUserConfig == null)
                    _localUserConfig = LocalUserConfig.Load( );
                _localUserConfig.PreviousUsers[_processName] = value;
                _localUserConfig.Save();
            }
        }

        public static bool RunUpdater( bool canAccept = true ,bool isProfessional = true )
        {
            _updaterClient = new UpdaterClient( AppVersion.ApplicationName, AppVersion.ApplicationVersion );

            string profConnectionString = null, curriculumConnectionString = null;
            GetConnectionsStrings( isProfessional, out profConnectionString, out curriculumConnectionString );

            if (!DatabaseHelper.CheckConnection( curriculumConnectionString, checkBaseQuery ) || !DatabaseHelper.CheckConnection( profConnectionString,checkBaseQuery ))
            {
                MessageBox.ShowInfo( "Не доступна одна из баз, проверка обновлений отключена" );
                return false;
            }

            if (_pContext == null)
            {
                _pContext = new ProfessionalServerSettingsImpl( profConnectionString );
                _pContext.SettingsChanged += pContext_SettingsChanged;
            }


            if (_pContext.StorageVersion != AppVersion.TargetDbVersion)
            {
                _dbUpdater = new DbUpdater( AppVersion.ApplicationName, new DbDescriptor[]
            {new DbDescriptor(){ConnectionString = curriculumConnectionString,DbName = IntegratorDb.Curriculum.GetEnumDescription() },
                new DbDescriptor(){ConnectionString =profConnectionString,DbName = IntegratorDb.Professional.GetEnumDescription() }},
                     Path.Combine( Directory.GetParent( Environment.CurrentDirectory ).FullName, "DbUpdates" ), _pContext );
                bool updateSuccessfull = false;
                do
                {
                    updateSuccessfull = _dbUpdater.Update(() => { return (new UpdateForm()); },
                        AppVersion.TargetDbVersion);
                    if (updateSuccessfull)
                        break;
                    if (!updateSuccessfull && !MessageBox.Confirm("Обновление базы данных завершилось ошибкой. Повторить обновление?"))
                        break;
                    _dbUpdater.CloseLogger();
                } while (true);
                if(!updateSuccessfull && !MessageBox.Confirm("Дальнейшая работа приложения может привести к ошибкам, продолжить работу?"))
                    Environment.Exit(0);
                return false;
            }

            var settings = _pContext.RepositorySettings;
            if (settings != null)
                foreach (var setting in settings)
                    _updaterClient.AddRepository( setting );
            if(canAccept)
                _updaterClient.UpdatesReady += _updaterClient_UpdatesReady;
            _updaterClient.RequestShutdown += _updaterClient_RequestShutdown;
            _updaterClient.UpdatingCanceled += _updaterClient_UpdatingCanceled;
            _updaterClient.Start( );
            //RunnerApplication.UpdaterClient.Start( );
            return true;
        }

        private static void GetConnectionsStrings( bool isProfessional, out string profConnectionString, out string curriculumConnectionString )
        {
            string localCurriculumConString = null, localProfessionalConString = null;

            if (isProfessional)
            {
                var localSettings = DatabaseSettingsSection.GetFromConfigSource( );
                localCurriculumConString = localSettings.CurriculumConnectionString;
                localProfessionalConString = localSettings.ConnectionString;
            }
            else
            {
                EntityConnectionStringBuilder connStringBuilder = new EntityConnectionStringBuilder( );
                connStringBuilder.ConnectionString = ConnectionStringManager.GetLocalConnectionString( IntegratorDb.Curriculum );
                localCurriculumConString = connStringBuilder.ProviderConnectionString;
                connStringBuilder.ConnectionString = ConnectionStringManager.GetLocalConnectionString( IntegratorDb.Professional );
                localProfessionalConString = connStringBuilder.ProviderConnectionString;
            }

            var globalConfig = CommonConfig.Load( );
            profConnectionString = String.IsNullOrEmpty( localProfessionalConString ) ? ConnectionStringManager.GetProviderConnectionString( globalConfig.ProfessionalConnectionString ) : localProfessionalConString;
            
            curriculumConnectionString = String.IsNullOrEmpty( localCurriculumConString ) ? ConnectionStringManager.GetProviderConnectionString( globalConfig.CurriculumConnectionString ) : localCurriculumConString;
        }

        static void _updaterClient_UpdatingCanceled( object sender, EventArgs e )
        {
            //Trace.WriteLine( "Call CloseLogger form" );
            List<IAsyncResult> asyncResults = new List<IAsyncResult>( );
            Application.OpenForms.OfType<XtraMessageBoxForm>( ).ForEach( item => asyncResults.Add( item.BeginInvoke( (Action)( ( ) =>
            {
                if (Thread.CurrentThread.ManagedThreadId == _updaterClient.RequestExecutionThreadId)
                {
                    item.Close( );
                    item.Dispose( );
                }
            }))));
            Task.Factory.StartNew( ( ) => { foreach (var item in asyncResults) item.AsyncWaitHandle.WaitOne( 1000 ); } ).Wait();
        }

        static void _updaterClient_UpdatesReady( object sender, UpdatesReadyEventArgs e )
        {
            var upInfo = e.UpdateInfo;
            StringBuilder sb = new StringBuilder( );
            sb.Append( "Готово новое{0} обновление номер: {1}".FormatString( upInfo.IsCritical ? " критическое" : "", upInfo.Version.ToString( ) ) );

            if (upInfo.Description != null)
            {
                sb.AppendLine();
                sb.Append( "Дополнительная информация:\n\t" );
                sb.Append( upInfo.Description );
            }
            var res = MessageBox.Show( sb.ToString( ), "Доступно новое обновление. Установить?", null, MessageBoxIcon.Question, MessageBoxButtons.YesNo, MessageBoxDefaultButton.Button1 );
            RunnerApplication.UpdaterClient.ConfirmUpdate( upInfo, res == DialogResult.Yes );
        }

        static void _updaterClient_RequestShutdown( object sender, EventArgs e )
        {
            Application.Exit( );
        }


        static void pContext_SettingsChanged( object sender, OnSettingChangedArgs e )
        {
            if (e.NewSettings == null || e.NewSettings.Count == 0)
                return;
            IEnumerable<string> errorEnumeration = null;
            e.NewSettings.Where( item => item.AreValid( out errorEnumeration ) ).ForEach( item => _updaterClient.AddRepository( item ));
            if (errorEnumeration != null)
                throw new Exception( "Thera are errors on pass settings to service:\n" + string.Join( "\n", errorEnumeration ) );
        }

        /// <summary>
        /// смена пользователя
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        private static bool ChangeUser(StartUpParameters param)
        {
            //выход пользователя
            if (CurrentUser != null)
            {
                using (new SessionScope())
                {
                    try
                    {
                        Event.Add(CurrentUser, EventTypes.ExitUser,
                                  ArmApplication.GetArmApplication(param.ARMSettings.ARMType).Id);
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex);
                    }
                }
                CurrentUser = null;
            }

            bool isAdmin = true;
            if (param.ARMSettings.ARMType == ARMType.PROBATIONER)
                isAdmin = false;
            var uc = new LoginControl( isAdmin, param.ARMSettings.UserID ) ;
            if (param.ApplicationIcon != null)
                uc.Icon = param.ApplicationIcon;
            if (uc.ShowDialog() == DialogResult.OK)
            {
                CurrentUser = uc.LogonUser;
                param.ARMSettings.UserID = CurrentUser.Id;
                using (new SessionScope())
                {
                    try
                    {
                        Event.Add(CurrentUser, EventTypes.EnterUser,
                                  ArmApplication.GetArmApplication(param.ARMSettings.ARMType).Id);
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex);
                    }
                }
                return true;
            }
            return false;
        }


        /// <summary>
        /// проверка на повторный запущенный процесс
        /// </summary>
        /// <returns></returns>
        public static bool AlreadyRunning()
        {
            Process[] prc = Process.GetProcessesByName(Process.GetCurrentProcess().ProcessName);

            if (prc.Length > 1)
            {
                MessageForm.ShowErrorMsg("Данное приложение уже запущено");
                Process.GetCurrentProcess().Kill();
                return true;
            }
            return false;
        }

        public static void ShowInfoPage( )
        {
            if (_showInfoForm == null)
            {
                lock (_showInfoFormLock)
                {
                    if (_showInfoForm == null)
                    {
                        var applicationControl = new ApplicationInfoControl( )
                        {
                            Dock = DockStyle.Fill,
                            DbVersion = _pContext.StorageVersion.ToString( ),
                            AppVersion = AppVersion.ApplicationVersion.ToString( ),
                            ApplicationName = AppVersion.ApplicationRuName,
                            AppDescription = BaseComponents.Properties.Resources.description,
                            CompanyName = "Эврика",
                            CompanySiteAddress = "www.eureca.ru",
                        };
                        _showInfoForm = new XtraForm( ) {Text = "Справка", Width  = applicationControl.Size.Width + 15, Height = applicationControl.Size.Height + 30, Icon = HelpIcon };
                        _showInfoForm.MinimumSize = _showInfoForm.Size;
                        _showInfoForm.FormClosed += _showInfoForm_FormClosed;
                        _showInfoForm.Controls.Add( applicationControl );
                        _showInfoForm.FormBorderStyle = FormBorderStyle.FixedDialog;
                        _showInfoForm.MaximizeBox = false;
                        _showInfoForm.MinimizeBox = false;
                        _showInfoForm.StartPosition = FormStartPosition.CenterScreen;
                        _showInfoForm.Show( );
                    }
                }
            }
            else
            {
                _showInfoForm.Focus( );
                return;
            }
            
        }

        static void _showInfoForm_FormClosed( object sender, FormClosedEventArgs e )
        {
            lock(_showInfoFormLock)
            {
                _showInfoForm = null;
            }
        }
    }
}
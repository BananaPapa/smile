﻿namespace Eureca.Professional.BaseComponents
{
    partial class ProfessionalStartForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            ((System.ComponentModel.ISupportInitialize)(this.workPanel)).BeginInit();
            this.SuspendLayout();
            // 
            // workPanel
            // 
            this.workPanel.Appearance.ForeColor = System.Drawing.Color.Transparent;
            this.workPanel.Appearance.Options.UseForeColor = true;
            this.workPanel.LookAndFeel.SkinName = "Blue";
            // 
            // ProfessionalStartForm
            // 
            this.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.Appearance.Options.UseForeColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(518, 546);
            this.LookAndFeel.SkinName = "Black";
            this.Name = "ProfessionalStartForm";
            this.Text = "ProfessionalStartForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ProfessionalStartFormFormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.workPanel)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

    }
}
﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Castle.ActiveRecord;
using Eureca.Professional.CommonClasses;
using Eureca.Professional.ActiveRecords;
using System.Linq;
using NHibernate.Criterion;
namespace Eureca.Professional.BaseComponents
{
    public partial class LoginControl : Form
    {
        #region поля

        private readonly int _lastUserId;
        private bool _isAdmin;
        private readonly List<User> _users = new List<User>();
        private User _user;

        #endregion

        #region свойства

        public User LogonUser
        {
            get { return _user; }
        }

        #endregion

        public LoginControl(bool isAdmin, int lastUserId)
        {
            _isAdmin = isAdmin;
            _lastUserId = lastUserId;
            InitializeComponent();
        }

        private void AcceptLogin()
        {
            if (comboBoxEditUsers.SelectedIndex == -1)
            {
                MessageForm.ShowErrorMsg("Необходимо выбрать пользователя");
                return;
            }
            _user = (User)comboBoxEditUsers.SelectedItem;
            if (_user.Password == textEditPassword.Text)
            {
               DialogResult = DialogResult.OK;
            }
            else
            {
                MessageForm.ShowErrorMsg("Вы ввели некорректный пароль. \nПопробуйте ввести правильный пароль.");
                _user = null;
            }
        }


        /// <summary>
        /// метод вставки данных из БД
        /// </summary>
        private void InsertData()
        {
            _users.Clear();
            var users = ActiveRecordBase<User>.FindAll(Expression.Eq("IsArchive", false)).OrderBy(item => item.LastName);
            _users.AddRange(_isAdmin
                                ? users.Where(items => items.Probationer == null)
                                : users.Where(items => items.Probationer != null));
            comboBoxEditUsers.Properties.Items.Clear();
            comboBoxEditUsers.Properties.Items.AddRange(_users.ToArray());
            if (comboBoxEditUsers.Properties.Items.Count > 0)
            {
                comboBoxEditUsers.SelectedIndex = 0;
            }
            if (_lastUserId != -1)
                foreach (User user in comboBoxEditUsers.Properties.Items)
                {
                    if (user.Id == _lastUserId)
                    {
                        comboBoxEditUsers.SelectedItem = user;
                        break;
                    }
                }
        }

        /// <summary>
        /// кнопка "Ок"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButtonOk_Click(object sender, EventArgs e)
        {
            AcceptLogin();
        }


        private void simpleButtonExit_Click(object sender, EventArgs e)
        {
        }

        private void textEditPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                AcceptLogin();
            }
        }

        private void LoginControl_Load(object sender, EventArgs e)
        {
            InsertData();

        }

        private void LoginControl_Shown(object sender, EventArgs e)
        {
            Activate();
            textEditPassword.Focus();
        }
    }
}
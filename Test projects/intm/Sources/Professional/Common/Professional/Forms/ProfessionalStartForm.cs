﻿using System;
using System.Windows.Forms;
using Castle.ActiveRecord;
using Eureca.Professional.BaseComponents.Configuration;
using Eureca.Professional.CommonClasses;
using Eureca.Professional.ActiveRecords;
using Eureca.Professional.CommonClasses.Configuration;
using Eureca.Utility.Extensions;
using Eureca.Integrator.Utility;
using Eureca.Common.Settings;
using Eureca.Updater.UpdaterClient;
using Eureca.Integrator.Utility.Settings;
using System.Data.EntityClient;
using Eureca.Professional.BaseComponents.Controls;
using log4net;

namespace Eureca.Professional.BaseComponents
{
    public partial class ProfessionalStartForm : TestingSkinForm
    {
        private static readonly ILog Log = LogManager.GetLogger( typeof( ProfessionalStartForm ) );

        public ProfessionalStartForm()
        {
            InitializeComponent();
        }

        private void ProfessionalStartFormFormClosing(object sender, FormClosingEventArgs e)
        {
            if (RunnerApplication.CurrentUser != null)
            {
                using (new SessionScope())
                {
                    try
                    {
                        Event.Add(RunnerApplication.CurrentUser, EventTypes.ExitUser,
                                  ArmApplication.GetArmApplication(RunnerApplication.CurrentArm).Id);
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex);
                    }
                }
                RunnerApplication.CurrentUser = null;
            }
        }

        public static void ConnectDbClick()
        {
            var db = DatabaseSettingsSection.GetFromConfigSource();
            string connectionString = db.ConnectionString;
            string resConnectionString;
            //производим смену подключения
            if (RunnerApplication.ChangeConnectionToDataBase( connectionString, out resConnectionString, el => el.Contains( IntegratorDb.Professional.GetEnumDescription( ) ) ))
            {
                //проверяем изменилась ли строка подключения
                if (resConnectionString.CompareTo(db.ConnectionString) == 0)
                {
                    //строка подключения не изменилась
                    return;
                }
                //обновляем строку подключения к БД
                db.ConnectionString = resConnectionString;
                //выходим пользователем
                if (RunnerApplication.CurrentUser != null)
                {
                    using (new SessionScope())
                    {
                        try
                        {
                            Event.Add(RunnerApplication.CurrentUser, EventTypes.ExitUser,
                                      ArmApplication.GetArmApplication(RunnerApplication.CurrentArm).Id);
                        }
                        catch (Exception ex)
                        {
                            Log.Error(ex);
                        }
                    }
                    RunnerApplication.CurrentUser = null;
                }

                //строка подключения изменилась, небоходимо вызвать авторизацию
                //делаем переключение флага инициализации для того чтобы мы смогли повторно ингициализировать
                RunnerApplication.ResetInitActiveRecord();
                //происходит новая инициализация
                var par = new StartUpParameters
                              {
                                  ARMSettings = ARMSettingsSection.GetFromConfigSource()
                              };
                if (!RunnerApplication.ConnectDb( ) && !RunnerApplication.Authotization( par ))
                {
                    AskBeforeExit = false;
                    Application.Exit();
                }
            }
        }

        public static void ConnectCurriculumDb( )
        {
            var item = SystemProperties.GetItem(SysPropEnum.CurriculumConnString);
            if (item == null)
            {
                item = new SystemProperties(SysPropEnum.CurriculumConnString, string.Empty);
                item.Create();
            }
            string resConnectionString;
            //производим смену подключения
            if (RunnerApplication.ChangeConnectionToDataBase( item.Value, out resConnectionString, el => el.Contains( IntegratorDb.Curriculum.GetEnumDescription( ) ) ))
            {
                    item.Value = resConnectionString;
                    item.UpdateAndFlush();
            }
        }


        public static void ChangeUpdateSettings( IUpdateSettingStorage settingStorage )
        {
            Form f = new Form( ) { Text = "Настройки репозитариев.", Size = new System.Drawing.Size( 600, 400 ) };
            UpdatesSettingsControl control = new UpdatesSettingsControl( ) { RepositorySettings = settingStorage, Dock = DockStyle.Fill };
            f.Controls.Add( control );
            f.ShowDialog( );
        }


        public static void SyncProfSettings( )
        {
            var globalConfig = CommonConfig.Load( );
            var localSettings = DatabaseSettingsSection.GetFromConfigSource( );
            string globCurrCS = ConnectionStringManager.GetProviderConnectionString( globalConfig.CurriculumConnectionString );
            string globProfCS = ConnectionStringManager.GetProviderConnectionString( globalConfig.ProfessionalConnectionString );
            bool globalSettingsChanged = false;
            EntityConnectionStringBuilder connStringBuilder = new EntityConnectionStringBuilder( );
            if (string.IsNullOrWhiteSpace( globCurrCS ))
            {
                connStringBuilder.ProviderConnectionString = localSettings.CurriculumConnectionString;
                globalConfig.CurriculumConnectionString = connStringBuilder.ConnectionString;
                globalSettingsChanged = true;
            }
            if (string.IsNullOrWhiteSpace( globalConfig.ProfessionalConnectionString ))
            {
                connStringBuilder.ProviderConnectionString = localSettings.ConnectionString;
                globalConfig.ProfessionalConnectionString = connStringBuilder.ConnectionString;
                globalSettingsChanged = true;
            }
            if (globalSettingsChanged)
                globalConfig.Save( );


            globCurrCS = ConnectionStringManager.GetProviderConnectionString( globalConfig.CurriculumConnectionString );
            globProfCS = ConnectionStringManager.GetProviderConnectionString( globalConfig.ProfessionalConnectionString );
            if (localSettings.ConnectionString != globProfCS || localSettings.CurriculumConnectionString != globCurrCS)
            {
                localSettings.ConnectionString = globProfCS;
                localSettings.CurriculumConnectionString = globCurrCS;
                Settings.Save( );
            }
        }

        protected void ChangeUserClick()
        {
            if (MessageForm.ShowYesNoQuestionMsg("Вы уверены, что хотите сменить пользователя?") == DialogResult.No)
            {
                return;
            }
            Hide();
            //делаем переключение флага инициализации для того чтобы мы смогли повторно ингициализировать
            RunnerApplication.ResetInitActiveRecord();
            //происходит новая инициализация
            var par = new StartUpParameters() {ARMSettings = ARMSettingsSection.GetFromConfigSource(), ApplicationIcon = this.Icon};
                              
            if (!RunnerApplication.ConnectDb() || !RunnerApplication.Authotization(par))
            {
                AskBeforeExit = false;
                Application.Exit();
            }
            Show();
        }

        public static void ShowSettingsClick()
        {
            ARMSettingsSection armSettings = ARMSettingsSection.GetFromConfigSource();
            var uc = new AdminSettingsForm(armSettings.ARMType);
            uc.ShowDialog();
        }
    }
}
using System;
using System.Collections;
using System.Configuration;
using System.Diagnostics.CodeAnalysis;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Eureca.Professional.BaseComponents.Configuration;
using Eureca.Professional.CommonClasses.Configuration;
using EurecaCorp.SystemTools;
using Eureca.Professional.ActiveRecords;
using EurecaCorp.Professional.BaseComponents.Configuration;

//using System.Threading;

namespace Eureca.Professional.BaseComponents
{
    public delegate void ConfigurationSectionChangedEventHandler(
        ConfigurationSection oldValue, ConfigurationSection newValue);

    public partial class AdminSettingsForm : XtraForm //SkinForm
    {
        // �������� ������� ���� CategoryListBoxItem
        private static ARMType _armType;
        private static AdminSettingsForm _instance;
        private static ArrayList _registeredSections; // ��������� ���������������� ������, ��������� �� ����� �������� 

        public AdminSettingsForm(ARMType armType)
        {
            _armType = armType;
            InitializeComponent();

            Text = "��������� ���������";

            //RegisterSection(typeof(SystemSettingsSection), "�������", "��������� ���������");
            switch (armType)
            {
                case ARMType.PROBATIONER:
                    RegisterSection(typeof (UserSettingsSection), "���", "��������� ����������������� ����");
                    break;
                case ARMType.ADMINISTRATION:
                    RegisterSection(typeof (AdministrationSettingsSection), "��������� ����������������� ��",
                                    "��������� ���� �����������������");
                    break;

                    //RegisterSection(typeof(DebugSettingsSection), "�������", "��������� ���������� �������");
                    //RegisterSection(typeof (DatabaseSettingsSection), "���� ������","��������� ����������� � ���� ������");
                    //RegisterSection(typeof(TransportSettingsSection), "���������", "��������� �������� ������");
            }

            ReadSections();
            UpdateCategoryListBox();
            DoubleBuffered = true;

            //DropShadow = false;
            //DoubleClickMaximizeEnabled = false;
        }

        public static event ConfigurationSectionChangedEventHandler ConfigurationSectionChanged;

        /// <summary>
        /// �������� ���� ���������������� ������ �� "�� null" � ������������ �� ConfigurationSection.
        /// �������� � ��������� �����. ������������ ����� ��������, �������� � �������� ��������� [Type sectionType].
        /// </summary>
        /// <param name="sectionType"></param>
        [SuppressMessage("Microsoft.Usage", "CA2208:InstantiateArgumentExceptionsCorrectly")]
        private static void CheckSectionTypeArg(Type sectionType)
        {
            if (null == sectionType) throw new ArgumentNullException();
            if (!ObjectExtention.TypesEqualOrSubclass(sectionType, typeof (ConfigurationSection)))
                throw new ArgumentOutOfRangeException();
        }

        /// <summary>
        /// ������ ������ � ������ sectionName �� ��������� ������������ � ���������
        /// ������������ �� ���� ���������� sectionType.
        /// </summary>
        /// <param name="sectionName">��� ������, �������� �� ��������� ������������</param>
        /// <param name="sectionType">��������� ��� �������� ������</param>
        /// <returns></returns>
        private static ConfigurationSection CreateAndReadSection(Type sectionType)
        {
            CheckSectionTypeArg(sectionType);

            ConfigurationSection section = Settings.GetSection(sectionType);

            if (null == section)
                throw new Exception("Section of type " + sectionType + " not found in configuration source");
            if (!section.GetType().Equals(sectionType))
                throw new Exception(
                    "Expected configuration section type is " + sectionType + ". " +
                    "Actual readed type: " + section.GetType() + ".");

            return section;
        }

        /// <summary>
        /// ����������� �� ���� �������� � ��������� _registeredSections, ������� � ������ ���������� 
        /// ��������������� ������ �� ��������� ������������.
        /// </summary>
        private void ReadSections()
        {
            if (null != _registeredSections)
            {
                foreach (object obj in _registeredSections)
                {
                    if (obj.GetType().Equals(typeof (CategoryListBoxItem)))
                    {
                        var item = (CategoryListBoxItem) obj;
                        item.SectionInstance = CreateAndReadSection(item.SectionType);

                        var clone =
                            (ConfigurationSection) ObjectExtention.CreatePropertyClone(item.SectionInstance);

                        item.OldSectionInstance = clone;
                    }
                }
            }
        }


        private void SaveSections()
        {
            Settings.Save();

            foreach (CategoryListBoxItem item in lbCategory.Items)
            {
                if (!ObjectExtention.CompareProperties(item.OldSectionInstance, item.SectionInstance, true))
                {
                    if (ConfigurationSectionChanged != null)
                    {
                        ConfigurationSectionChanged(item.OldSectionInstance, item.SectionInstance);
                    }
                }
            }
        }

        /// <summary>
        /// ��������� ���������� ��������� lbCategory � ������������ � ������������������� �
        /// ��������� _registeredSections ��������.
        /// </summary>
        private void UpdateCategoryListBox()
        {
            lbCategory.Items.Clear();

            if (null != _registeredSections)
                foreach (object obj in _registeredSections)
                    lbCategory.Items.Add(obj);
        }

        [SuppressMessage("Microsoft.Security", "CA2109:ReviewVisibleEventHandlers")]
        public static void OnAdminSettingsMenuShortcut(object sender, EventArgs e)
        {
            ShowForm();
        }

        public static void ShowForm()
        {
            if (null == _instance)
            {
                try
                {
                    if (null == _instance)
                    {
                        _instance = new AdminSettingsForm(_armType);
                        _instance.ShowDialog();
                    }
                }
                finally
                {
                    _instance = null;
                }
            }
        }

        private void lbCategory_Format(object sender, ListControlConvertEventArgs e)
        {
            var item = (CategoryListBoxItem) e.ListItem;
            e.Value = " " + item.Text;
        }

        private void lbCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            var item = (CategoryListBoxItem) lbCategory.SelectedItem;
            lCategory.Text = item.Description;
            _propertyGrid.SelectedObject = new ConfigSectionTypeDescriptor(item.SectionInstance);
            _propertyGrid.RetrieveFields();
        }

        private void AdminSettingsForm_Shown(object sender, EventArgs e)
        {
            if (lbCategory.Items.Count > 0)
                lbCategory.SelectedIndex = 0;
        }

        private static bool SectionRegistered(Type sectionType)
        {
            if (null != _registeredSections)
            {
                foreach (CategoryListBoxItem item in _registeredSections)
                {
                    if (item.SectionType == sectionType) return true;
                }
            }

            return false;
        }

        public static void RegisterSection(Type sectionType, string displayName, string description)
        {
            if (!SectionRegistered(sectionType))
            {
                CheckSectionTypeArg(sectionType);

                if (null == _registeredSections)
                    _registeredSections = new ArrayList();

                var item = new CategoryListBoxItem(sectionType, displayName, description);
                _registeredSections.Add(item);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveSections();
            Close();
        }

        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        //private void button1_Click(object sender, EventArgs e)
        //{
        //    ((ARMSettingsSection)((CategoryListBoxItem)_registeredSections[0]).SectionInstance).SystemSettingsMenuShortcut.KeyCode = Keys.Y;
        //}

        private void AdminSettingsForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                Close();
            }
        }
    }

    internal class CategoryListBoxItem
    {
        private readonly string _sectionName;
        private readonly Type _sectionType;


        public CategoryListBoxItem()
        {
            _sectionName = string.Empty;
            Text = Description = string.Empty;
        }

        public CategoryListBoxItem(Type sectionType, string text, string description)
        {
            _sectionType = sectionType;
            Text = text;
            Description = description;
        }

        public ConfigurationSection OldSectionInstance { get; set; }

        public string SectionName
        {
            get { return _sectionName; }
        }

        public string Text { get; set; }

        public string Description { get; set; }

        public Type SectionType
        {
            get { return _sectionType; }
        }

        public ConfigurationSection SectionInstance { get; set; }
    }
}
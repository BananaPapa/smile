﻿namespace Eureca.Professional.BaseComponents
{
    internal class StringScan
    {
        public static bool NotAllowedSymbols(string s)
        {
            bool result = false;

            foreach (char ch in s)
            {
                if (ch == '!' ||
                    ch == '@' ||
                    ch == '#' ||
                    ch == '№' ||
                    ch == '$' ||
                    ch == '%' ||
                    ch == '^' ||
                    ch == '&' ||
                    ch == '*' ||
                    ch == '(' ||
                    ch == ')' ||
                    ch == '{' ||
                    ch == '}' ||
                    ch == '[' ||
                    ch == ']' ||
                    ch == ':' ||
                    ch == ';' ||
                    ch == ',' ||
                    ch == '.' ||
                    ch == '?' ||
                    ch == '<' ||
                    ch == '>' ||
                    ch == '/' ||
                    ch == '+' ||
                    ch == '=' ||
                    ch == '_' ||
                    ch == '|' ||
                    ch == '~' ||
                    ch == '`' ||
                    ch == '0' ||
                    ch == '1' ||
                    ch == '2' ||
                    ch == '3' ||
                    ch == '4' ||
                    ch == '5' ||
                    ch == '6' ||
                    ch == '7' ||
                    ch == '8' ||
                    ch == '9' ||
                    ch == '"')

                    result = true;
            }

            return result;
        }
    }
}
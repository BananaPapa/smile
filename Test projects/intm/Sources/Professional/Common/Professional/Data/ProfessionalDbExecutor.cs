﻿using System.Data;
using System.Data.SqlClient;
using Eureca.Professional.CommonClasses.Data;
using Eureca.Professional.ActiveRecords;

namespace Eureca.Professional.BaseComponents.Data
{
    /// <summary>
    /// класс для работы с БД Профессионал
    /// </summary>
    public class ProfessionalDbExecutor
    {
        /// <summary>
        /// метод получения строки подключения
        /// </summary>
        /// <returns></returns>
        public static string GetConnectionString()
        {
            return DBExecutor.GetConnectionString();
        }

        /// <summary>
        /// получение статистики по тесту и пользователю
        /// </summary>
        /// <param name="user"></param>
        /// <param name="test"></param>
        /// <returns></returns>
        public static DataTable GetStatistic(User user, Test test)
        {
            string querry =
                string.Format(
                    "select * from vStatistic  where UserId = {0} and Testid = {1} order by sTime desc",
                    user.Id, test.Id);
            return DBExecutor.SelectQuerry(querry);
        }

        /// <summary>
        /// возвращает статистику по пользователя
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static DataTable GetStatistic(User user)
        {
            string querry =
                string.Format(
                    "select * from vStatistic  where UserId = {0} order by sTime desc",
                    user.Id);
            return DBExecutor.SelectQuerry(querry);
        }

        /// <summary>
        /// получение статистики по профилю
        /// </summary>
        /// <param name="user"></param>
        /// <param name="profilePassing"></param>
        /// <returns></returns>
        public static DataTable GetStatistic(User user, ProfilePassing profilePassing)
        {
            string querry =
                string.Format(
                    "select * from vStatistic  where UserId = {0} and ProfilePassingid = {1} order by sTime desc",
                    user.Id, profilePassing.Id);
            return DBExecutor.SelectQuerry(querry);
        }
        /// <summary>
        /// Обновление таблицы статистики
        /// </summary>
        /// <param name="user"></param>
        /// <param name="profilePassing"></param>
        /// <returns></returns>
        public static int UpdateStatistic(TestPassing tp,bool isVerified = true)
        {
                string query =
                    string.Format("insert into statistic " +
                                  "(testpassingid,userid,useruniqueid,stime,etime, " +
                                  "issupervise,maxeff,timeduration,result,perc,valuation,testid,verified)" +

                                  "select testpassingid,userid,UserUniqueId,sTime,eTime, " +
                                  "IsSupervise,ISNULL(MaxEff,100),TimeDuration,Result,Perc,Valuation,testid,{1} from vrawStatistic " +
                                  "where testpassingid = {0}", tp.Id,isVerified? 1 : 0);
                return DBExecutor.ExecNonQuery(query);
        }

        /// <summary>
        /// Обновление таблицы статистики
        /// </summary>
        /// <param name="user"></param>
        /// <param name="profilePassing"></param>
        /// <returns></returns>
        public static int UpdateStatistic(TestPassing tp,bool isSupervise,int perc,int valuation,int speed,double errorCount,string description,bool isVerified = true)
        {
            int timeDuration = (int)(tp.TimeInterval.FinishTime - tp.TimeInterval.StartTime).TotalSeconds;
            string query =
                string.Format("insert into statistic " +
                              "(testpassingid,userid,useruniqueid,stime,etime, " +
                              "issupervise,perc,valuation,testid,MaxEff,TimeDuration,Result,Speed,ErrorCount,Description,Verified)" +

                              "values( {0},{1},'{2}',{3},{4}, " +
                              "'{5}',{6},{7},{8},{6},{9},{6},{10},{11},'{12}',{13})", 
                              tp.Id,  tp.Probationer.User.Id,  tp.Probationer.User.UniqueId,DBExecutor.SqlConvert(tp.TimeInterval.StartTime),DBExecutor.SqlConvert(tp.TimeInterval.FinishTime),
                              isSupervise,perc,valuation,tp.Test.Id,timeDuration,speed,DBExecutor.SqlConvert(errorCount),description,isVerified ? 1: 0);

            return DBExecutor.ExecNonQuery(query);
        }

        /// <summary>
        /// возвращает пройденные профили
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static DataTable GetProfilePassins(User user)
        {
            string querry =
                string.Format(
                    "select * from vprofilepassings  where UserId = {0} order by startTime desc",
                    user.Id);
            return DBExecutor.SelectQuerry(querry);
        }


        /// <summary>
        /// получение выборки общего отчета по профилю
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="probationer"></param>
        /// <param name="profilePassing"></param>
        /// <returns></returns>
        public static int GetCommonProfile(DataSetProfessional ds, Probationer probationer,
                                           ProfilePassing profilePassing)
        {
            var adapter = new SqlDataAdapter();

            // Open the connection.
            var connection = new SqlConnection(DBExecutor.GetConnectionString());
            connection.Open();

            // Create a SqlCommand to retrieve Suppliers data.
            var command = new SqlCommand(string.Format(
                "select * from vCommonTests " +
                " where userid = {0} and ProfilePassingId = {1} and ScaleId is not null"
                ,
                probationer.User.Id, profilePassing.Id),
                                         connection);
            command.CommandType = CommandType.Text;

            // Set the SqlDataAdapter's SelectCommand.
            adapter.SelectCommand = command;

            int ret = adapter.Fill(ds.vCommonTests);
            connection.Close();
            return ret;
        }

       
    }
}
﻿using System;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Microsoft.Data.ConnectionUI;

namespace Eureca.Professional.ConnectDBLib
{
    public class connDB : XtraUserControl
    {
        private readonly SqlFileConnectionProperties _cp = new SqlFileConnectionProperties();
        private readonly SqlConnectionUIControl uic = new SqlConnectionUIControl();
        private SimpleButton _cancelButton;
        private XtraPanel _dbConnPanel;
        private string _err;
        private SimpleButton _okButton;
        private XtraPanel _okCancelConnPanel;
        private IContainer components;
        private SimpleButton glassButton1;
        private SimpleButton glassButton2;
        private XtraPanel panel1;
        private XtraPanel panel2;
        private TableLayoutPanel tableLayoutPanel1;

        public connDB()
        {
            InitializeComponent();
            var button7 = new SimpleButton();
            button7.Width = 0x7d;
            button7.Height = 0x1b;
            SimpleButton button = button7;
            var button8 = new SimpleButton {Width = 0x9b, Height = 0x1b};
            SimpleButton button2 = button8;
            foreach (Control control in uic.Controls)
            {
                if (control.Text == "Log on to the server")
                {
                    control.Text = "Авторизация на сервере";
                }
                if (control.Text == "S&erver name:")
                {
                    control.Text = "Имя сервера:";
                }
                if (control.Text == "Connect to a database")
                {
                    control.Text = "Подключить базу данных";
                }
                foreach (Control control2 in control.Controls)
                {
                    if (control2.Text == "Use S&QL Server Authentication")
                    {
                        control2.Text = "Использовать аутентификацию SQL Server";
                    }
                    if (control2.Text == "Use &Windows Authentication")
                    {
                        control2.Text = "Использовать аутентификацию Windows";
                    }
                    if (control2.Text == "&Logical name:")
                    {
                        control2.Text = "Логическое имя:";
                    }
                    if (control2.Text == "&Refresh")
                    {
                        control2.Text = "Обновить";
                        var button4 = new SimpleButton();
                        button4.Text = control2.Text;
                        button4.Location = control2.Location;
                        button4.Size = control2.Size;
                        SimpleButton button3 = button4;
                        object key =
                            typeof (Control).GetField("EventClick", BindingFlags.NonPublic | BindingFlags.Static).
                                GetValue(null);
                        PropertyInfo property = typeof (Component).GetProperty("Events",
                                                                               BindingFlags.NonPublic |
                                                                               BindingFlags.Instance);
                        var list = (EventHandlerList) property.GetValue(control2, null);
                        Delegate delegate2 = list[key];
                        list.RemoveHandler(key, delegate2);
                        ((EventHandlerList) property.GetValue(button3, null)).AddHandler(key, delegate2);
                        control.Controls.Add(button3);
                        control2.Dispose();
                    }
                    foreach (Control control3 in control.Controls)
                    {
                        if (control3.Text == "Attac&h a database file:")
                        {
                            control3.Text = "Подключить файл базы данных:";
                        }
                        if (control3.Text == "Select or enter a &database name:")
                        {
                            control3.Text = "Выбрать или ввести имя базы данных:";
                        }
                        foreach (Control control4 in control3.Controls)
                        {
                            if (control4.Text == "&Browse...")
                            {
                                control4.Text = "Обзор...";
                                var button6 = new SimpleButton();
                                button6.Text = control4.Text;
                                button6.Location = control4.Location;
                                button6.Size = control4.Size;
                                SimpleButton button5 = button6;
                                object obj3 = typeof (Control).GetField("EventClick",
                                                                        BindingFlags.NonPublic | BindingFlags.Static)
                                    .GetValue(null);
                                PropertyInfo info4 = typeof (Component).GetProperty("Events",
                                                                                    BindingFlags.NonPublic |
                                                                                    BindingFlags.Instance);
                                var list2 = (EventHandlerList) info4.GetValue(control4, null);
                                Delegate delegate3 = list2[obj3];
                                list2.RemoveHandler(obj3, delegate3);
                                ((EventHandlerList) info4.GetValue(button5, null)).AddHandler(obj3, delegate3);
                                control3.Controls.Add(button5);
                                control4.Dispose();
                            }
                            if (control4.Text == "&User name:")
                            {
                                control4.Text = "Имя пользователя:";
                            }
                            if (control4.Text == "&Password:")
                            {
                                control4.Text = "Пароль:";
                            }
                            if (control4.Text == "&Save my password")
                            {
                                control4.Text = "Сохранить мой пароль";
                            }
                        }
                    }
                }
            }
            uic.Dock = DockStyle.Top;
            uic.Parent = _dbConnPanel;
            ClientSize = Size.Add(uic.MinimumSize,
                                  new Size(0x2d, (button.Height + 0x5f) + _okCancelConnPanel.Height));
            base.Padding = new Padding(5);
            button.Text = "Дополнительно";
            button.Dock = DockStyle.None;
            button.Location = new Point(uic.Width - button.Width, uic.Bottom + 10);
            button.Anchor = AnchorStyles.Right | AnchorStyles.Top;
            button.Click += Advanced_Click;
            button.Parent = _dbConnPanel;
            button2.Text = "Проверка соединения";
            button2.Dock = DockStyle.None;
            button2.Location = new Point(10, uic.Bottom + 10);
            button2.Anchor = AnchorStyles.Right | AnchorStyles.Top;
            button2.Click += testConn_Click;
            button2.Parent = _dbConnPanel;
            base.Controls.Add(_dbConnPanel);
        }

        public string ConnectionString
        {
            get { return _cp.ConnectionStringBuilder.ConnectionString; }
            set { _cp.ConnectionStringBuilder.ConnectionString = value; }
        }

        public bool TestConnection
        {
            get { return TestingConnection(); }
        }

        public string Err
        {
            get { return _err; }
        }

        private void Advanced_Click(object sender, EventArgs e)
        {
            var form2 = new Form();
            form2.Text = "Свойства";
            form2.StartPosition = FormStartPosition.CenterScreen;
            Form form = form2;
            var grid = new PropertyGrid();
            grid.SelectedObject = _cp;
            grid.Dock = DockStyle.Fill;
            grid.Parent = form;
            form.ShowDialog();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        public void Initialize(string connString)
        {
            _cp.ConnectionStringBuilder.ConnectionString = connString;
            uic.Initialize(_cp);
            uic.LoadProperties();
        }

        private void InitializeComponent()
        {
            _okCancelConnPanel = new XtraPanel();
            _cancelButton = new SimpleButton();
            _okButton = new SimpleButton();
            _dbConnPanel = new XtraPanel();
            tableLayoutPanel1 = new TableLayoutPanel();
            panel1 = new XtraPanel();
            glassButton1 = new SimpleButton();
            glassButton2 = new SimpleButton();
            panel2 = new XtraPanel();
            _okCancelConnPanel.SuspendLayout();
            tableLayoutPanel1.SuspendLayout();
            panel1.SuspendLayout();
            base.SuspendLayout();
            _okCancelConnPanel.Controls.Add(_cancelButton);
            _okCancelConnPanel.Controls.Add(_okButton);
            _okCancelConnPanel.Dock = DockStyle.Fill;
            _okCancelConnPanel.Location = new Point(2, 0x17);
            _okCancelConnPanel.Margin = new Padding(2, 3, 2, 3);
            _okCancelConnPanel.Name = "_okCancelConnPanel";
            _okCancelConnPanel.Size = new Size(0xc4, 0x4a);
            _okCancelConnPanel.TabIndex = 0;
            _cancelButton.DialogResult = DialogResult.Cancel;
            _cancelButton.Location = new Point(0x153, 3);
            _cancelButton.Margin = new Padding(2, 3, 2, 3);
            _cancelButton.Name = "_cancelButton";
            _cancelButton.Size = new Size(90, 0x1d);
            _cancelButton.TabIndex = 1;
            _cancelButton.Text = "Отмена";
            _okButton.DialogResult = DialogResult.OK;
            _okButton.Location = new Point(0xea, 3);
            _okButton.Margin = new Padding(2, 3, 2, 3);
            _okButton.Name = "_okButton";
            _okButton.Size = new Size(90, 0x1d);
            _okButton.TabIndex = 0;
            _okButton.Text = "OK";
            _dbConnPanel.Location = new Point(3, 3);
            _dbConnPanel.Name = "_dbConnPanel";
            _dbConnPanel.Padding = new Padding(1);
            _dbConnPanel.Size = new Size(0x1a0, 0x1b7);
            _dbConnPanel.TabIndex = 1;
            tableLayoutPanel1.Anchor = AnchorStyles.Right | AnchorStyles.Left | AnchorStyles.Bottom | AnchorStyles.Top;
            tableLayoutPanel1.ColumnCount = 1;
            tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100f));
            tableLayoutPanel1.Controls.Add(panel1, 0, 1);
            tableLayoutPanel1.Location = new Point(0, 0);
            tableLayoutPanel1.Name = "tableLayoutPanel1";
            tableLayoutPanel1.RowCount = 2;
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 20f));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 20f));
            tableLayoutPanel1.Size = new Size(200, 100);
            tableLayoutPanel1.TabIndex = 0;
            panel1.Controls.Add(glassButton1);
            panel1.Controls.Add(glassButton2);
            panel1.Dock = DockStyle.Fill;
            panel1.Location = new Point(2, 0x17);
            panel1.Margin = new Padding(2, 3, 2, 3);
            panel1.Name = "panel1";
            panel1.Size = new Size(0xc4, 0x4a);
            panel1.TabIndex = 0;
            glassButton1.DialogResult = DialogResult.Cancel;
            glassButton1.Location = new Point(0x153, 3);
            glassButton1.Margin = new Padding(2, 3, 2, 3);
            glassButton1.Name = "glassButton1";
            glassButton1.Size = new Size(90, 0x1d);
            glassButton1.TabIndex = 1;
            glassButton1.Text = "Отмена";
            glassButton2.DialogResult = DialogResult.OK;
            glassButton2.Location = new Point(0xea, 3);
            glassButton2.Margin = new Padding(2, 3, 2, 3);
            glassButton2.Name = "glassButton2";
            glassButton2.Size = new Size(90, 0x1d);
            glassButton2.TabIndex = 0;
            glassButton2.Text = "OK";
            panel2.Dock = DockStyle.Fill;
            panel2.Location = new Point(3, 3);
            panel2.Name = "panel2";
            panel2.Padding = new Padding(1);
            panel2.Size = new Size(0x1e6, 0x23d);
            panel2.TabIndex = 1;
            base.AutoScaleDimensions = new SizeF(6f, 13f);
            base.Name = "connDB";
            base.Size = new Size(0x1eb, 0x272);
            _okCancelConnPanel.ResumeLayout(false);
            tableLayoutPanel1.ResumeLayout(false);
            panel1.ResumeLayout(false);
            base.ResumeLayout(false);
        }

        private void testConn_Click(object sender, EventArgs e)
        {
            try
            {
                if (TestingConnection())
                {
                    XtraMessageBox.Show("Проверка соединения успешна.", "Проверка соединения успешна");
                }
                else
                {
                    XtraMessageBox.Show(Err, "Проверка соединения безуспешна.");
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Проверка соединения безуспешна.");
            }
        }

        public bool TestingConnection()
        {
            var connection2 = new SqlConnection();
            connection2.ConnectionString = _cp.ConnectionStringBuilder.ConnectionString;

            int connnectionTimeOut = connection2.ConnectionTimeout;
            if (connnectionTimeOut > 20)
            {
                //    connection2.
                //todo
                //при большом таймауте(у администратора) сделать на время
                //тестирования соединения меньшим
            }
            SqlConnection connection = connection2;
            try
            {
                connection.Open();
            }
            catch (Exception exception)
            {
                _err = exception.Message;
                return false;
            }
            finally
            {
                connection.Close();
            }
            return true;
        }
    }
}
﻿using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace Eureca.Professional.ConnectDBLib.Properties
{
    [DebuggerNonUserCode, GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "2.0.0.0"),
     CompilerGenerated]
    internal class Resources
    {
        private static CultureInfo resourceCulture;
        private static ResourceManager resourceMan;

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        internal static CultureInfo Culture
        {
            get { return resourceCulture; }
            set { resourceCulture = value; }
        }

        //internal static Bitmap Health_care_shield_48x48
        //{
        //    get { return (Bitmap) ResourceManager.GetObject("Health_care_shield_48x48", resourceCulture); }
        //}

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        internal static ResourceManager ResourceManager
        {
            get
            {
                if (ReferenceEquals(resourceMan, null))
                {
                    var manager = new ResourceManager("ConnectDBLib.Properties.Resources", typeof (Resources).Assembly);
                    resourceMan = manager;
                }
                return resourceMan;
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Windows.Forms;
using Castle.ActiveRecord;
using Curriculum;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using Eureca.Professional.ActiveRecords;
using Eureca.Professional.BaseComponents;
using Eureca.Professional.BaseComponents.Configuration;
using Eureca.Professional.BaseComponents.Data;
using Eureca.Professional.CommonClasses;
using Eureca.Professional.ProfessionalProbationer.Properties;
using Eureca.Professional.TestingPlugInStandard;
using Eureca.Professional.TestingPlugInStandard.Controls.Reports;
using Eureca.Professional.TestingPlugInStandard.Controls.ShowTest;
using KeyboardSimulator;
using log4net;
using NHibernate.Criterion;
using Event = Eureca.Professional.ActiveRecords.Event;
using User = Eureca.Professional.ActiveRecords.User;
using File = System.IO.File;

namespace Eureca.Professional.ProfessionalProbationer
{
    /// <summary>
    ///     контрол запуска тестов
    /// </summary>
    public partial class RunEducation : XtraForm
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof (RunEducation));

        #region fields

        private const string fillingformtestPluginName = "FillingFormTest";
        
        private readonly List<Process> _openProcesses = new List<Process>();
        private bool _cancelCopy;
        private bool _curriculumExist;
        private string _filePath;
        private bool _formClosing;
        private string _lastAttachmentsFolder;
        private string _newPath;
        private bool _processCopy;
        private Command _remoteCommandObject;
        private Test _selectedTest;
        private string _serverHost;
        private bool _showAttachments;
        private bool _showTestStatistic;


        /// <summary>
        ///     текущий пользователь системы
        /// </summary>
        private User _user;

        private WaitingForm _wc;

        #endregion

        #region constructor

        public RunEducation(User user)
        {
            InitializeComponent();
            Icon = Resources.ProbationerIcon;
            _user = user;
        }

        #endregion

        #region methods

        private void ShowPanels()
        {
            while (!_formClosing)
            {
                ShowStatisticsPanel();
                ShowAttachmentsPanel();
                Thread.Sleep(100);
            }
        }


        /// <summary>
        ///     вставка данных
        /// </summary>
        public void InsertData()
        {
            splitContainerControlAll.Panel1.Text = string.Format("{0}", _user.FullName);
            //загрузка данных в профиль
            try
            {
                using (new SessionScope(FlushAction.Never))
                {
                    _user = ActiveRecordBase<User>.Find(_user.Id);
                    gridControlTest.DataSource = _user.Profile.Tests;
                    gridViewTests.BestFitColumns();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw;
            }

            if (_user.Profile == null)
            {
                throw new Exception("У данного пользователя нет профиля тестирования.");
            }

            Log.Error("Проверка на существование БД ");
            if (!Curriculum.Program.DbSingletone.DatabaseExists())
            {
                Log.Error("Проверка на существование БД безуспешна");
                MessageForm.ShowWarningMsg("Отсутствует подключение к БД планирования. " + Environment.NewLine +
                                           "Выполнение тестов будет возможно из профиля.");
                //нет подключения к СПО планирования учебного процесса
                xtraTabPageLessons.PageVisible = false;
                return;
            }
            Log.Error("Проверка на существование БД успешна");
            _curriculumExist = true;
            //инициализация календаря
            Person p;
            try
            {
                Log.Error("пробная выборка всех пользователей");
                List<Person> persons = Curriculum.Program.DbSingletone.Persons.ToList();

                Log.Error("Выборка успешна. Пользователей = " + persons.Count);
                p = persons.Where(i => i.ProfessionalUserUniqueId == _user.UniqueId)
                    .SingleOrDefault();
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                MessageForm.ShowErrorMsg("Некорректное подключение к БД планирования");
                throw;
            }
            if (p == null)
            {
                MessageForm.ShowWarningMsg("Данный пользователь не зарегистрирован в системе планирования." +
                                           Environment.NewLine + "Выполнение тестов будет возможно из профиля.");
                xtraTabPageLessons.PageVisible = false;
            }
            else
            {
                lessonsControlProb.Person = p;
                xtraTabPageProfile.PageVisible = false;
            }
        }

        /// <summary>
        ///     получение и установка статуса зачета в контроле
        /// </summary>
        private bool GetSuperviseStatus()
        {
            try
            {
                var setting = SystemProperties.GetItem(SysPropEnum.IsGlobalControl);
                if (setting != null && setting.Value == "1")
                    return true;

                var winUsers = SystemProperties.GetItem(SysPropEnum.WinUsersSupervisAccounts);
                if (winUsers != null)
                {
                    var users = winUsers.Value.Split('|');
                    if (users.Contains(Environment.UserName))
                        return true;
                }
                IPAddress ip =
                    Dns.GetHostEntry(Dns.GetHostName())
                        .AddressList.FirstOrDefault(item => item.AddressFamily == AddressFamily.InterNetwork);
                if (ip != null)
                {
                    SuperviseAdresses arm = SuperviseAdresses.GetSuperviseAdresses(ip.ToString());
                    if (arm == null)
                        return false;
                    return arm.IsSupervise;
                }
                return false;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return false;
            }
        }

        /// <summary>
        ///     метод прохождения теста
        /// </summary>
        /// <param name="profilePassingId"></param>
        /// <param name="objectDescription"></param>
        /// <param name="testId"></param>
        private bool ShowTest(int testId, int profilePassingId, ObjectDescription objectDescription)
        {
            try
            {

                ProfilePassing profilePassing;
                Test test;
                Probationer probationer;
                TestPassing tp;

                using (new SessionScope())
                {
                    Event.Add(RunnerApplication.CurrentUser, EventTypes.BeginTesting, testId);
                    profilePassing = ProfilePassing.Find(profilePassingId);
                    test = Test.Find(testId);
                    probationer = Probationer.Find(_user.Probationer.Id);
                }

                string error;
                if (!CheckTest(test, out error))
                {
                    Integrator.Utility.Forms.MessageBox.ShowError(error??"Тест содержит ошибки, обратитесь к админиcтратору.");
                    return false;
                }

                int count = 0;
                //Если не зачетное тестирование и тест не клавиатурный тренажер
                //тогда ограничиваем количество попыток прохождения теста
                if (!CheckPassingCount(test, probationer, ref count))
                    return false;

                //создание прохождения теста
                if (profilePassing != null)
                    tp = new TestPassing(test, profilePassing, probationer,
                        new TimeInterval(DateTime.Now, DateTime.Now), GetSuperviseStatus());
                else
                    tp = new TestPassing(test, probationer,
                        new TimeInterval(DateTime.Now, DateTime.Now), GetSuperviseStatus());
                tp.CreateAndFlush();
                if (!PerformTest(objectDescription, test, tp))
                    return false;

                //Вывод результатов
                //для экзаменационных тестов и последнего пробного
                if (test.Type.PlugInFileName == "TestingPlugInStandard" ||
                    ( test.Type.PlugInFileName == fillingformtestPluginName && !GetSuperviseStatus( ) ))
                    //Для обычных тестов выводим результат
                    ShowResult(_user.Probationer, profilePassing, test, tp);
                return true;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw;
            }
        }

        private bool CheckTest( Test test , out  string error)
        {
            error = null;
            if (test.Type.PlugInFileName == fillingformtestPluginName)
            {
                return RunnerApplication.JournalManager.CheckTest(test, out error);
            }
            return true;
        }

        private bool PerformTest(ObjectDescription objectDescription, Test test, TestPassing tp)
        {
            if (test.Type.PlugInFileName == "TestingPlugInStandard")
            {
                try
                {
                    DialogResult res;
                    using (new SessionScope())
                    {
                        var uc = new MainTestingControl(test, tp, objectDescription);
                        uc.ControlClosed += UcControlClosed;
                        uc.InitControl();
                        //показ формы тестирования
                        res = uc.ShowInForm(true, FormWindowState.Maximized);
                    }
                    //Запись в таблицу результатов прохождения теста
                    ProfessionalDbExecutor.UpdateStatistic(tp);
                    if (res == DialogResult.Abort)
                    {
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                    MessageBox.Show(ex.Message);
                }
            }
            else if (test.Type.PlugInFileName == "KeyboardSimulator")
            {
                //сохраняем прохождение теста
                tp.TimeInterval = new TimeInterval(DateTime.Now, DateTime.Now.AddSeconds(1));
                tp.UpdateAndFlush();


                TestQuestion testQuestion;
                string text = string.Empty;
                using (new SessionScope())
                {
                    //получаем случайный текст
                    TestQuestion[] testQuestions = ActiveRecordBase<TestQuestion>.FindAll(Restrictions.Eq("Test", test));
                    var r = new Random();
                    testQuestion = testQuestions[r.Next(0, testQuestions.Count())];
                    if (testQuestion.Question.Content.PlainContent != null)
                    {
                        text = testQuestion.Question.Content.PlainContent;
                    }
                    else
                    {
                        var richTextBox = new RichTextBox();
                        var ms = new MemoryStream(testQuestion.Question.Content.RichContent);
                        richTextBox.LoadFile(ms, RichTextBoxStreamType.RichText);
                        text = richTextBox.Text;
                    }
                }
                TimeSpan timeSpan = test.TimeLimit.HasValue
                    ? TimeSpan.FromSeconds(Math.Abs(test.TimeLimit.Value))
                    : new TimeSpan();

                var mainWindow = new MainWindow(text, timeSpan, GetSuperviseStatus());

                mainWindow.FinishTest +=
                    (sender, e) => mainWindowFinishTest(tp, GetSuperviseStatus(), e);
                mainWindow.ShowDialog();
            }
            else if (test.Type.PlugInFileName == fillingformtestPluginName)
            {
                bool isSupervised = GetSuperviseStatus();
                tp.TimeInterval = new TimeInterval(DateTime.Now, DateTime.Now.AddSeconds(1));
                tp.SaveAndFlush();
                var res = RunnerApplication.JournalManager.PerformTest(tp, this);
                tp.TimeInterval = new TimeInterval(tp.TimeInterval.StartTime, DateTime.Now.AddSeconds(1));
                tp.SaveAndFlush();
                ProfessionalDbExecutor.UpdateStatistic(tp, isSupervised, 0, 0, 0, 0, "", !isSupervised);
                return res;
            }
            return true;
        }

        private bool CheckPassingCount(Test test, Probationer probationer, ref int count)
        {
            if (!GetSuperviseStatus() && test.Type.PlugInFileName != "KeyboardSimulator")
            {
                //проверка количества пробных прохождений тестов
                TestPassing[] testPassings;

                using (new SessionScope(FlushAction.Never))
                {
                    testPassings =
                        ActiveRecordBase<TestPassing>.FindAll(Restrictions.Eq("Probationer", probationer),
                            Restrictions.Eq("Test", test),
                            Restrictions.Eq("IsSupervise", false));
                }
                if (testPassings != null)
                {
                    if (testPassings.Count() < test.MaxTeacherPassing)
                    {
                        count = test.MaxTeacherPassing - testPassings.Count();
                        string msg = "Вам осталось " + count +
                                     " раз(а) с учетом текущей попытки пройти тест в режиме тренировки."
                                     + Environment.NewLine + " Вы уверены, что хотите это сделать?";
                        if (MessageBox.Show(msg, "Предупреждение", MessageBoxButtons.YesNo) == DialogResult.No)
                        {
                            return false;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Вы исчерпали все попытки пройти тест(" + test.Title +
                                        ") в редиме тренировки." +
                                        Environment.NewLine +
                                        " Дальнейшее тестирование возможно в режиме контроля.");
                        return false;
                    }
                }
            }
            return true;
        }


        /// <summary>
        ///     запуск тестов в профиле
        /// </summary>
        /// <param name="selected"></param>
        private void RunProfileTests(bool selected)
        {
            try
            {
                //список тестов
                var tests = new List<Test>();
                using (new SessionScope())
                {
                    //обновление
                    _user.Profile.Refresh();
                    foreach (Test test in _user.Profile.Tests)
                        test.Refresh();
                    if (!selected)
                    {
                        //только выбранные тесты
                        //прохождение всего профиля не регистрируется
                        if (_user.Profile.Tests.Count == 0)
                        {
                            MessageForm.ShowErrorMsg("В Вашем профиле отсутствуют тесты, обратитесь к преподавателю");
                            return;
                        }
                        tests.AddRange(_user.Profile.Tests);
                    }
                    else
                    {
                        //прохождение всего профия
                        int[] rows = gridViewTests.GetSelectedRows();
                        //проверка на то что тест был выбран
                        bool selectTest = false;
                        foreach (int i in rows)
                            if (i >= 0)
                            {
                                selectTest = true;
                                break;
                            }

                        if (!selectTest)
                        {
                            MessageForm.ShowErrorMsg("Необходимо выбрать тест или набор тестов");
                            return;
                        }
                        //заполняем список тестов
                        foreach (int t1 in rows)
                        {
                            object id = gridViewTests.GetRowCellValue(t1, "Id");
                            if (id == null)
                            {
                                continue;
                            }
                            tests.AddRange(_user.Profile.Tests.Where(t => t.Id == (int) id));
                        }
                    }
                }
                RunTests(tests);
            }
            catch (Exception ex)
            {
                MessageForm.ShowErrorMsg("Ошибка прохождения профиля");
                Log.Error(ex);
                //throw;
            }
        }

        private void RunTests(List<Test> tests)
        {
            try
            {
                //список тестов
                using (new SessionScope())
                {
                    //обновление
                    _user.Profile.Refresh();
                    foreach (Test test in _user.Profile.Tests)
                        test.Refresh();
                }
                //создаем прохождение профиля

                //очищаем все процессы
                foreach (Process p in _openProcesses)
                {
                    if (p != null && !p.HasExited)
                        p.Kill();
                }
                _openProcesses.Clear();

                ClearAttachmentsFolder();
                ProfilePassing pp;
                //создаем прохождение профиля
                pp = new ProfilePassing(_user.Probationer, new TimeInterval(DateTime.Now, DateTime.Now));
                pp.CreateAndFlush();

                for (int i = 0; i < tests.Count; i++)
                {
                    Test test = tests[i];
                    //Test test = Test.Find(tests[i].Id);
                    if (i > 0 && tests[i - 1].Method.Equals(test.Method))
                    {
                        //если методики прошлого теста и настоящего совпадают
                        //тогда пропускаем описание методики и контрольные вопросы
                        if (!ShowTest(test.Id, pp.Id, ObjectDescription.Test))
                            break;
                    }
                    else
                    {
                        if (!ShowTest(test.Id, pp.Id, ObjectDescription.Method))
                            break;
                    }
                }


                using (new SessionScope())
                {
                    //обновление
                    _user.Profile.Refresh();
                    foreach (Test test in _user.Profile.Tests)
                        test.Refresh();
                    //изменяем у профиля конечное время прохождения по окончанию прохождения
                }
                pp.TimeInterval = new TimeInterval(pp.TimeInterval.StartTime, DateTime.Now);
                pp.UpdateAndFlush();
                //обновляем статистику
                LoadStatistic();
            }
            catch (Exception ex)
            {
                MessageForm.ShowErrorMsg("Ошибка прохождения профиля");
                Log.Error(ex);
                //throw;
            }
        }

        /// <summary>
        ///     Загрузка статистики по прохождениям тестирования
        /// </summary>
        private void LoadStatistic()
        {
            DateTime curDate = lessonsControlProb.CurrentDate;
            try
            {
                if (_curriculumExist)
                    lessonsControlProb.RefreshAll();
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
            lessonsControlProb.CurrentDate = curDate;
            if (gridViewTests.FocusedRowHandle < 0)
            {
                //загрузка статистики для всего профиля
                DataTable table = ProfessionalDbExecutor.GetStatistic(_user);
                simpleButtonShowResult.Enabled = table != null && table.Rows.Count != 0;
                gridControlStatistic.DataSource = table;
                gridViewStatistic.BestFitColumns();
            }
            else
            {
                object id = gridViewTests.GetRowCellValue(gridViewTests.FocusedRowHandle, "Id");
                using (new SessionScope())
                {
                    Test test = ActiveRecordBase<Test>.Find((int) id);
                    LoadLessonInfo(test.UniqueId, _lastAttachmentsFolder);
                    return;
                }
            }
            LoadLessonInfo(_selectedTest == null ? Guid.Empty : _selectedTest.UniqueId, _lastAttachmentsFolder);
        }

        private static void DeleteFile(string filePath)
        {
            try
            {
                File.Delete(filePath);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }

        /// <summary>
        ///     метод очистки папки прикрепленных файлов
        /// </summary>
        private static void ClearAttachmentsFolder()
        {
            //очистка всех директорий и файлов в директории прикрепленных файлов
            try
            {
                UserSettingsSection us = UserSettingsSection.GetFromConfigSource();
                string smallPath = us.AttachmentsDirectory;


                if (!Directory.Exists(smallPath))
                    Directory.CreateDirectory(smallPath);
                //очистка поддиректорий в этой директории 
                foreach (string stringPath in Directory.GetDirectories(smallPath))
                {
                    foreach (string filePath in Directory.GetFiles(stringPath))
                    {
                        DeleteFile(filePath);
                    }
                    Directory.Delete(stringPath);
                }
                //удаление всех файлов
                foreach (string fileName in Directory.GetFiles(smallPath))
                {
                    try
                    {
                        File.Delete(fileName);
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }

        /// <summary>
        ///     метод копирования прикреплений на жесткий диск и открытие
        /// </summary>
        private void RunAttachment(FileItem fileItem, List<FileItem> fileItems)
        {
            ClearAttachmentsFolder();

            string path = UserSettingsSection.GetFromConfigSource().AttachmentsDirectory;

            //создание директрории
            try
            {
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);
            }
            catch (Exception ex)
            {
                MessageForm.ShowErrorMsg("Ошибка создания директории " + ex.Message);
            }

            try
            {
                //инициализация контрола сообщений
                _wc = new WaitingForm("Получения файла...");
                _wc.WaitingCancel += WcWaitingCancel;
                _wc.Show();
                Enabled = false;


                //инициализация списка
                var selectedItems = new List<FileItem> {fileItem};
                //выбираем все картинки из папки
                if (fileItem.IsImage())
                    selectedItems = fileItems.Where(item => item.IsImage()).ToList();
                //выбираем все файлы DVD
                if (fileItem.IsDvd())
                    selectedItems = fileItems.Where(item => item.IsDvd()).ToList();

                _cancelCopy = false;

                //копирование файлов
                //копирую все необходимые файлы
                foreach (FileItem selectedItem in selectedItems)
                {
                    _processCopy = true;
                    if (_cancelCopy)
                        break;
                    string newPath = path + @"\" + Path.GetFileName(selectedItem.ItemPath);
                    if (File.Exists(newPath))
                    {
                        newPath = newPath.Substring(0, newPath.LastIndexOf(".")) + Guid.NewGuid() +
                                  newPath.Substring(newPath.LastIndexOf("."),
                                      newPath.Length - newPath.LastIndexOf("."));
                    }

                    _filePath = selectedItem.ItemPath;
                    _newPath = newPath;

                    try
                    {
                        var t = new Thread(Copy);
                        t.Start();
                        while (_processCopy && !_cancelCopy)
                        {
                            Application.DoEvents();
                            Thread.Sleep(100);
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex);
                        break;
                    }
                }
                try
                {
                    _wc.CloseForm();
                    _wc = null;
                }
                catch (Exception exception)
                {
                    Log.Error(exception);
                }
                //открытие файла в процессе
                try
                {
                    if (_cancelCopy)
                    {
                        return;
                    }
                    Process p = Process.Start(path + @"\" + Path.GetFileName(fileItem.ItemPath));
                    _openProcesses.Add(p);
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                    MessageForm.ShowErrorMsg("Ошибка открытия файла \n" + _filePath + Environment.NewLine +
                                             ex.Message);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                MessageForm.ShowErrorMsg("Ошибка открытия файла " + ex.Message);
            }
            finally
            {
                Enabled = true;
            }
        }

        private void WcWaitingCancel()
        {
            _cancelCopy = true;
        }

        /// <summary>
        ///     метод копирования
        /// </summary>
        private void Copy()
        {
            _processCopy = true;
            try
            {
                FileStream newFile = null;
                try
                {
                    //получаем дескриптор потока файла с сервера
                    FileStream fs = _remoteCommandObject.GetFile(_filePath);
                    if (fs == null)
                        throw new Exception("удаленный сервер (" + _serverHost + ") не вернул дескриптор файла " +
                                            _filePath);

                    const int maxSize = 1000000;
                    var buffer = new byte[maxSize];
                    long curr = 0;
                    newFile = File.Create(_newPath);
                    while (curr < fs.Length && !_cancelCopy)
                    {
                        if (fs.Length > curr + maxSize)
                        {
                            //прочитали
                            fs.Read(buffer, 0, maxSize);

                            //записали
                            newFile.Write(buffer, 0, maxSize);
                            _wc.SetMessage(
                                string.Format("Идет процесс получения файла..." + Environment.NewLine + "{0} - {1}%",
                                    Path.GetFileName(_newPath), (int) ((curr/(double) fs.Length)*100)));
                            curr += maxSize;
                        }
                        else
                        {
                            var count = (int) (fs.Length - curr);
                            //прочитали
                            fs.Read(buffer, 0, count);
                            //записали
                            newFile.Write(buffer, 0, count);
                            break;
                        }
                    }
                    //очистка дескрипторов файлов
                    fs.Close();
                    newFile.Close();
                    if (_cancelCopy)
                    {
                        //удаление отмененного файла
                        DeleteFile(_newPath);
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex);

                    //ошибка копирования
                    if (newFile != null)
                    {
                        newFile.Close();
                        File.Delete(_newPath);
                    }
                    //попытка скопировать самостоятельно
                    File.Copy(_filePath, _newPath);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                MessageForm.ShowErrorMsg("Ошибка копирования файла\n" + _filePath + "\nв файл" + _newPath);
            }
            finally
            {
                _processCopy = false;
            }
        }

        private void ShowStatisticsPanel()
        {
            try
            {
                MethodInvoker method = delegate
                {
                    if (_showTestStatistic)
                    {
                        splitContainerControlAll.Panel2.Visible = true;
                        if (splitContainerControlAll.SplitterPosition == 0)
                        {
                            splitContainerControlAll.SplitterPosition = 300;
                        }
                    }
                    else
                    {
                        splitContainerControlAll.SplitterPosition = 0;
                        splitContainerControlAll.Panel2.Visible = false;
                    }
                };
                if (InvokeRequired)
                    Invoke(method);
                else
                    method();
            }
            catch (ObjectDisposedException)
            {
            }
        }

        private void ShowAttachmentsPanel()
        {
            try
            {
                if (ParentForm == null)
                    return;
                MethodInvoker method = delegate
                {
                    if (_showAttachments)
                    {
                        splitContainerControlTests.Panel2.Visible = true;
                        if (splitContainerControlTests.SplitterPosition == 0)
                        {
                            splitContainerControlTests.SplitterPosition = 300;
                        }
                    }
                    else
                    {
                        splitContainerControlTests.SplitterPosition = 0;
                        splitContainerControlTests.Panel2.Visible = false;
                    }
                };
                Invoke(method);
            }
            catch (Exception)
            {
            }
        }

        private void LoadFolderTree(string attachmentsFolder, bool needRoot)
        {
            SystemProperties rootFolder = SystemProperties.GetItem(SysPropEnum.AttachmentsFolder);
            if (rootFolder == null || string.IsNullOrEmpty(rootFolder.Value))
            {
                MessageForm.ShowErrorMsg("Не задана корневая папка для прикрепленных материалов");
                return;
            }
            string fullPath = Path.Combine(rootFolder.Value, attachmentsFolder);

            try
            {
                List<FileItem> fileItems = _remoteCommandObject.GetFileItems(fullPath, needRoot);
                gridControlFiles.DataSource = fileItems;
                gridViewFiles.BestFitColumns();
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                MessageForm.ShowErrorMsg(ex.Message);
            }
        }

        /// <summary>
        ///     метод информации по тесту
        /// </summary>
        private void LoadLessonInfo(Guid testGuid, string attachmentsFolder)
        {
            //Если есть папка для занятий, то отображаем панель и заполняем ее
            if (!string.IsNullOrEmpty(attachmentsFolder))
            {
                if (attachmentsFolder.CompareTo(_lastAttachmentsFolder) != 0)
                {
                    _showAttachments = true;
                    LoadFolderTree(attachmentsFolder, false);
                }
            }
            else
            {
                _showAttachments = false;
            }

            _lastAttachmentsFolder = attachmentsFolder;

            //Если заняте тест, то выводим информацию по тесту
            if (testGuid == Guid.Empty)
            {
                _showTestStatistic = false;
                gridControlStatistic.DataSource = null;
                _selectedTest = null;
                return;
            }

            try
            {
                Test test;
                using (new SessionScope(FlushAction.Never))
                {
                    test = Test.FindOne(Restrictions.Eq("UniqueId", testGuid));

                    if (test == null)
                    {
                        _showTestStatistic = false;
                        _selectedTest = null;
                        string msg = "Отсутствует тест с идентификатором " + testGuid;
                        Log.Error(msg);
                        MessageForm.ShowErrorMsg(msg);
                        return;
                    }

                    //загрузка статистики определенного теста
                    DataTable table = ProfessionalDbExecutor.GetStatistic(_user, test);
                    simpleButtonShowResult.Enabled =
                        simpleButtonShowResult.Enabled = table != null && table.Rows.Count != 0;
                    gridControlStatistic.DataSource = table;
                    gridViewStatistic.BestFitColumns();
                }
                _selectedTest = Test.Find(test.Id);

                _showTestStatistic = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw;
            }
        }

        private void ShowResult(Probationer probationer, ProfilePassing profilePassing, Test test,
            TestPassing testPassing)
        {
            try
            {
                if (test.Type.PlugInFileName == fillingformtestPluginName)
                {
                    if (testPassing.IsSupervise)
                    {
                        bool isChecked = false;
                        using (new SessionScope(FlushAction.Never))
                        {
                            isChecked =
                                !vUnverifiedAssignments.FindAll(Expression.Eq("TestPassingsId", testPassing.Id)).Any();
                        }
                        if (isChecked)
                        {
                            RunnerApplication.JournalManager.ShowTestResult(testPassing, this);
                        }
                        else
                        {
                            Integrator.Utility.Forms.MessageBox.ShowInfo(
                                "Данный тест ещё не проверен. Проверьте результат позже.");
                        }
                    }
                    else
                    {
                        RunnerApplication.JournalManager.ShowTestResult(testPassing, this);
                    }
                }
                else
                {
                    var sm = new StandardReportManager();
                    sm.CreateDetailTestReport(probationer, profilePassing, test, testPassing);
                }
                Refresh();
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                MessageForm.ShowErrorMsg(ex.Message);
            }
        }

        #endregion

        #region StandardEventHandlers

        private void SimpleButtonRunSelectedTestClick(object sender, EventArgs e)
        {
            RunProfileTests(true);
        }

        private void GridView1FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            LoadStatistic();
        }

        private void RunEducationLoad(object sender, EventArgs e)
        {
            //инициализация удаленного сервера
            try
            {
                //получение хоста сервера из БД

                ArmApplication arm = ArmApplication.GetArmApplication(ARMType.ADMINISTRATION);
                if (!string.IsNullOrEmpty(arm.IPAdress))
                {
                    _serverHost = arm.IPAdress;
                }
                //инициализация работы с сервером
                _remoteCommandObject
                    = (Command) Activator.GetObject(
                        typeof (Command), "tcp://" + _serverHost + ":8086/RemWork");
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
            InsertData();
        }

        private void RunEducationFormClosing(object sender, FormClosingEventArgs e)
        {
            _formClosing = true;
            try
            {
                foreach (Process p in _openProcesses)
                {
                    if (p != null && !p.HasExited)
                        p.Kill();
                }
                _openProcesses.Clear();
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw;
            }
        }

        private void RunEducationShown(object sender, EventArgs e)
        {
            var showPanelThread = new Thread(ShowPanels);
            showPanelThread.Start();
        }


        private void SimpleButtonRunProfileClick(object sender, EventArgs e)
        {
            RunProfileTests(false);
        }

        private void LessonsControlProbLessonClick(object sender, LessonClickEventArgs e)
        {
            LoadLessonInfo(e.TestGuid, e.AttachmentsFolder);
        }

        private void SimpleButtonStartTestClick(object sender, EventArgs e)
        {
            RunTests(new List<Test> {_selectedTest});
        }

        private void GridViewFilesRowClick(object sender, RowClickEventArgs e)
        {
            if (e.Clicks > 1)
            {
                OnGridViewFilesOpenEventHandler();
            }
        }

        private void GridViewFilesKeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                OnGridViewFilesOpenEventHandler();
        }

        private void OnGridViewFilesOpenEventHandler()
        {
            var fileItem = gridViewFiles.GetRow(gridViewFiles.FocusedRowHandle) as FileItem;
            if (fileItem == null)
                return;
            //Если файл то копируем и открываем
            if (fileItem.ItemType == FileItemType.File)
            {
                var fileItems = gridControlFiles.DataSource as List<FileItem>;
                RunAttachment(fileItem, fileItems);
            }
            if (fileItem.ItemType == FileItemType.Folder)
            {
                int ind = fileItem.ItemPath.LastIndexOf(_lastAttachmentsFolder);
                LoadFolderTree(fileItem.ItemPath, fileItem.ItemPath.Length - (ind + _lastAttachmentsFolder.Length) > 1);
            }
        }

        private void gridViewStatistic_RowStyle(object sender, RowStyleEventArgs e)
        {
            var view = sender as GridView;
            if (e.RowHandle >= 0)
            {
                if (view != null)
                {
                    object stateEducationItem = view.GetRowCellValue(e.RowHandle, "IsSupervise");
                    if (stateEducationItem != null)
                    {
                        if (!bool.Parse(stateEducationItem.ToString()))
                        {
                            var font = new Font(e.Appearance.Font, FontStyle.Strikeout);
                            e.Appearance.Font = font;
                        }
                    }
                }
            }
        }

        private void simpleButtonShowResult_Click(object sender, EventArgs e)
        {
            if (gridViewStatistic.FocusedRowHandle < 0)
            {
                return;
            }
            object testPassingId = gridViewStatistic.GetRowCellValue(gridViewStatistic.FocusedRowHandle, "TestPassingId");
            TestPassing testPassing = ActiveRecordBase<TestPassing>.Find(testPassingId);

            ShowResult(_user.Probationer, testPassing.ProfilePassing, _selectedTest, testPassing);
        }

        #endregion

        #region UserEventHandlers

        /// <summary>
        ///     Закрытие тестирования
        /// </summary>
        /// <param name="control"></param>
        private void UcControlClosed(MainTestingControl control)
        {
            try
            {
                using (new SessionScope())
                {
                    Event.Add(RunnerApplication.CurrentUser,
                        control.DoTest ? EventTypes.AbortTesting : EventTypes.EndTesting, control.CurrentTest.Id);
                }
                MethodInvoker method = delegate
                {
                    control.DisposeControl();
                    if (control.ParentForm != null)
                        control.ParentForm.Close();
                };
                BeginInvoke(method);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                MessageForm.ShowErrorMsg("Произошла ошибка при сохранении прохождения теста");
            }
        }

        private void mainWindowFinishTest(TestPassing tp, bool isSupervise, FinishEventArgs e)
        {
            using (new SessionScope())
            {
                Event.Add(RunnerApplication.CurrentUser, EventTypes.EndTesting, tp.Test.Id);
                tp = ActiveRecordBase<TestPassing>.Find(tp.Id);

                var ti = new TimeInterval(tp.TimeInterval.StartTime,
                    tp.TimeInterval.StartTime.AddTicks(e.TimePeriod.Ticks));
                ti.CreateAndFlush();


                tp.TimeInterval = ti;
                tp.UpdateAndFlush();
            }

            //Скорость Группы в час 
            var speed = (int) ((e.SymbolPerMinute*60)/5);

            //Количество ошибок
            double err = e.CountError/
                         ((double) e.InputText.Length/500);

            int perc = 0;
            int valuation = 2;
            if (speed >= tp.Test.Speed &&
                e.CountError <= tp.Test.MaxErrorPercent)
            {
                perc = 100;
                valuation = 5;
            }
            //var desc = string.Format("Скорость {0} гр/час. {1:0.##} ошибок/100 гр",speed, err);
            string desc = string.Format("Скорость {0} гр/час. {1} ошибок на {2} знаков", speed, e.CountError,
                e.InputText.Length);
            try
            {
                ProfessionalDbExecutor.UpdateStatistic(tp, isSupervise, perc, valuation, speed, err, desc);
            }
            catch (Exception ex)
            {
                MessageForm.ShowErrorMsg(ex.Message);
            }
        }

        #endregion

        private void gridViewStatistic_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            var gridView = (sender as GridView);
            var selectedRowsIndexes = gridView.GetSelectedRows();
            if (selectedRowsIndexes.Length > 0)
                simpleButtonShowResult.Enabled = true;
        }
    }
}
﻿using System;
using System.Data.Linq;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraBars;
using Eureca.Professional.ActiveRecords;
using Eureca.Professional.BaseComponents;
using Eureca.Professional.BaseComponents.Configuration;
using Eureca.Professional.CommonClasses;
using Eureca.Professional.ProfessionalProbationer.Properties;
using System.Configuration;
using log4net;

namespace Eureca.Professional.ProfessionalProbationer
{
    public partial class StartForm : ProfessionalStartForm
    {
        private static readonly ILog Log = LogManager.GetLogger( typeof( StartForm ) );

        private bool _exit;

        public StartForm()
        {
            InitializeComponent();
        }

        private void barButtonItemDB_ItemClick(object sender, ItemClickEventArgs e)
        {
            ConnectDbClick();
        }

        private void BarButtonItemChangeUserItemClick(object sender, ItemClickEventArgs e)
        {
            ChangeUserClick();
        }

        private void barButtonItemSettings_ItemClick(object sender, ItemClickEventArgs e)
        {
            ShowSettingsClick();
        }

        private void BarButtonItemExitItemClick(object sender, ItemClickEventArgs e)
        {
            Close();
        }

        private void BarButtonItemRunTestsItemClick(object sender, ItemClickEventArgs e)
        {
            RunEducationTesting();
        }

        private void RunEducationTesting()
        {
            try
            {
                if (_exit)
                    return;
                try
                {
                    string str = string.Empty;
                     //Создаем подключение к БД планирования
                    var curriculumConnStr = SystemProperties.GetItem(SysPropEnum.CurriculumConnString);
                    if (curriculumConnStr == null)
                    {
                        str = "Отсутствует строка подключения к БД планирования";
                        Log.Error(str);
                        MessageForm.ShowWarningMsg(str);
                        //нет в БД подключения к СПО планирования
                    }
                    else
                    {
                        Log.Error("Строка подключения получаемая из БД "+curriculumConnStr.Value);
                        Curriculum.Program.SetCustomConnectionString(curriculumConnStr.Value);
                        if (!Curriculum.Program.DbSingletone.DatabaseExists())
                        {
                            str = "Некорректная строка подключения к БД планирования";
                            throw new Exception(str);
                        }
                        Curriculum.Program.DbSingletone.Connection.Open();
                        Log.Error("Строка подключения в _dataContextInstance = " + Curriculum.Program.DbSingletone.Connection.ConnectionString);
                        Log.Error("Пользователей в планировании = " + Curriculum.Program.DbSingletone.Persons.Count());
                        //Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                        //config.ConnectionStrings.ConnectionStrings["Eureca.Professional.ProfessionalProbationer.Properties.Settings.CurriculumConnectionString"].ConnectionString = curriculumConnStr.Value;
                        //config.Save(ConfigurationSaveMode.Modified, true);
                        //ConfigurationManager.RefreshSection("connectionStrings");

                    }
                    var uc = new RunEducation(RunnerApplication.CurrentUser);
                    uc.ShowDialog();
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                    MessageForm.ShowErrorMsg(ex.Message);
                }
                ChangeUser();
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }

        private void ChangeUser()
        {
            //делаем переключение флага инициализации для того чтобы мы смогли повторно ингициализировать
            RunnerApplication.ResetInitActiveRecord();
            //происходит новая инициализация
            var par = new StartUpParameters(){ApplicationIcon = Resources.ProbationerIcon};
            par.ARMSettings = ARMSettingsSection.GetFromConfigSource();
            if (!RunnerApplication.ConnectDb() || !RunnerApplication.Authotization(par))
            {
                AskBeforeExit = false;
                Close();
                Application.Exit();
                _exit = true;
            }
            RunEducationTesting();
        }

        private void StartForm_Load(object sender, EventArgs e)
        {
            Visible = false;
            RunEducationTesting();
        }

        private void barButtonItemInfo_ItemClick( object sender, ItemClickEventArgs e )
        {
            RunnerApplication.ShowInfoPage( );
        }
    }
}
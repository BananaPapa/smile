﻿namespace Eureca.Professional.ProfessionalProbationer
{
    partial class StartForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StartForm));
            this.barButtonItemInfo = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barButtonItemRunTests = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemChangeUser = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemExit = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemSettings = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemDB = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPageWork = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroupTestts = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup5 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            ((System.ComponentModel.ISupportInitialize)(this.workPanel)).BeginInit();
            this.workPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            this.SuspendLayout();
            // 
            // workPanel
            // 
            this.workPanel.Appearance.ForeColor = System.Drawing.Color.Transparent;
            this.workPanel.Appearance.Options.UseForeColor = true;
            this.workPanel.Controls.Add(this.ribbonControl1);
            this.workPanel.LookAndFeel.SkinName = "Blue";
            this.workPanel.Size = new System.Drawing.Size(540, 119);
            // 
            // ribbonPageGroup2
            // 
            ribbonPageGroup2.AllowTextClipping = false;
            ribbonPageGroup2.ItemLinks.Add(this.barButtonItemInfo);
            ribbonPageGroup2.Name = "ribbonPageGroup2";
            ribbonPageGroup2.Text = "Ифнормация";
            // 
            // barButtonItemInfo
            // 
            this.barButtonItemInfo.Caption = "Справка";
            this.barButtonItemInfo.Id = 18;
            this.barButtonItemInfo.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemInfo.LargeGlyph")));
            this.barButtonItemInfo.Name = "barButtonItemInfo";
            this.barButtonItemInfo.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemInfo_ItemClick);
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.barButtonItemRunTests,
            this.barButtonItemChangeUser,
            this.barButtonItemExit,
            this.barButtonItemSettings,
            this.barButtonItemDB,
            this.barButtonItemInfo});
            this.ribbonControl1.Location = new System.Drawing.Point(2, 2);
            this.ribbonControl1.MaxItemId = 19;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPageWork});
            this.ribbonControl1.ShowToolbarCustomizeItem = false;
            this.ribbonControl1.Size = new System.Drawing.Size(536, 115);
            this.ribbonControl1.Toolbar.ShowCustomizeItem = false;
            this.ribbonControl1.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden;
            // 
            // barButtonItemRunTests
            // 
            this.barButtonItemRunTests.Caption = "Тестирование";
            this.barButtonItemRunTests.Id = 1;
            this.barButtonItemRunTests.LargeGlyph = global::Eureca.Professional.ProfessionalProbationer.Properties.Resources.start;
            this.barButtonItemRunTests.Name = "barButtonItemRunTests";
            this.barButtonItemRunTests.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BarButtonItemRunTestsItemClick);
            // 
            // barButtonItemChangeUser
            // 
            this.barButtonItemChangeUser.Caption = "Смена пользователя";
            this.barButtonItemChangeUser.Id = 10;
            this.barButtonItemChangeUser.LargeGlyph = global::Eureca.Professional.ProfessionalProbationer.Properties.Resources.ChangeUsers;
            this.barButtonItemChangeUser.Name = "barButtonItemChangeUser";
            this.barButtonItemChangeUser.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BarButtonItemChangeUserItemClick);
            // 
            // barButtonItemExit
            // 
            this.barButtonItemExit.Caption = "Завершение работы";
            this.barButtonItemExit.Id = 12;
            this.barButtonItemExit.LargeGlyph = global::Eureca.Professional.ProfessionalProbationer.Properties.Resources.exit;
            this.barButtonItemExit.Name = "barButtonItemExit";
            this.barButtonItemExit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BarButtonItemExitItemClick);
            // 
            // barButtonItemSettings
            // 
            this.barButtonItemSettings.Caption = "Параметры приложения";
            this.barButtonItemSettings.Id = 14;
            this.barButtonItemSettings.LargeGlyph = global::Eureca.Professional.ProfessionalProbationer.Properties.Resources.options;
            this.barButtonItemSettings.Name = "barButtonItemSettings";
            this.barButtonItemSettings.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemSettings_ItemClick);
            // 
            // barButtonItemDB
            // 
            this.barButtonItemDB.Caption = "Подключение к БД";
            this.barButtonItemDB.Id = 15;
            this.barButtonItemDB.LargeGlyph = global::Eureca.Professional.ProfessionalProbationer.Properties.Resources.network_server_database32;
            this.barButtonItemDB.Name = "barButtonItemDB";
            this.barButtonItemDB.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemDB_ItemClick);
            // 
            // ribbonPageWork
            // 
            this.ribbonPageWork.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroupTestts,
            this.ribbonPageGroup1,
            this.ribbonPageGroup5,
            ribbonPageGroup2});
            this.ribbonPageWork.Name = "ribbonPageWork";
            this.ribbonPageWork.Text = "Действия пользователя";
            // 
            // ribbonPageGroupTestts
            // 
            this.ribbonPageGroupTestts.ItemLinks.Add(this.barButtonItemRunTests);
            this.ribbonPageGroupTestts.Name = "ribbonPageGroupTestts";
            this.ribbonPageGroupTestts.ShowCaptionButton = false;
            this.ribbonPageGroupTestts.Text = "Управление системой";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItemSettings);
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItemDB);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.ShowCaptionButton = false;
            this.ribbonPageGroup1.Text = "Настройки";
            // 
            // ribbonPageGroup5
            // 
            this.ribbonPageGroup5.ItemLinks.Add(this.barButtonItemChangeUser);
            this.ribbonPageGroup5.ItemLinks.Add(this.barButtonItemExit);
            this.ribbonPageGroup5.Name = "ribbonPageGroup5";
            this.ribbonPageGroup5.ShowCaptionButton = false;
            this.ribbonPageGroup5.Text = "Завершение работы";
            // 
            // StartForm
            // 
            this.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.Appearance.Options.UseForeColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(540, 119);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.LookAndFeel.SkinName = "Blue";
            this.MaximizeBox = false;
            this.Name = "StartForm";
            this.Text = "СПО «Ученик»";
            this.Load += new System.EventHandler(this.StartForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.workPanel)).EndInit();
            this.workPanel.ResumeLayout(false);
            this.workPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPageWork;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupTestts;
        private DevExpress.XtraBars.BarButtonItem barButtonItemRunTests;
        private DevExpress.XtraBars.BarButtonItem barButtonItemChangeUser;
        private DevExpress.XtraBars.BarButtonItem barButtonItemExit;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup5;
        private DevExpress.XtraBars.BarButtonItem barButtonItemSettings;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.BarButtonItem barButtonItemDB;
        private DevExpress.XtraBars.BarButtonItem barButtonItemInfo;
    }
}
﻿namespace Eureca.Professional.ProfessionalProbationer
{
    partial class RunEducation
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.simpleButtonRunSelectedTest = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonRunProfile = new DevExpress.XtraEditors.SimpleButton();
            this.gridControlTest = new DevExpress.XtraGrid.GridControl();
            this.gridViewTests = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTimeEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit();
            this.simpleButtonStartTest = new DevExpress.XtraEditors.SimpleButton();
            this.gridControlStatistic = new DevExpress.XtraGrid.GridControl();
            this.gridViewStatistic = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTimeEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.splitContainerControlTests = new DevExpress.XtraEditors.SplitContainerControl();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageProfile = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabPageLessons = new DevExpress.XtraTab.XtraTabPage();
            this.lessonsControlProb = new Curriculum.LessonsControl();
            this.gridControlFiles = new DevExpress.XtraGrid.GridControl();
            this.gridViewFiles = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnIcon = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemPictureEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.gridColumnName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.splitContainerControlAll = new DevExpress.XtraEditors.SplitContainerControl();
            this.simpleButtonShowResult = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlTest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTests)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlStatistic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewStatistic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControlTests)).BeginInit();
            this.splitContainerControlTests.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPageProfile.SuspendLayout();
            this.xtraTabPageLessons.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlFiles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewFiles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControlAll)).BeginInit();
            this.splitContainerControlAll.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.simpleButtonRunSelectedTest);
            this.groupControl1.Controls.Add(this.simpleButtonRunProfile);
            this.groupControl1.Controls.Add(this.gridControlTest);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(470, 317);
            this.groupControl1.TabIndex = 1;
            this.groupControl1.Text = "Тематики и тесты профиля";
            // 
            // simpleButtonRunSelectedTest
            // 
            this.simpleButtonRunSelectedTest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.simpleButtonRunSelectedTest.Appearance.Font = new System.Drawing.Font("Tahoma", 11F);
            this.simpleButtonRunSelectedTest.Appearance.Options.UseFont = true;
            this.simpleButtonRunSelectedTest.Location = new System.Drawing.Point(5, 273);
            this.simpleButtonRunSelectedTest.Name = "simpleButtonRunSelectedTest";
            this.simpleButtonRunSelectedTest.Size = new System.Drawing.Size(213, 39);
            this.simpleButtonRunSelectedTest.TabIndex = 2;
            this.simpleButtonRunSelectedTest.Text = "Запуск выбранных тестов";
            this.simpleButtonRunSelectedTest.Click += new System.EventHandler(this.SimpleButtonRunSelectedTestClick);
            // 
            // simpleButtonRunProfile
            // 
            this.simpleButtonRunProfile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.simpleButtonRunProfile.Appearance.Font = new System.Drawing.Font("Tahoma", 11F);
            this.simpleButtonRunProfile.Appearance.Options.UseFont = true;
            this.simpleButtonRunProfile.Location = new System.Drawing.Point(224, 273);
            this.simpleButtonRunProfile.Name = "simpleButtonRunProfile";
            this.simpleButtonRunProfile.Size = new System.Drawing.Size(201, 39);
            this.simpleButtonRunProfile.TabIndex = 1;
            this.simpleButtonRunProfile.Text = "Запуск всего профиля";
            this.simpleButtonRunProfile.Click += new System.EventHandler(this.SimpleButtonRunProfileClick);
            // 
            // gridControlTest
            // 
            this.gridControlTest.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControlTest.Location = new System.Drawing.Point(2, 20);
            this.gridControlTest.MainView = this.gridViewTests;
            this.gridControlTest.Name = "gridControlTest";
            this.gridControlTest.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTimeEdit2});
            this.gridControlTest.Size = new System.Drawing.Size(463, 247);
            this.gridControlTest.TabIndex = 0;
            this.gridControlTest.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewTests});
            // 
            // gridViewTests
            // 
            this.gridViewTests.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5});
            this.gridViewTests.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridViewTests.GridControl = this.gridControlTest;
            this.gridViewTests.GroupCount = 1;
            this.gridViewTests.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Count, "Method", null, "")});
            this.gridViewTests.Name = "gridViewTests";
            this.gridViewTests.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridViewTests.OptionsBehavior.Editable = false;
            this.gridViewTests.OptionsSelection.MultiSelect = true;
            this.gridViewTests.OptionsView.ShowAutoFilterRow = true;
            this.gridViewTests.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn3, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewTests.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.GridView1FocusedRowChanged);
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Идентификатор";
            this.gridColumn1.FieldName = "Id";
            this.gridColumn1.Name = "gridColumn1";
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Наименование";
            this.gridColumn2.FieldName = "Title";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Тематика";
            this.gridColumn3.FieldName = "Method";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count)});
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Тип";
            this.gridColumn4.FieldName = "CommentType";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 1;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Время";
            this.gridColumn5.ColumnEdit = this.repositoryItemTimeEdit2;
            this.gridColumn5.FieldName = "TimeLimitDatetime";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 2;
            // 
            // repositoryItemTimeEdit2
            // 
            this.repositoryItemTimeEdit2.AutoHeight = false;
            this.repositoryItemTimeEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemTimeEdit2.Name = "repositoryItemTimeEdit2";
            // 
            // simpleButtonStartTest
            // 
            this.simpleButtonStartTest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonStartTest.Appearance.Font = new System.Drawing.Font("Tahoma", 11F);
            this.simpleButtonStartTest.Appearance.Options.UseFont = true;
            this.simpleButtonStartTest.Location = new System.Drawing.Point(698, 150);
            this.simpleButtonStartTest.Name = "simpleButtonStartTest";
            this.simpleButtonStartTest.Size = new System.Drawing.Size(140, 33);
            this.simpleButtonStartTest.TabIndex = 7;
            this.simpleButtonStartTest.Text = "Пройти тест";
            this.simpleButtonStartTest.Click += new System.EventHandler(this.SimpleButtonStartTestClick);
            // 
            // gridControlStatistic
            // 
            this.gridControlStatistic.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControlStatistic.Location = new System.Drawing.Point(0, 0);
            this.gridControlStatistic.MainView = this.gridViewStatistic;
            this.gridControlStatistic.Name = "gridControlStatistic";
            this.gridControlStatistic.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTimeEdit3});
            this.gridControlStatistic.Size = new System.Drawing.Size(839, 143);
            this.gridControlStatistic.TabIndex = 0;
            this.gridControlStatistic.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewStatistic});
            // 
            // gridViewStatistic
            // 
            this.gridViewStatistic.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gridViewStatistic.Appearance.Row.Options.UseFont = true;
            this.gridViewStatistic.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn9,
            this.gridColumn12,
            this.gridColumn8,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn17});
            this.gridViewStatistic.GridControl = this.gridControlStatistic;
            this.gridViewStatistic.Name = "gridViewStatistic";
            this.gridViewStatistic.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridViewStatistic.OptionsBehavior.Editable = false;
            this.gridViewStatistic.OptionsView.ShowAutoFilterRow = true;
            this.gridViewStatistic.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.gridViewStatistic_RowStyle);
            this.gridViewStatistic.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridViewStatistic_SelectionChanged);
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Дата начала";
            this.gridColumn6.FieldName = "sTime";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 1;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Время начала";
            this.gridColumn7.ColumnEdit = this.repositoryItemTimeEdit3;
            this.gridColumn7.FieldName = "sTime";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 2;
            // 
            // repositoryItemTimeEdit3
            // 
            this.repositoryItemTimeEdit3.AutoHeight = false;
            this.repositoryItemTimeEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemTimeEdit3.Name = "repositoryItemTimeEdit3";
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Время окончания";
            this.gridColumn9.ColumnEdit = this.repositoryItemTimeEdit3;
            this.gridColumn9.FieldName = "eTime";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 3;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Длительность(сек)";
            this.gridColumn12.FieldName = "TimeDuration";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 4;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Наименование теста";
            this.gridColumn8.FieldName = "Title";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 0;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Результат(%)";
            this.gridColumn10.FieldName = "Perc";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 5;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Оценка";
            this.gridColumn11.FieldName = "Valuation";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 6;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "IsSuperVise";
            this.gridColumn15.FieldName = "IsSuperVise";
            this.gridColumn15.Name = "gridColumn15";
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "gridColumn16";
            this.gridColumn16.FieldName = "testpassingid";
            this.gridColumn16.Name = "gridColumn16";
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Комментарий";
            this.gridColumn17.FieldName = "Description";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 7;
            // 
            // splitContainerControlTests
            // 
            this.splitContainerControlTests.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControlTests.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControlTests.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControlTests.Name = "splitContainerControlTests";
            this.splitContainerControlTests.Panel1.Controls.Add(this.xtraTabControl1);
            this.splitContainerControlTests.Panel1.MinSize = 322;
            this.splitContainerControlTests.Panel1.Text = "Panel1";
            this.splitContainerControlTests.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.splitContainerControlTests.Panel2.Controls.Add(this.gridControlFiles);
            this.splitContainerControlTests.Panel2.ShowCaption = true;
            this.splitContainerControlTests.Panel2.Text = "Учебные материалы";
            this.splitContainerControlTests.Size = new System.Drawing.Size(843, 343);
            this.splitContainerControlTests.SplitterPosition = 362;
            this.splitContainerControlTests.TabIndex = 4;
            this.splitContainerControlTests.Text = "splitContainerControlTests";
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPageProfile;
            this.xtraTabControl1.Size = new System.Drawing.Size(475, 343);
            this.xtraTabControl1.TabIndex = 2;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageProfile,
            this.xtraTabPageLessons});
            // 
            // xtraTabPageProfile
            // 
            this.xtraTabPageProfile.Controls.Add(this.groupControl1);
            this.xtraTabPageProfile.Name = "xtraTabPageProfile";
            this.xtraTabPageProfile.Size = new System.Drawing.Size(470, 317);
            this.xtraTabPageProfile.Text = "Профиль";
            // 
            // xtraTabPageLessons
            // 
            this.xtraTabPageLessons.Controls.Add(this.lessonsControlProb);
            this.xtraTabPageLessons.Name = "xtraTabPageLessons";
            this.xtraTabPageLessons.Size = new System.Drawing.Size(470, 317);
            this.xtraTabPageLessons.Text = "Занятия";
            // 
            // lessonsControlProb
            // 
            this.lessonsControlProb.CurrentDate = new System.DateTime(2014, 9, 5, 0, 0, 0, 0);
            this.lessonsControlProb.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lessonsControlProb.HasInnerHideLogic = false;
            this.lessonsControlProb.HierarchyParent = null;
            this.lessonsControlProb.Location = new System.Drawing.Point(0, 0);
            this.lessonsControlProb.ManualFilter = true;
            this.lessonsControlProb.Name = "lessonsControlProb";
            this.lessonsControlProb.Person = null;
            this.lessonsControlProb.PrefixTag = null;
            this.lessonsControlProb.ReadOnly = true;
            this.lessonsControlProb.Size = new System.Drawing.Size(470, 317);
            this.lessonsControlProb.TabIndex = 0;
            this.lessonsControlProb.Tag = "Управление занятиями#r";
            this.lessonsControlProb.LessonClick += new System.EventHandler<Curriculum.LessonClickEventArgs>(this.LessonsControlProbLessonClick);
            // 
            // gridControlFiles
            // 
            this.gridControlFiles.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlFiles.Location = new System.Drawing.Point(0, 0);
            this.gridControlFiles.MainView = this.gridViewFiles;
            this.gridControlFiles.Name = "gridControlFiles";
            this.gridControlFiles.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPictureEdit1});
            this.gridControlFiles.Size = new System.Drawing.Size(358, 320);
            this.gridControlFiles.TabIndex = 0;
            this.gridControlFiles.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewFiles});
            // 
            // gridViewFiles
            // 
            this.gridViewFiles.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnIcon,
            this.gridColumnName,
            this.gridColumn13,
            this.gridColumn14});
            this.gridViewFiles.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridViewFiles.GridControl = this.gridControlFiles;
            this.gridViewFiles.Name = "gridViewFiles";
            this.gridViewFiles.OptionsBehavior.AutoSelectAllInEditor = false;
            this.gridViewFiles.OptionsBehavior.Editable = false;
            this.gridViewFiles.OptionsCustomization.AllowGroup = false;
            this.gridViewFiles.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewFiles.OptionsView.ShowAutoFilterRow = true;
            this.gridViewFiles.OptionsView.ShowGroupPanel = false;
            this.gridViewFiles.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn14, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridViewFiles.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.GridViewFilesRowClick);
            this.gridViewFiles.KeyUp += new System.Windows.Forms.KeyEventHandler(this.GridViewFilesKeyUp);
            // 
            // gridColumnIcon
            // 
            this.gridColumnIcon.Caption = " ";
            this.gridColumnIcon.ColumnEdit = this.repositoryItemPictureEdit1;
            this.gridColumnIcon.FieldName = "ItemIcon";
            this.gridColumnIcon.Name = "gridColumnIcon";
            this.gridColumnIcon.OptionsColumn.AllowEdit = false;
            this.gridColumnIcon.OptionsColumn.AllowFocus = false;
            this.gridColumnIcon.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumnIcon.OptionsColumn.AllowIncrementalSearch = false;
            this.gridColumnIcon.OptionsColumn.AllowMove = false;
            this.gridColumnIcon.OptionsColumn.AllowShowHide = false;
            this.gridColumnIcon.OptionsColumn.AllowSize = false;
            this.gridColumnIcon.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumnIcon.OptionsColumn.FixedWidth = true;
            this.gridColumnIcon.OptionsColumn.ReadOnly = true;
            this.gridColumnIcon.OptionsColumn.ShowCaption = false;
            this.gridColumnIcon.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumnIcon.OptionsColumn.TabStop = false;
            this.gridColumnIcon.OptionsFilter.AllowAutoFilter = false;
            this.gridColumnIcon.OptionsFilter.AllowFilter = false;
            this.gridColumnIcon.OptionsFilter.ImmediateUpdateAutoFilter = false;
            this.gridColumnIcon.Visible = true;
            this.gridColumnIcon.VisibleIndex = 0;
            this.gridColumnIcon.Width = 20;
            // 
            // repositoryItemPictureEdit1
            // 
            this.repositoryItemPictureEdit1.Name = "repositoryItemPictureEdit1";
            this.repositoryItemPictureEdit1.NullText = " ";
            this.repositoryItemPictureEdit1.ShowMenu = false;
            // 
            // gridColumnName
            // 
            this.gridColumnName.Caption = "Наименование";
            this.gridColumnName.FieldName = "ItemName";
            this.gridColumnName.Name = "gridColumnName";
            this.gridColumnName.Visible = true;
            this.gridColumnName.VisibleIndex = 1;
            this.gridColumnName.Width = 239;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Размер";
            this.gridColumn13.FieldName = "LengthView";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 2;
            this.gridColumn13.Width = 39;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Тип";
            this.gridColumn14.FieldName = "ItemExtension";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 3;
            this.gridColumn14.Width = 39;
            // 
            // splitContainerControlAll
            // 
            this.splitContainerControlAll.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControlAll.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControlAll.Horizontal = false;
            this.splitContainerControlAll.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControlAll.Name = "splitContainerControlAll";
            this.splitContainerControlAll.Panel1.Controls.Add(this.splitContainerControlTests);
            this.splitContainerControlAll.Panel1.ShowCaption = true;
            this.splitContainerControlAll.Panel1.Text = "Иванов Иван Иванович";
            this.splitContainerControlAll.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.splitContainerControlAll.Panel2.Controls.Add(this.simpleButtonShowResult);
            this.splitContainerControlAll.Panel2.Controls.Add(this.simpleButtonStartTest);
            this.splitContainerControlAll.Panel2.Controls.Add(this.gridControlStatistic);
            this.splitContainerControlAll.Panel2.ShowCaption = true;
            this.splitContainerControlAll.Panel2.Text = "Статистика по тестированию";
            this.splitContainerControlAll.Size = new System.Drawing.Size(843, 557);
            this.splitContainerControlAll.SplitterPosition = 208;
            this.splitContainerControlAll.TabIndex = 5;
            this.splitContainerControlAll.Text = "splitContainerControlAll";
            // 
            // simpleButtonShowResult
            // 
            this.simpleButtonShowResult.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonShowResult.Appearance.Font = new System.Drawing.Font("Tahoma", 11F);
            this.simpleButtonShowResult.Appearance.Options.UseFont = true;
            this.simpleButtonShowResult.Enabled = false;
            this.simpleButtonShowResult.Location = new System.Drawing.Point(522, 150);
            this.simpleButtonShowResult.Name = "simpleButtonShowResult";
            this.simpleButtonShowResult.Size = new System.Drawing.Size(170, 33);
            this.simpleButtonShowResult.TabIndex = 8;
            this.simpleButtonShowResult.Text = "Посмотреть результат";
            this.simpleButtonShowResult.Click += new System.EventHandler(this.simpleButtonShowResult_Click);
            // 
            // RunEducation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(843, 557);
            this.Controls.Add(this.splitContainerControlAll);
            this.MinimumSize = new System.Drawing.Size(859, 595);
            this.Name = "RunEducation";
            this.Text = "СПО «Обучаемый специалист»";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.RunEducationFormClosing);
            this.Load += new System.EventHandler(this.RunEducationLoad);
            this.Shown += new System.EventHandler(this.RunEducationShown);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlTest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTests)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlStatistic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewStatistic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControlTests)).EndInit();
            this.splitContainerControlTests.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPageProfile.ResumeLayout(false);
            this.xtraTabPageLessons.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlFiles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewFiles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControlAll)).EndInit();
            this.splitContainerControlAll.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraGrid.GridControl gridControlTest;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewTests;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraEditors.SimpleButton simpleButtonRunSelectedTest;
        private DevExpress.XtraEditors.SimpleButton simpleButtonRunProfile;
        private DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit repositoryItemTimeEdit2;
        private DevExpress.XtraGrid.GridControl gridControlStatistic;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewStatistic;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit repositoryItemTimeEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControlTests;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControlAll;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageProfile;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageLessons;
        private Curriculum.LessonsControl lessonsControlProb;
        private DevExpress.XtraEditors.SimpleButton simpleButtonStartTest;
        private DevExpress.XtraGrid.GridControl gridControlFiles;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewFiles;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnIcon;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraEditors.SimpleButton simpleButtonShowResult;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
    }
}

﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using DevExpress.Skins;
using Eureca.Integrator.Utility;
using Eureca.Professional.ActiveRecords;
using Eureca.Professional.BaseComponents;
using Eureca.Professional.BaseComponents.Configuration;
using Eureca.Professional.CommonClasses;
using Eureca.Professional.CommonClasses.Configuration;
using Eureca.Updater.UpdaterClient;
using Eureca.Utility.Extensions;
using log4net;
using MessageBox = Eureca.Integrator.Utility.Forms.MessageBox;
using DevExpress.XtraEditors;

namespace Eureca.Professional.ProfessionalProbationer
{
    internal static class Program
    {
        private static readonly ILog Log = LogManager.GetLogger( typeof( Program ) );

        private const int WhKeyboardLl = 13;
        private const int WmKeydown = 0x0100;
        private static readonly LowLevelKeyboardProc Proc = HookCallback;
        private static IntPtr _hookId = IntPtr.Zero;
        private static int _show;
        static bool _isAdmin = false;

        public static bool IsAdmin
        {
            get { return Program._isAdmin; }
            
        }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
           
            _hookId = SetHook(Proc);

            if (RunnerApplication.AlreadyRunning())
            {
                return;
            }
            SkinManager.EnableFormSkins();
            SkinManager.EnableMdiFormSkins();
            Thread.CurrentThread.CurrentCulture = new CultureInfo("ru-RU");
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("ru");

            Application.ThreadException += (ApplicationThreadException);
            AppDomain.CurrentDomain.UnhandledException += (CurrentDomainUnhandledException);


            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            if (!AppActivation.IsActivation(Application.StartupPath)
                && !AppActivation.Activation(Application.StartupPath))
            {
                return;
            }
            try
            {
                RunnerApplication.CurrentArm = ARMType.PROBATIONER;
                RunnerApplication.CurrentUser = null;
                //загружаю настройки арма
                ARMSettingsSection armSettings = ARMSettingsSection.GetFromConfigSource();
                //выставляю локальные настройки
                armSettings.ARMType = RunnerApplication.CurrentArm;
                //создаю контейнер для параметров
                var param = new StartUpParameters {ARMSettings = armSettings, ApplicationIcon = Properties.Resources.ProbationerIcon};

                //Синхронизация с глобальными настройками.
                ProfessionalStartForm.SyncProfSettings( );

                //запускаю функцию базового класса для всех армов
                if (RunnerApplication.ConnectDb(connStr=>connStr.Contains("Professional")))
                {
                    //сохраняю настройки
                    try
                    {
                        var ip = System.Net.Dns.GetHostEntry( System.Net.Dns.GetHostName( ) ).AddressList.FirstOrDefault( );
                        if (ip != null)
                        {
                            //прописываю IP адрес обучаемого
                            var arm = SuperviseAdresses.GetSuperviseAdresses( ip.ToString( ) ) ?? 
                                      new SuperviseAdresses( ) { IsSupervise = false };
                            arm.Adress = ip.ToString( );
                            arm.SaveAndFlush( );
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.Error( ex );
                    }

                    var identity = WindowsIdentity.GetCurrent( );
                    WindowsPrincipal pricipal = new WindowsPrincipal( identity );
                    _isAdmin = pricipal.IsInRole( WindowsBuiltInRole.Administrator );
                    Settings.Save( );
                    
                    RunnerApplication.RunUpdater(IsAdmin);
                    if (RunnerApplication.Authotization( param ))
                    {
                        Application.Run( new StartForm( ) );
                    }
                }
                UnhookWindowsHookEx(_hookId);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                MessageForm.ShowErrorMsg(ex.Message);
            }
        }

      
        private static IntPtr SetHook(LowLevelKeyboardProc proc)
        {
            using (Process curProcess = Process.GetCurrentProcess())
            using (ProcessModule curModule = curProcess.MainModule)
            {
                return SetWindowsHookEx(WhKeyboardLl, proc, GetModuleHandle(curModule.ModuleName), 0);
            }
        }

        private static IntPtr HookCallback(int nCode, IntPtr wParam, IntPtr lParam)
        {
            if ((nCode >= 0) && (wParam == (IntPtr) WmKeydown))
            {
                int vkCode = Marshal.ReadInt32(lParam);
                if (vkCode == 162)
                {
                    _show = 1;
                }
                else
                {
                    if ((Keys) vkCode == Keys.W && _show == 1)
                    {
                        ProfessionalStartForm.ShowSettingsClick();
                    }
                    if ((Keys) vkCode == Keys.Q && _show == 1)
                    {
                        ProfessionalStartForm.ConnectDbClick();
                    }
                    _show = 0;
                }
            }
            return CallNextHookEx(_hookId, nCode, wParam, lParam);
        }

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr SetWindowsHookEx(int idHook, LowLevelKeyboardProc lpfn, IntPtr hMod,
                                                      uint dwThreadId);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern bool UnhookWindowsHookEx(IntPtr hhk);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr CallNextHookEx(IntPtr hhk, int nCode, IntPtr wParam, IntPtr lParam);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr GetModuleHandle(string lpModuleName);


        private static void CurrentDomainUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Log.Error("Необработанное исключение");
            Log.Error(e.ExceptionObject);
            if (
                MessageForm.ShowYesNoQuestionMsg(
                    "Выполненное действие вызвало ошибку.\n За информацией обратитесь в службу технической поддержки. Продолжить работу?") !=
                DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private static void ApplicationThreadException(object sender, ThreadExceptionEventArgs e)
        {
            Log.Error("Необработанное исключение");
            Log.Error(e.Exception);
            if (
                MessageForm.ShowYesNoQuestionMsg(
                    "Выполненное действие вызвало ошибку.\n За информацией обратитесь в службу технической поддержки. Продолжить работу?") !=
                DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        #region Nested type: LowLevelKeyboardProc

        private delegate IntPtr LowLevelKeyboardProc(int nCode, IntPtr wParam, IntPtr lParam);

        #endregion
    }
}
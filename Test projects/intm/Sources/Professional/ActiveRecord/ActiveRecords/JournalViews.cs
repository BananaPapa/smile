﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Castle.ActiveRecord;
using Eureca.Utility.Extensions;
using NHibernate.Criterion;

namespace Eureca.Professional.ActiveRecords
{

    [ActiveRecord( "JournalsViews" )]
    public class JournalView : ActiveRecordValidationBase<JournalView>
    {
        [PrimaryKey( "JournalViewId" )]
        public int Id { get; internal set; }
        
        [BelongsTo("FormId", NotNull = false , Cascade = CascadeEnum.Delete)]
        public RtfForm ViewForm { get; set; }

        [BelongsTo("JournalId", NotNull = true , Cascade = CascadeEnum.None)]
        public Journal Journal { get; set; }

        public string Name
        {
            get 
            {
                using (new SessionScope())
                {
                    return JournalView.Find(Id).ViewForm.Name;
                }
            }
        }

        [Property( "[Default]" )]
        public bool IsDefault
        {
            get;
            set;
        }

        public JournalView (RtfForm viewForm, Journal journal)
	    {
            ViewForm = viewForm;
            Journal  =journal;
	    }

        public void DeleteWithReferences(bool force = false)
        {
            var syncJournalView = JournalView.Find( this.Id );
            var questionJournals = QuestionJournals.FindAll( Expression.Eq( "ViewForm", syncJournalView ) );
            if (questionJournals.Length > 0 )
            {
                if(! (force || MessageBox.Show(
                    "Данноое представление используется в {0} {1}. Удалить представление и тестовые задания?".FormatString(
                        questionJournals.Length, questionJournals.Length.Decline( "тестовых заданиях", "тестовом задании", "тестовых заданиях" ) ), "Удалить представление?", MessageBoxButtons.YesNo, MessageBoxIcon.Question ) == DialogResult.Yes))
                    return;
                foreach (var questionJournal in questionJournals)
                {
                    questionJournal.Question.Delete();
                    questionJournal.Delete( );
                }
            }
            var journal = syncJournalView.Journal;
            journal.JournalViews.Remove(syncJournalView);
            journal.UpdateAndFlush();
            syncJournalView.DeleteAndFlush( );
        }

        public JournalView()
        {
                
        }
    }
}

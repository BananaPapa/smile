﻿using System;
using Castle.ActiveRecord;

namespace Eureca.Professional.ActiveRecords
{
    /// <summary>
    /// Represents content active record (lazy loaded).
    /// </summary>
    [ActiveRecord("Contents", Lazy = true)]
    public class Content : ActiveRecordBase<Content>
    {
        /// <summary>
        /// Plain text content.
        /// </summary>
        private string _plainContent;

        /// <summary>
        /// Rich text content.
        /// </summary>
        private byte[] _richContent;

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <remarks>Access attribute of default constructor is set to public only for NHibernate to be able to create a proxy class as lazy loading is being used for this record.</remarks>
        [Obsolete("Use Form(string plainContent) or Form(byte[] richContent) instead.", true)]
        public Content()
        {
        }

        /// <summary>
        /// Initializes new instance of content active record with plain text.
        /// </summary>
        /// <param name="plainContent">Plain text content.</param>
        /// <exception cref="ArgumentNullException">Plain text is null.</exception>
        public Content(string plainContent)
        {
            if (plainContent == null)
            {
                throw new ArgumentNullException("plainContent");
            }

            _plainContent = plainContent;
        }

        /// <summary>
        /// Initializes new instance of content active record with rich text.
        /// </summary>
        /// <param name="richContent">Rich text data.</param>
        /// <exception cref="ArgumentNullException">Rich text is null.</exception>
        public Content(byte[] richContent)
        {
            if (richContent == null)
            {
                throw new ArgumentNullException("richContent");
            }

            _richContent = richContent;
        }

        /// <summary>
        /// Gets primary id of content.
        /// </summary>
        [PrimaryKey("ContentId")]
        public virtual long Id { get; protected internal set; }

        /// <summary>
        /// Gets or sets plain content.
        /// </summary>
        /// <remarks>If plain text is set as not null value rich text is set as null value.</remarks>
        /// <exception cref="InvalidOperationException">Value is null.</exception>
        [Property]
        public virtual string PlainContent
        {
            get { return _plainContent; }
            set { _plainContent = value; }
        }

        /// <summary>
        /// Gets or sets rich content.
        /// </summary>
        [Property(Length = 2147483647)]
        public virtual byte[] RichContent
        {
            get { return _richContent; }
            set { _richContent = value; }
        }

        /// <summary>
        /// сравнение ответа
        /// </summary>
        /// <param name="obj">другой объект ответа</param>
        /// <returns></returns>
        public override bool Equals( object obj )
        {
            var answerObj = (Content)obj;
            return Id == answerObj.Id;
        }

        /// <summary>
        /// получение хэш кода объекта
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode( )
        {
            return base.GetHashCode( );
        }
    }
}
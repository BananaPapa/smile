﻿using System;
using Castle.ActiveRecord;

namespace Eureca.Professional.ActiveRecords
{
    /// <summary>
    /// Represents sten active record.
    /// </summary>
    [ActiveRecord("Stens")]
    public class Sten : ActiveRecordBase<Sten>
    {
        /// <summary>
        ///  Default constructor to be used internally.
        /// </summary>
        internal Sten()
        {
        }

        /// <summary>
        /// Initializes new instance of sten active record.
        /// </summary>
        /// <param name="test">Test active record.</param>
        /// <param name="scale">Scale active record.</param>
        /// <param name="minRawPoints">Minimum value of raw points.</param>
        /// <param name="maxRawPoints">Maximum value of raw points.</param>
        /// <param name="value">Value of sten.</param>
        /// <param name="description">Description of sten (null if no description needed).</param>
        /// <exception cref="ArgumentNullException">Test or scale is null.</exception>
        public Sten(Test test, Scale scale, float minRawPoints, float maxRawPoints, int value, string description)
        {
            if (test == null)
            {
                throw new ArgumentNullException("test");
            }

            if (scale == null)
            {
                throw new ArgumentNullException("scale");
            }

            Test = test;
            Scale = scale;
            MinRawPoints = minRawPoints;
            MaxRawPoints = maxRawPoints;
            Value = value;
            Description = description;
        }

        /// <summary>
        /// Initializes new instance of sten active record without description.
        /// </summary>
        /// <param name="test">Test active record.</param>
        /// <param name="scale">Scale active record.</param>
        /// <param name="minRawPoints">Minimum value of raw points.</param>
        /// <param name="maxRawPoints">Maximum value of raw points.</param>
        /// <param name="value">Value of sten.</param>
        /// <exception cref="ArgumentNullException">Test or scale is null.</exception>
        public Sten(Test test, Scale scale, float minRawPoints, float maxRawPoints, int value)
            : this(test, scale, minRawPoints, maxRawPoints, value, null)
        {
        }

        /// <summary>
        /// Gets primary id of sten.
        /// </summary>
        [PrimaryKey("StenId")]
        public int Id { get; internal set; }

        /// <summary>
        /// Gets test of sten.
        /// </summary>
        [BelongsTo("TestId", Cascade = CascadeEnum.SaveUpdate)]
        public Test Test { get; internal set; }

        /// <summary>
        /// Gets scale of sten.
        /// </summary>
        [BelongsTo("ScaleId", Cascade = CascadeEnum.SaveUpdate)]
        public Scale Scale { get; internal set; }

        /// <summary>
        /// Gets or sets minimum value of raw points.
        /// </summary>
        [Property]
        public float MinRawPoints { get; set; }

        /// <summary>
        /// Gets or sets maximum value of raw points.
        /// </summary>
        [Property]
        public float MaxRawPoints { get; set; }

        /// <summary>
        /// Gets or sets value of sten.
        /// </summary>
        [Property]
        public int Value { get; set; }

        /// <summary>
        /// Gets or sets description of sten.
        /// </summary>
        [Property]
        public string Description { get; set; }
    }
}
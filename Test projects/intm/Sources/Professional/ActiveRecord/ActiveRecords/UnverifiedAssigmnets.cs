﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.ActiveRecord;

namespace Eureca.Professional.ActiveRecords
{
    [ActiveRecord("vUnverifiedAssignments")]
    public class vUnverifiedAssignments : ActiveRecordBase<vUnverifiedAssignments>
    {
        public vUnverifiedAssignments()
        {

        }

        [Property("[start]")]
        public DateTime Start { get; internal set; }

        [Property("[end]")]
        public DateTime End { get; internal set; }

        [PrimaryKey("StatisticId")]
        public int Id { get; set; }

        [Property("tpId")]
        public int TestPassingsId { get; internal set; }

        [Property( "FirstName" )]
        public string FirstName { get; internal set; }

        [Property( "MiddleName" )]
        public string MiddleName { get; internal set; }

        [Property( "LastName" )]
        public string LastName { get; internal set; }

        [Property( "Title" )]
        public string Title { get; internal set; }

    }
}

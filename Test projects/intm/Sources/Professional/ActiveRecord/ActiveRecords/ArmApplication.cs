﻿using System;
using Castle.ActiveRecord;

namespace Eureca.Professional.ActiveRecords
{
    /// <summary>
    /// перечислиние типов армов
    /// </summary>
    public enum ARMType
    {
        /// <summary>
        /// обучаемый специалист
        /// </summary>
        PROBATIONER = 1,
        /// <summary>
        /// преподаватель
        /// </summary>
        TEACHER = 2,
        /// <summary>
        /// администратор
        /// </summary>
        ADMINISTRATION = 3,
    }


    /// <summary>
    /// Represents ArmApplication active record.
    /// Not for create. Only for find record from Database
    /// </summary>
    [ActiveRecord("ArmApplications")]
    public class ArmApplication : ActiveRecordBase<ArmApplication>, IComparable
    {
        /// <summary>
        /// Default constructor to be used internally.
        /// </summary>
        internal ArmApplication()
        {
        }

        ///// <summary>
        ///// Initializes new instance of ArmApplication active record.
        ///// </summary>
        ///// <param name="name">ArmApplication name.</param>
        ///// <param name="abbreviation">Abbreviation of ArmApplication name.</param>
        ///// <exception cref="ArgumentNullException">Name is null.</exception>
        //private  ArmApplication(string name)
        //{
        //    if (name == null)
        //    {
        //        throw new ArgumentNullException("name");
        //    }

        //    Name = name;


        //}

        /// <summary>
        /// Gets primary id of ArmApplication.
        /// </summary>
        [PrimaryKey("ArmApplicationId")]
        public int Id { get; internal set; }

        /// <summary>
        /// Gets or sets ArmApplication name.
        /// </summary>
        [Property(Unique = true, NotNull = true)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets ArmApplication name.
        /// </summary>
        [Property(Unique = false, NotNull = false)]
        public string IPAdress { get; set; }



        #region IComparable Members

        /// <summary>
        /// сравнение АРМ приложений
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int CompareTo(object obj)
        {
            var other = (ArmApplication) obj;
            return Id.CompareTo(other.Id);
        }

        #endregion

        /// <summary>
        /// получение объекта АРМ по перечислению
        /// </summary>
        /// <param name="armType"></param>
        /// <returns></returns>
        public static ArmApplication GetArmApplication(ARMType armType)
        {
            return Find((int) armType);
        }

        /// <summary>
        /// строковое представление АРМ
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return Name;
        }

        /// <summary>
        /// сравнение объктов
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            var ArmApplicationObj = obj as ArmApplication;
            if (ArmApplicationObj == null)
                return false;
            return Id == ArmApplicationObj.Id;
        }

        /// <summary>
        /// перегружаемый метод
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
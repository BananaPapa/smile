﻿using System;
using System.Collections.Generic;
using Castle.ActiveRecord;

namespace Eureca.Professional.ActiveRecords
{
    /// <summary>
    /// Represents test type active record.
    /// </summary>
    [ActiveRecord("TestTypes")]
    public class TestType : ActiveRecordBase<TestType>, IComparable
    {
        /// <summary>
        /// Default constructor to be used internally.
        /// </summary>
        internal TestType()
        {
        }

        /// <summary>
        /// Initializes new test type active record.
        /// </summary>
        /// <param name="title">Title of test type.</param>
        /// <param name="plugInFileName">Plug-in file name of test type.</param>
        /// <param name="isAvailable">Whether test type plug-in is available.</param>
        /// <exception cref="ArgumentNullException">Title is null or plug-in file name is null.</exception>
        public TestType(string title, string plugInFileName, bool isAvailable)
        {
            if (title == null)
            {
                throw new ArgumentNullException("title");
            }

            if (plugInFileName == null)
            {
                throw new ArgumentNullException("plugInFileName");
            }

            Title = title;
            PlugInFileName = plugInFileName;
            IsAvailable = isAvailable;
            Tests = new List<Test>();
        }

        /// <summary>
        /// Gets primary id of test type.
        /// </summary>
        [PrimaryKey("TypeId")]
        public int Id { get; internal set; }

        /// <summary>
        /// Gets or sets title of test type.
        /// </summary>
        [Property(NotNull = true, Unique = true)]
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets plug-in file name of test type.
        /// </summary>
        [Property(NotNull = true, Unique = true)]
        public string PlugInFileName { get; set; }

        /// <summary>
        /// Gets or sets whether test type plug-in is available.
        /// </summary>
        [Property]
        public bool IsAvailable { get; set; }

        /// <summary>
        /// Gets (lazily) list of tests of this type.
        /// </summary>
        [HasMany(ColumnKey = "TypeId",
            Cascade = ManyRelationCascadeEnum.All,
            Lazy = true)]
        public IList<Test> Tests { get; internal set; }

        #region IComparable Members

        /// <summary>
        /// сравнение объектов
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int CompareTo(object obj)
        {
            var other = (TestType) obj;
            return Id.CompareTo(other.Id);
        }

        #endregion

        /// <summary>
        /// строковое представление объекта
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return Title;
        }

        /// <summary>
        /// перегузка метода
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (obj is TestType)
            {
                var newObj = (TestType) obj;
                return Id == newObj.Id;
            }
            else
                return false;
        }

        /// <summary>
        /// перегрузка метода
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
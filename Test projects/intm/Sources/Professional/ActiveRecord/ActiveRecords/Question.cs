﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Castle.ActiveRecord;
using Eureca.Utility.Extensions;
using NHibernate.Criterion;

namespace Eureca.Professional.ActiveRecords
{
    /// <summary>
    /// Represents question active record.
    /// </summary>
    [ActiveRecord("Questions")]
    public class Question : ActiveRecordBase<Question>
    {
        /// <summary>
        /// Default constructor to be used internally.
        /// </summary>
        internal Question()
        {
        }

        /// <summary>
        /// Initializes new instance of question active record.
        /// </summary>
        /// <param name="content">Form of question.</param>
        /// <param name="timeLimit">Time limit to answer the question (number or seconds or null).</param>
        /// <exception cref="ArgumentNullException">Form is null.</exception>
        public Question(Content content, int? timeLimit)
        {
            if (content == null)
            {
                throw new ArgumentNullException("content");
            }

            Content = content;
            TimeLimit = timeLimit;
            Standarts = new List<FormFilling>();
            //QuestionJournals = new List<QuestionJournals>();
            // Answers = new List<Answer>();
        }

        /// <summary>
        /// Initializes new instance of question active record without time limit.
        /// </summary>
        /// <param name="content">Form of question.</param>
        /// <exception cref="ArgumentNullException">Form is null.</exception>
        public Question(Content content) : this(content, null)
        {
        }

        /// <summary>
        /// Gets primary id of question.
        /// </summary>
        [PrimaryKey("QuestionId")]
        public int Id { get; internal set; }

        /// <summary>
        /// Gets or sets content of question.
        /// </summary>
        [BelongsTo("ContentId", NotNull = true, Cascade = CascadeEnum.SaveUpdate)]
        public Content Content { get; set; }

        ///<summary>
         ///Gets or sets bounded content of question.
         ///</summary>
        [HasAndBelongsToMany( Table = "Standarts", ColumnKey = "QuestionId", ColumnRef = "FormFillingId", Cascade = ManyRelationCascadeEnum.AllDeleteOrphan)]
        public IList<FormFilling> Standarts { get; set; }

        ///<summary>
        ///Gets or sets bounded content of question.
        ///</summary>
        //[HasMany( Table = "Standarts", ColumnKey = "QuestionId", Cascade = ManyRelationCascadeEnum.All )]
        //public IList<QuestionJournals> QuestionJournals { get; set; }

        /// <summary>
        /// Gets or sets time limit of question.
        /// </summary>
        [Property]
        public int? TimeLimit { get; set; }

        public bool DeleteWithReferences( bool force,Test test )
        {
            var syncQuestion = Question.Find( this.Id );
            var syncTest = Test.Find(test.Id);
            var testResults = FillingFormsTestResult.FindAll( Expression.Eq( "Question", syncQuestion ) );
            if(force || (testResults.Length>0 && MessageBox.Show(
                "К данному заданию привязаны {0} {1} прохождния теста. Удалить задание и результаты прохождения теста?".FormatString(
                    testResults.Length, testResults.Length.Decline( "результата", "результат", "результаты" ) ), "Удалить вопрос?", MessageBoxButtons.YesNo, MessageBoxIcon.Question ) == DialogResult.Yes))
            {
                var testPassings = TestPassing.FindAll( Expression.In( "Id", testResults.Select( result => result.Id ).ToArray( ) ) );
                foreach (var testPassing in testPassings)
                {
                    testPassing.DeleteAndFlush( );
                }
                
                if (testResults.Length>0)
                {
                    foreach (var fillingFormsTestResult in testResults)
                    {
                        fillingFormsTestResult.DeleteAndFlush( );
                    }
                }
            }
            else if (testResults.Length > 0)
            {
                return false;
            }
            var questionJournals = QuestionJournals.FindAll( Expression.Eq( "Question", syncQuestion ) );
            foreach (var questionJournal in questionJournals)
            {
                questionJournal.DeleteAndFlush( );
            }
            var testQuestion = TestQuestion.FindOne( Expression.And( Expression.Eq( "Test", syncTest ),
                    Expression.Eq( "Question", syncQuestion ) ) );
            testQuestion.DeleteAndFlush( );
            syncQuestion.Standarts.Clear();
            syncQuestion.DeleteAndFlush( );
            return true;
            
        }

    }
}
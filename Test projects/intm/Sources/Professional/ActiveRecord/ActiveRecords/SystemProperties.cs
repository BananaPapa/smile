﻿using System;
using System.Collections.Generic;
using Castle.ActiveRecord;
using NHibernate.Criterion;

namespace Eureca.Professional.ActiveRecords
{
    /// <summary>
    /// Represents SystemProperties active record.
    /// </summary>
    [ActiveRecord("SystemProperties")]
    public class SystemProperties : ActiveRecordBase<SystemProperties>
    {
        /// <summary>
        /// Default constructor to be used internally.
        /// </summary>
        internal SystemProperties()
        {
        }

        /// <summary>
        /// Initializes new instance of SystemProperties active record.
        /// </summary>
        /// <param name="id">System prop enum number.</param>
        /// <param name="value">System prop value.</param>
        /// <exception cref="ArgumentNullException">value is null.</exception>
        public SystemProperties(SysPropEnum id, string value)
        {
            if (value == null)
            {
                throw new ArgumentNullException("value");
            }
            EnumId = (int)id;
            Value = value;
        }

        /// <summary>
        /// Gets primary id of scale.
        /// </summary>
        [PrimaryKey("SystemPropertyID")]
        private int Id { get; set; }


        /// <summary>
        /// Gets or sets scale name.
        /// </summary>
        [Property(Unique = true, NotNull = true)]
        public int EnumId { get; set; }


        /// <summary>
        /// Gets or sets scale name.
        /// </summary>
        [Property(Unique = true, NotNull = true)]
        public string Value { get; set; }


        ///<summary>
        ///получение значения свойства
        ///</summary>
        ///<param name="enumId"></param>
        ///<returns></returns>
        public static SystemProperties GetItem(SysPropEnum enumId)
        {
            var item = FindOne(Expression.Eq("EnumId", (int)enumId));
            if (item == null)
                return null;
            return item;
        }

       

        /// <summary>
        /// сравнение шкал
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            var systemPropertyObj = (SystemProperties) obj;
            return Id == systemPropertyObj.Id;
        }

        /// <summary>
        /// хэш код объекта
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }


    ///<summary>
    ///перечисление системных свойств
    ///</summary>
    public enum SysPropEnum
    {
        ///<summary>
        ///строка подключения к БД планирования
        ///</summary>
        CurriculumConnString = 1,
        /// <summary>
        /// Папка для прикрепленных материалов
        /// </summary>
        AttachmentsFolder = 2,
        /// <summary>
        /// Версия базы данных
        /// </summary>
        DbVersion = 3,
        /// <summary>
        /// Настройки репазитариев
        /// </summary>
        RepositorySettings = 4,
        /// <summary>
        /// Флаг на то что все тесты в данное время будут Sepervise(Зачетные)
        /// </summary>
        IsGlobalControl = 11,
        /// <summary>
        /// Список "зачётных" пользователей windows. Пользователей которые будут проходить тесты только в зачётном режиме.
        /// </summary>
        WinUsersSupervisAccounts = 5
    }
}
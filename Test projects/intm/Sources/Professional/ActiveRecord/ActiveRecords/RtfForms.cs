﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.ActiveRecord;
using NHibernate.Criterion;
using NHibernate.Criterion.Lambda;

namespace Eureca.Professional.ActiveRecords
{
    [ActiveRecord( "Forms" )]
    public class RtfForm: ActiveRecordBase<RtfForm>
    {
        public RtfForm( )
        {
            Comments = new List<Comment>();
        }

        [PrimaryKey( "FormId" )]
        public int Id { get; internal set; }

        [Property( "FormType" )]
        public FormType RtfFormType { get; set; }
        byte[] _richContent;

        [Property( "FormName" )]
        public string Name { get; set; }

        [Property( "Content", Length = 2147483647 )]
        public byte[] Content
        {
            get { return _richContent; }
            set { _richContent = value; }
        }

        public MemoryStream ContentStream
        {
            get { return new MemoryStream(_richContent,0,_richContent.Length,true,true); }
            set { _richContent = value.GetBuffer(); }
        }

        [HasMany(ColumnKey = "FormId", Cascade = ManyRelationCascadeEnum.All , Lazy = true, Inverse = true)]
        public IList<Comment> Comments { get; set; }

        public RtfForm( byte[] richContent, FormType formType,string name = null )
        {
            if (richContent == null)
            {
                throw new ArgumentNullException( "richContent" );
            }

            this.Name = name;
        
            this.RtfFormType = formType;

            this.Comments = new List<Comment>();

            _richContent = richContent;
        }

        public string FullPath
        {
            get
            {
                string path = Name;

                try
                {
                    path = GetPath( path );

                }
                catch (Exception)
                {
                    using (new SessionScope( ))
                    {
                        path = GetPath( path );
                    }
                }
                return path;
            }
        }

        private string GetPath(string path)
        {
            JournalBranch branch = null;
            if (RtfFormType == FormType.Blanc)
                branch = JournalBranch.FindOne(Expression.Eq("BlancForm", this));
            else if (RtfFormType == FormType.Answer)
            {
                var filling = FormFilling.FindAll(Expression.Eq("FilledForm", this));
                if (filling.Length > 0)
                    branch = JournalBranch.FindOne(Expression.Eq("BlancForm", filling.First().Form));
                path = branch.Name;
            }
            if (branch != null)
            {
                var parent = branch.Parent;
                while (parent != null)
                {
                    path = parent.Name + " / " + path;
                    parent = parent.Parent;
                }
                path = branch.Journal.Name + ":" + path;
            }
            return path;
        }

        /// <summary>
        /// сравнение ответа
        /// </summary>
        /// <param name="obj">другой объект ответа</param>
        /// <returns></returns>
        public override bool Equals( object obj )
        {
            var answerObj = (RtfForm)obj;
            return Id == answerObj.Id;
        }

        /// <summary>
        /// получение хэш кода объекта
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode( )
        {
            return base.GetHashCode( );
        }
    }

    public enum FormType
    {
        Blanc = 1,
        Standart = 2,
        BlancJournal = 3,
        Answer = 4,
    }
}

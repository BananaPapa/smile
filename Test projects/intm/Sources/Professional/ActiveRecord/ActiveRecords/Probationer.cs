﻿using System;
using Castle.ActiveRecord;

namespace Eureca.Professional.ActiveRecords
{
    /// <summary>
    /// Represents probationer active record.
    /// </summary>
    [ActiveRecord("Probationers")]
    public class Probationer : ActiveRecordBase<Probationer>, IComparable
    {
        internal Probationer()
        {
        }

        /// <summary>
        /// исаытуемый
        /// </summary>
        /// <param name="user"></param>
        /// <param name="profile"></param>
        public Probationer(User user, Profile profile)
        {
            if (user == null)
                throw new ArgumentNullException("user");
            if (profile == null)
                throw new ArgumentNullException("profile");
            User = user;
            Profile = profile;
        }

        /// <summary>
        /// Gets primary id of probationer.
        /// </summary>
        [PrimaryKey(PrimaryKeyType.Foreign, "UserId")]
        public int Id { get; internal set; }


        /// <summary>
        /// Gets or sets user.
        /// </summary>
        [OneToOne]
        public User User { get; set; }

        /// <summary>
        /// Gets or sets Profile.
        /// </summary>
        [BelongsTo("ProfileId", NotNull = true, Cascade = CascadeEnum.SaveUpdate)]
        public Profile Profile { get; set; }

        #region IComparable Members

        /// <summary>
        /// сравнение обучаемых специалистов
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int CompareTo(object obj)
        {
            var other = (Probationer) obj;
            return Id.CompareTo(other.Id);
        }

        #endregion

        /// <summary>
        /// возвращает строковое значение обследуемого специалиста
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0}", User);
        }

        /// <summary>
        /// определяет, является ли obj текущим обучаемым
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            var newObj = (Probationer) obj;
            if (newObj == null)
                return false;
            if (Id == newObj.Id)
                return true;

            return false;
        }

        /// <summary>
        /// получение хэш кода объекта
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using Castle.ActiveRecord;

namespace Eureca.Professional.ActiveRecords
{
    /// <summary>
    /// Represents profile active record.
    /// </summary>
    [ActiveRecord("Profiles")]
    public class Profile : ActiveRecordBase<Profile>, IComparable
    {
        /// <summary>
        /// Default constructor to be used internally.
        /// </summary>
        internal Profile()
        {
        }

        /// <summary>
        /// Initializes new instance of profile active record.
        /// </summary>
        /// <param name="title">Title of profile.</param>
        /// <param name="description">Profile description.</param>
        /// <exception cref="ArgumentNullException">Title is null.</exception>
        public Profile(string title, string description)
        {
            if (title == null)
            {
                throw new ArgumentNullException("title");
            }

            Title = title;
            Description = description;
            Tests = new List<Test>();
        }

        /// <summary>
        /// Initializes new instance of profile active record without description.
        /// </summary>
        /// <param name="title">Title of profile.</param>
        /// <exception cref="ArgumentNullException">Title is null.</exception>
        public Profile(string title) : this(title, null)
        {
        }

        /// <summary>
        /// Gets primary id of profile.
        /// </summary>
        [PrimaryKey("ProfileId")]
        public int Id { get; internal set; }

        /// <summary>
        /// Gets or sets title of profile.
        /// </summary>
        [Property(Unique = true, NotNull = true)]
        public string Title { get; set; }

        /// <summary>
        /// Get or sets profile description.
        /// </summary>
        [Property]
        public string Description { get; set; }

        /// <summary>
        /// Gets (lazily) list of tests added to profile.
        /// </summary>
        [HasAndBelongsToMany(Table = "ProfileTests", ColumnKey = "ProfileId", ColumnRef = "TestId",
            Cascade = ManyRelationCascadeEnum.None,
            Lazy = true)]
        public IList<Test> Tests { get; internal set; }

        #region IComparable Members

        /// <summary>
        /// сравнение объектов
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int CompareTo(object obj)
        {
            var other = (Profile) obj;
            return Id.CompareTo(other.Id);
        }

        #endregion

        /// <summary>
        /// строковое представление объекта
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0}", Title);
        }

        /// <summary>
        /// перегружаемый метод
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            var newObj = (Profile) obj;
            if (newObj == null)
                return false;
            if (Id == newObj.Id)
                return true;

            return false;
        }

        /// <summary>
        /// перегружаемый метод
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
﻿using System;
using Castle.ActiveRecord;
using NHibernate.Criterion;

namespace Eureca.Professional.ActiveRecords
{

    #region TestQuestionKey

    /// <summary>
    /// Represents composite key of test question active record.
    /// </summary>
    [Serializable]
    internal class TestQuestionKey
    {
        /// <summary>
        /// Gets or sets primary id of test.
        /// </summary>
        [KeyProperty]
        public int TestId { get; set; }

        /// <summary>
        /// Gets or sets primary id of question.
        /// </summary>
        [KeyProperty]
        public int QuestionId { get; set; }

        /// <summary>
        /// Serves as a hash function for test question key.
        /// </summary>
        /// <returns>A hash code for the current test question key.</returns>
        public override int GetHashCode()
        {
            return TestId ^ QuestionId;
        }

        /// <summary>
        /// Determines whether the specified test question key is equal to the current test question key.
        /// </summary>
        /// <param name="obj">The System.Object to compare with the current test question key.</param>
        /// <returns>true if the specified System.Object is equal to the current test question key; otherwise, false.</returns>
        public override bool Equals(object obj)
        {
            if (this == obj)
            {
                return true;
            }

            var key = obj as TestQuestionKey;

            if (key == null)
            {
                return false;
            }

            return TestId == key.TestId && QuestionId == key.QuestionId;
        }
    }

    #endregion

    /// <summary>
    /// Represents test question active record.
    /// </summary>
    [ActiveRecord("TestQuestions")]
    public class TestQuestion : ActiveRecordBase<TestQuestion>
    {
        /// <summary>
        /// Default constructor to be used internally.
        /// </summary>
        internal TestQuestion()
        {
        }

        /// <summary>
        /// Initializes new instance of test question active record.
        /// </summary>
        /// <param name="test">Saved to database test active record.</param>
        /// <param name="question">Saved to database question active record.</param>
        /// <param name="order">Order of question.</param>
        /// <exception cref="ArgumentNullException">Test is null or question is null.</exception>
        /// <exception cref="ArgumentException">Test is unsaved or question is unsaved.</exception>
        public TestQuestion(Test test, Question question, int order)
        {
            if (test == null)
            {
                throw new ArgumentNullException("test");
            }

            if (question == null)
            {
                throw new ArgumentNullException("question");
            }

            if (test.Id == 0)
            {
                throw new ArgumentException("Test should be an active record that is saved to database.", "test");
            }

            if (question.Id == 0)
            {
                throw new ArgumentException("Question should be an active record that is saved to database.", "question");
            }

            Key = new TestQuestionKey {TestId = test.Id, QuestionId = question.Id};
            Test = test;
            Question = question;
            QuestionOrder = order;
        }

        /// <summary>
        /// Gets or sets (internally only) composite key of test question consisting of test id and question id.
        /// </summary>
        [CompositeKey]
        internal TestQuestionKey Key { get; set; }

        /// <summary>
        /// Gets test active record.
        /// </summary>
        [BelongsTo("TestId", Insert = false, Update = false)]
        public Test Test { get; internal set; }

        /// <summary>
        /// Gets question active record.
        /// </summary>
        [BelongsTo("QuestionId", Insert = false, Update = false)]
        public Question Question { get; internal set; }

        /// <summary>
        /// Gets or sets order of question.
        /// </summary>
        [Property]
        public int QuestionOrder { get; set; }

        /// <summary>
        /// строковое представление вопроса
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            byte[] rc = null;
            string pc = null;
            using (new SessionScope(FlushAction.Never))
            {
                var q = TestQuestion.FindFirst(Expression.And(Expression.Eq("Test",Test),Expression.Eq("Question",Question)));
                rc = q.Question.Content.RichContent;
                pc = q.Question.Content.PlainContent;
            }

            if (rc != null)
            {
                return string.Format( "{0} {1}", QuestionOrder, "Форматированный текст" );
            }
            if (pc!= null)
            {
                string res;
                string _text = pc;
                int len = _text.IndexOf( "\n" );
                if (len == -1)
                    len = _text.Length;
                if (len > 80)
                {
                    len = _text.LastIndexOf( " ", 80 );
                }
                if (len > 80)
                {
                    len = 80;
                }

                if (len < _text.Length && len != -1)
                {
                    res = _text.Substring( 0, len ) + " ...";
                }
                else
                {
                    res = _text;
                }
                return string.Format( "{0} {1}", QuestionOrder, res.Replace( "\r", "" ) );
            }
            return "Вопрос № " + QuestionOrder;
        }

        /// <summary>
        /// определяет является ли объект данным
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            var questionObj = (TestQuestion) obj;
            return Question.Id == questionObj.Question.Id;
        }

        /// <summary>
        /// получение хэш кода объекта
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
﻿using System;
using Castle.ActiveRecord;
using NHibernate.Criterion;

namespace Eureca.Professional.ActiveRecords
{
  
    /// <summary>
    /// Represents ArmApplication active record.
    /// Not for create. Only for find record from Database
    /// </summary>
    [ActiveRecord("SuperviseAdresses")]
    public class SuperviseAdresses : ActiveRecordBase<SuperviseAdresses>, IComparable
    {
        /// <summary>
        /// Default constructor to be used internally.
        /// </summary>
        public SuperviseAdresses()
        {
        }

        /// <summary>
        /// Gets primary id of ArmApplication.
        /// </summary>
        [PrimaryKey("AdressId")]
        public int Id { get; internal set; }

        /// <summary>
        /// Gets or sets ArmApplication name.
        /// </summary>
        [Property(Unique = true, NotNull = true)]
        public string Adress { get; set; }

        /// <summary>
        /// Gets or sets ArmApplication name.
        /// </summary>
        [Property(NotNull = true)]
        public bool IsSupervise { get; set; }



        #region IComparable Members

        /// <summary>
        /// сравнение АРМ приложений
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int CompareTo(object obj)
        {
            var other = (SuperviseAdresses)obj;
            return Adress.CompareTo(other.Adress);
        }

        #endregion

        /// <summary>
        /// получение объекта АРМ по перечислению
        /// </summary>
        /// <returns></returns>
        public static SuperviseAdresses GetSuperviseAdresses(string ipAdress)
        {
            using (new SessionScope())
            {
                return FindOne( Expression.Eq( "Adress", ipAdress ) );
                
            }
        }

        /// <summary>
        /// строковое представление АРМ
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return Adress;
        }

        /// <summary>
        /// сравнение объктов
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            var ArmApplicationObj = obj as SuperviseAdresses;
            if (ArmApplicationObj == null)
                return false;
            return Adress == ArmApplicationObj.Adress;
        }

        /// <summary>
        /// перегружаемый метод
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
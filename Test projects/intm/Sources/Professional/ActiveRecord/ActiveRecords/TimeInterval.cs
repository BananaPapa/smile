﻿using System;
using Castle.ActiveRecord;

namespace Eureca.Professional.ActiveRecords
{
    /// <summary>
    /// Represents time interval active record.
    /// </summary>
    [ActiveRecord("TimeIntervals")]
    public class TimeInterval : ActiveRecordBase<TimeInterval>
    {
        /// <summary>
        /// Default constructor to be used internally.
        /// </summary>
        internal TimeInterval()
        {
        }

        /// <summary>
        /// Initializes new instance of time interval active record.
        /// </summary>
        /// <param name="startTime">Start time of interval.</param>
        /// <param name="finishTime">Finish time of interval.</param>
        /// <exception cref="ArgumentException">Start time is later than finish time.</exception>
        public TimeInterval(DateTime startTime, DateTime finishTime)
        {
            if (startTime > finishTime)
            {
                throw new ArgumentException(string.Format("время начала {0} время окончания {1}", startTime, finishTime));
            }

            StartTime = startTime;
            FinishTime = finishTime;
        }

        /// <summary>
        /// Gets primary id of time interval.
        /// </summary>
        [PrimaryKey("TimeIntervalId")]
        public long Id { get; internal set; }

        /// <summary>
        /// Gets start time of interval.
        /// </summary>
        [Property(NotNull = true)]
        public DateTime StartTime { get; internal set; }

        /// <summary>
        /// Gets finish time of interval.
        /// </summary>
        [Property(NotNull = true)]
        public DateTime FinishTime { get; internal set; }
    }
}
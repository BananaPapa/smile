﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.ActiveRecord;

namespace Eureca.Professional.ActiveRecords
{
    [ActiveRecord("vStatistic" )]
    public class vStatistic : ActiveRecordBase<vStatistic>
    {
        public vStatistic()
        {
        }

        [PrimaryKey("StatisticId")]
        public int Id { get; set; }

        [Property]
        public int TestPassingId { get; internal set; }

        [Property("eTime")]
        public DateTime Start { get; internal set; }

        [Property("sTime")]
        public DateTime End { get; internal set; }

        [Property("IsSupervise")]
        public bool IsSupervised { get; internal set; }

        [Property("MaxEff")]
        public int MaxEff { get; internal set; }


        [Property("TimeDuration")]
        public int TimeDuration { get; internal set; }

        [Property("Result")]
        public int Result { get; internal set; }

        [Property("Perc")]
        public int Perc { get; internal set; }

        [Property("Valuation")]
        public int Valution { get; internal set; }

        [Property("PassingScore")]
        public int PassingScore { get; internal set; }

        [Property("Title")]
        public string Title { get; internal set; }
    }
}

﻿using System;
using Castle.ActiveRecord;

namespace Eureca.Professional.ActiveRecords
{

    #region QuestionAnswerKey

    /// <summary>
    /// Represents composite key of question answer active record.
    /// </summary>
    [Serializable]
    internal class QuestionAnswerKey
    {
        /// <summary>
        /// Gets or sets primary id of question.
        /// </summary>
        [KeyProperty]
        public int QuestionId { get; set; }

        /// <summary>
        /// Gets or sets primary id of answer.
        /// </summary>
        [KeyProperty]
        public int AnswerId { get; set; }

        /// <summary>
        /// Serves as a hash function for question answer key.
        /// </summary>
        /// <returns>A hash code for the current question answer key.</returns>
        public override int GetHashCode()
        {
            return QuestionId ^ AnswerId;
        }

        /// <summary>
        /// Determines whether the specified question answer key is equal to the current question answer key.
        /// </summary>
        /// <param name="obj">The System.Object to compare with the current question answer key.</param>
        /// <returns>true if the specified System.Object is equal to the current question answer key; otherwise, false.</returns>
        public override bool Equals(object obj)
        {
            if (this == obj)
            {
                return true;
            }

            var key = obj as QuestionAnswerKey;

            if (key == null)
            {
                return false;
            }

            return QuestionId == key.QuestionId && AnswerId == key.AnswerId;
        }
    }

    #endregion

    /// <summary>
    /// Represents question answer active record.
    /// </summary>
    [ActiveRecord("QuestionAnswers")]
    public class QuestionAnswer : ActiveRecordBase<QuestionAnswer>
    {
        /// <summary>
        /// Default constructor to be used internally.
        /// </summary>
        internal QuestionAnswer()
        {
        }

        /// <summary>
        /// Initializes new instance of question answer active record.
        /// </summary>
        /// <param name="question">Saved to database question active record.</param>
        /// <param name="answer">Saved to database answer active record.</param>
        /// <param name="order">Order of answer.</param>
        /// <exception cref="ArgumentNullException">Question is null or answer is null.</exception>
        /// <exception cref="ArgumentException">Question is unsaved or answer is unsaved.</exception>
        public QuestionAnswer(Question question, Answer answer, int order)
        {
            if (question == null)
            {
                throw new ArgumentNullException("question");
            }

            if (answer == null)
            {
                throw new ArgumentNullException("answer");
            }

            if (question.Id == 0)
            {
                throw new ArgumentException("Question should be an active record that is saved to database.", "question");
            }

            if (answer.Id == 0)
            {
                throw new ArgumentException("Answer should be an active record that is saved to database.", "answer");
            }

            Key = new QuestionAnswerKey {QuestionId = question.Id, AnswerId = answer.Id};
            Question = question;
            Answer = answer;
            AnswerOrder = order;
            //Questions = new List<Question>();
        }

        /// <summary>
        /// Gets or sets (internally only) composite key of question answer consisting of question id and answer id.
        /// </summary>
        [CompositeKey]
        internal QuestionAnswerKey Key { get; set; }

        /// <summary>
        /// Gets question active record.
        /// </summary>
        [BelongsTo("QuestionId", Insert = false, Update = false)]
        public Question Question { get; internal set; }

        /// <summary>
        /// Gets answer active record.
        /// </summary>
        [BelongsTo("AnswerId", Insert = false, Update = false)]
        public Answer Answer { get; internal set; }

        /// <summary>
        /// Gets or sets order of answer.
        /// </summary>
        [Property]
        public int AnswerOrder { get; set; }


        //[HasAndBelongsToMany(Table = "QuestionAnswers", ColumnKey = "QuestionId", ColumnRef = "QuestionId",
        //    Cascade = ManyRelationCascadeEnum.SaveUpdate,
        //    Lazy = true)]
        //public IList<Question> Questions { get; internal set; }
    }
}
﻿using System;
using System.Collections.Generic;
using Castle.ActiveRecord;

namespace Eureca.Professional.ActiveRecords
{
    /// <summary>
    /// Represents profile passing active record.
    /// </summary>
    [ActiveRecord("ProfilePassings")]
    public class ProfilePassing : ActiveRecordBase<ProfilePassing>
    {
        /// <summary>
        /// Default constructor to be used internally.
        /// </summary>
        internal ProfilePassing()
        {
        }

        /// <summary>
        /// Initializes new instance of profile passing active record.
        /// </summary>
        /// <param name="probationer">Probationer that has passed the specified profile.</param>
        /// <param name="timeInterval">Passing time interval.</param>
        /// <exception cref="ArgumentNullException">Profile or probationer or time interval is null.</exception>
        public ProfilePassing(Probationer probationer, TimeInterval timeInterval)
        {
            if (probationer == null)
            {
                throw new ArgumentNullException("probationer");
            }

            if (timeInterval == null)
            {
                throw new ArgumentNullException("timeInterval");
            }


            Probationer = probationer;
            Profile = probationer.Profile;
            TimeInterval = timeInterval;
            TestPassings = new List<TestPassing>();
        }

        /// <summary>
        /// Gets primary id of profile passing.
        /// </summary>
        [PrimaryKey("ProfilePassingId")]
        public int Id { get; internal set; }


        /// <summary>
        /// Gets probation that has passed the profile.
        /// </summary>
        [BelongsTo("UserId", NotNull = true, Cascade = CascadeEnum.SaveUpdate)]
        public Probationer Probationer { get; internal set; }


        /// <summary>
        /// Gets probation that has passed the profile.
        /// </summary>
        [BelongsTo("ProfileId", NotNull = true, Cascade = CascadeEnum.SaveUpdate)]
        public Profile Profile { get; internal set; }


        /// <summary>
        /// Gets passing time interval.
        /// </summary>
        [BelongsTo("TimeIntervalId", NotNull = true, Cascade = CascadeEnum.All)]
        public TimeInterval TimeInterval { get; set; }

        /// <summary>
        /// Gets list of test passings during the current profile passing.
        /// </summary>
        [HasMany(ColumnKey = "ProfilePassingId",
            Cascade = ManyRelationCascadeEnum.All,
            Lazy = true)]
        public IList<TestPassing> TestPassings { get; internal set; }
    }
}
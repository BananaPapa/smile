﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.ActiveRecord;
using Eureca.Professional.ActiveRecords;
using NHibernate.Criterion;

namespace Eureca.Professional.ActiveRecords
{
    public interface IJournalBranch
    {
        int Id { get; }

        string Name { get; set; }

        Journal Journal { get; }

        RtfForm BlancForm { get; set; }

        JournalBranch Parent { get; set; }

        int Order { get; set; }
    }

    [ActiveRecord("JournalsBranchs")]
    public class JournalBranch : ActiveRecordBase<JournalBranch>, IJournalBranch
    {
        [PrimaryKey("JournalsBranchId")]
        public int Id { get; internal set; }

        [Property]
        public string Name { get; set; }

        [BelongsTo("JournalId", NotNull = true, Cascade = CascadeEnum.None)]
        public Journal Journal { get; internal set; }


        [BelongsTo("FormId", NotNull = false, Cascade = CascadeEnum.Delete)]
        public RtfForm BlancForm { get; set; }

        [Property("[Order]")]
        public int Order { get; set; }

        [BelongsTo("ParentId", NotNull = false, Cascade = CascadeEnum.None)]
        public JournalBranch Parent { get; set; }

        //public List<JournalBranch> Children { get; set; }

        public JournalBranch()
        {
        }

        public JournalBranch(string name, Journal journal, JournalBranch parent = null, RtfForm form = null)
        {
            if (parent != null && parent.BlancForm != null)
            {
                throw new InvalidOperationException("Can`t add blanc under sheet with blanc");
            }
            Name = name;
            Journal = journal;
            if(parent!=null)
                Parent = parent;
            if (form != null)
                BlancForm = form;
        }

        public override bool Equals( object obj )
        {
            var journalBranch = (JournalBranch)obj;
            return Id == journalBranch.Id;
        }


        /// <summary>
        /// перегруженный метод
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode( )
        {
            return base.GetHashCode( ) ;
        }

        public void DeleteBranchWithReference(  )
        {
            JournalBranch branch = Id>0 ? JournalBranch.Find(this.Id) : this;
            Journal journal = Journal.Find(Journal.Id);
            Stack<JournalBranch> hierarchyStack = new Stack<JournalBranch>( );
            hierarchyStack.Push( branch);
            while (true)
            {
                if (hierarchyStack.Count == 0)
                    break;
                var curentNode = hierarchyStack.Peek( );
                var childs = JournalBranch.FindAll( Expression.Eq( "Parent", curentNode ) );
                if (childs.Length > 0)
                {
                    foreach (var journalBranch in childs)
                    {
                        hierarchyStack.Push( journalBranch );
                    }
                }
                else
                {
                    JournalBranch journalBranch = hierarchyStack.Pop( );
                    journal.JournalBranches.Remove(journalBranch);
                    journal.UpdateAndFlush();
                    journalBranch.DeleteAndFlush( );
                }
            }
        }
    }
}

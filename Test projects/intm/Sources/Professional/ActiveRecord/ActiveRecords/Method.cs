using System;
using System.Collections.Generic;
using Castle.ActiveRecord;
using Eureca.Professional.ActiveRecords;

namespace Eureca.Professional.ActiveRecords
{
    /// <summary>
    /// Represents method active record.
    /// </summary>
    [ActiveRecord( "Methods" )]
    public class Method : ActiveRecordBase<Method>, IComparable
    {
        /// <summary>
        /// Default constructor to be used internally.
        /// </summary>
        internal Method( )
        {
        }

        /// <summary>
        /// Initializes new instance of method active record.
        /// </summary>
        /// <param name="title">Title of method.</param>
        /// <param name="instruction">Method instruction (can be null).</param>
        /// <exception cref="ArgumentNullException">Title is null or instruction is null.</exception>
        public Method( string title, Content instruction )
        {
            if (title == null)
            {
                throw new ArgumentNullException( "title" );
            }

            if (instruction == null)
            {
                throw new ArgumentNullException( "instruction" );
            }

            Title = title;
            Instruction = instruction;
            Tests = new List<Test>( );
        }

        /// <summary>
        /// Gets primary id of method.
        /// </summary>
        [PrimaryKey( "MethodId" )]
        public int Id { get; internal set; }

        /// <summary>
        /// Gets or sets title of method.
        /// </summary>
        [Property( Unique = true, NotNull = true )]
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets method instruction.
        /// </summary>
        [BelongsTo( "InstructionContentId", Cascade = CascadeEnum.All )]
        public Content Instruction { get; set; }

       
        ///<summary>
        ///Gets (lazily) list of tests of this method.
        ///</summary>
        [HasMany( ColumnKey = "MethodId",
            Cascade = ManyRelationCascadeEnum.AllDeleteOrphan,
            Lazy = true , Inverse = true)]
        public IList<Test> Tests { get; internal set; }


        #region IComparable Members

        /// <summary>
        /// ��������� ��������
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int CompareTo( object obj )
        {
            var other = (Method)obj;
            return Id.CompareTo( other.Id );
        }

        #endregion

        /// <summary>
        /// ����������, �������� �� ������������ ������ ���������
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals( object obj )
        {
            if (obj is Method)
            {
                var newObj = (Method)obj;
                return Id == newObj.Id;
            }
            return false;
        }

        /// <summary>
        /// ������������� �����
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode( )
        {
            return base.GetHashCode( );
        }

        /// <summary>
        /// ������������� �����
        /// </summary>
        /// <returns></returns>
        public override string ToString( )
        {
            return Title;
        }
    }
}
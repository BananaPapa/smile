﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Castle.ActiveRecord;
using Eureca.Utility.Extensions;
using NHibernate.Criterion;

namespace Eureca.Professional.ActiveRecords
{
    [ActiveRecord( "Journals" )]
    public class Journal : ActiveRecordValidationBase<Journal> 
    {
        [PrimaryKey( "JournalId" )]
        public int Id { get; internal set; }

        [Property("JournalName")]
        public string Name { get; set; }

        [HasMany(ColumnKey = "JournalId", Cascade = ManyRelationCascadeEnum.Delete, Lazy = true,Inverse = true)]
        public IList<JournalBranch> JournalBranches { get; set; }


        [HasMany( ColumnKey = "JournalId", Table = "JournalsViews", Cascade = ManyRelationCascadeEnum.Delete, Lazy = true,Inverse = true)]
        public IList<JournalView> JournalViews { get; set; }


        public void AddBranch( string branchName, JournalBranch root , RtfForm form = null )
        {
            JournalBranches.Add( new JournalBranch( "раздел2", this, root, form ) );
        }

        public Journal( )
        {
            JournalBranches = new List<JournalBranch>();
            JournalViews = new List<JournalView>();
        }

        public Journal( string name, IList<JournalBranch> journalHierarchy =null, IEnumerable<JournalView> view = null )
        {
            //this.JournalBranches = journalHierarchy;
            this.Name = name;
            this.JournalBranches = journalHierarchy == null ? new List<JournalBranch>() : journalHierarchy.ToList();
            this.JournalViews = view == null ? new List<JournalView>( ) : view.ToList( );
        }

        /// <summary>
        /// сравнение ответа
        /// </summary>
        /// <param name="obj">другой объект ответа</param>
        /// <returns></returns>
        public override bool Equals( object obj )
        {
            var answerObj = (Journal)obj;
            return Id == answerObj.Id;
        }

        /// <summary>
        /// получение хэш кода объекта
        /// </summary>
        /// <returns></returns>

        public  void DeleteJournalWithReferences()
        {
            var journal = Journal.Find(this.Id);
            IEnumerable<Test> tests = null;
            var questionsEnumerable =
                QuestionJournals.FindAll( Expression.Eq( "Journal", journal ) )
                    .Select( journals => journals.Question );
            if (questionsEnumerable.Any( ))
            {
                var questions = questionsEnumerable.ToArray();
                var testQuestions = TestQuestion.FindAll( Expression.In( "Question", questions ) );
                if(testQuestions.Any())
                {
                    tests = testQuestions.Select( question => question.Test ).Distinct( );
                    MessageBox.Show( "Данный журнал используется в следующих тестах:\n{0}".FormatString( string.Join( "\n", tests.Select( test => test.Title ) ) ),"Ошибка удаления",MessageBoxButtons.OK,MessageBoxIcon.Exclamation );
                    return;
                }
            }
            using (new SessionScope())
            {
                journal = Find(journal.Id);
                var root = JournalBranch.FindOne(Expression.IsNull("Parent"), Expression.Eq("Journal", this));
                root.DeleteBranchWithReference();
                journal.JournalViews.Clear();
                journal.UpdateAndFlush();
                journal.DeleteAndFlush();
            }
        }


        public override int GetHashCode( )
        {
            return base.GetHashCode( );
        }


    }
}

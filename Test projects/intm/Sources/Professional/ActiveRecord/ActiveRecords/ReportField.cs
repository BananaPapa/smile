﻿using System;
using Castle.ActiveRecord;

namespace Eureca.Professional.ActiveRecords
{
    /// <summary>
    /// Represents ReportField active record.
    /// класс замена полей отчетов
    /// </summary>
    [ActiveRecord("ReportFields")]
    public class ReportField : ActiveRecordBase<ReportField>
    {
        /// <summary>
        /// пустой конструктор(необходим по умолчанию)
        /// </summary>
        public ReportField()
        {
        }

        /// <summary>
        /// конструктор с параметрами
        /// </summary>
        /// <param name="report">объъект отчета</param>
        /// <param name="fieldName">наименование поля</param>
        /// <param name="defaultValue">значение по умолчанию</param>
        /// <param name="userValue">пользовательское значение</param>
        public ReportField(Report report, string fieldName, string defaultValue, string userValue)
        {
            if (report == null)
            {
                throw new ArgumentNullException("report");
            }

            if (fieldName == null)
            {
                throw new ArgumentNullException("fieldName");
            }
            if (defaultValue == null)
            {
                throw new ArgumentNullException("defaultValue");
            }
            Report = report;
            FieldName = fieldName;
            DefaultValue = defaultValue;
            UserValue = userValue;
        }


        /// <summary>
        /// Gets primary id of ReportField.
        /// </summary>
        [PrimaryKey("ReportFieldId")]
        public int Id { get; internal set; }


        /// <summary>
        /// Gets or sets user.
        /// </summary>
        [BelongsTo("ReportId", NotNull = true, Cascade = CascadeEnum.SaveUpdate)]
        public Report Report { get; internal set; }


        /// <summary>
        /// Gets or sets Report name.
        /// </summary>
        [Property(NotNull = true)]
        public string FieldName { get; set; }

        /// <summary>
        /// Gets or sets Report name.
        /// </summary>
        [Property(NotNull = true)]
        public string DefaultValue { get; set; }

        /// <summary>
        /// Gets or sets Report name.
        /// </summary>
        [Property]
        public string UserValue { get; set; }
    }
}
﻿using System;
using Castle.ActiveRecord;

namespace Eureca.Professional.ActiveRecords
{
    /// <summary>
    /// Represents EventType active record.
    /// This Activer Record not Add Data in DataBase because id must be fixed in DB
    /// </summary>
    [ActiveRecord("EventTypes")]
    public class EventType : ActiveRecordBase<EventType>, IComparable
    {
        /// <summary>
        /// Default constructor to be used internally.
        /// </summary>
        private EventType()
        {
        }

        /// <summary>
        /// конструктор с параметрами
        /// не используется для создания типов событий
        /// создаются при инициализации БД
        /// </summary>
        /// <param name="id">идентификатор типа события(задан в перечислении)</param>
        /// <param name="title">наименование</param>
        private EventType(int id, string title)
        {
            if (string.IsNullOrEmpty(title))
            {
                throw new ArgumentNullException("title");
            }

            Title = title;
            Id = id;
        }

        #region fields

        /// <summary>
        /// Gets primary id of EventType.
        /// </summary>
        [PrimaryKey(PrimaryKeyType.Native, "EventTypeId")]
        public int Id { get; internal set; }


        /// <summary>
        /// Gets or sets EventType name.
        /// </summary>
        [Property(Unique = true, NotNull = true)]
        public string Title { get; set; }

        #endregion

        /// <summary>
        /// переводит целое значение из БД в перечисление
        /// </summary>
        public EventTypes EnumEventType
        {
            get { return (EventTypes) Id; }
        }

        #region IComparable Members

        /// <summary>
        /// сравнение типов событий
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int CompareTo(object obj)
        {
            var other = (EventType) obj;
            return Id.CompareTo(other.Id);
        }

        #endregion

        /// <summary>
        /// получение типа события по перечислению
        /// </summary>
        /// <param name="eventType"></param>
        /// <returns></returns>
        public static EventType GetEventType(EventTypes eventType)
        {
            return Find((int) eventType);
        }

        /// <summary>
        /// создание типа события по перечислениь
        /// </summary>
        /// <param name="eventType"></param>
        /// <param name="title"></param>
        /// <returns></returns>
        public static EventType CreateEventType(EventTypes eventType, string title)
        {
            var et = new EventType((int) eventType, title);
            et.CreateAndFlush();
            return et;
        }

        /// <summary>
        /// строковое представление типа
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return String.Format("{0}", Title);
        }

        /// <summary>
        /// сдавнение объектов
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (obj is EventType)
            {
                var EventTypeObj = (EventType) obj;
                return Id == EventTypeObj.Id;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// получение хэш кода объекта
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    /// <summary>
    /// типы событий
    /// </summary>
    public enum EventTypes
    {
        /// <summary>
        /// начало редактирования теста
        /// </summary>
        BeginEdit = 1,
        /// <summary>
        /// окнец редактирования теста
        /// </summary>
        EndEdit = 2,
        /// <summary>
        /// вход пользователя
        /// </summary>
        EnterUser = 3,
        /// <summary>
        /// выход пользоватедя
        /// </summary>
        ExitUser = 4,
        /// <summary>
        /// начало тестирования
        /// </summary>
        BeginTesting = 5,
        /// <summary>
        /// конец тестирования
        /// </summary>
        EndTesting = 6,
        /// <summary>
        /// прерывание тестирования
        /// </summary>
        AbortTesting = 7,
        
    }
}
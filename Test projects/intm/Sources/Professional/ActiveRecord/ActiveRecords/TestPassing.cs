﻿using System;
using System.Collections.Generic;
using System.Linq;
using Castle.ActiveRecord;

namespace Eureca.Professional.ActiveRecords
{
    /// <summary>
    /// Represents test passing active record.
    /// </summary>
    [ActiveRecord("TestPassings")]
    public class TestPassing : ActiveRecordBase<TestPassing>
    {
        /// <summary>
        /// Default constructor to be used internally.
        /// </summary>
        internal TestPassing()
        {
        }

        /// <summary>
        /// Initializes new instance of test passing active record.
        /// </summary>
        /// <param name="test">Test that has been passed.</param>
        /// <param name="probationer">Probationer that has passed the specified test.</param>
        /// <param name="timeInterval">Passing time interval.</param>
        /// <param name="isSupervise">Currently not used</param>
        /// <exception cref="ArgumentNullException">Test or probationer or time interval is null.</exception>
        public TestPassing(Test test, Probationer probationer, TimeInterval timeInterval, bool isSupervise)
        {
            if (test == null)
            {
                throw new ArgumentNullException("test");
            }

            if (probationer == null)
            {
                throw new ArgumentNullException("probationer");
            }

            if (timeInterval == null)
            {
                throw new ArgumentNullException("timeInterval");
            }

            Test = test;
            ProfilePassing = null;
            Probationer = probationer;
            TimeInterval = timeInterval;
            IsSupervise = isSupervise;
        }

        /// <summary>
        /// Initializes new instance of test passing active record during specified profile passing.
        /// </summary>
        /// <param name="test">Test that has been passed.</param>
        /// <param name="profilePassing">Profile passing that is being perfomed </param>
        /// <param name="probationer">Probationer that has passed the specified test.</param>
        /// <param name="timeInterval">Passing time interval.</param>
        /// <exception cref="ArgumentNullException">Test or profile passing or probationer or time interval is null.</exception>
        public TestPassing( Test test, ProfilePassing profilePassing, Probationer probationer, TimeInterval timeInterval, bool isSupervise)
            : this( test, probationer, timeInterval, isSupervise )
        {
            if (profilePassing == null)
            {
                throw new ArgumentNullException("profilePassing");
            }

            ProfilePassing = profilePassing;
        }

        /// <summary>
        /// Gets primary id of test passing.
        /// </summary>
        [PrimaryKey("TestPassingId")]
        public int Id { get; internal set; }

        /// <summary>
        /// Gets ProfilePassing
        /// </summary>
        [BelongsTo("ProfilePassingId", NotNull = false, Cascade = CascadeEnum.SaveUpdate)]
        public ProfilePassing ProfilePassing { get; internal set; }

        /// <summary>
        /// Gets test that has been passed.
        /// </summary>
        [BelongsTo("TestId", NotNull = true, Cascade = CascadeEnum.SaveUpdate)]
        public Test Test { get; internal set; }

        /// <summary>
        /// Gets probation that has passed the test.
        /// </summary>
        [BelongsTo("UserId", NotNull = true, Cascade = CascadeEnum.SaveUpdate)]
        public Probationer Probationer { get; internal set; }

        /// <summary>
        /// Gets or sets passing time interval.
        /// </summary>
        [BelongsTo("TimeIntervalId", NotNull = true, Cascade = CascadeEnum.All)]
        public TimeInterval TimeInterval { get; set; }

        ///// <summary>
        ///// Gets or sets bounded content of question.
        ///// </summary>
        //[HasAndBelongsToMany( Table = "FillingFormsTestResults", ColumnKey = "TestPassingId", ColumnRef = "FormFillingId", Cascade = ManyRelationCascadeEnum.All )]
        //public IList<FillingFormsTestResult> FormFillings { get; set; }

        /// <summary>
        /// Gets IsSupervise test 
        /// </summary>
        [Property]
        public bool IsSupervise { get; internal set; }
    }
}
﻿using System;
using Castle.ActiveRecord;

namespace Eureca.Professional.ActiveRecords
{

    #region MemorizationInfoKey

    /// <summary>
    /// Represents composite key of memorization info.
    /// </summary>
    [Serializable]
    internal class MemorizationInfoKey
    {
        /// <summary>
        /// Gets or sets primary id of test.
        /// </summary>
        [KeyProperty]
        public int TestId { get; set; }

        /// <summary>
        /// Gets or sets primary id of content.
        /// </summary>
        [KeyProperty]
        public long ContentId { get; set; }

        /// <summary>
        /// Serves as a hash function for memorization info key.
        /// </summary>
        /// <returns>A hash code for the current memorization info key.</returns>
        public override int GetHashCode()
        {
            //return
            return TestId ^ (int) ContentId;
        }

        /// <summary>
        /// Determines whether the specified memorization info key is equal to the current memorization infot key.
        /// </summary>
        /// <param name="obj">The System.Object to compare with the current memorization info key.</param>
        /// <returns>true if the specified System.Object is equal to the current memorization info key; otherwise, false.</returns>
        public override bool Equals(object obj)
        {
            if (this == obj)
            {
                return true;
            }

            var key = obj as MemorizationInfoKey;

            if (key == null)
            {
                return false;
            }

            return TestId == key.TestId && ContentId == key.ContentId;
        }
    }

    #endregion

    /// <summary>
    /// Represents memorization info active record.
    /// </summary>
    [ActiveRecord]
    public class MemorizationInfo : ActiveRecordBase<MemorizationInfo>
    {
        /// <summary>
        /// Default constructor to be used internally.
        /// </summary>
        internal MemorizationInfo()
        {
        }

        /// <summary>
        /// Initializes new instance of memorization info active record.
        /// </summary>
        /// <param name="test">Saved to database test active record.</param>
        /// <param name="content">Saved to database content active record.</param>
        /// <param name="timeLimit">Shot time limit.</param>
        /// <param name="showOrder">Show order.</param>
        /// <exception cref="ArgumentNullException">Test or content is null.</exception>
        /// <exception cref="ArgumentException">Test or content is unsaved.</exception>
        public MemorizationInfo(Test test, Content content, int timeLimit, int showOrder)
        {
            if (test == null)
            {
                throw new ArgumentNullException("test");
            }

            if (content == null)
            {
                throw new ArgumentNullException("content");
            }

            if (test.Id == 0)
            {
                throw new ArgumentException("Test should be an active record that is saved to database.", "test");
            }

            if (content.Id == 0)
            {
                throw new ArgumentException("Form should be an active record that is saved to database.", "content");
            }

            Key = new MemorizationInfoKey {TestId = test.Id, ContentId = content.Id};
            Test = test;
            Content = content;
            TimeLimit = timeLimit;
            ShowOrder = showOrder;
        }

        /// <summary>
        /// Gets or sets (internally only) composite key of memorization info.
        /// </summary>
        [CompositeKey]
        internal MemorizationInfoKey Key { get; set; }

        /// <summary>
        /// Gets test of memorization info.
        /// </summary>
        [BelongsTo("TestId", Insert = false, Update = false)]
        public Test Test { get; internal set; }

        /// <summary>
        /// Gets content of memorization info.
        /// </summary>
        [BelongsTo("ContentId", Insert = false, Update = false)]
        public Content Content { get; internal set; }

        /// <summary>
        /// Get show time limit. 
        /// </summary>
        [Property]
        public int TimeLimit { get; set; }

        /// <summary>
        /// Gets show order.
        /// </summary>
        [Property]
        public int ShowOrder { get; set; }
    }
}
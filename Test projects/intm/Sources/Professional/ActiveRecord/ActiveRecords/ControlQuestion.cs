﻿using System;
using Castle.ActiveRecord;

namespace Eureca.Professional.ActiveRecords
{

    #region ControlQuestionKey

    /// <summary>
    /// Represents composite key of control question.
    /// </summary>
    [Serializable]
    internal class ControlQuestionKey
    {
        /// <summary>
        /// Gets or sets primary id of method.
        /// </summary>
        [KeyProperty]
        public int MethodId { get; set; }

        /// <summary>
        /// Gets or sets primary id of question.
        /// </summary>
        [KeyProperty]
        public int QuestionId { get; set; }

        /// <summary>
        /// Serves as a hash function for control question key.
        /// </summary>
        /// <returns>A hash code for the current control question key.</returns>
        public override int GetHashCode()
        {
            return MethodId ^ QuestionId;
        }

        /// <summary>
        /// Determines whether the specified control question key is equal to the current control question key.
        /// </summary>
        /// <param name="obj">The System.Object to compare with the current control question key.</param>
        /// <returns>true if the specified System.Object is equal to the current control question key; otherwise, false.</returns>
        public override bool Equals(object obj)
        {
            if (this == obj)
            {
                return true;
            }

            var key = obj as ControlQuestionKey;

            if (key == null)
            {
                return false;
            }

            return MethodId == key.MethodId && QuestionId == key.QuestionId;
        }
    }

    #endregion

    /// <summary>
    /// Represents control question active record.
    /// </summary>
    [ActiveRecord("ControlQuestions")]
    public class ControlQuestion : ActiveRecordBase<ControlQuestion>
    {
        /// <summary>
        /// Default constructor to be used internally.
        /// </summary>
        internal ControlQuestion()
        {
        }

        /// <summary>
        /// Initializes new instance of control question active record.
        /// </summary>
        /// <param name="method">Saved to database method active record.</param>
        /// <param name="question">Saved to database questino active record.</param>
        /// <param name="rightAnswer">Right answer for control question.</param>
        /// <param name="comment">Form of comment for control question.</param>
        /// <param name="questionOrder">Order of control question.</param>
        /// <exception cref="ArgumentNullException">Method or question or right answer or comment is null.</exception>
        /// <exception cref="ArgumentException">Method or question is unsaved.</exception>
        public ControlQuestion(Method method, Question question, Answer rightAnswer, Content comment, int questionOrder)
        {
            #region Arguments validation

            if (method == null)
            {
                throw new ArgumentNullException("method");
            }

            if (question == null)
            {
                throw new ArgumentNullException("question");
            }

            //if (rightAnswer == null)
            //{
            //    throw new ArgumentNullException("rightAnswer");
            //}

            if (comment == null)
            {
                throw new ArgumentNullException("comment");
            }

            if (method.Id == 0)
            {
                throw new ArgumentException("Method should be an active record that is saved to database.", "method");
            }

            if (question.Id == 0)
            {
                throw new ArgumentException("Question should be an active record that is saved to database.", "question");
            }

            #endregion

            Key = new ControlQuestionKey {MethodId = method.Id, QuestionId = question.Id};
            Method = method;
            Question = question;
            RightAnswer = rightAnswer;
            Comment = comment;
            QuestionOrder = questionOrder;
        }

        /// <summary>
        /// Gets or sets (internally only) composite key of control question.
        /// </summary>
        [CompositeKey]
        internal ControlQuestionKey Key { get; set; }

        /// <summary>
        /// Gets method.
        /// </summary>
        [BelongsTo("MethodId", Insert = false, Update = false)]
        public Method Method { get; internal set; }

        /// <summary>
        /// Gets question.
        /// </summary>
        [BelongsTo("QuestionId", Insert = false, Update = false)]
        public Question Question { get; internal set; }

        /// <summary>
        /// Gets or sets right answer.
        /// </summary>
        [BelongsTo("RightAnswerId", Cascade = CascadeEnum.SaveUpdate)]
        public Answer RightAnswer { get; set; }

        /// <summary>
        /// Gets or sets comment.
        /// </summary>
        [BelongsTo("CommentContentId", Cascade = CascadeEnum.All)]
        public Content Comment { get; set; }

        /// <summary>
        /// Gets or sets question order.
        /// </summary>
        [Property]
        public int QuestionOrder { get; set; }

        /// <summary>
        /// строковое представление объекта
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            if (Question.Content.RichContent != null)
            {
                return string.Format("{0} {1}", QuestionOrder, "Форматированный текст");
            }
            if (Question.Content.PlainContent != null)
            {
                string res;
                string _text = Question.Content.PlainContent;
                int len = _text.IndexOf("\n");
                if (len == -1)
                    len = _text.Length;
                if (len > 80)
                {
                    len = _text.LastIndexOf(" ", 80);
                }
                if (len > 80)
                {
                    len = 80;
                }

                if (len < _text.Length && len != -1)
                {
                    res = _text.Substring(0, len) + " ...";
                }
                else
                {
                    res = _text;
                }
                return string.Format("{0} {1}", QuestionOrder, res.Replace("\r", ""));
            }
            return "";
        }

        /// <summary>
        /// перегруженный метод
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            var questionObj = (ControlQuestion) obj;
            return Question.Id == questionObj.Question.Id;
        }


        /// <summary>
        /// перегруженный метод
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
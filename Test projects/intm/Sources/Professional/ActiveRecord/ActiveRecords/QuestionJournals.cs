﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.ActiveRecord;

namespace Eureca.Professional.ActiveRecords
{
    [ActiveRecord( "QuestionJournals" )]
    public class QuestionJournals : ActiveRecordValidationBase<QuestionJournals>
    {
        [PrimaryKey( "QuestionJournalId" )]
        public int Id { get; internal set; }

        [BelongsTo( Column = "QuestionId", NotNull = true, Cascade = CascadeEnum.None )]
        public Question Question { get; set; }

        [BelongsTo( "JournalId", NotNull = false, Cascade = CascadeEnum.None )]
        public Journal Journal { get; set; }

        [BelongsTo( "JournalViewId", NotNull = false, Cascade = CascadeEnum.None )]
        public JournalView ViewForm { get; set; }

        public QuestionJournals( Question question, JournalView viewForm, Journal journal )
        {
            Question = question;
            ViewForm = viewForm;
            Journal = journal;
        }

        public QuestionJournals ()
	    {

	    }
    }
}

﻿using System;
using Castle.ActiveRecord;
using Castle.ActiveRecord.Queries;

namespace Eureca.Professional.ActiveRecords
{
    /// <summary>
    /// Represents Event active record.
    /// класс событий 
    /// </summary>
    [ActiveRecord("Events")]
    public class Event : ActiveRecordBase<Event>
    {
        /// <summary>
        /// конструктор по умолчанию
        /// </summary>
        internal Event()
        {
            Date = DateTime.Now;
        }

        /// <summary>
        /// конструктор с параметрами
        /// </summary>
        /// <param name="user">пользователь, инициатор события</param>
        /// <param name="eventType">тип события</param>
        /// <param name="objectId">объект, с которым произошло событие( -1 отсутствует объект)</param>
        public Event(User user, EventType eventType, int objectId)
            : this()
        {
            if (user == null)
                throw new ArgumentNullException("user");
            if (eventType == null)
                throw new ArgumentNullException("eventType");
            User = user;
            EventType = eventType;
            ObjectId = objectId;
        }

        /// <summary>
        /// контсруктор с инициализированным параметром(пустой объект)
        /// </summary>
        /// <param name="user">пользователь инициатор события</param>
        /// <param name="eventType">тип события</param>
        public Event(User user, EventType eventType)
            : this(user, eventType, -1)
        {
        }

        /// <summary>
        /// Gets primary id of Event.
        /// </summary>
        [PrimaryKey("EventId")]
        public int Id { get; internal set; }

        /// <summary>
        /// Gets or  Date.
        /// </summary>
        [Property("Date", NotNull = true)]
        public DateTime Date { get; internal set; }

        /// <summary>
        /// идентификатор объекта с которым произошло событие
        /// </summary>
        [Property]
        public int ObjectId { get; internal set; }

        /// <summary>
        /// Gets or sets user.
        /// </summary>
        [BelongsTo("UserId", NotNull = true, Cascade = CascadeEnum.SaveUpdate)]
        public User User { get; internal set; }


        /// <summary>
        /// Gets or sets user.
        /// </summary>
        [BelongsTo("EventTypeId", NotNull = true, Cascade = CascadeEnum.SaveUpdate)]
        public EventType EventType { get; internal set; }

        /// <summary>
        /// свойство исключительно для просмотра событий
        /// возвращает объект по типу события
        /// </summary>
        public Object Object
        {
            get
            {
                try
                {
                    switch (EventType.EnumEventType)
                    {
                            //вход и выход пользователя в арм
                        case EventTypes.EnterUser:
                        case EventTypes.ExitUser:
                            return ActiveRecordBase<ArmApplication>.Find(ObjectId);
                            //работа с тестом
                        case EventTypes.BeginEdit:
                        case EventTypes.EndEdit:
                        case EventTypes.AbortTesting:
                        case EventTypes.BeginTesting:
                        case EventTypes.EndTesting:
                            return ActiveRecordBase<Test>.Find(ObjectId);
                        default:
                            //значение неизвестно
                            return null;
                    }
                }
                catch (Exception)
                {
                    //объект не найден
                    return null;
                }
            }
        }


        /// <summary>
        /// addition new Event in Database
        /// </summary>
        /// <param name="user"></param>
        /// <param name="eventType"></param>
        /// <param name="objectId"></param>
        /// <returns></returns>
        public static int Add(User user, EventTypes eventType, int objectId)
        {
            using (new SessionScope())
            {
                var Event = new Event(user, EventType.GetEventType(eventType), objectId);
                Event.Save();
                return Event.Id;
            }
        }

        /// <summary>
        /// Static method from Event class, retrieves the all Events
        /// in a specified date interval. Uses named parameters.
        /// </summary>
        /// <param name="start">начало поиска</param>
        /// <param name="end">окончание поиска</param>
        /// <returns>возвращает массив событий</returns>
        public static Event[] GetEventsFromInterval(DateTime start, DateTime end)
        {
            var q = new SimpleQuery<Event>(typeof (Event),
                                           @"
            from Event e
            where e.Date between :start and :end
            order by e.Date desc
            ");
            q.SetParameter("start", start);
            q.SetParameter("end", end);

            return q.Execute();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.ActiveRecord;

namespace Eureca.Professional.ActiveRecords
{

    [ActiveRecord("FormFillings")]
    public class FormFilling : ActiveRecordBase<FormFilling>
    {
        [PrimaryKey("FormFillingId")]
        public  int Id { get; internal set; }

        [BelongsTo( Column = "FormId", NotNull = true, Cascade = CascadeEnum.None )]
        public RtfForm Form { get; set; }

        [BelongsTo( Column = "CompletedFormId", NotNull = false, Cascade = CascadeEnum.Delete)]
        public RtfForm FilledForm { get; set; }

        public bool IsFillingPresent 
        {
            get
            {
                return FilledForm != null;
            }
        }


        public FormFilling()
        {
                
        }

        public FormFilling(RtfForm blancForm, RtfForm standartForm = null)
        {
            if( blancForm== null)
                throw  new NullReferenceException("blancForm");

            Form = blancForm;
            FilledForm = standartForm;    
        }

    }
}
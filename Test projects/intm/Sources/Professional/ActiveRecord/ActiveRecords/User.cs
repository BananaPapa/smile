﻿using System;
using Castle.ActiveRecord;

namespace Eureca.Professional.ActiveRecords
{
    /// <summary>
    /// User roles enumeration.
    /// </summary>
    public enum UserRole
    {
        ///// <summary>
        ///// all users
        ///// </summary>
        //All = 0,
        ///<summary>
        /// User is probationer.
        ///</summary>
        Probationer = 1,
        /// <summary>
        /// User is Admin.
        /// </summary>
        Admin = 2,
    }

    /// <summary>
    /// Represents user active record.
    /// </summary>
    [ActiveRecord("Users")]
    public class User : ActiveRecordBase<User>, IComparable
    {
        /// <summary>
        /// Default constructor to be used internally.
        /// </summary>
        internal User()
        {
        }

        /// <summary>
        /// Constructor is initialized class of user
        /// </summary>
        /// <param name="firstName">Name of user</param>
        /// <param name="middleName">Parent name of user</param>
        /// <param name="lastName">surname of user</param>
        /// <param name="birthDate">birthdate of user</param> 
        /// <param name="password">password</param>
        public User(string firstName, string middleName, string lastName, DateTime birthDate,
                     string password)
        {
            if (firstName == null)
            {
                throw new ArgumentNullException("firstName");
            }
            if (middleName == null)
            {
                throw new ArgumentNullException("middleName");
            }
            if (lastName == null)
            {
                throw new ArgumentNullException("lastName");
            }
           if (password == null)
            {
                throw new ArgumentNullException("password");
            }



            FirstName = firstName;
            MiddleName = middleName;
            LastName = lastName;
            BirthDate = birthDate;
            Password = password;
            UniqueId = Guid.NewGuid();
            IsArchive = false;
        }

        /// <summary>
        /// Gets primary id of user.
        /// </summary>
        [PrimaryKey("UserId")]
        public int Id { get; internal set; }

        /// <summary>
        /// Gets UniqueId of user.
        /// </summary>
        [Property(NotNull = true)]
        public Guid UniqueId { get; internal set; }


        /// <summary>
        /// Gets or sets name of user.
        /// </summary>
        [Property(NotNull = true)]
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets surname of user.
        /// </summary>
        [Property]
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets MiddleName of user.
        /// </summary>
        [Property]
        public string MiddleName { get; set; }


        /// <summary>
        /// birthDate
        /// </summary>
        [Property(NotNull = true)]
        public DateTime BirthDate { get; set; }


        /// <summary>
        /// Gets or sets Password of user.
        /// </summary>
        [Property(NotNull = true)]
        public string Password { get; set; }



        /// <summary>
        /// архивный пользователь
        /// </summary>
        [Property(NotNull = true)]
        public bool IsArchive { get; set; }




        /// <summary>
        /// Get  user Probationer.
        /// </summary>
        [OneToOne]
        public Probationer Probationer { get; set; }

        /// <summary>
        /// профиль пользователя
        /// </summary>
        public Profile Profile
        {
            get
            {
                if (Probationer == null)
                    return null;
                else
                    return
                        Probationer.Profile;
            }
        }



        /// <summary>
        /// полное ФИО пользоветеля
        /// </summary>
        public string FullName
        {
            get { return string.Format("{0} {1} {2}", LastName, FirstName, MiddleName); }
        }

        #region IComparable Members

        /// <summary>
        /// сравнение объектов
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int CompareTo(object obj)
        {
            var other = (User) obj;
            return Id.CompareTo(other.Id);
        }

        #endregion

        /// <summary>
        /// перегружаемый метод
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0} {1} {2}", LastName, FirstName, MiddleName);
        }

        /// <summary>
        /// перегружаемый метод
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (obj is User)
            {
                var newObj = (User) obj;
                return Id == newObj.Id;
            }
            else
                return false;
        }

        /// <summary>
        /// перегружаемый метод
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
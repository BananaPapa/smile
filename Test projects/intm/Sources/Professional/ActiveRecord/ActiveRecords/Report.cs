﻿using System;
using System.Collections.Generic;
using Castle.ActiveRecord;

namespace Eureca.Professional.ActiveRecords
{
    /// <summary>
    /// Represents Report active record.
    /// </summary>
    [ActiveRecord("Reports")]
    public class Report : ActiveRecordBase<Report>
    {
        /// <summary>
        /// Default constructor to be used internally.
        /// </summary>
        [Obsolete("Use Report(string name, string pathstring) or Report(string path) instead.", true)]
        internal Report()
        {
        }


        /// <summary>
        /// Report for Test
        /// </summary>
        /// <param name="title">наименование отчета</param>
        /// <param name="description">описание</param>
        /// <param name="reportType">тип отчета</param>
        public Report(string title, string description, int reportType)
        {
            if (string.IsNullOrEmpty(title))
            {
                throw new ArgumentNullException("title");
            }
            Title = title;
            Description = description;
            ReportType = reportType;
            Tests = new List<Test>();
        }

        /// <summary>
        /// Gets primary id of Report.
        /// </summary>
        [PrimaryKey("ReportId")]
        public int Id { get; internal set; }

        /// <summary>
        /// Gets or sets Report name.
        /// </summary>
        [Property(NotNull = true)]
        public string Title { get; set; }


        /// <summary>
        /// Gets or sets ReportType
        /// </summary>
        [Property(NotNull = true)]
        public int ReportType { get; set; }

        /// <summary>
        /// Gets or sets Report name.
        /// </summary>
        [Property]
        public string Description { get; set; }


        /// <summary>
        /// Gets (lazily) list of tests the current Report belongs to.
        /// </summary>
        [HasAndBelongsToMany(Table = "TestReports", ColumnKey = "ReportId", ColumnRef = "TestId",
            Cascade = ManyRelationCascadeEnum.SaveUpdate,
            Lazy = true)]
        public IList<Test> Tests { get; internal set; }

        /// <summary>
        /// текстовое предаставление объекта
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0}", Title);
        }

        /// <summary>
        /// определяет является ли объект текущим отчетом
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            var ReportObj = (Report) obj;
            return Id == ReportObj.Id;
        }

        /// <summary>
        /// получение хэш кода объекта
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
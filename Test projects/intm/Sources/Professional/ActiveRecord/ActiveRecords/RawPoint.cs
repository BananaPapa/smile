﻿using System;
using Castle.ActiveRecord;

namespace Eureca.Professional.ActiveRecords
{

    #region RawPointKey

    /// <summary>
    /// Represents composite key of raw point.
    /// </summary>
    [Serializable]
    internal class RawPointKey
    {
        /// <summary>
        /// Gets or sets primary id of test.
        /// </summary>
        [KeyProperty]
        public int TestId { get; set; }

        /// <summary>
        /// Gets or sets primary id of scale.
        /// </summary>
        [KeyProperty]
        public int ScaleId { get; set; }

        /// <summary>
        /// Gets or sets primary id of question.
        /// </summary>
        [KeyProperty]
        public int QuestionId { get; set; }

        /// <summary>
        /// Gets or sets primary id of answer.
        /// </summary>
        [KeyProperty]
        public int AnswerId { get; set; }

        /// <summary>
        /// Serves as a hash function for raw point key.
        /// </summary>
        /// <returns>A hash code for the current raw point key.</returns>
        public override int GetHashCode()
        {
            return TestId ^ ScaleId ^ QuestionId ^ AnswerId;
        }

        /// <summary>
        /// Determines whether the specified raw point key is equal to the current raw point key.
        /// </summary>
        /// <param name="obj">The System.Object to compare with the current raw point key.</param>
        /// <returns>true if the specified System.Object is equal to the current raw point key; otherwise, false.</returns>
        public override bool Equals(object obj)
        {
            if (this == obj)
            {
                return true;
            }

            var key = obj as RawPointKey;

            if (key == null)
            {
                return false;
            }

            return TestId == key.TestId && ScaleId == key.ScaleId && QuestionId == key.QuestionId &&
                   AnswerId == key.AnswerId;
        }
    }

    #endregion

    /// <summary>
    /// Represents raw point active record.
    /// </summary>
    [ActiveRecord("RawPoints")]
    public class RawPoint : ActiveRecordBase<RawPoint>
    {
        /// <summary>
        /// Default constructor to be used internally.
        /// </summary>
        internal RawPoint()
        {
        }

        /// <summary>
        /// Initializes new instance of raw point active record.
        /// </summary>
        /// <param name="test">Saved to database test active record.</param>
        /// <param name="scale">Saved to database scale active record.</param>
        /// <param name="question">Saved to database question active record.</param>
        /// <param name="answer">Saved to database answer active record.</param>
        /// <param name="value">Value of raw point.</param>
        /// <exception cref="ArgumentNullException">Test or scale or question or answer is null.</exception>
        /// <exception cref="ArgumentException">Test or scale or question or answer is unsaved.</exception>
        public RawPoint(Test test, Scale scale, Question question, Answer answer, float value)
        {
            #region Arguments validation

            if (test == null)
            {
                throw new ArgumentNullException("test");
            }

            if (scale == null)
            {
                throw new ArgumentNullException("scale");
            }

            if (question == null)
            {
                throw new ArgumentNullException("question");
            }

            if (answer == null)
            {
                throw new ArgumentNullException("answer");
            }

            if (test.Id == 0)
            {
                throw new ArgumentException("Test should be an active record that is saved to database.", "test");
            }

            if (scale.Id == 0)
            {
                throw new ArgumentException("Scale should be an active record that is saved to database.", "scale");
            }

            if (question.Id == 0)
            {
                throw new ArgumentException("Question should be an active record that is saved to database.", "question");
            }

            if (answer.Id == 0)
            {
                throw new ArgumentException("Answer should be an active record that is saved to database.", "answer");
            }

            #endregion

            Key = new RawPointKey {TestId = test.Id, ScaleId = scale.Id, QuestionId = question.Id, AnswerId = answer.Id};
            Test = test;
            Scale = scale;
            Question = question;
            Answer = answer;
            Value = value;
        }

        /// <summary>
        /// Gets or sets (internally only) composite key of raw point.
        /// </summary>
        [CompositeKey]
        internal RawPointKey Key { get; set; }

        /// <summary>
        /// Get or sets value of raw point.
        /// </summary>
        [Property]
        public float Value { get; set; }

        /// <summary>
        /// Gets test active record.
        /// </summary>
        [BelongsTo("TestId", Insert = false, Update = false)]
        public Test Test { get; internal set; }

        /// <summary>
        /// Gets scale active record.
        /// </summary>
        [BelongsTo("ScaleId", Insert = false, Update = false)]
        public Scale Scale { get; internal set; }

        /// <summary>
        /// Gets question active record.
        /// </summary>
        [BelongsTo("QuestionId", Insert = false, Update = false)]
        public Question Question { get; internal set; }

        /// <summary>
        /// Gets answer active record.
        /// </summary>
        [BelongsTo("AnswerId", Insert = false, Update = false)]
        public Answer Answer { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.ActiveRecord;

namespace Eureca.Professional.ActiveRecords
{


    /// <summary>
    /// Represents composite key of test result.
    /// </summary>
    //[Serializable]
    //internal class FormFillingsTestResultKey
    //{
    //    /// <summary>
    //    /// Gets or sets primary id of test passing.
    //    /// </summary>
    //    [KeyProperty]
    //    public int TestPassingId { get; set; }

    //    /// <summary>
    //    /// Gets or sets primary id of answer.
    //    /// </summary>
    //    [KeyProperty]
    //    public int QuestionId { get; set; }

    //     ///<summary>
    //     ///Gets or sets primary id of question.
    //     ///</summary>
    //    [KeyProperty]
    //    public int FormFillingId { get; set; }

    //    /// <summary>
    //    /// Serves as a hash function for test result key.
    //    /// </summary>
    //    /// <returns>A hash code for the current test result key.</returns>
    //    public override int GetHashCode()
    //    {
    //        return TestPassingId ^ FormFillingId ^ QuestionId;
            
    //    }

    //    public override bool Equals(object obj)
    //    {
    //        if (this == obj)
    //        {
    //            return true;
    //        }

    //        var key = obj as FormFillingsTestResultKey;

    //        if (key == null)
    //        {
    //            return false;
    //        }
    //        return key.GetHashCode() == this.GetHashCode();
    //    }
    //}

    [ActiveRecord( "FillingFormsTestResults" )]
    public class FillingFormsTestResult : ActiveRecordBase<FillingFormsTestResult>
    {
        internal FillingFormsTestResult()
        {
                
        }
        
        /// <summary>
        /// Gets or sets (internally only) composite key of test result.
        /// </summary>
        [PrimaryKey( "ResultId" )]
        public int Id { get; set; }

        public FillingFormsTestResult( TestPassing testPassings, Question question, FormFilling formFilling = null )
        {
            //Key = new FormFillingsTestResultKey { TestPassingId = testPassings.Id, FormFillingId = formFilling.Id, QuestionId = question.Id };
            TestPassing = testPassings;
            Question = question;
            FormFilling = formFilling;
        }

        /// <summary>
        /// Gets test passing active record.
        /// </summary>
        [BelongsTo( "TestPassingId", NotNull = true, Cascade = CascadeEnum.None )]
        public TestPassing TestPassing { get; set; }

        /// <summary>
        /// Gets question active record.
        /// </summary>
        [BelongsTo( "QuestionId", NotNull = true, Cascade = CascadeEnum.None )]
        public Question Question { get; set; }

        /// <summary>
        /// Get form filling for assigment
        /// </summary>
        [BelongsTo(Column="FormFillingId", Cascade = CascadeEnum.All)]
        public FormFilling FormFilling { get; set; }

    }
}

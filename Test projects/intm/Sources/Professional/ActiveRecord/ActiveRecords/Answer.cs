﻿using System;
using Castle.ActiveRecord;

namespace Eureca.Professional.ActiveRecords
{
    /// <summary>
    /// Represents answer active record.
    /// </summary>
    [ActiveRecord("Answers")]
    public class Answer : ActiveRecordBase<Answer>
    {
        /// <summary>
        /// Default constructor to be used internally.
        /// </summary>
        internal Answer()
        {
        }

        /// <summary>
        /// Initializes new instance of answer active record.
        /// </summary>
        /// <param name="content">Form of answer.</param>
        /// <exception cref="ArgumentNullException">Form is null.</exception>
        public Answer(Content content)
        {
            if (content == null)
            {
                throw new ArgumentNullException("content");
            }

            Content = content;
        }

        /// <summary>
        /// Gets primary id of answer.
        /// </summary>
        [PrimaryKey("AnswerId")]
        public int Id { get; internal set; }

        /// <summary>
        /// Gets or sets content of answer.
        /// </summary>
        [BelongsTo("ContentId", NotNull = true, Cascade = CascadeEnum.SaveUpdate)]
        public Content Content { get; set; }


        /// <summary>
        /// строковое представление ответа
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            if (Content.RichContent != null)
            {
                return string.Format("Форматированный текст");
            }
            if (Content.PlainContent != null)
            {
                string res;
                string _text = Content.PlainContent;
                int len = _text.IndexOf("\n");
                if (len == -1)
                    len = _text.Length;
                if (len > 80)
                {
                    len = _text.LastIndexOf(" ", 80);
                }
                if (len > 80)
                {
                    len = 80;
                }

                if (len < _text.Length && len != -1)
                {
                    res = _text.Substring(0, len) + " ...";
                }
                else
                {
                    res = _text;
                }
                return res.Replace("\r", "");
            }
            return "";
        }

        /// <summary>
        /// сравнение ответа
        /// </summary>
        /// <param name="obj">другой объект ответа</param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            var answerObj = (Answer) obj;
            return Id == answerObj.Id;
        }

        /// <summary>
        /// получение хэш кода объекта
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using Castle.ActiveRecord;

namespace Eureca.Professional.ActiveRecords
{
    /// <summary>
    /// Represents scale active record.
    /// </summary>
    [ActiveRecord("Scales")]
    public class Scale : ActiveRecordBase<Scale>
    {
        /// <summary>
        /// Default constructor to be used internally.
        /// </summary>
        internal Scale()
        {
        }

        /// <summary>
        /// Initializes new instance of scale active record.
        /// </summary>
        /// <param name="name">Scale name.</param>
        /// <param name="abbreviation">Abbreviation of scale name.</param>
        /// <exception cref="ArgumentNullException">Name is null.</exception>
        public Scale(string name, string abbreviation)
        {
            if (name == null)
            {
                throw new ArgumentNullException("name");
            }

            Name = name;
            Abbreviation = abbreviation;
            TypeScale = 2; //пользовательский тип. можем редактировать
            Tests = new List<Test>();
        }

        /// <summary>
        /// Initializes new instance of scale active record without abbreviation.
        /// </summary>
        /// <param name="name">Scale name.</param>
        /// <exception cref="ArgumentNullException">Name is null.</exception>
        public Scale(string name) : this(name, null)
        {
        }

        /// <summary>
        /// Gets primary id of scale.
        /// </summary>
        [PrimaryKey("ScaleId")]
        public int Id { get; internal set; }

        /// <summary>
        /// Gets or sets scale name.
        /// </summary>
        [Property(Unique = true, NotNull = true)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets abbreviation of scale name (can be null if there is no abbreviation).
        /// </summary>
        [Property]
        public string Abbreviation { get; set; }

        /// <summary>
        /// тип шкалы
        /// Field Type
        ///1 - Стандартные шкалы(нельзя менять)
        ///2 - пользовательские шкалы(можно менять)
        /// </summary>
        [Property("Type")]
        public int TypeScale { get; internal set; }

        /// <summary>
        /// Gets list of tests the current scale belongs to.
        /// </summary>
        [HasAndBelongsToMany(Table = "TestScales", ColumnKey = "ScaleId", ColumnRef = "TestId",
            Cascade = ManyRelationCascadeEnum.SaveUpdate) ]
        public IList<Test> Tests { get; internal set; }

        /// <summary>
        /// строковое представление шкалы
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return Abbreviation.Length > 0 ? String.Format("{0} ({1})", Name, Abbreviation) : Name;
        }

        /// <summary>
        /// сравнение шкал
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            var scaleObj = (Scale) obj;
            return Id == scaleObj.Id;
        }

        /// <summary>
        /// хэш код объекта
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
﻿using System;
using Castle.ActiveRecord;

namespace Eureca.Professional.ActiveRecords
{

    #region TestAnswerKey

    /// <summary>
    /// Represents composite key of test answer active record.
    /// </summary>
    [Serializable]
    internal class TestAnswerKey
    {
        /// <summary>
        /// Gets or sets primary id of test.
        /// </summary>
        [KeyProperty]
        public int TestId { get; internal set; }

        /// <summary>
        /// Gets or sets primary id of answer.
        /// </summary>
        [KeyProperty]
        public int AnswerId { get; internal set; }

        /// <summary>
        /// Serves as a hash function for test answer key.
        /// </summary>
        /// <returns>A hash code for the current test answer key.</returns>
        public override int GetHashCode()
        {
            return TestId ^ AnswerId;
        }

        /// <summary>
        /// Determines whether the specified test answer key is equal to the current test answer key.
        /// </summary>
        /// <param name="obj">The System.Object to compare with the current test answer key.</param>
        /// <returns>true if the specified System.Object is equal to the current test answer key; otherwise, false.</returns>
        public override bool Equals(object obj)
        {
            if (this == obj)
            {
                return true;
            }

            var key = obj as TestAnswerKey;

            if (key == null)
            {
                return false;
            }

            return TestId == key.TestId && AnswerId == key.AnswerId;
        }
    }

    #endregion

    /// <summary>
    /// Represents test answer active record.
    /// </summary>
    [ActiveRecord("TestAnswers")]
    public class TestAnswer : ActiveRecordBase<TestAnswer>
    {
        /// <summary>
        /// Default constructor to be used internally.
        /// </summary>
        internal TestAnswer()
        {
        }

        /// <summary>
        /// Initializes new instance of test answer active record.
        /// </summary>
        /// <param name="test">Saved to database test active record.</param>
        /// <param name="answer">Saved to database answer active record.</param>
        /// <param name="order">Order of answer.</param>
        /// <exception cref="ArgumentNullException">Test is null or answer is null.</exception>
        /// <exception cref="ArgumentException">Test is unsaved or answer is unsaved.</exception>
        public TestAnswer(Test test, Answer answer, int order)
        {
            if (test == null)
            {
                throw new ArgumentNullException("test");
            }

            if (answer == null)
            {
                throw new ArgumentNullException("answer");
            }

            if (test.Id == 0)
            {
                throw new ArgumentException("Test should be an active record that is saved to database.", "test");
            }

            if (answer.Id == 0)
            {
                throw new ArgumentException("Answer should be an active record that is saved to database.", "answer");
            }

            Key = new TestAnswerKey {TestId = test.Id, AnswerId = answer.Id};
            Test = test;
            Answer = answer;
            AnswerOrder = order;
        }

        /// <summary>
        /// Gets or sets (internally only) composite key of test answer consisting of test id and answer id.
        /// </summary>
        [CompositeKey]
        internal TestAnswerKey Key { get; set; }

        /// <summary>
        /// Gets test active record.
        /// </summary>
        [BelongsTo("TestId", Insert = false, Update = false)]
        public Test Test { get; internal set; }

        /// <summary>
        /// Gets answer active record.
        /// </summary>
        [BelongsTo("AnswerId", Insert = false, Update = false)]
        public Answer Answer { get; internal set; }

        /// <summary>
        /// Gets or sets order of answer.
        /// </summary>
        [Property]
        public int AnswerOrder { get; set; }


        /// <summary>
        /// текстовое представление объекта
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            if (Answer.Content.RichContent != null)
            {
                return string.Format("{0} {1}", AnswerOrder, "Форматированный текст");
            }
            if (Answer.Content.PlainContent != null)
            {
                string res;
                string _text = Answer.Content.PlainContent;
                int len = _text.IndexOf("\n");
                if (len == -1)
                    len = _text.Length;
                if (len > 80)
                {
                    len = _text.LastIndexOf(" ", 80);
                }
                if (len > 80)
                {
                    len = 80;
                }

                if (len < _text.Length && len != -1)
                {
                    res = _text.Substring(0, len) + " ...";
                }
                else
                {
                    res = _text;
                }
                return string.Format("{0} {1}", AnswerOrder, res.Replace("\r", ""));
            }
            return "";
        }

        /// <summary>
        /// определяет, является ли объект данным объектом
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            var answerObj = (TestAnswer) obj;
            return Answer.Id == answerObj.Answer.Id;
        }

        /// <summary>
        /// получение хэш кода объекта
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
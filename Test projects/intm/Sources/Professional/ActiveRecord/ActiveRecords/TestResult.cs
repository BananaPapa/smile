﻿using System;
using Castle.ActiveRecord;

namespace Eureca.Professional.ActiveRecords
{

    #region TestResultKey

    /// <summary>
    /// Represents composite key of test result.
    /// </summary>
    [Serializable]
    internal class TestResultKey
    {
        /// <summary>
        /// Gets or sets primary id of test passing.
        /// </summary>
        [KeyProperty]
        public int TestPassingId { get; set; }

        /// <summary>
        /// Gets or sets primary id of question.
        /// </summary>
        [KeyProperty]
        public int QuestionId { get; set; }

        /// <summary>
        /// Gets or sets primary id of answer.
        /// </summary>
        [KeyProperty]
        public int AnswerId { get; set; }

        /// <summary>
        /// Serves as a hash function for test result key.
        /// </summary>
        /// <returns>A hash code for the current test result key.</returns>
        public override int GetHashCode()
        {
            return TestPassingId ^ QuestionId ^ AnswerId;
        }

        /// <summary>
        /// Determines whether the specified test result key is equal to the current test result key.
        /// </summary>
        /// <param name="obj">The System.Object to compare with the current test result key.</param>
        /// <returns>true if the specified System.Object is equal to the current test result key; otherwise, false.</returns>
        public override bool Equals(object obj)
        {
            if (this == obj)
            {
                return true;
            }

            var key = obj as TestResultKey;

            if (key == null)
            {
                return false;
            }

            return TestPassingId == key.TestPassingId && QuestionId == key.QuestionId &&
                   AnswerId == key.AnswerId;
        }
    }

    #endregion

    /// <summary>
    /// Represents test result active record.
    /// </summary>
    [ActiveRecord("TestResults")]
    public class TestResult : ActiveRecordBase<TestResult>
    {
        /// <summary>
        /// Default constructor to be used internally.
        /// </summary>
        internal TestResult()
        {
        }

        /// <summary>
        /// Initializes new instance of test result active record.
        /// </summary>
        /// <param name="testPassing">Saved to database test passing active record.</param>
        /// <param name="question">Saved to database question active record.</param>
        /// <param name="answer">Saved to database answer active record.</param>
        /// <param name="timeInterval"></param>
        /// <param name="isSupervise"></param>
        /// <exception cref="ArgumentNullException">Test passing or question or answer or time interval is null.</exception>
        /// <exception cref="ArgumentException">Test passing or question or answer is unsaved.</exception>
        public TestResult(TestPassing testPassing, Question question, Answer answer, TimeInterval timeInterval)
        {
            #region Arguments validation

            if (testPassing == null)
            {
                throw new ArgumentNullException("testPassing");
            }

            if (question == null)
            {
                throw new ArgumentNullException("question");
            }

            if (answer == null)
            {
                throw new ArgumentNullException("answer");
            }

            if (timeInterval == null)
            {
                throw new ArgumentNullException("timeInterval");
            }

            if (testPassing.Id == 0)
            {
                throw new ArgumentException("Test should be an active record that is saved to database.", "testPassing");
            }

            if (question.Id == 0)
            {
                throw new ArgumentException("Question should be an active record that is saved to database.", "question");
            }

            if (answer.Id == 0)
            {
                throw new ArgumentException("Answer should be an active record that is saved to database.", "answer");
            }

            #endregion

            Key = new TestResultKey {TestPassingId = testPassing.Id, QuestionId = question.Id, AnswerId = answer.Id};
            TestPassing = testPassing;
            Question = question;
            Answer = answer;
            TimeInterval = timeInterval;
            
        }

        /// <summary>
        /// Gets or sets (internally only) composite key of test result.
        /// </summary>
        [CompositeKey]
        internal TestResultKey Key { get; set; }

        /// <summary>
        /// Get or sets time interval.
        /// </summary>
        [BelongsTo("TimeIntervalId", Cascade = CascadeEnum.All)]
        public TimeInterval TimeInterval { get; internal set; }

        /// <summary>
        /// Gets test passing active record.
        /// </summary>
        [BelongsTo("TestPassingId", Insert = false, Update = false)]
        public TestPassing TestPassing { get; internal set; }

        /// <summary>
        /// Gets question active record.
        /// </summary>
        [BelongsTo("QuestionId", Insert = false, Update = false)]
        public Question Question { get; internal set; }

        /// <summary>
        /// Gets answer active record.
        /// </summary>
        [BelongsTo("AnswerId", Insert = false, Update = false)]
        public Answer Answer { get; internal set; }


        
    }
}
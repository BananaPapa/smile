﻿using System;
using System.Collections.Generic;
using Castle.ActiveRecord;
using Eureca.Professional.ActiveRecords;

namespace Eureca.Professional.ActiveRecords
{
    /// <summary>
    /// Represents test active record.
    /// </summary>
    [ActiveRecord( "Tests" )]
    public class Test : ActiveRecordBase<Test> //,IComparable
    {
        /// <summary>
        /// Default constructor to be used internally.
        /// </summary>
        internal Test( )
        {
        }

        /// <summary>
        /// Initializes new instance of test active record.
        /// </summary>
        /// 
        /// <param name="testType">Test type.</param>
        /// <param name="method">Test method.</param>
        /// <param name="title">Title of test.</param>
        /// <param name="timeLimit">Test time limit (number of  seconds).</param>
        /// <param name="passaingScore">passing score of test</param>
        /// <param name="maxTeacherPassing">MaxTeacherPassing</param>
        /// <param name="description">Test description.</param>
        /// <param name="options">Test options.</param>
        /// <exception cref="ArgumentNullException">Test type is null or method is null or title is null.</exception>
        public Test( TestType testType, Method method, string title, int? timeLimit, int passaingScore, int maxTeacherPassing, Content description, string options )
        {
            if (testType == null)
            {
                throw new ArgumentNullException( "testType" );
            }

            if (method == null)
            {
                throw new ArgumentNullException( "method" );
            }

            if (title == null)
            {
                throw new ArgumentNullException( "title" );
            }
            if (passaingScore < 0)
            {
                throw new Exception( "Проходной балл должен быть больше 0" );
            }
            if (maxTeacherPassing < 0)
            {
                throw new Exception( "Максимальное количество учебных прохождений должно быть больше 0" );
            }

            UniqueId = Guid.NewGuid( );
            Type = testType;
            Method = method;
            Title = title;
            TimeLimit = timeLimit;
            PassingScore = passaingScore;
            Description = description;
            Options = options;
            Scales = new List<Scale>( );
            Reports = new List<Report>( );
            Profiles = new List<Profile>();
        }

        /// <summary>
        /// Gets primary id of test.
        /// </summary>
        [PrimaryKey( "TestId" )]
        public int Id { get; internal set; }

        /// <summary>
        /// Gets or sets test type.
        /// </summary>
        [BelongsTo( "TypeId", NotNull = true, Cascade = CascadeEnum.None )]
        public TestType Type { get; set; }

        /// <summary>
        /// Gets or sets test method.
        /// </summary>
        [BelongsTo( "MethodId", NotNull = true, Cascade = CascadeEnum.None )]
        public Method Method { get; set; }

        /// <summary>
        /// Gets or sets title of test.
        /// </summary>
        [Property( Unique = true, NotNull = true )]
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets test description.
        /// </summary>
        [BelongsTo( "DescriptionContentId", Cascade = CascadeEnum.All )]
        public Content Description { get; set; }

        /// <summary>
        /// Gets or sets test time limit (number or seconds).
        /// </summary>
        [Property]
        public int? TimeLimit { get; set; }


        /// <summary>
        /// Gets or sets test speed (group/hour).
        /// </summary>
        [Property]
        public int? Speed { get; set; }

        /// <summary>
        /// Gets or sets test MaxErrorPercent.
        /// </summary>
        [Property]
        public int? MaxErrorPercent { get; set; }


        /// <summary>
        /// Gets or sets test UniqueId
        /// </summary>
        [Property]
        public Guid UniqueId { get; set; }

        /// <summary>
        /// Gets or sets passing score test 0..100 percent default 80
        /// </summary>
        [Property]
        public int PassingScore { get; set; }

        [Property]
        public int MaxTeacherPassing { get; set; }

        /// <summary>
        /// Gets or sets test options.
        /// </summary>
        [Property]
        public string Options { get; set; }

        /// <summary>
        /// Gets (lazily) list of tests the current scale belongs to.
        /// </summary>
        [HasAndBelongsToMany( Table = "TestScales", ColumnKey = "TestId", ColumnRef = "ScaleId",
            Cascade = ManyRelationCascadeEnum.SaveUpdate )]
        public IList<Scale> Scales { get; internal set; }


        /// <summary>
        /// Gets (lazily) list of tests the current report belongs to.
        /// </summary>
        [HasAndBelongsToMany( Table = "TestReports", ColumnKey = "TestId", ColumnRef = "ReportId",
            Cascade = ManyRelationCascadeEnum.SaveUpdate,
            Lazy = true )]
        public IList<Report> Reports { get; internal set; }


        /// <summary>
        /// Gets (lazily) list of profiles the current test is added to.
        /// </summary>
        [HasAndBelongsToMany( Table = "ProfileTests", ColumnKey = "TestId", ColumnRef = "ProfileId",
            Cascade = ManyRelationCascadeEnum.None,
            Lazy = true )]
        public IList<Profile> Profiles { get; internal set; }

        /// <summary>
        /// ограничение времени на тест
        /// </summary>
        public DateTime? TimeLimitDatetime
        {
            get
            {
                if (TimeLimit == null)
                    return null;
                var dt = new DateTime( 0 );
                return dt.AddSeconds( (double)TimeLimit );
            }
        }

        /// <summary>
        /// строковое представление объекта
        /// </summary>
        /// <returns></returns>
        public override string ToString( )
        {
            return Title;
        }

        /// <summary>
        /// определяет является ли объект текущим тестом
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals( object obj )
        {
            if (obj is Test)
            {
                var newObj = (Test)obj;
                return Id == newObj.Id;
            }
            return false;
        }

        public void DeleteWithReference()
        {
            var test = Find(Id);
            foreach (var scale in test.Scales)
            {
                scale.Tests.Remove( test );
            }
            test.Scales.Clear( );
            foreach (var report in test.Reports)
            {
                report.Tests.Remove( test );
            }
            test.Reports.Clear( );
            foreach (var profile in test.Profiles)
            {
                profile.Tests.Remove( test );
            }
            test.Profiles.Clear( );
            var method = test.Method;
            method.Tests.Remove( test );
            //remove test from method call delete test
            method.UpdateAndFlush( );
            //so if we call delete test or other method, exception will raise couse test already delete
        }

        /// <summary>
        /// перегружаемый метод
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode( )
        {
            return base.GetHashCode( );
        }
    }
}
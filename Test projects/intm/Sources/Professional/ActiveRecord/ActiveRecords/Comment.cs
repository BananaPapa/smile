﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.ActiveRecord;
using Eureca.Utility;
using Eureca.Utility.Attributes;

namespace Eureca.Professional.ActiveRecords
{
    public interface IComment
    {
        /// <summary>
        /// Gets primary id of content.
        /// </summary>
        int Id { get; }

        string CommentText { get; set; }

        CommentType CommentType { get; set; }

        int StartPosition { get; set; }

        int EndPosition { get; set; }
    }

    [ActiveRecord( "Comments" )]
    public class Comment : ActiveRecordBase<Comment>, IComment
    {
        public Comment( ) {}

        /// <summary>
        /// Gets primary id of content.
        /// </summary>
        [PrimaryKey( "CommentId" )]
        public int Id { get; internal set; }

        /// <summary>
        /// Gets or sets content of answer.
        /// </summary>
        [BelongsTo( "FormId", NotNull = true, Cascade = CascadeEnum.None)]
        public RtfForm Form { get; private set; }

        [Property( "Comment" )]
        public string CommentText { get;  set; }

        [Property]
        public CommentType CommentType { get;  set; }


        [Property]
        public int StartPosition { get; set; }

        [Property]
        public int EndPosition { get; set; }

        public Comment(RtfForm form , string comment , CommentType commentType , int startPosition , int endPosition )
        {
            this.Form = form;
            this.CommentText = comment;
            this.CommentType = commentType;
            this.StartPosition = startPosition;
            this.EndPosition = endPosition;
        }

        /// <summary>
        /// сравнение ответа
        /// </summary>
        /// <param name="obj">другой объект ответа</param>
        /// <returns></returns>
        public override bool Equals( object obj )
        {
            var answerObj = (IComment)obj;
            return Id == answerObj.Id;
        }

        /// <summary>
        /// получение хэш кода объекта
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode( )
        {
            return base.GetHashCode( );
        }


        public void Move( int delta )
        {
            StartPosition += delta;
            EndPosition += delta;
            if (StartPosition < 0)
                StartPosition = 0;
            if (EndPosition < 0)
                EndPosition = 0;
        }
    }

    public enum CommentType
    {
        [Browsable( true )]
        [Color( "#FF00AA00" )]
        [Description( "Информация" )]
        Info,
        [Browsable( true )]
        [Color( "#FFCCCC00" )]
        [Description( "Предупреждение" )]
        Warning,
        [Browsable( true )]
        [Color( "#FFAA0000" )]
        [Description( "Ошибка" )]
        Error,
        [Browsable(false)]
        [Color("#FF5555FF")]
        [Description("Ссылка")]
        Link 
    }

    public class CommentLink : SettingsBase
    {
        private int _blancId;

        public int TargetBlancId
        {
            get { return _blancId; }
            set { _blancId = value; }
        }

        public static CommentLink Deserialize( string xml, out Exception ex )
        {
            return (CommentLink)Deserialize( typeof( CommentLink ), xml, out ex );
        }

        public CommentLink( int blancId )
        {
            _blancId = blancId;
            
        }
        public override string Serialize( out Exception exception )
        {
            return Serialize( typeof( CommentLink ), this, out exception );
        }

        CommentLink()
        {
                
        }

    }
}

﻿using System;
using Castle.ActiveRecord;
using Eureca.Professional.ActiveRecords;
using NHibernate.Criterion;
using NUnit.Framework;

namespace Eureca.Professional.ActiveRecordsTests
{
    [TestFixture]
    public class EventTestFixture : ActiveRecordTestFixtureBase
    {
        #region Setup/Teardown

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            ActiveRecordBase<Event>.DeleteAll();
        }

        #endregion

        [Test]
        public void AdditionEvent()
        {
            int id = Event.Add(
                new User("1", "123", "123",  DateTime.Now,  "1"),
                EventTypes.BeginTesting, -1);
            Event ev = ActiveRecordBase<Event>.Find(id);
            ev.Delete();
            Assert.AreEqual(ActiveRecordBase<Event>.FindOne(Expression.Eq("Id", id)), null);
        }

        [Test]
        public void CreationSelectionDeletion()
        {
            var Event =
                new Event(new User("1", "123", "123",  DateTime.Now,  "1"),
                          ActiveRecordBase<EventType>.Find(1));
            Event.Create();

            Assert.Greater(Event.Id, 0);

            var Event2 =
                new Event(new User("1", "123", "123",  DateTime.Now,  "1"),
                          ActiveRecordBase<EventType>.Find(2));
            Event2.Create();

            Assert.Greater(Event2.Id, 0);

            Event[] Events = ActiveRecordBase<Event>.FindAll();

            Assert.AreEqual(Events.Length, 2);
            Assert.Greater(Events[1].Id, Events[0].Id);

            ActiveRecordBase<Event>.DeleteAll();

            Assert.AreEqual(ActiveRecordBase<Event>.FindAll().Length, 0);
        }

        [Test]
        public void GetEventsByTime()
        {
            //
            Event.Add(new User("1", "123", "123",  DateTime.Now,  "1"),
                      EventTypes.BeginTesting, -1);
            Event.Add(new User("1", "123", "123",  DateTime.Now,  "1"),
                      EventTypes.BeginTesting, -1);
            Event.Add(new User("1", "123", "123",  DateTime.Now,  "1"),
                      EventTypes.BeginTesting, -1);

            Assert.AreEqual(ActiveRecordBase<Event>.FindAll().Length, 3);

            Assert.AreEqual(Event.GetEventsFromInterval(DateTime.Now.AddDays(-1), DateTime.Now.AddDays(1)).Length, 3);

            Assert.AreEqual(Event.GetEventsFromInterval(DateTime.Now.AddDays(1), DateTime.Now.AddDays(1)).Length, 0);
        }
    }
}
﻿using System;
using System.Diagnostics;
using Castle.ActiveRecord;
using Eureca.Professional.ActiveRecords;
using NHibernate;
using NHibernate.Criterion;
using NUnit.Framework;

namespace Eureca.Professional.ActiveRecordsTests
{
    [TestFixture]
    public class ControlQuestionTestFixture : ActiveRecordTestFixtureBase
    {
        #region Setup/Teardown

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            ActiveRecordBase<ControlQuestion>.DeleteAll();
            ActiveRecordBase<Method>.DeleteAll();
            ActiveRecordBase<Answer>.DeleteAll();
            ActiveRecordBase<Question>.DeleteAll();
            ActiveRecordBase<Content>.DeleteAll();
        }

        #endregion

        private static void ArgumentsTest(bool methodIsNull, bool questionIsNull, bool answerIsNull, bool commentIsNull,
                                          bool toCreateMethod, bool toCreateQuestion)
        {
            Method method = null;

            if (!methodIsNull)
            {
                method = new Method("Method", new Content("Description"));

                if (toCreateMethod) method.Create();
            }

            Question question = null;

            if (!questionIsNull)
            {
                question = new Question(new Content("Question"));

                if (toCreateQuestion) question.Create();
            }

            Answer answer = null;

            if (!answerIsNull)
            {
                answer = new Answer(new Content("Answer"));
            }

            Content comment = null;

            if (!commentIsNull)
            {
                comment = new Content("Comment");
            }

            new ControlQuestion(method, question, answer, comment, 1);
        }

        //[Test]
        //[ExpectedException(typeof (ArgumentNullException))]
        //public void AnswerIsNull()
        //{
        //    ArgumentsTest(false, false, true, false, true, true);
        //}

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void CommentIsNull()
        {
            ArgumentsTest(false, false, false, true, true, true);
        }

        [Test]
        [ExpectedException(typeof (ADOException))]
        public void CompositeKeyUniqueness()
        {
            var method = new Method("Method", new Content("Description"));
            method.Create();

            var question = new Question(new Content("Question"));
            question.Create();

            var answer = new Answer(new Content("Answer"));
            var comment = new Content("Comment");

            var controlQuestion = new ControlQuestion(method, question, answer, comment, 1);
            controlQuestion.Create();

            var controlQuestion2 = new ControlQuestion(method, question, new Answer(new Content("AnotherAnwer")),
                                                       new Content("Another comment"), 2);
            controlQuestion2.Create();
        }

        [Test]
        public void CreationSelectionModificationDeletion()
        {
            var method = new Method("Method", new Content("Description"));
            method.Create();

            var question = new Question(new Content("Question"));
            question.Create();

            var answer = new Answer(new Content("Answer"));
            var comment = new Content("Comment");

            var controlQuestion = new ControlQuestion(method, question, answer, comment, 1);
            controlQuestion.Create();

            CreateScope(FlushAction.Never);

            controlQuestion = ActiveRecordBase<ControlQuestion>.FindOne(Expression.Eq("Method", method));

            Assert.AreNotEqual(controlQuestion, null);

            Debug.WriteLine("Control question: " + controlQuestion.Question.Content.PlainContent);

            controlQuestion.QuestionOrder = 2;
            controlQuestion.Comment.PlainContent = "NewComment";

            controlQuestion.Update();

            CloseScope();

            CreateScope(FlushAction.Never);

            controlQuestion = ActiveRecordBase<ControlQuestion>.FindOne(Expression.Eq("Method", method));

            Assert.AreEqual(controlQuestion.QuestionOrder, 2);
            Assert.AreEqual(controlQuestion.Comment.PlainContent, "NewComment");

            CloseScope();

            controlQuestion.Delete();

            Assert.AreEqual(ActiveRecordBase<ControlQuestion>.FindOne(Expression.Eq("Method", method)), null);
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void MethodIsNull()
        {
            ArgumentsTest(true, false, false, false, false, true);
        }

        [Test]
        [ExpectedException(typeof (ArgumentException))]
        public void MethodIsUnsaved()
        {
            ArgumentsTest(false, false, false, false, false, true);
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void QuestionIsNull()
        {
            ArgumentsTest(false, true, false, false, true, false);
        }

        [Test]
        [ExpectedException(typeof (ArgumentException))]
        public void QuestionIsUnsaved()
        {
            ArgumentsTest(false, false, false, false, true, false);
        }
    }
}
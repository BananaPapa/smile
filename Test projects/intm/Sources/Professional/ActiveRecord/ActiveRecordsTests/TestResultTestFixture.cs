﻿using System;
using Castle.ActiveRecord;
using Eureca.Professional.ActiveRecords;
using NHibernate;
using NHibernate.Criterion;
using NUnit.Framework;

namespace Eureca.Professional.ActiveRecordsTests
{
    [TestFixture]
    public class TestResultTestFixture : ActiveRecordTestFixtureBase
    {
        #region Setup/Teardown

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            ActiveRecordBase<TestResult>.DeleteAll();
            ActiveRecordBase<TestPassing>.DeleteAll();
            ActiveRecordBase<Test>.DeleteAll();
            ActiveRecordBase<Method>.DeleteAll();
            ActiveRecordBase<TestType>.DeleteAll();
            ActiveRecordBase<Answer>.DeleteAll();
            ActiveRecordBase<Question>.DeleteAll();
            ActiveRecordBase<Content>.DeleteAll();
            ActiveRecordBase<Probationer>.DeleteAll();
            ActiveRecordBase<Profile>.DeleteAll();
            ActiveRecordBase<TimeInterval>.DeleteAll();
        }

        #endregion

        private static void ArgumentsTest(bool testPassingIsNull, bool questionIsNull, bool answerIsNull,
                                          bool timeIntervalIsNull, bool toCreateTestPassing, bool toCreateQuestion,
                                          bool toCreateAnswer)
        {
            TestPassing passing = null;

            if (!testPassingIsNull)
            {
                passing = new TestPassing(new Test(
                                              new TestType("Type", "PlugIn", false),
                                              new Method("Method", new Content("Form")),
                                              "Test",
                                              null, 80, 5,
                                              new Content("Description"),
                                              null
                                              ),
                                          new Probationer(
                                              new User("1", "123", "123",  DateTime.Now,
                                                        "1"), new Profile("Profile")),
                                          new TimeInterval(DateTime.Now, DateTime.Now + TimeSpan.FromHours(1)),false);

                if (toCreateTestPassing) passing.Create();
            }

            Question question = null;

            if (!questionIsNull)
            {
                question = new Question(new Content("Question"));

                if (toCreateQuestion) question.Create();
            }

            Answer answer = null;

            if (!answerIsNull)
            {
                answer = new Answer(new Content("Answer"));
                if (toCreateAnswer) answer.Create();
            }

            TimeInterval interval = null;

            if (!timeIntervalIsNull)
            {
                interval = new TimeInterval(DateTime.Now, DateTime.Now + TimeSpan.FromHours(2));
            }

            new TestResult(passing, question, answer, interval);
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void AnswerIsNull()
        {
            ArgumentsTest(false, false, true, false, true, true, false);
        }

        [Test]
        [ExpectedException(typeof (ArgumentException))]
        public void AnswerIsUnsaved()
        {
            ArgumentsTest(false, false, false, false, true, true, false);
        }

        [Test]
        [ExpectedException(typeof (ADOException))]
        public void CompositeKeyUniqueness()
        {
            var passing = new TestPassing(new Test(
                                              new TestType("Type", "PlugIn", false),
                                              new Method("Method", new Content("Form")),
                                              "Test",
                                              null, 80, 5,
                                              new Content("Description"),
                                              null
                                              ),
                                          new Probationer(
                                              new User("1", "123", "123",  DateTime.Now,
                                                        "1"), new Profile("Profile")),
                                          new TimeInterval(DateTime.Now, DateTime.Now + TimeSpan.FromHours(1)),false);
            passing.Create();

            var question = new Question(new Content("Question"));
            question.Create();

            var answer = new Answer(new Content("Answer"));
            answer.Create();

            var interval = new TimeInterval(DateTime.Now, DateTime.Now + TimeSpan.FromHours(2));

            var result1 = new TestResult(passing, question, answer, interval);
            result1.Create();

            var result2 = new TestResult(passing, question, answer, interval);
            result2.Create();
        }

        [Test]
        public void CreationSelectionDeletion()
        {
            var passing = new TestPassing(new Test(
                                              new TestType("Type", "PlugIn", false),
                                              new Method("Method", new Content("Form")),
                                              "Test",
                                              null, 80, 5,
                                              new Content("Description"),
                                              null
                                              ),
                                          new Probationer(
                                              new User("1", "123", "123",  DateTime.Now,
                                                       "1"), new Profile("Profile")),
                                          new TimeInterval(DateTime.Now, DateTime.Now + TimeSpan.FromHours(1)),false);
            passing.Create();

            var question = new Question(new Content("Question"));
            question.Create();

            var answer = new Answer(new Content("Answer"));
            answer.Create();

            var interval = new TimeInterval(DateTime.Now, DateTime.Now + TimeSpan.FromHours(2));

            var result = new TestResult(passing, question, answer, interval);
            result.Create();

            CreateScope(FlushAction.Never);

            TestResult[] results = ActiveRecordBase<TestResult>.FindAll(Expression.Eq("TestPassing", passing));

            Assert.AreEqual(results.Length, 1);
            Assert.AreEqual(results[0].Question.Content.PlainContent, "Question");

            CloseScope();

            ActiveRecordBase<TestResult>.DeleteAll();

            Assert.AreEqual(ActiveRecordBase<TestResult>.FindAll().Length, 0);
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void QuestionIsNull()
        {
            ArgumentsTest(false, true, false, false, true, false, true);
        }

        [Test]
        [ExpectedException(typeof (ArgumentException))]
        public void QuestionIsUnsaved()
        {
            ArgumentsTest(false, false, false, false, true, false, true);
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void TestPassingIsNull()
        {
            ArgumentsTest(true, false, false, false, false, true, true);
        }

        [Test]
        [ExpectedException(typeof (ArgumentException))]
        public void TestPassingIsUnsaved()
        {
            ArgumentsTest(false, false, false, false, false, true, true);
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void TimeIntervalIsNull()
        {
            ArgumentsTest(false, false, false, true, true, true, true);
        }
    }
}
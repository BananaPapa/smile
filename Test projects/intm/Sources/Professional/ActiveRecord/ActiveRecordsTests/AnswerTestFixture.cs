﻿using System;
using System.Diagnostics;
using Castle.ActiveRecord;
using Eureca.Professional.ActiveRecords;
using NHibernate;
using NHibernate.Criterion;
using NUnit.Framework;

namespace Eureca.Professional.ActiveRecordsTests
{
    [TestFixture]
    public class AnswerTestFixture : ActiveRecordTestFixtureBase
    {
        #region Setup/Teardown

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            ActiveRecordBase<Answer>.DeleteAll();
            ActiveRecordBase<Content>.DeleteAll();
        }

        #endregion

        private void Selection(bool toCreateScope)
        {
            var answer = new Answer(new Content("Mega"));
            answer.Create();
            var answer2 = new Answer(new Content("Mega2"));
            answer2.Create();

            if (toCreateScope) CreateScope(FlushAction.Never);

            Answer[] answers = ActiveRecordBase<Answer>.FindAll();

            Assert.AreEqual(answers.Length, 2);

            foreach (Answer a in answers)
            {
                Debug.WriteLine("Answer: " + a.Content.PlainContent);
            }

            if (toCreateScope) CloseScope();
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void ContentIsNull()
        {
            new Answer(null);
        }

        [Test]
        public void Creation()
        {
            var content = new Content("SomeContent");
            var answer = new Answer(content);
            answer.Create();

            Assert.AreEqual(answer.Content.PlainContent, "SomeContent");
        }

        [Test]
        public void Deletion()
        {
            var content = new Content("SomeContent");
            var answer = new Answer(content);
            answer.Create();

            int id = answer.Id;

            answer.Delete();

            Assert.AreEqual(ActiveRecordBase<Answer>.FindOne(Expression.Eq("Id", id)), null);
        }

        [Test]
        public void Modification()
        {
            var content = new Content("SomeContent");
            content.Create();
            var content2 = new Content("SomeContent2");
            content2.Create();

            var answer = new Answer(content);
            answer.Create();

            Assert.AreEqual(answer.Content.PlainContent, "SomeContent");

            answer.Content = content2;
            answer.Update();

            CreateScope(FlushAction.Never);

            answer.Refresh();

            Assert.AreEqual(answer.Content.PlainContent, "SomeContent2");

            answer.Content.PlainContent = "SomeContent3";
            answer.Update();
            answer.Refresh();

            Assert.AreEqual(answer.Content.PlainContent, "SomeContent3");

            CloseScope();
        }

        [Test]
        public void SelectionWithScope()
        {
            Selection(true);
        }

        [Test]
        [ExpectedException(typeof (LazyInitializationException))]
        public void SelectionWithoutScope()
        {
            Selection(false);
        }
    }
}
﻿using System.Diagnostics;
using Castle.ActiveRecord;
using Castle.ActiveRecord.Framework;
using Eureca.Professional.ActiveRecords;
using NHibernate.Criterion;
using NUnit.Framework;

namespace Eureca.Professional.ActiveRecordsTests
{
    [TestFixture]
    public class SystemPropertiesTestFixture : ActiveRecordTestFixtureBase
    {
        #region Setup/Teardown

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            ActiveRecordBase<SystemProperties>.DeleteAll();
            
        }

        #endregion

        [Test]
        public void CreationSelectionModificationDeletion()
        {
            var sp = new SystemProperties(SysPropEnum.CurriculumConnString,"one");
            sp.Create();

            Assert.Greater(sp.EnumId, 0);
            Assert.AreEqual(sp.EnumId, (int)SysPropEnum.CurriculumConnString);

            sp.Value = "two";
            sp.Update();

            sp.Refresh();
            Assert.AreEqual(sp.Value, "two");

            int id = sp.EnumId;

            var item = SystemProperties.GetItem(SysPropEnum.CurriculumConnString);

            Assert.AreEqual(sp.Value, item.Value);

            sp.Delete();

            Assert.AreEqual(ActiveRecordBase<SystemProperties>.FindOne(Expression.Eq("EnumId", id)), null);
        }
    }
}
﻿using System;
using Castle.ActiveRecord;
using Eureca.Professional.ActiveRecords;
using NHibernate;
using NHibernate.Criterion;
using NUnit.Framework;

namespace Eureca.Professional.ActiveRecordsTests
{
    [TestFixture]
    public class QuestionAnswerTestFixture : ActiveRecordTestFixtureBase
    {
        #region Setup/Teardown

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            ActiveRecordBase<QuestionAnswer>.DeleteAll();
            ActiveRecordBase<Answer>.DeleteAll();
            ActiveRecordBase<Question>.DeleteAll();
            ActiveRecordBase<Content>.DeleteAll();
        }

        #endregion

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void AnswerIsNull()
        {
            var question = new Question(new Content("Question"));
            question.Create();

            new QuestionAnswer(question, null, 1);
        }

        [Test]
        [ExpectedException(typeof (ArgumentException))]
        public void AnswerIsUnsaved()
        {
            var question = new Question(new Content("Question"));
            question.Create();

            new QuestionAnswer(question, new Answer(new Content("123")), 1);
        }

        [Test]
        [ExpectedException(typeof (ADOException))]
        public void CompositeKeyUniqueness()
        {
            var question = new Question(new Content("Question"));
            question.Create();

            Assert.Greater(question.Id, 0);

            var answer = new Answer(new Content("SomeText"));
            answer.Create();
            Assert.Greater(answer.Id, 0);

            var questionAnswer1 = new QuestionAnswer(question, answer, 1);
            questionAnswer1.Create();

            var questionAnswer2 = new QuestionAnswer(question, answer, 1);
            questionAnswer2.Create();
        }

        [Test]
        public void CreationSelectionModificationDeletion()
        {
            var question = new Question(new Content("Question"));
            question.Create();

            Assert.Greater(question.Id, 0);

            var answer1 = new Answer(new Content("SomeText"));
            answer1.Create();
            Assert.Greater(answer1.Id, 0);

            var answer2 = new Answer(new Content(new byte[] {0x0F, 0x34}));
            answer2.Create();
            Assert.Greater(answer2.Id, 0);

            var questionAnswer1 = new QuestionAnswer(question, answer1, 1);
            questionAnswer1.Create();

            var questionAnswer2 = new QuestionAnswer(question, answer2, 1);
            questionAnswer2.Create();

            QuestionAnswer[] questionAnswers = ActiveRecordBase<QuestionAnswer>.FindAll(new Order("AnswerOrder", true),
                                                                                        Expression.Eq("Question",
                                                                                                      question));

            Assert.IsTrue( questionAnswers != null );
            Assert.IsTrue( questionAnswers.Length == 2 );
            Assert.IsTrue( questionAnswers[0].Question.Id == question.Id && questionAnswers[1].Question.Id == question.Id );
            Assert.IsTrue( questionAnswers[0].Answer.Id == answer1.Id );

            questionAnswers[0].AnswerOrder = 2;
            questionAnswers[0].Update();

            questionAnswers[1].AnswerOrder = 1;
            questionAnswers[1].Update();

            questionAnswers = ActiveRecordBase<QuestionAnswer>.FindAll(new Order("AnswerOrder", true),
                                                                       Expression.Eq("Question", question));

            Assert.IsTrue( questionAnswers[1].Answer.Id == answer1.Id );

            questionAnswer1.Delete();

            Assert.AreEqual(ActiveRecordBase<QuestionAnswer>.FindOne(Expression.Eq("Answer", questionAnswer1.Answer)),
                            null);

            ActiveRecordBase<QuestionAnswer>.DeleteAll();

            Assert.AreEqual(ActiveRecordBase<QuestionAnswer>.FindAll().Length, 0);
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void QuestionIsNull()
        {
            var answer = new Answer(new Content("SomeText"));
            answer.Create();
            Assert.Greater(answer.Id, 0);

            new QuestionAnswer(null, answer, 1);
        }

        [Test]
        [ExpectedException(typeof (ArgumentException))]
        public void QuestionIsUnsaved()
        {
            var answer = new Answer(new Content("SomeText"));
            answer.Create();
            Assert.Greater(answer.Id, 0);

            var question = new Question(new Content("Question"));

            new QuestionAnswer(question, answer, 1);
        }
    }
}
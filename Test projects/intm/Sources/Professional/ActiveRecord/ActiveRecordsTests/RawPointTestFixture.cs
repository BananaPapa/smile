﻿using System;
using Castle.ActiveRecord;
using Eureca.Professional.ActiveRecords;
using NHibernate;
using NHibernate.Criterion;
using NUnit.Framework;

namespace Eureca.Professional.ActiveRecordsTests
{
    [TestFixture]
    public class RawPointTestFixture : ActiveRecordTestFixtureBase
    {
        

        private static void ArgumentsTest(bool testIsNull, bool scaleIsNull, bool questionIsNull, bool answerIsNull,
                                          bool toCreateTest, bool toCreateScale, bool toCreateQuestion,
                                          bool toCreateAnswer)
        {
            Test test = null;

            if (!testIsNull)
            {
                test = new Test(
                    new TestType("Type", "PlugIn", false),
                    new Method("Method", new Content("Form")),
                    "Test",
                    null, 80, 5,
                    new Content("Description"),
                    null
                    );

                if (toCreateTest) test.Create();
            }

            Scale scale = null;

            if (!scaleIsNull)
            {
                scale = new Scale("Scale");

                if (toCreateScale) scale.Create();
            }

            Question question = null;

            if (!questionIsNull)
            {
                question = new Question(new Content("Question"));

                if (toCreateQuestion) question.Create();
            }

            Answer answer = null;

            if (!answerIsNull)
            {
                answer = new Answer(new Content("Answer"));
                if (toCreateAnswer) answer.Create();
            }

            new RawPoint(test, scale, question, answer, 1);
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void AnswerIsNull()
        {
            ArgumentsTest(false, false, false, true, true, true, true, true);
        }

        [Test]
        [ExpectedException(typeof (ArgumentException))]
        public void AnswerIsUnsaved()
        {
            ArgumentsTest(false, false, false, false, true, true, true, false);
        }

        [Test]
        [ExpectedException(typeof (ADOException))]
        public void CompositeKeyUniqueness()
        {
            var test = new Test(
                new TestType("Type", "PlugIn", false),
                new Method("Method", new Content("Form")),
                "Test",
                null, 80, 5,
                new Content("Description"),
                null
                );
            test.Create();

            var scale = new Scale("Scale");
            scale.Create();

            var question = new Question(new Content("Question"));
            question.Create();

            var answer = new Answer(new Content("Answer"));
            answer.Create();

            var point = new RawPoint(test, scale, question, answer, 1);
            point.Create();

            var point2 = new RawPoint(test, scale, question, answer, 12);
            point2.Create();
        }

        [Test]
        public void CreationSelectionModificationDeletion()
        {
            var test = new Test(
                new TestType("Type", "PlugIn", false),
                new Method("Method", new Content("Form")),
                "Test",
                null, 80, 5,
                new Content("Description"),
                null
                );
            test.Create();

            var scale = new Scale("Scale");
            scale.Create();

            var question = new Question(new Content("Question"));
            question.Create();

            var answer = new Answer(new Content("Answer"));
            answer.Create();

            var point = new RawPoint(test, scale, question, answer, 1);
            point.Create();

            point = ActiveRecordBase<RawPoint>.FindOne(Expression.Eq("Test", test), Expression.Eq("Scale", scale),
                                                       Expression.Eq("Question", question),
                                                       Expression.Eq("Answer", answer));

            Assert.AreNotEqual(point, null);
            Assert.AreEqual(point.Value, 1);

            point.Value = 234;
            point.Update();

            point.Refresh();

            Assert.AreEqual(point.Value, 234);

            point.Delete();

            Assert.AreEqual(
                ActiveRecordBase<RawPoint>.FindOne(Expression.Eq("Test", test), Expression.Eq("Scale", scale),
                                                   Expression.Eq("Question", question),
                                                   Expression.Eq("Answer", answer)), null);
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void QuestionIsNull()
        {
            ArgumentsTest(false, false, true, false, true, true, true, true);
        }

        [Test]
        [ExpectedException(typeof (ArgumentException))]
        public void QuestionIsUnsaved()
        {
            ArgumentsTest(false, false, false, false, true, true, false, true);
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void ScaleIsNull()
        {
            ArgumentsTest(false, true, false, false, true, true, true, true);
        }

        [Test]
        [ExpectedException(typeof (ArgumentException))]
        public void ScaleIsUnsaved()
        {
            ArgumentsTest(false, false, false, false, true, false, true, true);
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void TestIsNull()
        {
            ArgumentsTest(true, false, false, false, true, true, true, true);
        }

        [Test]
        [ExpectedException(typeof (ArgumentException))]
        public void TestIsUnsaved()
        {
            ArgumentsTest(false, false, false, false, false, true, true, true);
        }
    }
}
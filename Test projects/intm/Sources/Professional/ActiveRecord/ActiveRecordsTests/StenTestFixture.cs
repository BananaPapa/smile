﻿using System;
using Castle.ActiveRecord;
using Eureca.Professional.ActiveRecords;
using NHibernate.Criterion;
using NUnit.Framework;

namespace Eureca.Professional.ActiveRecordsTests
{
    [TestFixture]
    public class StenTestFixture : ActiveRecordTestFixtureBase
    {
        #region Setup/Teardown

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            ActiveRecordBase<Sten>.DeleteAll();
            ActiveRecordBase<Test>.DeleteAll();
            ActiveRecordBase<Method>.DeleteAll();
            ActiveRecordBase<TestType>.DeleteAll();
            ActiveRecordBase<Content>.DeleteAll();
            ActiveRecordBase<Scale>.DeleteAll();
        }

        #endregion

        [Test]
        public void CreationSelectionModificationDeletion()
        {
            var sten = new Sten(new Test(
                                    new TestType("Type", "PlugIn", false),
                                    new Method("Method", new Content("Form")),
                                    "Test",
                                    null, 80, 5,
                                    new Content("Description"),
                                    null
                                    ), new Scale("Scale"), 0, 20, 1);

            sten.Create();
            Assert.Greater(sten.Id, 0);

            sten = ActiveRecordBase<Sten>.Find(sten.Id);
            Assert.AreEqual(sten.Value, 1);

            sten.MaxRawPoints = 30;
            sten.Description = "Description";
            sten.Update();

            sten = ActiveRecordBase<Sten>.Find(sten.Id);
            Assert.AreEqual(sten.MaxRawPoints, 30);
            Assert.AreEqual(sten.Description, "Description");

            sten.Delete();

            Assert.AreEqual(ActiveRecordBase<Sten>.FindOne(Expression.Eq("Id", sten.Id)), null);
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void ScaleIsNull()
        {
            var test = new Test(
                new TestType("Type", "PlugIn", false),
                new Method("Method", new Content("Form")),
                "Test",
                null, 80, 5,
                new Content("Description"),
                null
                );

            new Sten(test, null, 0, 20, 1);
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void TestIsNull()
        {
            var scale = new Scale("Scale");

            var sten = new Sten(null, scale, 0, 20, 1);
            sten.Create();
        }
    }
}
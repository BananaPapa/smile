﻿using Castle.ActiveRecord;
using Castle.ActiveRecord.Framework.Config;
using Eureca.Professional.ActiveRecords;
using NUnit.Framework;

namespace Eureca.Professional.ActiveRecordsTests
{
    /// <summary>
    /// Base class for all test fixture in the active records tests project.
    /// </summary>
    public abstract class ActiveRecordTestFixtureBase
    {
        /// <summary>
        /// Whether active record framework has been already initialized.
        /// </summary>
        private static bool _initialized;

        /// <summary>
        /// Instance of session scope.
        /// </summary>
        private SessionScope _sessionScope;

        [TestFixtureSetUp]
        public virtual void TestFixtureSetUp()
        {
            InitFramework();

            ClearDatabase();
        }

        [TestFixtureTearDown]
        public virtual void TestFixtureTearDown()
        {
            ClearDatabase();
        }

        [SetUp]
        public virtual void SetUp()
        {
        }

        /// <summary>
        /// Initializes new session scope.
        /// </summary>
        /// <param name="flushAction"></param>
        protected void CreateScope(FlushAction flushAction)
        {
            _sessionScope = new SessionScope(flushAction);
        }

        [TearDown]
        public virtual void TearDown()
        {
        }

        /// <summary>
        /// Flushes and disposes previously initialized session scope.
        /// </summary>
        protected void CloseScope()
        {
            _sessionScope.Flush();
            _sessionScope.Dispose();
        }

        /// <summary>
        /// Clears all database data.
        /// </summary>
        protected static void ClearDatabase()
        {
            ActiveRecordBase<ReportField>.DeleteAll();
            ActiveRecordBase<TestResult>.DeleteAll();
            ActiveRecordBase<TestPassing>.DeleteAll();
            ActiveRecordBase<ProfilePassing>.DeleteAll();
            ActiveRecordBase<TimeInterval>.DeleteAll();
            ActiveRecordBase<Probationer>.DeleteAll();
            ActiveRecordBase<Profile>.DeleteAll();
            ActiveRecordBase<MemorizationInfo>.DeleteAll();
            ActiveRecordBase<ControlQuestion>.DeleteAll();
            ActiveRecordBase<Sten>.DeleteAll();
            ActiveRecordBase<RawPoint>.DeleteAll();
            ActiveRecordBase<Scale>.DeleteAll();
            ActiveRecordBase<TestQuestion>.DeleteAll();
            ActiveRecordBase<TestAnswer>.DeleteAll();
            ActiveRecordBase<QuestionAnswer>.DeleteAll();
            ActiveRecordBase<Answer>.DeleteAll();
            ActiveRecordBase<Question>.DeleteAll();
            ActiveRecordBase<Test>.DeleteAll();
            ActiveRecordBase<TestType>.DeleteAll();
            ActiveRecordBase<Method>.DeleteAll();
            ActiveRecordBase<Content>.DeleteAll();
            ActiveRecordBase<Event>.DeleteAll();
            ActiveRecordBase<User>.DeleteAll();
            ActiveRecordBase<Report>.DeleteAll();
            ActiveRecordBase<SystemProperties>.DeleteAll();
        }

        /// <summary>
        /// Initializes active record framework.
        /// </summary>
        protected virtual void InitFramework()
        {
            if (_initialized) return;

            ActiveRecordStarter.Initialize(ActiveRecordSectionHandler.Instance,
                                           typeof (Answer),
                                           typeof (Content),
                                           typeof (ControlQuestion),
                                           typeof (MemorizationInfo),
                                           typeof (Method),
                                           typeof (Probationer),
                                           typeof (Profile),
                                           typeof (ProfilePassing),
                                           typeof (Question),
                                           typeof (QuestionAnswer),
                                           typeof (RawPoint),
                                           typeof (Scale),
                                           typeof (Sten),
                                           typeof (Test),
                                           typeof (TestAnswer),
                                           typeof (TestPassing),
                                           typeof (TestQuestion),
                                           typeof (TestResult),
                                           typeof (TestType),
                                           typeof (TimeInterval),
                                           typeof (User),
                                           typeof (EventType),
                                           typeof (Event),
                                           typeof (Report),
                                           typeof (ReportField),
                                           typeof(SystemProperties)
                );

            _initialized = true;
        }
    }
}
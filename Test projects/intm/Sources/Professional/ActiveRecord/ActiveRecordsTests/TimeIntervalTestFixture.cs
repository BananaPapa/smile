﻿using System;
using Castle.ActiveRecord;
using Eureca.Professional.ActiveRecords;
using NUnit.Framework;

namespace Eureca.Professional.ActiveRecordsTests
{
    [TestFixture]
    public class TimeIntervalTestFixture : ActiveRecordTestFixtureBase
    {
        #region Setup/Teardown

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            ActiveRecordBase<TimeInterval>.DeleteAll();
        }

        #endregion

        [Test]
        public void CreationSelectionDeletion()
        {
            var timeInterval = new TimeInterval(DateTime.Now, DateTime.Now + TimeSpan.FromMinutes(1));
            timeInterval.Create();

            Assert.Greater(timeInterval.Id, (decimal)0);

            var timeInterval2 = new TimeInterval(DateTime.Now + TimeSpan.FromMinutes(1),
                                                 DateTime.Now + TimeSpan.FromMinutes(2));
            timeInterval2.Create();

            TimeInterval[] intervals = ActiveRecordBase<TimeInterval>.FindAll();

            Assert.AreEqual(intervals.Length, 2);
            Assert.Greater(intervals[1].StartTime, intervals[0].StartTime);
            Assert.Less(intervals[0].FinishTime, intervals[1].FinishTime);

            ActiveRecordBase<TimeInterval>.DeleteAll();

            Assert.AreEqual(ActiveRecordBase<TimeInterval>.FindAll().Length, 0);
        }

        [Test]
        [ExpectedException(typeof (ArgumentException))]
        public void StartTimeIsLaterThanFinishTime()
        {
            new TimeInterval(DateTime.Now, DateTime.Now - TimeSpan.FromMinutes(1));
        }
    }
}
﻿using Castle.ActiveRecord;
using Castle.ActiveRecord.Framework;
using Eureca.Professional.ActiveRecords;
using NUnit.Framework;

namespace Eureca.Professional.ActiveRecordsTests
{
    [TestFixture]
    public class EventTypeTestFixture : ActiveRecordTestFixtureBase
    {
        #region Setup/Teardown

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            //ActiveRecordBase<EventType>.DeleteAll();
            ActiveRecordBase<Event>.DeleteAll();
            ActiveRecordBase<Test>.DeleteAll();
            ActiveRecordBase<Method>.DeleteAll();
            ActiveRecordBase<TestType>.DeleteAll();
            ActiveRecordBase<Content>.DeleteAll();
        }

        #endregion

        [Test]
        public void ExistEventTypes()
        {
            for (int i = 1; i < 3; i++)
            {
                if (ActiveRecordBase<EventType>.Find(i) == null)
                {
                    throw new NotFoundException("with id = " + i);
                }
            }
        }

        [Test]
        [ExpectedException(typeof (ActiveRecordException))]
        public void UniqueName()
        {
            EventType eventType = EventType.CreateEventType(EventTypes.BeginEdit, "EventType");

            EventType eventType2 = EventType.CreateEventType(EventTypes.BeginTesting, "EventType");
        }
    }
}
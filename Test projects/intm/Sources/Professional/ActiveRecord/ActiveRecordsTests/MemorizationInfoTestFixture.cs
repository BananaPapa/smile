﻿using System;
using Castle.ActiveRecord;
using Eureca.Professional.ActiveRecords;
using NHibernate;
using NHibernate.Criterion;
using NUnit.Framework;

namespace Eureca.Professional.ActiveRecordsTests
{
    [TestFixture]
    public class MemorizationInfoTestFixture : ActiveRecordTestFixtureBase
    {
        #region Setup/Teardown

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            ActiveRecordBase<MemorizationInfo>.DeleteAll();
            ActiveRecordBase<Test>.DeleteAll();
            ActiveRecordBase<Method>.DeleteAll();
            ActiveRecordBase<TestType>.DeleteAll();
            ActiveRecordBase<Content>.DeleteAll();
        }

        #endregion

        private static void ArgumentsTest(bool testIsNull, bool contentIsNull, bool toCreateTest, bool toCreateContent)
        {
            Test test = null;

            if (!testIsNull)
            {
                test = new Test(
                    new TestType("Type", "PlugIn", false),
                    new Method("Method", new Content("Form")),
                    "Test",
                    null, 80, 5,
                    new Content("Description"),
                    null
                    );

                if (toCreateTest) test.Create();
            }

            Content content = null;

            if (!contentIsNull)
            {
                content = new Content("Form");

                if (toCreateContent) content.Create();
            }

            new MemorizationInfo(test, content, 100, 1);
        }

        [Test]
        [ExpectedException(typeof (ADOException))]
        public void CompositeTestUniqueness()
        {
            var test = new Test(
                new TestType("Type", "PlugIn", false),
                new Method("Method", new Content("Form")),
                "Test",
                null, 80, 5,
                new Content("Description"),
                null
                );

            test.Create();

            var content = new Content("Form");
            content.Create();

            var info = new MemorizationInfo(test, content, 100, 1);
            info.Create();

            var info2 = new MemorizationInfo(test, content, 200, 2);
            info2.Create();
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void ContentIsNull()
        {
            ArgumentsTest(false, true, true, false);
        }

        [Test]
        [ExpectedException(typeof (ArgumentException))]
        public void ContentIsUnsaved()
        {
            ArgumentsTest(false, false, true, false);
        }

        [Test]
        public void CreationSelectionModificationDeletion()
        {
            var test = new Test(
                new TestType("Type", "PlugIn", false),
                new Method("Method", new Content("Form")),
                "Test",
                null, 80, 5,
                new Content("Description"),
                null
                );

            test.Create();

            var content = new Content("Form");
            content.Create();

            var info = new MemorizationInfo(test, content, 100, 1);
            info.Create();

            CreateScope(FlushAction.Never);

            info = ActiveRecordBase<MemorizationInfo>.FindOne(Expression.Eq("Test", test));

            Assert.AreEqual(info.Content.PlainContent, "Form");

            info.Content.PlainContent = "Content2";
            info.Update();

            CloseScope();

            CreateScope(FlushAction.Never);

            info = ActiveRecordBase<MemorizationInfo>.FindOne(Expression.Eq("Test", test));

            Assert.AreEqual(info.Content.PlainContent, "Content2");

            CloseScope();

            info.Delete();

            Assert.AreEqual(ActiveRecordBase<MemorizationInfo>.FindOne(Expression.Eq("Test", test)), null);
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void TestIsNull()
        {
            ArgumentsTest(true, false, false, true);
        }

        [Test]
        [ExpectedException(typeof (ArgumentException))]
        public void TestIsUnsaved()
        {
            ArgumentsTest(false, false, false, true);
        }
    }
}
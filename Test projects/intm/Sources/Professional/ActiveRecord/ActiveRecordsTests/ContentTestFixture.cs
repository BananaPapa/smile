﻿using System;
using Castle.ActiveRecord;
using Eureca.Professional.ActiveRecords;
using NHibernate.Criterion;
using NUnit.Framework;

namespace Eureca.Professional.ActiveRecordsTests
{
    [TestFixture]
    public class ContentTestFixture : ActiveRecordTestFixtureBase
    {
        #region Setup/Teardown

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            ActiveRecordBase<Content>.DeleteAll();
        }

        #endregion

        [Test]
        public void CreationPlain()
        {
            var content = new Content("SomeContent");
            content.Create();

            Assert.AreEqual(content.PlainContent, "SomeContent");
            Assert.AreEqual(content.RichContent, null);
        }

        [Test]
        public void CreationRich()
        {
            var data = new byte[] {0x00, 0xAA, 0xFF};

            var content = new Content(data);
            content.Create();

            Assert.AreEqual(content.PlainContent, null);
            Assert.AreEqual(content.RichContent, data);
        }

        [Test]
        public void Deletion()
        {
            var content = new Content("SomeContent");
            content.Create();

            long id = content.Id;

            content.Delete();

            Assert.AreEqual(ActiveRecordBase<Content>.FindOne(Expression.Eq("Id", id)), null);
        }

        [Test]
        public void Modification()
        {
            var data = new byte[] {0xAC, 0xED, 0xFF};

            var content = new Content("SomeContent");
            content.Create();

            Assert.Greater(content.Id, (decimal)0);

            long id = content.Id;

            content.PlainContent = null;
            content.RichContent = data;
            content.Save();

            content = ActiveRecordBase<Content>.Find(id);

            Assert.AreEqual(content.PlainContent, null);
            Assert.AreEqual(content.RichContent, data);
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void PlainTextIsNull()
        {
            new Content((string) null);
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void RichTextIsNull()
        {
            new Content((byte[]) null);
        }

        [Test]
        public void Selection()
        {
            var content = new Content("SomeContent");
            content.Create();

            var content2 = new Content(new byte[] {0xAA, 0xEE, 0xFF});
            content2.Create();

            long id = content.Id;

            Content c = ActiveRecordBase<Content>.Find(id);

            Assert.AreEqual(c.PlainContent, "SomeContent");

            Assert.AreEqual(ActiveRecordBase<Content>.FindAll().Length, 2);
        }
    }
}
﻿using System;
using Castle.ActiveRecord;
using Eureca.Professional.ActiveRecords;
using NHibernate;
using NHibernate.Criterion;
using NUnit.Framework;

namespace Eureca.Professional.ActiveRecordsTests
{
    [TestFixture]
    public class TestAnswerTestFixture : ActiveRecordTestFixtureBase
    {
        #region Setup/Teardown

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            ActiveRecordBase<TestAnswer>.DeleteAll();
            ActiveRecordBase<Answer>.DeleteAll();
            ActiveRecordBase<Test>.DeleteAll();
            ActiveRecordBase<Method>.DeleteAll();
            ActiveRecordBase<TestType>.DeleteAll();
            ActiveRecordBase<Content>.DeleteAll();
        }

        #endregion

        private static Test NewTest()
        {
            return new Test(
                new TestType("Type", "PlugIn", false),
                new Method("Method", new Content("Form")),
                "Test",
                null,80,5,
                new Content("Description"),
                null
                );
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void AnswerIsNull()
        {
            Test test = NewTest();
            test.Create();

            new TestAnswer(test, null, 1);
        }

        [Test]
        [ExpectedException(typeof (ArgumentException))]
        public void AnswerIsUnsaved()
        {
            Test test = NewTest();
            test.Create();

            new TestAnswer(test, new Answer(new Content("123")), 1);
        }

        [Test]
        [ExpectedException(typeof (ADOException))]
        public void CompositeKeyUniqueness()
        {
            Test test = NewTest();
            test.Create();

            Assert.Greater(test.Id, 0);

            var answer = new Answer(new Content("SomeText"));
            answer.Create();
            Assert.Greater(answer.Id, 0);

            var testAnswer1 = new TestAnswer(test, answer, 1);
            testAnswer1.Create();

            var testAnswer2 = new TestAnswer(test, answer, 1);
            testAnswer2.Create();
        }

        [Test]
        public void CreationSelectionModificationDeletion()
        {
            Test test = NewTest();
            test.Create();

            Assert.Greater(test.Id, 0);

            var answer1 = new Answer(new Content("SomeText"));
            answer1.Create();
            Assert.Greater(answer1.Id, 0);

            var answer2 = new Answer(new Content(new byte[] {0x0F, 0x34}));
            answer2.Create();
            Assert.Greater(answer2.Id, 0);

            var testAnswer1 = new TestAnswer(test, answer1, 1);
            testAnswer1.Create();

            var testAnswer2 = new TestAnswer(test, answer2, 1);
            testAnswer2.Create();

            TestAnswer[] testAnswers = ActiveRecordBase<TestAnswer>.FindAll(new Order("AnswerOrder", true),
                                                                            Expression.Eq("Test", test));

            Assert.IsTrue(testAnswers != null);
            Assert.IsTrue( testAnswers.Length == 2 );
            Assert.IsTrue( testAnswers[0].Test.Id == test.Id && testAnswers[1].Test.Id == test.Id );
            Assert.IsTrue( testAnswers[0].Answer.Id == answer1.Id );

            testAnswers[0].AnswerOrder = 2;
            testAnswers[0].Update();

            testAnswers[1].AnswerOrder = 1;
            testAnswers[1].Update();

            testAnswers = ActiveRecordBase<TestAnswer>.FindAll(new Order("AnswerOrder", true),
                                                               Expression.Eq("Test", test));

            Assert.IsTrue( testAnswers[1].Answer.Id == answer1.Id );

            testAnswer1.Delete();

            Assert.AreEqual(ActiveRecordBase<TestAnswer>.FindOne(Expression.Eq("Answer", testAnswer1.Answer)), null);

            ActiveRecordBase<TestAnswer>.DeleteAll();

            Assert.AreEqual(ActiveRecordBase<TestAnswer>.FindAll().Length, 0);
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void TestIsNull()
        {
            var answer = new Answer(new Content("SomeText"));
            answer.Create();
            Assert.Greater(answer.Id, 0);

            new TestAnswer(null, answer, 1);
        }

        [Test]
        [ExpectedException(typeof (ArgumentException))]
        public void TestIsUnsaved()
        {
            var answer = new Answer(new Content("SomeText"));
            answer.Create();
            Assert.Greater(answer.Id, 0);

            Test test = NewTest();

            new TestAnswer(test, answer, 1);
        }
    }
}
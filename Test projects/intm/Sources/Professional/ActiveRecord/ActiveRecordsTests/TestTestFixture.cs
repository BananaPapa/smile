using System.Diagnostics;
using Castle.ActiveRecord;
using Eureca.Professional.ActiveRecords;
using NHibernate.Criterion;
using NUnit.Framework;

namespace Eureca.Professional.ActiveRecordsTests
{
    [TestFixture]
    public class TestTestMethodFixture : ActiveRecordTestFixtureBase
    {
        #region Setup/Teardown

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            ActiveRecordBase<TestAnswer>.DeleteAll();
            ActiveRecordBase<TestQuestion>.DeleteAll();
            ActiveRecordBase<QuestionAnswer>.DeleteAll();
            ActiveRecordBase<Question>.DeleteAll();
            ActiveRecordBase<Answer>.DeleteAll();
            ActiveRecordBase<Test>.DeleteAll();
            ActiveRecordBase<Method>.DeleteAll();
            ActiveRecordBase<TestType>.DeleteAll();
            ActiveRecordBase<Content>.DeleteAll();
            ActiveRecordBase<Scale>.DeleteAll();
            ActiveRecordBase<Profile>.DeleteAll();
        }

        #endregion

        [Test]
        public void CreationSelectionModificationDeletion()
        {
            var test = new Test(
                new TestType("Type", "PlugIn", false),
                new Method("Method", new Content("Form")),
                "Test",
                null, 80, 5,
                new Content("Description"),
                null
                );

            test.Create();

            Assert.Greater(test.Id, 0);

            int id = test.Id;

            CreateScope(FlushAction.Never);

            test = ActiveRecordBase<Test>.Find(id);

            test.Method.Instruction.PlainContent = "Description2";
            test.TimeLimit = 10;
            test.Options = "<?xml>";

            test.Update();

            CloseScope();

            CreateScope(FlushAction.Never);

            test.Refresh();

            Assert.AreEqual(test.Method.Instruction.PlainContent, "Description2");
            Assert.AreEqual(test.TimeLimit, 10);
            Assert.AreEqual(test.Options, "<?xml>");

            CloseScope();

            test.Delete();

            Assert.AreEqual(ActiveRecordBase<Test>.FindOne(Expression.Eq("Id", id)), null);
        }

        [Test]
        public void ProfilesCollection()
        {
            var test = new Test(
                new TestType("Type", "PlugIn", false),
                new Method("Method", new Content("Form")),
                "Test",
                null, 80, 5,
                new Content("Description"),
                null
                );
            test.Create();

            CreateScope(FlushAction.Never);

            test.Refresh();

            test.Profiles.Add(new Profile("Profile"));
            test.Profiles.Add(new Profile("Profile2"));
            test.Update();

            CloseScope();

            CreateScope(FlushAction.Never);

            test.Refresh();

            Assert.AreEqual(test.Profiles.Count, 2);

            Debug.WriteLine("Test profile 1: " + test.Profiles[0].Title);
            Debug.WriteLine("Test profile 2: " + test.Profiles[1].Title);

            test.Delete();

            CloseScope();
        }

        [Test]
        public void ScalesCollection()
        {
            var test = new Test(
                new TestType("Type", "PlugIn", false),
                new Method("Method", new Content("Form")),
                "Test",
                null, 80, 5,
                new Content("Description"),
                null
                );
            test.Create();

            CreateScope(FlushAction.Never);

            test.Refresh();

            test.Scales.Add(new Scale("Scale"));
            test.Scales.Add(new Scale("Scale2"));
            test.Update();

            CloseScope();

            CreateScope(FlushAction.Never);

            test.Refresh();

            Assert.AreEqual(test.Scales.Count, 2);

            Debug.WriteLine("Test scale 1: " + test.Scales[0].Name);
            Debug.WriteLine("Test scale 2: " + test.Scales[1].Name);

            test.Delete();

            CloseScope();
        }
    }
}
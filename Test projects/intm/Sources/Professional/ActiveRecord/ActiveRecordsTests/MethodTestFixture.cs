﻿using System.Diagnostics;
using Castle.ActiveRecord;
using Castle.ActiveRecord.Framework;
using Eureca.Professional.ActiveRecords;
using NHibernate.Criterion;
using NUnit.Framework;

namespace Eureca.Professional.ActiveRecordsTests
{
    [TestFixture]
    public class MethodTestFixture : ActiveRecordTestFixtureBase
    {
        #region Setup/Teardown

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            ActiveRecordBase<Test>.DeleteAll();
            ActiveRecordBase<Method>.DeleteAll();
            ActiveRecordBase<Content>.DeleteAll();
        }

        #endregion

        [Test]
        public void Creation()
        {
            var content = new Content("Instruction");
            var method = new Method("Method", content);

            method.Create();

            Assert.Greater(method.Id, 0);
        }

        [Test]
        public void Deletion()
        {
            var content = new Content("Instruction");
            var method = new Method("Method", content);
            method.Create();

            Assert.Greater(method.Id, 0);

            int id = method.Id;

            method.Delete();

            Assert.AreEqual(ActiveRecordBase<Method>.FindOne(Expression.Eq("Id", id)), null);
        }

        [Test]
        public void Modification()
        {
            var content = new Content("Instruction");
            var method = new Method("Method", content);

            method.Create();

            Assert.Greater(method.Id, 0);

            int id = method.Id;

            method.Title = "Another method";
            method.Save();

            method = ActiveRecordBase<Method>.Find(id);

            Assert.AreEqual(method.Title, "Another method");
        }

        [Test]
        public void Selection()
        {
            var method = new Method("Method", new Content("Form"));
            var method2 = new Method("Method2", new Content("Content2"));
            method.Create();
            method2.Create();

            CreateScope(FlushAction.Never);

            Method[] methods = ActiveRecordBase<Method>.FindAll();

            Assert.AreEqual(methods.Length, 2);

            foreach (Method m in methods)
            {
                Debug.WriteLine("Method: ", m.Instruction.PlainContent);
            }

            CloseScope();

            var test = new Test(
                new TestType("Type", "PlugIn", false),
                method,
                "Test",
                null, 80, 5,
                new Content("Description"),
                null
                );

            test.Create();

            CreateScope(FlushAction.Never);

            method.Refresh();

            Assert.AreEqual(method.Tests.Count, 1);
            Assert.AreEqual(method.Tests[0].Description.PlainContent, "Description");

            CloseScope();
        }

        [Test]
        [ExpectedException(typeof (ActiveRecordException))]
        public void UniqueTitle()
        {
            var content = new Content("Instruction");
            var method = new Method("Method", content);
            method.Create();

            var method2 = new Method("Method", content);
            method2.Create();
        }
    }
}
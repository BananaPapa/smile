﻿using Castle.ActiveRecord;
using Eureca.Professional.ActiveRecords;
using NHibernate.Criterion;
using NUnit.Framework;

namespace Eureca.Professional.ActiveRecordsTests
{
    [TestFixture]
    public class ReportFieldTestFixture : ActiveRecordTestFixtureBase
    {
        #region Setup/Teardown

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            ActiveRecordBase<ReportField>.DeleteAll();
        }

        #endregion

        [Test]
        public void AdditionReportField()
        {
            var rf = new ReportField(new Report("dfs", "fsd", 1), "FIO", "Фамилия", null);
            rf.SaveAndFlush();
            int id = rf.Id;
            ReportField ev = ActiveRecordBase<ReportField>.Find(id);
            ev.Delete();
            Assert.AreEqual(ActiveRecordBase<ReportField>.FindOne(Expression.Eq("Id", id)), null);
        }
    }
}
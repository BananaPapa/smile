﻿using System;
using Castle.ActiveRecord;
using Eureca.Professional.ActiveRecords;
using NUnit.Framework;

namespace Eureca.Professional.ActiveRecordsTests
{
    [TestFixture]
    public class ProbationerTestFixture : ActiveRecordTestFixtureBase
    {
        #region Setup/Teardown

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            ActiveRecordBase<Probationer>.DeleteAll();
        }

        #endregion

        [Test]
        public void CreationSelectionDeletion()
        {
            var probationer =
                new Probationer(
                    new User("1", "123", "123",  DateTime.Now,  "1"),
                    new Profile("Profile"));
            probationer.Create();

            Assert.Greater(probationer.Id, 0);

            var probationer2 =
                new Probationer(
                    new User("1", "123", "123",  DateTime.Now,  "1"),
                    new Profile("Profile1"));
            probationer2.Create();

            Assert.Greater(probationer2.Id, 0);

            Probationer[] probationers = ActiveRecordBase<Probationer>.FindAll();

            Assert.AreEqual(probationers.Length, 2);
            Assert.Greater(probationers[1].Id, probationers[0].Id);

            ActiveRecordBase<Probationer>.DeleteAll();

            Assert.AreEqual(ActiveRecordBase<Probationer>.FindAll().Length, 0);
        }
    }
}
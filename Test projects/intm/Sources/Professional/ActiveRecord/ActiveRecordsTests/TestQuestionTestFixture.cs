﻿using System;
using Castle.ActiveRecord;
using Eureca.Professional.ActiveRecords;
using NHibernate;
using NHibernate.Criterion;
using NUnit.Framework;

namespace Eureca.Professional.ActiveRecordsTests
{
    [TestFixture]
    public class TestQuestionTestFixture : ActiveRecordTestFixtureBase
    {
        #region Setup/Teardown

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            ActiveRecordBase<TestQuestion>.DeleteAll();
            ActiveRecordBase<Question>.DeleteAll();
            ActiveRecordBase<Test>.DeleteAll();
            ActiveRecordBase<Method>.DeleteAll();
            ActiveRecordBase<TestType>.DeleteAll();
            ActiveRecordBase<Content>.DeleteAll();
        }

        #endregion

        private static Test NewTest()
        {
            return new Test(
                new TestType("Type", "PlugIn", false),
                new Method("Method", new Content("Form")),
                "Test",
                null, 80,5,
                new Content("Description"),
                null
                );
        }

        [Test]
        [ExpectedException(typeof (ADOException))]
        public void CompositeKeyUniqueness()
        {
            Test test = NewTest();
            test.Create();

            Assert.Greater(test.Id, 0);

            var question = new Question(new Content("SomeText"));
            question.Create();
            Assert.Greater(question.Id, 0);

            var testQuestion1 = new TestQuestion(test, question, 1);
            testQuestion1.Create();

            var testQuestion2 = new TestQuestion(test, question, 1);
            testQuestion2.Create();
        }

        [Test]
        public void CreationSelectionModificationDeletion()
        {
            Test test = NewTest();
            test.Create();

            Assert.Greater(test.Id, 0);

            var question1 = new Question(new Content("SomeText"));
            question1.Create();
            Assert.Greater(question1.Id, 0);

            var question2 = new Question(new Content(new byte[] {0x0F, 0x34}));
            question2.Create();
            Assert.Greater(question2.Id, 0);

            var testQuestion1 = new TestQuestion(test, question1, 1);
            testQuestion1.Create();

            var testQuestion2 = new TestQuestion(test, question2, 1);
            testQuestion2.Create();

            TestQuestion[] testQuestions = ActiveRecordBase<TestQuestion>.FindAll(new Order("QuestionOrder", true),
                                                                                  Expression.Eq("Test", test));

            Assert.IsTrue( testQuestions != null );
            Assert.IsTrue( testQuestions.Length == 2 );
            Assert.IsTrue( testQuestions[0].Test.Id == test.Id && testQuestions[1].Test.Id == test.Id );
            Assert.IsTrue( testQuestions[0].Question.Id == question1.Id );

            testQuestions[0].QuestionOrder = 2;
            testQuestions[0].Update();

            testQuestions[1].QuestionOrder = 1;
            testQuestions[1].Update();

            testQuestions = ActiveRecordBase<TestQuestion>.FindAll(new Order("QuestionOrder", true),
                                                                   Expression.Eq("Test", test));

            Assert.IsTrue( testQuestions[1].Question.Id == question1.Id );

            testQuestion1.Delete();

            Assert.AreEqual(ActiveRecordBase<TestQuestion>.FindOne(Expression.Eq("Question", testQuestion1.Question)),
                            null);

            ActiveRecordBase<TestQuestion>.DeleteAll();

            Assert.AreEqual(ActiveRecordBase<TestQuestion>.FindAll().Length, 0);
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void QuestionIsNull()
        {
            Test test = NewTest();
            test.Create();

            new TestQuestion(test, null, 1);
        }

        [Test]
        [ExpectedException(typeof (ArgumentException))]
        public void QuestionIsUnsaved()
        {
            Test test = NewTest();
            test.Create();

            new TestQuestion(test, new Question(new Content("123")), 1);
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void TestIsNull()
        {
            var question = new Question(new Content("SomeText"));
            question.Create();
            Assert.Greater(question.Id, 0);

            new TestQuestion(null, question, 1);
        }

        [Test]
        [ExpectedException(typeof (ArgumentException))]
        public void TestIsUnsaved()
        {
            var question = new Question(new Content("SomeText"));
            question.Create();
            Assert.Greater(question.Id, 0);

            Test test = NewTest();

            new TestQuestion(test, question, 1);
        }
    }
}
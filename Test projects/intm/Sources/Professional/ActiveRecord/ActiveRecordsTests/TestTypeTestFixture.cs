using System;
using System.Diagnostics;
using Castle.ActiveRecord;
using Castle.ActiveRecord.Framework;
using Eureca.Professional.ActiveRecords;
using NHibernate;
using NHibernate.Criterion;
using NUnit.Framework;

namespace Eureca.Professional.ActiveRecordsTests
{
    [TestFixture]
    public class TestTypeTestFixture : ActiveRecordTestFixtureBase
    {
        #region Setup/Teardown

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            ActiveRecordBase<Test>.DeleteAll();
            ActiveRecordBase<TestType>.DeleteAll();
            ActiveRecordBase<Method>.DeleteAll();
        }

        #endregion

        private void TestsLazyReference(bool toCreateScope)
        {
            var testType = new TestType("SomeType", "SomePlugIn", false);
            testType.Create();

            Assert.Greater(testType.Id, 0);

            var test = new Test(
                testType,
                new Method("Method", new Content("Form")),
                "Test",
                null, 80, 5,
                new Content("Description"),
                null
                );

            test.Create();

            Assert.Greater(test.Id, 0);

            int id = testType.Id;

            if (toCreateScope) CreateScope(FlushAction.Never);

            testType = ActiveRecordBase<TestType>.Find(id);

            Assert.AreEqual(testType.Tests.Count, 1);

            foreach (Test t in testType.Tests)
            {
                Debug.WriteLine("Test Description: " + t.Description.PlainContent);
            }

            if (toCreateScope) CloseScope();
        }

        [Test]
        public void Creation()
        {
            var testType = new TestType("SomeType", "SomePlugIn", false);
            testType.Create();

            Assert.AreEqual(testType.Title, "SomeType");
        }

        [Test]
        public void Deletion()
        {
            var testType = new TestType("SomeType", "SomePlugIn", false);
            testType.Create();

            int id = testType.Id;

            testType.Delete();

            Assert.AreEqual(ActiveRecordBase<TestType>.FindOne(Expression.Eq("Id", id)), null);
        }

        [Test]
        public void Modification()
        {
            var testType = new TestType("SomeType", "SomePlugIn", false);
            testType.Create();

            testType.Title = "AnotherType";
            testType.Update();

            Assert.AreEqual(testType.Title, "AnotherType");
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void PlugInFileNameIsNull()
        {
            new TestType("SomeTitle", null, false);
        }

        [Test]
        public void Selection()
        {
            var testType = new TestType("SomeType", "SomePlugIn", false);
            testType.Create();

            var testType2 = new TestType("SomeType2", "SomePlugIn2", false);
            testType2.Create();


            TestType[] types = ActiveRecordBase<TestType>.FindAll();

            Assert.AreEqual(types.Length, 2);
        }

        [Test]
        public void TestsReferenceWithScope()
        {
            TestsLazyReference(true);
        }

        [Test]
        [ExpectedException(typeof (LazyInitializationException))]
        public void TestsReferenceWithoutScope()
        {
            TestsLazyReference(false);
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void TitleIsNull()
        {
            new TestType(null, "SomePlugIn", false);
        }

        [Test]
        [ExpectedException(typeof (ActiveRecordException))]
        public void UniquePlugInFileName()
        {
            var testType = new TestType("SomeType", "SomePlugIn", false);
            testType.Create();

            var testType2 = new TestType("SomeType2", "SomePlugIn", false);
            testType2.Create();
        }

        [Test]
        [ExpectedException(typeof (ActiveRecordException))]
        public void UniqueTitle()
        {
            var testType = new TestType("SomeType", "SomePlugIn", false);
            testType.Create();

            var testType2 = new TestType("SomeType", "SomePlugIn2", false);
            testType2.Create();
        }
    }
}
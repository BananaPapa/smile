﻿using System;
using Castle.ActiveRecord;
using Eureca.Professional.ActiveRecords;
using NHibernate.Criterion;
using NUnit.Framework;

namespace Eureca.Professional.ActiveRecordsTests
{
    [TestFixture]
    public class ProfilePassingTestFixture : ActiveRecordTestFixtureBase
    {
        #region Setup/Teardown

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            ActiveRecordBase<TestPassing>.DeleteAll();
            ActiveRecordBase<ProfilePassing>.DeleteAll();
            ActiveRecordBase<Test>.DeleteAll();
            ActiveRecordBase<Probationer>.DeleteAll();
            ActiveRecordBase<Profile>.DeleteAll();
            ActiveRecordBase<TimeInterval>.DeleteAll();
        }

        #endregion

        [Test]
        public void CreationSelectionDeletion()
        {
            var passing =
                new ProfilePassing(
                    new Probationer(
                        new User("1", "123", "123",  DateTime.Now,  "1"),
                        new Profile("Profile")),
                    new TimeInterval(DateTime.Now, DateTime.Now + TimeSpan.FromHours(1)));
            passing.Create();

            Assert.Greater(passing.Id, 0);

            CreateScope(FlushAction.Never);

            passing = ActiveRecordBase<ProfilePassing>.Find(passing.Id);

            passing.TestPassings.Add(new TestPassing(new Test(
                                                         new TestType("Type", "PlugIn", false),
                                                         new Method("Method", new Content("Form")),
                                                         "Test",
                                                         null, 80, 5,
                                                         new Content("Description"),
                                                         null
                                                         ), passing.Probationer,
                                                     new TimeInterval(DateTime.Now, DateTime.Now + TimeSpan.FromHours(1)), false));

            passing.Update();

            CloseScope();

            CreateScope(FlushAction.Never);

            passing = ActiveRecordBase<ProfilePassing>.Find(passing.Id);

            Assert.AreEqual(passing.TestPassings.Count, 1);
            Assert.AreEqual(passing.TestPassings[0].Test.Title, "Test");

            passing.TestPassings.Clear();
            passing.Update();

            CloseScope();

            passing.Delete();

            Assert.AreEqual(ActiveRecordBase<ProfilePassing>.FindOne(Expression.Eq("Id", passing.Id)), null);
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void ProbationerIsNull()
        {
            new ProfilePassing(null,
                               new TimeInterval(DateTime.Now, DateTime.Now + TimeSpan.FromHours(1)));
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void ProfileIsNull()
        {
            new ProfilePassing(
                new Probationer(
                    new User("1", "123", "123",  DateTime.Now,  "1"), null),
                new TimeInterval(DateTime.Now, DateTime.Now + TimeSpan.FromHours(1)));
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void TimeIntervalIsNull()
        {
            new ProfilePassing(
                new Probationer(
                    new User("1", "123", "123",  DateTime.Now,  "1"),
                    new Profile("Profile")), null);
        }
    }
}
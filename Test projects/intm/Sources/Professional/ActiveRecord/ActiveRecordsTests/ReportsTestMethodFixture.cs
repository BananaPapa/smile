﻿using System.Diagnostics;
using Castle.ActiveRecord;
using Eureca.Professional.ActiveRecords;
using NHibernate.Criterion;
using NUnit.Framework;

namespace Eureca.Professional.ActiveRecordsTests
{
    [TestFixture]
    public class ReportTestFixture : ActiveRecordTestFixtureBase
    {
        #region Setup/Teardown

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            ActiveRecordBase<Report>.DeleteAll();
            ActiveRecordBase<Test>.DeleteAll();
            ActiveRecordBase<Method>.DeleteAll();
            ActiveRecordBase<TestType>.DeleteAll();
            ActiveRecordBase<Content>.DeleteAll();
        }

        #endregion

        [Test]
        public void CreationSelectionModificationDeletion()
        {
            var Report = new Report("Report", "123", 1);
            Report.Create();

            Assert.Greater(Report.Id, 0);


            Report.Refresh();

            int id = Report.Id;

            Report.Delete();

            Assert.AreEqual(ActiveRecordBase<Report>.FindOne(Expression.Eq("Id", id)), null);
        }


        [Test]
        public void TestsCollection()
        {
            var Report = new Report("Report", "Description", 1);
            Report.Create();

            CreateScope(FlushAction.Never);

            Report.Refresh();

            Report.Tests.Add(new Test(
                                 new TestType("Type", "PlugIn", false),
                                 new Method("Method", new Content("Form")),
                                 "Test",
                                 null, 80, 5,
                                 new Content("Description"),
                                 null
                                 ));
            Report.Update();

            CloseScope();

            CreateScope(FlushAction.Never);

            Report.Refresh();

            Assert.AreEqual(Report.Tests.Count, 1);

            Debug.WriteLine("Report test: " + Report.Tests[0].Title);
            Debug.WriteLine("Report test method: " + Report.Tests[0].Method.Instruction.PlainContent);

            Report.Delete();

            CloseScope();
        }
    }
}
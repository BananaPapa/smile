﻿using System;
using System.Diagnostics;
using Castle.ActiveRecord;
using Eureca.Professional.ActiveRecords;
using NHibernate;
using NHibernate.Criterion;
using NUnit.Framework;

namespace Eureca.Professional.ActiveRecordsTests
{
    [TestFixture]
    public class QuestionTestFixture : ActiveRecordTestFixtureBase
    {
        #region Setup/Teardown

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            ActiveRecordBase<Question>.DeleteAll();
            ActiveRecordBase<Answer>.DeleteAll();
            ActiveRecordBase<Content>.DeleteAll();
        }

        #endregion

        private void Selection(bool toCreateScope)
        {
            var question = new Question(new Content("Mega"));
            question.Create();
            var question2 = new Question(new Content("Mega2"));
            question2.Create();

            if (toCreateScope) CreateScope(FlushAction.Never);

            Question[] questions = ActiveRecordBase<Question>.FindAll();

            Assert.AreEqual(questions.Length, 2);

            foreach (Question a in questions)
            {
                Debug.WriteLine("Question: " + a.Content.PlainContent);
            }

            if (toCreateScope) CloseScope();
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void ContentIsNull()
        {
            new Question(null, null);
        }

        [Test]
        public void Creation()
        {
            var content = new Content("SomeContent");
            var question = new Question(content, 1394);
            question.Create();

            Assert.AreEqual(question.TimeLimit, 1394);
        }

        [Test]
        public void Deletion()
        {
            var content = new Content("SomeContent");
            var question = new Question(content, null);
            question.Create();

            int id = question.Id;

            question.Delete();

            Assert.AreEqual(ActiveRecordBase<Question>.FindOne(Expression.Eq("Id", id)), null);
        }

        [Test]
        public void Modification()
        {
            var content = new Content("SomeContent");
            content.Create();
            var content2 = new Content("SomeContent2");
            content2.Create();

            var question = new Question(content, null);
            question.Create();

            Assert.AreEqual(question.Content.PlainContent, "SomeContent");
            Assert.AreEqual(question.TimeLimit, null);

            question.Content = content2;
            question.TimeLimit = 512;
            question.Update();

            CreateScope(FlushAction.Never);

            question.Refresh();

            Assert.AreEqual(question.Content.PlainContent, "SomeContent2");
            Assert.AreEqual(question.TimeLimit, 512);

            question.Content.PlainContent = "SomeContent3";
            question.Update();
            question.Refresh();

            Assert.AreEqual(question.Content.PlainContent, "SomeContent3");

            CloseScope();
        }

        [Test]
        public void SelectionWithScope()
        {
            Selection(true);
        }

        [Test]
        [ExpectedException(typeof (LazyInitializationException))]
        public void SelectionWithoutScope()
        {
            Selection(false);
        }
    }
}
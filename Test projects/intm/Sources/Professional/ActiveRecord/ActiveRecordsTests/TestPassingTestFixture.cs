﻿using System;
using Castle.ActiveRecord;
using Eureca.Professional.ActiveRecords;
using NHibernate.Criterion;
using NUnit.Framework;

namespace Eureca.Professional.ActiveRecordsTests
{
    [TestFixture]
    public class TestPassingTestFixture : ActiveRecordTestFixtureBase
    {
        #region Setup/Teardown

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            ActiveRecordBase<TestPassing>.DeleteAll();
            ActiveRecordBase<ProfilePassing>.DeleteAll();
            ActiveRecordBase<Test>.DeleteAll();
            ActiveRecordBase<Method>.DeleteAll();
            ActiveRecordBase<TestType>.DeleteAll();
            ActiveRecordBase<Content>.DeleteAll();
            ActiveRecordBase<Probationer>.DeleteAll();
            ActiveRecordBase<Profile>.DeleteAll();
            ActiveRecordBase<TimeInterval>.DeleteAll();
        }

        #endregion

        private static Test NewTest()
        {
            return new Test(
                new TestType("Type", "PlugIn", false),
                new Method("Method", new Content("Form")),
                "Test",
                null, 80, 5,
                new Content("Description"),
                null
                );
        }

        [Test]
        public void CreationSelectionDeletion()
        {
            var passing = new TestPassing(NewTest(),
                                          new Probationer(
                                              new User("1", "123", "123", DateTime.Now,
                                                        "1"), new Profile("Profile")),
                                          new TimeInterval(DateTime.Now, DateTime.Now + TimeSpan.FromHours(1)),false);
            passing.Create();

            Assert.Greater(passing.Id, 0);

            CreateScope(FlushAction.Never);

            passing = ActiveRecordBase<TestPassing>.Find(passing.Id);

            Assert.AreEqual(passing.Test.Description.PlainContent, "Description");

            CloseScope();

            passing.Delete();

            Assert.AreEqual(ActiveRecordBase<TestPassing>.FindOne(Expression.Eq("Id", passing.Id)), null);
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void ProbationerIsNull()
        {
            new TestPassing(NewTest(), null,
                            new TimeInterval(DateTime.Now, DateTime.Now + TimeSpan.FromHours(1)),false);
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void ProfileIsNull()
        {
            new TestPassing(NewTest(), null,
                            new Probationer(
                                new User("1", "123", "123",  DateTime.Now,  "1"),
                                new Profile("Profile")),
                            new TimeInterval(DateTime.Now, DateTime.Now + TimeSpan.FromHours(1)),false);
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void TestIsNull()
        {
            new TestPassing(null,
                            new Probationer(
                                new User("1", "123", "123",  DateTime.Now,  "1"),
                                new Profile("Profile")),
                            new TimeInterval(DateTime.Now, DateTime.Now + TimeSpan.FromHours(1)),false);
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void TimeIntervalIsNull()
        {
            new TestPassing(NewTest(),
                            new Probationer(
                                new User("1", "123", "123",  DateTime.Now,  "1"),
                                new Profile("Profile")), null,false);
        }
    }
}
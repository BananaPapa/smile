﻿using System.Diagnostics;
using Castle.ActiveRecord;
using Castle.ActiveRecord.Framework;
using Eureca.Professional.ActiveRecords;
using NHibernate.Criterion;
using NUnit.Framework;

namespace Eureca.Professional.ActiveRecordsTests
{
    [TestFixture]
    public class ScaleTestFixture : ActiveRecordTestFixtureBase
    {
        #region Setup/Teardown

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            ActiveRecordBase<Scale>.DeleteAll();
            ActiveRecordBase<Test>.DeleteAll();
            ActiveRecordBase<Method>.DeleteAll();
            ActiveRecordBase<TestType>.DeleteAll();
            ActiveRecordBase<Content>.DeleteAll();
        }

        #endregion

        [Test]
        public void CreationSelectionModificationDeletion()
        {
            var scale = new Scale("Scale");
            scale.Create();

            Assert.Greater(scale.Id, 0);

            scale.Abbreviation = "ABBR";
            scale.Update();

            scale.Refresh();
            Assert.AreEqual(scale.Abbreviation, "ABBR");

            int id = scale.Id;

            scale.Delete();

            Assert.AreEqual(ActiveRecordBase<Scale>.FindOne(Expression.Eq("Id", id)), null);
        }

        [Test]
        public void TestsCollection()
        {
            var scale = new Scale("Scale");
            scale.Create();

            CreateScope(FlushAction.Never);

            scale.Refresh();

            scale.Tests.Add(new Test(
                                new TestType("Type", "PlugIn", false),
                                new Method("Method", new Content("Form")),
                                "Test",
                                null, 80, 5,
                                new Content("Description"),
                                null
                                ));
            scale.Update();

            CloseScope();

            CreateScope(FlushAction.Never);

            scale.Refresh();

            Assert.AreEqual(scale.Tests.Count, 1);

            Debug.WriteLine("Scale test: " + scale.Tests[0].Title);
            Debug.WriteLine("Scale test method: " + scale.Tests[0].Method.Instruction.PlainContent);

            scale.Delete();

            CloseScope();
        }

        [Test]
        [ExpectedException(typeof (ActiveRecordException))]
        public void UniqueName()
        {
            var scale = new Scale("Scale");
            scale.Create();

            var scale2 = new Scale("Scale");
            scale2.Create();
        }
    }
}
﻿using System;
using Castle.ActiveRecord;
using Castle.ActiveRecord.Framework;
using Eureca.Professional.ActiveRecords;
using NHibernate.Criterion;
using NUnit.Framework;

namespace Eureca.Professional.ActiveRecordsTests
{
    [TestFixture]
    public class UserTestFixture : ActiveRecordTestFixtureBase
    {
        #region Setup/Teardown

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            ActiveRecordBase<Test>.DeleteAll();
            ActiveRecordBase<Method>.DeleteAll();
            ActiveRecordBase<TestType>.DeleteAll();
            ActiveRecordBase<Content>.DeleteAll();
            ActiveRecordBase<User>.DeleteAll();
        }

        #endregion

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void ArgumentIsNull()
        {
            new User(null, null, null,  DateTime.Now,  null);
        }

        [Test]
        public void CreationSelectionModificationDeletion()
        {
            var User = new User("FirstName", "middleName", "lastName",  DateTime.Now,  "password");
            User.Create();

            Assert.Greater(User.Id, 0);

            CreateScope(FlushAction.Never);

            User = ActiveRecordBase<User>.Find(User.Id);

            Assert.AreEqual(User.FirstName, "FirstName");
            Assert.AreEqual(User.MiddleName, "middleName");
            Assert.AreEqual(User.LastName, "lastName");
            Assert.AreEqual(User.Password, "password");


            CloseScope();


            CreateScope(FlushAction.Never);

            User = ActiveRecordBase<User>.Find(User.Id);
            var users = ActiveRecordBase<User>.FindAll();

            Assert.AreEqual(User.FirstName, "FirstName");
            Assert.AreEqual(User.MiddleName, "middleName");
            Assert.AreEqual(User.LastName, "lastName");
            Assert.AreEqual(User.Password, "password");


            CloseScope();

            User.Delete();

            Assert.AreEqual(ActiveRecordBase<User>.FindOne(Expression.Eq("Id", User.Id)), null);
        }

       
    }
}
﻿using System;
using Castle.ActiveRecord;
using Castle.ActiveRecord.Framework;
using Eureca.Professional.ActiveRecords;
using NHibernate.Criterion;
using NUnit.Framework;

namespace Eureca.Professional.ActiveRecordsTests
{
    [TestFixture]
    public class ProfileTestFixture : ActiveRecordTestFixtureBase
    {
        #region Setup/Teardown

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            ActiveRecordBase<Test>.DeleteAll();
            ActiveRecordBase<Method>.DeleteAll();
            ActiveRecordBase<TestType>.DeleteAll();
            ActiveRecordBase<Content>.DeleteAll();
            ActiveRecordBase<Profile>.DeleteAll();
        }

        #endregion

        [Test]
        public void CreationSelectionModificationDeletion()
        {
            var profile = new Profile("Title");
            profile.Create();

            Assert.Greater(profile.Id, 0);

            CreateScope(FlushAction.Never);

            profile = ActiveRecordBase<Profile>.Find(profile.Id);

            Assert.AreEqual(profile.Title, "Title");

            profile.Description = "Description";
            profile.Tests.Add(new Test(
                                  new TestType("Type", "PlugIn", false),
                                  new Method("Method", new Content("Form")),
                                  "Test",
                                  null, 80, 5,
                                  new Content("Description"),
                                  null
                                  ));

            profile.Update();

            CloseScope();

            CreateScope(FlushAction.Never);

            profile = ActiveRecordBase<Profile>.Find(profile.Id);

            Assert.AreEqual(profile.Description, "Description");
            Assert.AreEqual(profile.Tests.Count, 1);
            Assert.AreEqual(profile.Tests[0].Description.PlainContent, "Description");

            CloseScope();

            profile.Delete();

            Assert.AreEqual(ActiveRecordBase<Profile>.FindOne(Expression.Eq("Id", profile.Id)), null);
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void TitleIsNull()
        {
            new Profile(null);
        }

        [Test]
        [ExpectedException(typeof (ActiveRecordException))]
        public void TitleUniqueness()
        {
            var profile = new Profile("Title");
            profile.Create();

            var profile2 = new Profile("Title");
            profile2.Create();
        }
    }
}
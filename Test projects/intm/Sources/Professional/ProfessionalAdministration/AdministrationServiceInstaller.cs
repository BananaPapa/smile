﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;

namespace Eureca.Professional.ProfessionalAdministration
{
    [RunInstaller(true)]
    public partial class AdministrationServiceInstaller : System.Configuration.Install.Installer
    {
        public AdministrationServiceInstaller()
        {
            InitializeComponent();
        }
    }
}

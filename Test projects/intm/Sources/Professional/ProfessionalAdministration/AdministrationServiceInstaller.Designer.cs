﻿namespace Eureca.Professional.ProfessionalAdministration
{
    partial class AdministrationServiceInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._serviceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this._serviceIntaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // _serviceProcessInstaller
            // 
            this._serviceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalService;
            this._serviceProcessInstaller.Password = null;
            this._serviceProcessInstaller.Username = null;
            // 
            // _serviceIntaller
            // 
            this._serviceIntaller.Description = "Переодичски создаёт бэкап бызы согласно настройкам.";
            this._serviceIntaller.DisplayName = "Служба авто бэкапа базы СПО профессионал";
            this._serviceIntaller.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this._serviceProcessInstaller});
            this._serviceIntaller.ServiceName = "AdministrationService";
            this._serviceIntaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // AdministrationServiceInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this._serviceIntaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller _serviceProcessInstaller;
        private System.ServiceProcess.ServiceInstaller _serviceIntaller;
    }
}
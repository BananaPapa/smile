﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using Eureca.Professional.CommonClasses;
using log4net;


namespace Eureca.Professional.ProfessionalAdministration
{
    /// <summary>
    /// класс создания бэкап копии БД
    /// </summary>
    internal class BackUpDb
    {
        private static readonly ILog Log = LogManager.GetLogger( typeof( BackUpDb ) );

        #region Delegates

        /// <summary>
        /// делегат события успешного создания бэкапа
        /// </summary>
        public delegate void BackUpSuccessEventDelegate();
        /// <summary>
        /// делегат на ошибку создания копии БД
        /// </summary>
        /// <param name="msg"></param>
        public delegate void BackUpErrorEventDelegate(string msg);

        #endregion

        /// <summary>
        /// строка подключения к БД
        /// </summary>
        private readonly string _connectionString;
        /// <summary>
        /// имя файла бэкап копии
        /// </summary>
        private string _dbFileName;
        /// <summary>
        /// директория для файла БД
        /// </summary>
        private string _dbFilePath;

        public string DbName { get; private set; }

        /// <summary>
        /// конструктор
        /// </summary>
        /// <param name="connectionString">строка подключения</param>
        public BackUpDb(string connectionString)
        {
            _connectionString = connectionString;
            var conn = new SqlConnection(_connectionString);
            conn.Open();
            if (conn.State != ConnectionState.Open)
            {
                throw new Exception("Ошибка подключения к БД " + _connectionString);
            }
            DbName = conn.Database;
            conn.Close();
        }

        /// <summary>
        /// событие успешного создания бэкап копии БД
        /// </summary>
        public event BackUpSuccessEventDelegate OnBackUpSuccess;
        /// <summary>
        /// событие на ошибку при создании копии БД
        /// </summary>
        public event BackUpErrorEventDelegate ObBackUpError;
       


        /// <summary>
        /// метод вызова создания бэкапа
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="dbFilesDir"></param>
        public void CreateBackUp(string fileName, string dbFilesDir)
        {
            _dbFileName = fileName;
            _dbFilePath = dbFilesDir;
            var backUpDb = new Thread(BackUpDbMethod) {Name = "BackUpDBThread"};
            backUpDb.Start();
        }

        /// <summary>
        /// метод потока создания копии БД
        /// </summary>
        private void BackUpDbMethod()
        {
            try
            {
                var conn = new SqlConnection(_connectionString);
                var cmd = new SqlCommand();
                conn.Open();
                if (conn.State != ConnectionState.Open)
                {
                    string str = "Ошибка подключения к серверу баз данных " + _connectionString;
                    Log.Error(str);
                    RaiseError(str);
                }

                cmd.Connection = conn;
                cmd.CommandTimeout = 3600;

                //запускаем процесс архивирования БД

                string query = string.Format("BACKUP DATABASE [{1}] TO  DISK = N'{0}' " +
                                             "WITH NOFORMAT, INIT,  NAME = N'{1} full database BackUp', " +
                                             "SKIP, NOREWIND, NOUNLOAD,  STATS = 10",
                                             _dbFilePath + @"\" + _dbFileName,
                                             conn.Database);


                cmd.CommandText = query;

                try
                {
                    cmd.ExecuteNonQuery();
                    Log.Error("Успешное создание копии БД " + conn.Database + " в директорию " + _dbFilePath + @"\" + _dbFileName);
                    if (OnBackUpSuccess != null)
                        OnBackUpSuccess();
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                    RaiseError(ex.Message);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                RaiseError(ex.Message);
            }
        }
        private void RaiseError(string msg)
        {
            if (ObBackUpError != null)
            {
                ObBackUpError(msg);
            }
        }
    }
}
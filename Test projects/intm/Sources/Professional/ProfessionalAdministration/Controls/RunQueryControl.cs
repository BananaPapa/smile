﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Windows.Forms;
using Eureca.Professional.CommonClasses;
using Eureca.Professional.CommonClasses.Data;
using log4net;

namespace Eureca.Professional.ProfessionalAdministration
{
    public partial class RunQueryControl : BaseTestingControl
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof (RunQueryControl));
        private string _connectionString;

        public RunQueryControl(string connectionString)
        {
            _connectionString = connectionString;
            InitializeComponent();
        }

        public override void InitControl()
        {
            base.InitControl();
            CaptionControl = "Выполнение запросов";
        }

        private void memoEdit1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                Run();
            }
        }

        private void Run()
        {
            try
            {
                String query = memoEdit1.Text;
                DataTable tbl = DBExecutor.SelectQuerry(query);
                dataGridView1.DataSource = tbl;
            }
            catch (Exception ex)
            {
                MessageForm.ShowErrorMsg("Ошибка sql выборки\n");
                Log.Error(ex);
            }
        }

        private void SimpleButtonRunClick(object sender, EventArgs e)
        {
            Run();
        }

        private void DataGridView1DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            Log.Error(e);
        }

        private void SimpleButtonSelectFileClick(object sender, EventArgs e)
        {
            RunQueryFromFile();
        }

        /// <summary>
        /// метод запускает выполнение скриптов из файла
        /// </summary>
        private static void RunQueryFromFile()
        {
            string path;
            String successed = "";

            //открываем файлы
            var fbd = new FolderBrowserDialog {Description = @"Открыть папку с обновлениями"};

            if (fbd.ShowDialog() == DialogResult.OK)
            {
                path = fbd.SelectedPath;
            }
            else
            {
                return;
            }
            //пробегаемся по всем файлам и получаем список из имен
            string[] files = Directory.GetFiles(path);
            var res = new Dictionary<int, string>();
            var sorting = new List<int>();
            //получили все файлы нашего формата (наименование файла есть номер)
            foreach (string str in files)
            {
                string filename = DigitsOnly(Path.GetFileName(str));

                int r;
                if (int.TryParse(filename, out r))
                {
                    res.Add(r, str);
                    sorting.Add(r);
                }
            }

            //производим сортировку файлов для того чтобы выполнять обновления упорядоченно
            sorting.Sort();
            //каждый файл запускаем на выполнение
            foreach (int i in sorting)
            {
                string filename = res[i];
                bool succesed = ExecFile(filename);

                if (succesed)
                {
                    Log.Error("sql-script " + filename + " applied successfully.");
                    successed += "Обновление " + Path.GetFileName(filename) + " выполнено успешно\n";
                }
                else
                {
                    successed += "Обновление " + Path.GetFileName(filename) + " выполнено с ошибками\n";
                }
            }
            MessageForm.ShowOkMsg(string.IsNullOrEmpty(successed)
                                      ? "Не найдено ни одного файла необходимого формата."
                                      : successed);
        }

        /// <summary>
        /// метод выполнения запроса из файла
        /// </summary>
        /// <param name="fileName">путь к файлу</param>
        /// <returns>результат выполнения</returns>
        private static bool ExecFile(string fileName)
        {
            string sqlStr = string.Empty;
            bool succesed = true;
            try
            {
                //FileStream cryptedStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
                //						RijndaelManaged RMCrypto = new RijndaelManaged();
                //						CryptoStream csDecrypted = new CryptoStream(cryptedStream, RMCrypto.CreateDecryptor(Key, IV),CryptoStreamMode.Read);
                //						StreamReader sr = new StreamReader(csDecrypted,Encoding.UTF8);

                //открываем  и читаем файл
                var sr = new StreamReader(fileName, Encoding.Default);
                String line;
                while ((line = sr.ReadLine()) != null)
                {
                    sqlStr += line.Trim() + " \n";
                    //Log.Error(line);
                }
                sr.Close();
            }
            catch (Exception ex)
            {
                succesed = false;
                MessageBox.Show(@"Ошибка чтения файла: ");
                Log.Error(ex);
                return false;
            }
            //для каждого блока делаем выполнение
            int pos = -1; //sqlStr.ToLower().IndexOf(" go ");
            if (pos < 0)
            {
                pos = sqlStr.ToLower().IndexOf("\ngo ");
            }
            if (sqlStr.ToLower().IndexOf("go ") == 0)
            {
                pos = 0;
            }
            String errStr = "Ошибка выполнения команды в файле:\n" +
                            fileName;
            while (pos >= 0 || sqlStr.Length > 0)
            {
                if (pos < 0)
                {
                    pos = sqlStr.Length;
                }
                string str = sqlStr.Substring(0, pos);
                sqlStr = sqlStr.Remove(0, Math.Min(pos + 3, sqlStr.Length));
                pos = -1; //sqlStr.ToLower().IndexOf(" go ");
                if (pos < 0)
                {
                    pos = sqlStr.ToLower().IndexOf("\ngo ");
                }
                if (sqlStr.ToLower().IndexOf("go ") == 0)
                {
                    pos = 0;
                }
                try
                {
                    int ret = DBExecutor.ExecNonQuery(str);
                    if (ret < -1)
                    {
                        succesed = false;
                        Log.Error("sql-script " + fileName + " error");
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                    return false;
                }
            }
            return succesed;
        }

        /// <summary>
        /// получение первых цифр из строки
        /// </summary>
        /// <param name="inVal"></param>
        /// <returns></returns>
        public static string DigitsOnly(string inVal)
        {
            List<char> newPhon = new List<char>();
            //int i = 0;
            foreach (char c in inVal)
            {
                if (c.CompareTo('0') > 0 && c.CompareTo('9') < 0)
                    newPhon.Add(c);
                else
                {
                    return new string(newPhon.ToArray());
                }
            }
            return new string(newPhon.ToArray());
        }
    }
}
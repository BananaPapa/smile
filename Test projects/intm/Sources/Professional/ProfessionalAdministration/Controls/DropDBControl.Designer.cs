﻿namespace Eureca.Professional.ProfessionalAdministration
{
    partial class DropDBControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.simpleButtonClose = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.marqueeProgressBarControl1 = new DevExpress.XtraEditors.MarqueeProgressBarControl();
            this.simpleButtonCreateDB = new DevExpress.XtraEditors.SimpleButton();
            this.textEditDBName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButtonOpenDBFilesDirectory = new DevExpress.XtraEditors.SimpleButton();
            this.textEditDbFilesDirectory = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.comboBoxEdit1 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.marqueeProgressBarControl1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDBName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDbFilesDirectory.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButtonClose
            // 
            this.simpleButtonClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonClose.Location = new System.Drawing.Point(392, 101);
            this.simpleButtonClose.Name = "simpleButtonClose";
            this.simpleButtonClose.Size = new System.Drawing.Size(85, 43);
            this.simpleButtonClose.TabIndex = 20;
            this.simpleButtonClose.Text = "Закрыть";
            this.simpleButtonClose.Click += new System.EventHandler(this.simpleButtonClose_Click);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.marqueeProgressBarControl1);
            this.panelControl1.Location = new System.Drawing.Point(2, 87);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(293, 57);
            this.panelControl1.TabIndex = 19;
            this.panelControl1.Visible = false;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(5, 6);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(250, 26);
            this.labelControl4.TabIndex = 11;
            this.labelControl4.Text = "Идет процесс создания архивной копии БД\r\nи удаления экземпляра БД с сервера баз д" +
    "анных";
            // 
            // marqueeProgressBarControl1
            // 
            this.marqueeProgressBarControl1.EditValue = 0;
            this.marqueeProgressBarControl1.Location = new System.Drawing.Point(5, 34);
            this.marqueeProgressBarControl1.Name = "marqueeProgressBarControl1";
            this.marqueeProgressBarControl1.Size = new System.Drawing.Size(283, 18);
            this.marqueeProgressBarControl1.TabIndex = 10;
            // 
            // simpleButtonCreateDB
            // 
            this.simpleButtonCreateDB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonCreateDB.Location = new System.Drawing.Point(300, 101);
            this.simpleButtonCreateDB.Name = "simpleButtonCreateDB";
            this.simpleButtonCreateDB.Size = new System.Drawing.Size(85, 43);
            this.simpleButtonCreateDB.TabIndex = 18;
            this.simpleButtonCreateDB.Text = "Удалить БД";
            this.simpleButtonCreateDB.Click += new System.EventHandler(this.simpleButtonCreateDB_Click);
            // 
            // textEditDBName
            // 
            this.textEditDBName.EditValue = "";
            this.textEditDBName.Location = new System.Drawing.Point(122, 61);
            this.textEditDBName.Name = "textEditDBName";
            this.textEditDBName.Size = new System.Drawing.Size(320, 20);
            this.textEditDBName.TabIndex = 17;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(2, 64);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(108, 13);
            this.labelControl3.TabIndex = 16;
            this.labelControl3.Text = "Наименование файла";
            // 
            // simpleButtonOpenDBFilesDirectory
            // 
            this.simpleButtonOpenDBFilesDirectory.Location = new System.Drawing.Point(448, 32);
            this.simpleButtonOpenDBFilesDirectory.Name = "simpleButtonOpenDBFilesDirectory";
            this.simpleButtonOpenDBFilesDirectory.Size = new System.Drawing.Size(29, 23);
            this.simpleButtonOpenDBFilesDirectory.TabIndex = 15;
            this.simpleButtonOpenDBFilesDirectory.Text = "...";
            // 
            // textEditDbFilesDirectory
            // 
            this.textEditDbFilesDirectory.Enabled = false;
            this.textEditDbFilesDirectory.Location = new System.Drawing.Point(122, 35);
            this.textEditDbFilesDirectory.Name = "textEditDbFilesDirectory";
            this.textEditDbFilesDirectory.Size = new System.Drawing.Size(320, 20);
            this.textEditDbFilesDirectory.TabIndex = 14;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(2, 32);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(85, 26);
            this.labelControl2.TabIndex = 13;
            this.labelControl2.Text = "Путь для backUp \r\nфайлов БД";
            // 
            // comboBoxEdit1
            // 
            this.comboBoxEdit1.Location = new System.Drawing.Point(122, 9);
            this.comboBoxEdit1.Name = "comboBoxEdit1";
            this.comboBoxEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit1.Size = new System.Drawing.Size(320, 20);
            this.comboBoxEdit1.TabIndex = 21;
            this.comboBoxEdit1.SelectedIndexChanged += new System.EventHandler(this.comboBoxEdit1_SelectedIndexChanged);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(3, 12);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(98, 13);
            this.labelControl1.TabIndex = 22;
            this.labelControl1.Text = "Существующие БД";
            // 
            // DropDBControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.comboBoxEdit1);
            this.Controls.Add(this.simpleButtonClose);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.simpleButtonCreateDB);
            this.Controls.Add(this.textEditDBName);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.simpleButtonOpenDBFilesDirectory);
            this.Controls.Add(this.textEditDbFilesDirectory);
            this.Controls.Add(this.labelControl2);
            this.MinimumSize = new System.Drawing.Size(489, 158);
            this.Name = "DropDBControl";
            this.Size = new System.Drawing.Size(489, 158);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.marqueeProgressBarControl1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDBName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDbFilesDirectory.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit1.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton simpleButtonClose;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.MarqueeProgressBarControl marqueeProgressBarControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCreateDB;
        private DevExpress.XtraEditors.TextEdit textEditDBName;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.SimpleButton simpleButtonOpenDBFilesDirectory;
        private DevExpress.XtraEditors.TextEdit textEditDbFilesDirectory;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
    }
}

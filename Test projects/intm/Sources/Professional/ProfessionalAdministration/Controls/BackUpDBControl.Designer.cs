﻿namespace Eureca.Professional.ProfessionalAdministration
{
    partial class BackUpDbControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.simpleButtonClose = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.marqueeProgressBarControl1 = new DevExpress.XtraEditors.MarqueeProgressBarControl();
            this.simpleButtonCreateDB = new DevExpress.XtraEditors.SimpleButton();
            this.textEditDBName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButtonOpenDBFilesDirectory = new DevExpress.XtraEditors.SimpleButton();
            this.textEditDbFilesDirectory = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.textEditTimerInterval = new DevExpress.XtraEditors.TextEdit();
            this.checkEditEnableBackUpTimer = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.marqueeProgressBarControl1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDBName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDbFilesDirectory.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTimerInterval.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditEnableBackUpTimer.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButtonClose
            // 
            this.simpleButtonClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonClose.Location = new System.Drawing.Point(374, 108);
            this.simpleButtonClose.Name = "simpleButtonClose";
            this.simpleButtonClose.Size = new System.Drawing.Size(107, 43);
            this.simpleButtonClose.TabIndex = 20;
            this.simpleButtonClose.Text = "Закрыть";
            this.simpleButtonClose.Click += new System.EventHandler(this.SimpleButtonCloseClick);
            // 
            // panelControl1
            // 
            this.panelControl1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.marqueeProgressBarControl1);
            this.panelControl1.Location = new System.Drawing.Point(5, 103);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(237, 48);
            this.panelControl1.TabIndex = 19;
            this.panelControl1.Visible = false;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(5, 6);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(220, 13);
            this.labelControl4.TabIndex = 11;
            this.labelControl4.Text = "Идет процесс создания архивной копии БД";
            // 
            // marqueeProgressBarControl1
            // 
            this.marqueeProgressBarControl1.EditValue = 0;
            this.marqueeProgressBarControl1.Location = new System.Drawing.Point(5, 25);
            this.marqueeProgressBarControl1.Name = "marqueeProgressBarControl1";
            this.marqueeProgressBarControl1.Size = new System.Drawing.Size(220, 18);
            this.marqueeProgressBarControl1.TabIndex = 10;
            // 
            // simpleButtonCreateDB
            // 
            this.simpleButtonCreateDB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonCreateDB.Location = new System.Drawing.Point(262, 108);
            this.simpleButtonCreateDB.Name = "simpleButtonCreateDB";
            this.simpleButtonCreateDB.Size = new System.Drawing.Size(106, 43);
            this.simpleButtonCreateDB.TabIndex = 18;
            this.simpleButtonCreateDB.Text = "Создать копию";
            this.simpleButtonCreateDB.Click += new System.EventHandler(this.SimpleButtonCreateDbClick);
            // 
            // textEditDBName
            // 
            this.textEditDBName.EditValue = "";
            this.textEditDBName.Location = new System.Drawing.Point(125, 38);
            this.textEditDBName.Name = "textEditDBName";
            this.textEditDBName.Size = new System.Drawing.Size(320, 20);
            this.textEditDBName.TabIndex = 17;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(5, 41);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(108, 13);
            this.labelControl3.TabIndex = 16;
            this.labelControl3.Text = "Наименование файла";
            // 
            // simpleButtonOpenDBFilesDirectory
            // 
            this.simpleButtonOpenDBFilesDirectory.Location = new System.Drawing.Point(451, 9);
            this.simpleButtonOpenDBFilesDirectory.Name = "simpleButtonOpenDBFilesDirectory";
            this.simpleButtonOpenDBFilesDirectory.Size = new System.Drawing.Size(29, 23);
            this.simpleButtonOpenDBFilesDirectory.TabIndex = 15;
            this.simpleButtonOpenDBFilesDirectory.Text = "...";
            this.simpleButtonOpenDBFilesDirectory.Visible = false;
            // 
            // textEditDbFilesDirectory
            // 
            this.textEditDbFilesDirectory.Enabled = false;
            this.textEditDbFilesDirectory.Location = new System.Drawing.Point(125, 12);
            this.textEditDbFilesDirectory.Name = "textEditDbFilesDirectory";
            this.textEditDbFilesDirectory.Size = new System.Drawing.Size(320, 20);
            this.textEditDbFilesDirectory.TabIndex = 14;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(5, 9);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(85, 26);
            this.labelControl2.TabIndex = 13;
            this.labelControl2.Text = "Путь для backUp \r\nфайлов БД";
            // 
            // textEditTimerInterval
            // 
            this.textEditTimerInterval.EditValue = "24";
            this.textEditTimerInterval.Location = new System.Drawing.Point(397, 64);
            this.textEditTimerInterval.Name = "textEditTimerInterval";
            this.textEditTimerInterval.Properties.ValidateOnEnterKey = true;
            this.textEditTimerInterval.Size = new System.Drawing.Size(48, 20);
            this.textEditTimerInterval.TabIndex = 22;
            // 
            // checkEditEnableBackUpTimer
            // 
            this.checkEditEnableBackUpTimer.Location = new System.Drawing.Point(8, 65);
            this.checkEditEnableBackUpTimer.Name = "checkEditEnableBackUpTimer";
            this.checkEditEnableBackUpTimer.Properties.Caption = "Запускать создание копии БД через каждый интервал времени, часы";
            this.checkEditEnableBackUpTimer.Size = new System.Drawing.Size(383, 18);
            this.checkEditEnableBackUpTimer.TabIndex = 23;
            // 
            // BackUpDbControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.checkEditEnableBackUpTimer);
            this.Controls.Add(this.textEditTimerInterval);
            this.Controls.Add(this.simpleButtonClose);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.simpleButtonCreateDB);
            this.Controls.Add(this.textEditDBName);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.simpleButtonOpenDBFilesDirectory);
            this.Controls.Add(this.textEditDbFilesDirectory);
            this.Controls.Add(this.labelControl2);
            this.MinimumSize = new System.Drawing.Size(485, 112);
            this.Name = "BackUpDbControl";
            this.Size = new System.Drawing.Size(485, 156);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.marqueeProgressBarControl1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDBName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDbFilesDirectory.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTimerInterval.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditEnableBackUpTimer.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton simpleButtonClose;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.MarqueeProgressBarControl marqueeProgressBarControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCreateDB;
        private DevExpress.XtraEditors.TextEdit textEditDBName;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.SimpleButton simpleButtonOpenDBFilesDirectory;
        private DevExpress.XtraEditors.TextEdit textEditDbFilesDirectory;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit textEditTimerInterval;
        private DevExpress.XtraEditors.CheckEdit checkEditEnableBackUpTimer;
    }
}

﻿using System;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using Eureca.Professional.BaseComponents.Configuration;
using Eureca.Professional.CommonClasses;
using Eureca.Professional.CommonClasses.Configuration;
using EurecaCorp.Professional.BaseComponents.Configuration;
using log4net;


namespace Eureca.Professional.ProfessionalAdministration
{
    /// <summary>
    /// контрол создания бэкап копии БД
    /// </summary>
    public partial class BackUpDbControl : BaseTestingControl
    {
        private static readonly ILog Log = LogManager.GetLogger( typeof( Program ) );

        /// <summary>
        /// настройки администратора
        /// </summary>
        private AdministrationSettingsSection _ad = AdministrationSettingsSection.GetFromConfigSource();


        /// <summary>
        /// строка подключения
        /// </summary>
        private readonly string _connectionString;

        /// <summary>
        /// настройки БД
        /// </summary>
        private readonly DatabaseSettingsSection _db = DatabaseSettingsSection.GetFromConfigSource();


        /// <summary>
        /// конструктор
        /// </summary>
        public BackUpDbControl()
        {
            InitializeComponent();
            if (!Directory.Exists(_ad.DBBackUpFilesDirectory))
            {
                try
                {
                    Directory.CreateDirectory(_ad.DBBackUpFilesDirectory);
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                    MessageForm.ShowErrorMsg(ex.Message);
                }
            }
            _connectionString = _db.ConnectionString;
            textEditDbFilesDirectory.Text = _ad.DBBackUpFilesDirectory;
            textEditTimerInterval.Text = _ad.BackUpTimerInterval.ToString();
            checkEditEnableBackUpTimer.Checked = _ad.EnableBackUpTimer;
        }

        /// <summary>
        /// метод инициализации контрола 
        /// </summary>
        public override void InitControl()
        {
            base.InitControl();
            CaptionControl = "Архивирование БД";
            DateTime dt = DateTime.Now;
            var conn = new SqlConnection(_connectionString);
            conn.Open();
            textEditDBName.Text += string.Format("{3}_{0}_{1}_{2}.bak", dt.Year, dt.Month, dt.Day, conn.Database);
            conn.Close();
        }

        /// <summary>
        /// метод включает пользовательский интерфейс
        /// </summary>
        private void EnableButtons()
        {
            try
            {
                MethodInvoker method = delegate
                                           {
                                               simpleButtonCreateDB.Enabled = true;
                                               simpleButtonClose.Enabled = true;
                                               panelControl1.Visible = false;
                                           };
                Invoke(method);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }

        /// <summary>
        /// нажатие на кнопку закрыть
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SimpleButtonCloseClick(object sender, EventArgs e)
        {
            if (ParentForm != null && SaveTimerSettings())
                ParentForm.Close();
        }

        /// <summary>
        /// нажатие на кнопку создать бэкап копию БД
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SimpleButtonCreateDbClick(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textEditDbFilesDirectory.Text.Trim()))
            {
                MessageForm.ShowErrorMsg("Необходимо указать путь для  BackUp копии БД");
                return;
            }
            if (string.IsNullOrEmpty(textEditDBName.Text.Trim()))
            {
                MessageForm.ShowErrorMsg("Необходимо указать наименование файла БД");
                return;
            }
            string fileName = textEditDbFilesDirectory.Text + @"\" + textEditDBName.Text;
            if (File.Exists(fileName))
            {
                if (
                    MessageForm.ShowYesNoQuestionMsg(string.Format("Данный файл {0} существует. \nЗаменить? ", fileName)) ==
                    DialogResult.No)
                {
                    return;
                }
            }

            panelControl1.Visible = true;

            simpleButtonCreateDB.Enabled = false;
            simpleButtonClose.Enabled = false;
            try
            {
                var back = new BackUpDb(_connectionString);
                back.OnBackUpSuccess += BackOnBackUpSuccess;
                back.ObBackUpError += BackObBackUpError;
                back.CreateBackUp(textEditDBName.Text.Trim(), textEditDbFilesDirectory.Text.Trim());
            }
            catch (Exception ex)
            {
                MessageForm.ShowErrorMsg(ex.Message);
                Log.Error(ex);
            }
        }

        /// <summary>
        /// обработчик события ошибки от класса создания копии БД
        /// </summary>
        /// <param name="msg"></param>
        void BackObBackUpError(string msg)
        {
            MessageForm.ShowErrorMsg(msg);
            EnableButtons();
        }

      

        /// <summary>
        /// обработчик события успешного завершения бэкапа
        /// </summary>
        private void BackOnBackUpSuccess()
        {
            MessageForm.ShowOkMsg("Архивация произошла успешно");
            EnableButtons();
        }

       
       


        private bool SaveTimerSettings()
        {
           
            //если некорректное значение
            int r =0;
            int.TryParse(textEditTimerInterval.Text,out r);
            if ( r <= 0)
            {
                MessageForm.ShowErrorMsg("Временной интервал должен быть больше 0");
                return false;
            }
            //выставляем настройки таймер
            _ad.EnableBackUpTimer = checkEditEnableBackUpTimer.Checked;
            _ad.BackUpTimerInterval = int.Parse(textEditTimerInterval.Text);


            Settings.Save();
            return true;
        }

       
    }
}
﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using Eureca.Professional.BaseComponents;
using Eureca.Professional.BaseComponents.Configuration;
using Eureca.Professional.CommonClasses;
using Eureca.Professional.CommonClasses.Data;
using EurecaCorp.Professional.BaseComponents.Configuration;
using log4net;

namespace Eureca.Professional.ProfessionalAdministration
{
    public partial class DropDBControl : BaseTestingControl
    {
        private static readonly ILog Log = LogManager.GetLogger( typeof( DropDBControl ) );
        private readonly AdministrationSettingsSection _ad = AdministrationSettingsSection.GetFromConfigSource();
        private readonly string _connectionString;

        public DropDBControl(string connectionString)
        {
            InitializeComponent();
            if (!Directory.Exists(_ad.DBBackUpFilesDirectory))
            {
                try
                {
                    Directory.CreateDirectory(_ad.DBBackUpFilesDirectory);
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                    MessageForm.ShowErrorMsg(ex.Message);
                }
            }
            _connectionString = connectionString;
            textEditDbFilesDirectory.Text = _ad.DBBackUpFilesDirectory;
        }

        public override void InitControl()
        {
            base.InitControl();
            CaptionControl = "Удаление БД";

            insertData();
        }

        private void insertData()
        {
            try
            {
                DataTable tbl = DBExecutor.SelectQuerry("use master\n " +
                                                        "select * from sysdatabases where dbid>4");
                comboBoxEdit1.Properties.Items.Clear();
                foreach (DataRow row in tbl.Rows)
                {
                    comboBoxEdit1.Properties.Items.Add(row[0]);
                }
                if (comboBoxEdit1.Properties.Items.Count > 0)
                    comboBoxEdit1.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw;
            }
        }

        private void simpleButtonClose_Click(object sender, EventArgs e)
        {
            if (ParentForm != null)
                ParentForm.Close();
        }

        private void simpleButtonCreateDB_Click(object sender, EventArgs e)
        {
            if (comboBoxEdit1.SelectedItem == null)
            {
                MessageForm.ShowErrorMsg("Необходимо выбрать БД на сервере");
                return;
            }
            if (string.IsNullOrEmpty(textEditDbFilesDirectory.Text.Trim()))
            {
                MessageForm.ShowErrorMsg("Необходимо указать путь для  BackUp копии БД");
                return;
            }
            if (string.IsNullOrEmpty(textEditDBName.Text.Trim()))
            {
                MessageForm.ShowErrorMsg("Необходимо указать наименование файла БД.");
                return;
            }
            string fileName = textEditDbFilesDirectory.Text + @"\" + textEditDBName.Text;
            if (File.Exists(fileName))
            {
                if (
                    MessageForm.ShowYesNoQuestionMsg(string.Format("Данный файл {0} существует. \nЗаменить? ", fileName)) ==
                    DialogResult.No)
                {
                    return;
                }
            }
            //проверка пароля
            var ask = new AskPasswordForm();
            ask.InitControl();
            if (ask.ShowInForm(false, false, "") == DialogResult.Cancel)
            {
                MessageForm.ShowErrorMsg("Операщия не завершена. \n Ввод пароля не произведен.");
                return;
            }
            panelControl1.Visible = true;
            simpleButtonCreateDB.Enabled = false;
            simpleButtonClose.Enabled = false;
            var _DropDB = new Thread(DropDBMethod);
            _DropDB.Name = "DropDBThread";
            _DropDB.Start();
        }

        private void DropDBMethod()
        {
            string query;
            var conn = new SqlConnection(_connectionString);
            var cmd = new SqlCommand();
            conn.Open();
            if (conn.State != ConnectionState.Open)
            {
                MessageForm.ShowErrorMsg("Ошибка подключения к серверу баз данных");
            }

            cmd.Connection = conn;
            cmd.CommandTimeout = 1200;
            cmd.CommandType = CommandType.Text;

            string fileName = textEditDBName.Text.Trim();
            string dbName = comboBoxEdit1.SelectedItem.ToString();


            //проверяем, я вляется ли эта БД которую можно удалять
            try
            {
                cmd.CommandText = string.Format("use {0}\n " +
                                                "select * from users", dbName);
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                MessageForm.ShowErrorMsg("Данная БД не является БД Профессионала");
                EnableButtons();
                return;
            }
            //запускаем процесс архивирования БД


            query = string.Format("BACKUP DATABASE [{2}] TO  DISK = N'{0}\\{1}' " +
                                  "WITH NOFORMAT, INIT,  NAME = N'{2} full database BackUp', " +
                                  "SKIP, NOREWIND, NOUNLOAD,  STATS = 10",
                                  textEditDbFilesDirectory.Text,
                                  fileName,
                                  dbName);


            cmd.CommandText = query;

            try
            {
                cmd.ExecuteNonQuery();
                query =
                    string.Format("EXEC msdb.dbo.sp_delete_database_backuphistory @database_name = N'{0}' \n" +
                                  // "GO \n"+
                                  "USE [master] \n" +
                                  // "GO \n"+
                                  "ALTER DATABASE [{0}] SET  SINGLE_USER WITH ROLLBACK IMMEDIATE \n" +
                                  // "GO \n"+
                                  "ALTER DATABASE [{0}] SET  SINGLE_USER \n" +
                                  //"GO \n"+
                                  "DROP DATABASE [{0}] \n"
                                  //"GO "
                                  , dbName)
                    ;
                //MessageForm.ShowOkMsg(query);

                cmd.CommandText = query;
                cmd.ExecuteNonQuery();
                MessageForm.ShowOkMsg(string.Format(
                    "БД {0} удалена.", dbName));
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                MessageForm.ShowErrorMsg(ex.Message);
            }
            finally
            {
                EnableButtons();
                MethodInvoker method = delegate { insertData(); };
                Invoke(method);
            }
        }

        private void EnableButtons()
        {
            MethodInvoker method = delegate
                                       {
                                           simpleButtonCreateDB.Enabled = true;
                                           simpleButtonClose.Enabled = true;
                                           panelControl1.Visible = false;
                                       };
            Invoke(method);
        }

        private void comboBoxEdit1_SelectedIndexChanged(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now;
            textEditDBName.Text = string.Format("{3}_{0}_{1}_{2}_{4}_{5}.bak", dt.Year, dt.Month, dt.Day,
                                                comboBoxEdit1.SelectedItem, dt.Hour, dt.Minute);
        }
    }
}
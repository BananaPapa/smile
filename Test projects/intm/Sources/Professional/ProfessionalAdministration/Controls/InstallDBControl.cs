﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using Eureca.Professional.BaseComponents.Configuration;
using Eureca.Professional.CommonClasses;
using Eureca.Integrator.Utility.Forms;
using EurecaCorp.Professional.BaseComponents.Configuration;
using log4net;


namespace Eureca.Professional.ProfessionalAdministration
{
    public partial class InstallDBControl : BaseTestingControl
    {
        private static readonly ILog Log = LogManager.GetLogger( typeof( InstallDBControl ) );

        private readonly AdministrationSettingsSection _ad = AdministrationSettingsSection.GetFromConfigSource();

        private readonly string _connectionString;

        public InstallDBControl(string ConnectionString)
        {
            InitializeComponent();
            if (!Directory.Exists(_ad.DBBackUpFilesDirectory))
            {
                try
                {
                    Directory.CreateDirectory(_ad.DBBackUpFilesDirectory);
                   
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                    MessageForm.ShowErrorMsg(ex.Message);
                }
            }
            if (!Directory.Exists(_ad.DBFilesDirectory))
            {
                try
                {
                    Directory.CreateDirectory(_ad.DBFilesDirectory);
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                    MessageForm.ShowErrorMsg(ex.Message);
                }
            }
            textEditDbFilesDirectory.Text = _ad.DBFilesDirectory;

           
            _connectionString = ConnectionString;
        }

        public override void InitControl()
        {
            base.InitControl();
            CaptionControl = "Восстановление БД";
        }

        private void simpleButtonOpenBackUp_Click(object sender, EventArgs e)
        {
            using (var dlg = new OpenFileDialog())
            {
                dlg.InitialDirectory = _ad.DBBackUpFilesDirectory;
                dlg.Filter = "bak files (*.bak)|*.bak";
                dlg.RestoreDirectory = true;
                DialogResult res = dlg.ShowDialog();
                if (res == DialogResult.OK)
                {
                    textEditBackUpFile.Text = dlg.FileName;
                }
            }
        }

        private void simpleButtonOpenDBFilesDirectory_Click(object sender, EventArgs e)
        {
            using (var dlg = new FolderBrowserDialog())
            {
                dlg.SelectedPath = _ad.DBFilesDirectory;
                DialogResult res = dlg.ShowDialog();
                if (res == DialogResult.OK)
                {
                    textEditDbFilesDirectory.Text = dlg.SelectedPath;
                }
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textEditBackUpFile.Text.Trim()))
            {
                MessageForm.ShowErrorMsg("Необходимо указать файл BackUp копии БД");
                return;
            }
            if (string.IsNullOrEmpty(textEditDBName.Text.Trim()))
            {
                MessageForm.ShowErrorMsg("Необходимо указать наименование БД");
                return;
            }
            panelControl1.Visible = true;

            simpleButtonCreateDB.Enabled = false;
            simpleButtonClose.Enabled = false;

            var _restoreDB = new Thread(RestoreDB);
            _restoreDB.Name = "RestoreDBThread";
            _restoreDB.Start();
        }

        private void RestoreDB()
        {
            string query;
            var conn = new SqlConnection(_connectionString);
            var cmd = new SqlCommand();
            conn.Open();
            if (conn.State != ConnectionState.Open)
            {
                MessageForm.ShowErrorMsg("Ошибка подключения к серверу баз данных");
            }
            cmd.Connection = conn;

            string dbName = textEditDBName.Text.Trim();

            query = string.Format("use master \n SELECT COUNT(*) FROM dbo.sysdatabases WHERE NAME ='{0}'", dbName);
            cmd.CommandText = query;
            try
            {
                var ret = (int) cmd.ExecuteScalar();
                if (ret == 1)
                {
                    MessageForm.ShowErrorMsg("Данная БД уже установлена на этом сервере");
                    EnableButtons();
                    return;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                MessageForm.ShowErrorMsg(ex.Message);
            }

            //запускаем процесс восстановления БД

            query = string.Format(
                @"restore database {1} from disk='{0}' " +
                @"WITH MOVE 'ProfessionalEtalon' TO '{2}\{1}"+Guid.NewGuid()+"_Data.MDF', " +
                @"MOVE 'ProfessionalEtalon_log' TO '{2}\{1}"+Guid.NewGuid()+"_Log.LDF'",
                textEditBackUpFile.Text,
                dbName,
                textEditDbFilesDirectory.Text
                );


            cmd.CommandText = query;
            cmd.CommandTimeout = 3600;

            try
            {
                cmd.ExecuteNonQuery();
                MessageForm.ShowOkMsg(string.Format("БД {0} успешно установлена.", textEditDBName.Text));
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                MessageForm.ShowErrorMsg(ex.Message);
            }
            finally
            {
                EnableButtons();
            }
        }

        private void EnableButtons()
        {
            MethodInvoker method = delegate
                {
                    simpleButtonCreateDB.Enabled = true;
                    simpleButtonClose.Enabled = true;
                    panelControl1.Visible = false;
                };
            Invoke(method);
        }

        private void simpleButtonClose_Click(object sender, EventArgs e)
        {
            if (ParentForm != null) ParentForm.Close();
        }
    }
}
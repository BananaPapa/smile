﻿namespace Eureca.Professional.ProfessionalAdministration
{
    partial class InstallDBControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.textEditBackUpFile = new DevExpress.XtraEditors.TextEdit();
            this.simpleButtonOpenBackUp = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonOpenDBFilesDirectory = new DevExpress.XtraEditors.SimpleButton();
            this.textEditDbFilesDirectory = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.textEditDBName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButtonCreateDB = new DevExpress.XtraEditors.SimpleButton();
            this.marqueeProgressBarControl1 = new DevExpress.XtraEditors.MarqueeProgressBarControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButtonClose = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.textEditBackUpFile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDbFilesDirectory.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDBName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.marqueeProgressBarControl1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(3, 19);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(114, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Файл BackUp копии БД";
            // 
            // textEditBackUpFile
            // 
            this.textEditBackUpFile.Enabled = false;
            this.textEditBackUpFile.Location = new System.Drawing.Point(123, 16);
            this.textEditBackUpFile.Name = "textEditBackUpFile";
            this.textEditBackUpFile.Size = new System.Drawing.Size(320, 20);
            this.textEditBackUpFile.TabIndex = 1;
            // 
            // simpleButtonOpenBackUp
            // 
            this.simpleButtonOpenBackUp.Location = new System.Drawing.Point(449, 13);
            this.simpleButtonOpenBackUp.Name = "simpleButtonOpenBackUp";
            this.simpleButtonOpenBackUp.Size = new System.Drawing.Size(29, 23);
            this.simpleButtonOpenBackUp.TabIndex = 2;
            this.simpleButtonOpenBackUp.Text = "...";
            this.simpleButtonOpenBackUp.Click += new System.EventHandler(this.simpleButtonOpenBackUp_Click);
            // 
            // simpleButtonOpenDBFilesDirectory
            // 
            this.simpleButtonOpenDBFilesDirectory.Location = new System.Drawing.Point(449, 39);
            this.simpleButtonOpenDBFilesDirectory.Name = "simpleButtonOpenDBFilesDirectory";
            this.simpleButtonOpenDBFilesDirectory.Size = new System.Drawing.Size(29, 23);
            this.simpleButtonOpenDBFilesDirectory.TabIndex = 5;
            this.simpleButtonOpenDBFilesDirectory.Text = "...";
            this.simpleButtonOpenDBFilesDirectory.Click += new System.EventHandler(this.simpleButtonOpenDBFilesDirectory_Click);
            // 
            // textEditDbFilesDirectory
            // 
            this.textEditDbFilesDirectory.Enabled = false;
            this.textEditDbFilesDirectory.Location = new System.Drawing.Point(123, 42);
            this.textEditDbFilesDirectory.Name = "textEditDbFilesDirectory";
            this.textEditDbFilesDirectory.Size = new System.Drawing.Size(320, 20);
            this.textEditDbFilesDirectory.TabIndex = 4;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(3, 39);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(92, 26);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "Путь для рабочих \r\nфайлов БД";
            // 
            // textEditDBName
            // 
            this.textEditDBName.EditValue = "Professional";
            this.textEditDBName.Location = new System.Drawing.Point(123, 68);
            this.textEditDBName.Name = "textEditDBName";
            this.textEditDBName.Size = new System.Drawing.Size(320, 20);
            this.textEditDBName.TabIndex = 7;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(3, 71);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(90, 13);
            this.labelControl3.TabIndex = 6;
            this.labelControl3.Text = "Наименование БД";
            // 
            // simpleButtonCreateDB
            // 
            this.simpleButtonCreateDB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonCreateDB.Location = new System.Drawing.Point(313, 122);
            this.simpleButtonCreateDB.Name = "simpleButtonCreateDB";
            this.simpleButtonCreateDB.Size = new System.Drawing.Size(85, 43);
            this.simpleButtonCreateDB.TabIndex = 9;
            this.simpleButtonCreateDB.Text = "Создать";
            this.simpleButtonCreateDB.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // marqueeProgressBarControl1
            // 
            this.marqueeProgressBarControl1.EditValue = 0;
            this.marqueeProgressBarControl1.Location = new System.Drawing.Point(5, 38);
            this.marqueeProgressBarControl1.Name = "marqueeProgressBarControl1";
            this.marqueeProgressBarControl1.Size = new System.Drawing.Size(283, 28);
            this.marqueeProgressBarControl1.TabIndex = 10;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.marqueeProgressBarControl1);
            this.panelControl1.Location = new System.Drawing.Point(3, 94);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(293, 71);
            this.panelControl1.TabIndex = 11;
            this.panelControl1.Visible = false;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(5, 6);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(175, 26);
            this.labelControl4.TabIndex = 11;
            this.labelControl4.Text = "Идет процесс создания БД\r\n(может занять длительное время)";
            // 
            // simpleButtonClose
            // 
            this.simpleButtonClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonClose.Location = new System.Drawing.Point(405, 122);
            this.simpleButtonClose.Name = "simpleButtonClose";
            this.simpleButtonClose.Size = new System.Drawing.Size(85, 43);
            this.simpleButtonClose.TabIndex = 12;
            this.simpleButtonClose.Text = "Закрыть";
            this.simpleButtonClose.Click += new System.EventHandler(this.simpleButtonClose_Click);
            // 
            // InstallDBControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.simpleButtonClose);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.simpleButtonCreateDB);
            this.Controls.Add(this.textEditDBName);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.simpleButtonOpenDBFilesDirectory);
            this.Controls.Add(this.textEditDbFilesDirectory);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.simpleButtonOpenBackUp);
            this.Controls.Add(this.textEditBackUpFile);
            this.Controls.Add(this.labelControl1);
            this.MinimumSize = new System.Drawing.Size(482, 144);
            this.Name = "InstallDBControl";
            this.Size = new System.Drawing.Size(493, 172);
            ((System.ComponentModel.ISupportInitialize)(this.textEditBackUpFile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDbFilesDirectory.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDBName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.marqueeProgressBarControl1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit textEditBackUpFile;
        private DevExpress.XtraEditors.SimpleButton simpleButtonOpenBackUp;
        private DevExpress.XtraEditors.SimpleButton simpleButtonOpenDBFilesDirectory;
        private DevExpress.XtraEditors.TextEdit textEditDbFilesDirectory;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit textEditDBName;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCreateDB;
        private DevExpress.XtraEditors.MarqueeProgressBarControl marqueeProgressBarControl1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.SimpleButton simpleButtonClose;

    }
}

﻿using System.Windows.Forms;
using DevExpress.XtraBars;
using Eureca.Professional.BaseComponents;
using Eureca.Professional.CommonClasses;
using Eureca.Professional.CommonClasses.Configuration;
using Eureca.Updater.UpdaterClient;


namespace Eureca.Professional.ProfessionalAdministration
{
    public partial class StartForm : ProfessionalStartForm
    {
        // private bool _exit;

        public StartForm()
        {
            InitializeComponent();
            AskBeforeExit = false;
        }

        private void barButtonItemDB_ItemClick(object sender, ItemClickEventArgs e)
        {
            ConnectDbClick();
        }

        private void barButtonItemChangeUser_ItemClick(object sender, ItemClickEventArgs e)
        {
            ChangeUserClick();
        }

        private void barButtonItemSettings_ItemClick(object sender, ItemClickEventArgs e)
        {
            ShowSettingsClick();
        }

        private void barButtonItemExit_ItemClick(object sender, ItemClickEventArgs e)
        {
            Close();
        }

        private void barButtonItemUsers_ItemClick(object sender, ItemClickEventArgs e)
        {
            var uc = new ViewUsers(RunnerApplication.CurrentUser);
            uc.InitControl();
            uc.ShowInForm();
            RunnerApplication.CurrentUser.Refresh();
        }

        private void barButtonItemEvents_ItemClick(object sender, ItemClickEventArgs e)
        {
            var uc = new ViewEvents();
            uc.InitControl();
            uc.ShowInForm();
        }

        private void barButtonItemRestore_ItemClick(object sender, ItemClickEventArgs e)
        {
            RestoreDB();
        }


        private void RestoreDB()
        {
            DatabaseSettingsSection _db = DatabaseSettingsSection.GetFromConfigSource();
            string connStr = _db.ConnectionString;
            string resConnectionString;
            MessageForm.ShowOkMsg("Подключитесь к необходимому серверу баз данных к БД 'Master'");
            while (true)
            {
                if (!RunnerApplication.ChangeConnectionToDataBase(_db.ConnectionString, out resConnectionString))
                {
                    if (MessageForm.ShowYesNoQuestionMsg("Некорректное подключение.\nПродолжить попытку подключения?")
                        == DialogResult.Yes)
                    {
                        continue;
                    }
                    _db.ConnectionString = connStr;
                    Settings.Save(_db);
                    return;
                }
                if (!resConnectionString.ToUpper().Contains("MASTER"))
                {
                    MessageForm.ShowOkMsg("Необходимо выбрать БД 'Master'");
                    continue;
                }
                break;
            }
            _db = DatabaseSettingsSection.GetFromConfigSource();
            var uc = new InstallDBControl(_db.ConnectionString);
            uc.InitControl();
            uc.ShowInForm(false, false, "");
            _db.ConnectionString = connStr;
            Settings.Save(_db);
        }

        private void DropDB()
        {
            DatabaseSettingsSection _db = DatabaseSettingsSection.GetFromConfigSource();
            string connStr = _db.ConnectionString;
            string resConnectionString;
            MessageForm.ShowOkMsg("Подключитесь к необходимому серверу баз данных к БД 'Master'");
            while (true)
            {
                if (!RunnerApplication.ChangeConnectionToDataBase(_db.ConnectionString, out resConnectionString))
                {
                    if (MessageForm.ShowYesNoQuestionMsg("Некорректное подключение.\nПродолжить попытку подключения?")
                        == DialogResult.Yes)
                    {
                        continue;
                    }
                    //пользователь нажал нет и выход из метода
                    _db.ConnectionString = connStr;
                    Settings.Save(_db);
                    return;
                }
                _db = DatabaseSettingsSection.GetFromConfigSource();
                if (!resConnectionString.ToUpper().Contains("MASTER"))
                {
                    MessageForm.ShowOkMsg("Необходимо выбрать БД 'Master'");
                    continue;
                }
                break;
            }
            _db = DatabaseSettingsSection.GetFromConfigSource();
            var uc = new DropDBControl(_db.ConnectionString);
            uc.InitControl();
            uc.ShowInForm(false, false, "");
            _db.ConnectionString = connStr;
            Settings.Save(_db);
        }

        private void barButtonItemBackUp_ItemClick(object sender, ItemClickEventArgs e)
        {
            var uc = new BackUpDbControl();
            uc.InitControl();
            uc.ShowInForm(false, false, "");
        }

        private void barButtonItemDeleteDB_ItemClick(object sender, ItemClickEventArgs e)
        {
            DropDB();
        }

        private void barButtonItem1_ItemClick(object sender, ItemClickEventArgs e)
        {
            var runq = new RunQueryControl(DatabaseSettingsSection.GetFromConfigSource().ConnectionString);
            runq.InitControl();
            runq.ShowInForm();
        }

        private void barButtonItemUpdateSettings_ItemClick( object sender, ItemClickEventArgs e )
        {
            ChangeUpdateSettings( (IUpdateSettingStorage)RunnerApplication.UpdateSettingsContext );
        }

        private void barButtonItemInfo_ItemClick( object sender, ItemClickEventArgs e )
        {
            RunnerApplication.ShowInfoPage( );
        }
    }
}
﻿namespace Eureca.Professional.ProfessionalAdministration
{
    partial class StartForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StartForm));
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barButtonItemUsers = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemChangeUser = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemExit = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemSettings = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemDB = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemEvents = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemRestore = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemBackUp = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemDeleteDB = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemUpdateSettings = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemInfo = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPageWork = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroupTestts = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup5 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            ((System.ComponentModel.ISupportInitialize)(this.workPanel)).BeginInit();
            this.workPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            this.SuspendLayout();
            // 
            // workPanel
            // 
            this.workPanel.Appearance.ForeColor = System.Drawing.Color.Transparent;
            this.workPanel.Appearance.Options.UseForeColor = true;
            this.workPanel.Controls.Add(this.ribbonControl1);
            this.workPanel.LookAndFeel.SkinName = "Blue";
            this.workPanel.Size = new System.Drawing.Size(947, 116);
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.barButtonItemUsers,
            this.barButtonItemChangeUser,
            this.barButtonItemExit,
            this.barButtonItemSettings,
            this.barButtonItemDB,
            this.barButtonItemEvents,
            this.barButtonItemRestore,
            this.barButtonItemBackUp,
            this.barButtonItemDeleteDB,
            this.barButtonItem1,
            this.barButtonItemUpdateSettings,
            this.barButtonItemInfo});
            this.ribbonControl1.Location = new System.Drawing.Point(2, 2);
            this.ribbonControl1.MaxItemId = 23;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPageWork});
            this.ribbonControl1.ShowToolbarCustomizeItem = false;
            this.ribbonControl1.Size = new System.Drawing.Size(943, 112);
            this.ribbonControl1.Toolbar.ShowCustomizeItem = false;
            this.ribbonControl1.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden;
            // 
            // barButtonItemUsers
            // 
            this.barButtonItemUsers.Caption = "Пользователи";
            this.barButtonItemUsers.Id = 1;
            this.barButtonItemUsers.LargeGlyph = global::Eureca.Professional.ProfessionalAdministration.Properties.Resources.users;
            this.barButtonItemUsers.Name = "barButtonItemUsers";
            this.barButtonItemUsers.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemUsers_ItemClick);
            // 
            // barButtonItemChangeUser
            // 
            this.barButtonItemChangeUser.Caption = "Смена пользователя";
            this.barButtonItemChangeUser.Id = 10;
            this.barButtonItemChangeUser.LargeGlyph = global::Eureca.Professional.ProfessionalAdministration.Properties.Resources.ChangeUsers;
            this.barButtonItemChangeUser.Name = "barButtonItemChangeUser";
            this.barButtonItemChangeUser.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemChangeUser_ItemClick);
            // 
            // barButtonItemExit
            // 
            this.barButtonItemExit.Caption = "Завершение работы";
            this.barButtonItemExit.Id = 12;
            this.barButtonItemExit.LargeGlyph = global::Eureca.Professional.ProfessionalAdministration.Properties.Resources.exit;
            this.barButtonItemExit.Name = "barButtonItemExit";
            this.barButtonItemExit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemExit_ItemClick);
            // 
            // barButtonItemSettings
            // 
            this.barButtonItemSettings.Caption = "Параметры приложения";
            this.barButtonItemSettings.Id = 14;
            this.barButtonItemSettings.LargeGlyph = global::Eureca.Professional.ProfessionalAdministration.Properties.Resources.options;
            this.barButtonItemSettings.Name = "barButtonItemSettings";
            this.barButtonItemSettings.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemSettings_ItemClick);
            // 
            // barButtonItemDB
            // 
            this.barButtonItemDB.Caption = "Подключение к БД";
            this.barButtonItemDB.Id = 15;
            this.barButtonItemDB.LargeGlyph = global::Eureca.Professional.ProfessionalAdministration.Properties.Resources.network_server_database32;
            this.barButtonItemDB.Name = "barButtonItemDB";
            this.barButtonItemDB.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemDB_ItemClick);
            // 
            // barButtonItemEvents
            // 
            this.barButtonItemEvents.Caption = "События";
            this.barButtonItemEvents.Id = 16;
            this.barButtonItemEvents.LargeGlyph = global::Eureca.Professional.ProfessionalAdministration.Properties.Resources.ViewEvents;
            this.barButtonItemEvents.Name = "barButtonItemEvents";
            this.barButtonItemEvents.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemEvents_ItemClick);
            // 
            // barButtonItemRestore
            // 
            this.barButtonItemRestore.Caption = "Создать";
            this.barButtonItemRestore.Id = 17;
            this.barButtonItemRestore.LargeGlyph = global::Eureca.Professional.ProfessionalAdministration.Properties.Resources.restore;
            this.barButtonItemRestore.Name = "barButtonItemRestore";
            this.barButtonItemRestore.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemRestore_ItemClick);
            // 
            // barButtonItemBackUp
            // 
            this.barButtonItemBackUp.Caption = "Архивировать";
            this.barButtonItemBackUp.Id = 18;
            this.barButtonItemBackUp.LargeGlyph = global::Eureca.Professional.ProfessionalAdministration.Properties.Resources.BackUpDB;
            this.barButtonItemBackUp.Name = "barButtonItemBackUp";
            this.barButtonItemBackUp.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemBackUp_ItemClick);
            // 
            // barButtonItemDeleteDB
            // 
            this.barButtonItemDeleteDB.Caption = "Удалить";
            this.barButtonItemDeleteDB.Id = 19;
            this.barButtonItemDeleteDB.LargeGlyph = global::Eureca.Professional.ProfessionalAdministration.Properties.Resources.DeleteDatabase;
            this.barButtonItemDeleteDB.Name = "barButtonItemDeleteDB";
            this.barButtonItemDeleteDB.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemDeleteDB_ItemClick);
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Выполнить запрос";
            this.barButtonItem1.Id = 20;
            this.barButtonItem1.LargeGlyph = global::Eureca.Professional.ProfessionalAdministration.Properties.Resources.images;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // barButtonItemUpdateSettings
            // 
            this.barButtonItemUpdateSettings.Caption = "Обновления";
            this.barButtonItemUpdateSettings.Id = 21;
            this.barButtonItemUpdateSettings.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemUpdateSettings.LargeGlyph")));
            this.barButtonItemUpdateSettings.Name = "barButtonItemUpdateSettings";
            this.barButtonItemUpdateSettings.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemUpdateSettings_ItemClick);
            // 
            // barButtonItemInfo
            // 
            this.barButtonItemInfo.Caption = "Справка";
            this.barButtonItemInfo.Id = 22;
            this.barButtonItemInfo.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemInfo.LargeGlyph")));
            this.barButtonItemInfo.Name = "barButtonItemInfo";
            this.barButtonItemInfo.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemInfo_ItemClick);
            // 
            // ribbonPageWork
            // 
            this.ribbonPageWork.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroupTestts,
            this.ribbonPageGroup2,
            this.ribbonPageGroup1,
            this.ribbonPageGroup5,
            this.ribbonPageGroup3});
            this.ribbonPageWork.Name = "ribbonPageWork";
            this.ribbonPageWork.Text = "Действия пользователя";
            // 
            // ribbonPageGroupTestts
            // 
            this.ribbonPageGroupTestts.ItemLinks.Add(this.barButtonItemUsers);
            this.ribbonPageGroupTestts.ItemLinks.Add(this.barButtonItemEvents);
            this.ribbonPageGroupTestts.Name = "ribbonPageGroupTestts";
            this.ribbonPageGroupTestts.ShowCaptionButton = false;
            this.ribbonPageGroupTestts.Text = "Управление системой";
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.ItemLinks.Add(this.barButtonItemRestore);
            this.ribbonPageGroup2.ItemLinks.Add(this.barButtonItemBackUp);
            this.ribbonPageGroup2.ItemLinks.Add(this.barButtonItemDeleteDB);
            this.ribbonPageGroup2.ItemLinks.Add(this.barButtonItem1);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.ShowCaptionButton = false;
            this.ribbonPageGroup2.Text = "Управление базой данных";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItemSettings);
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItemDB);
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItemUpdateSettings);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.ShowCaptionButton = false;
            this.ribbonPageGroup1.Text = "Настройки";
            // 
            // ribbonPageGroup5
            // 
            this.ribbonPageGroup5.ItemLinks.Add(this.barButtonItemChangeUser);
            this.ribbonPageGroup5.ItemLinks.Add(this.barButtonItemExit);
            this.ribbonPageGroup5.Name = "ribbonPageGroup5";
            this.ribbonPageGroup5.ShowCaptionButton = false;
            this.ribbonPageGroup5.Text = "Завершение работы";
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.AllowMinimize = false;
            this.ribbonPageGroup3.AllowTextClipping = false;
            this.ribbonPageGroup3.ItemLinks.Add(this.barButtonItemInfo);
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            this.ribbonPageGroup3.Text = "Информация";
            // 
            // StartForm
            // 
            this.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.Appearance.Options.UseForeColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(947, 116);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.LookAndFeel.SkinName = "Blue";
            this.MaximizeBox = false;
            this.Name = "StartForm";
            this.Text = "СПО «Администратор»";
            ((System.ComponentModel.ISupportInitialize)(this.workPanel)).EndInit();
            this.workPanel.ResumeLayout(false);
            this.workPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPageWork;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupTestts;
        private DevExpress.XtraBars.BarButtonItem barButtonItemUsers;
        private DevExpress.XtraBars.BarButtonItem barButtonItemChangeUser;
        private DevExpress.XtraBars.BarButtonItem barButtonItemExit;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup5;
        private DevExpress.XtraBars.BarButtonItem barButtonItemSettings;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.BarButtonItem barButtonItemDB;
        private DevExpress.XtraBars.BarButtonItem barButtonItemEvents;
        private DevExpress.XtraBars.BarButtonItem barButtonItemRestore;
        private DevExpress.XtraBars.BarButtonItem barButtonItemBackUp;
        private DevExpress.XtraBars.BarButtonItem barButtonItemDeleteDB;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItemUpdateSettings;
        private DevExpress.XtraBars.BarButtonItem barButtonItemInfo;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
    }
}
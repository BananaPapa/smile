﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Remoting;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using DevExpress.Skins;
using Eureca.Integrator.Utility;
using Eureca.Integrator.Utility.Settings;
using Eureca.Professional.ActiveRecords;
using Eureca.Professional.BaseComponents;
using Eureca.Professional.BaseComponents.Configuration;
using Eureca.Professional.CommonClasses;
using Eureca.Professional.CommonClasses.Configuration;
using Eureca.Utility.Extensions;
using System.Collections.Generic;
using EurecaCorp.Professional.BaseComponents.Configuration;
using System.Threading.Tasks;
using DevExpress.XtraEditors;
using log4net;
using MessageBox = Eureca.Integrator.Utility.Forms.MessageBox;

namespace Eureca.Professional.ProfessionalAdministration
{
    /// <summary>
    /// Администратор
    /// производит администрирования БД, создание копий
    /// </summary>
    internal static class Program
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof (Program));

        #region fields
        /// <summary>
        /// таймер создания копии БД
        /// </summary>
        private static System.Timers.Timer _backUpTimer;

        /// <summary>
        /// наименование сервиса
        /// </summary>
        private const string ServiceName = "AdministrationService";

        #endregion

        #region main
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            AppDomain.CurrentDomain.UnhandledException += ( CurrentDomain_UnhandledException );



            bool isServiceWasStopped = false; 

            if (!AppActivation.IsActivation(Application.StartupPath) 
                && !AppActivation.Activation(Application.StartupPath))
            {
                return;
            }

            var ad = AdministrationSettingsSection.GetFromConfigSource();
            //проверка на интервал таймера
            if (ad.BackUpTimerInterval <= 0)
            {
                ad.BackUpTimerInterval = 24;
                Settings.Save();
            }
            //запуск таймера который срабатывает каждую минуту
            _backUpTimer = new System.Timers.Timer(60000);
            _backUpTimer.Elapsed += BackUpTimerElapsed;

            //AppDomain.CurrentDomain.UnhandledException += (CurrentDomain_UnhandledException);

            try
            {
                //если служба то запускаю службу
                if (!Environment.UserInteractive)
                {
                    var servicesToRun = new ServiceBase[] { new AdministrationService() };
                    ServiceBase.Run(servicesToRun);
                    return;
                }
                else
                {
                    if (IsInstalled())
                    {

                        var sc = new ServiceController(ServiceName);
                        if (sc.Status == ServiceControllerStatus.Running)
                        {
                            StopService();
                            isServiceWasStopped = true;
                        }
                    }
                    else
                    {
                        if (InstallService())
                        {
                            throw new Exception("Failed to install service");
                        }
                    }
                }

                //настраиваю параметры для удаленного взаимодействия
                try
                {
                    RemotingConfiguration.Configure(Application.StartupPath + "\\" + "ProfessionalAdministration.exe.config", false);
                    RemotingConfiguration.CustomErrorsMode = CustomErrorsModes.Off;
                    RemotingConfiguration.CustomErrorsEnabled(true);
                }
                catch (Exception ex)
                {
                    string mess = "Запущен сервис удаленного доступа. " + ex.Message;
                    Log.Error(ex);
                    Log.Error(mess);
                    if (Environment.UserInteractive)
                    {
                        MessageForm.ShowErrorMsg(mess);
                    }
                    else
                    {
                        return;
                    }
                }
                //инициализация таймера создания резервной копии
                if (ad.EnableBackUpTimer)
                {
                    _backUpTimer.Start();
                    //вызываем обработчик таймера при страрте программы 
                    //т.к. следующий обработчик вызовется только чере интервал
                    BackUpTimerElapsed(null, null);
                }


                SkinManager.EnableFormSkins();
                SkinManager.EnableMdiFormSkins();

                Thread.CurrentThread.CurrentCulture = new CultureInfo("ru-RU");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("ru");

                Application.EnableVisualStyles();
                //Application.SetCompatibleTextRenderingDefault(false);
                Application.ThreadException += (Application_ThreadException);

                RunnerApplication.CurrentUser = null;
                RunnerApplication.CurrentArm = ARMType.ADMINISTRATION;
                
                //Синхронизация с глобальными настройками.
                ProfessionalStartForm.SyncProfSettings( );

                //загружаю настройки арма
                ARMSettingsSection armSettings = ARMSettingsSection.GetFromConfigSource( );
                DatabaseSettingsSection db = DatabaseSettingsSection.GetFromConfigSource();
                //выставляю локальные настройки
                armSettings.ARMType = RunnerApplication.CurrentArm;
                //создаю контейнер для параметров
                var param = new StartUpParameters { ARMSettings = armSettings , ApplicationIcon = Properties.Resources.AdministrationIcon};

                //попытка подключения к БД
                //если попытка удачная, то опучкаем вопрос о создании БД
                //иначе создаем БД из BackUp


                if ((!DBAdministration.ExistConnection(db.ConnectionString)) ||
                    (db.ConnectionString.ToUpper().Contains("MASTER")))
                {
                    var admSettings = AdministrationSettingsSection.GetFromConfigSource();
                    string backUpDirectory = admSettings.DBBackUpFilesDirectory;

                    if (CheckBackupsAvailability(backUpDirectory) && MessageForm.ShowYesNoQuestionMsg( "Подключение отсутствует. \n Вы хотите восстановить БД из BackUp копии?") == DialogResult.Yes)
                    {
                        DBAdministration.RestoreDB();
                    }
                    else if (MessageBox.Confirm( "База данных не указана. Создать базу данных?" ))
                    {
                        DBAdministration.CreateDB( Path.Combine( AppDomain.CurrentDomain.BaseDirectory, "Professional.sql" ) );
                    }
                    else
                        MessageBox.ShowInfo( String.Format( @"Подключитесь к БД '{0}'", IntegratorDb.Professional.GetEnumDescription( ) ) );
                }

                //инсталляция сервиса администрирования
                //InstallService();

                //запускаю функцию базового класса для всех армов
                while (true)
                {
                    if (RunnerApplication.ConnectDb())
                    {
                        //сохраняю настройки
                        Settings.Save();

                        try
                        {
                            //прописываю IP адрес сервера
                            var arm = ArmApplication.GetArmApplication(param.ARMSettings.ARMType);
                            var ip = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName());
                            arm.IPAdress = ip.AddressList[0].ToString();
                            arm.SaveAndFlush();
                        }
                        catch (Exception ex)
                        {
                            Log.Error(ex);
                        }

                        Log.Error("Старт программы");
                        RunnerApplication.RunUpdater( );
                        //CheckForUpdates( );
                        //запускаю приложение
                        if (RunnerApplication.Authotization( param ))
                        {
                            Application.Run( new StartForm( ) );
                        }
                        break;
                    }
                    break;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                MessageForm.ShowErrorMsg(ex.Message);
            }
            finally
            {
                if (isServiceWasStopped && Environment.UserInteractive)
                {
                    if (!StartService())
                        throw new Exception("Failed to start service.");
                }
            }
        }

        private static bool CheckBackupsAvailability(string backUpDirectory)
        {
            return backUpDirectory != null && Directory.Exists(backUpDirectory) && Directory.EnumerateFiles(backUpDirectory).Any(item => Path.GetExtension(item).ToLower() == ".bak");
        }

        #endregion

        #region serviceMethods
        private static bool IsInstalled()
        {
            try
            {
                var key = "HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\services\\" + ServiceName + "\\";
                var name = "ImagePath";

                object keyValue = Microsoft.Win32.Registry.GetValue(key, name, null);
                if (keyValue == null)
                {
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return false;
            }
        }

        private static bool InstallService()
        {
            //проверка на то что программа записана в реестр автозапуска
            try
            {
                while (!IsInstalled())
                {
                    if (MessageForm.ShowYesNoQuestionMsg("Записать программу в автозапуск?") == DialogResult.Yes)
                    {
                        var p = new Process
                        {
                            StartInfo =
                            {
                                FileName = string.Format("\"{0}\\Files\\instsrv.exe\"", Application.StartupPath),

                                Arguments =
                                    string.Format(" {0} \"{1}\"", ServiceName, Application.ExecutablePath)
                            }

                        };
                        Log.Error(p.StartInfo.FileName);
                        Log.Error(p.StartInfo.Arguments);
                        if (!p.Start())
                        {

                            string str = "Ошибка при инсталляции сервиса";
                            Log.Error(str);
                            MessageForm.ShowErrorMsg(str);
                            return false;
                        }
                        Thread.Sleep(500);
                        if (!IsInstalled())
                        {
                            string str = "Ошибка при инсталляции сервиса";
                            Log.Error(str);
                            MessageForm.ShowErrorMsg(str);
                            return false;
                        }

                        return true;

                    }
                    return false; ;
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageForm.ShowErrorMsg("Ошибка при записи в автозапуск." + Environment.NewLine
                    + "Запустите программу в от имени Администратора.");
                Log.Error(ex);
                return false;
            }
        }

        private static bool RemoveService()
        {
            //проверка на то что программа записана в реестр автозапуска
            try
            {
                while (IsInstalled())
                {
                    if (MessageForm.ShowYesNoQuestionMsg("Удалить программу из автозапуска?") == DialogResult.Yes)
                    {
                        var p = new Process
                        {
                            StartInfo =
                            {
                                FileName = string.Format("\"{0}\\Files\\instsrv.exe\"", Application.StartupPath),
                                Arguments =
                                    string.Format(" {0} remove", ServiceName)
                            }
                        };
                        if (!p.Start())
                        {
                            string str = "Ошибка при деинсталляции сервиса";
                            Log.Error(str);
                            MessageForm.ShowErrorMsg(str);
                            return false;
                        }
                        Thread.Sleep(500);
                        if (IsInstalled())
                        {
                            string str = "Ошибка при деинсталляции сервиса";
                            Log.Error(str);
                            MessageForm.ShowErrorMsg(str);
                            return false;
                        }
                        return true;

                    }
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageForm.ShowErrorMsg("Ошибка при записи в автозапуск." + Environment.NewLine
                    + "Запустите программу в от имени Администратора.");
                Log.Error(ex);
                return false;
            }
        }

        private static bool StartService()
        {
            Command cmd = new Command();
            return cmd.RunService(ServiceName);
        }
        private static bool StopService()
        {
            var cmd = new Command();
            return cmd.StopService(ServiceName);
        }
        static void BackUpDbOnBackUpSuccess()
        {
            var ad = AdministrationSettingsSection.GetFromConfigSource();
            ad.BackUpTimerElapsedDate = DateTime.Now;
            Settings.Save();
        }
        #endregion

        #region EventHandlers

        static void BackUpTimerElapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                var db = DatabaseSettingsSection.GetFromConfigSource();
                var ad = AdministrationSettingsSection.GetFromConfigSource();
                if (ad.BackUpTimerElapsedDate.AddHours(ad.BackUpTimerInterval) > DateTime.Now)
                {
                    Log.Error(string.Format("Срабатывание таймера создания копии БД отклоненео, дата создания прошлой копии = {0}", ad.BackUpTimerElapsedDate));
                    return;
                }
                if (!Directory.Exists(ad.DBBackUpFilesDirectory)) 
                {
                    if (!Directory.CreateDirectory(ad.DBBackUpFilesDirectory).Exists)
                    {
                        Log.Error(string.Format("Не найдена директория для файлов резервной копии {0}", ad.DBBackUpFilesDirectory));
                        MessageBeep(MessageBeepType.Error);
                        return;
                    }
                }
                BackUpDb backUpDb = new BackUpDb(db.ConnectionString);
                backUpDb.OnBackUpSuccess += BackUpDbOnBackUpSuccess;
                DateTime dt = DateTime.Now;
                backUpDb.CreateBackUp(string.Format("{3}_{0}_{1}_{2}_{4}.bak", dt.Year, dt.Month, dt.Day, backUpDb.DbName, dt.Hour), ad.DBBackUpFilesDirectory);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Log.Error("Необработанное исключение");
            Log.Error(e.ExceptionObject);
            if (
                MessageForm.ShowYesNoQuestionMsg(
                    "Выполненное действие вызвало ошибку.\n За информацией обратитесь в службу технической поддержки. Продолжить работу?") !=
                DialogResult.Yes)
                Application.Exit();
        }

        private static void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
        {
            Log.Error("Необработанное исключение");
            Log.Error(e.Exception);
            if (
                MessageForm.ShowYesNoQuestionMsg(
                    "Выполненное действие вызвало ошибку.\n За информацией обратитесь в службу технической поддержки. Продолжить работу?") !=
                DialogResult.Yes)
                Application.Exit();
        }
        #endregion


        /// <summary>
        /// обертка над системной функцией
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        [DllImport("user32.dll", SetLastError = true)]
        public static extern bool MessageBeep(
            MessageBeepType type
        );
    }
    
        /// <summary>
        /// перечисление гудков системы
        /// </summary>
        public enum MessageBeepType
    {
        Default = -1,
        Ok = 0x00000000,
        Error = 0x00000010,
        Question = 0x00000020,
        Warning = 0x00000030,
        Information = 0x00000040,
    }


}
﻿using System.ServiceProcess;
using Eureca.Professional.CommonClasses;
using log4net;

namespace Eureca.Professional.ProfessionalAdministration
{
    partial class AdministrationService : ServiceBase
    {
        private static readonly ILog Log = LogManager.GetLogger( typeof( AdministrationService ) );
        public AdministrationService()
        {
            InitializeComponent();
            ServiceName = "AdministrationService";
            Log.Error("Старт из сервиса");
        }

        protected override void OnStart(string[] args)
        {
            // TODO: Add code here to start your service.
        }

        protected override void OnStop()
        {
            // TODO: Add code here to perform any tear-down necessary to stop your service.
        }
    }
}

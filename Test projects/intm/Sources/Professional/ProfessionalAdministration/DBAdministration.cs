﻿using System;
using System.Windows.Forms;
using Eureca.Professional.BaseComponents;
using Eureca.Professional.CommonClasses;
using Eureca.Professional.CommonClasses.Configuration;
using Eureca.Professional.ConnectDBLib;
using Eureca.Integrator.Utility;
using Eureca.Utility.Extensions;
using log4net;
using MessageBox = Eureca.Integrator.Utility.Forms.MessageBox;


namespace Eureca.Professional.ProfessionalAdministration
{
    public class DBAdministration
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof (DBAdministration));
        public static bool ExistConnection(string connectionString)
        {
            var connDB1 = new connDB();
            connDB1.Initialize(connectionString);
            return connDB1.TestConnection;
        }

        public static void CreateDB(string scriptFilePath)
        {
            if (scriptFilePath == null)
                throw new NullReferenceException("scriptFilePath");
            try
            {
                DatabaseSettingsSection _db = DatabaseSettingsSection.GetFromConfigSource();
                string resConnectionString;
                MessageForm.ShowOkMsg("Вам необходимо создать подключение к БД 'Master'");
                while (true)
                {

                    //создаем соединение с БД
                    if (!RunnerApplication.ChangeConnectionToDataBase(_db.ConnectionString, out resConnectionString,el=>el.Contains("master")))
                    {
                        //соединение некорректно
                        if (MessageForm.ShowYesNoQuestionMsg("Соединение некорректно. \n Повторить попытку") ==
                            DialogResult.No)
                        {
                            if (
                                MessageForm.ShowYesNoQuestionMsg(
                                    "Вы хотите продолжить работу и подключиться к уже существующей БД?") ==
                                DialogResult.No)
                            {
                                return;
                            }
                            break;
                        }
                    }
                    else
                    {
                        _db.ConnectionString = resConnectionString;
                        Settings.Save();
                        //соединение корректно устанавливаем БД
                        _db = DatabaseSettingsSection.GetFromConfigSource();
                        //соединение есть, теперь необходимо восстановить БД из BuckUp
                        Eureca.Integrator.Utility.Forms.MarqueeProgressForm.Show("Создание базы данных...", (form) =>
                        {
                            Exception ex = null;
                            if (!DatabaseHelper.ExecFile(scriptFilePath, _db.ConnectionString, out ex))
                            {
                                MessageBox.ShowError("Ошибка установки БД. Сообщите администратору.");
                            }
                        });
                        MessageBox.ShowInfo("Выберите базу Professional.");
                        RunnerApplication.ChangeConnectionToDataBase( _db.ConnectionString, out resConnectionString,el => el.Contains( IntegratorDb.Professional.GetEnumDescription() ) );
                        _db.ConnectionString = resConnectionString;
                        Settings.Save();
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw;
            }
        }

        public static void RestoreDB()
        {
            try
            {
                DatabaseSettingsSection _db = DatabaseSettingsSection.GetFromConfigSource();
                string resConnectionString;
                MessageForm.ShowOkMsg("Вам необходимо создать подключение к БД 'Master'");
                while (true)
                {
                    //создаем соединение с БД
                    if (!RunnerApplication.ChangeConnectionToDataBase( _db.ConnectionString, out resConnectionString, el => el.Contains( "master" ) ))
                    {
                        //соединение некорректно
                        if (MessageForm.ShowYesNoQuestionMsg("Соединение некорректно. \n Повторить попытку") ==
                            DialogResult.No)
                        {
                            if (
                                MessageForm.ShowYesNoQuestionMsg(
                                    "Вы хотите продолжить работу и подключиться к уже существующей БД?") ==
                                DialogResult.No)
                            {
                                return;
                            }
                            break;
                        }
                    }
                    else
                    {
                        _db.ConnectionString = resConnectionString;
                        Settings.Save();
                        //соединение корректно устанавливаем БД
                        _db = DatabaseSettingsSection.GetFromConfigSource();
                        //соединение есть, теперь необходимо восстановить БД из BuckUp
                        var uc = new InstallDBControl(_db.ConnectionString);
                        uc.InitControl();
                        uc.ShowInForm();
                        MessageBox.ShowError("Выберите базу Professional.");
                        RunnerApplication.ChangeConnectionToDataBase( _db.ConnectionString, out resConnectionString, el => el.Contains( IntegratorDb.Professional.GetEnumDescription( ) ) );
                        _db.ConnectionString = resConnectionString;
                        Settings.Save();
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw;
            }
        }
    
    
    }
}
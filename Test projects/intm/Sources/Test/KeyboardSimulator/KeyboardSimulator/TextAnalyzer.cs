﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DiffPlex;
using DiffPlex.Model;

namespace KeyboardSimulator
{
    public class TextAnalyzer
    {
        public static AnalyzerResult Analyze(string strA,string strB)
        {
            var res = new AnalyzerResult();
            Differ differ = new Differ();
            DiffResult diffResult = differ.CreateCharacterDiffs(strA, strB, false, false);

            foreach (DiffBlock block in diffResult.DiffBlocks)
            {

                if (block.DeleteCountA == 0 && block.InsertCountB != 0)
                {
                    //"Лишние      " + block.InsertCountB.ToString() + "  симв. на поз. " + block.InsertStartB.ToString() + "\r\n";
                    res.Remove.Add(new ErrorAnylyzer { Start = block.InsertStartB, Count = block.InsertCountB });

                }

                if (block.InsertCountB == 0 && block.DeleteCountA != 0)
                {
                    //"Пропущено   " + block.DeleteCountA.ToString() + "  симв. на поз. " + block.InsertStartB.ToString() + "\r\n";
                    res.Insert.Add(new ErrorAnylyzer { Start = block.DeleteStartA, Count = block.DeleteCountA });
                }


                if (block.InsertCountB != 0 && block.DeleteCountA != 0)
                {

                    res.Replace.Add(new ErrorAnylyzer { Start = block.InsertStartB, Count = block.InsertCountB });
                    var insertCount = block.DeleteCountA - block.InsertCountB;
                    if (insertCount > 0)
                        res.Insert.Add(new ErrorAnylyzer { Start = block.InsertStartB + block.InsertCountB, Count = insertCount });

                    //res.Replace.Add(new ErrorAnylyzer { Start = block.DeleteStartA + block.InsertCountB, Count = block.DeleteCountA - block.InsertCountB });

                }







                //else
                //{
                //    //"Опечатка в " + block.DeleteCountA.ToString() + "/" + block.InsertCountB.ToString() + " симв. на поз. " + block.InsertStartB.ToString() + "\r\n";
                //    res.Replace.Add(new ErrorAnylyzer {Start = block.InsertStartB, Count = Math.Max(block.DeleteCountA, block.InsertCountB)});
                //}
            }
            res.ErrorCount = res.Remove.Sum(el => el.Count) +
                             res.Replace.Sum(el => el.Count);
                             //res.Insert.Where(el=>el.Start<strB.Length).Sum(item=>item.Count);


            //if (block.DeleteCountA == 0)
            //    {
            //        removeError = block.InsertCountB;
            //    }

            //    if (diffBlock.DeleteCountA > diffBlock.InsertCountB)
            //    {
            //        countError += block.DeleteCountA;
            //    }
            //    else
            //    {
            //        countError += block.InsertCountB;
            //    }

            //}
            return res;
        }
    }


    public class AnalyzerResult
    {
        public AnalyzerResult()
        {
            Remove = new List<ErrorAnylyzer>();
            Insert = new List<ErrorAnylyzer>();
            Replace = new List<ErrorAnylyzer>();
        }
        public int ErrorCount { get; set; }
        public List<ErrorAnylyzer> Remove { get; set; }
        public List<ErrorAnylyzer> Insert { get; set; }
        public List<ErrorAnylyzer> Replace { get; set; }
        public List<ErrorAnylyzer> Common{
            get { return Remove.Union(Replace.ToList()).OrderBy(el => el.Start).ToList(); }
        }
    }

    public class ErrorAnylyzer
    {
        public int Start { get; set; }
        public int Count { get; set; }
    }
}

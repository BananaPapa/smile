﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using DiffPlex;
using DiffPlex.Model;

namespace KeyboardSimulator
{
    internal class MainWindowViewModel:DependencyObject
    {
        private string stringEnter = '\x00b6' + "\r\n";
        private List<HelperKey> NumberFieldH = new List<HelperKey>();
        private List<HelperKey> NumberField1H = new List<HelperKey>();
        private List<HelperKey> NumberField2H = new List<HelperKey>();
        private List<HelperKey> NumberField3H = new List<HelperKey>();
        private List<HelperKey> NumberField4H = new List<HelperKey>();
        private List<HelperKey> NumberField5H = new List<HelperKey>();
        private List<HelperKey> FirstFieldH = new List<HelperKey>();
        private List<HelperKey> SecondFieldH = new List<HelperKey>();
        private List<HelperKey> ThirdFieldH = new List<HelperKey>();
        private List<HelperKey> SpaceFieldH = new List<HelperKey>();

        private void FillHelperKeyList()
        {


            NumberFieldH = new List<HelperKey>()
            {
                new HelperKey(){RusTitle = "Ё",EngTitle = "~",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.FirstFinger, SupportedSymbols = "`~Ёё"},
                new HelperKey(){RusTitle = "1",EngTitle = "1",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.FirstFinger, SupportedSymbols = "1"},
                new HelperKey(){RusTitle = "2",EngTitle = "2",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.SecondFinger, SupportedSymbols = "2"},
                new HelperKey(){RusTitle = "3",EngTitle = "3",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.ThirdFinger, SupportedSymbols = "3"},
                new HelperKey(){RusTitle = "4",EngTitle = "4",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.FourthFinger, SupportedSymbols = "4"},
                new HelperKey(){RusTitle = "5",EngTitle = "5",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.FourthFinger, SupportedSymbols = "5"},
                new HelperKey(){RusTitle = "6",EngTitle = "6",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.SecondFourthFinger, SupportedSymbols = "6"},
                new HelperKey(){RusTitle = "7",EngTitle = "7",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.SecondFourthFinger, SupportedSymbols = "7"},
                new HelperKey(){RusTitle = "8",EngTitle = "8",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.ThirdFinger, SupportedSymbols = "8"},
                new HelperKey(){RusTitle = "9",EngTitle = "9",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.SecondFinger, SupportedSymbols = "9"},
                new HelperKey(){RusTitle = "0",EngTitle = "0",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.FirstFinger, SupportedSymbols = "0"},
                new HelperKey(){RusTitle = "-",EngTitle = "-",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.FirstFinger, SupportedSymbols = "-_"},
                new HelperKey(){RusTitle = "=",EngTitle = "=",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.FirstFinger, SupportedSymbols = "=+"},
                new HelperKey(){RusTitle = "backspace",EngTitle = "backspace",  TypeButton = TypeButton.Func2}
                
            };

            FirstFieldH = new List<HelperKey>()
            {
                new HelperKey(){RusTitle = "Tab",EngTitle = "Tab",  TypeButton = TypeButton.Func1},
                new HelperKey(){RusTitle = "Й",EngTitle = "Q",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.FirstFinger, SupportedSymbols = "ЙйQq"},
                new HelperKey(){RusTitle = "Ц",EngTitle = "W",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.SecondFinger, SupportedSymbols = "WwЦц"},
                new HelperKey(){RusTitle = "У",EngTitle = "E",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.ThirdFinger, SupportedSymbols = "УуEe"},
                new HelperKey(){RusTitle = "К",EngTitle = "R",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.FourthFinger, SupportedSymbols = "RrКк"},
                new HelperKey(){RusTitle = "Е",EngTitle = "T",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.FourthFinger, SupportedSymbols = "ЕеTt"},
                new HelperKey(){RusTitle = "Н",EngTitle = "Y",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.SecondFourthFinger, SupportedSymbols = "YyНн"},
                new HelperKey(){RusTitle = "Г",EngTitle = "U",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.SecondFourthFinger, SupportedSymbols = "ГгUu"},
                new HelperKey(){RusTitle = "Ш",EngTitle = "I",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.ThirdFinger, SupportedSymbols = "IiШш"},
                new HelperKey(){RusTitle = "Щ",EngTitle = "O",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.SecondFinger, SupportedSymbols = "ЩщOo"},
                new HelperKey(){RusTitle = "З",EngTitle = "P",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.FirstFinger, SupportedSymbols = "PpЗз"},
                new HelperKey(){RusTitle = "Х",EngTitle = "[",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.FirstFinger, SupportedSymbols = "Хх{["},
                new HelperKey(){RusTitle = "Ъ",EngTitle = "]",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.FirstFinger, SupportedSymbols = "}]Ъъ"},
                new HelperKey(){RusTitle = "\\",EngTitle = "\\",  TypeButton = TypeButton.Func1, StandartColorButton = StandartColorButton.FirstFinger, SupportedSymbols = "|\\/"}
            };



            SecondFieldH = new List<HelperKey>()
            {
                new HelperKey(){RusTitle = "Caps",EngTitle = "Caps",  TypeButton = TypeButton.Func2},
                new HelperKey(){RusTitle = "Ф",EngTitle = "A",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.FirstFinger, SupportedSymbols = "ФфAa"},
                new HelperKey(){RusTitle = "Ы",EngTitle = "S",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.SecondFinger, SupportedSymbols = "SsЫы"},
                new HelperKey(){RusTitle = "В",EngTitle = "D",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.ThirdFinger, SupportedSymbols = "ВвDd"},
                new HelperKey(){RusTitle = "А",EngTitle = "F",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.FourthFinger, SupportedSymbols = "FfАа"},
                new HelperKey(){RusTitle = "П",EngTitle = "G",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.FourthFinger, SupportedSymbols = "ПпGg"},
                new HelperKey(){RusTitle = "Р",EngTitle = "H",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.SecondFourthFinger, SupportedSymbols = "HhРр"},
                new HelperKey(){RusTitle = "О",EngTitle = "J",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.SecondFourthFinger, SupportedSymbols = "ОоJj"},
                new HelperKey(){RusTitle = "Л",EngTitle = "K",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.ThirdFinger, SupportedSymbols = "KkЛл"},
                new HelperKey(){RusTitle = "Д",EngTitle = "L",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.SecondFinger, SupportedSymbols = "ДдLl"},
                new HelperKey(){RusTitle = "Ж",EngTitle = ";",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.FirstFinger, SupportedSymbols = ":;Жж"},
                new HelperKey(){RusTitle = "Э",EngTitle = "'",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.FirstFinger, SupportedSymbols = "Ээ\"'"},
                new HelperKey(){RusTitle = "Enter",EngTitle = "Enter",  TypeButton = TypeButton.Func2, StandartColorButton = StandartColorButton.FirstFinger, SupportedSymbols = '\x00b6'+"\r\n"}
            };

            ThirdFieldH = new List<HelperKey>()
            {
                new HelperKey(){RusTitle = "Shift",EngTitle = "Shift",  TypeButton = TypeButton.Func3},
                new HelperKey(){RusTitle = "Я",EngTitle = "Z",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.FirstFinger, SupportedSymbols = "ZzЯя"},
                new HelperKey(){RusTitle = "Ч",EngTitle = "X",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.SecondFinger, SupportedSymbols = "ЧчXx"},
                new HelperKey(){RusTitle = "С",EngTitle = "C",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.ThirdFinger, SupportedSymbols = "CcСс"},
                new HelperKey(){RusTitle = "М",EngTitle = "V",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.FourthFinger, SupportedSymbols = "МмVv"},
                new HelperKey(){RusTitle = "И",EngTitle = "B",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.FourthFinger, SupportedSymbols = "bBИи"},
                new HelperKey(){RusTitle = "Т",EngTitle = "N",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.SecondFourthFinger, SupportedSymbols = "тТNn"},
                new HelperKey(){RusTitle = "Ь",EngTitle = "M",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.SecondFourthFinger, SupportedSymbols = "mMьЬ"},
                new HelperKey(){RusTitle = "Б",EngTitle = ",",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.ThirdFinger, SupportedSymbols = "Бб,<"},
                new HelperKey(){RusTitle = "Ю",EngTitle = ".",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.SecondFinger, SupportedSymbols = ">.юЮ"},
                new HelperKey(){RusTitle = ".",EngTitle = "/",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.FirstFinger, SupportedSymbols = "?/.,"},
                new HelperKey(){RusTitle = "Shift",EngTitle = "Shift",  TypeButton = TypeButton.Func3}
            };



            NumberField1H = new List<HelperKey>()
            {
                new HelperKey(){RusTitle = "Num",EngTitle = "Num",  TypeButton = TypeButton.Standart},
                new HelperKey(){RusTitle = "/",EngTitle = "/",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.ThirdFinger, SupportedSymbols = "/"},
                new HelperKey(){RusTitle = "*",EngTitle = "*",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.SecondFinger, SupportedSymbols = "*"},
            };
            NumberField2H = new List<HelperKey>()
            {
                new HelperKey(){RusTitle = "7",EngTitle = "7",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.SecondFourthFinger, SupportedSymbols = "7"},
                new HelperKey(){RusTitle = "8",EngTitle = "8",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.ThirdFinger, SupportedSymbols = "8"},
                new HelperKey(){RusTitle = "9",EngTitle = "9",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.SecondFinger, SupportedSymbols = "9"},
            };

            NumberField3H = new List<HelperKey>()
            {
                new HelperKey(){RusTitle = "4",EngTitle = "4",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.SecondFourthFinger, SupportedSymbols = "4"},
                new HelperKey(){RusTitle = "5",EngTitle = "5",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.ThirdFinger, SupportedSymbols = "5"},
                new HelperKey(){RusTitle = "6",EngTitle = "6",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.SecondFinger, SupportedSymbols = "6"},
            };

            NumberField4H = new List<HelperKey>()
            {
                new HelperKey(){RusTitle = "1",EngTitle = "1",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.SecondFourthFinger, SupportedSymbols = "1"},
                new HelperKey(){RusTitle = "2",EngTitle = "2",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.ThirdFinger, SupportedSymbols = "2"},
                new HelperKey(){RusTitle = "3",EngTitle = "3",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.SecondFinger, SupportedSymbols = "3"},
            };
            NumberField5H = new List<HelperKey>()
            {
                new HelperKey(){RusTitle = "0",EngTitle = "0",  TypeButton = TypeButton.Func2, StandartColorButton = StandartColorButton.BigFinger, SupportedSymbols = "0"},
                new HelperKey(){RusTitle = "Del",EngTitle = ".",  TypeButton = TypeButton.Standart, StandartColorButton = StandartColorButton.ThirdFinger, SupportedSymbols = "."},
            };


            SpaceFieldH = new List<HelperKey>()
            {
                new HelperKey(){RusTitle = "",EngTitle = "",  TypeButton = TypeButton.Func1},
                new HelperKey(){RusTitle = "",EngTitle = "",  TypeButton = TypeButton.Func1},
                new HelperKey(){RusTitle = "",EngTitle = "",  TypeButton = TypeButton.Func1},
                new HelperKey(){RusTitle = "   ",EngTitle = "   ",  TypeButton = TypeButton.Func4, StandartColorButton = StandartColorButton.BigFinger, SupportedSymbols = " "},
                new HelperKey(){RusTitle = "",EngTitle = "",  TypeButton = TypeButton.Func1},
                new HelperKey(){RusTitle = "",EngTitle = "",  TypeButton = TypeButton.Func1},
                new HelperKey(){RusTitle = "",EngTitle = "",  TypeButton = TypeButton.Func1}
                
            };
        }

        //List<KeyboardButton> NumberField;
        //List<KeyboardButton> FirstField;
        //List<KeyboardButton> SecondField;
        //List<KeyboardButton> ThirdField;
        //List<KeyboardButton> NumberField1;
        //List<KeyboardButton> NumberField2;
        //List<KeyboardButton> NumberField3;
        //List<KeyboardButton> NumberField4;
        //List<KeyboardButton> NumberField5;
        //List<KeyboardButton> SpaceField;

        public string InputText
        {
            get { return (string)GetValue(InputTextProperty); }
            set { SetValue(InputTextProperty, value); }
        }

        // Using a DependencyProperty as the backing store for InputText.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty InputTextProperty =
            DependencyProperty.Register("InputText", typeof(string), typeof(MainWindowViewModel), new UIPropertyMetadata(null));

        public string OutputText
        {
            get { return (string)GetValue(OutputTextProperty); }
            set { SetValue(OutputTextProperty, value); }
        }

        // Using a DependencyProperty as the backing store for OutputText.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty OutputTextProperty =
            DependencyProperty.Register("OutputText", typeof(string), typeof(MainWindowViewModel), new PropertyMetadata(String.Empty/*, OutputText_callback, OutputText_coerce*/));

        //private static object OutputText_coerce(DependencyObject d, object basevalue)
        //{
        //    MainWindowViewModel mainWindowViewModel = (MainWindowViewModel)d;

        //    return basevalue;
        //}

        //private static void OutputText_callback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        //{
        //    MainWindowViewModel mainWindowViewModel = (MainWindowViewModel) d;
            
        //}

        public bool IsEndless { get; set;}

        public int SymbolPerSecond
        {
            get { return (int)GetValue(SymbolPerSecondProperty); }
            set { SetValue(SymbolPerSecondProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SymbolPerSecond.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SymbolPerSecondProperty =
            DependencyProperty.Register("SymbolPerSecond", typeof(int), typeof(MainWindowViewModel), new UIPropertyMetadata(0));




        public int CountError
        {
            get { return (int)GetValue(CountErrorProperty); }
            set { SetValue(CountErrorProperty, value); }
        }

        // Using a DependencyProperty as the backing store for CountError.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CountErrorProperty =
            DependencyProperty.Register("CountError", typeof(int), typeof(MainWindowViewModel), new UIPropertyMetadata(0));  

        

        public string TimePeriod
        {
            get { return (string)GetValue(TimePeriodProperty); }
            set { SetValue(TimePeriodProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TimePeriod.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TimePeriodProperty =
            DependencyProperty.Register("TimePeriod", typeof(string), typeof(MainWindowViewModel), new UIPropertyMetadata("00:00:00"));




        public string RemainingTime
        {
            get { return (string)GetValue(RemainingTimeProperty); }
            set { SetValue(RemainingTimeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for RemainingTime.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty RemainingTimeProperty =
            DependencyProperty.Register("RemainingTime", typeof(string), typeof(MainWindowViewModel), new UIPropertyMetadata(string.Empty));

        


        public DateTime DateTimePeriod{ get; set;}


        public bool IsNotExamen
        {
            get { return (bool)GetValue(IsNotExamenProperty); }
            set { SetValue(IsNotExamenProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsNotExamen.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsNotExamenProperty =
            DependencyProperty.Register("IsNotExamen", typeof(bool), typeof(MainWindowViewModel), new UIPropertyMetadata(true));



        

        public List<KeyboardButton> NumberField
        {
            get { return (List<KeyboardButton>)GetValue(NumberFieldProperty); }
            set { SetValue(NumberFieldProperty, value); }
        }

        // Using a DependencyProperty as the backing store for NumberField.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty NumberFieldProperty =
            DependencyProperty.Register("NumberField", typeof(List<KeyboardButton>), typeof(MainWindowViewModel), new UIPropertyMetadata(null));




        public List<KeyboardButton> FirstField
        {
            get { return (List<KeyboardButton>)GetValue(FirstFieldProperty); }
            set { SetValue(FirstFieldProperty, value); }
        }

        // Using a DependencyProperty as the backing store for FirstField.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty FirstFieldProperty =
            DependencyProperty.Register("FirstField", typeof(List<KeyboardButton>), typeof(MainWindowViewModel), new UIPropertyMetadata(null));



        public List<KeyboardButton> SecondField
        {
            get { return (List<KeyboardButton>)GetValue(SecondFieldProperty); }
            set { SetValue(SecondFieldProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SecondField.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SecondFieldProperty =
            DependencyProperty.Register("SecondField", typeof(List<KeyboardButton>), typeof(MainWindowViewModel), new UIPropertyMetadata(null));




        public List<KeyboardButton> ThirdField
        {
            get { return (List<KeyboardButton>)GetValue(ThirdFieldProperty); }
            set { SetValue(ThirdFieldProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Thirdfield.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ThirdFieldProperty =
            DependencyProperty.Register("ThirdField", typeof(List<KeyboardButton>), typeof(MainWindowViewModel), new UIPropertyMetadata(null));



        public int CaretIndex
        {
            get { return (int)GetValue(CaretIndexProperty); }
            set { SetValue(CaretIndexProperty, value); }
        }

        // Using a DependencyProperty as the backing store for CaretIndex.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CaretIndexProperty =
            DependencyProperty.Register("CaretIndex", typeof(int), typeof(MainWindowViewModel), new UIPropertyMetadata(0/*, CaretIndex_callback*/));

        //private static void CaretIndex_callback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        //{
            
        //}


        public List<KeyboardButton> PrevKeyboardButtons = new List<KeyboardButton>();

        public List<KeyboardButton> SpaceField
        {
            get { return (List<KeyboardButton>)GetValue(SpaceFieldProperty); }
            set { SetValue(SpaceFieldProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SpaceField.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SpaceFieldProperty =
            DependencyProperty.Register("SpaceField", typeof(List<KeyboardButton>), typeof(MainWindowViewModel), new UIPropertyMetadata(null));



        public List<KeyboardButton> NumberField1
        {
            get { return (List<KeyboardButton>)GetValue(NumberField1Property); }
            set { SetValue(NumberField1Property, value); }
        }

        // Using a DependencyProperty as the backing store for NumberField1.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty NumberField1Property =
            DependencyProperty.Register("NumberField1", typeof(List<KeyboardButton>), typeof(MainWindowViewModel), new UIPropertyMetadata(null));




        public List<KeyboardButton> NumberField2
        {
            get { return (List<KeyboardButton>)GetValue(NumberField2Property); }
            set { SetValue(NumberField2Property, value); }
        }

        // Using a DependencyProperty as the backing store for NumberField2.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty NumberField2Property =
            DependencyProperty.Register("NumberField2", typeof(List<KeyboardButton>), typeof(MainWindowViewModel), new UIPropertyMetadata(null));




        public List<KeyboardButton> NumberField3
        {
            get { return (List<KeyboardButton>)GetValue(NumberField3Property); }
            set { SetValue(NumberField3Property, value); }
        }

        // Using a DependencyProperty as the backing store for NumberField3.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty NumberField3Property =
            DependencyProperty.Register("NumberField3", typeof(List<KeyboardButton>), typeof(MainWindowViewModel), new UIPropertyMetadata(null));



        public List<KeyboardButton> NumberField4
        {
            get { return (List<KeyboardButton>)GetValue(NumberField4Property); }
            set { SetValue(NumberField4Property, value); }
        }

        // Using a DependencyProperty as the backing store for NumberField4.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty NumberField4Property =
            DependencyProperty.Register("NumberField4", typeof(List<KeyboardButton>), typeof(MainWindowViewModel), new UIPropertyMetadata(null));



        public List<KeyboardButton> NumberField5
        {
            get { return (List<KeyboardButton>)GetValue(NumberField5Property); }
            set { SetValue(NumberField5Property, value); }
        }

        // Using a DependencyProperty as the backing store for NumberField5.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty NumberField5Property =
            DependencyProperty.Register("NumberField5", typeof(List<KeyboardButton>), typeof(MainWindowViewModel), new UIPropertyMetadata(null));

        //private MainWindow _mainWindow;
        public MainWindowViewModel()
        {
            
            FillHelperKeyList();
            NumberField = FillListButton(NumberFieldH);
            FirstField = FillListButton(FirstFieldH);
            SecondField = FillListButton(SecondFieldH);
            ThirdField = FillListButton(ThirdFieldH);
            NumberField1 = FillListButton(NumberField1H);
            NumberField2 = FillListButton(NumberField2H);
            NumberField3 = FillListButton(NumberField3H);
            NumberField4 = FillListButton(NumberField4H);
            NumberField5 = FillListButton(NumberField5H);
            SpaceField = FillListButton(SpaceFieldH);


        }


        List<KeyboardButton> FillListButton(List<HelperKey> helperKeyList)
        {
            List<KeyboardButton> keyboardButtonList = new List<KeyboardButton>();
            foreach (HelperKey helperKey in helperKeyList)
            {
                if (helperKey.TypeButton == TypeButton.Standart)
                {
                    keyboardButtonList.Add(new StandartButton(helperKey));
                    continue;
                }
                if (helperKey.TypeButton == TypeButton.Func1)
                {
                    keyboardButtonList.Add(new FuncButton1(helperKey));
                    continue;
                }
                if (helperKey.TypeButton == TypeButton.Func2)
                {
                    keyboardButtonList.Add(new FuncButton2(helperKey));
                    continue;
                }
                if (helperKey.TypeButton == TypeButton.Func3)
                {
                    keyboardButtonList.Add(new FuncButton3(helperKey));
                    continue;
                }
                if (helperKey.TypeButton == TypeButton.Func4)
                {
                    keyboardButtonList.Add(new FuncButton4(helperKey));
                    continue;
                }
            }
            return keyboardButtonList;
        }


        public void ClearPrevKeyboardButton()
        {
            foreach (KeyboardButton keyboardButton in PrevKeyboardButtons)
            {
                keyboardButton.IsNextPressButton = false;
            }
            PrevKeyboardButtons.Clear();
        }

        public void FindNextKeyboardButton(string findSymbol)
        {
            if (!IsNotExamen) return;
            PrevKeyboardButtons.Clear();


            //на энтер отдельный поиск так как символ выбранный у нас для показа перевода каретки при посике в любой строчке выдаёт что найден по нулевой позиции
            if (findSymbol=='\x00b6'.ToString())
            {
                foreach (KeyboardButton keyboardButton in SecondField)
                {
                    if ((keyboardButton.HelperKey.RusTitle=="Enter"))
                    {
                        PrevKeyboardButtons.Add(keyboardButton);
                    }
                }
                foreach (KeyboardButton keyboardButton in PrevKeyboardButtons)
                {
                    keyboardButton.IsNextPressButton = true;
                }
                return; 
            }

            foreach (KeyboardButton keyboardButton in FirstField)
            {
                if ((keyboardButton.HelperKey.SupportedSymbols != null) && (keyboardButton.HelperKey.SupportedSymbols.IndexOf(findSymbol) > -1))
                {
                    PrevKeyboardButtons.Add(keyboardButton);
                }
            }
            foreach (KeyboardButton keyboardButton in SecondField)
            {
                if ((keyboardButton.HelperKey.SupportedSymbols != null) && (keyboardButton.HelperKey.SupportedSymbols.IndexOf(findSymbol) > -1))
                {
                    
                    PrevKeyboardButtons.Add(keyboardButton);
                }
            }
            foreach (KeyboardButton keyboardButton in ThirdField)
            {
                if ((keyboardButton.HelperKey.SupportedSymbols != null) && (keyboardButton.HelperKey.SupportedSymbols.IndexOf(findSymbol) > -1))
                {
                    PrevKeyboardButtons.Add(keyboardButton);
                }
            }
            foreach (KeyboardButton keyboardButton in NumberField)
            {
                if ((keyboardButton.HelperKey.SupportedSymbols != null) && (keyboardButton.HelperKey.SupportedSymbols.IndexOf(findSymbol) > -1))
                {
                    PrevKeyboardButtons.Add(keyboardButton);
                }
            }
            foreach (KeyboardButton keyboardButton in NumberField1)
            {
                if ((keyboardButton.HelperKey.SupportedSymbols != null) && (keyboardButton.HelperKey.SupportedSymbols.IndexOf(findSymbol) > -1))
                {
                    PrevKeyboardButtons.Add(keyboardButton);
                }
            }
            foreach (KeyboardButton keyboardButton in NumberField2)
            {
                if ((keyboardButton.HelperKey.SupportedSymbols != null) && (keyboardButton.HelperKey.SupportedSymbols.IndexOf(findSymbol) > -1))
                {
                    PrevKeyboardButtons.Add(keyboardButton);
                }
            }
            foreach (KeyboardButton keyboardButton in NumberField3)
            {
                if ((keyboardButton.HelperKey.SupportedSymbols != null) && (keyboardButton.HelperKey.SupportedSymbols.IndexOf(findSymbol) > -1))
                {
                    PrevKeyboardButtons.Add(keyboardButton);
                }
            }
            foreach (KeyboardButton keyboardButton in NumberField4)
            {
                if ((keyboardButton.HelperKey.SupportedSymbols != null) && (keyboardButton.HelperKey.SupportedSymbols.IndexOf(findSymbol) > -1))
                {
                    PrevKeyboardButtons.Add(keyboardButton);
                }
            }
            foreach (KeyboardButton keyboardButton in NumberField5)
            {
                if ((keyboardButton.HelperKey.SupportedSymbols != null) && (keyboardButton.HelperKey.SupportedSymbols.IndexOf(findSymbol) > -1))
                {
                    PrevKeyboardButtons.Add(keyboardButton);
                }
            }
            foreach (KeyboardButton keyboardButton in SpaceField)
            {
                if ((keyboardButton.HelperKey.SupportedSymbols != null) && (keyboardButton.HelperKey.SupportedSymbols.IndexOf(findSymbol) > -1))
                {
                    PrevKeyboardButtons.Add(keyboardButton);
                }
            }

            foreach (KeyboardButton keyboardButton in PrevKeyboardButtons)
            {
                keyboardButton.IsNextPressButton = true;
            }
        }

        //#region The RoutedUICommands

        //public static RoutedUICommand KeyPressedCommand = new RoutedUICommand();

        //#endregion The RoutedUICommands
    }
}

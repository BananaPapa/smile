﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace KeyboardSimulator
{
    internal class KeyboardButton:DependencyObject
    {
        public KeyboardButton(HelperKey helperKey)
        {
            _helperKey = helperKey;
            InputLanguageManager.Current.InputLanguageChanged += inputLanguageManager_InputLanguageChanged;
            _isNextPressButton = false;
            CheckLang(InputLanguageManager.Current.CurrentInputLanguage);
            IsNextPressButton = false;
        }




        public LinearGradientBrush BackgroundButton
        {
            get { return (LinearGradientBrush)GetValue(BackgroundButtonProperty); }
            set { SetValue(BackgroundButtonProperty, value); }
        }

        // Using a DependencyProperty as the backing store for BackgroundButton.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty BackgroundButtonProperty =
            DependencyProperty.Register("BackgroundButton", typeof(LinearGradientBrush), typeof(KeyboardButton), new UIPropertyMetadata(null));

        
        public string Title
        {
            get { return (string)GetValue(Title1Property); }
            set { SetValue(Title1Property, value); }
        }

        // Using a DependencyProperty as the backing store for Title.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty Title1Property =
            DependencyProperty.Register("Title", typeof(string), typeof(KeyboardButton), new UIPropertyMetadata(null));

        public bool IsPress
        {
            get { return (bool)GetValue(IsPressProperty); }
            set { SetValue(IsPressProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsPress.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsPressProperty =
            DependencyProperty.Register("IsPress", typeof(bool), typeof(KeyboardButton), new UIPropertyMetadata(null));


        private Point point1=new Point(0,0);
        private Point point2 = new Point(0.5, 0.5);
        
        private bool _isNextPressButton;

        private HelperKey _helperKey;
        public HelperKey HelperKey
        {
            get
            {
                return _helperKey;
            }
        }

        public bool IsNextPressButton
        {
            get
            {
                return _isNextPressButton;
            }
            set
            {
                _isNextPressButton = value;
                if (_isNextPressButton)
                {
                    switch (_helperKey.StandartColorButton)
                    {
                        case StandartColorButton.FirstFinger:

                            BackgroundButton = new LinearGradientBrush(Colors.White, Color.FromArgb(255, 186, 136, 147), point1, point2);

                            //BackgroundButton = new LinearGradientBrush(Colors.White, Colors.Black, point1, point2); 
                            return;
                        case StandartColorButton.SecondFinger:
                            BackgroundButton = new LinearGradientBrush(Colors.White, Color.FromArgb(255, 189, 153, 126), point1, point2);

                            return;
                        case StandartColorButton.ThirdFinger:
                            BackgroundButton = new LinearGradientBrush(Colors.White, Color.FromArgb(255, 134, 162, 104), point1, point2);

                            return;
                        case StandartColorButton.FourthFinger:
                            BackgroundButton = new LinearGradientBrush(Colors.White, Color.FromArgb(255, 185, 159, 150), point1, point2);

                            return;
                        case StandartColorButton.SecondFourthFinger:
                            BackgroundButton = new LinearGradientBrush(Colors.White, Color.FromArgb(255, 107, 135, 165), point1, point2);

                            return;
                        case StandartColorButton.BigFinger:
                            BackgroundButton = new LinearGradientBrush(Colors.White, Color.FromArgb(255, 150, 125, 158), point1, point2);

                            return;
                        case StandartColorButton.FuncButton:
                            BackgroundButton = new LinearGradientBrush(Colors.White, Color.FromArgb(255, 128, 128, 128), point1, point2);

                            return;
                        default:
                            BackgroundButton = new LinearGradientBrush(Colors.White, Color.FromArgb(255, 128, 128, 128), point1, point2);

                            return;
                    }


                    //BackgroundButton = new LinearGradientBrush(Colors.White, Colors.Black, point1,point2); 
                    //BackgroundButton = Brushes.Black;
                    return;
                }
                else
                {
                    switch (_helperKey.StandartColorButton)
                    {
                        case StandartColorButton.FirstFinger:

                            BackgroundButton = new LinearGradientBrush(Colors.White, Color.FromArgb(255, 248, 182, 186), point1, point2);
                            
                            //BackgroundButton = new LinearGradientBrush(Colors.White, Colors.Black, point1, point2); 
                            return;
                        case StandartColorButton.SecondFinger:
                            BackgroundButton = new LinearGradientBrush(Colors.White, Color.FromArgb(255, 251, 204, 168), point1, point2);
                            
                            return;
                        case StandartColorButton.ThirdFinger:
                            BackgroundButton = new LinearGradientBrush(Colors.White, Color.FromArgb(255, 177, 215, 138), point1, point2);
                            
                            return;
                        case StandartColorButton.FourthFinger:
                            BackgroundButton = new LinearGradientBrush(Colors.White, Color.FromArgb(255, 145, 211, 200), point1, point2);
                            
                            return;
                        case StandartColorButton.SecondFourthFinger:
                            BackgroundButton = new LinearGradientBrush(Colors.White, Color.FromArgb(255, 142, 179, 220), point1, point2);
                            
                            return;
                        case StandartColorButton.BigFinger:
                            BackgroundButton = new LinearGradientBrush(Colors.White, Color.FromArgb(255, 197, 165, 206), point1, point2);
                            
                            return;
                        case StandartColorButton.FuncButton:
                            BackgroundButton = new LinearGradientBrush(Colors.White, Colors.DarkGray, point1, point2);
                            
                            return;
                        default:
                            BackgroundButton = new LinearGradientBrush(Colors.White, Colors.DarkGray, point1, point2);
                            
                            return;
                    }

                }
            }
        }

        
        public string RusTitle
        {
            get
            {
                return _helperKey.RusTitle;
            }
        }

        public string EngTitle
        {
            get
            {
                return _helperKey.EngTitle;
            }
        }



        void inputLanguageManager_InputLanguageChanged(object sender, InputLanguageEventArgs e)
        {
            CheckLang(e.NewLanguage);
        }


        private void CheckLang(CultureInfo cultureInfo)
        {
            if (cultureInfo.KeyboardLayoutId == 1049)
            {
                Title = _helperKey.RusTitle;
                return;
            }
            if (cultureInfo.KeyboardLayoutId == 1033)
            {
                Title = _helperKey.EngTitle;
                return;
            }
            MessageBox.Show("Раскладка клавиатуры не подходящая");
        }







    }

    internal class StandartButton : KeyboardButton
    {
        public StandartButton(HelperKey helperKey) : base(helperKey)
        {
        }
    }

    internal class FuncButton1 : KeyboardButton
    {
        public FuncButton1(HelperKey helperKey) : base(helperKey)
        {
        }
    }
    internal class FuncButton2 : KeyboardButton
    {
        public FuncButton2(HelperKey helperKey) : base(helperKey)
        {
        }
    }
    internal class FuncButton3 : KeyboardButton
    {
        public FuncButton3(HelperKey helperKey) : base(helperKey)
        {
        }
    }
    internal class FuncButton4 : KeyboardButton
    {
        public FuncButton4(HelperKey helperKey) : base(helperKey)
        {
        }
    }

}

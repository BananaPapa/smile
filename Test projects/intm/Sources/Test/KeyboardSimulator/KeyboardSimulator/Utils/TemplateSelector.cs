﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows;
using System.Windows.Controls;

namespace KeyboardSimulator.Utils
{
    public class ListBoxDataTemplateSelector : DataTemplateSelector
    {
        public DataTemplate StandartButton { get; set; }
        public DataTemplate FuncButton1 { get; set; }
        public DataTemplate FuncButton2 { get; set; }
        public DataTemplate FuncButton3 { get; set; }
        public DataTemplate FuncButton4 { get; set; }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            if (item is StandartButton)
                return StandartButton;
            if (item is FuncButton1)
                return FuncButton1;
            if (item is FuncButton2)
                return FuncButton2;
            if (item is FuncButton3)
                return FuncButton3;
            if (item is FuncButton4)
                return FuncButton4;

            if (item is KeyboardButton)
                throw new Exception();

            return null;
        }
    }
}

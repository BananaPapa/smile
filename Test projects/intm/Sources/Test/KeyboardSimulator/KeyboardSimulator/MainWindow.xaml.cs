﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Globalization;
using System.Windows.Threading;
using DiffPlex;
using DiffPlex.Model;
using System.IO;

namespace KeyboardSimulator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Events
        public event EventHandler<FinishEventArgs> FinishTest;
        
        #endregion    

        #region Fields
        
        private Stopwatch sWatch = new Stopwatch();
        private string _originalText;

        //таймер для ежесекундной проверки
        private DispatcherTimer dispatherTimer = new DispatcherTimer();
        //таймер срабатывает когда время теста закончилось
        private DispatcherTimer dispatherTimerRemaningTime = new DispatcherTimer();
        //private DateTime timeStart;
        private MainWindowViewModel _mainWindowViewModel;
        private TimeSpan _remaningTime;
        private string stringEnter = '\x00b6' + "\r\n";
        //private bool _isEndless;
        #endregion

        #region Properties
        
        private string SimpleInputText
        {
            get
            {
                return _mainWindowViewModel.InputText.Replace(stringEnter, "\n");
            }
        }

        private string SimpleOutputText
        {
            get
            {
                //return _mainWindowViewModel.OutputText.Replace(stringEnter, "\n");
                return _mainWindowViewModel.OutputText.Replace(stringEnter, "\n");
            }
        }

        private bool _isNotExamen = true;
        #endregion

        #region Constructors
        
        public MainWindow()
        {

            InitializeComponent();
            inputTextBox.SelectionStart = 2;
            inputTextBox.SelectionLength = 4;
        }

        public MainWindow(string inputText, TimeSpan timeSpan = new TimeSpan(), bool isExamen = false)
            : this()
        {

#if DEBUG
            inputTextBox.CommandBindings.Clear();
            outputTextBox.CommandBindings.Clear();
#endif




            Closed += new EventHandler(MainWindow_Closed);
            _originalText = inputText;
            _isNotExamen = !isExamen;
            _remaningTime = timeSpan;
            this.DataContext = _mainWindowViewModel = new MainWindowViewModel();
            //{
            //    NumberField = NumberField,
            //    FirstField = FirstField,
            //    SecondField = SecondField,
            //    ThirdField = ThirdField,
            //    NumberField1 = NumberField1,
            //    NumberField2 = NumberField2,
            //    NumberField3 = NumberField3,
            //    NumberField4 = NumberField4,
            //    NumberField5 = NumberField5,
            //    SpaceField = SpaceField
            //};

            //char charEnter = '\x00b6';

            inputText = inputText.Replace("\r\n", "\n").Replace("\n", stringEnter);
            dispatherTimer.Interval = new TimeSpan(0, 0, 0, 0, 300);
            dispatherTimer.Tick += dispatherTimer_Tick;
            dispatherTimerRemaningTime.Interval = timeSpan;
            dispatherTimerRemaningTime.Tick += new EventHandler(dispatherTimerRemaningTime_Tick);
            _mainWindowViewModel.InputText = inputText;
            _mainWindowViewModel.IsNotExamen = _isNotExamen;
            _mainWindowViewModel.IsEndless = timeSpan.Ticks == 0 ? true : false;
            ((MainWindowViewModel)this.DataContext).RemainingTime = new DateTime(_remaningTime.Ticks).ToLongTimeString();
            outputTextBox.Focus();
            //IInputElement focusedElement = FocusManager.GetFocusedElement(outputTextBox);
            //FocusManager.SetFocusedElement(outputTextBox, focusedElement);
            WindowState = System.Windows.WindowState.Maximized;
            WindowStyle = System.Windows.WindowStyle.ThreeDBorderWindow;
        }

        #endregion

        #region Methods
        public void InvokeFinishTest(FinishEventArgs e)
        {
            EventHandler<FinishEventArgs> handler = FinishTest;
            if (handler != null) handler(this, e);
        }
        #endregion

        #region EventHandlers
        void dispatherTimerRemaningTime_Tick(object sender, EventArgs e)
        {
            dispatherTimerRemaningTime.Stop();
            this.Close();
        }

        void MainWindow_Closed(object sender, EventArgs e)
        {



            if ((sWatch.IsRunning) || (dispatherTimer.IsEnabled))
            {
                sWatch.Stop();
                dispatherTimer.Stop();
                Counting(false);
            }
            //DateTime dateTimeNow = DateTime.Now;

            //if (SimpleInputText.Length == SimpleOutputText.Length)
            //{
            //    MessageBox.Show(
            //        String.Format(
            //            "Тест закончен.\nКоличество ошибок - {0}.\nВремя выполнения - {1}.\nЗнаков в минуту {2}",
            //            _mainWindowViewModel.CountError,
            //            _mainWindowViewModel.TimePeriod,
            //            _mainWindowViewModel.SymbolPerSecond));
            //}
            //else
            //{
            //    MessageBox.Show(
            //        String.Format(
            //            "Тест прерван.\nКоличество ошибок - {0}.\nВремя выполнения - {1}.\nЗнаков в минуту {2}",
            //            _mainWindowViewModel.CountError,
            //            _mainWindowViewModel.TimePeriod,
            //            _mainWindowViewModel.SymbolPerSecond));

            //}
            InvokeFinishTest(new FinishEventArgs(_mainWindowViewModel.CountError, _mainWindowViewModel.DateTimePeriod, new TimeSpan(_mainWindowViewModel.DateTimePeriod.Ticks), _mainWindowViewModel.OutputText, _originalText, _mainWindowViewModel.SymbolPerSecond));
        }

        void dispatherTimer_Tick(object sender, EventArgs e)
        {
            Counting();
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {

           // Clipboard.Clear();

            
            //if (_mainWindowViewModel.InputText.Replace("=\r\n", "\r\n").Length > _mainWindowViewModel.OutputText.Length)
            KeyboardButton keyboardButton = FindKeyboardButtonUp(e);

            if (keyboardButton != null)
            {
                keyboardButton.IsPress = true;
            }



            if (e.Key == Key.Return)
            {
                //SimpleOutputText.Replace("\n", stringEnter);'\x00b6'
                //_mainWindowViewModel.OutputText = _mainWindowViewModel.OutputText.Replace(stringEnter, "\r\n").Replace("\r\n", "\n").Replace("\n", stringEnter);
                int selectionStart = outputTextBox.SelectionStart;
                _mainWindowViewModel.OutputText = _mainWindowViewModel.OutputText.Insert(selectionStart, '\x00b6'.ToString());
                outputTextBox.SelectionStart = selectionStart + 1;
            }
            else
            {

                int index = 0;


                int selectionStart = outputTextBox.SelectionStart;
                while (true)
                {

                    if (index >= _mainWindowViewModel.OutputText.Length) break;
                    int indexOf = _mainWindowViewModel.OutputText.IndexOf("\r\n", index);
                    if (indexOf > -1)
                    {
                        index = indexOf + 1;
                        if (index == 0)
                        {
                            _mainWindowViewModel.OutputText = _mainWindowViewModel.OutputText.Remove(0, 2);
                            if ((selectionStart == 0) || (selectionStart == 1) || (selectionStart == 2))
                            {
                                selectionStart = 0;

                            }
                            else
                            {
                                if (selectionStart > index)
                                    selectionStart -= 2;

                            }
                        }
                        else
                        {
                            char ch = _mainWindowViewModel.OutputText[indexOf - 1];
                            if (ch != '\x00b6')
                            {
                                _mainWindowViewModel.OutputText = _mainWindowViewModel.OutputText.Remove(indexOf, 2);
                                if ((selectionStart == 0) || (selectionStart == 1) || (selectionStart == 2))
                                {
                                    selectionStart = 0;
                                }
                                else
                                {
                                    if (selectionStart > index)
                                        selectionStart -= 2;

                                }
                            }
                        }

                    }
                    else
                    {
                        break;
                    }
                }
                outputTextBox.SelectionStart = selectionStart;


                index = 0;
                selectionStart = outputTextBox.SelectionStart;
                while (true)
                {
                    if (index >= _mainWindowViewModel.OutputText.Length) break;
                    int indexOf = _mainWindowViewModel.OutputText.IndexOf('\x00b6', index);
                    if (indexOf > -1)
                    {
                        index = indexOf + 1;
                        if (index == 0)
                        {
                            _mainWindowViewModel.OutputText = _mainWindowViewModel.OutputText.Remove(0, 1);
                            if ((selectionStart == 0) || (selectionStart == 1))
                            {
                                selectionStart = 0;

                            }
                            else
                            {
                                if (selectionStart > index)
                                    selectionStart -= 1;

                            }
                        }
                        else
                        {
                            if ((indexOf + 2 > _mainWindowViewModel.OutputText.Length) || !((_mainWindowViewModel.OutputText[indexOf + 1] == '\r') && (_mainWindowViewModel.OutputText[indexOf + 2] == '\n')))
                            {
                                _mainWindowViewModel.OutputText = _mainWindowViewModel.OutputText.Remove(indexOf, 1);
                                if ((selectionStart == 0) || (selectionStart == 1))
                                {
                                    selectionStart = 0;
                                }
                                else
                                {
                                    if (selectionStart >= index)
                                        selectionStart -= 1;

                                }
                            }
                        }

                    }
                    else
                    {
                        break;
                    }
                }
                outputTextBox.SelectionStart = selectionStart;



            }
        }

        //срабатывает для того чтобы проверять всё, в том числе и для подсчёта прошедшего или оставшегося времени
        private void StartTimer()
        {
            if (!dispatherTimer.IsEnabled)
            {
                //timeStart = DateTime.Now;
                dispatherTimer.Start();

                sWatch.Start();
            }
            if (!dispatherTimerRemaningTime.IsEnabled)
            {
                if (!_mainWindowViewModel.IsEndless)
                {
                    dispatherTimerRemaningTime.Start();
                }
            }
        }


        private void Window_KeyUp(object sender, KeyEventArgs e)
        {


            double fff = outputTextBox.GetLineIndexFromCharacterIndex(outputTextBox.SelectionStart);
            if (fff > 0)
            {
                //высота одной строчки
                double getLastVisibleLineIndex = inputTextBox.GetLastVisibleLineIndex() + 1;
                double ddd = inputTextBox.ActualHeight / getLastVisibleLineIndex;
                //получим высоту от начала текстобкса до следующей строки селектедИндекс текстбокса
                double aaa = outputTextBox.GetLineIndexFromCharacterIndex(outputTextBox.SelectionStart) + 2;
                double ccc = aaa * ddd;
                double eee = ccc - inputScrollViewer.ActualHeight;

                inputScrollViewer.ScrollToVerticalOffset(eee);
            }


            if (e.Key != Key.Return)
            {

                int index = 0;

                if ((outputTextBox.SelectionStart != 0) && (_mainWindowViewModel.OutputText != string.Empty) && (_mainWindowViewModel.OutputText[outputTextBox.SelectionStart - 1] == '\x00b6'))
                {
                    outputTextBox.SelectionStart++;
                }

                int selectionStart = outputTextBox.SelectionStart;
                while (true)
                {
                    if (index >= _mainWindowViewModel.OutputText.Length) break;
                    int indexOf = _mainWindowViewModel.OutputText.IndexOf("\r\n", index);
                    if (indexOf > -1)
                    {
                        index = indexOf + 1;
                        if (index == 0)
                        {
                            _mainWindowViewModel.OutputText = _mainWindowViewModel.OutputText.Remove(0, 2);
                            if ((selectionStart == 0) || (selectionStart == 1) || (selectionStart == 2))
                            {
                                selectionStart = 0;

                            }
                            else
                            {
                                if (selectionStart > index)
                                    selectionStart -= 2;

                            }
                        }
                        else
                        {
                            char ch = _mainWindowViewModel.OutputText[indexOf - 1];
                            if (ch != '\x00b6')
                            {
                                _mainWindowViewModel.OutputText = _mainWindowViewModel.OutputText.Remove(indexOf, 2);
                                if ((selectionStart == 0) || (selectionStart == 1) || (selectionStart == 2))
                                {
                                    selectionStart = 0;
                                }
                                else
                                {
                                    if (selectionStart > index)
                                        selectionStart -= 2;

                                }
                            }
                        }

                    }
                    else
                    {
                        break;
                    }
                }
                outputTextBox.SelectionStart = selectionStart;


                index = 0;
                selectionStart = outputTextBox.SelectionStart;
                while (true)
                {
                    if (index >= _mainWindowViewModel.OutputText.Length) break;
                    int indexOf = _mainWindowViewModel.OutputText.IndexOf('\x00b6', index);
                    if (indexOf > -1)
                    {
                        index = indexOf + 1;
                        if (index == 0)
                        {
                            _mainWindowViewModel.OutputText = _mainWindowViewModel.OutputText.Remove(0, 1);
                            if ((selectionStart == 0) || (selectionStart == 1))
                            {
                                selectionStart = 0;

                            }
                            else
                            {
                                if (selectionStart > index)
                                    selectionStart -= 1;

                            }
                        }
                        else
                        {
                            if ((indexOf + 2 > _mainWindowViewModel.OutputText.Length) || !((_mainWindowViewModel.OutputText[indexOf + 1] == '\r') && (_mainWindowViewModel.OutputText[indexOf + 2] == '\n')))
                            {
                                _mainWindowViewModel.OutputText = _mainWindowViewModel.OutputText.Remove(indexOf, 1);
                                if ((selectionStart == 0) || (selectionStart == 1))
                                {
                                    selectionStart = 0;
                                }
                                else
                                {
                                    if (selectionStart >= index)
                                        selectionStart -= 1;

                                }
                            }
                        }

                    }
                    else
                    {
                        break;
                    }
                }
                outputTextBox.SelectionStart = selectionStart;



            }








            KeyboardButton keyboardButton = FindKeyboardButtonUp(e);
            if ((keyboardButton != null))
            {
                keyboardButton.IsPress = false;

            }


            _mainWindowViewModel.ClearPrevKeyboardButton();
            Counting();
            if (SimpleInputText.Length > SimpleOutputText.Length && _mainWindowViewModel.InputText.Length > outputTextBox.SelectionStart)
            {
                _mainWindowViewModel.FindNextKeyboardButton(
                    _mainWindowViewModel.InputText[outputTextBox.SelectionStart].ToString());
            }

            if ((SimpleInputText.Length == SimpleOutputText.Length) && (_mainWindowViewModel.CountError == 0) && (outputTextBox.SelectionStart == _mainWindowViewModel.OutputText.Length))
            {
                this.Close();
            }



        }





        KeyboardButton FindKeyboardButtonUp(KeyEventArgs e)
        {
            string array = @"QWERTYUIOPASDFGHJKLZXCVBNM";
            //KeyboardButton keyboardButton = null;
            if (array.Contains(e.Key.ToString()))
            {
                return FindKeyboardButton(e.Key.ToString());
            }
            if (e.Key == Key.Oem7)
            {
                return FindKeyboardButton("'");
            }
            if (e.Key == Key.Oem1)
            {
                return FindKeyboardButton(";");
            }
            if (e.Key == Key.Oem4)
            {
                return FindKeyboardButton("[");
            }
            if (e.Key == Key.Oem6)
            {
                return FindKeyboardButton("]");
            }
            if (e.Key == Key.OemComma)
            {
                return FindKeyboardButton("<");
            }
            if (e.Key == Key.OemPeriod)
            {
                return FindKeyboardButton("<");
            }
            if (e.Key == Key.Oem2)
            {
                return FindKeyboardButton("<");
            }
            if (e.Key == Key.Return)
            {

                return FindKeyboardButton("Enter");
            }
            if (e.Key == Key.Space)
            {
                return FindKeyboardButton("   ");
            }
            if (e.Key == Key.Oem5)
            {
                return FindKeyboardButton("\\");
            }
            if (e.Key == Key.OemPlus)
            {
                return FindKeyboardButton("=");
            }
            if (e.Key == Key.OemMinus)
            {
                return FindKeyboardButton("-");
            }
            if (e.Key == Key.Oem3)
            {
                return FindKeyboardButton("~");
            }
            if ((e.Key == Key.D1))
            {
                return FindKeyboardButton("1");
            }
            if ((e.Key == Key.D2))
            {
                return FindKeyboardButton("2");
            }
            if ((e.Key == Key.D3))
            {
                return FindKeyboardButton("3");
            }
            if ((e.Key == Key.D4))
            {
                return FindKeyboardButton("4");
            }
            if ((e.Key == Key.D5))
            {
                return FindKeyboardButton("5");
            }
            if ((e.Key == Key.D6))
            {
                return FindKeyboardButton("6");
            }
            if ((e.Key == Key.D7))
            {
                return FindKeyboardButton("7");
            }
            if ((e.Key == Key.D8))
            {
                return FindKeyboardButton("8");
            }
            if ((e.Key == Key.D9))
            {
                return FindKeyboardButton("9");
            }
            if ((e.Key == Key.D0))
            {
                return FindKeyboardButton("0");
            }

            if ((e.Key == Key.NumPad0))
            {
                return FindNumpad("0");
            }
            if ((e.Key == Key.NumPad1))
            {
                return FindNumpad("1");
            }
            if ((e.Key == Key.NumPad2))
            {
                return FindNumpad("2");
            }
            if ((e.Key == Key.NumPad3))
            {
                return FindNumpad("3");
            }
            if ((e.Key == Key.NumPad4))
            {
                return FindNumpad("4");
            }
            if ((e.Key == Key.NumPad5))
            {
                return FindNumpad("5");
            }
            if ((e.Key == Key.NumPad6))
            {
                return FindNumpad("6");
            }
            if ((e.Key == Key.NumPad7))
            {
                return FindNumpad("7");
            }
            if ((e.Key == Key.NumPad8))
            {
                return FindNumpad("8");
            }
            if ((e.Key == Key.NumPad9))
            {
                return FindNumpad("9");
            }
            return null;
        }


        private KeyboardButton FindKeyboardButton(string key)
        {
            KeyboardButton returnKeyboardButton = null;
            if ((key == "A") || (key == "S") || (key == "D") || (key == "F") || (key == "G") || (key == "H") || (key == "J") || (key == "K") || (key == "L"))
            {
                foreach (KeyboardButton keyboardButton in _mainWindowViewModel.SecondField)
                {
                    if (keyboardButton.EngTitle == key)
                    {
                        if (!sWatch.IsRunning)
                        {
                            StartTimer();
                        }
                        return keyboardButton;
                    }
                }
            }
            if ((key == "Z") || (key == "X") || (key == "C") || (key == "V") || (key == "B") || (key == "N") || (key == "M"))
            {
                foreach (KeyboardButton keyboardButton in _mainWindowViewModel.ThirdField)
                {
                    if (keyboardButton.EngTitle == key)
                    {
                        if (!sWatch.IsRunning)
                        {
                            StartTimer();

                        }
                        return keyboardButton;
                    }
                }
            }
            if ((key == "Q") || (key == "W") || (key == "E") || (key == "R") || (key == "T") || (key == "Y") || (key == "U") || (key == "I") || (key == "O") || (key == "P"))
            {
                foreach (KeyboardButton keyboardButton in _mainWindowViewModel.FirstField)
                {
                    if (keyboardButton.EngTitle == key)
                    {
                        if (!sWatch.IsRunning)
                        {
                            StartTimer();

                        }
                        return keyboardButton;
                    }
                }
            }

            if ("1234567890".Contains(key))
            {
                foreach (KeyboardButton keyboardButton in _mainWindowViewModel.NumberField)
                {
                    if (keyboardButton.EngTitle == key)
                    {
                        if (!sWatch.IsRunning)
                        {
                            StartTimer();

                        }
                        return keyboardButton;
                    }

                }
            }
            if (key == "Enter")
            {
                foreach (KeyboardButton keyboardButton in _mainWindowViewModel.SecondField)
                {
                    if (keyboardButton.EngTitle == key)
                    {
                        if (!sWatch.IsRunning)
                        {
                            StartTimer();

                        }
                        return keyboardButton;
                    }
                }
            }
            if (key == "   ")
            {
                foreach (KeyboardButton keyboardButton in _mainWindowViewModel.SpaceField)
                {
                    if (keyboardButton.EngTitle == key)
                    {
                        if (!sWatch.IsRunning)
                        {
                            StartTimer();

                        }
                        return keyboardButton;
                    }
                }
            }

            return returnKeyboardButton;
        }


        KeyboardButton FindNumpad(string key)
        {
            foreach (KeyboardButton keyboardButton in _mainWindowViewModel.NumberField2)
            {
                if (keyboardButton.EngTitle == key)
                    return keyboardButton;
            }
            foreach (KeyboardButton keyboardButton in _mainWindowViewModel.NumberField3)
            {
                if (keyboardButton.EngTitle == key)
                    return keyboardButton;
            }
            foreach (KeyboardButton keyboardButton in _mainWindowViewModel.NumberField4)
            {
                if (keyboardButton.EngTitle == key)
                    return keyboardButton;
            }
            foreach (KeyboardButton keyboardButton in _mainWindowViewModel.NumberField5)
            {
                if (keyboardButton.EngTitle == key)
                    return keyboardButton;
            }

            return null;
        }


        private void button1_Click_2(object sender, RoutedEventArgs e)
        {
            if ((sWatch.IsRunning) || (dispatherTimer.IsEnabled))
            {
                button1.Content = "Закрыть";


                sWatch.Stop();
                dispatherTimer.Stop();
                var analyzerResult = Counting(false);


                outputTextBox.Visibility = Visibility.Collapsed;
                richTextBox1.Visibility = Visibility.Visible;

                //string rtf = @"{\rtf1\ansi\ansicpg1252\deff0\deflang1033{\fonttbl{\f0\fnil\fcharset0 Arial;}} {\colortbl ;\red0\green0\blue0;\red255\green0\blue0;} \viewkind4\uc1\pard\qc\cf1\fs16 test \b bold \cf2\b0\i italic\cf0\i0\fs17  \par } ";
                //string rtf = @"asdfasdfasdfasf";



                //using (MemoryStream ms = new MemoryStream(Encoding.Default.GetBytes(rtf)))

                //TextRange txtR = new TextRange(richTextBox1.Document.ContentStart, richTextBox1.Document.ContentEnd);
                //if (txtR.CanLoad(DataFormats.Rtf))
                //    txtR.Load(ms, DataFormats.Rtf);
                string simpleOutputText = SimpleOutputText;//.Length<= SimpleInputText.Length
                                              //? SimpleOutputText
                //                              //: SimpleOutputText.Substring(0, SimpleInputText.Length);
                //string simpleOutputTextError = SimpleOutputText.Length > SimpleInputText.Length
                //                                   ? SimpleOutputText.Substring(SimpleInputText.Length)
                //                                   : string.Empty;
                int offset = 0;
                var paragraph = new Paragraph {FontSize = 20};
                //TextBlock textBlock=new TextBlock();
                //paragraph.Inlines.Add(textBlock);
                richTextBox1.Document.Blocks.Clear();
                richTextBox1.Document.Blocks.Add(paragraph);
                string str = string.Empty;

                foreach (var block in analyzerResult.Common)
                {
                    var insertStartB = block.Start;
                    var insertCountB = block.Count;
                    //var insertCountB = diffBlock.DeleteCountA + diffBlock.InsertCountB;
                    //? diffBlock.InsertCountB
                    //: diffBlock.DeleteCountA;

                    //var insertStartB = diffBlock.DeleteStartA < diffBlock.InsertStartB
                    //                       ? diffBlock.DeleteStartA
                    //                       : diffBlock.InsertStartB;

                    if (insertStartB < offset)
                        insertStartB = offset;
                    String normal = simpleOutputText.Substring(0, insertStartB - offset);
                    simpleOutputText = simpleOutputText.Remove(0, insertStartB - offset);
                    if (simpleOutputText.Length < insertCountB)
                        insertCountB = simpleOutputText.Length;
                    String error = simpleOutputText.Substring(0, insertCountB);
                    simpleOutputText = simpleOutputText.Remove(0, insertCountB);

                    var runNormal = new Run(normal);
                    var runError = new Run(error) { Background = new SolidColorBrush(Colors.Red) };

                    paragraph.Inlines.Add(runNormal);
                    paragraph.Inlines.Add(runError);
                    offset = insertCountB + insertStartB;
                    str += normal + error;



                    //var insertCountB = diffBlock.InsertCountB == 0 ? 1 : diffBlock.InsertCountB;

                    //String normal = simpleOutputText.Substring(0, diffBlock.InsertStartB - offset);
                    //simpleOutputText = simpleOutputText.Remove(0, diffBlock.InsertStartB - offset);
                    //String error = simpleOutputText.Substring(0, insertCountB);
                    //simpleOutputText = simpleOutputText.Remove(0, insertCountB);
                    //Run runNormal = new Run(normal);
                    ////runNormal.FontSize = 20;
                    //Run runError = new Run(error);
                    ////runError.FontSize = 20;
                    ////runError.Foreground = new SolidColorBrush(Colors.Red);
                    //runError.Background = new SolidColorBrush(Colors.Red);

                    ////textBlock.Inlines.Add(runNormal);
                    ////textBlock.Inlines.Add(runError);
                    //paragraph.Inlines.Add(runNormal);
                    //paragraph.Inlines.Add(runError);
                    //offset = insertCountB + diffBlock.InsertStartB;
                    //str += normal + error;
                }

                paragraph.Inlines.Add(simpleOutputText);
                //var runErrorend = new Run(simpleOutputTextError);// {Background = new SolidColorBrush(Colors.Red)};
                //paragraph.Inlines.Add(runErrorend);

            }
            else
            {
                this.Close();
            }


            //RichTextBox rich
        }

        private void _canExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;
            e.Handled = true;
        }

        private void inputScrollViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            outputScrollViewer.ScrollToVerticalOffset(inputScrollViewer.VerticalOffset);
            outputScrollViewer.ScrollToHorizontalOffset(inputScrollViewer.HorizontalOffset);
        }

        private void outputScrollViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            inputScrollViewer.ScrollToVerticalOffset(outputScrollViewer.VerticalOffset);
            inputScrollViewer.ScrollToHorizontalOffset(outputScrollViewer.HorizontalOffset);
        }

        private void outputTextBox_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            Clipboard.Clear();
            _mainWindowViewModel.ClearPrevKeyboardButton();
            if (_mainWindowViewModel.InputText.Length > outputTextBox.SelectionStart)
            {
                _mainWindowViewModel.FindNextKeyboardButton(
                    _mainWindowViewModel.InputText[outputTextBox.SelectionStart].ToString());
            }
        }

        private void ProgressBar_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            ProgressBar progressBar = (ProgressBar)sender;
            if (e.NewValue > progressBar.Maximum - 1)
            {
                progressBar.Foreground = Brushes.Red;
                return;
            }
            if (e.NewValue < progressBar.Maximum - 1)
            {
                progressBar.Foreground = Brushes.Yellow;
                return;
            }
            if (e.NewValue == progressBar.Maximum - 1)
            {
                progressBar.Foreground = Brushes.Green;
                return;
            }

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void Expander_Collapsed(object sender, RoutedEventArgs e)
        {
            ExpanderRow.Height = new GridLength(30);
        }

        private void Expander_Expanded(object sender, RoutedEventArgs e)
        {
            //inputScrollViewer.Height
            ExpanderRow.Height = new GridLength(340);
        }

        private void outputTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            Rect rect;
            double lengthText = 0; //длинна текста
            int countChars = this.outputTextBox.GetLineLength(0); //получаем кол-во символов

            if (countChars > 5)
            {
                rect = this.outputTextBox.GetRectFromCharacterIndex(outputTextBox.SelectionStart, true);
                lengthText = rect.X;
                outputScrollViewer.ScrollToHorizontalOffset(lengthText - outputScrollViewer.Width / 2);
            }
        }
        #endregion
      

        /// <summary>
        /// 
        /// </summary>
        /// <param name="isNotFinishCounting">только для финального подсчёта так как в количество ошибок включаются и недонабранные и перебранные</param>
        /// <returns></returns>
        public AnalyzerResult Counting(bool isNotFinishCounting = true)
        {

            string input = string.Empty;
            string output = string.Empty;
            int countError = 0;

            input = SimpleInputText;
            output = SimpleOutputText;

            if (SimpleInputText.Length > SimpleOutputText.Length)
            {
                input = isNotFinishCounting ? SimpleInputText.Remove(output.Length) : SimpleInputText;
            }
            //if (SimpleInputText.Length < SimpleOutputText.Length)
            //{
            //    input = SimpleInputText;
            //    output = SimpleOutputText;
            //}
            //if (SimpleInputText.Length == SimpleOutputText.Length)
            //{
            //    input = SimpleInputText;
            //    output = SimpleOutputText;
            //}


            var res = TextAnalyzer.Analyze(input, output);
            //if (isNotFinishCounting)
            //{
            //    if(res.Insert.Count>0)
            //    {
            //        var len = input.Length - res.Remove.Sum(el => el.Count) - res.Insert.Sum(el => el.Count);
            //        input = input.Substring(0,len);
            //    }
            //    //input = 
            //            //SimpleInputText.Length > (output.Length - res.Insert.Sum(el => el.Count))
            //            //    ? SimpleInputText.Remove(output.Length - res.Insert.Sum(el => el.Count))
            //            //    : SimpleInputText;

            //    res = TextAnalyzer.Analyze(input, output);
            //}


            if (!isNotFinishCounting)
            {
                countError += res.ErrorCount +  res.Insert.Sum(el => el.Count);
            }
            else
            {
                countError = res.ErrorCount; //+ res.Insert.Where(el=>el.Start<output.Length).Sum(el => el.Count) ;
            }

            ((MainWindowViewModel)this.DataContext).CountError = countError;
            {

                if (sWatch.ElapsedMilliseconds < 1000)
                {
                    ((MainWindowViewModel)this.DataContext).SymbolPerSecond = 0;
                }
                else
                {
                    ((MainWindowViewModel)this.DataContext).SymbolPerSecond = (int)((_mainWindowViewModel.OutputText.Length * 60000 / sWatch.ElapsedMilliseconds));
                }

            }
            ((MainWindowViewModel)this.DataContext).TimePeriod = (new DateTime(sWatch.Elapsed.Ticks)).ToLongTimeString();
            if (_remaningTime.Ticks > sWatch.Elapsed.Ticks)
                ((MainWindowViewModel)this.DataContext).RemainingTime = new DateTime(_remaningTime.Ticks - sWatch.Elapsed.Ticks).ToLongTimeString();
            ((MainWindowViewModel)this.DataContext).DateTimePeriod = new DateTime(sWatch.Elapsed.Ticks);

            return res;
        }

      
    }

    internal class HelperKey
    {
        public string RusTitle;
        public string EngTitle;
        public bool Lang;
        public TypeButton TypeButton;
        public StandartColorButton StandartColorButton;
        public string SupportedSymbols;
    }

    internal enum TypeButton
    {
        //обычная кнопка 
        Standart,
        //на 50% длиннее обычной
        Func1,
        //на 100% длинее обычной
        Func2,
        //на 150% длинее обычной, например шифт
        Func3,
        //пробел
        Func4
    }

    /// <summary>
    /// Стандартный цвет кнопок
    /// </summary>
    internal enum StandartColorButton
    {
        FuncButton, FirstFinger, SecondFinger, ThirdFinger, FourthFinger, BigFinger, SecondFourthFinger

    }


    public class FinishEventArgs : EventArgs
    {

        private int _countError;
        private DateTime _timePeriod;
        private TimeSpan _timeSpan;
        private string _outputText;
        private string _inputText;
        private double _symbolPerMinute;
        public FinishEventArgs(int countError, DateTime timeperiod, TimeSpan timeSpan, string outputText, string inputText, double symbolPerMinute)
        {
            _countError = countError;
            _timePeriod = timeperiod;
            _outputText = outputText;
            _inputText = inputText;
            _timeSpan = timeSpan;
            _symbolPerMinute = symbolPerMinute;
        }

        /// <summary>
        /// Количество ошибок
        /// </summary>
        public int CountError
        {
            get { return _countError; }
        }

        /// <summary>
        /// Время выполнения теста
        /// </summary>
        public DateTime TimePeriod
        {
            get { return _timePeriod; }
        }

        /// <summary>
        /// Время выполнения в таймСпан
        /// </summary>
        public TimeSpan TimeSpan
        {
            get { return _timeSpan; }
        }

        /// <summary>
        /// Текст набранный пользователем в результате выполнения
        /// </summary>
        public string OutputText
        {
            get { return _outputText; }
        }

        public string InputText
        {
            get { return _inputText; }
        }

        public double SymbolPerMinute
        {
            get { return _symbolPerMinute; }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DiffPlex;
using DiffPlex.Model;

namespace WFAGuiTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Differ differ = new Differ();
            DiffResult result;
            result = differ.CreateCharacterDiffs(textBox1.Text, textBox2.Text, false);
            //result = differ.CreateLineDiffs(textBox1.Text, textBox2.Text, false);
            tb3.Clear();
            tb4.Clear();
            tb5.Clear();
            foreach (DiffBlock block in result.DiffBlocks)
            {
                tb3.Text += block.DeleteStartA.ToString() + ":" + block.DeleteCountA.ToString() + "\r\n";
                tb4.Text += block.InsertStartB.ToString() + ":" + block.InsertCountB.ToString() + "\r\n";

                if (block.DeleteCountA ==0)
                {
                    tb5.Text += "Лишние      " + block.InsertCountB.ToString() + "  симв. на поз. " + block.InsertStartB.ToString() + "\r\n";
                }
                else if (block.InsertCountB == 0)
                {
                    tb5.Text += "Пропущено   " + block.DeleteCountA.ToString() + "  симв. на поз. " + block.InsertStartB.ToString() + "\r\n";
                }
                else
                {
                    tb5.Text += "Опечатка в " + block.DeleteCountA.ToString() + "/" + block.InsertCountB.ToString() + " симв. на поз. " + block.InsertStartB.ToString() + "\r\n";
                }
                
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Eureca.Utility.Extensions;

namespace DebugApp
{
    public interface IAddMessageInterface
    {
        void AddMessage( SeparatedArgs args );
    }

    public class SeparatedArgs
    {
        private string _message;
        private object lockObject = new object();

        public string Message
        {
            get { return _message; }
        }

        public void AddMessage(string message)
        {
            lock (lockObject)
            {
                _message += message + "\n";
            }
        }

    }

    public class Test_of_priorityInvoker : IAddMessageInterface
    {
        public int Priority { get; private set; }

        #region IAddMessageInterface Members

        public void AddMessage( SeparatedArgs args )
        {
            Thread.Sleep(100);
            args.AddMessage("object {0} prior {1} from thread {2}".FormatString(this.GetHashCode(),Priority,Thread.CurrentThread.ManagedThreadId));
        }

        #endregion

        public Test_of_priorityInvoker(int priority)
        {
            Priority = priority;
        }      
    }
}

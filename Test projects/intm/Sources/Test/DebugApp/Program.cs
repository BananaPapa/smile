﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Castle.ActiveRecord;
using Castle.ActiveRecord.Framework.Config;
using Castle.DynamicProxy.Generators;
using Curriculum;
using Curriculum.Forms;
using DebugApp.Properties;
using Eureca.Professional.ActiveRecords;
using Eureca.Professional.BaseComponents;
using Eureca.Professional.FillingFormsTest;
using Eureca.Utility;
using log4net;

namespace DebugApp
{
    static class Program
    {
        private static ILog log = LogManager.GetLogger(typeof (Program));
        private static Form targetForm;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main( )
        {
            PriorityInvoker<IAddMessageInterface> invoker = new PriorityInvoker<IAddMessageInterface>();
            //Test_of_priorityInvoker invokeObject = null;
            Test_of_priorityInvoker invokeObject = new Test_of_priorityInvoker( 0 );
            invoker.Addlistener( invokeObject.Priority, invokeObject );

            Test_of_priorityInvoker invokeObject2 = new Test_of_priorityInvoker( 0 );
            invoker.Addlistener( invokeObject2.Priority, invokeObject2 );
            Test_of_priorityInvoker invokeObject3 = new Test_of_priorityInvoker( 0 );
            invoker.Addlistener( invokeObject3.Priority, invokeObject3 );
            Test_of_priorityInvoker invokeObject4 = new Test_of_priorityInvoker( 1 );
            invoker.Addlistener( invokeObject4.Priority, invokeObject4 );
            Test_of_priorityInvoker invokeObject6 = new Test_of_priorityInvoker( 1 );
            invoker.Addlistener( invokeObject6.Priority, invokeObject6 );
            Test_of_priorityInvoker invokeObject7 = new Test_of_priorityInvoker( 2 );
            invoker.Addlistener( invokeObject7.Priority, invokeObject7 );

            var sepArgs = new SeparatedArgs();
            Parallel.Invoke(
            ()=>{
                for (int i = 0; i < 5; i++)
                {
                
                    invoker.Invoke( invokerObject => invokerObject.AddMessage( sepArgs ) );
                    sepArgs.AddMessage("\n\n");
                }
            }
            ,
            ()=>
            {
                Thread.Sleep(500);
                invoker.Removelistener(invokeObject7);
            },
            ()=>
            {
                Thread.Sleep(800);
                invoker.Addlistener(2, invokeObject7 );
                invoker.Removelistener(invokeObject6);
            }
                );

            Trace.WriteLine(sepArgs.Message);

            Application.EnableVisualStyles( );
            Application.SetCompatibleTextRenderingDefault( false );
            //InitActiveRecords( @"Data Source=.\sqlexpress2012;Initial Catalog=Professional;Integrated Security=True" );
            //Application.Run( new CompareStandartsControl(null, null));
            RunnerApplication.InitActiveRecords( @"Data Source=.\sqlexpress2012;Initial Catalog=Professional;Integrated Security=True" );


            var journal = Journal.FindFirst();
            if (journal == null)
            {
                journal = new Journal("Новый Журнал");
                journal.Save();
                var journalBranch = new JournalBranch("Основной раздел", journal);
                journalBranch.Save();
            }

            int res =
                (new BindingList<Tuple<string, TestQuestion>>()).Select(tuple => tuple.Item2.QuestionOrder).DefaultIfEmpty(-1).Max() + 1;

            //FormFilling formFilling = new FormFilling();
            //using (new SessionScope())
            //{
            //    FormFilling formFilling2 = FormFilling.Find( 2076 );
            //    targetForm = new BlancVerificationForm( formFilling2.FilledForm.ContentStream, formFilling2.Form.ContentStream, formFilling2.FilledForm.Comments.ToList( ) ) { WindowState = FormWindowState.Maximized };
                
            //}
            using (var scope =new TransactionScope())
            {
                try
                {
                    //var method = Method.Find(1047);
                    //method.DeleteAndFlush();
                    //foreach (Test test in selectTest)
                    //{
                    //    if (!DeleteTest( false, test ))
                    //        return false;
                    //}

                    //var fuckYOU = new List<Test>( );
                    //foreach (var profile in Profile.FindAll( ))
                    //{
                    //    fuckYOU.AddRange( profile.Tests );
                    //}

                    //foreach (ControlQuestion controlQuestion in ControlQuestion.FindAll( Restrictions.Eq( "Method", method ) ))
                    //{
                    //    foreach (QuestionAnswer questionAnswer in QuestionAnswer.FindAll( Restrictions.Eq( "Question", controlQuestion.Question ) ))
                    //    {
                    //        questionAnswer.DeleteAndFlush( );
                    //    }
                    //    controlQuestion.DeleteAndFlush( );
                    //}
                    log.Info("hi");
                  
                    var test = Test.Find(105);
                    foreach (var scale in test.Scales)
                    {
                        scale.Tests.Remove(test);
                    }
                    test.Scales.Clear();
                    foreach (var report in test.Reports)
                    {
                        report.Tests.Remove(test);
                    }
                    test.Reports.Clear();
                    foreach (var profile in test.Profiles)
                    {
                        profile.Tests.Remove( test );
                    }
                    test.Profiles.Clear();
                    var method = test.Method;
                    method.Tests.Remove(test);
                    method.UpdateAndFlush( );
                    log.Info( "call delete" );
                    //test.DeleteAndFlush();
                    
                    //test.Scales.Clear();
                    //test.Reports.Clear();
                    //var content = test.Description;
                    //test.Description = null;
                    //test.UpdateAndFlush();
                    //content.DeleteAndFlush();
                    //test.DeleteAndFlush();
                    log.Info( "Fuck?" );
                    log.Info( "Stop here" );
                }
                catch (Exception e)
                {
                    scope.VoteRollBack();
                }
            }
            //targetForm = new BlancTestResultPreviewForm( 738469 ){ WindowState = FormWindowState.Maximized };
            //targetForm = new EditQuestionBlancForm( formFilling ) { WindowState = FormWindowState.Maximized };
            //targetForm = new JournalListForm() { WindowState = FormWindowState.Maximized };
            //targetForm = new ShowResultForm(TestPassing.Find(738441)){ WindowState = FormWindowState.Maximized };
            //targetForm = new EditAssigmentForm(Question.Find(3234)){ WindowState = FormWindowState.Maximized };
            //targetForm = new ShowResultForm(TestPassing.Find(738442)){ WindowState = FormWindowState.Maximized };
            //targetForm = new EditAssigmentForm(Question.Find( 3238 ) ) { WindowState = FormWindowState.Maximized };
            //targetForm = new EditJournalTreeForm( Journal.Find( 10 ) ) { WindowState = FormWindowState.Maximized, MinimumSize = new Size( 100, 100 ) };

            targetForm = new EditRoleForm(new Role(),Enumerable.Empty<Role>()) {ManualFilter = true,WindowState = FormWindowState.Maximized };
            //FillingBlancsForm
            var hostForm = new Form( ) { WindowState = FormWindowState.Maximized };
            //hostForm.Controls.Add( new RtfMarkUpTextControl( Resources.description, null ) { Dock = DockStyle.Fill } );
            hostForm.Load += hostForm_Load;
            Application.Run( hostForm );
        }

        static void hostForm_Load( object sender, EventArgs e )
        {
            if(targetForm== null)
                (sender as Form).Close();
            using (var tran = new TransactionScope())
            {
                var res = targetForm.ShowDialog();
                if (res == DialogResult.OK)
                {
                    //var fillingRes = ( targetForm as FormFillingTestAssigmentsListForm ).CurrentFilling;
                    tran.VoteCommit();
                }
                else
                {
                    tran.VoteRollBack();
                }
            }
        }

        private static void InitActiveRecords( string connectionString )
        {
            var properties = new Dictionary<string, string>( );
            //Castle.Core.Configuration.ConfigurationCollection conf = new Castle.Core.Configuration.ConfigurationCollection();
            //conf.Add(Castle.Core.Configuration.
            properties.Add( NHibernate.Cfg.Environment.ConnectionDriver, "NHibernate.Driver.SqlClientDriver" );
            properties.Add( NHibernate.Cfg.Environment.Dialect, "NHibernate.Dialect.MsSql2008Dialect" );
            properties.Add( NHibernate.Cfg.Environment.ConnectionProvider, "NHibernate.Connection.DriverConnectionProvider" );
            properties.Add( NHibernate.Cfg.Environment.ConnectionString, connectionString );
            properties.Add( NHibernate.Cfg.Environment.ProxyFactoryFactoryClass, "NHibernate.ByteCode.Castle.ProxyFactoryFactory, NHibernate.ByteCode.Castle" );

            var source = new InPlaceConfigurationSource( );

            source.Add( typeof( ActiveRecordBase ), properties );
            //source.Debug = true;
            //source.
            ActiveRecordStarter.Initialize( source,
                                           typeof( Journal ),
                                           typeof( RtfForm ),
                                           typeof( JournalBranch )
                );
            //проверка на инициализацию именно этой БД
            ActiveRecordBase<Journal>.FindAll( );
            //ActiveRecordBase<CommentText>.FindAll( );
            //ActiveRecordBase<Question>.FindAll( );

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Timers;
using System.Windows.Forms;
using Castle.ActiveRecord;
using Eureca.Professional.ActiveRecords;
using Eureca.Professional.CommonClasses;
using log4net;
using NHibernate;
using Timer = System.Timers.Timer;
using NHibernate.Criterion;

namespace Eureca.Professional.TestingPlugInStandard.Controls.ShowTest
{
    public partial class MainTestingControl : BaseTestingControl
    {
        private static readonly ILog Log = LogManager.GetLogger( typeof( MainTestingControl ) );

        #region Delegates

        public delegate void InvokeDelegate(bool ByTimer);

        /// <summary>делегат событий от контрола</summary>
        /// <param name="control"></param>
        public delegate void MainTestingControlHandler(MainTestingControl control);

        #endregion

        #region fields

        // private const int MaxQuestionsCount = 3000;

        /// <summary>
        /// ссылка на объект класса
        /// </summary>
        private readonly Test _test;

        /// <summary>время начала обдумывания вопроса</summary>
        private DateTime _beginThink;

        /// <summary>
        /// время обдумывания вопроса
        /// </summary>
        private int _thinkSecond;
        /// <summary>
        /// время тестирования
        /// </summary>
        private int _testPassingSecond;
        /// <summary>
        /// таймер отсыета временных интервалов
        /// </summary>
        private Timer _timerThinkSecond;

        /// <summary>
        /// тип данного действия
        /// это либо контрольные вопросы 
        /// либо вопросы теста
        /// </summary>
        private ObjectDescription _currentType = ObjectDescription.None;

        /// <summary>текущий вопрос из списка вопросов</summary>
        private WorkQuestion _currentWorkQuestion;

        /// <summary>
        /// флаг делается тест или нет
        /// </summary>
        private bool _doTest;

        /// <summary>
        /// поток загрузки вопросов и ответов в фоновом режиме
        /// </summary>
        private Thread _loadQuestionThread;

        /// <summary>
        /// флаг необходимости загрузки нового вопроса
        /// </summary>
        private bool _needQuestion;

        private int _nextQuestionId;

        /// <summary>
        /// ответы на вопрос, заполняются при очередной загрузке вопроса
        /// </summary>
        private QuestionAnswer[] _questionAnswers;

        /// <summary>
        /// ответы на все вопросы теста 
        /// заполняются при 
        /// </summary>
        private TestAnswer[] _testAnswers = { };

        private TestPassing _testPassing;

        /// <summary>остаток времени тестирования. Число больше 0 если установлено время
        /// и -1 если не установлено</summary>
        private int _testSeconds = -1;

        /// <summary> свойства теста </summary>
        private TestSettings _testSettings;

        /// <summary>Секундный таймер отслеживания состояний запускается при старте теста</summary>
        private Timer _timerRefresStates;

        /// <summary>Секундный таймер отображает состояния времен теста и вопросов 
        /// запускается при старте теста
        /// </summary>
        private Timer _timerViewTimes;

        /// <summary>контрол ответов на вопросы</summary>
        private AnswerTestingControl _ucAnswer;

        /// <summary>контрол навигации</summary>
        private NavigationTestingControl _ucNav;

        /// <summary>ссылка на контрол </summary>
        private NavigationAnswerTestingControl _ucNavigationAnswer;

        /// <summary>контрол вопросов </summary>
        private QuestionTestingControl _ucQuestion;

        /// <summar> контрол вывода на жкран инхормации о таймерах</summar></summary>
        private TimersTestingControl _ucTimers;

        /// <summary>Список вопросов с состояниями</summary>
        private List<WorkQuestion> _workQuestions = new List<WorkQuestion>();

        #endregion

        #region Events

        /// <summary>Событие окончания теста</summary>
        public event MainTestingControlHandler ControlClosed;

        #endregion

        #region Properties

        /// <summary>
        /// флаг делается тест или нет
        /// </summary>
        public bool DoTest
        {
            get { return _doTest; }
        }

        public Test CurrentTest
        {
            get { return _test; }
        }

        #endregion

        #region constructor

        /// <summary>
        /// конструктор
        /// </summary>
        public MainTestingControl(Test test, TestPassing testPassing, ObjectDescription objectDescription)
        {
            if (objectDescription == ObjectDescription.None)
                throw new Exception("неверное описание объекта");
            _currentType = objectDescription;
            _test = test;
            _testPassing = ActiveRecordBase<TestPassing>.Find(testPassing.Id);
            InitializeComponent();
        }

        #endregion

        #region методы

        public override void InitControl()
        {
            try
            {
                base.InitControl();
                if (_test.TimeLimit != null && _test.TimeLimit > 0)
                {
                    _testSeconds = (int)_test.TimeLimit;
                }
                else
                {
                    //если в темте время не установлено, то устанавливаем в -1
                    _testSeconds = -1;
                }

                //получение свойств теста
                _testSettings = TestSettings.GetSettings(_test.Options);


                //создаем наименование
                CaptionControl = "Тестирование";


                //вывод на экран описание методики
                if (_currentType == ObjectDescription.Method)
                    SetMethodDescriptionControl();
                //вывод на экран описание теста
                SetTestDescriptionControl();
                //создание элементов управления
                CreateAllControls();

                //показ контрольных вопросов


                //запуск 
                Start(_currentType);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw;
            }
        }

        /// <summary>метод создания всех контролов и вывод нв главный контрол </summary>
        private void CreateAllControls()
        {
            try
            {
                //создание контрола навигации
                _ucNav = new NavigationTestingControl(_testSettings.AllowUserSelectQuestion);
                _ucNav.SelectQuestion += ucNav_SelectQuestion;
                _ucNav.NeedNextQuestion += _ucNav_NeedNextQuestion;
                _ucNav.InitControl();

                groupControlNavigation.Controls.Clear();
                groupControlNavigation.Controls.Add(_ucNav.ShowAsControl());

                //очистка таблицы - панели контролов
                tableLayoutPanelTesting.Controls.Clear();

                //создание таймера обдумывания
                _timerThinkSecond = new Timer(1000);
                _timerThinkSecond.Elapsed += new ElapsedEventHandler(_thinkSecondTimer_Elapsed);

                //создание таймеров
                _timerViewTimes = new Timer(1000);
                _timerViewTimes.Elapsed += _timerSecond_Elapsed;

                //создаем секундный таймер обновления состояний
                _timerRefresStates = new Timer(1000);
                _timerRefresStates.Elapsed += _timerRefresStates_Elapsed;

                //сохдание контрола таймеров
                _ucTimers = new TimersTestingControl();
                _ucTimers.simpleButtonHideClick += _ucTimers_simpleButtonHideClick;
                _ucTimers.InitControl();
                tableLayoutPanelTesting.Controls.Add(_ucTimers.ShowAsControl(), 0, 0);


                //создание контрола вопросов
                _ucQuestion = new QuestionTestingControl();
                _ucQuestion.InitControl();

                tableLayoutPanelTesting.Controls.Add(_ucQuestion.ShowAsControl(), 0, 1);


                //создание контрола ответов
                _ucAnswer = new AnswerTestingControl(_test);
                _ucAnswer.InitControl();
                _ucAnswer._AnswerSelected += ucAnswer_AnswerSelected;

                tableLayoutPanelTesting.Controls.Add(_ucAnswer.ShowAsControl(), 0, 2);


                //создание новой строки в таблице контролов для 
                //вставки туда панели навигации по контролам


                //Создания контрола навигации по ответам
                _ucNavigationAnswer = new NavigationAnswerTestingControl(_test);
                _ucNavigationAnswer.InitControl();
                _ucNavigationAnswer.NavigationAnswerButtonClick += _ucNavigationAnswer_NavigationAnswerButtonClick;

                tableLayoutPanelTesting.Controls.Add(_ucNavigationAnswer.ShowAsControl(), 0, 3);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw;
            }
        }

        void _thinkSecondTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            _thinkSecond++;
            _testPassingSecond++;
        }

        /// <summary>
        /// метод перемешивания вопросов в тесте
        /// </summary>
        /// <param name="workQuestions"></param>
        /// <returns></returns>
        private static List<WorkQuestion> RandomizeQuestion(List<WorkQuestion> workQuestions)
        {
            var res = new List<WorkQuestion>();
            var r = new Random();
            while (workQuestions.Count > 0)
            {
                int id = r.Next(workQuestions.Count);
                res.Add(workQuestions[id]);
                workQuestions.RemoveAt(id);
            }
            return res;
        }


        /// <summary>метод старта теста</summary>
        private void Start(ObjectDescription objectDescription)
        {
            try
            {
                _doTest = true;
                _workQuestions.Clear();
                _currentType = objectDescription;
                if (objectDescription == ObjectDescription.Test)
                {
                    //ветка инициализации вопросов теста
                    _testPassing.TimeInterval = new TimeInterval(DateTime.Now, DateTime.Now.AddSeconds(1));
                    _testPassingSecond = 0;
                    _testPassing.UpdateAndFlush();
                    //создаю список с вопросами и состоянием
                    //состояние будет храниться в главном контроле и выводиться в контроле навигации
                    using (new SessionScope())
                    {
                        foreach (
                            TestQuestion testQuestion in
                                ActiveRecordBase<TestQuestion>.FindAll(new Order("QuestionOrder", true),
                                    Expression.Eq("Test", _test)))
                        {
                            var w = new WorkQuestion(testQuestion.Question, testQuestion.QuestionOrder,
                                WorkQuestionState.None);
                            _workQuestions.Add(w);
                        }
                        //получаем ответы для всего теста
                        _testAnswers = ActiveRecordBase<TestAnswer>.FindAll(new Order("AnswerOrder", true),
                            Expression.Eq("Test", _test));
                    }
                }
                else
                {
                    ControlQuestion[] cQuestions;

                    using (new SessionScope())
                    {
                        cQuestions = ActiveRecordBase<ControlQuestion>.FindAll(new Order("QuestionOrder", true),
                            Expression.Eq("Method", _test.Method));


                        //ветка инициализации контрольных вопросов
                        foreach (
                            ControlQuestion controlQuestion in cQuestions)
                        {
                            var w = new WorkQuestion(controlQuestion.Question, controlQuestion.QuestionOrder,
                                WorkQuestionState.None);
                            w.RightAnswer = controlQuestion.RightAnswer;
                            _workQuestions.Add(w);
                        }
                    }
                    if (_workQuestions.Count == 0)
                    {
                        //если нет контролльных вопросов то запускаем тест
                        Start(ObjectDescription.Test);
                        return;
                    }

                    MessageForm.ShowOkMsg("Перед тестированием Вам необходимо ответить на контрольные вопросы");

                }
                if (_testSettings.Random)
                {
                    //необходимо перемешать вопросы в тесте
                    _workQuestions = RandomizeQuestion(_workQuestions);
                }

                //создаем секундный таймер отображения времен

                //создаю поток для щагрузки следующего
                _loadQuestionThread = new Thread(ThreadLoadNextQuestion);


                _ucNav.FillControl(_workQuestions);
                //старт таймера


                //вывод вопроса
                if (!GoToNextQuestion())
                {
                    return;
                }


                if (_currentType == ObjectDescription.Test)
                {
                    //запуск таймера времени теста
                    _timerViewTimes.Start();
                }
                
                //"быстрый" запуск метода таймера
                _timerRefresStates_Elapsed(null, null);
                //запуск таймера состояний
                _timerRefresStates.Start();
                _timerThinkSecond.Start();
                //старт потока загрузки вопроса
               
                _loadQuestionThread.Start();
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw;
            }
        }

        /// <summary>метод остановки теста(таймеров отсчета)</summary>
        private void Stop()
        {
            _doTest = false;
            Thread.Sleep(100);
            _timerRefresStates.Stop();
            _timerViewTimes.Stop();
            _timerThinkSecond.Stop();
        }

        /// <summary>метод подгрузки вопроса из БД
        /// для того чтобы загрузка следующего вопроса
        /// происходила при ответе на текущий вопрос
        /// сделано это для ускорения работы с БД</summary>
        private void ThreadLoadNextQuestion()
        {
            try
            {
                while (DoTest)
                {
                    if (!_needQuestion)
                    {
                        Thread.Sleep(10);
                    }
                    else
                    {
                        LoadNextQuestion();
                        _needQuestion = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw;
            }
        }

        /// <summary> загрузка контента вопроса в память из БД </summary>
        /// <param name="workQuestion"></param>
        private void LoadQuestion(WorkQuestion workQuestion)
        {
            string s;
            byte[] b;
            using (new SessionScope())
            {
                var question = Question.Find( workQuestion._question.Id );
                try
                {
                    s = question.Content.PlainContent;
                    b = question.Content.RichContent;
                }
                catch (ADOException ex)
                {
                    Log.Error(ex);
                    Thread.Sleep(100);
                    s = question.Content.PlainContent;
                    b = question.Content.RichContent;
                }
                catch (ObjectNotFoundException ex)
                {
                    Log.Error(ex);
                    //заново прогружаем вопрос
                    Thread.Sleep(100);
                    question = ActiveRecordBase<Question>.Find(question.Id);
                    s = question.Content.PlainContent;
                    b = question.Content.RichContent;
                }
                try
                {
                    //загружаем ответы
                   
                        _questionAnswers = ActiveRecordBase<QuestionAnswer>.FindAll(new Order("AnswerOrder", true),
                            Expression.Eq("Question",
                                question));
                        //загружаем ответы
                        foreach (QuestionAnswer answer in _questionAnswers)
                        {
                            s = answer.Answer.Content.PlainContent;
                            b = answer.Answer.Content.RichContent;
                        }
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                    throw;
                }
            }
        }


        /// <summary>метод потока который подгружает следующие ответы к вопросу</summary>
        private void LoadNextQuestion()
        {
            try
            {
                WorkQuestion workQuestion;
                int currentIndex = _workQuestions.IndexOf(_currentWorkQuestion);

                for (int i = currentIndex + 1; i < _workQuestions.Count; i++)
                {
                    workQuestion = _workQuestions[i];

                    if (workQuestion._state == WorkQuestionState.Error
                        || workQuestion._state == WorkQuestionState.Ok)
                    {
                        //пропускаем отвеченые и пропущенный вопросы
                        continue;
                    }
                    _nextQuestionId = workQuestion._question.Id;
                    //устанавливаем вопрос
                    LoadQuestion(workQuestion);
                    return;
                }
                //получение вопросов в режиме ожидания
                WorkQuestion workq = GetNextAcessQuestion();
                if (workq != null)
                    LoadQuestion(workq);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw;
            }
        }

        /// <summary>метод установки на экран описание методики</summary>
        private void SetMethodDescriptionControl()
        {
            //создание контрола вопросов
            var ucMethodDescription = new DesctiptionTestingControl(ObjectDescription.Method, _test);
            ucMethodDescription.PressContinue += ucDescription_PressContinue;
            ucMethodDescription.InitControl();
            ucMethodDescription.ShowInForm(false);
        }

        /// <summary>метод установки на экран описание теста</summary>
        private void SetTestDescriptionControl()
        {
            if (_test.Description != null)
            {
                //создание контрола вопросов
                var ucTestDescription = new DesctiptionTestingControl(ObjectDescription.Test, _test);
                ucTestDescription.PressContinue += ucDescription_PressContinue;
                ucTestDescription.InitControl();
                ucTestDescription.ShowInForm(false);
            }
        }

        /// <summary>метод перемещения на следующий вопрос</summary>
        private bool GoToNextQuestion()
        {
            try
            {
                WorkQuestion workQuestion;
                int currentIndex = _workQuestions.IndexOf(_currentWorkQuestion);

                //ожидание прогрузки вопроса из другого потока
                //ждем 5 сек
                int wait = 0;
                while (wait != 5000)
                {
                    if (_needQuestion)
                    {
                        Thread.Sleep(10);
                        wait += 10;
                    }
                    else
                        break;
                }
                //проверка на инициализацию
                if (_currentWorkQuestion == null)
                {
                    if (_workQuestions.Count == 0)
                    {
                        MessageForm.ShowWarningMsg("В тесте отсутствуют вопросы");
                        OnControlClosed();
                        return false;
                    }

                    using (new SessionScope())
                    {
                        _questionAnswers = ActiveRecordBase<QuestionAnswer>.FindAll( new Order( "AnswerOrder", true ),
                                                                               Expression.Eq( "Question",
                                                                                             _workQuestions[0].
                                                                                                 _question ) );
                    }
                   
                    SetQuestion(_workQuestions[0], true);
                    return true;
                }
                //проверка на последний вопрос
                WorkQuestion nextAcessQuestion = GetNextAcessQuestion();
                if (IsLastQuestion( _currentWorkQuestion ) && ( nextAcessQuestion == null || (_currentWorkQuestion == nextAcessQuestion && _questionAnswers.Length == 0 && _testAnswers.Length == 0) ))
                {
                    Stop();
                    if (_currentType == ObjectDescription.Method)
                    {
                        MessageForm.ShowOkMsg(
                            "Вы ответили на контрольные вопросы.\nТеперь необходимо ответить на вопросы теста.");
                        Start(ObjectDescription.Test);
                        return false;
                    }
                    MessageForm.ShowOkMsg("Тест закончен");
                    //посылка события о том что тест закончен
                    OnControlClosed();
                    return false;
                }

                for (int i = currentIndex + 1; i < _workQuestions.Count; i++)
                {
                    workQuestion = _workQuestions[i];
                    if (workQuestion._state == WorkQuestionState.Error
                        || workQuestion._state == WorkQuestionState.Ok)
                    {
                        //пропускаем отвеченые и пропущенный вопросы
                        continue;
                    }
                    //установка подходящего вопроса
                    SetQuestion(workQuestion, true);
                    return true;
                }
                //поиск вопросов в режиме ожидания
                if (nextAcessQuestion == null)
                {
                    Stop();
                    if (_currentType == ObjectDescription.Method)
                    {
                        MessageForm.ShowOkMsg(
                            "Вы ответили на контрольные вопросы.\nТеперь необходимо ответить на вопросы теста.");
                        Start(ObjectDescription.Test);
                        return false;
                    }
                    MessageForm.ShowOkMsg("Тест закончен");
                    //посылка события о том что тест закончен
                    OnControlClosed();
                    return false;
                }
                SetQuestion(nextAcessQuestion, true);
                return true;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw;
            }
        }

        /// <summary>метод проверяет является ли вопрос последним или нет</summary>
        /// <param name="question"></param>
        /// <returns></returns>
        private bool IsLastQuestion(WorkQuestion question)
        {
            foreach (WorkQuestion workQuestion in _workQuestions)
            {
                if (workQuestion._order > question._order)
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>Метод проверяет на существование вопросов в режиме ожидания</summary>
        /// <returns></returns>
        private WorkQuestion GetNextAcessQuestion()
        {
            foreach (WorkQuestion workQuestion in _workQuestions)
            {
                //ищу пустое состояния и состояние ожидания
                if (workQuestion._state == WorkQuestionState.None ||
                    workQuestion._state == WorkQuestionState.Wait)
                {
                    return workQuestion;
                }
                //ищу текущее стостояние
                if (workQuestion._state == WorkQuestionState.Current)
                {
                    return workQuestion;
                }
            }
           
            return null;
        }

        /// <summary>Метод поиска вопроса по идентификатору</summary>
        /// <param name="questionID"></param>
        /// <returns></returns>
        private WorkQuestion FindQuestion(int questionID)
        {
            foreach (WorkQuestion workQuestion in _workQuestions)
            {
                if (workQuestion._question.Id == questionID)
                {
                    return workQuestion;
                }
            }
            return null;
        }

        /// <summary>Метод выставляет состояние у текущего вопроса и выводит в панель навигации</summary>
        /// <param name="state"></param>
        private void SetCurrentWorkQuestionState(WorkQuestionState state)
        {
            //установили состояние
            _currentWorkQuestion._state = state;
            //запомнили установленное состояние
            foreach (WorkQuestion workQuestion in _workQuestions)
            {
                if (workQuestion._order == _currentWorkQuestion._order)
                {
                    //сохраняем состояние в списке
                    workQuestion._state = _currentWorkQuestion._state;
                    break;
                }
            }
            //отобразили состояние 
            _ucNav.SetStateWorkQuestion(_currentWorkQuestion);
        }

        /// <summary>Метод отслеживания состояний у вопросов</summary>
        private void RefreshStates(bool byTimer)
        {
            try
            {
                int askedCount = 0;
                for (int i = 0; i < _workQuestions.Count; i++)
                {
                    WorkQuestion workQuestion = _workQuestions[i];
                    //Если состояние ожидания и текущее, то вычитаем время
                    //либо, если время больше 0,то переводим в состояние ошибки
                    //если время равно -1 то пропускаем вопрос
                    if (workQuestion._state == WorkQuestionState.Wait ||
                        workQuestion._state == WorkQuestionState.Current)
                    {
                        if (workQuestion._time == 1)
                        {
                            workQuestion._state = WorkQuestionState.Error;
                        }
                        else if (workQuestion._time > 0 && byTimer)
                            workQuestion._time--;
                    }
                    if (!_disposing)
                        _ucNav.SetStateWorkQuestion(workQuestion);

                    //подсчет количества отвеченых вопросов
                    if (workQuestion._state == WorkQuestionState.Ok)
                        askedCount++;
                }
                _ucTimers.SetAskedQuestion(askedCount, _workQuestions.Count);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }

        /// <summary>Метод обновляет занчение таймер теста</summary>
        private void RefreshTestTimer(bool byTimer)
        {
            if (_ucTimers != null)
            {
                if (_testSeconds > 0)
                {
                    _ucTimers.SetTestTime(_testSeconds);
                }
                if (_testSeconds <= 0 && _testSeconds != -1)
                {
                    OnControlClosed();
                    //создание события что тест закончен
                    MessageForm.ShowErrorMsg("Время вышло. Тест закончен");
                    //посылка события о том что тест закончен

                    return;
                }
                if (_testSeconds != -1 && _currentType == ObjectDescription.Test && byTimer)
                    _testSeconds--;
            }
        }

        /// <summary>Метод обновляет занчение таймер вопроса</summary>
        private void RefreshQuestionTimer(bool ByTimer)
        {
            if (_ucTimers != null && _currentWorkQuestion != null)
            {
                if (_currentWorkQuestion._time > 0)
                {
                    _ucTimers.SetQuestionTime(_currentWorkQuestion._time);
                }
                else if (_currentWorkQuestion._state == WorkQuestionState.Error)
                {
                    //создание события что тест закончен
                    MessageForm.ShowErrorMsg("Время вышло. Вопрос пропущен");
                    //посылка события о том что вопрос закончен
                    SetCurrentWorkQuestionState(WorkQuestionState.Error);
                    GoToNextQuestion();
                }
                else
                {
                    //Если параметр null то значит часы выводить не надо
                    _ucTimers.HideQuestionTimer();
                }
            }
        }

        /// <summary>Метод разрушения контрола и освобождения ресурсов</summary>
        public override void DisposeControl()
        {
            Stop();
            _disposing = true;
            base.DisposeControl();
            Dispose();
        }


        /// <summary>порождение события на закрытие контрола </summary>
        private void OnControlClosed()
        {
            //выдаем событие на то что пользователь выбрал описание теста
            if (ControlClosed == null)
            {
                MessageForm.ShowWarningMsg("Нет подписаных на событие OnControlClosed");
            }
            else
            {
                try
                {
                    //using (new SessionScope())
                    //{
                        var ti = new TimeInterval(_testPassing.TimeInterval.StartTime,
                                                  _testPassing.TimeInterval.StartTime.AddSeconds(_testPassingSecond));
                        ti.CreateAndFlush();
                        _testPassing = ActiveRecordBase<TestPassing>.Find(_testPassing.Id);
                        _testPassing.TimeInterval = ti;
                        _testPassing.SaveAndFlush();
                    //}
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                    throw;
                }

                ControlClosed(this);
            }
        }


        /// <summary>метод устанавливает вопрос на экран </summary>
        /// <param name="question"></param>
        /// <param name="setFocusIntoNavBar">флаг говорит о том нужно ли переходить в панели навига
        /// ции на вопрос</param>
        private void SetQuestion(WorkQuestion question, bool setFocusIntoNavBar)
        {
            try
            {
                if (_currentWorkQuestion != null)
                {
                    //прошлый текущий вопрос ставим в состояние "ожидания"
                    //если у него состояние "текущий"
                    foreach (WorkQuestion workQuestion in _workQuestions)
                    {
                        if (workQuestion._order == _currentWorkQuestion._order &&
                            workQuestion._state == WorkQuestionState.Current)
                        {
                            workQuestion._state = WorkQuestionState.Wait;
                            break;
                        }
                        //сохраняем состояние в списке
                    }
                }
                //делаем вопрос текущим
                _currentWorkQuestion = question;

                //новый текущий вопрос ставим в состояние "текущий"
                SetCurrentWorkQuestionState(WorkQuestionState.Current);

                //переход на следующй вопрос в контроле навигации
                _ucNav.SetQuestion(_currentWorkQuestion, setFocusIntoNavBar);

                //создание вопроса в контроле вопросов
                _ucQuestion.SetQuestion(_currentWorkQuestion);

                //проверка на то что загруженный и текущий вопрос совпадают
                //и если не совпадают то прогрузка ответов для вопроса
                if (_currentWorkQuestion._question.Id != _nextQuestionId)
                    LoadQuestion(_currentWorkQuestion);

                //создание ответов в контроле ответов
                //передаю объект вопрос, из которого можно будет в
                if (_questionAnswers.Length == 0 && _testAnswers.Length == 0)
                {
                    //перехожу не следующий вопрос если нет ответов
                    GoToNextQuestion();
                }
                if (ParentForm == null)
                {
                    _ucAnswer.SetAnswers(_currentWorkQuestion, _questionAnswers, _testAnswers);
                }
                else
                {
                    MethodInvoker method =
                        delegate { _ucAnswer.SetAnswers(_currentWorkQuestion, _questionAnswers, _testAnswers); };
                    Invoke(method);
                }

                //обновление состояний
                if (ParentForm != null)
                    BeginInvoke(new InvokeDelegate(RefreshStates), false);

                //обновление таймера
                if (ParentForm != null)
                    BeginInvoke(new InvokeDelegate(RefreshQuestionTimer), false);

                //помечаем время начала отображения вопроса
                _beginThink = DateTime.Now;
                _thinkSecond = 0;

                //загрузка следующего вопроса из БД
                //выставляем флажок что необходимо загрузка
                //а поток сам загрузит
                _needQuestion = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw;
            }
        }

        #endregion

        #region обработчик событий от пользовательских контролов

        /// <summary>обработчик события от контрола навигации по ответам</summary>
        /// <param name="nextType"></param>
        private void _ucNavigationAnswer_NavigationAnswerButtonClick(NavigationAnswerActionType nextType)
        {
            try
            {
                switch (nextType)
                {
                    case NavigationAnswerActionType.DO_ANSWER:
                        {
                            //выбор ответа(ов)
                            ucAnswer_AnswerSelected(_ucAnswer.GetSelectedAnswer());
                            break;
                        }
                    case NavigationAnswerActionType.SKIP:
                        {
                            //if(!_testSettings.Many)
                            //{
                            //    MessageForm.ShowErrorMsg("В данном тесте не разрешено")
                            //}
                            //отмена ответа
                            //обновление состояний для контрола с таймерами
                            RefreshStates(false);
                            //перешли на следующий вопрос
                            GoToNextQuestion();
                            break;
                        }
                    case NavigationAnswerActionType.CLEAR:
                        {
                            //отмена ответа
                            _ucAnswer.ClearSelected();
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }

        /// <summary>обработчик события выбор вопроса в панели навигации</summary>
        /// <param name="questionID"></param>
        private void ucNav_SelectQuestion(int questionID)
        {
            WorkQuestion wq = FindQuestion(questionID);
            if (_currentWorkQuestion != null)
            {
                //отобразили состояние у текущего вопроса
                _ucNav.SetStateWorkQuestion(_currentWorkQuestion);
                //проверка установлен вопрос или нет
                if (questionID != _currentWorkQuestion._question.Id)
                {
                    SetQuestion(wq, false);
                    return;
                }
            }
            //если текущий вопрос пустой, то устанавливаем этот вопрос
            SetQuestion(wq, false);
        }

        /// <summary>Обработчик события выбора в панели навигации не правильного вопроса
        /// т.е отвеченого дибо пропущенного</summary>
        /// <param name="questionID"></param>
        private void _ucNav_NeedNextQuestion(int questionID)
        {
            GoToNextQuestion();
        }

        /// <summary> обработчик события на выбор ответа
        /// получение ответа и возможные реакции на это
        /// пока реакция есть выдаче следующего вопроса</summary>
        /// <param name="answer"></param>
        private void ucAnswer_AnswerSelected(Answer[] answers)
        {
            try
            {
                if (answers.Length == 0)
                {
                    MessageForm.ShowErrorMsg("Необходимо выбрать хотя бы 1 ответ");
                    return;
                }
                if (_currentType == ObjectDescription.Test)
                {
                    //если в данный момент мы отвечаем на вопросы теста
                    //запись в БД результаты вопроса
                    foreach (Answer answer in answers)
                    {
                        //using (new SessionScope(FlushAction.Auto))
                        //{
                            var tr = new TestResult(_testPassing, _currentWorkQuestion._question, answer,
                                                    new TimeInterval(_beginThink, _beginThink.AddSeconds(_thinkSecond)));
                            tr.CreateAndFlush();
                        //}
                    }
                }
                else
                {
                    //если вопросы методики
                    if (answers.Length > 1)
                    {
                        MessageForm.ShowErrorMsg("Необходимо выбрать только 1 ответ");
                        return;
                    }
                    if (_currentWorkQuestion.RightAnswer == null)
                    {
                        MessageForm.ShowErrorMsg(
                            "на данный вопрос не был задан правильный ответ. Обратитесь к обучающему специалисту");
                    }
                    else
                    {
                        if (!_currentWorkQuestion.RightAnswer.Equals(answers[0]))
                        {
                            MessageForm.ShowErrorMsg("Вы выбрали неправильный ответ");
                            return;
                        }
                    }
                }
                //поставили текущий вопрос в состояние "Ок"
                SetCurrentWorkQuestionState(WorkQuestionState.Ok);
                //обновление состояний для контрола с таймерами
                RefreshStates(false);
                //перешли на следующий вопрос
                GoToNextQuestion();
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw;
            }
        }

        /// <summary>обработчик события нажития кнопки "продолжить" в контроле описания методики</summary>
        /// <param name="control"></param>
        private void ucDescription_PressContinue(DesctiptionTestingControl control)
        {
            if (control.ParentForm != null)
            {
                control.ParentForm.Close();
            }
        }

        /// <summary>Обработчик стобытия нажатия на кнопку скрыть/показать панель навигации
        /// Кнопка на панели таймеров и статистики</summary>
        private void _ucTimers_simpleButtonHideClick(bool show)
        {
            if (show)
            {
                tableLayoutPanelMain.ColumnStyles.Remove(tableLayoutPanelMain.ColumnStyles[0]);
                tableLayoutPanelMain.ColumnStyles.Insert(0, new ColumnStyle(SizeType.Absolute, 200F));
            }
            else
            {
                tableLayoutPanelMain.ColumnStyles.Remove(tableLayoutPanelMain.ColumnStyles[0]);
                tableLayoutPanelMain.ColumnStyles.Insert(0, new ColumnStyle(SizeType.Absolute, 0F));
            }
        }

        #endregion

        #region Обработчик событий от стандартных контролов

        /// <summary>обработчик события секундного таймера </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _timerSecond_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                _timerViewTimes.Stop();

                //тест
                //вывод времени всего теста
                RefreshTestTimer(true);

                //текущий вопрос
                RefreshQuestionTimer(true);

                _timerViewTimes.Start();
            }
            catch (InvalidOperationException ex)
            {
                Log.Error(ex);
                //таймер разрушен в другом потоке при закрытии окна
                return;
            }
        }

        /// <summary>обработчик события таймера проверки состояния</summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _timerRefresStates_Elapsed(object sender, ElapsedEventArgs e)
        {
            _timerRefresStates.Stop();
            RefreshStates(true);
            _timerRefresStates.Start();
        }

        public override void ParentFormFormClosing(object sender, FormClosingEventArgs e)
        {
            if (!_disposing)
            {
                if (MessageForm.ShowYesNoQuestionMsg("Вы уверены, что хотите прервать выполнение тестирования") ==
                    DialogResult.No)
                {
                    e.Cancel = true;
                    return;
                }
                if (ParentForm != null)
                    ParentForm.DialogResult = DialogResult.Abort;

                OnControlClosed();
            }
            base.ParentFormFormClosing(sender, e);
        }

        #endregion
    }
}
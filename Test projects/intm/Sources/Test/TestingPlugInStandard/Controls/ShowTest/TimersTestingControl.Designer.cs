﻿namespace Eureca.Professional.TestingPlugInStandard.Controls.ShowTest
{
    partial class TimersTestingControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.progressBarControl1 = new DevExpress.XtraEditors.ProgressBarControl();
            this.simpleButtonHide = new DevExpress.XtraEditors.SimpleButton();
            this.labelControlTestTime = new DevExpress.XtraEditors.LabelControl();
            this.labelControlQuestionTime = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarControl1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(245, -3);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(127, 13);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "Оставшееся время теста";
            this.labelControl1.Visible = false;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(383, -3);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(139, 13);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "Оставшееся время вопроса";
            this.labelControl2.Visible = false;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(55, -3);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(105, 13);
            this.labelControl3.TabIndex = 4;
            this.labelControl3.Text = "Количество ответов";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(188, -3);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(29, 13);
            this.labelControl4.TabIndex = 5;
            this.labelControl4.Text = "0 из 3";
            // 
            // progressBarControl1
            // 
            this.progressBarControl1.Location = new System.Drawing.Point(55, 16);
            this.progressBarControl1.Name = "progressBarControl1";
            this.progressBarControl1.Properties.Maximum = 3;
            this.progressBarControl1.Properties.Step = 1;
            this.progressBarControl1.Size = new System.Drawing.Size(162, 27);
            this.progressBarControl1.TabIndex = 6;
            // 
            // simpleButtonHide
            // 
            this.simpleButtonHide.Location = new System.Drawing.Point(3, 16);
            this.simpleButtonHide.Name = "simpleButtonHide";
            this.simpleButtonHide.Size = new System.Drawing.Size(46, 27);
            this.simpleButtonHide.TabIndex = 7;
            this.simpleButtonHide.Text = "<<";
            this.simpleButtonHide.Click += new System.EventHandler(this.simpleButtonHide_Click);
            // 
            // labelControlTestTime
            // 
            this.labelControlTestTime.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.labelControlTestTime.Appearance.Options.UseFont = true;
            this.labelControlTestTime.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControlTestTime.Location = new System.Drawing.Point(265, 17);
            this.labelControlTestTime.Name = "labelControlTestTime";
            this.labelControlTestTime.Size = new System.Drawing.Size(66, 24);
            this.labelControlTestTime.TabIndex = 8;
            this.labelControlTestTime.Text = "0:00:00";
            this.labelControlTestTime.Visible = false;
            // 
            // labelControlQuestionTime
            // 
            this.labelControlQuestionTime.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.labelControlQuestionTime.Appearance.Options.UseFont = true;
            this.labelControlQuestionTime.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControlQuestionTime.Location = new System.Drawing.Point(418, 16);
            this.labelControlQuestionTime.Name = "labelControlQuestionTime";
            this.labelControlQuestionTime.Size = new System.Drawing.Size(65, 25);
            this.labelControlQuestionTime.TabIndex = 9;
            this.labelControlQuestionTime.Text = "0:00:00";
            this.labelControlQuestionTime.Visible = false;
            // 
            // TimersTestingControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.labelControlQuestionTime);
            this.Controls.Add(this.labelControlTestTime);
            this.Controls.Add(this.simpleButtonHide);
            this.Controls.Add(this.progressBarControl1);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Name = "TimersTestingControl";
            this.Size = new System.Drawing.Size(545, 45);
            ((System.ComponentModel.ISupportInitialize)(this.progressBarControl1.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.ProgressBarControl progressBarControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButtonHide;
        private DevExpress.XtraEditors.LabelControl labelControlTestTime;
        private DevExpress.XtraEditors.LabelControl labelControlQuestionTime;
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Eureca.Professional.ActiveRecords;
using Eureca.Professional.CommonClasses;


namespace Eureca.Professional.TestingPlugInStandard.Controls.ShowTest
{
    /// <summary>
    /// делега на передачу состояния ответа "наверх " к главному контролу, который уже и решает что с этим делать
    /// </summary>
    /// <param name="answers">передаваемый ответ</param>
    public delegate void AnswerEventHandler(ActiveRecords.Answer[] answers);

    /// <summary>Контрол ответов
    /// Созержит в себе таблицу контролов, в которой располагаются более простые контролы
    /// ответов(горизонтальные ответы, вертикальные ответы)
    /// он получает событие от более простых контролов и передает наверх основной форме результат
    /// принимать или не принимать ответ</summary>
    public partial class AnswerTestingControl : BaseTestingControl
    {
        #region fields

        /// <summary>
        /// ссылка на объект теста
        /// </summary>
        private readonly Test _test;

        public List<BaseSimpleAnswer> _uControls = new List<BaseSimpleAnswer>();

        #endregion

        #region events

        /// <summary>событие на выбор ответа</summary>
        public event AnswerEventHandler _AnswerSelected;

        #endregion

        #region constructor

        /// <summary>
        /// конструктор контрола
        /// </summary>
        /// <param name="allowUserSelectQuestion">флаг разрешения пользователю выбирать вопросы
        /// если флаг выставлен в "true", значит мы не знаем какой вопрос может выбрать пользователь следующим, поэтому приходится 
        /// подгружать ответы 
        /// а если в "false" тогда мы заранее знаем какие ответы можно использовать
        /// </param>
        public AnswerTestingControl(Test test)
        {
            InitializeComponent();
            _test = test;
        }

        #endregion

        #region methods

        public override void InitControl()
        {
            base.InitControl();
        }

        /// <summary>Посылка события о том что пользователь выбрал ответ </summary>
        /// <param name="answers"></param>
        private void OnAnswerSelected(ActiveRecords.Answer[] answers)
        {
            if (_AnswerSelected == null)
            {
                MessageForm.ShowWarningMsg("Нет подписаных на событие _AnswerSelected");
            }
            else
            {
                _AnswerSelected(answers);
            }
        }


        /// <summary>
        /// метод создает контрол ответов на вопрос
        /// </summary>
        /// <param name="testQquestion"></param>
        /// <param name="tempQuestionAnswers"></param>
        /// <param name="tempTestAnswers"></param>
        public void SetAnswers(WorkQuestion testQquestion, QuestionAnswer[] tempQuestionAnswers,
                               TestAnswer[] tempTestAnswers)
        {
            int answersCount = tempQuestionAnswers.Length + tempTestAnswers.Length;
            int i;
            //добавляем все ответы в один список

            var answersOrder =
                (from questionAnswers in tempQuestionAnswers
                 select new {questionAnswers.Answer, questionAnswers.AnswerOrder}).
                    Union
                    (from testAnswers in tempTestAnswers
                     select new {testAnswers.Answer, testAnswers.AnswerOrder}).
                    OrderBy(res => res.AnswerOrder);


            //проверка на необходимость создания контрола
            if (tableLayoutPanel1.RowCount > answersCount || tableLayoutPanel1.RowCount < answersCount ||
                answersCount == 1)
            {
                //необходимо пересоздание контрола
                tableLayoutPanel1.RowCount = answersCount;
                tableLayoutPanel1.RowStyles.Clear();
                tableLayoutPanel1.Controls.Clear();

                for (i = 0; i < answersCount; i++)
                {
                    //создаем контрол ответа(горизонтального)
                    BaseSimpleAnswer uc = new SimpleHorisontalAnswer(_test);
                    uc.InitControl();
                    uc.SimpleAnswerSelected += uc_SimpleAnswerSelected;
                    uc.SimpleAnswerSelecting += uc_SimpleAnswerSelecting;
                    tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 100/answersCount));
                    tableLayoutPanel1.Controls.Add(uc.ShowAsControl(), 0, i);
                }
            }

            //запись в контрол
            i = 0;
            _uControls.Clear();
            foreach (var answerOrder in answersOrder)
            {
                var uc = (BaseSimpleAnswer) tableLayoutPanel1.GetControlFromPosition(0, i);
                _uControls.Add(uc);
                uc.SetCheckState(false);
                uc.SetAnswer(answerOrder.Answer);
                i++;
            }
        }

        /// <summary>
        /// метод возвращает выбранные ответы
        /// </summary>
        /// <returns></returns>
        public Answer[] GetSelectedAnswer()
        {
            var res = new List<Answer>();
            foreach (BaseSimpleAnswer uc in _uControls)
            {
                if (uc.GetCheckState())
                {
                    res.Add(uc._answer);
                }
            }
            return res.ToArray();
        }

        public void ClearSelected()
        {
            foreach (BaseSimpleAnswer uc in _uControls)
            {
                uc.SetCheckState(false);
            }
        }

        #endregion

        #region обработчики событий пользовательских контролов

        /// <summary>событие ответа пользователя
        /// происходит дублирование ответа наверх к основной форме
        /// для того чтобы основная форма принимала решение относителино ответа
        /// (запись в БД, перезапуск таймера .....)</summary>
        /// <param name="control"></param>
        /// <param name="answer"></param>
        private void uc_SimpleAnswerSelected(BaseSimpleAnswer control, Answer answer)
        {
            //посылка событя наверх

            OnAnswerSelected(new[] {answer});
        }

        /// <summary>обытие преднамереного ответа от пользователя
        /// выставляется в контроле галочка
        /// в других контролах галочка убирается</summary>
        /// <param name="control">контрол с ответом</param>
        /// <param name="answer">ответ</param>
        private void uc_SimpleAnswerSelecting(BaseSimpleAnswer control, Answer answer)
        {
            TestSettings ts = TestSettings.GetSettings(_test.Options);
            if (ts.Many)
            {
                //множественный выбор разрешен, поэтому меняем состояние ответа в контроле

                control.SetCheckState(!control.IsNeedChangeCheckState());
            }
            else
            {
                //если не разрешен множественный выбор ответа то помечаем один как выделенный 
                //а остальные как не выделенные
                foreach (BaseSimpleAnswer ans in tableLayoutPanel1.Controls)
                {
                    if (ans.Equals(control))
                    {
                        //кроме теущего контрола
                        continue;
                    }
                    //проставляю состояние переключателей других контролов в UnChecked
                    ans.SetCheckState(false);
                }
            }
        }

        #endregion
    }
}
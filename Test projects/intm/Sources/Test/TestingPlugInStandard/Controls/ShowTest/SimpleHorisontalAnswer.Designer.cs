﻿using System.Windows.Forms;
using System;
using System.Windows;
using Eureca.Professional.TestingPlugInStandard.Components;

namespace Eureca.Professional.TestingPlugInStandard.Controls.ShowTest
{
    partial class SimpleHorisontalAnswer
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.answerLabel = new System.Windows.Forms.Label();
            this.exRichTextBoxAnswer = new System.Windows.Forms.RichTextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.doubleClickRadioButton1 = new Eureca.Professional.TestingPlugInStandard.Components.DoubleClickRadioButton();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // answerLabel
            // 
            this.answerLabel.BackColor = System.Drawing.Color.White;
            this.answerLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.answerLabel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.answerLabel.Location = new System.Drawing.Point(0, 0);
            this.answerLabel.Name = "answerLabel";
            this.answerLabel.Size = new System.Drawing.Size(518, 68);
            this.answerLabel.TabIndex = 3;
            this.answerLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.answerLabel.Click += new System.EventHandler(this.beginSelectAnswer);
            this.answerLabel.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.endSelectAnswer);
            // 
            // exRichTextBoxAnswer
            // 
            this.exRichTextBoxAnswer.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.exRichTextBoxAnswer.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.exRichTextBoxAnswer.CausesValidation = false;
            this.exRichTextBoxAnswer.Cursor = System.Windows.Forms.Cursors.Default;
            this.exRichTextBoxAnswer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.exRichTextBoxAnswer.HideSelection = false;
            this.exRichTextBoxAnswer.Location = new System.Drawing.Point(0, 0);
            this.exRichTextBoxAnswer.Name = "exRichTextBoxAnswer";
            this.exRichTextBoxAnswer.ReadOnly = true;
            this.exRichTextBoxAnswer.Size = new System.Drawing.Size(518, 68);
            this.exRichTextBoxAnswer.TabIndex = 5;
            this.exRichTextBoxAnswer.Text = "";
            this.exRichTextBoxAnswer.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.endSelectAnswer);
            this.exRichTextBoxAnswer.Click += new System.EventHandler(this.beginSelectAnswer);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.exRichTextBoxAnswer);
            this.panel1.Controls.Add(this.answerLabel);
            this.panel1.Location = new System.Drawing.Point(24, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(518, 68);
            this.panel1.TabIndex = 6;
            // 
            // doubleClickRadioButton1
            // 
            this.doubleClickRadioButton1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.doubleClickRadioButton1.Location = new System.Drawing.Point(0, 0);
            this.doubleClickRadioButton1.Name = "doubleClickRadioButton1";
            this.doubleClickRadioButton1.Size = new System.Drawing.Size(542, 68);
            this.doubleClickRadioButton1.TabIndex = 4;
            this.doubleClickRadioButton1.TabStop = true;
            this.doubleClickRadioButton1.UseVisualStyleBackColor = true;
            this.doubleClickRadioButton1.Click += new System.EventHandler(this.beginSelectAnswer);
            this.doubleClickRadioButton1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.endSelectAnswer);
            // 
            // SimpleHorisontalAnswer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.doubleClickRadioButton1);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "SimpleHorisontalAnswer";
            this.Size = new System.Drawing.Size(542, 68);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Label answerLabel;
        private RichTextBox exRichTextBoxAnswer;
        private Panel panel1;
        private DoubleClickRadioButton doubleClickRadioButton1;

    }
}
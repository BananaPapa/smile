﻿namespace Eureca.Professional.TestingPlugInStandard.Controls.ShowTest
{
    partial class NavigationAnswerTestingControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.simpleButtonDoAnswer = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonSkip = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonClear = new DevExpress.XtraEditors.SimpleButton();
            this.SuspendLayout();
            // 
            // simpleButtonDoAnswer
            // 
            this.simpleButtonDoAnswer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonDoAnswer.Location = new System.Drawing.Point(222, 3);
            this.simpleButtonDoAnswer.Name = "simpleButtonDoAnswer";
            this.simpleButtonDoAnswer.Size = new System.Drawing.Size(118, 23);
            this.simpleButtonDoAnswer.TabIndex = 0;
            this.simpleButtonDoAnswer.Text = "Ответить";
            this.simpleButtonDoAnswer.Click += new System.EventHandler(this.simpleButtonDoAnswer_Click);
            // 
            // simpleButtonSkip
            // 
            this.simpleButtonSkip.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonSkip.Location = new System.Drawing.Point(384, 3);
            this.simpleButtonSkip.Name = "simpleButtonSkip";
            this.simpleButtonSkip.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonSkip.TabIndex = 1;
            this.simpleButtonSkip.Text = "Пропустить";
            this.simpleButtonSkip.Click += new System.EventHandler(this.simpleButtonSkip_Click);
            // 
            // simpleButtonClear
            // 
            this.simpleButtonClear.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonClear.Location = new System.Drawing.Point(498, 3);
            this.simpleButtonClear.Name = "simpleButtonClear";
            this.simpleButtonClear.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonClear.TabIndex = 2;
            this.simpleButtonClear.Text = "Очистить";
            this.simpleButtonClear.Click += new System.EventHandler(this.simpleButtonClear_Click);
            // 
            // NavigationAnswerTestingControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.simpleButtonClear);
            this.Controls.Add(this.simpleButtonSkip);
            this.Controls.Add(this.simpleButtonDoAnswer);
            this.Name = "NavigationAnswerTestingControl";
            this.Size = new System.Drawing.Size(576, 29);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton simpleButtonDoAnswer;
        private DevExpress.XtraEditors.SimpleButton simpleButtonSkip;
        private DevExpress.XtraEditors.SimpleButton simpleButtonClear;
    }
}

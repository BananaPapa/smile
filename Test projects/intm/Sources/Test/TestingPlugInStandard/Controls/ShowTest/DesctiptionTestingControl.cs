﻿using System;
using System.IO;
using System.Windows.Forms;
using Castle.ActiveRecord;
using Eureca.Professional.ActiveRecords;
using Eureca.Professional.CommonClasses;


namespace Eureca.Professional.TestingPlugInStandard.Controls.ShowTest
{
    /// <summary>
    /// делегат на событие от класса окна описания
    /// </summary>
    /// <param name="control"></param>
    public delegate void DescriptionEventHandler(DesctiptionTestingControl control);

    /// <summary>
    /// контрол окна описания
    /// </summary>
    public partial class DesctiptionTestingControl : BaseTestingControl
    {
        private readonly ObjectDescription _objectDescription = ObjectDescription.None;
        private readonly Test _test;

        #region Events

        /// <summary>
        /// событие нажатие клавиши продолжить
        /// </summary>
        public event DescriptionEventHandler PressContinue;

        #endregion

        #region конструктор

        public DesctiptionTestingControl(ObjectDescription objectDescription, Test test)
        {
            _objectDescription = objectDescription;
            _test = test;
            InitializeComponent();
        }

        #endregion

        #region методы

        public override void InitControl()
        {
            base.InitControl();

            {
                exRichTextBox1.Clear();
                //try
                //{
                if (_objectDescription == ObjectDescription.Method)
                {
                    CaptionControl = _test.Method.Title;
                    //загрузка контента
                    //using (new SessionScope())
                    //{
                    _test.Method.Instruction = ActiveRecordBase<Content>.Find(_test.Method.Instruction.Id);
                    //}
                    if (_test.Method.Instruction.PlainContent == null)
                    {
                        var ms = new MemoryStream(_test.Method.Instruction.RichContent);
                        exRichTextBox1.LoadFile(ms, RichTextBoxStreamType.RichText);
                    }
                    else
                        exRichTextBox1.AppendText(_test.Method.Instruction.PlainContent);
                }
                if (_objectDescription == ObjectDescription.Test)
                {
                    CaptionControl = _test.Title;
                    //загрузка контента
                    //using (new SessionScope())
                    //{
                    _test.Description = ActiveRecordBase<Content>.Find(_test.Description.Id);
                    //}
                    if (_test.Description.PlainContent == null)
                    {
                        var ms = new MemoryStream(_test.Description.RichContent);
                        exRichTextBox1.LoadFile(ms, RichTextBoxStreamType.RichText);
                    }
                    else
                        exRichTextBox1.AppendText(_test.Description.PlainContent);
                }
                exRichTextBox1.ScrollToCaret();
                //}
                //catch (Exception ex)
                //{
                //    MessageForm.Show(ex.Message);
                //}
            }
        }


        private void OnPressContinue()
        {
            //выдаем событие на то что пользователь выбрал описание теста
            if (PressContinue == null)
            {
                MessageForm.ShowWarningMsg("Нет подписаных на событие PressContinue");
            }
            else
            {
                PressContinue(this);
            }
        }

        #endregion

        #region обработчики событий

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            OnPressContinue();
        }

        #endregion
    }
}
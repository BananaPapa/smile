﻿namespace Eureca.Professional.TestingPlugInStandard.Controls.ShowTest
{
    partial class MainTestingControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
                //_iSessionScope.Dispose();
               

            }

            _timerRefresStates.Stop();
            _timerRefresStates.Dispose();

            _timerViewTimes.Stop();
            _timerViewTimes.Dispose();

            //base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupControlNavigation = new DevExpress.XtraEditors.GroupControl();
            this.groupControlTesting = new DevExpress.XtraEditors.GroupControl();
            this.tableLayoutPanelTesting = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanelMain = new System.Windows.Forms.TableLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlNavigation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlTesting)).BeginInit();
            this.groupControlTesting.SuspendLayout();
            this.tableLayoutPanelMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupControlNavigation
            // 
            this.groupControlNavigation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControlNavigation.Location = new System.Drawing.Point(0, 0);
            this.groupControlNavigation.Margin = new System.Windows.Forms.Padding(0);
            this.groupControlNavigation.Name = "groupControlNavigation";
            this.groupControlNavigation.ShowCaption = false;
            this.groupControlNavigation.Size = new System.Drawing.Size(200, 569);
            this.groupControlNavigation.TabIndex = 0;
            this.groupControlNavigation.Text = "Панель навигации";
            // 
            // groupControlTesting
            // 
            this.groupControlTesting.Controls.Add(this.tableLayoutPanelTesting);
            this.groupControlTesting.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControlTesting.Location = new System.Drawing.Point(200, 0);
            this.groupControlTesting.Margin = new System.Windows.Forms.Padding(0);
            this.groupControlTesting.Name = "groupControlTesting";
            this.groupControlTesting.ShowCaption = false;
            this.groupControlTesting.Size = new System.Drawing.Size(602, 569);
            this.groupControlTesting.TabIndex = 1;
            this.groupControlTesting.Text = "Панель тестирования";
            // 
            // tableLayoutPanelTesting
            // 
            this.tableLayoutPanelTesting.AutoSize = true;
            this.tableLayoutPanelTesting.ColumnCount = 1;
            this.tableLayoutPanelTesting.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelTesting.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelTesting.Location = new System.Drawing.Point(2, 2);
            this.tableLayoutPanelTesting.Name = "tableLayoutPanelTesting";
            this.tableLayoutPanelTesting.RowCount = 4;
            this.tableLayoutPanelTesting.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanelTesting.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelTesting.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 311F));
            this.tableLayoutPanelTesting.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanelTesting.Size = new System.Drawing.Size(598, 565);
            this.tableLayoutPanelTesting.TabIndex = 0;
            // 
            // tableLayoutPanelMain
            // 
            this.tableLayoutPanelMain.ColumnCount = 2;
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelMain.Controls.Add(this.groupControlNavigation, 0, 0);
            this.tableLayoutPanelMain.Controls.Add(this.groupControlTesting, 1, 0);
            this.tableLayoutPanelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelMain.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelMain.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanelMain.Name = "tableLayoutPanelMain";
            this.tableLayoutPanelMain.RowCount = 1;
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelMain.Size = new System.Drawing.Size(802, 569);
            this.tableLayoutPanelMain.TabIndex = 2;
            // 
            // MainTestingControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanelMain);
            this.Name = "MainTestingControl";
            this.Size = new System.Drawing.Size(802, 569);
            ((System.ComponentModel.ISupportInitialize)(this.groupControlNavigation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlTesting)).EndInit();
            this.groupControlTesting.ResumeLayout(false);
            this.groupControlTesting.PerformLayout();
            this.tableLayoutPanelMain.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControlNavigation;
        private DevExpress.XtraEditors.GroupControl groupControlTesting;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelTesting;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelMain;



    }
}
﻿
using Eureca.Professional.TestingPlugInStandard.Components;

namespace Eureca.Professional.TestingPlugInStandard.Controls.ShowTest
{
    partial class DesctiptionTestingControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.exRichTextBox1 = new ExRichTextBox();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.SuspendLayout();
            // 
            // exRichTextBox1
            // 
            this.exRichTextBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.exRichTextBox1.AutoWordSelection = true;
            this.exRichTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.exRichTextBox1.Cursor = System.Windows.Forms.Cursors.Default;
            this.exRichTextBox1.EnableAutoDragDrop = true;
            this.exRichTextBox1.HiglightColor = RtfColor.White;
            this.exRichTextBox1.Location = new System.Drawing.Point(0, 0);
            this.exRichTextBox1.Name = "exRichTextBox1";
            this.exRichTextBox1.ReadOnly = true;
            this.exRichTextBox1.Size = new System.Drawing.Size(519, 219);
            this.exRichTextBox1.TabIndex = 0;
            this.exRichTextBox1.Text = "";
            this.exRichTextBox1.TextColor = RtfColor.Black;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.5F);
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.Location = new System.Drawing.Point(377, 225);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(130, 47);
            this.simpleButton1.TabIndex = 1;
            this.simpleButton1.Text = "Продолжить";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // DesctiptionTestingControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.exRichTextBox1);
            this.MinimumSize = new System.Drawing.Size(519, 275);
            this.Name = "DesctiptionTestingControl";
            this.Size = new System.Drawing.Size(519, 275);
            this.ResumeLayout(false);

        }

        #endregion

        private ExRichTextBox exRichTextBox1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
    }
}
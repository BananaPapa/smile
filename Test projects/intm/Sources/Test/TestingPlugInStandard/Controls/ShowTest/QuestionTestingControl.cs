﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using Castle.ActiveRecord;
using Eureca.Professional.ActiveRecords;
using Eureca.Professional.CommonClasses;


namespace Eureca.Professional.TestingPlugInStandard.Controls.ShowTest
{
    public partial class QuestionTestingControl : BaseTestingControl
    {
        public QuestionTestingControl()
        {
            InitializeComponent();
        }

        public override void InitControl()
        {
            base.InitControl();
        }

        public void SetQuestion(WorkQuestion testQuestion)
        {
            if (InvokeRequired)
            {
                Invoke(new SetQuestionDeletate(SetQuestion), testQuestion);
            }
            else
            {
                using (new SessionScope())
                {
                    var question = Question.Find(testQuestion._question.Id);
                    if (question.Content.PlainContent == null)
                    {
                        exRichTextBoxQuestion.Visible = true;
                        questionLabel.Visible = false;
                        var ms = new MemoryStream( question.Content.RichContent );
                        exRichTextBoxQuestion.LoadFile(ms, RichTextBoxStreamType.RichText);
                    }
                    else
                    {
                        exRichTextBoxQuestion.Visible = false;
                        questionLabel.Visible = true;
                        int linesCount =
                            question.Content.PlainContent.Split( new[] { Environment.NewLine },
                                StringSplitOptions.None).Length;

                        questionLabel.Font =
                            new Font("Microsoft Sans Serif",
                                25F, FontStyle.Bold,
                                GraphicsUnit.Pixel,
                                204);

                        questionLabel.Padding = new Padding(10);
                        questionLabel.Text = question.Content.PlainContent;
                    }
                }
            }
        }

        #region Nested type: SetQuestionDeletate

        private delegate void SetQuestionDeletate(WorkQuestion testQuestion);

        #endregion
    }
}
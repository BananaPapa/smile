﻿namespace Eureca.Professional.TestingPlugInStandard.Controls.ShowTest
{
    partial class NavigationTestingControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NavigationTestingControl));
            this.treeListColumn2 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListColumn4 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeList1 = new DevExpress.XtraTreeList.TreeList();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.treeList1)).BeginInit();
            this.SuspendLayout();
            // 
            // treeListColumn2
            // 
            this.treeListColumn2.Caption = "Вопрос";
            this.treeListColumn2.FieldName = "QuestionNumber";
            this.treeListColumn2.ImageAlignment = System.Drawing.StringAlignment.Center;
            this.treeListColumn2.MinWidth = 35;
            this.treeListColumn2.Name = "treeListColumn2";
            this.treeListColumn2.OptionsColumn.AllowEdit = false;
            this.treeListColumn2.OptionsColumn.AllowMove = false;
            this.treeListColumn2.OptionsColumn.AllowSort = false;
            this.treeListColumn2.Visible = true;
            this.treeListColumn2.VisibleIndex = 0;
            this.treeListColumn2.Width = 145;
            // 
            // treeListColumn4
            // 
            this.treeListColumn4.Caption = "ID";
            this.treeListColumn4.FieldName = "ID";
            this.treeListColumn4.Name = "treeListColumn4";
            this.treeListColumn4.UnboundType = DevExpress.XtraTreeList.Data.UnboundColumnType.Integer;
            this.treeListColumn4.Width = 40;
            // 
            // treeList1
            // 
            this.treeList1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.DodgerBlue;
            this.treeList1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.treeList1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.treeList1.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
                                                                                                     this.treeListColumn2,
                                                                                                     this.treeListColumn4});
            this.treeList1.ColumnsImageList = this.imageList1;
            this.treeList1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeList1.Location = new System.Drawing.Point(0, 0);
            this.treeList1.Name = "treeList1";
            this.treeList1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.treeList1.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.treeList1.OptionsView.ShowIndicator = false;
            this.treeList1.OptionsView.ShowRoot = false;
            this.treeList1.OptionsView.ShowVertLines = false;
            this.treeList1.Size = new System.Drawing.Size(206, 629);
            this.treeList1.StateImageList = this.imageList1;
            this.treeList1.TabIndex = 0;
            this.treeList1.TreeLevelWidth = 12;
            this.treeList1.TreeLineStyle = DevExpress.XtraTreeList.LineStyle.None;
            this.treeList1.BeforeFocusNode += new DevExpress.XtraTreeList.BeforeFocusNodeEventHandler(this.treeList1_BeforeFocusNode);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "Question.jpg");
            this.imageList1.Images.SetKeyName(1, "wait.jpg");
            this.imageList1.Images.SetKeyName(2, "ок.jpg");
            this.imageList1.Images.SetKeyName(3, "error.jpg");
            // 
            // NavigationTestingControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.treeList1);
            this.Name = "NavigationTestingControl";
            this.Size = new System.Drawing.Size(206, 629);
            ((System.ComponentModel.ISupportInitialize)(this.treeList1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn2;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn4;
        private DevExpress.XtraTreeList.TreeList treeList1;
        private System.Windows.Forms.ImageList imageList1;



    }
}
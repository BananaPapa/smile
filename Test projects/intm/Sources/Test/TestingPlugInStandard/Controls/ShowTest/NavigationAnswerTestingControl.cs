﻿using System;
using Eureca.Professional.ActiveRecords;
using Eureca.Professional.CommonClasses;

namespace Eureca.Professional.TestingPlugInStandard.Controls.ShowTest
{
    public delegate void NavigationAnswerEventHandler(NavigationAnswerActionType nextType);

    /// <summary>
    /// класс - контрол который инкапсулирует обработчики множественного выбора 
    /// ответов.
    /// </summary>
    public partial class NavigationAnswerTestingControl : BaseTestingControl
    {
        private readonly Test _test;

        private NavigationAnswerTestingControl()
        {
            InitializeComponent();
        }

        public NavigationAnswerTestingControl(Test test)
        {
            _test = test;
            InitializeComponent();

            TestSettings testSettings = TestSettings.GetSettings(_test.Options);
            simpleButtonDoAnswer.Visible = testSettings.Many;
            simpleButtonSkip.Visible = testSettings.AllowUserSkipQuestion;
        }

        /// <summary>
        /// событие которое выдает контрол навигиции по ответам
        /// </summary>
        public event NavigationAnswerEventHandler NavigationAnswerButtonClick;

        /// <summary>
        /// метод инициализации контрола
        /// </summary>
        public new void InitControl()
        {
            base.InitControl();
        }


        /// <summary>
        /// активация события нажатия на кнопку "Ответить"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButtonDoAnswer_Click(object sender, EventArgs e)
        {
            NavigationAnswerButtonClick(NavigationAnswerActionType.DO_ANSWER);
        }

        /// <summary>
        /// активация события нажатия на кнопку "Пропустить"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButtonSkip_Click(object sender, EventArgs e)
        {
            NavigationAnswerButtonClick(NavigationAnswerActionType.SKIP);
        }

        /// <summary>
        /// активация события нажатия на кнопку "Очистить"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButtonClear_Click(object sender, EventArgs e)
        {
            NavigationAnswerButtonClick(NavigationAnswerActionType.CLEAR);
        }
    }

    /// <summary>
    /// перечисление действий в панели навигации
    /// </summary>
    public enum NavigationAnswerActionType
    {
        DO_ANSWER,
        SKIP,
        CLEAR
    } ;
}
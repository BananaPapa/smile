﻿
using Eureca.Professional.TestingPlugInStandard.Components;

namespace Eureca.Professional.TestingPlugInStandard.Controls.ShowTest
{
    partial class QuestionTestingControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.exRichTextBoxQuestion = new ExRichTextBox();
            this.questionLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // exRichTextBoxQuestion
            // 
            this.exRichTextBoxQuestion.AutoWordSelection = true;
            this.exRichTextBoxQuestion.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.exRichTextBoxQuestion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.exRichTextBoxQuestion.EnableAutoDragDrop = true;
            this.exRichTextBoxQuestion.HiglightColor = RtfColor.White;
            this.exRichTextBoxQuestion.Location = new System.Drawing.Point(0, 0);
            this.exRichTextBoxQuestion.Name = "exRichTextBoxQuestion";
            this.exRichTextBoxQuestion.ReadOnly = true;
            this.exRichTextBoxQuestion.Size = new System.Drawing.Size(741, 336);
            this.exRichTextBoxQuestion.TabIndex = 0;
            this.exRichTextBoxQuestion.Text = "";
            this.exRichTextBoxQuestion.TextColor = RtfColor.Black;
            // 
            // questionLabel
            // 
            this.questionLabel.BackColor = System.Drawing.Color.White;
            this.questionLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.questionLabel.Location = new System.Drawing.Point(0, 0);
            this.questionLabel.Name = "questionLabel";
            this.questionLabel.Size = new System.Drawing.Size(741, 336);
            this.questionLabel.TabIndex = 1;
            this.questionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // QuestionTestingControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.questionLabel);
            this.Controls.Add(this.exRichTextBoxQuestion);
            this.Name = "QuestionTestingControl";
            this.Size = new System.Drawing.Size(741, 336);
            this.ResumeLayout(false);

        }

        #endregion

        private ExRichTextBox exRichTextBoxQuestion;
        private System.Windows.Forms.Label questionLabel;
    }
}
﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using Eureca.Professional.ActiveRecords;

namespace Eureca.Professional.TestingPlugInStandard.Controls.ShowTest
{
    public partial class SimpleHorisontalAnswer : BaseSimpleAnswer, ISimpleAnswer
    {
        #region fields

        private readonly Test _test;

        #endregion

        #region constructor

        public SimpleHorisontalAnswer(Test test)
        {
            _test = test;
            InitializeComponent();
        }

        #endregion

        #region Methods

        /// <summary>Установка ответа</summary>
        /// <param name="answer"></param>
        public override void SetAnswer(Answer answer)
        {
            if (_answer != null && _answer.Id == answer.Id)
            {
                //если ответ этот же то выход
                return;
            }
            _answer = answer;

            if (InvokeRequired)
            {
                Invoke(new SetAnswerDelegate(SetAnswer), answer);
            }

            else
            {
                if (answer.Content.PlainContent == null)
                {
                    exRichTextBoxAnswer.Visible = true;
                    answerLabel.Visible = false;
                    var ms = new MemoryStream(answer.Content.RichContent);
                    exRichTextBoxAnswer.LoadFile(ms, RichTextBoxStreamType.RichText);
                }
                else
                {
                    exRichTextBoxAnswer.Visible = false;
                    answerLabel.Visible = true;
                    answerLabel.Font =
                        new Font("Microsoft Sans Serif",
                                 25F, FontStyle.Bold,
                                 GraphicsUnit.Pixel,
                                 204);

                    answerLabel.Padding = new Padding(10);
                    answerLabel.Text = answer.Content.PlainContent;
                }
            }
        }

        /// <summary>Установка ответа</summary>
        public override void SetCheckState(bool state)
        {
            if (state)
            {
                answerLabel.BackColor = SystemColors.Highlight;
                exRichTextBoxAnswer.BackColor = SystemColors.Highlight;
            }
            else
            {
                answerLabel.BackColor = BackColor;
                exRichTextBoxAnswer.BackColor = BackColor;
            }
            doubleClickRadioButton1.Checked = state;
            LastCheckState = doubleClickRadioButton1.Checked;
        }

        public override bool GetCheckState()
        {
            return doubleClickRadioButton1.Checked;
        }

        /// <summary>
        /// перегруженный метод получения состояния
        /// определяется по 2 параметрам
        /// 1) текущее состояние переключателя 
        /// 2) прошлое состояние переключателя
        /// </summary>
        /// <returns></returns>
        public override bool IsNeedChangeCheckState()
        {
            if (LastCheckState)
            {
                return doubleClickRadioButton1.Checked;
            }
            return !doubleClickRadioButton1.Checked;
        }

        public override void InitControl()
        {
            base.InitControl();
        }


        /// <summary>
        /// событие клика на элементы контрола
        /// по этому событяю выставляю ответ как помеченый
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void beginSelectAnswer(object sender, EventArgs e)
        {
            doubleClickRadioButton1.Checked = true;
            answerLabel.BackColor = SystemColors.Highlight;
            exRichTextBoxAnswer.BackColor = SystemColors.Highlight;
            OnSimpleAnswerSelecting(_answer);
        }

        //событие выбора ответа получается при двойной клик на контрол
        /// <summary>
        /// событие двойного клика на элементы контрола
        /// по этому событию выставляется ответ как принятый если не разрешен множественный выбор 
        /// и как помеченый, если разрешен
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void endSelectAnswer(object sender, MouseEventArgs e)
        {
            TestSettings ts = TestSettings.GetSettings(_test.Options);
            if (ts.Many)
            {
                //если разрешем множественный выбор то ставим как "выделено"
                beginSelectAnswer(sender, e);
            }
            else
            {
                //множественный выбор не разрешен, поэтому ставим как "принять ответ"
                OnSimpleAnswerSelected(_answer);
            }
        }

        #endregion

        #region Nested type: SetAnswerDelegate

        /// <summary>
        /// делегат на установку состояния ответа
        /// </summary>
        /// <param name="answer"></param>
        private delegate void SetAnswerDelegate(Answer answer);

        #endregion
    }
}
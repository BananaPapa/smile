﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Eureca.Professional.CommonClasses;
using log4net;


namespace Eureca.Professional.TestingPlugInStandard.Controls.ShowTest
{
    public delegate void TimersEventHandler(bool show);

    /// <summary>
    /// контрол таймеров теста
    /// </summary>
    public partial class TimersTestingControl : BaseTestingControl
    {
        private static readonly ILog Log = LogManager.GetLogger( typeof( TimersTestingControl ) );

        #region события

        /// <summary>
        /// события нажатия на кнопну "скыть окно навигации"
        /// </summary>
        public event TimersEventHandler simpleButtonHideClick;

        #endregion

        #region поля

        /// <summary>Поле состояния окна навигации </summary>
        private bool _show = true;

        #endregion

        #region конструктор

        public TimersTestingControl()
        {
            InitializeComponent();
        }

        #endregion

        #region методы

        public override void InitControl()
        {
            base.InitControl();
        }

        public void SetAskedQuestion(int asked, int total)
        {
            if (ParentForm == null)
                return;
            MethodInvoker method = delegate
                                       {
                                           if (progressBarControl1.Properties.Maximum != total)
                                               progressBarControl1.Properties.Maximum = total;
                                           if (progressBarControl1.Position != asked)
                                               progressBarControl1.Position = asked;
                                           labelControl4.Text = string.Format("{0} из {1}", asked, total);
                                       };
            Invoke(method);
        }

        public void SetTestTime(int seconds)
        {
            int r, g;
            int koefficient = 1;

            if (seconds > 255/koefficient)
            {
                r = 0;
                g = 255;
            }
            else
            {
                if (seconds > 127/koefficient)
                {
                    g = 255;
                    r = (255 - seconds*koefficient)*2;
                }
                else
                {
                    r = 255;
                    g = seconds*koefficient*2;
                }
            }

            if (InvokeRequired)
            {
                try
                {
                    BeginInvoke(new SetTestTimeDelegate(SetTestTime), seconds);
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                    MessageForm.ShowErrorMsg(ex.ToString());
                    return;
                }
            }

            if (ParentForm == null)
            {
                //изменение цвета

                labelControlTestTime.BackColor = Color.FromArgb(((((byte) (r)))), ((((byte) (g)))), ((((0)))));
                labelControlTestTime.Visible = true;
                labelControl1.Visible = true;
                //установка времени

                DateTime _time = new DateTime().AddSeconds(seconds);
                string time = _time.ToLongTimeString();

                labelControlTestTime.Text = time;
            }
            else
            {
                MethodInvoker method = delegate
                                           {
                                               //изменение цвета

                                               labelControlTestTime.BackColor = Color.FromArgb(((((byte) (r)))),
                                                                                               ((((byte) (g)))),
                                                                                               ((((0)))));
                                               labelControlTestTime.Visible = true;
                                               labelControl1.Visible = true;
                                               //установка времени

                                               DateTime _time = new DateTime().AddSeconds(seconds);
                                               string time = _time.ToLongTimeString();

                                               labelControlTestTime.Text = time;
                                           };
                BeginInvoke(method);
            }
        }

        public void SetQuestionTime(int seconds)
        {
            int r, g;
            int koefficient = 10;

            if (seconds >= 255/koefficient)
            {
                r = 0;
                g = 255;
            }
            else
            {
                if (seconds > 127/koefficient)
                {
                    g = 255;
                    r = (255 - seconds*koefficient)*2;
                }
                else
                {
                    r = 255;
                    g = seconds*koefficient*2;
                }
            }


            DateTime _time = new DateTime().AddSeconds(seconds);
            if (ParentForm == null && InvokeRequired)
            {
                //изменение цвета

                labelControlQuestionTime.BackColor = Color.FromArgb(((((byte) (r)))), ((((byte) (g)))), ((((0)))));


                labelControlQuestionTime.Visible = true;
                labelControl2.Visible = true;
                string time = _time.ToLongTimeString();

                labelControlQuestionTime.Text = time;
            }
            else
            {
                MethodInvoker method = delegate
                                           {
                                               //изменение цвета

                                               labelControlQuestionTime.BackColor = Color.FromArgb(((((byte) (r)))),
                                                                                                   ((((byte) (g)))),
                                                                                                   ((((0)))));


                                               labelControlQuestionTime.Visible = true;
                                               labelControl2.Visible = true;
                                               string time = _time.ToLongTimeString();

                                               labelControlQuestionTime.Text = time;
                                           };
                Invoke(method);
            }
        }

        public void HideQuestionTimer()
        {
            try
            {
                if (ParentForm == null)
                {
                    labelControlQuestionTime.Visible = false;
                    labelControl2.Visible = false;
                }
                else
                {
                    MethodInvoker method = delegate
                                               {
                                                   labelControlQuestionTime.Visible = false;
                                                   labelControl2.Visible = false;
                                               };
                    Invoke(method);
                }
            }
            catch (InvalidOperationException ex)
            {
                //возникает при закрытии формы
                Log.Error(ex);
                return;
            }
        }


        private void OnSimpeButtonHideClick()
        {
            if (simpleButtonHideClick == null)
            {
                MessageForm.ShowErrorMsg("Нет подписаных на событие simpleButtonHideClick");
            }
            else
            {
                //порождаем событие нажатия на кнопку
                simpleButtonHideClick(_show);
            }
        }

        public void SetVisibleNavigationPanel(bool visible)
        {
            _show = visible;
            if (_show)
            {
                simpleButtonHide.Text = "<<";
                simpleButtonHide.ToolTip = "Скрыть панель навигации";
            }
            else
            {
                simpleButtonHide.Text = ">>";
                simpleButtonHide.ToolTip = "Показать панель навигации";
            }
            OnSimpeButtonHideClick();
        }

        #endregion

        #region обработчики событий

        private void simpleButtonHide_Click(object sender, EventArgs e)
        {
            _show = !_show;
            SetVisibleNavigationPanel(_show);
        }

        #endregion

        #region Nested type: SetTestTimeDelegate

        private delegate void SetTestTimeDelegate(int seconds);

        #endregion
    }
}
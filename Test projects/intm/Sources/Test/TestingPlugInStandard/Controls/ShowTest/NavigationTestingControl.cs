﻿using System;
using System.Collections.Generic;
using Castle.ActiveRecord;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;
using Eureca.Professional.ActiveRecords;
using Eureca.Professional.CommonClasses;
using log4net;


namespace Eureca.Professional.TestingPlugInStandard.Controls.ShowTest
{
    public delegate void NavigationEventHandler(int questionID);

    /// <summary>
    /// контрол  - панель навигации по вопросам
    /// </summary>
    public partial class NavigationTestingControl : BaseTestingControl
    {
        private static readonly ILog Log = LogManager.GetLogger( typeof( NavigationTestingControl ) );

        #region поля

        /// <summary>флаг определяет
        /// позволено ли пользователю перемещаться самостоятельно</summary>
        private readonly bool _allowUserSelectQuestion = true;

        /// <summary>
        /// текущий вопрос
        /// </summary>
        private WorkQuestion _workQuestion;

        #endregion

        #region события

        /// <summary>событие на выбор описания навигации</summary>
        public event NavigationEventHandler SelectQuestion;

        /// <summary>событие на выбор описания навигации</summary>
        public event NavigationEventHandler NeedNextQuestion;

        #endregion

        #region конструктор

        public NavigationTestingControl(bool allowUserSelectQuestion)
        {
            _allowUserSelectQuestion = allowUserSelectQuestion;
            InitializeComponent();
        }

        #endregion

        #region методы

        public override void InitControl()
        {
            base.InitControl();
            treeList1.Enabled = _allowUserSelectQuestion;
            treeList1.BestFitColumns();
        }

        public void FillControl(List<WorkQuestion> workQuestions)
        {
            treeList1.Nodes.Clear();
            int i = 0;
            foreach (WorkQuestion workQuestion in workQuestions)
            {
                TreeListNode node = treeList1.AppendNode(null, null);
                node["ID"] = workQuestion._question.Id;
                node.ImageIndex = -1;
                string res = string.Format("{0} {1}", i, "вопрос");
                node["QuestionNumber"] = res;
                node["Order"] = workQuestion._order;
                i++;
            }
        }


        /// <summary>метод установки вопроса в списке</summary>
        /// <param name="workQuestion"></param>
        /// <param name="setFocus">Установка фокуса на элемент</param>
        public void SetQuestion(WorkQuestion workQuestion, bool setFocus)
        {
            if (InvokeRequired)
            {
                try
                {
                    Invoke(new SetQuestionDelegate(SetQuestion), workQuestion, setFocus);
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                    MessageForm.ShowErrorMsg(ex.ToString());
                    return;
                }
            }
            else
                _workQuestion = workQuestion;
            foreach (TreeListNode node in treeList1.Nodes)
            {
                if (int.Parse(node["ID"].ToString()) == _workQuestion._question.Id)
                {
                    if (setFocus)
                    {
                        treeList1.SetFocusedNode(node);
                    }
                    node["QuestionNumber"] = FormatQuestion(_workQuestion);
                    node.StateImageIndex = (int) _workQuestion._state;
                    break;
                }
            }
        }

        /// <summary>Метод форматирования строку вопроса</summary>
        /// <param name="workQuestion"></param>
        /// <returns></returns>
        public static string FormatQuestion(WorkQuestion workQuestion)
        {
            string formattedString;

            string _text;
            using (new SessionScope())
            {
                var question = Question.Find(workQuestion._question.Id);
                _text = question.Content.PlainContent;
            }
            if (string.IsNullOrEmpty(_text))
            {
                formattedString = "Форматированный текст";
            }
            else
            {
                int len = _text.IndexOf("\n");
                if (len == -1)
                    len = _text.Length;
                if (len > 40)
                {
                    len = _text.LastIndexOf(" ", 40);
                }
                if (len > 40)
                {
                    len = 40;
                }

                if (len < _text.Length && len != -1)
                {
                    formattedString = _text.Substring(0, len) + " ...";
                }
                else
                {
                    formattedString = _text;
                }
                formattedString = formattedString.Replace("\r", "");
            }
            string res = string.Format("{0} {1}", workQuestion._order, formattedString);
            return res;
        }

        /// <summary>метод устанавливает состояние вопроса</summary>
        /// <param name="workQuestion"></param>
        public void SetStateWorkQuestion(WorkQuestion workQuestion)
        {
            if (InvokeRequired)
            {
                try
                {
                    Invoke(new SetStateWorkQuestionDelegate(SetStateWorkQuestion), workQuestion);
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                    MessageForm.ShowErrorMsg(ex.ToString());
                    return;
                }
            }
            else
            {
                var state = (int) workQuestion._state;

                foreach (TreeListNode node in treeList1.Nodes)
                {
                    if (int.Parse(node["ID"].ToString()) == workQuestion._question.Id)
                    {
                        node.StateImageIndex = state;
                        break;
                    }
                }
                if (_workQuestion == workQuestion && workQuestion._state == WorkQuestionState.Error)
                {
                    OnNeedNextQuestion(_workQuestion._question.Id);
                }
            }
        }


        /// <summary>Метод вызова события на установку вопроса</summary>
        /// <param name="questionID"></param>
        private void OnSelectQuestion(int questionID)
        {
            //выдаем событие на то что пользователь выбрал вопрос
            if (SelectQuestion == null)
            {
                MessageForm.ShowErrorMsg("Нет подписаных на событие SelectQuestion");
            }
            else
            {
                SelectQuestion(questionID);
            }
        }

        /// <summary>Метод вызова события на установку вопроса</summary>
        /// <param name="questionID"></param>
        private void OnNeedNextQuestion(int questionID)
        {
            //выдаем событие на то что пользователь выбрал вопрос
            //который следует сменить на другой
            if (NeedNextQuestion == null)
            {
                MessageForm.ShowErrorMsg("Нет подписаных на событие NeedNextQuestion");
            }
            else
            {
                NeedNextQuestion(questionID);
            }
        }

        #endregion

        #region обработчики событий

        private void treeList1_BeforeFocusNode(object sender, BeforeFocusNodeEventArgs e)
        {
            if (!_allowUserSelectQuestion)
            {
                return;
            }
            try
            {
                int QuestionID = int.Parse(e.Node["ID"].ToString());

                if (e.Node.StateImageIndex == (int) WorkQuestionState.Error)
                {
                    //пользователь выбрал вопрос с ошибкой
                    //MessageForm.Show("На этот вопрос не разрешено отвечать");
                    //treeList1.FocusedNode = null;
                    //OnNeedNextQuestion(QuestionID);
                    e.CanFocus = false;
                }
                else if (e.Node.StateImageIndex == (int) WorkQuestionState.Ok)
                {
                    //пользователь выбрал вопрос с ошибкой
                    //MessageForm.Show("На этот вопрос Вы уже дали ответ");
                    //treeList1.FocusedNode = null;
                    //OnNeedNextQuestion(QuestionID);
                    e.CanFocus = false;
                }
                else
                    OnSelectQuestion(QuestionID);
            }
            catch (NullReferenceException)
            {
                //MessageForm.Show("Искл");
                //это происходит когда создается первый элемент в списке и он фокусируется
                //старт же происходит при методе start в главном контроле
            }
        }

        #endregion

        #region Nested type: SetQuestionDelegate

        private delegate void SetQuestionDelegate(WorkQuestion workQuestion, bool setFocus);

        #endregion

        #region Nested type: SetStateWorkQuestionDelegate

        private delegate void SetStateWorkQuestionDelegate(WorkQuestion workQuestion);

        #endregion
    }
}
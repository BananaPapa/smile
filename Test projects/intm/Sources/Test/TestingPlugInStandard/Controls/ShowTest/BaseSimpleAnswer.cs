﻿using System;
using Eureca.Professional.ActiveRecords;
using Eureca.Professional.CommonClasses;


namespace Eureca.Professional.TestingPlugInStandard.Controls.ShowTest
{
    public delegate void SimpleAnswerEventHandler(BaseSimpleAnswer control, Answer answer);

    /// <summary>
    /// базовый класс для всех простых контролов ответов
    /// </summary>
    public partial class BaseSimpleAnswer : BaseTestingControl, ISimpleAnswer
    {
        #region Fields

        /// <summary>
        /// ответ
        /// </summary>
        public Answer _answer;

        #endregion

        public BaseSimpleAnswer()
        {
            InitializeComponent();
        }

        /// <summary>
        /// флаг прошлого состояния, для того чтобы понимать, ставить контрол выделенным или нет
        /// </summary>
        public bool LastCheckState { get; protected set; }

        #region ISimpleAnswer Members

        public virtual void SetCheckState(bool state)
        {
            throw new Exception("Этот метод должен перегружаться в дочерних контролах");
            //используется в дочерних контролах
        }

        public virtual void SetAnswer(Answer answer)
        {
            throw new Exception("Этот метод должен перегружаться в дочерних контролах");
            //используется в дочерних контролах
        }

        public virtual bool IsNeedChangeCheckState()
        {
            throw new Exception("Этот метод должен перегружаться в дочерних контролах");
            //используется в дочерних контролах
        }

        public virtual bool GetCheckState()
        {
            throw new Exception("Этот метод должен перегружаться в дочерних контролах");
            //используется в дочерних контролах
        }

        #endregion

        #region Events

        /// <summary> событие на завершенный выбор ответа</summary>
        public event SimpleAnswerEventHandler SimpleAnswerSelected;

        /// <summary> событие на преднамереный выбор ответа</summary>
        public event SimpleAnswerEventHandler SimpleAnswerSelecting;

        #endregion

        /// <summary>Событие завершенный выбор простого ответа передаем наверх</summary>
        /// <param name="answer"></param>
        protected void OnSimpleAnswerSelected(Answer answer)
        {
            if (SimpleAnswerSelected == null)
            {
                MessageForm.ShowWarningMsg("Нет подписаных на событие SimpleAnswerSelected");
            }
            else
            {
                SimpleAnswerSelected(this, answer);
            }
        }

        /// <summary>Событие на преднамереный выбор простого ответа передаем наверх</summary>
        /// <param name="answer"></param>
        protected void OnSimpleAnswerSelecting(Answer answer)
        {
            if (SimpleAnswerSelecting == null)
            {
                MessageForm.ShowWarningMsg("Нет подписаных на событие SimpleAnswerSelected");
            }
            else
            {
                SimpleAnswerSelecting(this, answer);
            }
        }
    }

    /// <summary>
    /// интерфейс для простых ответов
    /// </summary>
    internal interface ISimpleAnswer
    {
        /// <summary>
        /// метод установки ответа
        /// </summary>
        /// <param name="answer"></param>
        void SetAnswer(Answer answer);

        /// <summary>
        /// метод изменения состояния
        /// </summary>
        /// <param name="state"></param>
        void SetCheckState(bool state);

        /// <summary>
        /// метод проверки состояния ответа в контроле
        /// нужно ли его изменить
        /// </summary>
        /// <returns></returns>
        bool IsNeedChangeCheckState();

        /// <summary>
        /// метод проверки реального состояния в контроле
        /// </summary>
        /// <returns></returns>
        bool GetCheckState();
    }
}
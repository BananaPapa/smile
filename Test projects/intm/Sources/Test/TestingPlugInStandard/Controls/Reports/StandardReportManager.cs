﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using Castle.ActiveRecord;
using DevExpress.XtraCharts;
using DevExpress.XtraReports.UI;
using Eureca.Professional.ActiveRecords;
using Eureca.Professional.CommonClasses;
using Eureca.Professional.CommonClasses.Data;
using EurecaCorp.Professional.TestingPlugInStandard.Controls.Reports;
using log4net;


namespace Eureca.Professional.TestingPlugInStandard.Controls.Reports
{
    /// <summary>
    /// класс создатель отчетов
    /// </summary>
    public class StandardReportManager
    {
        private static readonly ILog Log = LogManager.GetLogger( typeof( StandardReportManager ) );
        /// <summary>
        /// создает общий отчет по тесту
        /// </summary>
        /// <param name="probationer"></param>
        /// <param name="profilePassing"></param>
        /// <param name="test"></param>
        /// <param name="testpassing"></param>
        public void GenerateCommonReport(Probationer probationer, ProfilePassing profilePassing, Test test,
                                         TestPassing testpassing)
        {
            try
            {
                var repCommonTests = new XtraReportCommonTests
                                         {
                                             labelFIO = {Text = probationer.User.FullName},
                                             labelBirthDay = {Text = probationer.User.BirthDate.ToShortDateString()},
                                             labelMethodName = {Text = test.Method.ToString()},
                                             labelProfileName = {Text = profilePassing.Profile.ToString()},
                                             labelTime =
                                                 {
                                                     Text = string.Format("{0}       с {1}  -  по {2}",
                                                                          testpassing.TimeInterval.StartTime.
                                                                              ToShortDateString(),
                                                                          testpassing.TimeInterval.StartTime.
                                                                              ToLongTimeString(),
                                                                          testpassing.TimeInterval.FinishTime.
                                                                              ToLongTimeString())
                                                 },
                                             xrLabelTestName = {Text = test.Title}
                                         };

                GetCommonTest(repCommonTests.dataSetStandardTests1, probationer, profilePassing, test);

                if (repCommonTests.dataSetStandardTests1.vTestsResultDetail.Count == 0)
                {
                    MessageForm.ShowErrorMsg("Отсутствуют данные тестирования");
                    return;
                }

                var coeff = (int) (Math.Round(repCommonTests.dataSetStandardTests1.vTestsResultDetail.Rows.Count/
                                              (decimal) 30, 0));
                if (coeff == 0) coeff = 1;

                repCommonTests.xrChart2.Size =
                    new Size(repCommonTests.xrChart2.Size.Width*coeff,
                             repCommonTests.xrChart2.Size.Height);
                repCommonTests.xrChart3.Size = repCommonTests.xrChart2.Size;


                ((XYDiagram) repCommonTests.xrChart3.Diagram).AxisY.Title.Text = ActiveRecordBase<Scale>.Find(1).Name;


                repCommonTests.CreateDocument();


                repCommonTests.ShowPreviewDialog();
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                MessageForm.ShowErrorMsg(ex.Message);
            }
        }

        /// <summary>
        /// создает подробный отчет по тесту
        /// </summary>
        /// <param name="probationer"></param>
        /// <param name="profilePassing"></param>
        /// <param name="test"></param>
        /// <param name="testpassing"></param>
        public void CreateDetailTestReport(Probationer probationer, ProfilePassing profilePassing, Test test,
                                           TestPassing testpassing)
        {
            try
            {
                var repDetailTests = new XtraReportDetailTests
                                         {
                                             labelFIO = {Text = probationer.User.FullName},
                                             labelBirthDay = {Text = probationer.User.BirthDate.ToShortDateString()},
                                             labelMethodName = {Text = test.Method.ToString()},
                                             labelProfileName = {Text = profilePassing.Profile.ToString()},
                                             labelTime =
                                                 {
                                                     Text = string.Format("{0}       с {1}  -  по {2}",
                                                                          testpassing.TimeInterval.StartTime.
                                                                              ToShortDateString(),
                                                                          testpassing.TimeInterval.StartTime.
                                                                              ToLongTimeString(),
                                                                          testpassing.TimeInterval.FinishTime.
                                                                              ToLongTimeString())
                                                 },
                                             xrLabelTestName = {Text = test.Title}
                                         };

                GetCommonTest(repDetailTests.dataSetStandardTests1, probationer, profilePassing, test);
                GetDetailTest(repDetailTests.dataSetStandardTests1, probationer, profilePassing, test);


                GetRichcontent(repDetailTests.dataSetStandardTests1);

                repDetailTests.ShowPreviewDialog();
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                MessageForm.ShowErrorMsg(ex.Message);
            }
        }

       
        /// <summary>
        /// получение выборки общего отчета по тесту
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="probationer"></param>
        /// <param name="profilePassing"></param>
        /// <param name="test"></param>
        public static int GetCommonTest(DataSetStandardTests ds, Probationer probationer, ProfilePassing profilePassing,
                                        Test test)
        {
            var adapter = new SqlDataAdapter();

            // Open the connection.
            var connection = new SqlConnection(DBExecutor.GetConnectionString());
            connection.Open();

            // SQL запрос для общих тестов
            var command = new SqlCommand(string.Format(
                "select * from vCommonTests " +
                " where userid = {0} and ProfilePassingId = {1} and TestId = {2} and ScaleId is not null"
                , probationer.User.Id, profilePassing.Id, test.Id),
                                         connection) {CommandType = CommandType.Text};
            adapter.SelectCommand = command;

            adapter.Fill(ds.vCommonTests);
            //запрос для физиологической информации(время на ответ)
            command.CommandText = string.Format(
                "select sum(Value) Value,max(TimeDuration)TimeDuration, max(QuestionOrder) QuestionOrder, " +
                "max(TimeIntervalId) TimeIntervalId " +
                "from vTestsResultDetail " +
                "where UserId = {0} and ProfilePassingId ={1}  and TestId = {2} and ( ScaleId = 1 or scaleId is null) " +
                "group by QuestionOrder " +
                "order by TimeIntervalId "
                , probationer.User.Id, profilePassing.Id, test.Id);

           int ret = adapter.Fill(ds.vTestsResultDetail);

            connection.Close();
            //возвращаем количество ответов на вопрос, если оно равно 0, то автоматически все по шкалам равно 0
            return ret;
        }


        /// <summary>
        /// получение выборки подробного отчета по тесту
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="probationer"></param>
        /// <param name="profilePassing"></param>
        /// <param name="test"></param>
        public static int GetDetailTest(DataSetStandardTests ds, Probationer probationer, ProfilePassing profilePassing,
                                        Test test)
        {
            var adapter = new SqlDataAdapter();

            // Open the connection.
            var connection = new SqlConnection(DBExecutor.GetConnectionString());
            connection.Open();

            // Create a SqlCommand to retrieve Suppliers data.
            var command = new SqlCommand(string.Format(
                "select * from vDetailTests " +
                " where userid = {0} and ProfilePassingId = {1} and TestId = {2} and Value = 0 order by TimeIntervalId"
                ,
                probationer.User.Id, profilePassing.Id, test.Id),
                                         connection) {CommandType = CommandType.Text};

            // Set the SqlDataAdapter's SelectCommand.
            adapter.SelectCommand = command;

            int ret = adapter.Fill(ds.vDetailTests);

            //Заполнение столбца с правильными ответами



            connection.Close();
            return ret;
        }


        /// <summary>
        /// метод переделывает Form из byte[] в string
        /// </summary>
        /// <param name="ds"></param>
        private static void GetRichcontent(DataSetStandardTests ds)
        {
            var rtb = new RichTextBox();
            foreach (DataRow dr in ds.vDetailTests.Rows)
            {
                if (dr[ds.vDetailTests.qPlainContentColumn] != DBNull.Value)
                {
                    dr[ds.vDetailTests.qRichTextColumn] = dr[ds.vDetailTests.qPlainContentColumn];
                }
                else
                {
                    var ms = new MemoryStream((byte[])dr[ds.vDetailTests.qRichContentColumn]);
                    rtb.LoadFile(ms, RichTextBoxStreamType.RichText);
                    dr[ds.vDetailTests.qRichTextColumn] = rtb.Text;
                }

                if (dr[ds.vDetailTests.aPlainContentColumn] != DBNull.Value)
                {
                    dr[ds.vDetailTests.aRichTextColumn] = dr[ds.vDetailTests.aPlainContentColumn];
                }
                else
                {
                    var ms = new MemoryStream((byte[])dr[ds.vDetailTests.aRichContentColumn]);
                    rtb.LoadFile(ms, RichTextBoxStreamType.RichText);
                    dr[ds.vDetailTests.aRichTextColumn] = rtb.Text;
                }
                if (dr[ds.vDetailTests.raPlainContentColumn] != DBNull.Value)
                {
                    dr[ds.vDetailTests.raRichTextColumn] = dr[ds.vDetailTests.raPlainContentColumn];
                }
                else
                {
                    var ms = new MemoryStream((byte[])dr[ds.vDetailTests.raRichContentColumn]);
                    rtb.LoadFile(ms, RichTextBoxStreamType.RichText);
                    dr[ds.vDetailTests.raRichTextColumn] = rtb.Text;
                }
            }
        }

    }
}
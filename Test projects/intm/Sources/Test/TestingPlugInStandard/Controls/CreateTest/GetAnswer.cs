﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using Castle.ActiveRecord;
using Eureca.Professional.ActiveRecords;
using Eureca.Professional.CommonClasses;

namespace Eureca.Professional.TestingPlugInStandard.Controls.CreateTest
{
    public partial class GetAnswer : BaseTestingControl
    {
        private Answer _answer;
        private List<Answer> _answers;

        public GetAnswer()
        {
            InitializeComponent();
        }

        internal Answer SelectedAnswer
        {
            get { return _answer; }
            set
            {
                _answer = value;
                if (_answer != null)
                {
                    for (int i = 0; i < listBoxControlAllAnswers.ItemCount; i++)
                    {
                        var answer = (Answer) listBoxControlAllAnswers.Items[i];
                        if (answer.Id == _answer.Id)
                        {
                            listBoxControlAllAnswers.SelectedIndex = i;
                            return;
                        }
                    }
                    listBoxControlAllAnswers.Items.Add(_answer);
                    listBoxControlAllAnswers.SelectedIndex = listBoxControlAllAnswers.ItemCount - 1;
                }
            }
        }

        internal void InitControl(List<Answer> allAnswers)
        {
            _answers = allAnswers;
            base.InitControl();
            CaptionControl = "Выбор и добавление ответа";

            if (allAnswers.Count < 1)
                RefreshAnswers();
            //SearchAnswer(null);
        }

        public void RefreshAnswers()
        {
            _answers.Clear();
            //_answers.AddRange(ActiveRecordBase<Answer>.FindAll(Expre));
        }

        //private void SearchAnswer(string text)
        //{
        //    listBoxControlAllAnswers.Items.Clear();
        //    if (string.IsNullOrEmpty(text))
        //    {
        //        foreach (Answer item in _answers)
        //        {
        //            listBoxControlAllAnswers.Items.Add(item);
        //        }
        //    }
        //    else
        //    {
        //IEnumerable<Answer> selectAnswer =
        //    from answers in _answers
        //    where answers.Form.PlainContent != null
        //    && answers.Form.PlainContent.StartsWith(text)
        //    //where answers.Text.Trim().Contains(text.Trim())
        //    select answers;
        //foreach (Answer item in selectAnswer)
        //{
        //    listBoxControlAllAnswers.Items.Add(item);
        //}
        //}
        //}


        //private void textEditSearchAnswer_KeyDown(object sender, KeyEventArgs e)
        //{

        //}

        private void listBoxControlAllAnswers_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBoxControlAllAnswers.SelectedIndex != -1)
            {
                _answer = (Answer) listBoxControlAllAnswers.SelectedItem;
                ViewBodyAnswer();
            }
        }

        private void ViewBodyAnswer()
        {
            if (_answer.Content.PlainContent == null)
            {
                exRichTextBoxSelectAnswer.Visible = true;
                answerLabel.Visible = false;
                var ms = new MemoryStream(_answer.Content.RichContent);
                exRichTextBoxSelectAnswer.LoadFile(ms, RichTextBoxStreamType.RichText);
            }
            else
            {
                exRichTextBoxSelectAnswer.Visible = false;
                answerLabel.Visible = true;
                int linesCount =
                    _answer.Content.PlainContent.Split(new[] {Environment.NewLine}, StringSplitOptions.None).Length;

                answerLabel.Font =
                    new Font("Microsoft Sans Serif",
                             25F, FontStyle.Bold,
                             GraphicsUnit.Pixel,
                             204);

                answerLabel.Padding = new Padding(10);
                answerLabel.Text = _answer.Content.PlainContent;
            }
        }


        private Answer GetNewAnswer()
        {
            if (radioGroup1.SelectedIndex == 0)
            {
                string text = exRichTextBoxNewAnswer.Text;
                if (string.IsNullOrEmpty(text.Trim()))
                {
                    MessageForm.ShowErrorMsg("Необходимо ввести ответ");
                    return null;
                }
                if (text.Length > 255)
                {
                    //длина более чем допустимая в БД
                    if (MessageForm.ShowYesNoQuestionMsg(
                        "Длина текста вопроса более чем допустимая в БД.\nСохранить текст в формате RTF?")
                        == DialogResult.Yes)
                    {
                        var ms_ = new MemoryStream();
                        exRichTextBoxNewAnswer.SaveFile(ms_, RichTextBoxStreamType.RichText);
                        var c = new Content(ms_.ToArray());
                        if (ms_.Length > Consts.ContentMaxSize)
                        {
                            MessageForm.ShowErrorMsg("Размер Вашего вопроса составляет " + ms_.Length/1024 +
                                                     " Кбайт. \nУменьшите размер текста и изображений до " +
                                                     Consts.ContentMaxSize/1024 + " Кбайт.");
                            return null;
                        }
                        c.Save();
                        var a = new Answer(c);
                        a.Save();
                        return a;
                    }
                    return null;
                }
                else
                {
                    //поиск существующего ответа
                    //foreach (Answer existAnswer in _answers)
                    //{
                    //    if (text.CompareTo(existAnswer.Form.PlainContent) ==0)
                    //    {
                    //        return existAnswer;
                    //    }
                    //}
                    var c = new Content(text);
                    c.Save();
                    var a = new Answer(c);
                    a.Save();
                    return a;
                }
            }
            else
            {
                var ms_ = new MemoryStream();
                exRichTextBoxNewAnswer.SaveFile(ms_, RichTextBoxStreamType.RichText);
                var c = new Content(ms_.ToArray());
                if (ms_.Length > Consts.ContentMaxSize)
                {
                    MessageForm.ShowErrorMsg("Размер Вашего вопроса составляет " + ms_.Length/1024 +
                                             " Кбайт. \nУменьшите размер текста и изображений до " +
                                             Consts.ContentMaxSize/1024 + " Кбайт.");
                    return null;
                }
                c.Save();
                var a = new Answer(c);
                a.Save();
                return a;
            }
        }

        private void simpleButtonAdd_Click(object sender, EventArgs e)
        {
            //using (new SessionScope())
            //{
                Answer answer = GetNewAnswer();
                if (answer == null)
                    return;
                if (!_answers.Contains(answer))
                    _answers.Add(answer);
                listBoxControlAllAnswers.SelectedIndex = listBoxControlAllAnswers.Items.Add(answer);
                SelectedAnswer = answer;
            //}
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            var ofd = new OpenFileDialog();
            ofd.Filter = "Изображения  |(*.BMP;*.JPG;*.JPEG;*.bmp;*.jpg;*.jpeg)";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                var fInfo = new FileInfo(ofd.FileName);
                int maxLength = 204800; //200 кБайт
                if (fInfo.Length > maxLength)
                {
                    MessageForm.ShowErrorMsg(
                        "Размер изображения (" + fInfo.Length/1024 +
                        " кБайт) превышает максимальное допустимое значение (" + maxLength/1024 +
                        " кБайт).\nДля добавления данной картинки уменьшите ее размер.");
                    return;
                }
                exRichTextBoxNewAnswer.InsertImage(Image.FromFile(ofd.FileName));
            }
        }

        //private void simpleButtonFind_Click(object sender, EventArgs e)
        //{

        //        SearchAnswer(exRichTextBoxNewAnswer.Text);

        //}

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            exRichTextBoxNewAnswer.Paste();
        }
    }
}
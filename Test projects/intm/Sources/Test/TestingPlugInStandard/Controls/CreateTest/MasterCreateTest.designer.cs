﻿namespace Eureca.Professional.TestingPlugInStandard.Controls.CreateTest
{
    partial class MasterCreateTest
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.CreatePlan = new DevExpress.XtraWizard.WizardControl();
            this.welcomeWizardPage1 = new DevExpress.XtraWizard.WelcomeWizardPage();
            this.wizardPageCreteTest = new DevExpress.XtraWizard.WizardPage();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.spinEditMaxErrorPercent = new DevExpress.XtraEditors.SpinEdit();
            this.spinEditSpeed = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.comboBoxEditTestType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.spinEditMaxTeacherPassing = new DevExpress.XtraEditors.SpinEdit();
            this.spinEditPassingScore = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.checkEditNoTime = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.propertyGridControl1 = new DevExpress.XtraVerticalGrid.PropertyGridControl();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.editorRowRandom = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRowMany = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRowSelectQuestion = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRowSkipQuestion = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.timeEditTestLimit = new DevExpress.XtraEditors.TimeEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.textEditTestComment = new DevExpress.XtraEditors.TextEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.textEditTestName = new DevExpress.XtraEditors.TextEdit();
            this.completionWizardPage = new DevExpress.XtraWizard.CompletionWizardPage();
            this.wizardPageScales = new DevExpress.XtraWizard.WizardPage();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.textEditScaleShortName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.textEditScaleName = new DevExpress.XtraEditors.TextEdit();
            this.simpleButtonDeleteScale = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonAddScale = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonDragOneToAll = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonDragAllToSelect = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonDragAllToAll = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonDragOneToSelect = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.listBoxControlSelectedScales = new DevExpress.XtraEditors.ListBoxControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.listBoxControlAllScales = new DevExpress.XtraEditors.ListBoxControl();
            this.wizardPageAnswers = new DevExpress.XtraWizard.WizardPage();
            this.simpleButtonChangeAnswer = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonAddIntoSelectedAnswer = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonDeleteSelectedAnswer = new DevExpress.XtraEditors.SimpleButton();
            this.listBoxControlSelectedAnswers = new DevExpress.XtraEditors.ListBoxControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.spinEdit1 = new DevExpress.XtraEditors.SpinEdit();
            this.wizardPageStens = new DevExpress.XtraWizard.WizardPage();
            this.simpleButtonSaveStens = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonCheckStens = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.spinEditStensCount = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.gridControlStens = new DevExpress.XtraGrid.GridControl();
            this.gridViewStens = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.listBoxControlSelectedScalesStens = new DevExpress.XtraEditors.ListBoxControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.wizardPageReports = new DevExpress.XtraWizard.WizardPage();
            this.simpleButtonToAllReports = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonToSelectedReportsAll = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonToAllReportsAll = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonToSelectedReports = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.listBoxControlSelectedReports = new DevExpress.XtraEditors.ListBoxControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.listBoxControlAllReports = new DevExpress.XtraEditors.ListBoxControl();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreatePlan)).BeginInit();
            this.CreatePlan.SuspendLayout();
            this.wizardPageCreteTest.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditMaxErrorPercent.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditSpeed.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditTestType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditMaxTeacherPassing.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditPassingScore.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditNoTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.propertyGridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeEditTestLimit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTestComment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTestName.Properties)).BeginInit();
            this.wizardPageScales.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditScaleShortName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditScaleName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxControlSelectedScales)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxControlAllScales)).BeginInit();
            this.wizardPageAnswers.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxControlSelectedAnswers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).BeginInit();
            this.wizardPageStens.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditStensCount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlStens)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewStens)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxControlSelectedScalesStens)).BeginInit();
            this.wizardPageReports.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxControlSelectedReports)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxControlAllReports)).BeginInit();
            this.SuspendLayout();
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Caption = "Check";
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            // 
            // CreatePlan
            // 
            this.CreatePlan.Controls.Add(this.welcomeWizardPage1);
            this.CreatePlan.Controls.Add(this.wizardPageCreteTest);
            this.CreatePlan.Controls.Add(this.completionWizardPage);
            this.CreatePlan.Controls.Add(this.wizardPageScales);
            this.CreatePlan.Controls.Add(this.wizardPageAnswers);
            this.CreatePlan.Controls.Add(this.wizardPageStens);
            this.CreatePlan.Controls.Add(this.wizardPageReports);
            this.CreatePlan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CreatePlan.Location = new System.Drawing.Point(0, 0);
            this.CreatePlan.Name = "CreatePlan";
            this.CreatePlan.Pages.AddRange(new DevExpress.XtraWizard.BaseWizardPage[] {
            this.welcomeWizardPage1,
            this.wizardPageCreteTest,
            this.wizardPageScales,
            this.wizardPageReports,
            this.wizardPageAnswers,
            this.wizardPageStens,
            this.completionWizardPage});
            this.CreatePlan.Size = new System.Drawing.Size(642, 546);
            this.CreatePlan.SelectedPageChanging += new DevExpress.XtraWizard.WizardPageChangingEventHandler(this.CreatePlan_SelectedPageChanging);
            this.CreatePlan.FinishClick += new System.ComponentModel.CancelEventHandler(this.CreatePlan_FinishClick);
            // 
            // welcomeWizardPage1
            // 
            this.welcomeWizardPage1.Enabled = true;
            this.welcomeWizardPage1.IntroductionText = "Этот мастер поможет создать (изменить) тест";
            this.welcomeWizardPage1.Name = "welcomeWizardPage1";
            this.welcomeWizardPage1.Size = new System.Drawing.Size(425, 413);
            // 
            // wizardPageCreteTest
            // 
            this.wizardPageCreteTest.Controls.Add(this.groupControl1);
            this.wizardPageCreteTest.DescriptionText = "Вам необходимо ввести основные свойства теста";
            this.wizardPageCreteTest.Enabled = true;
            this.wizardPageCreteTest.Name = "wizardPageCreteTest";
            this.wizardPageCreteTest.Size = new System.Drawing.Size(610, 401);
            this.wizardPageCreteTest.Text = "Основные свойства теста";
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.groupControl2);
            this.groupControl1.Controls.Add(this.labelControl17);
            this.groupControl1.Controls.Add(this.comboBoxEditTestType);
            this.groupControl1.Controls.Add(this.spinEditMaxTeacherPassing);
            this.groupControl1.Controls.Add(this.spinEditPassingScore);
            this.groupControl1.Controls.Add(this.labelControl16);
            this.groupControl1.Controls.Add(this.labelControl6);
            this.groupControl1.Controls.Add(this.checkEditNoTime);
            this.groupControl1.Controls.Add(this.labelControl19);
            this.groupControl1.Controls.Add(this.propertyGridControl1);
            this.groupControl1.Controls.Add(this.timeEditTestLimit);
            this.groupControl1.Controls.Add(this.labelControl10);
            this.groupControl1.Controls.Add(this.textEditTestComment);
            this.groupControl1.Controls.Add(this.labelControl9);
            this.groupControl1.Controls.Add(this.labelControl8);
            this.groupControl1.Controls.Add(this.textEditTestName);
            this.groupControl1.Location = new System.Drawing.Point(16, 13);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(573, 372);
            this.groupControl1.TabIndex = 22;
            this.groupControl1.Text = "Свойства теста";
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.spinEditMaxErrorPercent);
            this.groupControl2.Controls.Add(this.spinEditSpeed);
            this.groupControl2.Controls.Add(this.labelControl18);
            this.groupControl2.Controls.Add(this.labelControl20);
            this.groupControl2.Location = new System.Drawing.Point(5, 188);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.ShowCaption = false;
            this.groupControl2.Size = new System.Drawing.Size(558, 97);
            this.groupControl2.TabIndex = 17;
            this.groupControl2.Text = "Настройки теста";
            // 
            // spinEditMaxErrorPercent
            // 
            this.spinEditMaxErrorPercent.EditValue = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.spinEditMaxErrorPercent.Location = new System.Drawing.Point(194, 28);
            this.spinEditMaxErrorPercent.Name = "spinEditMaxErrorPercent";
            this.spinEditMaxErrorPercent.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEditMaxErrorPercent.Properties.IsFloatValue = false;
            this.spinEditMaxErrorPercent.Properties.Mask.EditMask = "N00";
            this.spinEditMaxErrorPercent.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.spinEditMaxErrorPercent.Size = new System.Drawing.Size(100, 20);
            this.spinEditMaxErrorPercent.TabIndex = 22;
            // 
            // spinEditSpeed
            // 
            this.spinEditSpeed.EditValue = new decimal(new int[] {
            1200,
            0,
            0,
            0});
            this.spinEditSpeed.Location = new System.Drawing.Point(194, 2);
            this.spinEditSpeed.Name = "spinEditSpeed";
            this.spinEditSpeed.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEditSpeed.Properties.IsFloatValue = false;
            this.spinEditSpeed.Properties.Mask.EditMask = "N00";
            this.spinEditSpeed.Properties.MaxValue = new decimal(new int[] {
            30000,
            0,
            0,
            0});
            this.spinEditSpeed.Size = new System.Drawing.Size(100, 20);
            this.spinEditSpeed.TabIndex = 21;
            // 
            // labelControl18
            // 
            this.labelControl18.Location = new System.Drawing.Point(5, 31);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(176, 13);
            this.labelControl18.TabIndex = 20;
            this.labelControl18.Text = "Маскимальное количество ошибок";
            // 
            // labelControl20
            // 
            this.labelControl20.Location = new System.Drawing.Point(5, 9);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(157, 13);
            this.labelControl20.TabIndex = 18;
            this.labelControl20.Text = "Минимальная скорость(гр/час)";
            // 
            // labelControl17
            // 
            this.labelControl17.Location = new System.Drawing.Point(5, 165);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(50, 13);
            this.labelControl17.TabIndex = 16;
            this.labelControl17.Text = "Тип теста";
            // 
            // comboBoxEditTestType
            // 
            this.comboBoxEditTestType.Location = new System.Drawing.Point(161, 162);
            this.comboBoxEditTestType.Name = "comboBoxEditTestType";
            this.comboBoxEditTestType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditTestType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEditTestType.Size = new System.Drawing.Size(402, 20);
            this.comboBoxEditTestType.TabIndex = 15;
            // 
            // spinEditMaxTeacherPassing
            // 
            this.spinEditMaxTeacherPassing.EditValue = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.spinEditMaxTeacherPassing.Location = new System.Drawing.Point(161, 106);
            this.spinEditMaxTeacherPassing.Name = "spinEditMaxTeacherPassing";
            this.spinEditMaxTeacherPassing.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEditMaxTeacherPassing.Properties.IsFloatValue = false;
            this.spinEditMaxTeacherPassing.Properties.Mask.EditMask = "N00";
            this.spinEditMaxTeacherPassing.Properties.MaxValue = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.spinEditMaxTeacherPassing.Size = new System.Drawing.Size(56, 20);
            this.spinEditMaxTeacherPassing.TabIndex = 14;
            // 
            // spinEditPassingScore
            // 
            this.spinEditPassingScore.EditValue = new decimal(new int[] {
            80,
            0,
            0,
            0});
            this.spinEditPassingScore.Location = new System.Drawing.Point(161, 77);
            this.spinEditPassingScore.Name = "spinEditPassingScore";
            this.spinEditPassingScore.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEditPassingScore.Properties.IsFloatValue = false;
            this.spinEditPassingScore.Properties.Mask.EditMask = "N00";
            this.spinEditPassingScore.Properties.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.spinEditPassingScore.Size = new System.Drawing.Size(56, 20);
            this.spinEditPassingScore.TabIndex = 13;
            // 
            // labelControl16
            // 
            this.labelControl16.Location = new System.Drawing.Point(5, 106);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(150, 13);
            this.labelControl16.TabIndex = 12;
            this.labelControl16.Text = "Макс. пробных тестирований";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(5, 77);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(101, 13);
            this.labelControl6.TabIndex = 10;
            this.labelControl6.Text = "Проходной балл, %";
            // 
            // checkEditNoTime
            // 
            this.checkEditNoTime.EditValue = true;
            this.checkEditNoTime.Location = new System.Drawing.Point(159, 48);
            this.checkEditNoTime.Name = "checkEditNoTime";
            this.checkEditNoTime.Properties.Caption = "нет ограничения";
            this.checkEditNoTime.Size = new System.Drawing.Size(119, 19);
            this.checkEditNoTime.TabIndex = 8;
            this.checkEditNoTime.CheckedChanged += new System.EventHandler(this.checkEditNoTime_CheckedChanged);
            // 
            // labelControl19
            // 
            this.labelControl19.Location = new System.Drawing.Point(5, 289);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(46, 26);
            this.labelControl19.TabIndex = 7;
            this.labelControl19.Text = "Другие\r\nсвойства";
            // 
            // propertyGridControl1
            // 
            this.propertyGridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.propertyGridControl1.DefaultEditors.AddRange(new DevExpress.XtraVerticalGrid.Rows.DefaultEditor[] {
            new DevExpress.XtraVerticalGrid.Rows.DefaultEditor(typeof(bool), this.repositoryItemCheckEdit2)});
            this.propertyGridControl1.Location = new System.Drawing.Point(115, 289);
            this.propertyGridControl1.Name = "propertyGridControl1";
            this.propertyGridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1});
            this.propertyGridControl1.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.editorRowRandom,
            this.editorRowMany,
            this.editorRowSelectQuestion,
            this.editorRowSkipQuestion});
            this.propertyGridControl1.Size = new System.Drawing.Size(448, 78);
            this.propertyGridControl1.TabIndex = 6;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // editorRowRandom
            // 
            this.editorRowRandom.Height = 17;
            this.editorRowRandom.Name = "editorRowRandom";
            this.editorRowRandom.Properties.Caption = "Перемешивать вопросы";
            this.editorRowRandom.Properties.FieldName = "Random";
            this.editorRowRandom.Properties.RowEdit = this.repositoryItemCheckEdit1;
            this.editorRowRandom.Properties.UnboundType = DevExpress.Data.UnboundColumnType.Boolean;
            // 
            // editorRowMany
            // 
            this.editorRowMany.Name = "editorRowMany";
            this.editorRowMany.Properties.Caption = "Множественный выбор ответов";
            this.editorRowMany.Properties.FieldName = "Many";
            this.editorRowMany.Properties.RowEdit = this.repositoryItemCheckEdit1;
            // 
            // editorRowSelectQuestion
            // 
            this.editorRowSelectQuestion.Name = "editorRowSelectQuestion";
            this.editorRowSelectQuestion.Properties.Caption = "Возможность выбирать вопрос";
            this.editorRowSelectQuestion.Properties.FieldName = "AllowUserSelectQuestion";
            this.editorRowSelectQuestion.Properties.RowEdit = this.repositoryItemCheckEdit1;
            // 
            // editorRowSkipQuestion
            // 
            this.editorRowSkipQuestion.Name = "editorRowSkipQuestion";
            this.editorRowSkipQuestion.Properties.Caption = "Возможность пропустить вопрос";
            this.editorRowSkipQuestion.Properties.FieldName = "AllowUserSkipQuestion";
            this.editorRowSkipQuestion.Properties.RowEdit = this.repositoryItemCheckEdit1;
            // 
            // timeEditTestLimit
            // 
            this.timeEditTestLimit.EditValue = new System.DateTime(2009, 4, 10, 0, 0, 0, 0);
            this.timeEditTestLimit.Enabled = false;
            this.timeEditTestLimit.Location = new System.Drawing.Point(284, 48);
            this.timeEditTestLimit.Name = "timeEditTestLimit";
            this.timeEditTestLimit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.timeEditTestLimit.Size = new System.Drawing.Size(100, 20);
            this.timeEditTestLimit.TabIndex = 1;
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(5, 136);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(67, 13);
            this.labelControl10.TabIndex = 5;
            this.labelControl10.Text = "Комментарий";
            // 
            // textEditTestComment
            // 
            this.textEditTestComment.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textEditTestComment.EditValue = "";
            this.textEditTestComment.Location = new System.Drawing.Point(161, 132);
            this.textEditTestComment.Name = "textEditTestComment";
            this.textEditTestComment.Properties.MaxLength = 4000;
            this.textEditTestComment.Size = new System.Drawing.Size(402, 20);
            this.textEditTestComment.TabIndex = 2;
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(5, 51);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(105, 13);
            this.labelControl9.TabIndex = 3;
            this.labelControl9.Text = "Продолжительность";
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(5, 28);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(73, 13);
            this.labelControl8.TabIndex = 1;
            this.labelControl8.Text = "Наименование";
            // 
            // textEditTestName
            // 
            this.textEditTestName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textEditTestName.EditValue = "";
            this.textEditTestName.Location = new System.Drawing.Point(115, 21);
            this.textEditTestName.Name = "textEditTestName";
            this.textEditTestName.Properties.MaxLength = 250;
            this.textEditTestName.Size = new System.Drawing.Size(448, 20);
            this.textEditTestName.TabIndex = 0;
            // 
            // completionWizardPage
            // 
            this.completionWizardPage.Enabled = true;
            this.completionWizardPage.Name = "completionWizardPage";
            this.completionWizardPage.Size = new System.Drawing.Size(425, 413);
            // 
            // wizardPageScales
            // 
            this.wizardPageScales.Controls.Add(this.labelControl13);
            this.wizardPageScales.Controls.Add(this.textEditScaleShortName);
            this.wizardPageScales.Controls.Add(this.labelControl12);
            this.wizardPageScales.Controls.Add(this.textEditScaleName);
            this.wizardPageScales.Controls.Add(this.simpleButtonDeleteScale);
            this.wizardPageScales.Controls.Add(this.simpleButtonAddScale);
            this.wizardPageScales.Controls.Add(this.simpleButtonDragOneToAll);
            this.wizardPageScales.Controls.Add(this.simpleButtonDragAllToSelect);
            this.wizardPageScales.Controls.Add(this.simpleButtonDragAllToAll);
            this.wizardPageScales.Controls.Add(this.simpleButtonDragOneToSelect);
            this.wizardPageScales.Controls.Add(this.labelControl11);
            this.wizardPageScales.Controls.Add(this.listBoxControlSelectedScales);
            this.wizardPageScales.Controls.Add(this.labelControl7);
            this.wizardPageScales.Controls.Add(this.listBoxControlAllScales);
            this.wizardPageScales.DescriptionText = "Вам необходимо выбрать шкалы тестирования";
            this.wizardPageScales.Enabled = true;
            this.wizardPageScales.Name = "wizardPageScales";
            this.wizardPageScales.Size = new System.Drawing.Size(610, 401);
            this.wizardPageScales.Text = "Шкалы тестирования";
            // 
            // labelControl13
            // 
            this.labelControl13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl13.Location = new System.Drawing.Point(13, 349);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(73, 13);
            this.labelControl13.TabIndex = 15;
            this.labelControl13.Text = "Аббревиатура";
            // 
            // textEditScaleShortName
            // 
            this.textEditScaleShortName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textEditScaleShortName.EditValue = "";
            this.textEditScaleShortName.Location = new System.Drawing.Point(101, 346);
            this.textEditScaleShortName.Name = "textEditScaleShortName";
            this.textEditScaleShortName.Size = new System.Drawing.Size(159, 20);
            this.textEditScaleShortName.TabIndex = 7;
            // 
            // labelControl12
            // 
            this.labelControl12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl12.Location = new System.Drawing.Point(13, 325);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(73, 13);
            this.labelControl12.TabIndex = 13;
            this.labelControl12.Text = "Наименование";
            // 
            // textEditScaleName
            // 
            this.textEditScaleName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textEditScaleName.Location = new System.Drawing.Point(101, 322);
            this.textEditScaleName.Name = "textEditScaleName";
            this.textEditScaleName.Size = new System.Drawing.Size(159, 20);
            this.textEditScaleName.TabIndex = 6;
            // 
            // simpleButtonDeleteScale
            // 
            this.simpleButtonDeleteScale.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.simpleButtonDeleteScale.Location = new System.Drawing.Point(101, 372);
            this.simpleButtonDeleteScale.Name = "simpleButtonDeleteScale";
            this.simpleButtonDeleteScale.Size = new System.Drawing.Size(82, 23);
            this.simpleButtonDeleteScale.TabIndex = 9;
            this.simpleButtonDeleteScale.Text = "Удалить";
            this.simpleButtonDeleteScale.Click += new System.EventHandler(this.simpleButtonDeleteScale_Click);
            // 
            // simpleButtonAddScale
            // 
            this.simpleButtonAddScale.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.simpleButtonAddScale.Location = new System.Drawing.Point(13, 372);
            this.simpleButtonAddScale.Name = "simpleButtonAddScale";
            this.simpleButtonAddScale.Size = new System.Drawing.Size(82, 23);
            this.simpleButtonAddScale.TabIndex = 8;
            this.simpleButtonAddScale.Text = "Добавить";
            this.simpleButtonAddScale.Click += new System.EventHandler(this.simpleButtonAddScale_Click);
            // 
            // simpleButtonDragOneToAll
            // 
            this.simpleButtonDragOneToAll.Location = new System.Drawing.Point(287, 85);
            this.simpleButtonDragOneToAll.Name = "simpleButtonDragOneToAll";
            this.simpleButtonDragOneToAll.Size = new System.Drawing.Size(26, 23);
            this.simpleButtonDragOneToAll.TabIndex = 3;
            this.simpleButtonDragOneToAll.Text = "<";
            this.simpleButtonDragOneToAll.Click += new System.EventHandler(this.simpleButtonDragOneToAll_Click);
            // 
            // simpleButtonDragAllToSelect
            // 
            this.simpleButtonDragAllToSelect.Location = new System.Drawing.Point(287, 137);
            this.simpleButtonDragAllToSelect.Name = "simpleButtonDragAllToSelect";
            this.simpleButtonDragAllToSelect.Size = new System.Drawing.Size(26, 23);
            this.simpleButtonDragAllToSelect.TabIndex = 4;
            this.simpleButtonDragAllToSelect.Text = ">>";
            this.simpleButtonDragAllToSelect.Click += new System.EventHandler(this.simpleButtonDragAllToSelect_Click);
            // 
            // simpleButtonDragAllToAll
            // 
            this.simpleButtonDragAllToAll.Location = new System.Drawing.Point(287, 166);
            this.simpleButtonDragAllToAll.Name = "simpleButtonDragAllToAll";
            this.simpleButtonDragAllToAll.Size = new System.Drawing.Size(26, 23);
            this.simpleButtonDragAllToAll.TabIndex = 5;
            this.simpleButtonDragAllToAll.Text = "<<";
            this.simpleButtonDragAllToAll.Click += new System.EventHandler(this.simpleButtonDragAllToAll_Click);
            // 
            // simpleButtonDragOneToSelect
            // 
            this.simpleButtonDragOneToSelect.Location = new System.Drawing.Point(287, 56);
            this.simpleButtonDragOneToSelect.Name = "simpleButtonDragOneToSelect";
            this.simpleButtonDragOneToSelect.Size = new System.Drawing.Size(26, 23);
            this.simpleButtonDragOneToSelect.TabIndex = 2;
            this.simpleButtonDragOneToSelect.Text = ">";
            this.simpleButtonDragOneToSelect.Click += new System.EventHandler(this.simpleButtonDragOneToSelect_Click);
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(338, 6);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(120, 13);
            this.labelControl11.TabIndex = 5;
            this.labelControl11.Text = "Список выбраных шкал";
            // 
            // listBoxControlSelectedScales
            // 
            this.listBoxControlSelectedScales.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listBoxControlSelectedScales.HorizontalScrollbar = true;
            this.listBoxControlSelectedScales.Location = new System.Drawing.Point(338, 25);
            this.listBoxControlSelectedScales.Name = "listBoxControlSelectedScales";
            this.listBoxControlSelectedScales.Size = new System.Drawing.Size(256, 294);
            this.listBoxControlSelectedScales.SortOrder = System.Windows.Forms.SortOrder.Ascending;
            this.listBoxControlSelectedScales.TabIndex = 1;
            this.listBoxControlSelectedScales.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listBoxControlSelectedScales_MouseDoubleClick);
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(13, 6);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(91, 13);
            this.labelControl7.TabIndex = 3;
            this.labelControl7.Text = "Список всех шкал";
            // 
            // listBoxControlAllScales
            // 
            this.listBoxControlAllScales.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.listBoxControlAllScales.HorizontalScrollbar = true;
            this.listBoxControlAllScales.Location = new System.Drawing.Point(13, 25);
            this.listBoxControlAllScales.Name = "listBoxControlAllScales";
            this.listBoxControlAllScales.Size = new System.Drawing.Size(247, 294);
            this.listBoxControlAllScales.SortOrder = System.Windows.Forms.SortOrder.Ascending;
            this.listBoxControlAllScales.TabIndex = 0;
            this.listBoxControlAllScales.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listBoxControlAllScales_MouseDoubleClick);
            // 
            // wizardPageAnswers
            // 
            this.wizardPageAnswers.Controls.Add(this.simpleButtonChangeAnswer);
            this.wizardPageAnswers.Controls.Add(this.simpleButtonAddIntoSelectedAnswer);
            this.wizardPageAnswers.Controls.Add(this.simpleButtonDeleteSelectedAnswer);
            this.wizardPageAnswers.Controls.Add(this.listBoxControlSelectedAnswers);
            this.wizardPageAnswers.Controls.Add(this.labelControl15);
            this.wizardPageAnswers.Controls.Add(this.labelControl14);
            this.wizardPageAnswers.Controls.Add(this.spinEdit1);
            this.wizardPageAnswers.DescriptionText = "Если есть ответы, которые будут присутствовать во всех вопросах теста, создайте и" +
    "х здесь";
            this.wizardPageAnswers.Enabled = true;
            this.wizardPageAnswers.Name = "wizardPageAnswers";
            this.wizardPageAnswers.Size = new System.Drawing.Size(610, 401);
            this.wizardPageAnswers.Text = "Ответы";
            // 
            // simpleButtonChangeAnswer
            // 
            this.simpleButtonChangeAnswer.Location = new System.Drawing.Point(19, 95);
            this.simpleButtonChangeAnswer.Name = "simpleButtonChangeAnswer";
            this.simpleButtonChangeAnswer.Size = new System.Drawing.Size(102, 23);
            this.simpleButtonChangeAnswer.TabIndex = 14;
            this.simpleButtonChangeAnswer.Text = "Изменить";
            this.simpleButtonChangeAnswer.Click += new System.EventHandler(this.simpleButtonChangeAnswer_Click);
            // 
            // simpleButtonAddIntoSelectedAnswer
            // 
            this.simpleButtonAddIntoSelectedAnswer.Location = new System.Drawing.Point(19, 66);
            this.simpleButtonAddIntoSelectedAnswer.Name = "simpleButtonAddIntoSelectedAnswer";
            this.simpleButtonAddIntoSelectedAnswer.Size = new System.Drawing.Size(99, 23);
            this.simpleButtonAddIntoSelectedAnswer.TabIndex = 13;
            this.simpleButtonAddIntoSelectedAnswer.Text = "Добавить";
            this.simpleButtonAddIntoSelectedAnswer.Click += new System.EventHandler(this.simpleButtonAddIntoSelectedAnswer_Click);
            // 
            // simpleButtonDeleteSelectedAnswer
            // 
            this.simpleButtonDeleteSelectedAnswer.Location = new System.Drawing.Point(19, 123);
            this.simpleButtonDeleteSelectedAnswer.Name = "simpleButtonDeleteSelectedAnswer";
            this.simpleButtonDeleteSelectedAnswer.Size = new System.Drawing.Size(102, 23);
            this.simpleButtonDeleteSelectedAnswer.TabIndex = 7;
            this.simpleButtonDeleteSelectedAnswer.Text = "Удалить";
            this.simpleButtonDeleteSelectedAnswer.Click += new System.EventHandler(this.simpleButtonDeleteSelectedAnswer_Click);
            // 
            // listBoxControlSelectedAnswers
            // 
            this.listBoxControlSelectedAnswers.HorizontalScrollbar = true;
            this.listBoxControlSelectedAnswers.Location = new System.Drawing.Point(167, 32);
            this.listBoxControlSelectedAnswers.Name = "listBoxControlSelectedAnswers";
            this.listBoxControlSelectedAnswers.Size = new System.Drawing.Size(283, 298);
            this.listBoxControlSelectedAnswers.SortOrder = System.Windows.Forms.SortOrder.Ascending;
            this.listBoxControlSelectedAnswers.TabIndex = 6;
            this.listBoxControlSelectedAnswers.SelectedIndexChanged += new System.EventHandler(this.listBoxControlSelectedAnswers_SelectedIndexChanged);
            this.listBoxControlSelectedAnswers.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ListBoxControlSelectedAnswersMouseDoubleClick);
            // 
            // labelControl15
            // 
            this.labelControl15.Location = new System.Drawing.Point(167, 13);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(139, 13);
            this.labelControl15.TabIndex = 5;
            this.labelControl15.Text = "Список созданных ответов";
            // 
            // labelControl14
            // 
            this.labelControl14.Location = new System.Drawing.Point(19, 13);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(70, 13);
            this.labelControl14.TabIndex = 4;
            this.labelControl14.Text = "Номер ответа";
            // 
            // spinEdit1
            // 
            this.spinEdit1.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEdit1.Location = new System.Drawing.Point(19, 29);
            this.spinEdit1.Name = "spinEdit1";
            this.spinEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEdit1.Properties.IsFloatValue = false;
            this.spinEdit1.Properties.Mask.EditMask = "N00";
            this.spinEdit1.Properties.MaxLength = 2;
            this.spinEdit1.Properties.MaxValue = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.spinEdit1.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEdit1.Size = new System.Drawing.Size(44, 20);
            this.spinEdit1.TabIndex = 3;
            // 
            // wizardPageStens
            // 
            this.wizardPageStens.Controls.Add(this.simpleButtonSaveStens);
            this.wizardPageStens.Controls.Add(this.simpleButtonCheckStens);
            this.wizardPageStens.Controls.Add(this.labelControl4);
            this.wizardPageStens.Controls.Add(this.spinEditStensCount);
            this.wizardPageStens.Controls.Add(this.labelControl3);
            this.wizardPageStens.Controls.Add(this.gridControlStens);
            this.wizardPageStens.Controls.Add(this.listBoxControlSelectedScalesStens);
            this.wizardPageStens.Controls.Add(this.labelControl1);
            this.wizardPageStens.DescriptionText = "На этой странице необходимо сконфигурировать оценки ";
            this.wizardPageStens.Enabled = true;
            this.wizardPageStens.Name = "wizardPageStens";
            this.wizardPageStens.Size = new System.Drawing.Size(610, 401);
            this.wizardPageStens.Text = "Конфигурирование оценок";
            // 
            // simpleButtonSaveStens
            // 
            this.simpleButtonSaveStens.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.simpleButtonSaveStens.Location = new System.Drawing.Point(267, 322);
            this.simpleButtonSaveStens.Name = "simpleButtonSaveStens";
            this.simpleButtonSaveStens.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonSaveStens.TabIndex = 11;
            this.simpleButtonSaveStens.Text = "Сохранить";
            this.simpleButtonSaveStens.Click += new System.EventHandler(this.simpleButtonSaveStens_Click);
            // 
            // simpleButtonCheckStens
            // 
            this.simpleButtonCheckStens.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.simpleButtonCheckStens.Location = new System.Drawing.Point(186, 322);
            this.simpleButtonCheckStens.Name = "simpleButtonCheckStens";
            this.simpleButtonCheckStens.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonCheckStens.TabIndex = 10;
            this.simpleButtonCheckStens.Text = "Проверить";
            this.simpleButtonCheckStens.Click += new System.EventHandler(this.simpleButtonCheckStens_Click);
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(181, 6);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(69, 13);
            this.labelControl4.TabIndex = 9;
            this.labelControl4.Text = "Число оценок";
            // 
            // spinEditStensCount
            // 
            this.spinEditStensCount.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditStensCount.Location = new System.Drawing.Point(300, 3);
            this.spinEditStensCount.Name = "spinEditStensCount";
            this.spinEditStensCount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEditStensCount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditStensCount.Properties.IsFloatValue = false;
            this.spinEditStensCount.Properties.Mask.EditMask = "N00";
            this.spinEditStensCount.Properties.MaxLength = 1;
            this.spinEditStensCount.Properties.MaxValue = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.spinEditStensCount.Size = new System.Drawing.Size(100, 20);
            this.spinEditStensCount.TabIndex = 8;
            this.spinEditStensCount.EditValueChanged += new System.EventHandler(this.spinEditStensCount_EditValueChanged);
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(181, 24);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(81, 13);
            this.labelControl3.TabIndex = 7;
            this.labelControl3.Text = "Таблица оценок";
            // 
            // gridControlStens
            // 
            this.gridControlStens.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControlStens.Location = new System.Drawing.Point(181, 43);
            this.gridControlStens.MainView = this.gridViewStens;
            this.gridControlStens.Name = "gridControlStens";
            this.gridControlStens.Size = new System.Drawing.Size(414, 273);
            this.gridControlStens.TabIndex = 6;
            this.gridControlStens.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewStens});
            this.gridControlStens.EditorKeyUp += new System.Windows.Forms.KeyEventHandler(this.gridControlStens_EditorKeyUp);
            // 
            // gridViewStens
            // 
            this.gridViewStens.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5});
            this.gridViewStens.GridControl = this.gridControlStens;
            this.gridViewStens.Name = "gridViewStens";
            this.gridViewStens.OptionsCustomization.AllowFilter = false;
            this.gridViewStens.OptionsCustomization.AllowSort = false;
            this.gridViewStens.OptionsMenu.EnableColumnMenu = false;
            this.gridViewStens.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Идентификатор";
            this.gridColumn1.FieldName = "Id";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Width = 108;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Значение оценки";
            this.gridColumn2.FieldName = "Value";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 2;
            this.gridColumn2.Width = 98;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "От(%)";
            this.gridColumn3.FieldName = "MinRawPoints";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.ToolTip = "Процент, начиная с которого дается данная оценка";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 0;
            this.gridColumn3.Width = 66;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "До(%)";
            this.gridColumn4.FieldName = "MaxRawPoints";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.ToolTip = "Процент, заканчивая которым дается данная оценка";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 1;
            this.gridColumn4.Width = 99;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Описание";
            this.gridColumn5.FieldName = "Description";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 3;
            // 
            // listBoxControlSelectedScalesStens
            // 
            this.listBoxControlSelectedScalesStens.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.listBoxControlSelectedScalesStens.HorizontalScrollbar = true;
            this.listBoxControlSelectedScalesStens.Location = new System.Drawing.Point(15, 22);
            this.listBoxControlSelectedScalesStens.Name = "listBoxControlSelectedScalesStens";
            this.listBoxControlSelectedScalesStens.Size = new System.Drawing.Size(153, 319);
            this.listBoxControlSelectedScalesStens.SortOrder = System.Windows.Forms.SortOrder.Ascending;
            this.listBoxControlSelectedScalesStens.TabIndex = 5;
            this.listBoxControlSelectedScalesStens.SelectedIndexChanged += new System.EventHandler(this.listBoxControlSelectedScalesStens_SelectedIndexChanged);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(15, 6);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(65, 13);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "Список шкал";
            // 
            // wizardPageReports
            // 
            this.wizardPageReports.Controls.Add(this.simpleButtonToAllReports);
            this.wizardPageReports.Controls.Add(this.simpleButtonToSelectedReportsAll);
            this.wizardPageReports.Controls.Add(this.simpleButtonToAllReportsAll);
            this.wizardPageReports.Controls.Add(this.simpleButtonToSelectedReports);
            this.wizardPageReports.Controls.Add(this.labelControl2);
            this.wizardPageReports.Controls.Add(this.listBoxControlSelectedReports);
            this.wizardPageReports.Controls.Add(this.labelControl5);
            this.wizardPageReports.Controls.Add(this.listBoxControlAllReports);
            this.wizardPageReports.DescriptionText = "Страница отчетов. Выберите отчеты, которые будут генерироваться по данному тесту";
            this.wizardPageReports.Enabled = true;
            this.wizardPageReports.Name = "wizardPageReports";
            this.wizardPageReports.Size = new System.Drawing.Size(610, 401);
            this.wizardPageReports.Text = "Отчеты";
            // 
            // simpleButtonToAllReports
            // 
            this.simpleButtonToAllReports.Location = new System.Drawing.Point(272, 103);
            this.simpleButtonToAllReports.Name = "simpleButtonToAllReports";
            this.simpleButtonToAllReports.Size = new System.Drawing.Size(26, 23);
            this.simpleButtonToAllReports.TabIndex = 17;
            this.simpleButtonToAllReports.Text = "<";
            this.simpleButtonToAllReports.Click += new System.EventHandler(this.SimpleButtonToAllReportsClick);
            // 
            // simpleButtonToSelectedReportsAll
            // 
            this.simpleButtonToSelectedReportsAll.Location = new System.Drawing.Point(272, 155);
            this.simpleButtonToSelectedReportsAll.Name = "simpleButtonToSelectedReportsAll";
            this.simpleButtonToSelectedReportsAll.Size = new System.Drawing.Size(26, 23);
            this.simpleButtonToSelectedReportsAll.TabIndex = 16;
            this.simpleButtonToSelectedReportsAll.Text = ">>";
            this.simpleButtonToSelectedReportsAll.Click += new System.EventHandler(this.SimpleButtonToSelectedReportsAllClick);
            // 
            // simpleButtonToAllReportsAll
            // 
            this.simpleButtonToAllReportsAll.Location = new System.Drawing.Point(272, 184);
            this.simpleButtonToAllReportsAll.Name = "simpleButtonToAllReportsAll";
            this.simpleButtonToAllReportsAll.Size = new System.Drawing.Size(26, 23);
            this.simpleButtonToAllReportsAll.TabIndex = 15;
            this.simpleButtonToAllReportsAll.Text = "<<";
            this.simpleButtonToAllReportsAll.Click += new System.EventHandler(this.SimpleButtonToAllReportsAllClick);
            // 
            // simpleButtonToSelectedReports
            // 
            this.simpleButtonToSelectedReports.Location = new System.Drawing.Point(272, 74);
            this.simpleButtonToSelectedReports.Name = "simpleButtonToSelectedReports";
            this.simpleButtonToSelectedReports.Size = new System.Drawing.Size(26, 23);
            this.simpleButtonToSelectedReports.TabIndex = 14;
            this.simpleButtonToSelectedReports.Text = ">";
            this.simpleButtonToSelectedReports.Click += new System.EventHandler(this.SimpleButtonToSelectedReportsClick);
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(304, 24);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(136, 13);
            this.labelControl2.TabIndex = 13;
            this.labelControl2.Text = "Список выбраных отчетов";
            // 
            // listBoxControlSelectedReports
            // 
            this.listBoxControlSelectedReports.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.listBoxControlSelectedReports.HorizontalScrollbar = true;
            this.listBoxControlSelectedReports.Location = new System.Drawing.Point(304, 43);
            this.listBoxControlSelectedReports.Name = "listBoxControlSelectedReports";
            this.listBoxControlSelectedReports.Size = new System.Drawing.Size(256, 248);
            this.listBoxControlSelectedReports.SortOrder = System.Windows.Forms.SortOrder.Ascending;
            this.listBoxControlSelectedReports.TabIndex = 12;
            this.listBoxControlSelectedReports.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ListBoxControlSelectedReportsMouseDoubleClick);
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(17, 24);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(107, 13);
            this.labelControl5.TabIndex = 11;
            this.labelControl5.Text = "Список всех отчетов";
            // 
            // listBoxControlAllReports
            // 
            this.listBoxControlAllReports.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.listBoxControlAllReports.HorizontalScrollbar = true;
            this.listBoxControlAllReports.Location = new System.Drawing.Point(17, 43);
            this.listBoxControlAllReports.Name = "listBoxControlAllReports";
            this.listBoxControlAllReports.Size = new System.Drawing.Size(247, 248);
            this.listBoxControlAllReports.SortOrder = System.Windows.Forms.SortOrder.Ascending;
            this.listBoxControlAllReports.TabIndex = 10;
            this.listBoxControlAllReports.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ListBoxControlAllReportsMouseDoubleClick);
            // 
            // MasterCreateTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.CreatePlan);
            this.MinimumSize = new System.Drawing.Size(642, 546);
            this.Name = "MasterCreateTest";
            this.Size = new System.Drawing.Size(642, 546);
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreatePlan)).EndInit();
            this.CreatePlan.ResumeLayout(false);
            this.wizardPageCreteTest.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditMaxErrorPercent.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditSpeed.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditTestType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditMaxTeacherPassing.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditPassingScore.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditNoTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.propertyGridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeEditTestLimit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTestComment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTestName.Properties)).EndInit();
            this.wizardPageScales.ResumeLayout(false);
            this.wizardPageScales.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditScaleShortName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditScaleName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxControlSelectedScales)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxControlAllScales)).EndInit();
            this.wizardPageAnswers.ResumeLayout(false);
            this.wizardPageAnswers.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxControlSelectedAnswers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).EndInit();
            this.wizardPageStens.ResumeLayout(false);
            this.wizardPageStens.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditStensCount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlStens)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewStens)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxControlSelectedScalesStens)).EndInit();
            this.wizardPageReports.ResumeLayout(false);
            this.wizardPageReports.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxControlSelectedReports)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxControlAllReports)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraWizard.WizardControl CreatePlan;
        private DevExpress.XtraWizard.WelcomeWizardPage welcomeWizardPage1;
        private DevExpress.XtraWizard.WizardPage wizardPageCreteTest;
        private DevExpress.XtraWizard.CompletionWizardPage completionWizardPage;
        private DevExpress.XtraWizard.WizardPage wizardPageScales;
        private DevExpress.XtraWizard.WizardPage wizardPageAnswers;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.TextEdit textEditTestName;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.TextEdit textEditTestComment;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.ListBoxControl listBoxControlAllScales;
        private DevExpress.XtraEditors.SimpleButton simpleButtonDragOneToAll;
        private DevExpress.XtraEditors.SimpleButton simpleButtonDragAllToSelect;
        private DevExpress.XtraEditors.SimpleButton simpleButtonDragAllToAll;
        private DevExpress.XtraEditors.SimpleButton simpleButtonDragOneToSelect;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.ListBoxControl listBoxControlSelectedScales;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.TextEdit textEditScaleShortName;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.TextEdit textEditScaleName;
        private DevExpress.XtraEditors.SimpleButton simpleButtonDeleteScale;
        private DevExpress.XtraEditors.SimpleButton simpleButtonAddScale;
        private DevExpress.XtraEditors.TimeEdit timeEditTestLimit;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.SpinEdit spinEdit1;
        private DevExpress.XtraEditors.SimpleButton simpleButtonDeleteSelectedAnswer;
        private DevExpress.XtraEditors.ListBoxControl listBoxControlSelectedAnswers;
        private DevExpress.XtraEditors.SimpleButton simpleButtonAddIntoSelectedAnswer;
        private DevExpress.XtraWizard.WizardPage wizardPageStens;
        private DevExpress.XtraEditors.ListBoxControl listBoxControlSelectedScalesStens;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraGrid.GridControl gridControlStens;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewStens;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.SimpleButton simpleButtonSaveStens;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCheckStens;
        private DevExpress.XtraEditors.SpinEdit spinEditStensCount;
        private DevExpress.XtraWizard.WizardPage wizardPageReports;
        private DevExpress.XtraEditors.SimpleButton simpleButtonToAllReports;
        private DevExpress.XtraEditors.SimpleButton simpleButtonToSelectedReportsAll;
        private DevExpress.XtraEditors.SimpleButton simpleButtonToAllReportsAll;
        private DevExpress.XtraEditors.SimpleButton simpleButtonToSelectedReports;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.ListBoxControl listBoxControlSelectedReports;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.ListBoxControl listBoxControlAllReports;
        private DevExpress.XtraEditors.SimpleButton simpleButtonChangeAnswer;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraVerticalGrid.PropertyGridControl propertyGridControl1;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.CheckEdit checkEditNoTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRowRandom;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRowMany;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRowSelectQuestion;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRowSkipQuestion;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.SpinEdit spinEditMaxTeacherPassing;
        private DevExpress.XtraEditors.SpinEdit spinEditPassingScore;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditTestType;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.SpinEdit spinEditMaxErrorPercent;
        private DevExpress.XtraEditors.SpinEdit spinEditSpeed;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.LabelControl labelControl20;
    }
}
﻿namespace Eureca.Professional.TestingPlugInStandard.Controls.CreateTest
{
    partial class CreateMethod
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.simpleButtonInstruction = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonChangeControlQuestions = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonOk = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(10, 10);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(73, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Наименование";
            // 
            // textEdit1
            // 
            this.textEdit1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textEdit1.Location = new System.Drawing.Point(10, 29);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Properties.MaxLength = 250;
            this.textEdit1.Size = new System.Drawing.Size(230, 20);
            this.textEdit1.TabIndex = 0;
            // 
            // simpleButtonInstruction
            // 
            this.simpleButtonInstruction.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonInstruction.Location = new System.Drawing.Point(10, 55);
            this.simpleButtonInstruction.Name = "simpleButtonInstruction";
            this.simpleButtonInstruction.Size = new System.Drawing.Size(230, 23);
            this.simpleButtonInstruction.TabIndex = 1;
            this.simpleButtonInstruction.Text = "Редактировать описание";
            this.simpleButtonInstruction.Click += new System.EventHandler(this.simpleButtonInstruction_Click);
            // 
            // simpleButtonChangeControlQuestions
            // 
            this.simpleButtonChangeControlQuestions.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonChangeControlQuestions.Location = new System.Drawing.Point(10, 84);
            this.simpleButtonChangeControlQuestions.Name = "simpleButtonChangeControlQuestions";
            this.simpleButtonChangeControlQuestions.Size = new System.Drawing.Size(230, 23);
            this.simpleButtonChangeControlQuestions.TabIndex = 2;
            this.simpleButtonChangeControlQuestions.Text = "Редактировать вопросы";
            this.simpleButtonChangeControlQuestions.Click += new System.EventHandler(this.simpleButtonChangeControlQuestions_Click);
            // 
            // simpleButtonOk
            // 
            this.simpleButtonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonOk.Location = new System.Drawing.Point(84, 115);
            this.simpleButtonOk.Name = "simpleButtonOk";
            this.simpleButtonOk.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonOk.TabIndex = 3;
            this.simpleButtonOk.Text = "Сохранить";
            this.simpleButtonOk.Click += new System.EventHandler(this.simpleButtonOk_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButton1.Location = new System.Drawing.Point(165, 115);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 4;
            this.simpleButton1.Text = "Закрыть";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // CreateMethod
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.simpleButtonOk);
            this.Controls.Add(this.simpleButtonChangeControlQuestions);
            this.Controls.Add(this.simpleButtonInstruction);
            this.Controls.Add(this.textEdit1);
            this.Controls.Add(this.labelControl1);
            this.Name = "CreateMethod";
            this.Size = new System.Drawing.Size(250, 150);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraEditors.SimpleButton simpleButtonInstruction;
        private DevExpress.XtraEditors.SimpleButton simpleButtonChangeControlQuestions;
        private DevExpress.XtraEditors.SimpleButton simpleButtonOk;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
    }
}
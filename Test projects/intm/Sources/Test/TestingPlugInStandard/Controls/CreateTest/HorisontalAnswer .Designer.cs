﻿using Eureca.Professional.TestingPlugInStandard.Components;

namespace Eureca.Professional.TestingPlugInStandard.Controls.CreateTest
{
    partial class HorisontalAnswer
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.exRichTextBoxAnswer = new Eureca.Professional.TestingPlugInStandard.Components.ExRichTextBox();
            this.AnswerLabel = new System.Windows.Forms.Label();
            this.checkEditRightAnswer = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditRightAnswer.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(3, 3);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(11, 24);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "1";
            this.labelControl1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureEditAnswer_MouseClick);
            this.labelControl1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.radioGroup1_MouseDoubleClick);
            // 
            // exRichTextBoxAnswer
            // 
            this.exRichTextBoxAnswer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.exRichTextBoxAnswer.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.exRichTextBoxAnswer.HiglightColor = Eureca.Professional.TestingPlugInStandard.Components.RtfColor.White;
            this.exRichTextBoxAnswer.Location = new System.Drawing.Point(24, 3);
            this.exRichTextBoxAnswer.Name = "exRichTextBoxAnswer";
            this.exRichTextBoxAnswer.ReadOnly = true;
            this.exRichTextBoxAnswer.Size = new System.Drawing.Size(463, 60);
            this.exRichTextBoxAnswer.TabIndex = 5;
            this.exRichTextBoxAnswer.Text = "";
            this.exRichTextBoxAnswer.TextColor = Eureca.Professional.TestingPlugInStandard.Components.RtfColor.Black;
            this.exRichTextBoxAnswer.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureEditAnswer_MouseClick);
            this.exRichTextBoxAnswer.TextChanged += new System.EventHandler(this.exRichTextBoxAnswer_TextChanged);
            this.exRichTextBoxAnswer.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.radioGroup1_MouseDoubleClick);
            // 
            // AnswerLabel
            // 
            this.AnswerLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AnswerLabel.Location = new System.Drawing.Point(24, 3);
            this.AnswerLabel.Name = "AnswerLabel";
            this.AnswerLabel.Size = new System.Drawing.Size(463, 60);
            this.AnswerLabel.TabIndex = 6;
            this.AnswerLabel.Visible = false;
            this.AnswerLabel.Click += new System.EventHandler(this.AnswerLabel_Click);
            this.AnswerLabel.DoubleClick += new System.EventHandler(this.radioGroup1_MouseDoubleClick);
            // 
            // checkEditRightAnswer
            // 
            this.checkEditRightAnswer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEditRightAnswer.Location = new System.Drawing.Point(513, 3);
            this.checkEditRightAnswer.Name = "checkEditRightAnswer";
            this.checkEditRightAnswer.Properties.Caption = "Правильный";
            this.checkEditRightAnswer.Size = new System.Drawing.Size(93, 18);
            this.checkEditRightAnswer.TabIndex = 7;
            this.checkEditRightAnswer.CheckedChanged += new System.EventHandler(this.checkEditRightAnswer_CheckedChanged);
            // 
            // HorisontalAnswer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Controls.Add(this.checkEditRightAnswer);
            this.Controls.Add(this.AnswerLabel);
            this.Controls.Add(this.exRichTextBoxAnswer);
            this.Controls.Add(this.labelControl1);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "HorisontalAnswer";
            this.Size = new System.Drawing.Size(609, 72);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureEditAnswer_MouseClick);
            this.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.radioGroup1_MouseDoubleClick);
            ((System.ComponentModel.ISupportInitialize)(this.checkEditRightAnswer.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private Components.ExRichTextBox exRichTextBoxAnswer;
        private System.Windows.Forms.Label AnswerLabel;
        private DevExpress.XtraEditors.CheckEdit checkEditRightAnswer;
    }
}
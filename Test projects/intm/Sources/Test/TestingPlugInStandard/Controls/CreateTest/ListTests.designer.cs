﻿namespace Eureca.Professional.TestingPlugInStandard.Controls.CreateTest
{
    partial class ListTests
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBoxControlTests = new DevExpress.XtraEditors.ListBoxControl();
            this.simpleButtonDelete = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonChangeTest = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonAddTest = new DevExpress.XtraEditors.SimpleButton();
            this.listBoxControlMethods = new DevExpress.XtraEditors.ListBoxControl();
            this.simpleButtonAddMethod = new DevExpress.XtraEditors.SimpleButton();
            this.simpeButtonChangeMethod = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonDeleteMethod = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.simpleExport = new DevExpress.XtraEditors.SimpleButton();
            this.simpleImport = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.simpleButtonMethodControlQuestion = new DevExpress.XtraEditors.SimpleButton();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxControlTests)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxControlMethods)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.SuspendLayout();
            // 
            // listBoxControlTests
            // 
            this.listBoxControlTests.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxControlTests.HorizontalScrollbar = true;
            this.listBoxControlTests.Location = new System.Drawing.Point(0, 0);
            this.listBoxControlTests.Name = "listBoxControlTests";
            this.listBoxControlTests.Size = new System.Drawing.Size(336, 393);
            this.listBoxControlTests.SortOrder = System.Windows.Forms.SortOrder.Ascending;
            this.listBoxControlTests.TabIndex = 1;
            this.listBoxControlTests.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listBoxControlTests_MouseDoubleClick);
            // 
            // simpleButtonDelete
            // 
            this.simpleButtonDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.simpleButtonDelete.Location = new System.Drawing.Point(246, 24);
            this.simpleButtonDelete.Name = "simpleButtonDelete";
            this.simpleButtonDelete.Size = new System.Drawing.Size(78, 23);
            this.simpleButtonDelete.TabIndex = 9;
            this.simpleButtonDelete.Text = "Удалить";
            this.simpleButtonDelete.Click += new System.EventHandler(this.simpleButtonDeleteTest_Click);
            // 
            // simpleButtonChangeTest
            // 
            this.simpleButtonChangeTest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.simpleButtonChangeTest.Location = new System.Drawing.Point(162, 24);
            this.simpleButtonChangeTest.Name = "simpleButtonChangeTest";
            this.simpleButtonChangeTest.Size = new System.Drawing.Size(78, 23);
            this.simpleButtonChangeTest.TabIndex = 8;
            this.simpleButtonChangeTest.Text = "Изменить";
            this.simpleButtonChangeTest.Click += new System.EventHandler(this.simpleButtonChangeTest_Click);
            // 
            // simpleButtonAddTest
            // 
            this.simpleButtonAddTest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.simpleButtonAddTest.Location = new System.Drawing.Point(78, 24);
            this.simpleButtonAddTest.Name = "simpleButtonAddTest";
            this.simpleButtonAddTest.Size = new System.Drawing.Size(78, 23);
            this.simpleButtonAddTest.TabIndex = 7;
            this.simpleButtonAddTest.Text = "Добавить";
            this.simpleButtonAddTest.Click += new System.EventHandler(this.simpleButtonAdd_Click);
            // 
            // listBoxControlMethods
            // 
            this.listBoxControlMethods.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxControlMethods.HorizontalScrollbar = true;
            this.listBoxControlMethods.Location = new System.Drawing.Point(0, 0);
            this.listBoxControlMethods.Name = "listBoxControlMethods";
            this.listBoxControlMethods.Size = new System.Drawing.Size(334, 393);
            this.listBoxControlMethods.SortOrder = System.Windows.Forms.SortOrder.Ascending;
            this.listBoxControlMethods.TabIndex = 0;
            this.listBoxControlMethods.SelectedIndexChanged += new System.EventHandler(this.listBoxControlMethods_SelectedIndexChanged);
            this.listBoxControlMethods.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listBoxControlMethods_MouseDoubleClick);
            // 
            // simpleButtonAddMethod
            // 
            this.simpleButtonAddMethod.Location = new System.Drawing.Point(77, 24);
            this.simpleButtonAddMethod.Name = "simpleButtonAddMethod";
            this.simpleButtonAddMethod.Size = new System.Drawing.Size(78, 23);
            this.simpleButtonAddMethod.TabIndex = 3;
            this.simpleButtonAddMethod.Text = "Добавить";
            this.simpleButtonAddMethod.Click += new System.EventHandler(this.simpleButtonAddMethod_Click);
            // 
            // simpeButtonChangeMethod
            // 
            this.simpeButtonChangeMethod.Location = new System.Drawing.Point(161, 24);
            this.simpeButtonChangeMethod.Name = "simpeButtonChangeMethod";
            this.simpeButtonChangeMethod.Size = new System.Drawing.Size(78, 23);
            this.simpeButtonChangeMethod.TabIndex = 4;
            this.simpeButtonChangeMethod.Text = "Изменить";
            this.simpeButtonChangeMethod.Click += new System.EventHandler(this.simpeButtonChangeMethod_Click);
            // 
            // simpleButtonDeleteMethod
            // 
            this.simpleButtonDeleteMethod.Location = new System.Drawing.Point(245, 24);
            this.simpleButtonDeleteMethod.Name = "simpleButtonDeleteMethod";
            this.simpleButtonDeleteMethod.Size = new System.Drawing.Size(78, 23);
            this.simpleButtonDeleteMethod.TabIndex = 5;
            this.simpleButtonDeleteMethod.Text = "Удалить";
            this.simpleButtonDeleteMethod.Click += new System.EventHandler(this.simpleButtonDeleteMethod_Click);
            // 
            // simpleButton4
            // 
            this.simpleButton4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.simpleButton4.Location = new System.Drawing.Point(10, 24);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(62, 23);
            this.simpleButton4.TabIndex = 6;
            this.simpleButton4.Text = "Вопросы";
            this.simpleButton4.ToolTip = "Переход к редактированию вопросов";
            this.simpleButton4.Click += new System.EventHandler(this.simpleButton4_Click);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.simpleExport);
            this.groupControl1.Controls.Add(this.simpleImport);
            this.groupControl1.Controls.Add(this.simpleButtonAddTest);
            this.groupControl1.Controls.Add(this.simpleButton4);
            this.groupControl1.Controls.Add(this.simpleButtonDelete);
            this.groupControl1.Controls.Add(this.simpleButtonChangeTest);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupControl1.Location = new System.Drawing.Point(0, 393);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(336, 83);
            this.groupControl1.TabIndex = 12;
            this.groupControl1.Text = "Управление тестами";
            // 
            // simpleExport
            // 
            this.simpleExport.Location = new System.Drawing.Point(162, 53);
            this.simpleExport.Name = "simpleExport";
            this.simpleExport.Size = new System.Drawing.Size(162, 23);
            this.simpleExport.TabIndex = 11;
            this.simpleExport.Text = "Экспорт";
            this.simpleExport.ToolTip = "Экспорт теста в файл";
            this.simpleExport.Click += new System.EventHandler(this.simpleExport_Click);
            // 
            // simpleImport
            // 
            this.simpleImport.Location = new System.Drawing.Point(10, 53);
            this.simpleImport.Name = "simpleImport";
            this.simpleImport.Size = new System.Drawing.Size(146, 23);
            this.simpleImport.TabIndex = 10;
            this.simpleImport.Text = "Импорт";
            this.simpleImport.ToolTip = "Импортирование тестов из файла";
            this.simpleImport.Click += new System.EventHandler(this.simpleImport_Click);
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.simpleButtonMethodControlQuestion);
            this.groupControl2.Controls.Add(this.simpleButtonAddMethod);
            this.groupControl2.Controls.Add(this.simpleButtonDeleteMethod);
            this.groupControl2.Controls.Add(this.simpeButtonChangeMethod);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupControl2.Location = new System.Drawing.Point(0, 393);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(334, 83);
            this.groupControl2.TabIndex = 13;
            this.groupControl2.Text = "Управление тематиками";
            // 
            // simpleButtonMethodControlQuestion
            // 
            this.simpleButtonMethodControlQuestion.Location = new System.Drawing.Point(9, 24);
            this.simpleButtonMethodControlQuestion.Name = "simpleButtonMethodControlQuestion";
            this.simpleButtonMethodControlQuestion.Size = new System.Drawing.Size(62, 23);
            this.simpleButtonMethodControlQuestion.TabIndex = 2;
            this.simpleButtonMethodControlQuestion.Text = "Вопросы";
            this.simpleButtonMethodControlQuestion.ToolTip = "Переход к редактированию вопросов";
            this.simpleButtonMethodControlQuestion.Click += new System.EventHandler(this.simpleButtonMethodControlQuestion_Click);
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.splitContainerControl1.Panel1.Controls.Add(this.listBoxControlMethods);
            this.splitContainerControl1.Panel1.Controls.Add(this.groupControl2);
            this.splitContainerControl1.Panel1.Location = new System.Drawing.Point(2, 21);
            this.splitContainerControl1.Panel1.MinSize = 332;
            this.splitContainerControl1.Panel1.Name = "";
            this.splitContainerControl1.Panel1.ShowCaption = true;
            this.splitContainerControl1.Panel1.Size = new System.Drawing.Size(334, 476);
            this.splitContainerControl1.Panel1.TabIndex = 0;
            this.splitContainerControl1.Panel1.Text = "Список тематик";
            this.splitContainerControl1.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.splitContainerControl1.Panel2.Controls.Add(this.listBoxControlTests);
            this.splitContainerControl1.Panel2.Controls.Add(this.groupControl1);
            this.splitContainerControl1.Panel2.Location = new System.Drawing.Point(345, 21);
            this.splitContainerControl1.Panel2.MinSize = 332;
            this.splitContainerControl1.Panel2.Name = "";
            this.splitContainerControl1.Panel2.ShowCaption = true;
            this.splitContainerControl1.Panel2.Size = new System.Drawing.Size(336, 476);
            this.splitContainerControl1.Panel2.TabIndex = 1;
            this.splitContainerControl1.Panel2.Text = "Список тестов";
            this.splitContainerControl1.Size = new System.Drawing.Size(683, 499);
            this.splitContainerControl1.SplitterPosition = 338;
            this.splitContainerControl1.TabIndex = 14;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // ListTests
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainerControl1);
            this.MinimumSize = new System.Drawing.Size(676, 496);
            this.Name = "ListTests";
            this.Size = new System.Drawing.Size(683, 499);
            ((System.ComponentModel.ISupportInitialize)(this.listBoxControlTests)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxControlMethods)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.ListBoxControl listBoxControlTests;
        private DevExpress.XtraEditors.SimpleButton simpleButtonDelete;
        private DevExpress.XtraEditors.SimpleButton simpleButtonChangeTest;
        private DevExpress.XtraEditors.SimpleButton simpleButtonAddTest;
        private DevExpress.XtraEditors.ListBoxControl listBoxControlMethods;
        private DevExpress.XtraEditors.SimpleButton simpleButtonAddMethod;
        private DevExpress.XtraEditors.SimpleButton simpeButtonChangeMethod;
        private DevExpress.XtraEditors.SimpleButton simpleButtonDeleteMethod;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.SimpleButton simpleButtonMethodControlQuestion;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.SimpleButton simpleExport;
        private DevExpress.XtraEditors.SimpleButton simpleImport;
    }
}
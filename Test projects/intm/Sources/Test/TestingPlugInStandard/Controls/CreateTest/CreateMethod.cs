﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using Castle.ActiveRecord;
using Eureca.Professional.ActiveRecords;
using Eureca.Professional.CommonClasses;


namespace Eureca.Professional.TestingPlugInStandard.Controls.CreateTest
{
    internal delegate void CreateMethodEventHandler(Method method);

    internal partial class CreateMethod : BaseTestingControl
    {
        private readonly List<Method> _methods;
        private Method _method;

        internal CreateMethod()
        {
            InitializeComponent();
        }

        internal CreateMethod(Method method, List<Method> methods)
            : this()
        {
            _method = method;
            _methods = methods;
        }

        internal event CreateMethodEventHandler AddMethod;
        internal event CreateMethodEventHandler ChangeMethod;

        public override void InitControl()
        {
            base.InitControl();
            if (_method == null)
            {
                CaptionControl = "Создание тематики";
            }
            else
            {
                CaptionControl = "Редактирование тематикии";
            }
            InsertData();
        }

        private void InsertData()
        {
            if (_method != null)
            {
                //_method = Method.GetById(_method.Id);
                textEdit1.Text = _method.Title;
            }
        }

        private bool Examine()
        {
            if (string.IsNullOrEmpty(textEdit1.Text.Trim()))
            {
                MessageForm.ShowWarningMsg("Наименование тематики не может быть пустым");
                return false;
            }
            else
            {
                foreach (Method m in _methods)
                {
                    if (_method == null && m.Title.CompareTo(textEdit1.Text.Trim()) == 0)
                    {
                        MessageForm.ShowWarningMsg("Данная тематика уже существует");
                        return false;
                    }
                    if (_method != null && !_method.Equals(m) && m.Title.CompareTo(textEdit1.Text.Trim()) == 0)
                    {
                        MessageForm.ShowWarningMsg("Данная тематика уже существует");
                        return false;
                    }
                }
            }
            return true;
        }


        private bool SaveMethod()
        {
            if (!Examine())
            {
                return false;
            }
            if (_method == null)
            {
                _method = new Method(textEdit1.Text, new Content(""));
                _method.CreateAndFlush();
                AddMethod(_method);
            }
            else
            {
                _method.Title = textEdit1.Text;
                _method.UpdateAndFlush();
                //ResetSessionScope()();
                ChangeMethod(_method);
            }
            return true;
        }

        private void simpleButtonInstruction_Click(object sender, EventArgs e)
        {
            if (_method == null)
            {
                if (!Examine())
                {
                    return;
                }
                if (MessageForm.ShowYesNoQuestionMsg(
                    "Для добавления описания необходимо создать тематику.\n Создать тематику в БД?")
                    == DialogResult.No)
                {
                    return;
                }
                _method = new Method(textEdit1.Text, new Content(""));
                _method.CreateAndFlush();
                AddMethod(_method);
            }

            var instrSettingsForm = new InstructionsSettingsForm();
            instrSettingsForm.Method = _method;
            using (new SessionScope(FlushAction.Never))
            {
                _method = Method.Find(_method.Id);
                if (_method.Instruction.PlainContent == null)
                {
                    var ms = new MemoryStream(_method.Instruction.RichContent);
                    instrSettingsForm._instructionsTextBox.LoadFile(ms, RichTextBoxStreamType.RichText);
                }
                else
                    instrSettingsForm._instructionsTextBox.Text = _method.Instruction.PlainContent;
            }
            instrSettingsForm.LeftButtonText = "Сохранить";
            instrSettingsForm.InitControl();
            if (instrSettingsForm.ShowInForm() == DialogResult.OK)
            {
                //cохранение тематики
            }
        }

        private void simpleButtonOk_Click(object sender, EventArgs e)
        {
            SaveMethod( );
        //ParentForm.Close();
        }

        private void simpleButtonChangeControlQuestions_Click(object sender, EventArgs e)
        {
            if (_method == null)
            {
                if (!Examine())
                {
                    return;
                }
                if (MessageForm.ShowYesNoQuestionMsg(
                    "Для добавления вопросов необходимо создать тематику.\n Создать тематику в БД?")
                    == DialogResult.No)
                {
                    return;
                }
                _method = new Method(textEdit1.Text, new Content(""));
                _method.CreateAndFlush();
                AddMethod(_method);
            }
            var uc = new ChangeControlQuestions(_method);
            uc.InitControl();
            uc.ShowInForm();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            ParentForm.Close();
        }

       
    }
}
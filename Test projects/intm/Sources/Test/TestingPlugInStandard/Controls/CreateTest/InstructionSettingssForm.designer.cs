
using Eureca.Professional.TestingPlugInStandard.Components;

namespace Eureca.Professional.TestingPlugInStandard.Controls.CreateTest
{
    partial class InstructionsSettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InstructionsSettingsForm));
            this._buttonsPanel = new System.Windows.Forms.Panel();
            this._startButton = new System.Windows.Forms.Button();
            this._cancelButton = new System.Windows.Forms.Button();
            this.TextPropertiesToolStrip = new System.Windows.Forms.ToolStrip();
            this.TypeButton = new System.Windows.Forms.ToolStripButton();
            this.TypeColorButton = new System.Windows.Forms.ToolStripButton();
            this.insertImageToolStripLabel = new System.Windows.Forms.ToolStripLabel();
            this._copyPasteContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this._instructionsTextBox = new ExRichTextBox();
            this._buttonsPanel.SuspendLayout();
            this.TextPropertiesToolStrip.SuspendLayout();
            this._copyPasteContextMenuStrip.SuspendLayout();
            this._tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // _buttonsPanel
            // 
            this._buttonsPanel.Controls.Add(this._startButton);
            this._buttonsPanel.Controls.Add(this._cancelButton);
            this._buttonsPanel.Location = new System.Drawing.Point(1, 478);
            this._buttonsPanel.Margin = new System.Windows.Forms.Padding(0);
            this._buttonsPanel.Name = "_buttonsPanel";
            this._buttonsPanel.Size = new System.Drawing.Size(755, 43);
            this._buttonsPanel.TabIndex = 5;
            // 
            // _startButton
            // 
            this._startButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this._startButton.Location = new System.Drawing.Point(87, 8);
            this._startButton.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this._startButton.Name = "_startButton";
            this._startButton.Size = new System.Drawing.Size(228, 29);
            this._startButton.TabIndex = 4;
            this._startButton.Text = "�����";
            this._startButton.UseVisualStyleBackColor = true;
            this._startButton.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.OnPreviewKeyDown);
            this._startButton.Click += new System.EventHandler(this.OnStartButtonClick);
            // 
            // _cancelButton
            // 
            this._cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this._cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._cancelButton.Location = new System.Drawing.Point(445, 8);
            this._cancelButton.Margin = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this._cancelButton.Name = "_cancelButton";
            this._cancelButton.Size = new System.Drawing.Size(228, 29);
            this._cancelButton.TabIndex = 5;
            this._cancelButton.Text = "��������";
            this._cancelButton.UseVisualStyleBackColor = true;
            this._cancelButton.Click += new System.EventHandler(this._cancelButton_Click);
            // 
            // TextPropertiesToolStrip
            // 
            this.TextPropertiesToolStrip.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TextPropertiesToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.TextPropertiesToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TypeButton,
            this.TypeColorButton,
            this.insertImageToolStripLabel});
            this.TextPropertiesToolStrip.Location = new System.Drawing.Point(1, 1);
            this.TextPropertiesToolStrip.Name = "TextPropertiesToolStrip";
            this.TextPropertiesToolStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.TextPropertiesToolStrip.Size = new System.Drawing.Size(859, 21);
            this.TextPropertiesToolStrip.TabIndex = 6;
            this.TextPropertiesToolStrip.Text = "��������� ������� ����������� ������";
            // 
            // TypeButton
            // 
            this.TypeButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.TypeButton.Image = ((System.Drawing.Image)(resources.GetObject("TypeButton.Image")));
            this.TypeButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TypeButton.Name = "TypeButton";
            this.TypeButton.Size = new System.Drawing.Size(47, 18);
            this.TypeButton.Text = "�����";
            this.TypeButton.ToolTipText = "��������� ������� ������ ����������� ������";
            this.TypeButton.Click += new System.EventHandler(this.TypeButton_Click);
            // 
            // TypeColorButton
            // 
            this.TypeColorButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.TypeColorButton.Image = ((System.Drawing.Image)(resources.GetObject("TypeColorButton.Image")));
            this.TypeColorButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TypeColorButton.Name = "TypeColorButton";
            this.TypeColorButton.Size = new System.Drawing.Size(80, 18);
            this.TypeColorButton.Text = "���� ������";
            this.TypeColorButton.ToolTipText = "��������� ����� ������ ����������� ������";
            this.TypeColorButton.Click += new System.EventHandler(this.TypeColorButton_Click);
            // 
            // insertImageToolStripLabel
            // 
            this.insertImageToolStripLabel.ActiveLinkColor = System.Drawing.Color.Transparent;
            this.insertImageToolStripLabel.BackColor = System.Drawing.Color.Transparent;
            this.insertImageToolStripLabel.Name = "insertImageToolStripLabel";
            this.insertImageToolStripLabel.Size = new System.Drawing.Size(105, 18);
            this.insertImageToolStripLabel.Text = "�������� ��������";
            this.insertImageToolStripLabel.Click += new System.EventHandler(this.insertImageToolStripLabel_Click);
            // 
            // _copyPasteContextMenuStrip
            // 
            this._copyPasteContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copyToolStripMenuItem,
            this.pasteToolStripMenuItem});
            this._copyPasteContextMenuStrip.Name = "contextMenuStrip1";
            this._copyPasteContextMenuStrip.Size = new System.Drawing.Size(147, 48);
            // 
            // copyToolStripMenuItem
            // 
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            this.copyToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.copyToolStripMenuItem.Text = "����������";
            this.copyToolStripMenuItem.Click += new System.EventHandler(this.copyToolStripMenuItem_Click);
            // 
            // pasteToolStripMenuItem
            // 
            this.pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
            this.pasteToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.pasteToolStripMenuItem.Text = "��������";
            this.pasteToolStripMenuItem.Click += new System.EventHandler(this.pasteToolStripMenuItem_Click);
            // 
            // _tableLayoutPanel
            // 
            this._tableLayoutPanel.BackColor = System.Drawing.Color.LightBlue;
            this._tableLayoutPanel.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this._tableLayoutPanel.ColumnCount = 1;
            this._tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._tableLayoutPanel.Controls.Add(this._buttonsPanel, 0, 2);
            this._tableLayoutPanel.Controls.Add(this.TextPropertiesToolStrip, 0, 0);
            this._tableLayoutPanel.Controls.Add(this._instructionsTextBox, 0, 1);
            this._tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tableLayoutPanel.ForeColor = System.Drawing.SystemColors.ControlText;
            this._tableLayoutPanel.Location = new System.Drawing.Point(7, 6);
            this._tableLayoutPanel.Name = "_tableLayoutPanel";
            this._tableLayoutPanel.RowCount = 3;
            this._tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 21F));
            this._tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 43F));
            this._tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this._tableLayoutPanel.Size = new System.Drawing.Size(861, 522);
            this._tableLayoutPanel.TabIndex = 0;
            // 
            // _instructionsTextBox
            // 
            this._instructionsTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this._instructionsTextBox.ContextMenuStrip = this._copyPasteContextMenuStrip;
            this._instructionsTextBox.Cursor = System.Windows.Forms.Cursors.Default;
            this._instructionsTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this._instructionsTextBox.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._instructionsTextBox.HiglightColor = RtfColor.White;
            this._instructionsTextBox.Location = new System.Drawing.Point(1, 23);
            this._instructionsTextBox.Margin = new System.Windows.Forms.Padding(0);
            this._instructionsTextBox.Name = "_instructionsTextBox";
            this._instructionsTextBox.Size = new System.Drawing.Size(859, 454);
            this._instructionsTextBox.TabIndex = 7;
            this._instructionsTextBox.Text = "";
            this._instructionsTextBox.TextColor = RtfColor.Black;
            this._instructionsTextBox.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.OnPreviewKeyDown);
            // 
            // InstructionsSettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CaptionControl = "����������";
            this.Controls.Add(this._tableLayoutPanel);
            this.MinimumSize = new System.Drawing.Size(606, 231);
            this.Name = "InstructionsSettingsForm";
            this.Padding = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.Size = new System.Drawing.Size(875, 534);
            this.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.OnPreviewKeyDown);
            this._buttonsPanel.ResumeLayout(false);
            this.TextPropertiesToolStrip.ResumeLayout(false);
            this.TextPropertiesToolStrip.PerformLayout();
            this._copyPasteContextMenuStrip.ResumeLayout(false);
            this._tableLayoutPanel.ResumeLayout(false);
            this._tableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button _cancelButton;
        private System.Windows.Forms.ToolStripButton TypeButton;
        private System.Windows.Forms.ToolStripButton TypeColorButton;
        public System.Windows.Forms.ToolStrip TextPropertiesToolStrip;
        //public System.Windows.Standarts.Panel _buttonsPanel;
        private System.Windows.Forms.ContextMenuStrip _copyPasteContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem;
        private System.Windows.Forms.ToolStripLabel insertImageToolStripLabel;
        private System.Windows.Forms.TableLayoutPanel _tableLayoutPanel;
        public ExRichTextBox _instructionsTextBox;
        public System.Windows.Forms.Button _startButton;
        public System.Windows.Forms.Panel _buttonsPanel;
    }
}
﻿using System;
using System.Collections.Generic;
using Castle.ActiveRecord;
using Eureca.Professional.ActiveRecords;
using Eureca.Professional.CommonClasses;
using NHibernate.Criterion;

namespace Eureca.Professional.TestingPlugInStandard.Controls.CreateTest
{
   
    /// <summary>
    /// контрол создания ответа и конфигурирования сырых баллов
    /// </summary>
    internal partial class CreateAnswer : BaseTestingControl
    {
        #region поля

        private readonly List<Answer> _allAnswers;
        private readonly ObjectWithId _answer;
        private readonly bool _flagTestAnswer;
        private readonly Question _question;
        private readonly List<ScaleCoefficient> _scaleCoefficients = new List<ScaleCoefficient>();

        /// <summary>
        /// ссылка на класс теста
        /// если  = null значи это тематика
        /// </summary>
        private readonly Test _test;

        #endregion

        #region события

        internal event CreateAnswerEventHandler AnswerChanged;
        //internal event CreateAnswerEventHandler ExistAnswer;

        #endregion

        #region конструктор

        internal CreateAnswer(ObjectWithId answer, bool flagTestAnswer, Test test, Question question,
                              List<Answer> allAnswers)
        {
            _answer = answer;
            _question = question;
            _flagTestAnswer = flagTestAnswer;
            if (test != null)
                _test = ActiveRecordBase<Test>.Find(test.Id);
            _allAnswers = allAnswers;
            InitializeComponent();
        }

        #endregion

        #region методы

        public override void InitControl()
        {
            base.InitControl();
            CaptionControl = "Изменение ответа";

            getAnswer1.InitControl(_allAnswers);

            if (_answer != null && _answer.Value != null)
            {
                getAnswer1.SelectedAnswer = (Answer) _answer.Value;
            }
            else
            {
                xtraTabPageCoefficients.PageEnabled = false;
            }
            //отключаем вкладку с выбором ответа, т.к. этот ответ нельзя
            //здесь менять, меняется в мастере
            if (_flagTestAnswer)
            {
                xtraTabPageName.PageEnabled = false;
            }
            //отключаем вкладку с сырыми баллами
            xtraTabPageCoefficients.PageEnabled = !( _test == null || _question == null || _answer == null || _answer.Value == null );
            InsertData();
        }

        private void SetCurrentAnswer(Answer answer)
        {
            //SetCurrentAnswer(answer);
            _answer.SetValue(answer);
            xtraTabPageCoefficients.PageEnabled = !(_test == null || _question == null || _answer == null || _answer.Value == null);
        }

        private void InsertData()
        {
            if (_test == null || _question == null)
                return;
            //шкалы
            listBoxControlSelectedScales.Items.Clear();
            foreach (Scale scale in _test.Scales)
            {
                listBoxControlSelectedScales.Items.Add(scale);
            }

            //сырые баллы по шкалам
            var coefficients = new List<RawPoint>();
            //using (new SessionScope())
            //{
                coefficients.AddRange(ActiveRecordBase<RawPoint>.FindAll( //new Order("QuestionOrder", true),
                                          Expression.Eq("Test", _test),
                                          Expression.Eq("Question", _question),
                                          Expression.Eq("Answer", _answer.Value)));
                _scaleCoefficients.Clear();
                foreach (RawPoint coefficient in coefficients)
                {
                    //поиск в списке со шкалами
                    for (int i = 0; i < listBoxControlSelectedScales.ItemCount; i++)
                    {
                        if (((Scale) listBoxControlSelectedScales.Items[i]).Id == coefficient.Scale.Id)
                        {
                            //Записываем данные в таблицу с коэффициентами
                            var sc = new ScaleCoefficient((Scale) listBoxControlSelectedScales.Items[i], coefficient);
                            _scaleCoefficients.Add(sc);

                            //удаляем найденную шкалу
                            listBoxControlSelectedScales.Items.RemoveAt(i);
                            break;
                        }
                    }
                }
                RefreshTable();
            //}
        }

        /// <summary>Метод обновляет таблицу сырых баллов</summary>
        private void RefreshTable()
        {
            gridControl1.DataSource = _scaleCoefficients;
            gridView1.BestFitColumns();
        }

        private bool OnAnswerChanged(Answer answer)
        {
            if (AnswerChanged == null)
            {
                MessageForm.ShowErrorMsg("Нет подписавшихся на событие AnswerChanged");
                return false;
            }
            else
            {
                if (AnswerChanged(new ObjectWithId(_answer.ID, answer)))
                {
                    var coefficients = new List<RawPoint>();
                    //using (new SessionScope())
                    //{
                        coefficients.AddRange(ActiveRecordBase<RawPoint>.FindAll( //new Order("QuestionOrder", true),
                                                  Expression.Eq("Test", _test),
                                                  Expression.Eq("Question", _question),
                                                  Expression.Eq("Answer", _answer.Value)));
                    
                        foreach (RawPoint rawPoint in coefficients)
                        {
                            rawPoint.Delete();
                        }
                        foreach (ScaleCoefficient scaleCoefficient in _scaleCoefficients)
                        {
                            var rp = new RawPoint(_test, scaleCoefficient.scale, _question,
                                                  (Answer) _answer.Value, scaleCoefficient.rawPointsCoefficient.Value);
                            rp.Save();
                        }
                    //}
                    return true;
                }
                return false;
            }
        }

        /// <summary>Метод перемещения из списка шкал без коэффициентов 
        /// в таблицу с коэффициентами</summary>
        /// <param name="All"></param>
        private void ToWithCoefficients(bool All)
        {
            //if(_answer.Value == null)
            //{
            //    if(getAnswer1.SelectedAnswer == null)
            //    {
            //        MessageForm.ShowErrorMsg("Необходимо выбрать ответ");
            //        return;
            //    }
            //    else
            //    {
            //        _answer.SetValue(getAnswer1.SelectedAnswer);
            //    }
            //}
            if (!All)
            {
                if (listBoxControlSelectedScales.SelectedIndex == -1)
                    return;
                //Создаем коэффициент 
                var scale = (Scale) listBoxControlSelectedScales.SelectedItem;
                //if(scale.TypeScale == 2)
                //{
                //    MessageForm.ShowErrorMsg(string.Format("Шкалу {0} нельзя конфигурировать", scale));
                //    return;
                //}
                RawPoint coef = null;
                try
                {
                    coef = new RawPoint(_test, scale, _question, (Answer) _answer.Value,
                                        1);
                }
                catch (Exception)
                {
                    MessageForm.ShowErrorMsg("Ошибка добавления сырых баллов");
                    return;
                }
                //добавление коэффициента в таблицу
                _scaleCoefficients.Add(new ScaleCoefficient(scale, coef));
                //удаление шкалы из списка пустых шкал
                listBoxControlSelectedScales.Items.Remove(listBoxControlSelectedScales.SelectedItem);
            }
            else
            {
                foreach (Scale scale in listBoxControlSelectedScales.Items)
                {
                    //if (scale.TypeScale == 2)
                    //{
                    //    MessageForm.ShowErrorMsg(string.Format("Шкалу {0} нельзя конфигурировать", scale));
                    //    return;
                    //}
                    var coef = new RawPoint(_test, scale, _question, (Answer) _answer.Value,
                                            1);
                    // coef.Save();
                    //добавление коэффициента в таблицу
                    _scaleCoefficients.Add(new ScaleCoefficient(scale, coef));
                }
                listBoxControlSelectedScales.Items.Clear();
            }
            RefreshTable();
        }

        /// <summary>
        /// Метод перемещения из списка выбраных шкал во все шкалы
        /// </summary>
        /// <param name="All"></param>
        private void DropWithCoefficients(bool All)
        {
            if (!All)
            {
                if (gridView1.RowCount == 0 || gridView1.FocusedRowHandle < 0)
                    return;
                //удаление коэффициента
                //поиск шкалы и коэффициента
                int scaleId = int.Parse(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "ScaleId").ToString());
                for (int i = 0; i < _scaleCoefficients.Count; i++)
                {
                    ScaleCoefficient scaleCoefficient = _scaleCoefficients[i];
                    if (scaleId == scaleCoefficient.scale.Id)
                    {
                        //удаление коэффициента
                        // scaleCoefficient.rawPointsCoefficient.Delete();
                        listBoxControlSelectedScales.Items.Add(_scaleCoefficients[i].scale);
                        _scaleCoefficients.RemoveAt(i);
                        break;
                    }
                }
            }
            else
            {
                for (int i = 0; i < _scaleCoefficients.Count; i++)
                {
                    ScaleCoefficient scaleCoefficient = _scaleCoefficients[i];
                    //удаление коэффициента
                    // scaleCoefficient.rawPointsCoefficient.Delete();
                    listBoxControlSelectedScales.Items.Add(scaleCoefficient.scale);
                    _scaleCoefficients.RemoveAt(i);
                    i--;
                }
            }
            RefreshTable();
        }

        #endregion

        #region обработчики событий стандартных компонент

        private void simpleButtonOk_Click(object sender, EventArgs e)
        {
            if (getAnswer1.SelectedAnswer == null)
            {
                MessageForm.ShowOkMsg("Выберите ответ из списка");
                return;
            }
            SetCurrentAnswer( getAnswer1.SelectedAnswer );
            //_answer.SetValue(getAnswer1.SelectedAnswer);
            if (!OnAnswerChanged(getAnswer1.SelectedAnswer))
            {
                return;
            }

            if (ParentForm != null) ParentForm.Close();
        }


        private void simpleButtonCancel_Click(object sender, EventArgs e)
        {
            ParentForm.Close();
        }

        private void simpleButtonToOne_Click(object sender, EventArgs e)
        {
            ToWithCoefficients(false);
        }

        private void simpleButtonToMany_Click(object sender, EventArgs e)
        {
            ToWithCoefficients(true);
        }

        private void simpleButtonDropOne_Click(object sender, EventArgs e)
        {
            DropWithCoefficients(false);
        }

        private void simpleButtonDropmany_Click(object sender, EventArgs e)
        {
            DropWithCoefficients(true);
        }

        #endregion
    }

    internal delegate bool CreateAnswerEventHandler( ObjectWithId answer );

}
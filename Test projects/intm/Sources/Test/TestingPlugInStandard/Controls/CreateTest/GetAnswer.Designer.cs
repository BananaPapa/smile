﻿
using Eureca.Professional.TestingPlugInStandard.Components;

namespace Eureca.Professional.TestingPlugInStandard.Controls.CreateTest
{
    partial class GetAnswer
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.listBoxControlAllAnswers = new DevExpress.XtraEditors.ListBoxControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.radioGroup1 = new DevExpress.XtraEditors.RadioGroup();
            this.contextMenuStripExRich = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.simpleButtonAdd = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.answerLabel = new System.Windows.Forms.Label();
            this.exRichTextBoxSelectAnswer = new Eureca.Professional.TestingPlugInStandard.Components.ExRichTextBox();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.exRichTextBoxNewAnswer = new Eureca.Professional.TestingPlugInStandard.Components.ExRichTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxControlAllAnswers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup1.Properties)).BeginInit();
            this.contextMenuStripExRich.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            this.SuspendLayout();
            // 
            // listBoxControlAllAnswers
            // 
            this.listBoxControlAllAnswers.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listBoxControlAllAnswers.Location = new System.Drawing.Point(3, 1);
            this.listBoxControlAllAnswers.Name = "listBoxControlAllAnswers";
            this.listBoxControlAllAnswers.Size = new System.Drawing.Size(136, 177);
            this.listBoxControlAllAnswers.TabIndex = 14;
            this.listBoxControlAllAnswers.SelectedIndexChanged += new System.EventHandler(this.listBoxControlAllAnswers_SelectedIndexChanged);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(171, 28);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(67, 26);
            this.labelControl1.TabIndex = 17;
            this.labelControl1.Text = "Добавить\r\nизображение";
            this.labelControl1.Visible = false;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(244, 28);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(28, 23);
            this.simpleButton1.TabIndex = 12;
            this.simpleButton1.Text = "...";
            this.simpleButton1.Visible = false;
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // radioGroup1
            // 
            this.radioGroup1.EditValue = 1;
            this.radioGroup1.Location = new System.Drawing.Point(5, 23);
            this.radioGroup1.Name = "radioGroup1";
            this.radioGroup1.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Простой текст"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(2, "Форматированный текст")});
            this.radioGroup1.Size = new System.Drawing.Size(160, 47);
            this.radioGroup1.TabIndex = 7;
            this.radioGroup1.ToolTip = "Переключение режима ввода";
            // 
            // contextMenuStripExRich
            // 
            this.contextMenuStripExRich.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1});
            this.contextMenuStripExRich.Name = "contextMenuStripExRich";
            this.contextMenuStripExRich.Size = new System.Drawing.Size(123, 26);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(122, 22);
            this.toolStripMenuItem1.Text = "Вставить";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // simpleButtonAdd
            // 
            this.simpleButtonAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.simpleButtonAdd.Location = new System.Drawing.Point(306, 290);
            this.simpleButtonAdd.Name = "simpleButtonAdd";
            this.simpleButtonAdd.Size = new System.Drawing.Size(89, 23);
            this.simpleButtonAdd.TabIndex = 10;
            this.simpleButtonAdd.Text = "Сохранить";
            this.simpleButtonAdd.Click += new System.EventHandler(this.simpleButtonAdd_Click);
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.splitContainerControl1);
            this.groupControl1.Location = new System.Drawing.Point(405, 6);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(145, 318);
            this.groupControl1.TabIndex = 20;
            this.groupControl1.Text = "Текущий ответ";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(2, 21);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.listBoxControlAllAnswers);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.answerLabel);
            this.splitContainerControl1.Panel2.Controls.Add(this.exRichTextBoxSelectAnswer);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(141, 295);
            this.splitContainerControl1.SplitterPosition = 182;
            this.splitContainerControl1.TabIndex = 18;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // answerLabel
            // 
            this.answerLabel.BackColor = System.Drawing.Color.White;
            this.answerLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.answerLabel.Location = new System.Drawing.Point(0, 0);
            this.answerLabel.Name = "answerLabel";
            this.answerLabel.Size = new System.Drawing.Size(141, 108);
            this.answerLabel.TabIndex = 19;
            this.answerLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // exRichTextBoxSelectAnswer
            // 
            this.exRichTextBoxSelectAnswer.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.exRichTextBoxSelectAnswer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.exRichTextBoxSelectAnswer.HiglightColor = Eureca.Professional.TestingPlugInStandard.Components.RtfColor.White;
            this.exRichTextBoxSelectAnswer.Location = new System.Drawing.Point(0, 0);
            this.exRichTextBoxSelectAnswer.Name = "exRichTextBoxSelectAnswer";
            this.exRichTextBoxSelectAnswer.Size = new System.Drawing.Size(141, 108);
            this.exRichTextBoxSelectAnswer.TabIndex = 18;
            this.exRichTextBoxSelectAnswer.Text = "";
            this.exRichTextBoxSelectAnswer.TextColor = Eureca.Professional.TestingPlugInStandard.Components.RtfColor.Black;
            // 
            // groupControl2
            // 
            this.groupControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupControl2.Controls.Add(this.labelControl1);
            this.groupControl2.Controls.Add(this.simpleButton1);
            this.groupControl2.Controls.Add(this.radioGroup1);
            this.groupControl2.Controls.Add(this.exRichTextBoxNewAnswer);
            this.groupControl2.Controls.Add(this.simpleButtonAdd);
            this.groupControl2.Location = new System.Drawing.Point(3, 6);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(400, 318);
            this.groupControl2.TabIndex = 19;
            this.groupControl2.Text = "Добавление ответа в БД";
            // 
            // exRichTextBoxNewAnswer
            // 
            this.exRichTextBoxNewAnswer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.exRichTextBoxNewAnswer.AutoWordSelection = true;
            this.exRichTextBoxNewAnswer.ContextMenuStrip = this.contextMenuStripExRich;
            this.exRichTextBoxNewAnswer.EnableAutoDragDrop = true;
            this.exRichTextBoxNewAnswer.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.exRichTextBoxNewAnswer.HiglightColor = Eureca.Professional.TestingPlugInStandard.Components.RtfColor.White;
            this.exRichTextBoxNewAnswer.Location = new System.Drawing.Point(5, 76);
            this.exRichTextBoxNewAnswer.Name = "exRichTextBoxNewAnswer";
            this.exRichTextBoxNewAnswer.Size = new System.Drawing.Size(390, 208);
            this.exRichTextBoxNewAnswer.TabIndex = 8;
            this.exRichTextBoxNewAnswer.Text = "";
            this.exRichTextBoxNewAnswer.TextColor = Eureca.Professional.TestingPlugInStandard.Components.RtfColor.Black;
            // 
            // GetAnswer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.groupControl2);
            this.MinimumSize = new System.Drawing.Size(553, 327);
            this.Name = "GetAnswer";
            this.Size = new System.Drawing.Size(553, 327);
            ((System.ComponentModel.ISupportInitialize)(this.listBoxControlAllAnswers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup1.Properties)).EndInit();
            this.contextMenuStripExRich.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.ListBoxControl listBoxControlAllAnswers;
        private DevExpress.XtraEditors.RadioGroup radioGroup1;
        private Components.ExRichTextBox exRichTextBoxNewAnswer;
        private DevExpress.XtraEditors.SimpleButton simpleButtonAdd;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private ExRichTextBox exRichTextBoxSelectAnswer;
        private System.Windows.Forms.Label answerLabel;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripExRich;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
    }
}
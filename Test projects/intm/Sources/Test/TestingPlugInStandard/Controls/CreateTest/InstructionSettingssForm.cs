using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using Eureca.Professional.ActiveRecords;
using Eureca.Professional.CommonClasses;


namespace Eureca.Professional.TestingPlugInStandard.Controls.CreateTest
{
    internal partial class InstructionsSettingsForm : BaseTestingControl
    {
        private readonly ColorDialog colorDlg = new ColorDialog();
        private readonly FontDialog fontDlg = new FontDialog();
        private bool IsEnter;
        private Method _method;

        public InstructionsSettingsForm()
        {
            InitializeComponent();
            //TextPropertiesToolStrip.BackColor = Color.FromArgb(255, 128, 255, 255);
            //_buttonsPanel.BackColor = Color.FromArgb(255, 128, 255, 255);
        }

        public string LeftButtonText
        {
            set { _startButton.Text = value; }
        }

        public string RightButtonText
        {
            set { _cancelButton.Text = value; }
        }


        public bool CopyPasteContextMenuDispose
        {
            set { if (value) _copyPasteContextMenuStrip.Dispose(); }
        }

        //public string InstructionText
        //{
        //    get { return _instructionsTextBox.Rtf; }

        //    set
        //    {
        //        try
        //        {
        //            _instructionsTextBox.Rtf = value;
        //            //_instructionText = value;
        //        }
        //        catch (Exception)
        //        {
        //            _instructionsTextBox.Text = value;
        //            //_instructionText = value;
        //        }
        //    }
        //}

        public Method Method
        {
            set { _method = value; }
        }

        public override void InitControl()
        {
            base.InitControl();
        }


        private void OnPreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                IsEnter = true;
        }

        public virtual void OnStartButtonClick(object sender, EventArgs e)
        {
            if (IsEnter)
            {
                // ��� ������� ������
                _instructionsTextBox.InsertTextAsRtf("\n");
                _instructionsTextBox.Focus();
                IsEnter = false;
            }
            else
            {
                // ���� �����

                var ms_ = new MemoryStream();

                _instructionsTextBox.SaveFile(ms_, RichTextBoxStreamType.RichText);

                if (ms_.Length > Consts.ContentMaxSize)
                {
                    MessageForm.ShowErrorMsg("������ ���������� ���������� " + ms_.Length/1024 +
                                             " �����. \n��������� ������ ������ � ����������� �� " +
                                             Consts.ContentMaxSize/1024 + " �����.");
                    return;
                }

                _method.Instruction = new Content(ms_.ToArray());
                _method.UpdateAndFlush();
                //ResetSessionScope()();
                if (ParentForm != null)
                    ParentForm.DialogResult = DialogResult.OK;
                //Save();
            }
        }

        public void SaveInstruction()
        {
            _method.Update();
        }

        private void TypeButton_Click(object sender, EventArgs e)
        {
            fontDlg.Font = _instructionsTextBox.SelectionFont;

            if (fontDlg.ShowDialog() == DialogResult.OK)
            {
                _instructionsTextBox.SelectionFont = fontDlg.Font;
            }
        }

        private void TypeColorButton_Click(object sender, EventArgs e)
        {
            colorDlg.Color = _instructionsTextBox.SelectionColor;

            if (colorDlg.ShowDialog() == DialogResult.OK)
            {
                _instructionsTextBox.SelectionColor = colorDlg.Color;
            }
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _instructionsTextBox.Copy();
        }

        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _instructionsTextBox.Paste();
        }

        private void _cancelButton_Click(object sender, EventArgs e)
        {
            //DialogResult = DialogResult.Cancel;
            //Close();
        }

        private void insertImageToolStripLabel_Click(object sender, EventArgs e)
        {
            var _dialog = new OpenFileDialog();
            _dialog.Filter = "��� �����������|*.bmp;*.ico;*.gif;*.jpeg;*.jpg;*.png;*.tif;*.tiff|" +
                             "Windows Bitmap(*.bmp)|*.bmp|" +
                             "Windows Icon(*.ico)|*.ico|" +
                             "GIF (*.gif)|(*.gif)|" +
                             "JPEG  (*.jpg)|*.jpg;*.jpeg|" +
                             "PNG (*.png)|*.png|" +
                             "TIF (*.tif)|*.tif;*.tiff";
            if (DialogResult.OK == _dialog.ShowDialog(this))
            {
                try
                {
                    // If file is an icon
                    if (_dialog.FileName.ToUpper().EndsWith(".ICO"))
                    {
                        // Create a new icon, get it's handle, and create a bitmap from
                        // its handle
                        _instructionsTextBox.InsertImage(Bitmap.FromHicon((new Icon(_dialog.FileName)).Handle));
                    }
                    else
                    {
                        // Image img = Image.FromFile(_dialog.FileName)
                        // Create a bitmap from the filename
                        var fInfo = new FileInfo(_dialog.FileName);
                        int maxLength = 102400;
                        if (fInfo.Length > maxLength)
                        {
                            MessageForm.ShowErrorMsg(
                                "������ ����������� (" + fInfo.Length/1024 +
                                " �����) ��������� ������������ ���������� �������� (" + maxLength/1024 +
                                " �����).\n��� ���������� ������ �������� ��������� �� ������.");
                            return;
                        }

                        _instructionsTextBox.InsertImage(Image.FromFile(_dialog.FileName));
                    }
                }
                catch (Exception _e)
                {
                    MessageForm.ShowErrorMsg("���������� ������� ����:\n\n" + _e.Message);
                }
            }
        }
    }
}
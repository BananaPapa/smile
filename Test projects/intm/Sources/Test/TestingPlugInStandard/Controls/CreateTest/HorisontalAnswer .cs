﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using Castle.ActiveRecord;
using Eureca.HumanTestingSystem.TestingPlugInStandard.Controls.CreateTest;
using Eureca.Professional.ActiveRecords;
using Eureca.Professional.CommonClasses;
using Eureca.Professional.TestingPlugInStandard.Controls.CreateTest;
using NHibernate.Criterion;

namespace Eureca.Professional.TestingPlugInStandard.Controls.CreateTest
{
    /// <summary>
    /// контрол для отображения ответа
    /// </summary>
    internal partial class HorisontalAnswer : BaseTestingControl
    {
        #region поля

        /// <summary>Список всех ответов в тесте</summary>
        private readonly List<Answer> _allAnswers;

        /// <summary>Флаг отвечает за тестовые вопросы
        /// если true значит тестовый вопрос и его нельзя редактировать</summary>
        private readonly bool _flagTestAnswer;

        private readonly Test _test;
        private ObjectWithId _answer;
        private bool _change;
        private Question _question;

        #endregion

        #region свойства

        internal Answer answer
        {
            get { return (Answer) _answer.Value; }
            // set { _answer.Value = value; }
        }

        internal int Order
        {
            get { return _answer.ID; }
        }

        internal bool Change
        {
            get { return _change; }
        }

        internal bool FlagTestAnswer
        {
            get { return _flagTestAnswer; }
        }

        public bool RightAnswer
        {
            get { return checkEditRightAnswer.Checked; }
        }

        #endregion

        #region события

        internal event HorisontalAnswerEventHandler getFocus;
        internal event HorisontalAnswerEventHandler setRight;

        #endregion

        #region конструктор

        public HorisontalAnswer(ObjectWithId answer, bool testAnswer, Question question, Test test,
                                List<Answer> allAnswer)
        {
            _flagTestAnswer = testAnswer;
            _answer = answer;
            _question = question;
            _test = test;
            _allAnswers = allAnswer;
            InitializeComponent();
        }

        #endregion

        #region методы

        public override void InitControl()
        {
            base.InitControl();
            SetAnswer(_answer, _question);
        }


        public void SetAnswer(ObjectWithId newAnswer, Question testQuestion)
        {
            exRichTextBoxAnswer.Clear();

            _answer = newAnswer;
            _question = testQuestion;
            //установка ответа
            var a = (Answer) _answer.Value;
            string pc = null;
            byte[] rc = null;
            using (new SessionScope())
            {
                var content = Content.Find(a.Content.Id);
                pc = content.PlainContent;
                rc = content.RichContent;
            }

            if (pc == null)
            {
                exRichTextBoxAnswer.Visible = true;
                AnswerLabel.Visible = false;
                var ms = new MemoryStream(rc);
                exRichTextBoxAnswer.LoadFile(ms, RichTextBoxStreamType.RichText);
            }
            else
            {
                exRichTextBoxAnswer.Visible = false;
                AnswerLabel.Visible = true;

                AnswerLabel.Font =
                    new Font("Microsoft Sans Serif",
                             25F, FontStyle.Bold,
                             GraphicsUnit.Pixel,
                             204);

                AnswerLabel.Padding = new Padding(10);
                AnswerLabel.Text = pc;
            }

            //загрузка сырых баллов для того чтобы понять "правильный ответ у нас или нет"
            //для теста
            if (_test != null)
            {
                var rawPoints = new List<RawPoint>();
                using (new SessionScope(FlushAction.Never))
                {
                    rawPoints.AddRange(ActiveRecordBase<RawPoint>.FindAll(Expression.Eq("Test", _test),
                                                                          Expression.Eq("Question", _question),
                                                                          Expression.Eq("Answer", a)));
                }
                var existScale = new List<Scale>();
                //взяли все шкалы из сырых баллов
                foreach (RawPoint rawPoint in rawPoints)
                {
                    if (rawPoint.Value > 0)
                        existScale.Add(rawPoint.Scale);
                }
                //просмотр по всем шкалам.
                //если на шкалу нет сырых баллов, то создание сырого балла со значением 1
                bool exist = false;

                foreach (Scale scale in _test.Scales)
                {
                    //if (scale.TypeScale == 2)
                    //    continue;
                    if (!existScale.Contains(scale))
                    {
                        exist = false;
                        break;
                    }
                    else
                    {
                        exist = true;
                    }
                }
                //проставляем флажок правильного ответа
                checkEditRightAnswer.Checked = exist;
            }

            labelControl1.Text = Order.ToString();
            _change = false;
        }

        public void SetRightAnswer(bool right)
        {
            checkEditRightAnswer.Checked = right;
            if (right)
            {
                labelControl1.BackColor = Color.LightGreen;
            }
            else
            {
                labelControl1.BackColor = BackColor;
            }
        }

        #endregion

        #region обработчики событий стандартных компонент

        private void exRichTextBoxAnswer_TextChanged(object sender, EventArgs e)
        {
            _change = true;
        }

        private void radioGroup1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            var uc = new CreateAnswer(_answer, _flagTestAnswer, _test, _question, _allAnswers);
            uc.AnswerChanged += uc_AnswerChanged;
            uc.InitControl();
            uc.ShowInForm();
        }

        private void pictureEditAnswer_MouseClick(object sender, MouseEventArgs e)
        {
            getFocus(this);
        }

        private void AnswerLabel_Click(object sender, EventArgs e)
        {
            getFocus(this);
        }

        private void radioGroup1_MouseDoubleClick(object sender, EventArgs e)
        {
            var uc = new CreateAnswer(_answer, _flagTestAnswer, _test, _question, _allAnswers);
            uc.AnswerChanged += uc_AnswerChanged;
            uc.InitControl();
            uc.ShowInForm();
        }

        /// <summary>
        /// обработчик события изменения значения переключателя
        /// выставляю правильный ответ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkEditRightAnswer_CheckedChanged(object sender, EventArgs e)
        {
            if (_test != null)
            {
                using (new TransactionScope())
                {
                //для теста
                //получаю список все сырых баллов по всем шкалам
                var rawPoints = new List<RawPoint>();
                rawPoints.AddRange(ActiveRecordBase<RawPoint>.FindAll(Expression.Eq("Test", _test),
                                                                      Expression.Eq("Question", _question),
                                                                      Expression.Eq("Answer", _answer.Value)));
                if (checkEditRightAnswer.Checked)
                {
                    //создаю у всех шкал сырые баллы с коэффициентом 1

                    //просмотр по всем шкалам.
                    //если на шкалу нет сырых баллов, то создание сырого балла со значением 1
                    foreach (Scale scale in _test.Scales)
                    {
                        //if (scale.TypeScale == 2)
                        //    continue;
                        bool exist = false;
                        foreach (RawPoint rawPoint in rawPoints)
                        {
                            if (scale.Equals(rawPoint.Scale))
                            {
                                //Если есть сырой балл то проверяю, если он меньше 0 тогда делаю его 1
                                if (rawPoint.Value < 0)
                                {
                                    rawPoint.Value = 1;
                                    rawPoint.Save();
                                }
                                exist = true;
                                break;
                            }
                        }
                        if (!exist)
                        {
                            //нет сырых баллов на эту шкалу
                            var rp = new RawPoint(_test, scale, _question, (Answer)_answer.Value, 1);
                            rp.Save();
                        }
                    }
                }
                else
                {
                    //задаю вопрос про удаление галочки
                    if (rawPoints.Count > 0)
                    {
                        //просматриваю ответ, является ли он "правильный"
                        var existScale = new List<Scale>();
                        foreach (RawPoint rawPoint in rawPoints)
                        {
                            if (rawPoint.Value > 0)
                                //
                                existScale.Add(rawPoint.Scale);
                        }
                        //просмотр по всем шкалам.
                        //если на шкалу нет сырых баллов, то создание сырого балла со значением 1
                        bool exist = false;

                        foreach (Scale scale in _test.Scales)
                        {
                            //if (scale.TypeScale == 2)
                            //    continue;
                            if (!existScale.Contains(scale))
                            {
                                exist = false;
                                break;
                            }
                            else
                            {
                                exist = true;
                            }
                        }

                        //if (exist &&
                        //    MessageForm.ShowYesNoQuestionMsg(
                        //        "Удалятся все сырые баллы для данного ответа. Продолжить") ==
                        //    DialogResult.Yes)
                        //{
                        //удаляю все сырые баллы
                        foreach (RawPoint rawPoint in rawPoints)
                        {
                            rawPoint.Delete();
                        }
                        //}
                    }
                    //else
                    //{
                    //    checkEditRightAnswer.Checked = true;
                    //}
                }
                }
            }
            else
            {
                //для тематики
                setRight(this);
            }
        }

        #endregion

        #region обработчики событий пользовательских компонент

        private bool uc_AnswerChanged(ObjectWithId newAnswer)
        {
            using (new TransactionScope())
            {
            QuestionAnswer[] questionAnswers =
                ActiveRecordBase<QuestionAnswer>.FindAll(new Order("AnswerOrder", true),
                                                         Expression.Eq("Question",
                                                                       _question));
            //поиск уже существующего такого ответа
            //в ответах вопроса
            foreach (QuestionAnswer qA in questionAnswers)
            {
                if (qA.Answer.Equals(newAnswer.Value) && qA.AnswerOrder != newAnswer.ID)
                {
                    MessageForm.ShowErrorMsg("Данный ответ уже присутствует в наборе");
                    return false;
                }
            }
            //поиск уже существующего такого ответа
            //в ответах теста
            if (_test != null)
            {
                TestAnswer[] testAnswers = ActiveRecordBase<TestAnswer>.FindAll(new Order("AnswerOrder", true),
                                                                                Expression.Eq("Test", _test));
                foreach (TestAnswer tA in testAnswers)
                {
                    if (tA.Answer.Equals(newAnswer.Value) && tA.AnswerOrder != newAnswer.ID)
                    {
                        MessageForm.ShowErrorMsg("Данный ответ уже присутствует в наборе");
                        return false;
                    }
                }
            }
            foreach (QuestionAnswer questionAnswer in questionAnswers)
            {
                if (questionAnswer.AnswerOrder == newAnswer.ID)
                {
                    if (!questionAnswer.Answer.Equals(newAnswer.Value))
                    {
                        questionAnswer.Delete();
                        var newQuestionAnswer = new QuestionAnswer(_question, (Answer)newAnswer.Value,
                                                                   newAnswer.ID);
                        newQuestionAnswer.Save();
                    }
                    break;
                }
            }
            SetAnswer(newAnswer, _question);
            return true;
            }
        }

        #endregion
    }
}

namespace Eureca.HumanTestingSystem.TestingPlugInStandard.Controls.CreateTest
{
    internal delegate void HorisontalAnswerEventHandler(HorisontalAnswer sender);
}
﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Eureca.Professional.ActiveRecords;
using Eureca.Professional.CommonClasses;

namespace Eureca.Professional.TestingPlugInStandard.Controls.CreateTest
{
    internal delegate bool AddAnswerEventHandler(Answer answer);

    internal partial class AddAnswer : BaseTestingControl
    {
        private readonly List<Answer> _allAnswers;
        private readonly Answer _answer;

        internal AddAnswer(Answer answer, List<Answer> allAnswers)
        {
            InitializeComponent();
            _answer = answer;
            _allAnswers = allAnswers;
        }

        internal event AddAnswerEventHandler AddAnswerEvent;

        public override void InitControl()
        {
            base.InitControl();
            getAnswer1.InitControl(_allAnswers);
            if (_answer != null)
            {
                getAnswer1.SelectedAnswer = _answer;
            }
            CaptionControl = "Выбор и добавление ответа";
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            if (ParentForm != null)
                ParentForm.DialogResult = DialogResult.Cancel;
            CaptionControl = "Добавление вопроса";
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (getAnswer1.SelectedAnswer == null)
            {
                MessageForm.ShowWarningMsg("Выберите ответ");
                return;
            }
            if (AddAnswerEvent(getAnswer1.SelectedAnswer))
            {
                if (ParentForm != null)
                    ParentForm.DialogResult = DialogResult.OK;
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using Castle.ActiveRecord;
using Eureca.Professional.BaseComponents;
using EurecaCorp.Drawing;
using Eureca.Professional.ActiveRecords;
using Eureca.Professional.CommonClasses;
using Eureca.Professional.TestingPlugInStandard.Components;
using log4net;
using Microsoft.Office.Interop.Word;
using Application = System.Windows.Forms.Application;
using NHibernate.Criterion;

//using System.Drawing;

namespace Eureca.Professional.TestingPlugInStandard.Controls.CreateTest
{
    /// <summary>
    /// контрол - список тестов
    /// </summary>
    public partial class ListTests : BaseTestingControl
    {
        private static readonly ILog Log = LogManager.GetLogger( typeof( ListTests ) );

        #region поля

        private const int MaxIteration = 10;
        private static WaitingForm _wc;
        private readonly List<Method> _methods = new List<Method>();
        private readonly List<Test> _tests = new List<Test>();
        private readonly List<int> _threadpull = new List<int>();
        private readonly User _user;
        private string _fileName;
        private string _filePath;
        private Test _selectedTest;

        #endregion

        #region конструктор формы списка тестов

        public ListTests(User user)
        {
            InitializeComponent();
            _user = user;
        }

        #endregion

        #region методы

        /// <summary>
        /// метод итнициализации контрола
        /// </summary>
        public override void InitControl()
        {
            //_iSessionScope = iSession;
            base.InitControl();


            CaptionControl = "Управление тематиками и тестами";

            //Создание пользователя
            InsertData();
        }

        /// <summary>
        /// вставка данных из БД
        /// </summary>
        private void InsertData()
        {
            _tests.Clear();
            _methods.Clear( );
            using (var sess = new SessionScope())
            {
                _tests.AddRange(Test.FindAll());
                _methods.AddRange( Method.FindAll( ));

                listBoxControlMethods.Items.Clear();
                listBoxControlTests.Items.Clear();
                foreach (Method m in _methods)
                {
                    listBoxControlMethods.Items.Add(m);
                }
                if (listBoxControlMethods.ItemCount > 0)
                    listBoxControlMethods.SelectedIndex = 0;
            }
        }

        /// <summary>
        /// заполнение списка с тестами
        /// </summary>
        private void FillTest()
        {
            if (listBoxControlMethods.SelectedIndex == -1)
                return;
            var m = (Method) listBoxControlMethods.SelectedItem;
            IEnumerable<Test> selectTest =
                from tests in _tests
                where tests.Method.Id == m.Id
                select tests;
            listBoxControlTests.Items.Clear();
            foreach (Test item in selectTest)
            {
                listBoxControlTests.Items.Add(item);
            }
        }

        /// <summary>метод вызывает контрол редактирования теста</summary>
        private void ChangeTest(Method method, Test test)
        {
            Event.Add(_user, EventTypes.BeginEdit, test.Id);
            var createTest = new MasterCreateTest(_user, method, test);
            createTest.ChangeTest += createTest_ChangeTest;
            createTest.InitControl();
            const string str = "Вы уверены, что хотите прервать работу мастера?";
            createTest.ShowInForm(true, false, str);
            Event.Add(_user, EventTypes.EndEdit, test.Id);

        }

        /// <summary>метод вызавает контрол создания теста</summary>
        private void AddTest()
        {
            if (listBoxControlMethods.SelectedIndex == -1)
            {
                MessageForm.ShowErrorMsg("Для создания теста необходимо выбрать тематику");
                return;
            }
            var m = (Method) listBoxControlMethods.SelectedItem;
            var createTest = new MasterCreateTest(_user, m, null);
            createTest.AddTest += createTest_AddTest;
            createTest.ChangeTest += createTest_ChangeTest;
            createTest.InitControl();
            string str = "Вы уверены, что хотите прервать работу мастера?";
            createTest.ShowInForm(true, false, str);
        }

        /// <summary>
        /// удаление теста
        /// </summary>
        /// <param name="ask"></param>
        /// <param name="test"></param>
        /// <returns></returns>
        private bool DeleteTest(bool ask, Test test)
        {
            try
            {
                //TestDBExecutor.DropTest(test);
                using (new SessionScope(FlushAction.Never))
                {
                    test.DeleteWithReference();
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                MessageForm.ShowErrorMsg(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// открытие контрола добавления новой тематики
        /// </summary>
        private void AddMethod()
        {
            var uc = new CreateMethod(null, _methods);
            uc.AddMethod += uc_AddMethod;
            uc.ChangeMethod += uc_ChangeMethod;
            uc.InitControl();
            uc.ShowInForm();
        }

        /// <summary>
        /// изменение методики
        /// </summary>
        /// <param name="method"></param>
        private void ChangeMethod(Method method)
        {
            var uc = new CreateMethod(method, _methods);
            uc.ChangeMethod += uc_ChangeMethod;
            uc.InitControl();
            uc.ShowInForm();
        }

        /// <summary>
        /// метод удаления тематики
        /// </summary>
        /// <param name="method"></param>
        /// <returns></returns>
        private bool DeleteMethod(Method method)
        {
            try
            {
                IEnumerable<Test> selectTest =
                    from tests in _tests
                    where tests.Method.Id == method.Id
                    select tests;
                //using (new SessionScope())
                //{
                //method = ActiveRecordBase<Method>.Find(method.Id);

                
                if (selectTest.Count() > 0)
                {
                    if (MessageForm.ShowYesNoQuestionMsg(
                        "Эта тематика содержит набор тестов\nВы уверены, что хотите удалить эту тематику и все\nсодержащиеся в ней тесты из БД?")
                        == DialogResult.No)
                    {
                        return false;
                    }
                }
                else
                {
                    if (MessageForm.ShowYesNoQuestionMsg("Вы уверены, что хотите удалить эту тематику из БД?")
                        == DialogResult.No)
                    {
                        return false;
                    }
                }
                using (new SessionScope(FlushAction.Never))
                {
                    foreach (Test test in selectTest)
                    {
                        test.DeleteWithReference();
                    }

                    foreach (
                        ControlQuestion controlQuestion in ControlQuestion.FindAll(Restrictions.Eq("Method", method)))
                    {
                        foreach (
                            QuestionAnswer questionAnswer in
                                QuestionAnswer.FindAll(Restrictions.Eq("Question", controlQuestion.Question)))
                        {
                            questionAnswer.DeleteAndFlush();
                        }
                        controlQuestion.DeleteAndFlush();
                    }

                    method = Method.Find(method.Id);
                    method.DeleteAndFlush();
                }

                InsertData();
                return true;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                MessageForm.ShowErrorMsg(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// редактирование теста
        /// </summary>
        private void CommonChangeTest()
        {
            if (listBoxControlMethods.SelectedIndex == -1)
            {
                MessageForm.ShowErrorMsg("Для редактирования теста необходимо выбрать тематику");
                return;
            }
            if (listBoxControlTests.SelectedIndex == -1)
            {
                MessageForm.ShowErrorMsg("Для редактирования необходимо выбрать тест");
                return;
            }
            var m = (Method) listBoxControlMethods.SelectedItem;
            var t = (Test) listBoxControlTests.SelectedItem;
            Event.Add(_user, EventTypes.BeginEdit, t.Id);
            ChangeTest(m, t);
            Event.Add(_user, EventTypes.EndEdit, t.Id);
        }

        /// <summary>
        /// получение текста из ячейки таблицы в Word
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        private static string GetTextFromCell(string text)
        {
            if (text.Trim().Length <= 2)
                return "";
            return text.Trim().Remove(text.Trim().Length - 2, 2);
        }

        /// <summary>
        /// метод установки сообщения в процессовое окно при импорте и экспорте теста
        /// </summary>
        /// <param name="message"></param>
        private void SetMessage(string message)
        {
            if (_wc != null)
                try
                {
                    _wc.SetMessage(message);
                    Application.DoEvents();
                }
                catch (Exception ex)
                {
                    Application.DoEvents();
                    Log.Error(ex);
                }
        }

        /// <summary>
        /// получение текста контента по идентификатору
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private string GetContentText(long id)
        {
            Content c = ActiveRecordBase<Content>.Find(id);
            var r = new ExRichTextBox();
            if (c.PlainContent == null)
            {
                //форматированный текст
                var ms = new MemoryStream(c.RichContent);
                r.LoadFile(ms, RichTextBoxStreamType.RichText);
            }
            else
            {
                r.Text = c.PlainContent;
            }
            return r.Text;
        }

        /// <summary>
        /// метод импортирования теста в систему
        /// </summary>
        private void Import()
        {
            var wm = new WordManager();
            Method method = null;
            Test test = null;
            string str = null;
            int questionTime = 0;
            var r = new ExRichTextBox();

            try
            {
                //создание экземпяра документа
                wm.Open(_filePath + _fileName);
                Thread.Sleep(1000);

                SetMessage("Обработка таблицы 'Общие сведения о тесте'");

                #region чтение информации из таблицы Общие сведения о тесте

                Application.DoEvents();
                Table tbl = wm.WordDoc.Tables[1];
                try
                {
                    Log.Error("чтение таблицы 1. Общие сведения о тесте строк " + tbl.Rows.Count);
                    string testCaption = GetTextFromCell(tbl.Cell(1, 2).Range.Text);
                    Log.Error("testCaption = " + testCaption);
                    string testDescription = GetTextFromCell(tbl.Cell(2, 2).Range.Text);
                    Log.Error("testDescription = " + testDescription);
                    string methodCaption = GetTextFromCell(tbl.Cell(4, 2).Range.Text);
                    Log.Error("methodCaption = " + methodCaption);
                    string methodDescription = GetTextFromCell(tbl.Cell(5, 2).Range.Text);
                    Log.Error("methodDescription = " + methodDescription);
                    str = GetTextFromCell(tbl.Cell(7, 2).Range.Text);
                    Log.Error("testTime = " + str);
                    int testTime = int.Parse(str)*60;
                    string randomize = GetTextFromCell(tbl.Cell(9, 2).Range.Text).ToUpper();
                    Log.Error("randomize = " + randomize);
                    questionTime = 0;
                    string many = "НЕТ";

                    //if (tbl.Rows.Count > 10)
                    //{
                    //    str = GetTextFromCell(tbl.Cell(11, 2).Range.Text);
                    //    Log.Error("questionTime = " + questionTime);
                    //    questionTime = int.Parse(str);

                    //}
                    if (tbl.Rows.Count > 11)
                    {
                        many = GetTextFromCell(tbl.Cell(12, 2).Range.Text).ToUpper();
                        Log.Error("many = " + many);
                    }

                    //проверки входных данных


                    if (testTime < 0)
                        testTime = 0;
                    if (questionTime < 0)
                        questionTime = 0;
                    using (new SessionScope( ))
                    {
                        //проверяем есть ли такие тематики
                        foreach (Test ttt in ActiveRecordBase<Test>.FindAll())
                        {
                            if (ttt.Title.CompareTo(testCaption) == 0)
                            {
                                MessageForm.ShowErrorMsg("Данный тест уже существует в тематике " + ttt.Method);
                                return;
                            }
                        }

                        //проверяем есть ли такие тематики
                        foreach (Method m in ActiveRecordBase<Method>.FindAll())
                        {
                            if (m.Title.CompareTo(methodCaption) == 0)
                            {
                                method = m;
                            }
                        }
                    }
                    Content c = null;
                    if (method == null)
                    {
                        //создаем тематику
                        c = new Content(methodDescription);
                        c.CreateAndFlush();
                        method = new Method(methodCaption, c);
                        method.CreateAndFlush();
                    }


                    //создание типа теста

                    TestType testType =
                        ActiveRecordBase<TestType>.FindOne(Expression.Eq("PlugInFileName", "TestingPlugInStandard"));
                    if (testType == null)
                    {
                        //создаем тип теста для стандартных тестов
                        testType = new TestType("Стандартные тесты", "TestingPlugInStandard", true);
                        testType.CreateAndFlush();
                    }

                    //создание опций
                    var ts = new TestSettings();
                    if (randomize == "СЛУЧАЙНЫЙ")
                    {
                        ts.Random = true;
                    }
                    //if (many == "ДА")
                    //{
                    //    ts.Many = true;
                    //}


                    //создание теста
                    c = new Content(testDescription);
                    c.SaveAndFlush();
                    string options = TestDetailsSerializer.Serialize(ts);
                    test = new Test(testType, method,
                                    testCaption,
                                    testTime, 80,5,
                                    c,
                                    options);


                    test.CreateAndFlush();
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                    MessageForm.ShowErrorMsg(
                        "Ошибка импорта теста.\n В таблице 'Общие сведения о тесте' некорректные данные.\n" +
                        "Дополнительная информация помещена в лог файл.");
                    return;
                }


                //}

                #endregion

                SetMessage("Добавление обязательных шкал и отчетов");

                #region добавление обязательных отчетов и шкал

                //using (new SessionScope())
                //{
                    foreach (Report rep in ActiveRecordBase<Report>.FindAll(Expression.Eq("ReportType", 1)))
                    {
                        test.Reports.Add(rep);
                    }

                    //добавление обязательных шкал
                    foreach (Scale sc in ActiveRecordBase<Scale>.FindAll())
                        if (sc.TypeScale < 2)
                        {
                            test.Scales.Add(sc);
                        }
                //}

                test.SaveAndFlush();

                #endregion

                SetMessage("Обработка таблицы 'Порядок оценки теста'");

                #region чтение информации об оценках теста(таблица 2) Порядок оценки теста

                try
                {
                    tbl = wm.WordDoc.Tables[2];
                    Log.Error("Чтение информации из таблицы 2 Порядок оценки теста строк " + tbl.Rows.Count);
                    float min = 0, max = 0;
                    int value = 0;
                    string description;
                    for (int i = 2; i < tbl.Rows.Count + 1; i++)
                    {
                        Row row = tbl.Rows[i];
                        str = GetTextFromCell(tbl.Cell(row.Index, 2).Range.Text);
                        Log.Error("оценка " + str);
                        value = int.Parse(str);
                        str = GetTextFromCell(tbl.Cell(row.Index, 3).Range.Text);
                        Log.Error("Мин процент = " + str);
                        min = float.Parse(str);
                        if (i < tbl.Rows.Count)
                        {
                            str = GetTextFromCell(tbl.Cell(row.Index + 1, 3).Range.Text);
                            Log.Error("Макс процент = " + str);
                            max = float.Parse(str);
                        }
                        else
                        {
                            max = 100;
                        }

                        description = GetTextFromCell(tbl.Cell(row.Index, 4).Range.Text);
                        foreach (Scale s in test.Scales)
                        {
                            var sten = new Sten(test, s, min, max, value, description);
                            sten.CreateAndFlush();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                    MessageForm.ShowErrorMsg(
                        "Ошибка импорта теста.\n В таблице 'Порядок оценки теста' некорректные данные.\n" +
                        "Дополнительная информация помещена в лог файл.");
                    return;
                }

                #endregion

                #region обновление информации в главном контроле

                try
                {
                    MethodInvoker methodDelegate = delegate
                                                       {
                                                           {
                                                               try
                                                               {
                                                                   InsertData();
                                                               }
                                                               catch (Exception ex)
                                                               {
                                                                   Log.Error(ex);
                                                               }
                                                           }
                                                           ;
                                                       };
                    BeginInvoke(methodDelegate);
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                }

                #endregion

                SetMessage("Обработка таблиц с вопросами");

                #region чтение информации о вопросах(таблица 3...n)

                try
                {
                    using ( new TransactionScope())
                    {
                        int questionCount = wm.WordDoc.Tables.Count - 2;
                        Content c = null;
                        object begin = null;
                        object end = null;
                        for (int i = 3; i < wm.WordDoc.Tables.Count + 1; i++)
                        {
                            tbl = wm.WordDoc.Tables[i];
                            Log.Error("Чтение информации таблица " + i + "из " + questionCount + " строк " +
                                    tbl.Rows.Count);
                            SetMessage(string.Format("Импорт вопроса № {0} из {1}", i - 2, questionCount));
                            //создание контента вопроса

                            //string questionStr = GetTextFromCell(tbl.Cell(1, 2).Range.Text);
                            r.Clear();
                            begin = tbl.Cell(1, 2).Range.Start;
                            end = tbl.Cell(1, 2).Range.End - 1;

                            Range range = wm.WordDoc.Range(ref begin, ref end);

                            bool good = false;
                            int failedIteration = 0;
                            while (!good)
                            {
                                try
                                {
                                    //вставка данных в RichTextBox
                                    Clipboard.Clear();
                                    range.Copy();
                                    r.Paste();
                                    Clipboard.Clear();
                                    good = true;
                                }
                                catch (Exception ex)
                                {
                                    Log.Error(ex);
                                    if (failedIteration > MaxIteration)
                                        break;
                                    failedIteration++;
                                }
                            }
                            //если есть картинка, то добавляем ее 
                            string pictureStr = GetTextFromCell(tbl.Cell(2, 2).Range.Text);

                            if (!string.IsNullOrEmpty(pictureStr))
                            {
                                r.AppendText("\r");
                                //вставляем картинку
                                try
                                {
                                    Image img = GraphicUtils.LoadImageFromFile(_filePath + pictureStr);
                                    r.InsertImage(img);
                                    img.Dispose();
                                }
                                catch (Exception ex)
                                {
                                    Log.Error(ex);
                                    MessageForm.ShowErrorMsg("Ошибка при обработке изображения " + _filePath +
                                                             pictureStr);
                                    throw;
                                }
                            }
                            //создание контента
                            using (var ms_ = new MemoryStream())
                            {
                                r.SaveFile(ms_, RichTextBoxStreamType.RichText);
                                if (ms_.Length > Consts.ContentMaxSize)
                                {
                                    throw new Exception("Размер контента вопроса составляет " + ms_.Length/1024 +
                                                        " Кбайт. \nУменьшите размер текста и изображений до " +
                                                        Consts.ContentMaxSize/1024 + " Кбайт.");
                                }
                                c = new Content(ms_.ToArray());
                                ms_.Close();
                            }
                            c.SaveAndFlush();
                            //создаю вопрос
                            var q = new Question(c, questionTime);
                            q.CreateAndFlush();
                            //прикрепляю вопрос к тесту
                            var tq = new TestQuestion(test, q, i - 2);
                            tq.CreateAndFlush();

                            //прохожу по каждой строке и считываю данные ответов
                            for (int j = 3; j < tbl.Rows.Count + 1; j++)
                            {
                                //считываю ответ
                                using (new TransactionScope())
                                {
                                    //считываю  признак правильности ответа
                                    string right = GetTextFromCell(tbl.Cell(j, 1).Range.Text).ToUpper();

                                    //str = GetTextFromCell(tbl.Cell(j, 2).Range.Text);
                                    r.Clear();

                                    begin = tbl.Cell(j, 2).Range.Start;
                                    end = tbl.Cell(j, 2).Range.End - 1;

                                    //вставка данных в RichTextBox
                                    good = false;
                                    failedIteration = 0;
                                    while (!good)
                                    {
                                        try
                                        {
                                            Clipboard.Clear();
                                            wm.WordDoc.Range(ref begin, ref end).Copy();
                                            r.Paste();
                                            Clipboard.Clear();
                                            good = true;
                                        }
                                        catch (Exception ex)
                                        {
                                            Log.Error(ex);
                                            if (failedIteration > MaxIteration)
                                                break;
                                            failedIteration++;
                                        }
                                    }
                                    using (var ms = new MemoryStream())
                                    {
                                        r.SaveFile(ms, RichTextBoxStreamType.RichText);
                                        c = new Content(ms.ToArray());
                                        if (ms.Length > Consts.ContentMaxSize)
                                        {
                                            throw new Exception("Размер контента ответа составляет " + ms.Length/1024 +
                                                                " Кбайт. \nУменьшите размер текста и изображений до " +
                                                                Consts.ContentMaxSize/1024 + " Кбайт.");
                                        }
                                        ms.Close();
                                    }
                                    c.SaveAndFlush();
                                    //создаю ответ
                                    var a = new Answer(c);
                                    a.CreateAndFlush();

                                    //привязка ответа к вопросу теста
                                    var qa = new QuestionAnswer(q, a, j - 2);
                                    qa.CreateAndFlush();

                                    //создание сырых баллов на ответ
                                    foreach (Scale s in test.Scales)
                                    {
                                        RawPoint coef = null;
                                        if (right == "ПРАВИЛЬНО")
                                        {
                                            coef = new RawPoint(test, s, q, a, 1);
                                            coef.CreateAndFlush();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                    MessageForm.ShowErrorMsg("Ошибка импорта теста.\n В таблице 'вопросы' некорректные данные.\n" +
                                             "Дополнительная информация помещена в лог файл.");
                    return;
                }

                #endregion
            }

            catch (Exception ex)
            {
                MessageForm.ShowErrorMsg(ex.Message);
                Log.Error(ex);
            }
            finally
            {
                //закрытие документа
                wm.KillWinWord();
                if (_threadpull.Count > 0)
                    _threadpull.RemoveAt(0);
                if (_threadpull.Count == 0 && _wc != null)
                {
                    _wc.CloseForm();
                    _wc = null;
                }
            }
        }

        /// <summary>
        /// экспорт теста из БД в файл
        /// </summary>
        private void Export()
        {
            var wm = new WordManager();
            Test test = null;
            var r = new ExRichTextBox();
            string savePath = _filePath + _fileName;
            Object beforeRow = Type.Missing;
            MemoryStream ms;

            try
            {
                test = _selectedTest;

                //создание экземпяра документа
                wm.New();
                using (TransactionScope root = new TransactionScope( ))
                {
                    TestQuestion[] questions = ActiveRecordBase<TestQuestion>.FindAll(new Order("QuestionOrder", true),
                                                                                      Expression.Eq("Test", test));

                    Scale scale = null;
                    //поиск шкалы результат
                    foreach (Scale s in test.Scales)
                    {
                        if (s.TypeScale == 1)
                        {
                            scale = s;
                            break;
                        }
                    }
                    if (scale == null)
                    {
                        MessageForm.ShowErrorMsg("Не определено ни одной шкалы");
                        return;
                    }
                    Sten[] stens = ActiveRecordBase<Sten>.FindAll(new Order("Value", true),
                                                                  Expression.Eq("Test", test),
                                                                  Expression.Eq("Scale", scale));


                    SetMessage(string.Format("Создание таблиц в документе (Длительность {0} мин.)",
                                             (questions.Length/600)));
                    //создание документа по шаблону
                    wm.CreateTemplateDocument(questions.Length, stens.Length);

                    #region запись в таблицу Общие сведения о тесте

                    SetMessage("Обработка таблицы 'Общие сведения о тесте'");
                    Table tbl = wm.WordDoc.Tables[1];
                    try
                    {
                        Log.Error("запись в таблицу 1. Общие сведения о тесте строк ");
                        tbl.Cell(1, 1).Range.Text = "Название теста";
                        tbl.Cell(1, 2).Range.Text = test.Title;
                        Log.Error("test.Title = " + test.Title);


                        tbl.Cell(2, 1).Range.Text = "Описание теста";
                        tbl.Cell(2, 2).Range.Text = GetContentText(test.Description.Id);
                        Log.Error("test.Description = " + GetContentText(test.Description.Id));

                        tbl.Cell(3, 1).Range.Text = "Картинка теста";
                        tbl.Cell(4, 1).Range.Text = "Тема теста";
                        tbl.Cell(4, 2).Range.Text = test.Method.Title;
                        Log.Error("test.Method.Title = " + test.Method.Title);


                        tbl.Cell(5, 1).Range.Text = "Описание темы";
                        tbl.Cell(5, 2).Range.Text = GetContentText(test.Method.Instruction.Id);
                        Log.Error("GetContentText(test.Method.Instruction.Id) = " +
                                GetContentText(test.Method.Instruction.Id));

                        tbl.Cell(6, 1).Range.Text = "Картинка темы";
                        tbl.Cell(7, 1).Range.Text = "Время на тест (мин.)";
                        if (test.TimeLimit != null)
                        {
                            tbl.Cell(7, 2).Range.Text = (test.TimeLimit/60).ToString();
                            Log.Error("test.TimeLimit.ToString() = " + (test.TimeLimit/60));
                        }
                        tbl.Cell(8, 1).Range.Text = "Количество вопросов";
                        tbl.Cell(8, 2).Range.Text = questions.Length.ToString();

                        tbl.Cell(9, 1).Range.Text = "Порядок вопросов";
                        TestSettings ts = TestSettings.GetSettings(test.Options);
                        if (ts.Random)
                        {
                            tbl.Cell(9, 2).Range.Text = "СЛУЧАЙНЫЙ";
                        }
                        else
                        {
                            tbl.Cell(9, 2).Range.Text = "Последовательный";
                        }

                        tbl.Cell(10, 1).Range.Text = "Тест готов";

                        tbl.Cell(11, 1).Range.Text = "Время вопросов";
                        tbl.Cell(12, 1).Range.Text = "Множественный выбор";

                        if (ts.Many)
                        {
                            tbl.Cell(12, 2).Range.Text = "Да";
                        }
                        else
                        {
                            tbl.Cell(12, 2).Range.Text = "Нет";
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex);
                        MessageForm.ShowErrorMsg(
                            "Ошибка экспорта теста.\n В таблице 'Общие сведения о тесте' некорректные данные.\n" +
                            "Дополнительная информация помещена в лог файл.");
                        throw;
                    }

                    #endregion

                    #region запись в таблицу Порядок оценки теста

                    SetMessage("Обработка таблицы 'Порядок оценки теста'");
                    tbl = wm.WordDoc.Tables[2];

                    try
                    {
                        Log.Error("запись в таблицу 2. Порядок оценки теста ");

                        //шапка
                        tbl.Cell(1, 1).Range.Text = "№";
                        tbl.Cell(1, 2).Range.Text = "Оценка";
                        tbl.Cell(1, 2).Range.Text = "Процент";
                        tbl.Cell(1, 2).Range.Text = "Описание оценки";
                        tbl.Cell(1, 2).Range.Text = "Картинка оценки";
                        int i = 2;
                        foreach (Sten sten in stens)
                        {
                            tbl.Cell(i, 1).Range.Text = (i - 1).ToString();
                            tbl.Cell(i, 2).Range.Text = sten.Value.ToString();
                            tbl.Cell(i, 3).Range.Text = sten.MinRawPoints.ToString();
                            tbl.Cell(i, 4).Range.Text = sten.Description;
                            i++;
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex);
                        MessageForm.ShowErrorMsg(
                            "Ошибка экспорта теста.\n В таблице 'Общие сведения о тесте' некорректные данные.\n" +
                            "Дополнительная информация помещена в лог файл.");
                        throw;
                    }

                    #endregion

                    SetMessage("Обработка таблиц с вопросами");

                    #region Обработка таблиц с вопросами

                    int questionCount = questions.Length;
                    int j = 3;
                    foreach (TestQuestion tq in questions)
                    {
                        string tempS = string.Format("Экспорт вопроса № {0} из {1}", j - 2, questionCount);
                        SetMessage(tempS);
                        Log.Error(tempS);

                        tbl = wm.WordDoc.Tables[j];

                        //загрузка контента вопроса
                        if (tq.Question.Content.PlainContent == null)
                        {
                            //форматированный текст
                            ms = new MemoryStream(tq.Question.Content.RichContent);
                            r.LoadFile(ms, RichTextBoxStreamType.RichText);
                        }
                        else
                        {
                            //обычный тукст
                            r.Text = tq.Question.Content.PlainContent;
                        }
                        //копирование
                        r.SelectAll();
                        bool good = false;
                        int failedIteration = 0;
                        while (!good)
                        {
                            try
                            {
                                //очистка буфера
                                Clipboard.Clear();
                                r.Copy();
                                tbl.Cell(1, 2).Range.Paste();
                                Clipboard.Clear();
                                good = true;
                            }
                            catch (Exception ex)
                            {
                                Log.Error(ex);
                                if (failedIteration > MaxIteration)
                                    break;
                                failedIteration++;
                            }
                        }
                        //вставка в word
                        tbl.Cell(1, 1).Range.Text = "Текст вопроса";
                        tbl.Cell(2, 1).Range.Text = "Картинка вопроса";


                        //экспорт ответа
                        QuestionAnswer[] questionAnswers;
                        using (TransactionScope child = new TransactionScope( TransactionMode.Inherits ))
                        {
                            //выборка из БД
                            questionAnswers =
                                ActiveRecordBase<QuestionAnswer>.FindAll(new Order("AnswerOrder", true),
                                                                         Expression.Eq("Question", tq.Question
                                                                             ));
                            int k = 3;
                            foreach (QuestionAnswer questionAnswer in questionAnswers)
                            {
                                //добавляем строку в таблицу документа word
                                tbl.Rows.Add(ref beforeRow);

                                //очищаем компонент ExRichTextBox
                                r.Clear();

                                //загрузка контента ответа
                                if (questionAnswer.Answer.Content.PlainContent == null)
                                {
                                    //форматированный текст
                                    ms = new MemoryStream(questionAnswer.Answer.Content.RichContent);
                                    r.LoadFile(ms, RichTextBoxStreamType.RichText);
                                }
                                else
                                {
                                    //простой текст
                                    r.Text = questionAnswer.Answer.Content.PlainContent;
                                }
                                r.SelectAll();

                                good = false;
                                failedIteration = 0;
                                while (!good)
                                {
                                    try
                                    {
                                        //очистка буфера
                                        Clipboard.Clear();
                                        r.Copy();
                                        //вставка в документ word
                                        tbl.Cell(k, 2).Range.Paste();
                                        Clipboard.Clear();
                                        good = true;
                                    }
                                    catch (Exception ex)
                                    {
                                        Log.Error(ex);
                                        if (failedIteration > MaxIteration)
                                            break;
                                        failedIteration++;
                                    }
                                }
                                //загрузка сырых баллов и проверка правильности ответа
                                var rawPoints = new List<RawPoint>();
                                //using (new TransactionScope())
                                //{
                                    //загрузка сырых баллов из БД
                                    rawPoints.AddRange(ActiveRecordBase<RawPoint>.FindAll(Expression.Eq("Test", test),
                                                                                          Expression.Eq("Question",
                                                                                                        tq.Question),
                                                                                          Expression.Eq("Answer",
                                                                                                        questionAnswer.
                                                                                                            Answer)));
                                    foreach (RawPoint rp in rawPoints)
                                    {
                                        //если есть хоть 1 положительный сырой балл
                                        //значит ответ будем считать правильным
                                        if (rp.Value > 0)
                                        {
                                            tbl.Cell(k, 1).Range.Text = "Правильно";
                                            break;
                                        }
                                    }
                                //}


                                k++;
                            }
                            child.VoteCommit();
                        }


                        j++;
                    }

                    #endregion
                }


                wm.SaveAs(savePath);

                return;
            }
            catch (Exception ex)
            {
                MessageForm.ShowErrorMsg(ex.Message);
                Log.Error(ex);
                return;
            }
            finally
            {
                //закрытие документа
                wm.KillWinWord();
                if (_threadpull.Count > 0)
                    _threadpull.RemoveAt(0);
                if (_threadpull.Count == 0 && _wc != null)
                {
                    _wc.CloseForm();
                    _wc = null;
                }
            }
        }

        #endregion

        #region обработчики событий от пользовательских контролов

        /// <summary>обработчик события нажатия на кнопку финиш
        /// в контроле создания изменения теста</summary>
        /// <param name="test"></param>
        private void createTest_ChangeTest(Test test)
        {
            Test _test;
            int i = 0;
            for (i = 0; i < listBoxControlTests.ItemCount; i++)
            {
                _test = (Test) listBoxControlTests.Items[i];
                if (_test.Id == test.Id)
                {
                    _test = test;
                    break;
                }
            }
            //var uc = new ChangeQuestions(test);
            //uc.InitControl();
            //uc.ShowInForm();
        }

        /// <summary>Обработчик события мастера на добавление теста</summary>
        /// <param name="test"></param>
        private void createTest_AddTest(Test test)
        {
            Test t = ActiveRecordBase<Test>.Find(test.Id);
            //t.Method.Tests.Add(t);
            _tests.Add(t);
            //t.Method.Update();
            FillTest();
            Event.Add(_user, EventTypes.EndEdit, t.Id);
        }

        /// <summary>Обработчик события добавление тематики</summary>
        /// <param name="method"></param>
        private void uc_AddMethod(Method method)
        {
            listBoxControlMethods.Items.Add(method);
            listBoxControlMethods.SelectedItem = method;
            _methods.Add(method);
        }

        /// <summary>
        /// обработчик события изменения методики
        /// </summary>
        /// <param name="method"></param>
        private void uc_ChangeMethod(Method method)
        {
            //int sel = listBoxControlMethods.SelectedIndex;
            //InsertData();
            //listBoxControlMethods.SelectedIndex = sel;
        }

        #endregion

        #region обработчики событий от стандартных контролов

        private void listBoxControlTests_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            CommonChangeTest();
        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            if (listBoxControlTests.SelectedIndex == -1)
            {
                MessageForm.ShowWarningMsg("Для редактирования необходимо выбрать тест");
                return;
            }
            var test = (Test) listBoxControlTests.SelectedItem;
            Event.Add(_user, EventTypes.BeginEdit, test.Id);
            if (test.Type.PlugInFileName == "FillingFormTest")
            {
                //SessionScope.Current.Flush();
                RunnerApplication.JournalManager.EditTestQuestions(test);
                //using (new SessionScope(FlushAction.Never))
                //{
                //    test = Test.Find(test.Id);
                //}
            }
            else
            {
                var uc = new ChangeQuestions(test);
                uc.InitControl();
                uc.ShowInForm();
            }
            Event.Add(_user, EventTypes.EndEdit, test.Id);
        }

        private void simpleButtonDeleteTest_Click(object sender, EventArgs e)
        {
            if (listBoxControlTests.SelectedIndex == -1)
            {
                MessageForm.ShowWarningMsg("Для удаления необходимо выбрать тест из списка");
                return;
            }

            if (MessageForm.ShowYesNoQuestionMsg("Вы уверены, что хотите удалить тест из БД?")
                == DialogResult.No)
            {
                return;
            }
            var test = (Test) listBoxControlTests.SelectedItem;

            DeleteTest(true, test);
            _tests.Clear();

            _tests.AddRange(ActiveRecordBase<Test>.FindAll());

            FillTest();
        }

        private void simpleButtonAddMethod_Click(object sender, EventArgs e)
        {
            AddMethod();
            if (SessionScope.Current != null)
                SessionScope.Current.Flush( );
        }

        private void simpleButtonAdd_Click(object sender, EventArgs e)
        {
            AddTest();
            if (SessionScope.Current != null)
                SessionScope.Current.Flush( );
        }

        private void listBoxControlMethods_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillTest();
        }

        private void simpleButtonChangeTest_Click(object sender, EventArgs e)
        {
            CommonChangeTest();
        }

        private void simpeButtonChangeMethod_Click(object sender, EventArgs e)
        {
            if (listBoxControlMethods.SelectedIndex == -1)
            {
                MessageForm.ShowWarningMsg("Для редактирования тематики необходимо выбрать ее в списке");
                return;
            }
            ChangeMethod((Method) listBoxControlMethods.SelectedItem);
        }

        private void listBoxControlMethods_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (listBoxControlMethods.SelectedIndex == -1)
            {
                MessageForm.ShowWarningMsg("Для редактирования тематики необходимо выбрать ее в списке");
                return;
            }
            ChangeMethod((Method) listBoxControlMethods.SelectedItem);
        }

        private void simpleButtonDeleteMethod_Click(object sender, EventArgs e)
        {
            if (listBoxControlMethods.SelectedIndex == -1)
            {
                MessageForm.ShowWarningMsg("Для удаления тематики необходимо выбрать ее в списке");
                return;
            }
            var m = (Method) listBoxControlMethods.SelectedItem;


            DeleteMethod(m);
        }

        private void simpleButtonMethodControlQuestion_Click(object sender, EventArgs e)
        {
            if (listBoxControlMethods.SelectedIndex == -1)
            {
                MessageForm.ShowWarningMsg("Для редактирования контрольных вопросов необходимо выбрать ее в списке");
                return;
            }
            var uc = new ChangeControlQuestions((Method) listBoxControlMethods.SelectedItem);
            uc.InitControl();
            uc.ShowInForm();
        }


        private void simpleImport_Click(object sender, EventArgs e)
        {
            var ofd = new OpenFileDialog();
            ofd.Filter = @"Документ Microsoft Word |*.doc";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                string path = ofd.FileName;
                int index = path.LastIndexOf("\\");
                _filePath = path.Substring(0, index + 1);
                _fileName = path.Remove(0, index + 1);
            }
            else
            {
                return;
            }
            if (_wc == null)
            {
                _wc = new WaitingForm("Старт импорта теста", false, false);
                _wc.Show(this);
            }
            _threadpull.Add(1);
            Import();
            //System.Threading.Thread t = new System.Threading.Thread(Import);
            //t.Start();
            //_threadpull.Add(t.ManagedThreadId);
        }


        private void simpleExport_Click(object sender, EventArgs e)
        {
            if (listBoxControlTests.SelectedIndex == -1)
            {
                MessageForm.ShowWarningMsg("Для экспорта необходимо выбрать тест");
                _selectedTest = null;
                return;
            }
            _selectedTest = (Test) listBoxControlTests.SelectedItem;


            var sfd = new SaveFileDialog();
            sfd.Filter = "Документ Microsoft Word |*.doc";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                string path = sfd.FileName;
                int index = path.LastIndexOf("\\");
                _filePath = path.Substring(0, index + 1);
                _fileName = path.Remove(0, index + 1);
            }
            else
            {
                return;
            }
            if (_wc == null)
            {
                _wc = new WaitingForm("Старт экспорта теста", false, false);
                _wc.Show(this);
            }
            _threadpull.Add(1);
            Export();
            //System.Threading.Thread t = new System.Threading.Thread(Export);
            //t.Start();
            //_threadpull.Add(t.ManagedThreadId);
        }


        public override void ParentFormFormClosing(object sender, FormClosingEventArgs e)
        {
            if (_threadpull.Count > 0)
            {
                MessageForm.ShowErrorMsg("Выполняется процесс. Попробуйте позже");
                e.Cancel = true;
                return;
            }
            base.ParentFormFormClosing(sender, e);
        }

        #endregion
    }
}
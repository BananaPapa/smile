﻿using Eureca.Professional.TestingPlugInStandard.Components;

namespace Eureca.Professional.TestingPlugInStandard.Controls.CreateTest
{
    partial class ChangeQuestions
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainerControlNavigation = new DevExpress.XtraEditors.SplitContainerControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.simpleButtonAddQuestion = new DevExpress.XtraEditors.SimpleButton();
            this.spinEditQuestionNumber = new DevExpress.XtraEditors.SpinEdit();
            this.simpleButtonDeleteQuestion = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonInsertQuestion = new DevExpress.XtraEditors.SimpleButton();
            this.listBoxControlQuestions = new DevExpress.XtraEditors.ListBoxControl();
            this.splitContainerControlQuestion = new DevExpress.XtraEditors.SplitContainerControl();
            this.checkEditNoTime = new DevExpress.XtraEditors.CheckEdit();
            this.timeEditTestLimit = new DevExpress.XtraEditors.TimeEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.radioGroupFormat = new DevExpress.XtraEditors.RadioGroup();
            this.exRichTextBoxQuestion = new Eureca.Professional.TestingPlugInStandard.Components.ExRichTextBox();
            this.contextMenuStripExRich = new System.Windows.Forms.ContextMenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButtonSaveQuestion = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.simpleButtonChangeAnswer = new DevExpress.XtraEditors.SimpleButton();
            this.spinEditAnswerNumber = new DevExpress.XtraEditors.SpinEdit();
            this.simpleButtonInsertAnswer = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonDeleteAnswer = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonAddAnswer = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControlNavigation)).BeginInit();
            this.splitContainerControlNavigation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditQuestionNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxControlQuestions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControlQuestion)).BeginInit();
            this.splitContainerControlQuestion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditNoTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeEditTestLimit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupFormat.Properties)).BeginInit();
            this.contextMenuStripExRich.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditAnswerNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainerControlNavigation
            // 
            this.splitContainerControlNavigation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControlNavigation.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControlNavigation.Name = "splitContainerControlNavigation";
            this.splitContainerControlNavigation.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.splitContainerControlNavigation.Panel1.Controls.Add(this.groupControl2);
            this.splitContainerControlNavigation.Panel1.Controls.Add(this.listBoxControlQuestions);
            this.splitContainerControlNavigation.Panel1.MinSize = 150;
            this.splitContainerControlNavigation.Panel1.ShowCaption = true;
            this.splitContainerControlNavigation.Panel1.Text = "Список вопросов";
            this.splitContainerControlNavigation.Panel2.Controls.Add(this.splitContainerControlQuestion);
            this.splitContainerControlNavigation.Panel2.MinSize = 400;
            this.splitContainerControlNavigation.Panel2.ShowCaption = true;
            this.splitContainerControlNavigation.Panel2.Text = "Вопрос";
            this.splitContainerControlNavigation.Size = new System.Drawing.Size(832, 703);
            this.splitContainerControlNavigation.SplitterPosition = 190;
            this.splitContainerControlNavigation.TabIndex = 1;
            this.splitContainerControlNavigation.Text = "splitContainerControl1";
            // 
            // groupControl2
            // 
            this.groupControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl2.Controls.Add(this.simpleButtonAddQuestion);
            this.groupControl2.Controls.Add(this.spinEditQuestionNumber);
            this.groupControl2.Controls.Add(this.simpleButtonDeleteQuestion);
            this.groupControl2.Controls.Add(this.simpleButtonInsertQuestion);
            this.groupControl2.Location = new System.Drawing.Point(1, 602);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(185, 78);
            this.groupControl2.TabIndex = 5;
            this.groupControl2.Text = "Управление вопросами";
            // 
            // simpleButtonAddQuestion
            // 
            this.simpleButtonAddQuestion.Location = new System.Drawing.Point(5, 22);
            this.simpleButtonAddQuestion.Name = "simpleButtonAddQuestion";
            this.simpleButtonAddQuestion.Size = new System.Drawing.Size(71, 23);
            this.simpleButtonAddQuestion.TabIndex = 1;
            this.simpleButtonAddQuestion.Text = "Добавить";
            this.simpleButtonAddQuestion.Click += new System.EventHandler(this.simpleButtonAddQuestion_Click);
            // 
            // spinEditQuestionNumber
            // 
            this.spinEditQuestionNumber.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEditQuestionNumber.Location = new System.Drawing.Point(83, 46);
            this.spinEditQuestionNumber.Name = "spinEditQuestionNumber";
            this.spinEditQuestionNumber.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEditQuestionNumber.Properties.IsFloatValue = false;
            this.spinEditQuestionNumber.Properties.Mask.EditMask = "N00";
            this.spinEditQuestionNumber.Properties.MaxValue = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.spinEditQuestionNumber.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEditQuestionNumber.Size = new System.Drawing.Size(68, 20);
            this.spinEditQuestionNumber.TabIndex = 4;
            // 
            // simpleButtonDeleteQuestion
            // 
            this.simpleButtonDeleteQuestion.Location = new System.Drawing.Point(82, 22);
            this.simpleButtonDeleteQuestion.Name = "simpleButtonDeleteQuestion";
            this.simpleButtonDeleteQuestion.Size = new System.Drawing.Size(69, 23);
            this.simpleButtonDeleteQuestion.TabIndex = 2;
            this.simpleButtonDeleteQuestion.Text = "Удалить";
            this.simpleButtonDeleteQuestion.Click += new System.EventHandler(this.simpleButtonDeleteQuestion_Click);
            // 
            // simpleButtonInsertQuestion
            // 
            this.simpleButtonInsertQuestion.Location = new System.Drawing.Point(6, 46);
            this.simpleButtonInsertQuestion.Name = "simpleButtonInsertQuestion";
            this.simpleButtonInsertQuestion.Size = new System.Drawing.Size(70, 23);
            this.simpleButtonInsertQuestion.TabIndex = 3;
            this.simpleButtonInsertQuestion.Text = "Вставить";
            this.simpleButtonInsertQuestion.Click += new System.EventHandler(this.simpleButtonInsertQuestion_Click);
            // 
            // listBoxControlQuestions
            // 
            this.listBoxControlQuestions.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listBoxControlQuestions.HorizontalScrollbar = true;
            this.listBoxControlQuestions.Location = new System.Drawing.Point(0, 0);
            this.listBoxControlQuestions.Name = "listBoxControlQuestions";
            this.listBoxControlQuestions.Size = new System.Drawing.Size(186, 596);
            this.listBoxControlQuestions.TabIndex = 0;
            this.listBoxControlQuestions.SelectedIndexChanged += new System.EventHandler(this.listBoxControlQuestions_SelectedIndexChanged);
            // 
            // splitContainerControlQuestion
            // 
            this.splitContainerControlQuestion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControlQuestion.Horizontal = false;
            this.splitContainerControlQuestion.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControlQuestion.Name = "splitContainerControlQuestion";
            this.splitContainerControlQuestion.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.splitContainerControlQuestion.Panel1.Controls.Add(this.checkEditNoTime);
            this.splitContainerControlQuestion.Panel1.Controls.Add(this.timeEditTestLimit);
            this.splitContainerControlQuestion.Panel1.Controls.Add(this.labelControl1);
            this.splitContainerControlQuestion.Panel1.Controls.Add(this.radioGroupFormat);
            this.splitContainerControlQuestion.Panel1.Controls.Add(this.exRichTextBoxQuestion);
            this.splitContainerControlQuestion.Panel1.Controls.Add(this.simpleButton5);
            this.splitContainerControlQuestion.Panel1.Controls.Add(this.labelControl2);
            this.splitContainerControlQuestion.Panel1.Controls.Add(this.simpleButtonSaveQuestion);
            this.splitContainerControlQuestion.Panel1.MinSize = 200;
            this.splitContainerControlQuestion.Panel1.ShowCaption = true;
            this.splitContainerControlQuestion.Panel1.Text = "Вопрос";
            this.splitContainerControlQuestion.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.splitContainerControlQuestion.Panel2.Controls.Add(this.groupControl3);
            this.splitContainerControlQuestion.Panel2.Controls.Add(this.groupControl1);
            this.splitContainerControlQuestion.Panel2.MinSize = 200;
            this.splitContainerControlQuestion.Panel2.ShowCaption = true;
            this.splitContainerControlQuestion.Panel2.Text = "Ответы";
            this.splitContainerControlQuestion.Size = new System.Drawing.Size(636, 703);
            this.splitContainerControlQuestion.SplitterPosition = 322;
            this.splitContainerControlQuestion.TabIndex = 0;
            this.splitContainerControlQuestion.Text = "splitContainerControl2";
            // 
            // checkEditNoTime
            // 
            this.checkEditNoTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.checkEditNoTime.EditValue = true;
            this.checkEditNoTime.Location = new System.Drawing.Point(296, 271);
            this.checkEditNoTime.Name = "checkEditNoTime";
            this.checkEditNoTime.Properties.Caption = "нет ограничения";
            this.checkEditNoTime.Size = new System.Drawing.Size(108, 19);
            this.checkEditNoTime.TabIndex = 14;
            this.checkEditNoTime.CheckedChanged += new System.EventHandler(this.checkEditNoTime_CheckedChanged);
            // 
            // timeEditTestLimit
            // 
            this.timeEditTestLimit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.timeEditTestLimit.EditValue = new System.DateTime(2009, 4, 10, 0, 0, 0, 0);
            this.timeEditTestLimit.Enabled = false;
            this.timeEditTestLimit.Location = new System.Drawing.Point(410, 270);
            this.timeEditTestLimit.Name = "timeEditTestLimit";
            this.timeEditTestLimit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.timeEditTestLimit.Size = new System.Drawing.Size(100, 20);
            this.timeEditTestLimit.TabIndex = 13;
            this.timeEditTestLimit.EditValueChanged += new System.EventHandler(this.timeEditTestLimit_EditValueChanged);
            // 
            // labelControl1
            // 
            this.labelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl1.Location = new System.Drawing.Point(18, 274);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(29, 13);
            this.labelControl1.TabIndex = 12;
            this.labelControl1.Text = "Текст";
            this.labelControl1.Click += new System.EventHandler(this.labelControl1_Click);
            // 
            // radioGroupFormat
            // 
            this.radioGroupFormat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radioGroupFormat.Location = new System.Drawing.Point(53, 269);
            this.radioGroupFormat.Name = "radioGroupFormat";
            this.radioGroupFormat.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Простой"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Форматированный")});
            this.radioGroupFormat.Size = new System.Drawing.Size(237, 23);
            this.radioGroupFormat.TabIndex = 11;
            this.radioGroupFormat.SelectedIndexChanged += new System.EventHandler(this.radioGroupFormat_SelectedIndexChanged);
            // 
            // exRichTextBoxQuestion
            // 
            this.exRichTextBoxQuestion.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.exRichTextBoxQuestion.ContextMenuStrip = this.contextMenuStripExRich;
            this.exRichTextBoxQuestion.EnableAutoDragDrop = true;
            this.exRichTextBoxQuestion.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.exRichTextBoxQuestion.ForeColor = System.Drawing.Color.Green;
            this.exRichTextBoxQuestion.HiglightColor = Eureca.Professional.TestingPlugInStandard.Components.RtfColor.White;
            this.exRichTextBoxQuestion.Location = new System.Drawing.Point(12, 29);
            this.exRichTextBoxQuestion.Name = "exRichTextBoxQuestion";
            this.exRichTextBoxQuestion.Size = new System.Drawing.Size(612, 234);
            this.exRichTextBoxQuestion.TabIndex = 10;
            this.exRichTextBoxQuestion.Text = "";
            this.exRichTextBoxQuestion.TextColor = Eureca.Professional.TestingPlugInStandard.Components.RtfColor.Black;
            this.exRichTextBoxQuestion.TextChanged += new System.EventHandler(this.exRichTextBoxQuestion_TextChanged);
            // 
            // contextMenuStripExRich
            // 
            this.contextMenuStripExRich.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1});
            this.contextMenuStripExRich.Name = "contextMenuStripExRich";
            this.contextMenuStripExRich.Size = new System.Drawing.Size(123, 26);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(122, 22);
            this.toolStripMenuItem1.Text = "Вставить";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // simpleButton5
            // 
            this.simpleButton5.Location = new System.Drawing.Point(589, 1);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(28, 23);
            this.simpleButton5.TabIndex = 7;
            this.simpleButton5.Text = "...";
            this.simpleButton5.Visible = false;
            this.simpleButton5.Click += new System.EventHandler(this.simpleButton5_Click);
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(463, 6);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(120, 13);
            this.labelControl2.TabIndex = 7;
            this.labelControl2.Text = "Добавить изображение";
            this.labelControl2.Visible = false;
            // 
            // simpleButtonSaveQuestion
            // 
            this.simpleButtonSaveQuestion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.simpleButtonSaveQuestion.Location = new System.Drawing.Point(542, 269);
            this.simpleButtonSaveQuestion.Name = "simpleButtonSaveQuestion";
            this.simpleButtonSaveQuestion.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonSaveQuestion.TabIndex = 9;
            this.simpleButtonSaveQuestion.Text = "Сохранить";
            this.simpleButtonSaveQuestion.ToolTip = "Сохранение вопроса в БД";
            this.simpleButtonSaveQuestion.Click += new System.EventHandler(this.simpleButtonSaveQuestion_Click);
            // 
            // groupControl3
            // 
            this.groupControl3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl3.Controls.Add(this.simpleButtonChangeAnswer);
            this.groupControl3.Controls.Add(this.spinEditAnswerNumber);
            this.groupControl3.Controls.Add(this.simpleButtonInsertAnswer);
            this.groupControl3.Controls.Add(this.simpleButtonDeleteAnswer);
            this.groupControl3.Controls.Add(this.simpleButtonAddAnswer);
            this.groupControl3.Location = new System.Drawing.Point(3, 300);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(628, 53);
            this.groupControl3.TabIndex = 10;
            this.groupControl3.Text = "Управление ответами";
            // 
            // simpleButtonChangeAnswer
            // 
            this.simpleButtonChangeAnswer.Location = new System.Drawing.Point(84, 25);
            this.simpleButtonChangeAnswer.Name = "simpleButtonChangeAnswer";
            this.simpleButtonChangeAnswer.Size = new System.Drawing.Size(71, 23);
            this.simpleButtonChangeAnswer.TabIndex = 11;
            this.simpleButtonChangeAnswer.Text = "Изменить";
            this.simpleButtonChangeAnswer.Click += new System.EventHandler(this.simpleButtonChangeAnswer_Click);
            // 
            // spinEditAnswerNumber
            // 
            this.spinEditAnswerNumber.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEditAnswerNumber.Location = new System.Drawing.Point(311, 25);
            this.spinEditAnswerNumber.Name = "spinEditAnswerNumber";
            this.spinEditAnswerNumber.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEditAnswerNumber.Properties.IsFloatValue = false;
            this.spinEditAnswerNumber.Properties.Mask.EditMask = "N00";
            this.spinEditAnswerNumber.Properties.MaxValue = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.spinEditAnswerNumber.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEditAnswerNumber.Size = new System.Drawing.Size(68, 20);
            this.spinEditAnswerNumber.TabIndex = 14;
            // 
            // simpleButtonInsertAnswer
            // 
            this.simpleButtonInsertAnswer.Location = new System.Drawing.Point(236, 25);
            this.simpleButtonInsertAnswer.Name = "simpleButtonInsertAnswer";
            this.simpleButtonInsertAnswer.Size = new System.Drawing.Size(70, 23);
            this.simpleButtonInsertAnswer.TabIndex = 13;
            this.simpleButtonInsertAnswer.Text = "Вставить";
            this.simpleButtonInsertAnswer.Click += new System.EventHandler(this.simpleButtonInsertAnswer_Click);
            // 
            // simpleButtonDeleteAnswer
            // 
            this.simpleButtonDeleteAnswer.Location = new System.Drawing.Point(161, 25);
            this.simpleButtonDeleteAnswer.Name = "simpleButtonDeleteAnswer";
            this.simpleButtonDeleteAnswer.Size = new System.Drawing.Size(69, 23);
            this.simpleButtonDeleteAnswer.TabIndex = 12;
            this.simpleButtonDeleteAnswer.Text = "Удалить";
            this.simpleButtonDeleteAnswer.Click += new System.EventHandler(this.simpleButtonDeleteAnswer_Click);
            // 
            // simpleButtonAddAnswer
            // 
            this.simpleButtonAddAnswer.Location = new System.Drawing.Point(7, 25);
            this.simpleButtonAddAnswer.Name = "simpleButtonAddAnswer";
            this.simpleButtonAddAnswer.Size = new System.Drawing.Size(71, 23);
            this.simpleButtonAddAnswer.TabIndex = 10;
            this.simpleButtonAddAnswer.Text = "Добавить";
            this.simpleButtonAddAnswer.Click += new System.EventHandler(this.simpleButtonAddAnswer_Click);
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.tableLayoutPanel1);
            this.groupControl1.Location = new System.Drawing.Point(1, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.ShowCaption = false;
            this.groupControl1.Size = new System.Drawing.Size(632, 294);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "groupControl1";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(2, 2);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(628, 290);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // ChangeQuestions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainerControlNavigation);
            this.MinimumSize = new System.Drawing.Size(805, 599);
            this.Name = "ChangeQuestions";
            this.Size = new System.Drawing.Size(832, 703);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControlNavigation)).EndInit();
            this.splitContainerControlNavigation.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spinEditQuestionNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxControlQuestions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControlQuestion)).EndInit();
            this.splitContainerControlQuestion.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEditNoTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeEditTestLimit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupFormat.Properties)).EndInit();
            this.contextMenuStripExRich.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spinEditAnswerNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControlNavigation;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.SimpleButton simpleButtonAddQuestion;
        private DevExpress.XtraEditors.SpinEdit spinEditQuestionNumber;
        private DevExpress.XtraEditors.SimpleButton simpleButtonDeleteQuestion;
        private DevExpress.XtraEditors.SimpleButton simpleButtonInsertQuestion;
        private DevExpress.XtraEditors.ListBoxControl listBoxControlQuestions;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControlQuestion;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.SimpleButton simpleButtonSaveQuestion;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.SimpleButton simpleButtonChangeAnswer;
        private DevExpress.XtraEditors.SpinEdit spinEditAnswerNumber;
        private DevExpress.XtraEditors.SimpleButton simpleButtonInsertAnswer;
        private DevExpress.XtraEditors.SimpleButton simpleButtonDeleteAnswer;
        private DevExpress.XtraEditors.SimpleButton simpleButtonAddAnswer;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private ExRichTextBox exRichTextBoxQuestion;
        private DevExpress.XtraEditors.RadioGroup radioGroupFormat;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.CheckEdit checkEditNoTime;
        private DevExpress.XtraEditors.TimeEdit timeEditTestLimit;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripExRich;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
    }
}

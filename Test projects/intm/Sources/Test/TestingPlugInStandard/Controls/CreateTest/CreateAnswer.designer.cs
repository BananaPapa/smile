﻿using Eureca.HumanTestingSystem.TestingPlugInStandard.Controls.CreateTest;

namespace Eureca.Professional.TestingPlugInStandard.Controls.CreateTest
{
    partial class CreateAnswer
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if (disposing && ( components != null ))
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            this.getAnswer1 = new Eureca.Professional.TestingPlugInStandard.Controls.CreateTest.GetAnswer();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.simpleButtonOk = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageName = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabPageCoefficients = new DevExpress.XtraTab.XtraTabPage();
            this.simpleButtonDropmany = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonToMany = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonDropOne = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonToOne = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.listBoxControlSelectedScales = new DevExpress.XtraEditors.ListBoxControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPageName.SuspendLayout();
            this.xtraTabPageCoefficients.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxControlSelectedScales)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // getAnswer1
            // 
            this.getAnswer1.AcceptButton = null;
            this.getAnswer1.CancelButton = null;
            this.getAnswer1.CaptionControl = "Основной контрол тестирования";
            this.getAnswer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.getAnswer1.ExitQuestion = null;
            this.getAnswer1.Location = new System.Drawing.Point(2, 2);
            this.getAnswer1.MinimumSize = new System.Drawing.Size(240, 264);
            this.getAnswer1.Name = "getAnswer1";
            this.getAnswer1.Size = new System.Drawing.Size(678, 418);
            this.getAnswer1.TabIndex = 0;
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.getAnswer1);
            this.groupControl1.Location = new System.Drawing.Point(3, 3);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.ShowCaption = false;
            this.groupControl1.Size = new System.Drawing.Size(682, 422);
            this.groupControl1.TabIndex = 1;
            this.groupControl1.Text = "Выбранный ответ";
            // 
            // simpleButtonOk
            // 
            this.simpleButtonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonOk.Location = new System.Drawing.Point(543, 463);
            this.simpleButtonOk.Name = "simpleButtonOk";
            this.simpleButtonOk.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonOk.TabIndex = 11;
            this.simpleButtonOk.Text = "Ок";
            this.simpleButtonOk.Click += new System.EventHandler(this.simpleButtonOk_Click);
            // 
            // simpleButtonCancel
            // 
            this.simpleButtonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonCancel.Location = new System.Drawing.Point(624, 463);
            this.simpleButtonCancel.Name = "simpleButtonCancel";
            this.simpleButtonCancel.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonCancel.TabIndex = 12;
            this.simpleButtonCancel.Text = "Отмена";
            this.simpleButtonCancel.Click += new System.EventHandler(this.simpleButtonCancel_Click);
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.xtraTabControl1.Location = new System.Drawing.Point(3, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPageName;
            this.xtraTabControl1.Size = new System.Drawing.Size(696, 457);
            this.xtraTabControl1.TabIndex = 14;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageName,
            this.xtraTabPageCoefficients});
            // 
            // xtraTabPageName
            // 
            this.xtraTabPageName.Controls.Add(this.groupControl1);
            this.xtraTabPageName.Name = "xtraTabPageName";
            this.xtraTabPageName.Size = new System.Drawing.Size(691, 431);
            this.xtraTabPageName.Text = "Наименование ответа";
            // 
            // xtraTabPageCoefficients
            // 
            this.xtraTabPageCoefficients.Controls.Add(this.simpleButtonDropmany);
            this.xtraTabPageCoefficients.Controls.Add(this.simpleButtonToMany);
            this.xtraTabPageCoefficients.Controls.Add(this.simpleButtonDropOne);
            this.xtraTabPageCoefficients.Controls.Add(this.simpleButtonToOne);
            this.xtraTabPageCoefficients.Controls.Add(this.labelControl1);
            this.xtraTabPageCoefficients.Controls.Add(this.listBoxControlSelectedScales);
            this.xtraTabPageCoefficients.Controls.Add(this.gridControl1);
            this.xtraTabPageCoefficients.Controls.Add(this.labelControl11);
            this.xtraTabPageCoefficients.Name = "xtraTabPageCoefficients";
            this.xtraTabPageCoefficients.Size = new System.Drawing.Size(690, 429);
            this.xtraTabPageCoefficients.Text = "Сырые баллы";
            // 
            // simpleButtonDropmany
            // 
            this.simpleButtonDropmany.Location = new System.Drawing.Point(358, 178);
            this.simpleButtonDropmany.Name = "simpleButtonDropmany";
            this.simpleButtonDropmany.Size = new System.Drawing.Size(43, 23);
            this.simpleButtonDropmany.TabIndex = 5;
            this.simpleButtonDropmany.Text = ">>";
            this.simpleButtonDropmany.Click += new System.EventHandler(this.simpleButtonDropmany_Click);
            // 
            // simpleButtonToMany
            // 
            this.simpleButtonToMany.Location = new System.Drawing.Point(358, 149);
            this.simpleButtonToMany.Name = "simpleButtonToMany";
            this.simpleButtonToMany.Size = new System.Drawing.Size(43, 23);
            this.simpleButtonToMany.TabIndex = 4;
            this.simpleButtonToMany.Text = "<<";
            this.simpleButtonToMany.Click += new System.EventHandler(this.simpleButtonToMany_Click);
            // 
            // simpleButtonDropOne
            // 
            this.simpleButtonDropOne.Location = new System.Drawing.Point(358, 96);
            this.simpleButtonDropOne.Name = "simpleButtonDropOne";
            this.simpleButtonDropOne.Size = new System.Drawing.Size(43, 23);
            this.simpleButtonDropOne.TabIndex = 3;
            this.simpleButtonDropOne.Text = ">";
            this.simpleButtonDropOne.Click += new System.EventHandler(this.simpleButtonDropOne_Click);
            // 
            // simpleButtonToOne
            // 
            this.simpleButtonToOne.Location = new System.Drawing.Point(358, 67);
            this.simpleButtonToOne.Name = "simpleButtonToOne";
            this.simpleButtonToOne.Size = new System.Drawing.Size(43, 23);
            this.simpleButtonToOne.TabIndex = 2;
            this.simpleButtonToOne.Text = "<";
            this.simpleButtonToOne.Click += new System.EventHandler(this.simpleButtonToOne_Click);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(421, 3);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(172, 13);
            this.labelControl1.TabIndex = 9;
            this.labelControl1.Text = "Список  шкал без коэффициентов";
            // 
            // listBoxControlSelectedScales
            // 
            this.listBoxControlSelectedScales.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listBoxControlSelectedScales.HorizontalScrollbar = true;
            this.listBoxControlSelectedScales.Location = new System.Drawing.Point(421, 22);
            this.listBoxControlSelectedScales.Name = "listBoxControlSelectedScales";
            this.listBoxControlSelectedScales.Size = new System.Drawing.Size(281, 388);
            this.listBoxControlSelectedScales.SortOrder = System.Windows.Forms.SortOrder.Ascending;
            this.listBoxControlSelectedScales.TabIndex = 1;
            // 
            // gridControl1
            // 
            this.gridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.gridControl1.Location = new System.Drawing.Point(3, 22);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(334, 388);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsCustomization.AllowFilter = false;
            this.gridView1.OptionsCustomization.AllowGroup = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Шкала";
            this.gridColumn1.FieldName = "ScaleName";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 246;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Коэффициент";
            this.gridColumn2.FieldName = "Coefficient";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 96;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "gridColumn3";
            this.gridColumn3.FieldName = "ScaleId";
            this.gridColumn3.Name = "gridColumn3";
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(3, 3);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(163, 13);
            this.labelControl11.TabIndex = 6;
            this.labelControl11.Text = "Список шкал с коэффициентами";
            // 
            // CreateAnswer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.xtraTabControl1);
            this.Controls.Add(this.simpleButtonCancel);
            this.Controls.Add(this.simpleButtonOk);
            this.MinimumSize = new System.Drawing.Size(702, 493);
            this.Name = "CreateAnswer";
            this.Size = new System.Drawing.Size(702, 493);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPageName.ResumeLayout(false);
            this.xtraTabPageCoefficients.ResumeLayout(false);
            this.xtraTabPageCoefficients.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxControlSelectedScales)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private GetAnswer getAnswer1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButtonOk;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCancel;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageName;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageCoefficients;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.ListBoxControl listBoxControlSelectedScales;
        private DevExpress.XtraEditors.SimpleButton simpleButtonDropmany;
        private DevExpress.XtraEditors.SimpleButton simpleButtonToMany;
        private DevExpress.XtraEditors.SimpleButton simpleButtonDropOne;
        private DevExpress.XtraEditors.SimpleButton simpleButtonToOne;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
    }
}
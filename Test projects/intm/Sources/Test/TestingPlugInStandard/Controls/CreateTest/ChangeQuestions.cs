﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Castle.ActiveRecord;
using DevExpress.Utils.Drawing.Helpers;
using DevExpress.XtraBars.Docking2010.Views.Widget;
using Eureca.Professional.ActiveRecords;
using Eureca.Professional.CommonClasses;
using NHibernate.Criterion;

namespace Eureca.Professional.TestingPlugInStandard.Controls.CreateTest
{
    /// <summary>
    /// контрол конфигурирования вопросов теста
    /// </summary>
    public partial class ChangeQuestions : BaseTestingControl
    {
        #region поля

        /// <summary>Список всех ответов в тесте</summary>
        private readonly List<Answer> _allAnswers = new List<Answer>();

        private readonly List<TestQuestion> _questions = new List<TestQuestion>();
        private readonly Test _test;
        private readonly List<HorisontalAnswer> _uControls = new List<HorisontalAnswer>();
        private bool _change;
        private HorisontalAnswer _currentAnswer;
        private TestQuestion _currentQuestion;
        //private bool _plain = true;
        private TestAnswer[] _testAnswers;

        #endregion

        #region конструктор

        public ChangeQuestions(Test test)
        {
            _test = test;
            InitializeComponent();
        }

        #endregion

        #region методы

        public override void InitControl()
        {
            base.InitControl();
            SetDefault();
            InsertData();
        }

        private void SetDefault()
        {
            CaptionControl = "Редактирование вопросов";
        }

        private void InsertData()
        {
            //загрузка всех ответов
            //_allAnswers.AddRange(ActiveRecordBase<Answer>.FindAll());
            //загрузка тестовых ответов
            _testAnswers = ActiveRecordBase<TestAnswer>.FindAll(new Order("AnswerOrder", true),
                Expression.Eq("Test", _test));
            //загрузка и обновление вопросов
            RefreshQuestions();
        }

        /// <summary>
        /// добавление или вставка вопроса
        /// если questionOrder == -1 тогда добовление вопроса в конец
        /// иначе вставка
        /// </summary>
        /// <param name="quesitonOrder"></param>
        private void InsertQuestion(int quesitonOrder)
        {
            TestQuestion newQuestion;
            int MaxOrder = 1;
            //using (new SessionScope())
            //{
                if (_change && _questions.Count > 0)
                {
                    if (MessageForm.ShowYesNoQuestionMsg("Вопрос был изменен. Сохранить?")
                        == DialogResult.Yes)
                    {
                        if (!SaveQuestion())
                        {
                            return;
                        }
                    }
                    _change = false;
                }

                if (quesitonOrder == -1)
                {
                    foreach (TestQuestion testQuestion in _questions)
                    {
                        if (testQuestion.QuestionOrder >= MaxOrder)
                            MaxOrder = testQuestion.QuestionOrder + 1;
                    }
                    if (MaxOrder == -1)
                    {
                        //нет вопросов. Это првый вопрос
                        MaxOrder = 1;
                    }
                }
                else
                {
                    foreach (TestQuestion testQuestion in _questions)
                    {
                        if (testQuestion.QuestionOrder == quesitonOrder)
                        {
                            MessageForm.ShowErrorMsg(string.Format("Вопрос с порядковым номером {0} уже есть в тесте",
                                quesitonOrder));
                            return;
                        }
                        MaxOrder = quesitonOrder;
                    }
                }

                //создаем вопрос
                var q = new Question(new Content(""), null);
                q.CreateAndFlush();

                newQuestion = new TestQuestion(_test, q, MaxOrder);
                newQuestion.CreateAndFlush( );

                _questions.Add(newQuestion);
                listBoxControlQuestions.SelectedIndex = listBoxControlQuestions.Items.Add( newQuestion );
            //}
            if (listBoxControlQuestions.ItemCount == 0)
            {
                exRichTextBoxQuestion.Clear();
                splitContainerControlNavigation.Panel2.Enabled = false;
            }
            else
            {
                splitContainerControlNavigation.Panel2.Enabled = true;
            }
            
        }

        /// <summary>
        /// добавление или вставка ответа
        /// если answerOrder == -1 тогда добавление ответа в конец
        /// иначе вставка 
        /// </summary>
        /// <param name="answerOrder"></param>
        private void InsertAnswer(int answerOrder)
        {
            int MaxOrder = -1;
            if (_currentQuestion == null)
            {
                MessageForm.ShowErrorMsg("Необходимо создать или выбрать вопрос");
                return;
            }
            if (answerOrder == -1)
            {
                foreach (HorisontalAnswer uc in _uControls)
                {
                    if (uc.Order >= MaxOrder)
                        MaxOrder = uc.Order + 1;
                }
                if (MaxOrder == -1)
                {
                    //нет ответов. Это первый ответ
                    MaxOrder = 1;
                }
            }
            else
            {
                foreach (HorisontalAnswer uc in _uControls)
                {
                    if (uc.Order == answerOrder)
                    {
                        MessageForm.ShowErrorMsg(string.Format("Ответ с порядковым номером {0} уже есть в тесте",
                                                               answerOrder));
                        return;
                    }
                }

                MaxOrder = answerOrder;
            }


            var ucr = new CreateAnswer(new ObjectWithId(MaxOrder, null), false, _test, _currentQuestion.Question,
                                       _allAnswers);
            ucr.AnswerChanged += ucr_AnswerChanged;
            ucr.InitControl();
            ucr.ShowInForm();
        }

        private bool SaveQuestion()
        {
            using (new SessionScope())
            {
                if (!_change)
                    return true;
                if (_currentQuestion == null)
                {
                    MessageForm.ShowWarningMsg("Для сохранения необходимо создать вопрос");
                    return true;
                }
                Content c = GetContent();
                if (c == null)
                {
                    return false;
                }
                int? timeLimit = null;
                if (timeEditTestLimit.Time.Ticks > 0)
                {
                    timeLimit = (int) (timeEditTestLimit.Time.Ticks/10000000);
                }
                _currentQuestion.Question.Content = c;
                _currentQuestion.Question.TimeLimit = timeLimit;
                _currentQuestion.Question.UpdateAndFlush();
                //ResetSessionScope()();
                _change = false;

                return true;
            }
        }

        private void RefreshAnswers()
        {
            if (_currentQuestion == null)
            {
                _uControls.Clear();
            }
            IEnumerable<HorisontalAnswer> res =
                _uControls.OrderBy(uc => uc.Order);

            int answersCount = _uControls.Count;
            //проверка на необходимость создания контрола
            //необходимо пересоздание контрола
            tableLayoutPanel1.RowCount = answersCount;
            tableLayoutPanel1.RowStyles.Clear();
            tableLayoutPanel1.Controls.Clear();
            if (_currentQuestion == null) //Нет вопроса
            {
                return;
            }
            int i = 0;
            foreach (HorisontalAnswer uc in res)
            {
                tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 100/answersCount));
                tableLayoutPanel1.Controls.Add(uc.ShowAsControl(), 0, i);

                i++;
            }
            _currentAnswer = null;
        }


        /// <summary>
        /// метод получения контанта
        /// </summary>
        /// <returns></returns>
        private Content GetContent()
        {
            if (radioGroupFormat.SelectedIndex == 0)
            {
                string text = exRichTextBoxQuestion.Text;
                if (string.IsNullOrEmpty(text.Trim()))
                {
                    MessageForm.ShowErrorMsg("Необходимо ввести ответ");
                    return null;
                }
                if (text.Length > 255)
                {
                    //длина более чем допустимая в БД
                    if (MessageForm.ShowYesNoQuestionMsg(
                        "Длина текста вопроса более чем допустимая в БД.\nСохранить текст в формате RTF?")
                        == DialogResult.Yes)
                    {
                        var ms_ = new MemoryStream();
                        exRichTextBoxQuestion.SaveFile(ms_, RichTextBoxStreamType.RichText);
                        var c = new Content(ms_.ToArray());
                        if (ms_.Length > Consts.ContentMaxSize)
                        {
                            MessageForm.ShowErrorMsg("Размер Вашего вопроса составляет " + ms_.Length/1024 +
                                                     " Кбайт. \nУменьшите размер текста и изображений до " +
                                                     Consts.ContentMaxSize/1024 + " Кбайт.");
                            return null;
                        }
                        c.Save();
                        radioGroupFormat.SelectedIndex = 1;
                        return c;
                    }
                    return null;
                }
                else
                {
                    var c = new Content(text);
                    c.Save();
                    return c;
                }
            }
            else
            {
                var ms_ = new MemoryStream();
                exRichTextBoxQuestion.SaveFile(ms_, RichTextBoxStreamType.RichText);
                var c = new Content(ms_.ToArray());
                if (ms_.Length > Consts.ContentMaxSize)
                {
                    MessageForm.ShowErrorMsg("Размер Вашего вопроса составляет " + ms_.Length/1024 +
                                             " Кбайт. \nУменьшите размер текста и изображений до " +
                                             Consts.ContentMaxSize/1024 + " Кбайт.");
                    return null;
                }
                c.Save();
                return c;
            }
        }

        /// <summary>
        /// обновление вопросов
        /// </summary>
        private void RefreshQuestions()
        {
            _questions.Clear();
            listBoxControlQuestions.Items.Clear();


            using (new SessionScope())
            {
                _questions.AddRange(ActiveRecordBase<TestQuestion>.FindAll(new Order("QuestionOrder", true),
                    Expression.Eq("Test", _test)));



                listBoxControlQuestions.Items.AddRange(_questions.ToArray());
            }

            if (listBoxControlQuestions.ItemCount == 0)
            {
                exRichTextBoxQuestion.Clear();
                splitContainerControlNavigation.Panel2.Enabled = false;
            }
            else
            {
                if (_currentQuestion == null)
                    listBoxControlQuestions.SelectedIndex = 0;
                else
                    listBoxControlQuestions.SelectedItem = _currentQuestion;

                splitContainerControlNavigation.Panel2.Enabled = true;
            }

        }


        private void SetQuestion(TestQuestion testQuestion)
        {
            if (_change && _currentQuestion != null)
            {
                if (MessageForm.ShowYesNoQuestionMsg("Вопрос был изменен. Сохранить?")
                    == DialogResult.Yes)
                {
                    if (!SaveQuestion())
                        return;
                }
                _change = false;
            }
            exRichTextBoxQuestion.Clear();
            _currentQuestion = testQuestion;

            string pc = null;
            byte[] rc = null;
            using (new SessionScope(FlushAction.Never))
            {
                Question ques = Question.Find(_currentQuestion.Question.Id);
                pc = ques.Content.PlainContent;
                rc = ques.Content.RichContent;
            }
            //установка вопроса
            if (pc == null)
            {
                //форматированный текст
                var ms = new MemoryStream(rc);
                exRichTextBoxQuestion.LoadFile(ms, RichTextBoxStreamType.RichText);
                radioGroupFormat.SelectedIndex = 1;
            }
            else
            {
                exRichTextBoxQuestion.Text = pc;
                radioGroupFormat.SelectedIndex = 0;
            }
            //установка времени вопроса
            if (_currentQuestion.Question.TimeLimit != null && _currentQuestion.Question.TimeLimit > 0)
            {
                timeEditTestLimit.Time = new DateTime().AddSeconds((double) _currentQuestion.Question.TimeLimit);
                checkEditNoTime.Checked = false;
            }
            else
            {
                timeEditTestLimit.Time = new DateTime(0);
                checkEditNoTime.Checked = true;
            }


            using (new SessionScope(FlushAction.Never))
            {
              QuestionAnswer[] questionAnswers =
                    ActiveRecordBase<QuestionAnswer>.FindAll(new Order("AnswerOrder", true),
                        Restrictions.Eq("Question", _currentQuestion.Question
                            ));
            


            int answerCount = _testAnswers.Length + questionAnswers.Length;
            if (_uControls.Count != answerCount)
            {
                //установка ответа
                _uControls.Clear();
                foreach (TestAnswer testAnswer in _testAnswers)
                {
                    var uc = new HorisontalAnswer(new ObjectWithId(testAnswer.AnswerOrder, testAnswer.Answer), true,
                        _currentQuestion.Question,
                        _test,
                        _allAnswers);
                    uc.getFocus += uc_getFocus;
                    uc.InitControl();
                    _uControls.Add(uc);
                }
                foreach (QuestionAnswer questionAnswer in questionAnswers)
                {
                    var uc =
                        new HorisontalAnswer(new ObjectWithId(questionAnswer.AnswerOrder, questionAnswer.Answer),
                            false, _currentQuestion.Question,
                            _test, _allAnswers);
                    uc.getFocus += uc_getFocus;
                    uc.InitControl();
                    _uControls.Add(uc);
                }
                RefreshAnswers();
            }
            else
            {
                int i = 0;
                foreach (TestAnswer testAnswer in _testAnswers)
                {
                    _uControls[i].SetAnswer(new ObjectWithId(testAnswer.AnswerOrder, testAnswer.Answer),
                        testQuestion.Question);
                    i++;
                }
                foreach (QuestionAnswer questionAnswer in questionAnswers)
                {
                    _uControls[i].SetAnswer(new ObjectWithId(questionAnswer.AnswerOrder, questionAnswer.Answer),
                        testQuestion.Question);
                    i++;
                }
            }
            
            }

            _change = false;
            Application.DoEvents();
        }

        #endregion

        #region обработчики событий от стандартных контролов

        private void simpleButtonAddQuestion_Click(object sender, EventArgs e)
        {
            InsertQuestion(-1);
        }

        private void listBoxControlQuestions_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBoxControlQuestions.SelectedIndex == -1)
            {
                _currentQuestion = null;
                return;
            }
            //проверка на изменение вопроса делается внутри метода установки вопроса
            //int order = (int)listBoxControlQuestions.SelectedItem;
            //foreach (TestQuestion tq in _questions)
            //{
            //    if (tq.QuestionOrder == order)
            //    {
            //        SetQuestion(tq);
            //        break;
            //    }
            //}
            SetQuestion((TestQuestion) listBoxControlQuestions.SelectedItem);
            _change = false;
        }

        private void simpleButtonSaveQuestion_Click(object sender, EventArgs e)
        {
            if (SaveQuestion())
            {
                MessageForm.ShowOkMsg("Вопрос успешно сохранен");
            }
            listBoxControlQuestions.SelectedItem = _currentQuestion;
            listBoxControlQuestions.Refresh();
        }

        private void exRichTextBoxQuestion_TextChanged(object sender, EventArgs e)
        {
            _change = true;
        }

        private void simpleButtonDeleteQuestion_Click(object sender, EventArgs e)
        {
            //using (new SessionScope())
            //{
            if (listBoxControlQuestions.SelectedIndex == -1)
                return;
            if (MessageForm.ShowYesNoQuestionMsg("Вы уверены, что хотите удалить вопрос?")
                == DialogResult.No)
            {
                return;
            }

            _currentQuestion.Delete();
            _currentQuestion = null;
            //}
            RefreshQuestions();
            RefreshAnswers();
            _change = false;

        }

        private void simpleButtonInsertQuestion_Click(object sender, EventArgs e)
        {
            InsertQuestion((int) spinEditQuestionNumber.Value);
        }

        private void simpleButtonAddAnswer_Click(object sender, EventArgs e)
        {
            InsertAnswer(-1);
        }

        private void simpleButtonChangeAnswer_Click(object sender, EventArgs e)
        {
            if (_currentAnswer == null)
            {
                MessageForm.ShowWarningMsg("Выберите ответ");
                return;
            }

            var uc = new CreateAnswer(new ObjectWithId(_currentAnswer.Order, _currentAnswer.answer),
                                      _currentAnswer.FlagTestAnswer, _test, _currentQuestion.Question, _allAnswers);
            uc.AnswerChanged += uc_AnswerChanged;
            uc.InitControl();
            uc.ShowInForm();
        }

        private void simpleButton5_Click(object sender, EventArgs e)
        {
            var ofd = new OpenFileDialog();
            ofd.Filter = "Изображения  |(*.BMP;*.JPG;*.JPEG;*.bmp;*.jpg;*.jpeg)";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                var fInfo = new FileInfo(ofd.FileName);
                int maxLength = 102400; //100 кБайт
                if (fInfo.Length > maxLength)
                {
                    MessageForm.ShowWarningMsg(
                        "Размер изображения (" + fInfo.Length/1024 +
                        " кБайт) превышает максимальное допустимое значение (" + maxLength/1024 +
                        " кБайт).\nДля добавления данной картинки уменьшите ее размер.");
                    return;
                }
                exRichTextBoxQuestion.InsertImage(Image.FromFile(ofd.FileName));
                radioGroupFormat.SelectedIndex = 1;
                _change = true;
            }
        }

        private void radioGroupFormat_SelectedIndexChanged(object sender, EventArgs e)
        {
            //labelControl2.Visible = true;
            //simpleButton5.Visible = true;
            _change = true;
        }

        private void simpleButtonInsertAnswer_Click(object sender, EventArgs e)
        {
            InsertAnswer((int) spinEditAnswerNumber.Value);
        }

        private void checkEditNoTime_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEditNoTime.Checked)
            {
                timeEditTestLimit.Time = new DateTime(0);
                timeEditTestLimit.Enabled = false;
            }
            else
            {
                timeEditTestLimit.Enabled = true;
            }
            _change = true;
        }

        private void timeEditTestLimit_EditValueChanged(object sender, EventArgs e)
        {
            _change = true;
        }

        private void simpleButtonDeleteAnswer_Click(object sender, EventArgs e)
        {
            if (_currentAnswer == null)
            {
                MessageForm.ShowWarningMsg("Выберите ответ");
                return;
            }
            QuestionAnswer[] questionAnswers;
            using (new SessionScope(FlushAction.Never))
            {
                questionAnswers =
                    ActiveRecordBase<QuestionAnswer>.FindAll(new Order("AnswerOrder", true),
                        Expression.Eq("Question",
                            _currentQuestion.Question));
            }

            //поиск в ответах теста

            foreach (TestAnswer tA in _testAnswers)
            {
                if (tA.Answer.Equals(_currentAnswer.answer))
                {
                    MessageForm.ShowErrorMsg("Данный ответ невозможно удалить. Это ответ для всех вопросов теста");
                    return;
                }
            }

            //поиск в ответах вопроса
            foreach (QuestionAnswer q in questionAnswers)
            {
                if (q.Answer.Equals(_currentAnswer.answer) &&
                    (MessageForm.ShowYesNoQuestionMsg("Вы уверены, что хотите удалить ответ") == DialogResult.Yes))
                {
                    q.DeleteAndFlush();
                    _uControls.Remove(_currentAnswer);
                    _currentAnswer = null;
                    //обновление ответов
                    RefreshAnswers();
                    return;
                }
            }

            _change = false;
        }

        public override void ParentFormFormClosing(object sender, FormClosingEventArgs e)
        {
            if (_change)
            {
                if (MessageForm.ShowYesNoQuestionMsg("Текущий вопрос не сохранен. Сохранить?") == DialogResult.Yes)
                {
                    SaveQuestion();
                }
            }
            //если есть  вопросы для теста то выходим
            //if (ActiveRecordBase<TestAnswer>.FindAll(new Order("AnswerOrder", true),
            //                                         Expression.Eq("Test", _test)).Length > 0)

            //    return;
            //делаем просмотр по всем вопросам и смотрим есть ли на них ответы
            using (new SessionScope(FlushAction.Never))
            {
                TestQuestion[] testQuestions = ActiveRecordBase<TestQuestion>.FindAll(new Order("QuestionOrder", true),
                    Expression.Eq("Test", _test));
                var testType = _test.Type;
                foreach (TestQuestion testQuestion in testQuestions)
                {
                    QuestionAnswer[] answers = null;
                    using (new SessionScope(FlushAction.Never))
                    {
                        answers = ActiveRecordBase<QuestionAnswer>.FindAll(new Order("AnswerOrder", true),
                            Expression.Eq("Question",
                                testQuestion.Question));
                    }
                    List<string> errorMsg = new List<string>();

                    if (testQuestion.Question.Content == null ||
                        (string.IsNullOrEmpty(testQuestion.Question.Content.PlainContent) &&
                         testQuestion.Question.Content.RichContent == null))
                        errorMsg.Add("отсутствует текст вопроса");
                    if (testType.PlugInFileName != "KeyboardSimulator" && answers.Length == 0)
                        errorMsg.Add("отсутствуют ответы");
                    if (errorMsg.Count > 0)
                    {
                        MessageForm.ShowErrorMsg(string.Format("Для вопроса с номером {0} : ",
                            testQuestion.QuestionOrder) + String.Join(", ", errorMsg));
                        e.Cancel = true;
                        return;
                    }

                }
            }
            //TestDBExecutor.ClearDataBase();
            base.ParentFormFormClosing(sender, e);
        }


        private void labelControl1_Click(object sender, EventArgs e)
        {
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            exRichTextBoxQuestion.Paste();
        }

        #endregion

        #region обработчики событий от пользовательских контролов

        /// <summary>
        /// Обработчик события на добавления ответа
        /// </summary>
        /// <param name="answer"></param>
        private bool ucr_AnswerChanged(ObjectWithId answer)
        {
            using (new SessionScope())
            {
                //поиск уже существующего такого ответа
                QuestionAnswer[] questionAnswers =
                    ActiveRecordBase<QuestionAnswer>.FindAll(new Order("AnswerOrder", true),
                        Expression.Eq("Question",
                            _currentQuestion.
                                Question));
                foreach (QuestionAnswer q in questionAnswers)
                {
                    if (q.Answer.Equals(answer.Value))
                    {
                        MessageForm.ShowErrorMsg("Данный ответ уже присутствует в наборе");
                        return false;
                    }
                }

                //поиск уже существующего такого ответа
                //в ответах теста

                foreach (TestAnswer tA in _testAnswers)
                {
                    if (tA.Answer.Equals(answer.Value) && tA.AnswerOrder != answer.ID)
                    {
                        MessageForm.ShowErrorMsg("Данный ответ уже присутствует в наборе");
                        return false;
                    }
                }
            }
            //добавление ответа в набор
            var questionAnswer = new QuestionAnswer(_currentQuestion.Question, (Answer) answer.Value,
                answer.ID);
            questionAnswer.Save();
            var uc = new HorisontalAnswer(answer, false, _currentQuestion.Question, _test, _allAnswers);
            uc.getFocus += uc_getFocus;
            uc.InitControl();
            _uControls.Add(uc);
            RefreshAnswers();
            return true;
        }

        /// <summary>
        /// обработчик события на изменения ответа
        /// </summary>
        /// <param name="answer"></param>
        /// <returns></returns>
        private bool uc_AnswerChanged(ObjectWithId answer)
        {
            QuestionAnswer[] questionAnswers;
            using (new SessionScope())
            {
                questionAnswers = ActiveRecordBase<QuestionAnswer>.FindAll(new Order("AnswerOrder", true),
                    Expression.Eq("Question",
                        _currentQuestion.
                            Question));
            }//поиск уже существующего такого ответа
            foreach (QuestionAnswer q in questionAnswers)
            {
                if (q.Answer.Equals(answer.Value) && q.AnswerOrder != answer.ID)
                {
                    MessageForm.ShowErrorMsg("Данный ответ уже присутствует в наборе");
                    return false;
                }
            }

            //поиск уже существующего такого ответа
            //в ответах теста

            foreach (TestAnswer tA in _testAnswers)
            {
                if (tA.Answer.Equals(answer.Value) && tA.AnswerOrder != answer.ID)
                {
                    MessageForm.ShowErrorMsg("Данный ответ уже присутствует в наборе");
                    return false;
                }
            }

            foreach (QuestionAnswer questionAnswer in questionAnswers)
            {
                if (questionAnswer.AnswerOrder == answer.ID)
                {
                    if (!questionAnswer.Answer.Equals(answer.Value))
                    {
                        questionAnswer.Delete();
                        var newQuestionAnswer = new QuestionAnswer(_currentQuestion.Question, (Answer) answer.Value,
                                                                   answer.ID);
                        newQuestionAnswer.Save();
                    }
                    //обновляем ответ
                    _currentAnswer.SetAnswer(answer, _currentQuestion.Question);

                    return true;
                }
            }
            return true;
        }

        private void uc_getFocus(HorisontalAnswer sender)
        {
            foreach (HorisontalAnswer h in _uControls)
            {
                h.BackgroundImage = null;
            }
            _currentAnswer = sender;
            sender.BackgroundImage = EurecaCorp.Professional.TestingPlugInStandard.Properties.Resources.back; 

        }

        #endregion

       
    }
}
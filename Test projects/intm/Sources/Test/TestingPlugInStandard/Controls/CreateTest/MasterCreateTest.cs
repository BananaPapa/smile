﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using Castle.ActiveRecord;
using DevExpress.Data.PLinq.Helpers;
using DevExpress.XtraWizard;
using Eureca.HumanTestingSystem.TestingPlugInStandard.Controls.CreateTest;
using Eureca.Professional.ActiveRecords;
using Eureca.Professional.CommonClasses;
using NHibernate.Criterion;

namespace Eureca.Professional.TestingPlugInStandard.Controls.CreateTest
{
    /// <summary>
    /// мастер создания и редактирования теста
    /// </summary>
    internal partial class MasterCreateTest : BaseTestingControl
    {
        #region fields

        /// <summary>
        /// список всех ответов(подгружается 1 раз и хранится в памяти для уменьшения связи с БД)
        /// </summary>
        private readonly List<Answer> _allAnswers = new List<Answer>();

        /// <summar>Список всех шкал</summar></summary>
        private readonly List<Scale> _allScales = new List<Scale>();


        /// <summary>Ссылка на методику</summary>
        private readonly Method _method;

        /// <summar>Список всех отчетов</summar></summary>
        private readonly List<Report> _reports = new List<Report>();

        /// <summary>Список стенов для данного теста и шкалы</summary>
        private readonly List<Sten> _stens = new List<Sten>();

        /// <summary>
        /// список ответов для всего теста
        /// </summary>
        private readonly List<TestAnswer> _testAnswers = new List<TestAnswer>();

        private readonly User _user;

        /// <summary>
        /// флаг изменения таблицы со стенами
        /// </summary>
        private bool _changeSten;

        /// <summary>
        /// сообщение об ошибке
        /// </summary>
        private string _error;

        /// <summary>Ссылка на тест</summary>
        private Test _test;

        #endregion

        #region properties

        public string Error
        {
            get
            {
                string err = _error;
                _error = "";
                return err;
            }
            set { _error = value; }
        }

        #endregion

        #region constructor

        /// <summary>
        /// конструктор по умолчанию инициализирует контрол
        /// </summary>
        private MasterCreateTest()
        {
            InitializeComponent();
        }

        /// <summary>
        /// конструктор создает контрол и инициализирует его
        /// </summary>
        /// <param name="user"></param>
        /// <param name="method">ссылка на тематику(методику)</param>
        /// <param name="test">ссылка на тест</param>
        internal MasterCreateTest(User user, Method method, Test test)
            : this()
        {
            _user = user;
            //ResetSessionScope()();
            SetDefault();

            using (new SessionScope(FlushAction.Never))
            {
                _method = Method.Find(method.Id);
                if (test != null)
                    _test = Test.Find(test.Id);
               
            }
            InsertData();
        }

        #endregion

        #region Events

        public event MasterCreateTestEventHandler AddTest;
        public event MasterCreateTestEventHandler ChangeTest;

        #endregion

        #region методы

        /// <summary>
        /// метод инициализации контрола
        /// </summary>
        public override void InitControl()
        {
            base.InitControl();
            CaptionControl = "Мастер создания и редактирования теста";
        }

        /// <summary>
        /// метод выставляет динамические значения контролов
        /// </summary>
        private void SetDefault()
        {
            timeEditTestLimit.Time = new DateTime(0);
        }

        /// <summary>
        /// метод всталяет данные из БД
        /// </summary>
        private void InsertData()
        {
            //загрузка всех шкал
            _allAnswers.Clear();
            listBoxControlAllScales.Items.Clear();
            //Загрузка типов тестов
            comboBoxEditTestType.Properties.Items.Clear();
            using (var sess = new SessionScope(FlushAction.Never))
            {
                comboBoxEditTestType.Properties.Items.AddRange( TestType.FindAll( ));


                //загрузка шкал в список
                _allScales.AddRange(ActiveRecordBase<Scale>.FindAll());
                foreach (Scale scale in _allScales)
                {
                    listBoxControlAllScales.Items.Add(scale);
                }
                if (listBoxControlAllScales.ItemCount > 0)
                    listBoxControlAllScales.SelectedIndex = 0;


                //Загрузка всех отчетов
                _reports.AddRange(ActiveRecordBase<Report>.FindAll(Expression.Eq("ReportType", 1)));
                listBoxControlAllReports.Items.Clear();
                foreach (Report report in _reports)
                {
                    listBoxControlAllReports.Items.Add(report);
                }
                if (listBoxControlAllReports.ItemCount > 0)
                    listBoxControlAllReports.SelectedIndex = 0;
            }


            //если теста нет то вставляем данные по умолчанию
            if (_test == null)
            {
                propertyGridControl1.SelectedObject = new TestSettings();
                comboBoxEditTestType.SelectedIndex = 0;
            }
            else
            {
                //Если тест не новый, то вставляем данные
                //свойства
                string testDescription = null;
                List<Report> reports = null;
                List<Scale> scales = null;
                using (var sess = new SessionScope(FlushAction.Never))
                {
                    _test = Test.Find(_test.Id);
                    testDescription = _test.Description.PlainContent;
                    reports = _test.Reports.ToList();
                    scales = _test.Scales.ToList();
                }

                textEditTestName.Text = _test.Title;
                if (_test.TimeLimit != null && _test.TimeLimit != 0)
                {
                    timeEditTestLimit.Time = new DateTime().AddSeconds((double) _test.TimeLimit);
                    checkEditNoTime.Checked = false;
                }
                else
                {
                    checkEditNoTime.Checked = true;
                }
                if (_test.Speed.HasValue)
                    spinEditSpeed.Value = _test.Speed.Value;
                if (_test.MaxErrorPercent.HasValue)
                    spinEditMaxErrorPercent.Value = _test.MaxErrorPercent.Value;
                spinEditPassingScore.Value = _test.PassingScore;
                spinEditMaxTeacherPassing.Value = _test.MaxTeacherPassing;


                textEditTestComment.Text = testDescription;
                propertyGridControl1.SelectedObject = TestSettings.GetSettings(_test.Options);
                comboBoxEditTestType.SelectedItem = _test.Type;
                comboBoxEditTestType.Enabled = false;
                //шкалы

                foreach (Scale scale in scales)
                {
                    listBoxControlSelectedScales.Items.Add(scale);
                    listBoxControlAllScales.Items.Remove(scale);
                }
                //отчеты
                listBoxControlSelectedReports.Items.Clear();


                foreach (Report report in reports)
                {
                    listBoxControlSelectedReports.Items.Add(report);
                    listBoxControlAllReports.Items.Remove(report);
                }

                //ответы
                LoadTestAnswers();
            }
            //propertyGridControl1.RetrieveFields();
            //propertyGridControl1.BestFit();
        }

        /// <summary>
        /// метод загрузки ответов к тесту
        /// </summary>
        private void LoadTestAnswers()
        {
            _testAnswers.Clear();
            listBoxControlSelectedAnswers.Items.Clear();
            using (new SessionScope(FlushAction.Never))
            {
                _testAnswers.AddRange(ActiveRecordBase<TestAnswer>.FindAll(new Order("AnswerOrder", true),
                    Expression.Eq("Test", _test)));
                foreach (TestAnswer testAnswer in _testAnswers)
                {
                    listBoxControlSelectedAnswers.Items.Add(testAnswer);
                }
            }
        }

        /// <summary>
        /// загрузка страницы со стенами(оценками)
        /// </summary>
        private void LoadStanPage()
        {
            if (listBoxControlSelectedScalesStens.SelectedIndex == -1)
            {
                listBoxControlSelectedScalesStens.SelectedIndex = 0;
            }
            var scale = (Scale) listBoxControlSelectedScalesStens.SelectedItem;
            _stens.Clear();
            using (new SessionScope(FlushAction.Never))
            {
                _stens.AddRange(ActiveRecordBase<Sten>.FindAll(new Order("Value", true),
                    Expression.Eq("Test", _test), Expression.Eq("Scale", scale)));
                spinEditStensCount.Value = _stens.Count;
            }
            RefreshStensGrid();
        }

        /// <summary>Метод перемещения из списка всех шкал в список выбраных шкал</summary>
        /// <param name="All"></param>
        private void dragFromAllToSelectedOneScale(bool All)
        {
            if (listBoxControlAllScales.SelectedIndex == -1)
                return;
            if (!All)
            {
                var scale = (Scale) listBoxControlAllScales.SelectedItem;
                //if (scale.TypeScale < 3)
                //{
                //    MessageForm.ShowErrorMsg(string.Format("Шкалу {0} нельзя конфигурировать", scale));
                //    return;
                //}
                listBoxControlSelectedScales.Items.Add(listBoxControlAllScales.SelectedItem);
                listBoxControlAllScales.Items.Remove(listBoxControlAllScales.SelectedItem);
            }
            else
            {
                foreach (object o in listBoxControlAllScales.Items)
                {
                    var scale = (Scale) o;
                    //if (scale.TypeScale < 3)
                    //{
                    //    MessageForm.ShowErrorMsg(string.Format("Шкалу {0} нельзя конфигурировать", scale));
                    //    return;
                    //}
                    listBoxControlSelectedScales.Items.Add(o);
                }
                listBoxControlAllScales.Items.Clear();
            }
        }

        /// <summary>Метод перемещения из списка выбраных шкал во все шкалы</summary>
        /// <param name="All"></param>
        private void dragFromSelectedToAllOneScale(bool All)
        {
            if (listBoxControlSelectedScales.SelectedIndex == -1)
                return;
            if (!All)
            {
                var scale = (Scale) listBoxControlSelectedScales.SelectedItem;
                if (scale.TypeScale < 2)
                {
                    MessageForm.ShowErrorMsg(string.Format("Шкалу {0} нельзя конфигурировать", scale));
                    return;
                }
                listBoxControlAllScales.Items.Add(listBoxControlSelectedScales.SelectedItem);
                listBoxControlSelectedScales.Items.Remove(listBoxControlSelectedScales.SelectedItem);
            }
            else
            {
                foreach (object o in listBoxControlSelectedScales.Items)
                {
                    var scale = (Scale) o;
                    if (scale.TypeScale < 2)
                    {
                        MessageForm.ShowErrorMsg(string.Format("Шкалу {0} нельзя конфигурировать", scale));
                        return;
                    }
                    listBoxControlAllScales.Items.Add(o);
                }
                listBoxControlSelectedScales.Items.Clear();
            }
        }


        /// <summary>Метод перемещения из списка всех отчетов в список выбраных отчетов</summary>
        /// <param name="All"></param>
        private void dragFromAllToSelectedOneReport(bool All)
        {
            if (listBoxControlAllReports.SelectedIndex == -1)
                return;
            if (!All)
            {
                listBoxControlSelectedReports.Items.Add(listBoxControlAllReports.SelectedItem);
                listBoxControlAllReports.Items.Remove(listBoxControlAllReports.SelectedItem);
            }
            else
            {
                foreach (object o in listBoxControlAllReports.Items)
                {
                    listBoxControlSelectedReports.Items.Add(o);
                }
                listBoxControlAllReports.Items.Clear();
            }
        }

        /// <summary>Метод перемещения из списка выбраных отчетов во все отчеты</summary>
        /// <param name="All"></param>
        private void dragFromSelectedToAllOneReport(bool All)
        {
            if (listBoxControlSelectedReports.SelectedIndex == -1)
                return;
            if (!All)
            {
                listBoxControlAllReports.Items.Add(listBoxControlSelectedReports.SelectedItem);
                listBoxControlSelectedReports.Items.Remove(listBoxControlSelectedReports.SelectedItem);
            }
            else
            {
                foreach (object o in listBoxControlSelectedReports.Items)
                {
                    listBoxControlAllReports.Items.Add(o);
                }
                listBoxControlSelectedReports.Items.Clear();
            }
        }

        private void ChangeTableStans(int count)
        {
            while (_stens.Count != count)
            {
                if (_stens.Count > count)
                {
                    _stens[_stens.Count - 1].DeleteAndFlush();
                    _stens.RemoveAt(_stens.Count - 1);
                }
                if (_stens.Count < count)
                {
                    var sten = new Sten(_test, (Scale) listBoxControlSelectedScalesStens.SelectedItem, 0,
                        0, 0);
                    sten.CreateAndFlush();
                    _stens.Add(sten);
                }
                RefreshStensGrid();
            }
        }

        private void RefreshStensGrid()
        {
            gridControlStens.DataSource = _stens;
            gridViewStens.BestFitColumns();
        }

        private bool CheckStens()
        {
            float max1, min1, max2, min2;
            for (int i = 0; i < gridViewStens.RowCount; i++)
            {
                //id1 = int.Parse(gridViewStens.GetRowCellValue(i, "Id").ToString());
                max1 = float.Parse(gridViewStens.GetRowCellValue(i, "MaxRawPoints").ToString());
                min1 = float.Parse(gridViewStens.GetRowCellValue(i, "MinRawPoints").ToString());
                //val1 = int.Parse(gridViewStens.GetRowCellValue(i, "Value").ToString());
                if (min1 > max1)
                {
                    _error = string.Format("Минимальное значение больше максимального у интервала от {0} до {1}", min1,
                        max1);
                    return false;
                }
                for (int j = i + 1; j < gridViewStens.RowCount; j++)
                {
                    //id2 = int.Parse(gridViewStens.GetRowCellValue(j, "Id").ToString());
                    max2 = float.Parse(gridViewStens.GetRowCellValue(j, "MaxRawPoints").ToString());
                    min2 = float.Parse(gridViewStens.GetRowCellValue(j, "MinRawPoints").ToString());
                    //val2 = int.Parse(gridViewStens.GetRowCellValue(j, "Value").ToString());
                    if (min2 > max2)
                    {
                        _error = string.Format("Минимальное значение больше максимального у интервала от {0} до {1}",
                            min2, max2);
                        return false;
                    }
                    //проверка на экстремумы первого значения
                    if (max1 > min2 && max1 < max2
                        || min1 > min2 && min1 < max2
                        || max2 > min1 && max2 < max1
                        || min2 > min1 && min2 < max1
                        || min1 == min2 && max1 == max2 && (max1 - min1 > 0)) //равенство на границе
                    {
                        _error = string.Format("Интервал от {0} до {1} пересекается с интервалом от {2} до {3}", min1,
                            max1, min2, max2);
                        return false;
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// метод создания теста
        /// </summary>
        private bool CreateTest()
        {
            //Создаем либо обновляем тест
            int timeLimit = 0;
            int passingScore = 0;
            int maxTeachingPassing = 0;
            string testName = textEditTestName.Text.Trim();
            if (string.IsNullOrEmpty(testName))
            {
                MessageForm.ShowWarningMsg("Введите наименование теста");
                return false;
            }
            var alies = _test != null
                ? Test.FindFirst(Expression.And(Expression.Eq("Title", testName),
                    Expression.Not(Expression.Eq("Id", _test.Id))))
                : Test.FindFirst(Expression.Eq("Title", testName));
            if (alies != null)
            {
                MessageForm.ShowWarningMsg("Тест с таким именем уже существует");
                return false;
            }

            passingScore = (int) spinEditPassingScore.Value;
            maxTeachingPassing = (int) spinEditMaxTeacherPassing.Value;

            if (timeEditTestLimit.Time.Ticks > 0)
            {
                timeLimit = (int) (timeEditTestLimit.Time.Ticks/10000000);
            }
            bool isNew = false;

            if (_test == null)
            {
                //SessionScope.Current.Evict();
                var c = new Content(textEditTestComment.Text);
                c.CreateAndFlush();

                _test = new Test(comboBoxEditTestType.SelectedItem as TestType, _method,
                    testName,
                    timeLimit, passingScore, maxTeachingPassing,
                    c,
                    null);
                isNew = true;
                //добавляем стандартные шкалы
                foreach (Scale sc in _allScales)
                    if (sc.TypeScale < 2)
                    {
                        _test.Scales.Add(sc);
                        //перемещаем эти шкалы в мастере
                        listBoxControlAllScales.Items.Remove(sc);
                        listBoxControlSelectedScales.Items.Add(sc);
                    }
                //_test.CreateAndFlush();
            }

            _test.Title = testName;
            _test.TimeLimit = timeLimit;
            _test.PassingScore = passingScore;
            _test.Type = TestType.Find((comboBoxEditTestType.SelectedItem as TestType).Id);
            _test.MaxTeacherPassing = maxTeachingPassing;
            _test.Speed = (int) spinEditSpeed.Value;
            _test.MaxErrorPercent = (int) spinEditMaxErrorPercent.Value;

            if (isNew)
            {
                if (_test.Description.PlainContent.CompareTo(textEditTestComment.Text) != 0)
                {
                    _test.Description = new Content(textEditTestComment.Text);
                }
            }
            _test.Options = TestDetailsSerializer.Serialize(propertyGridControl1.SelectedObject);


            if (isNew)
            {
                Event.Add(_user, EventTypes.BeginEdit, _test.Id);
                _test.CreateAndFlush( );
                AddTest(_test);
            }
            else
            {
                _test.UpdateAndFlush();
            }
            return true;
        }

        /// <summary>
        /// метод сохранения стенов
        /// </summary>
        /// <returns></returns>
        private bool SaveStens()
        {
            int id;
            if (!CheckStens())
            {
                MessageForm.ShowErrorMsg(Error);
                return false;
            }

            for (int i = 0; i < gridViewStens.RowCount; i++)
            {
                id = int.Parse(gridViewStens.GetRowCellValue(i, "Id").ToString()); //.GetDataRow(i);
                foreach (Sten sten in _stens)
                {
                    if (sten.Id == id)
                    {
                        sten.MaxRawPoints =
                            float.Parse(gridViewStens.GetRowCellValue(i, "MaxRawPoints").ToString());
                        sten.MinRawPoints =
                            float.Parse(gridViewStens.GetRowCellValue(i, "MinRawPoints").ToString());
                        sten.Value = int.Parse(gridViewStens.GetRowCellValue(i, "Value").ToString());

                        sten.Description = (String) gridViewStens.GetRowCellValue(i, "Description");
                        sten.UpdateAndFlush();
                        //ResetSessionScope()();
                    }
                }
            }

            return true;
        }

        #endregion

        #region Обработчики событий от стандартных контролов

        /// <summary>Изменение выбраной страницы</summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CreatePlan_SelectedPageChanging(object sender, WizardPageChangingEventArgs e)
        {
            #region панель с настройками теста и созданием его

            if (e.PrevPage == wizardPageCreteTest && e.Direction == Direction.Forward)
            {
                if (!CreateTest())
                {
                    e.Cancel = true;
                }
            }

            #endregion

            #region панель со шкалами

            if (e.PrevPage == wizardPageScales && e.Direction == Direction.Forward)
            {
                bool change = false;
                //вперед
                //Удалям неиспользованные шкалы

                if (_test != null)
                {
                    for (int i = 0; i < _test.Scales.Count; i++)
                    {
                        Scale scale = _test.Scales[i];
                        if (!listBoxControlSelectedScales.Items.Contains(scale))
                        {
                            _test.Scales.Remove(scale);
                            i--;
                            change = true;
                        }
                    }
                    //Добавляем шкалы в тест
                    foreach (Scale scale in listBoxControlSelectedScales.Items)
                    {
                        if (!_test.Scales.Contains(scale))
                        {
                            _test.Scales.Add(scale);
                            change = true;
                        }
                    }
                }
                switch (listBoxControlSelectedScales.Items.Count)
                {
                    case 0:
                        MessageForm.ShowErrorMsg("Для продолжения необходимо добавить хотя бы 1 шкалу");
                        e.Cancel = true;
                        return;
                    default:
                        spinEditStensCount.Enabled = true;
                        break;
                }
                if (change)
                {
                    _test.UpdateAndFlush();
                    //ResetSessionScope()();
                }
            }

            #endregion

            #region панель с отчетами

            if (e.PrevPage == wizardPageReports && e.Direction == Direction.Forward)
            {
                bool change = false;
                //вперед
                //Удалям неиспользованные отчеты

                using (new SessionScope())
                {
                    _test = Test.Find(_test.Id);
                    if (_test != null)
                    {
                        for (int i = 0; i < _test.Reports.Count; i++)
                        {
                            Report report = _test.Reports[i];
                            if (!listBoxControlSelectedReports.Items.Contains(report))
                            {
                                _test.Reports.Remove(report);
                                i--;
                                change = true;
                            }
                        }
                        //Добавляем отчеты в тест
                        foreach (Report report in listBoxControlSelectedReports.Items)
                        {
                            if (!_test.Reports.Contains(report))
                            {
                                _test.Reports.Add(report);
                                change = true;
                            }
                        }
                    }
                }
                //switch (listBoxControlSelectedReports.Items.Count)
                //{
                //    case 0:
                //        MessageForm.Show("Для продолжения необходимо добавить хотя бы 1 шкалу");
                //        e.Cancel = true;
                //        return;
                //    default:
                //        spinEditStensCount.Enabled = true;
                //        break;
                //}
                if (change)
                {
                    _test.UpdateAndFlush();
                }
            }

            #endregion

            #region панель с ответами

            if (e.PrevPage == wizardPageAnswers && e.Direction == Direction.Forward)
            {
                //если на панели с ответами нажали вперед
                listBoxControlSelectedScalesStens.Items.Clear();
                foreach (object item in listBoxControlSelectedScales.Items)
                {
                    listBoxControlSelectedScalesStens.Items.Add(item);
                }
                // LoadStanPage();
            }

            #endregion

            #region панель со стенами(оценками)

            if (e.PrevPage == wizardPageStens && e.Direction == Direction.Forward)
            {
                if (_changeSten)
                    if (!SaveStens())
                    {
                        e.Cancel = true;
                    }
            }

            #endregion
        }

        private void listBoxControlAllScales_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            dragFromAllToSelectedOneScale(false);
        }

        private void listBoxControlSelectedScales_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            dragFromSelectedToAllOneScale(false);
        }

        private void simpleButtonDragOneToSelect_Click(object sender, EventArgs e)
        {
            dragFromAllToSelectedOneScale(false);
        }

        private void simpleButtonDragOneToAll_Click(object sender, EventArgs e)
        {
            dragFromSelectedToAllOneScale(false);
        }

        private void simpleButtonDragAllToSelect_Click(object sender, EventArgs e)
        {
            dragFromAllToSelectedOneScale(true);
        }

        private void simpleButtonDragAllToAll_Click(object sender, EventArgs e)
        {
            dragFromSelectedToAllOneScale(true);
        }

        private void simpleButtonAddScale_Click(object sender, EventArgs e)
        {
            string name = textEditScaleName.Text.Trim();
            if (string.IsNullOrEmpty(name))
            {
                MessageForm.ShowWarningMsg("Введите наименование шкалы");
                return;
            }

            string shortName = textEditScaleShortName.Text.Trim();
            if (string.IsNullOrEmpty(shortName))
            {
                MessageForm.ShowWarningMsg("Введите аббревиатуру шкалы");
                return;
            }
            foreach (Scale scale_ in _allScales)
            {
                if (scale_.Name.ToUpper().CompareTo(name.ToUpper()) == 0)
                {
                    MessageForm.ShowWarningMsg("Данная шкала уже есть в наборе");
                    return;
                }
            }
            var scale = new Scale(name.Trim(), shortName);
            scale.CreateAndFlush();

            listBoxControlAllScales.Items.Add(scale);
            _allScales.Add(scale);
            textEditScaleName.Text = "";
            textEditScaleShortName.Text = "";
        }


        private void simpleButtonAddIntoSelectedAnswer_Click(object sender, EventArgs e)
        {
            foreach (TestAnswer testAnswer in _testAnswers)
            {
                if (spinEdit1.Value == testAnswer.AnswerOrder)
                {
                    MessageForm.ShowErrorMsg("Данный порядковый номер уже существует");
                    return;
                }
            }
            var uc = new AddAnswer(null, _allAnswers);
            uc.InitControl();
            uc.AddAnswerEvent += uc_AddAnswerEvent;
            uc.ShowInForm();
        }

        /// <summary>
        /// кнопка финиш
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CreatePlan_FinishClick(object sender, CancelEventArgs e)
        {
            if (ParentForm != null)
                ParentForm.Hide();
            ExitQuestion = null;
            //вызов события на изменение теста
            MethodInvoker method = () => ChangeTest(_test);
            Invoke(method);
        }

        private void simpleButtonDeleteSelectedAnswer_Click(object sender, EventArgs e)
        {
            if (listBoxControlSelectedAnswers.SelectedIndex == -1)
            {
                MessageForm.ShowWarningMsg("Выберите ответ");
                return;
            }
            if (MessageForm.ShowYesNoQuestionMsg("Вы уверены, что хотите удалить ответ?")
                == DialogResult.No)
            {
                return;
            }
            var item = (TestAnswer) listBoxControlSelectedAnswers.SelectedItem;
            item.Delete();
            LoadTestAnswers();
        }

        private void listBoxControlSelectedScalesStens_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadStanPage();
        }

        private void simpleButtonSaveStens_Click(object sender, EventArgs e)
        {
            if (SaveStens())
            {
                MessageForm.ShowOkMsg("Сохранение выполнено успешно");
            }
        }

        private void simpleButtonCheckStens_Click(object sender, EventArgs e)
        {
            if (CheckStens())
            {
                MessageForm.ShowOkMsg("Проверка выполнена успешно");
            }
            else
            {
                MessageForm.ShowErrorMsg(Error);
            }
        }

        private void listBoxControlSelectedAnswers_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBoxControlSelectedAnswers.SelectedIndex == -1)
                return;
            var testAnswer = (TestAnswer) listBoxControlSelectedAnswers.SelectedItem;
            spinEdit1.Value = testAnswer.AnswerOrder;
        }

        private void spinEditStensCount_EditValueChanged(object sender, EventArgs e)
        {
            ChangeTableStans((int) spinEditStensCount.Value);
        }

        private void ListBoxControlSelectedAnswersMouseDoubleClick(object sender, MouseEventArgs e)
        {
            ChangeTestAnswer();
        }

        private void SimpleButtonToSelectedReportsClick(object sender, EventArgs e)
        {
            dragFromAllToSelectedOneReport(false);
        }

        private void SimpleButtonToAllReportsClick(object sender, EventArgs e)
        {
            dragFromSelectedToAllOneReport(false);
        }

        private void ListBoxControlSelectedReportsMouseDoubleClick(object sender, MouseEventArgs e)
        {
            dragFromSelectedToAllOneReport(false);
        }

        private void ListBoxControlAllReportsMouseDoubleClick(object sender, MouseEventArgs e)
        {
            dragFromAllToSelectedOneReport(false);
        }

        private void SimpleButtonToSelectedReportsAllClick(object sender, EventArgs e)
        {
            dragFromAllToSelectedOneReport(true);
        }

        private void SimpleButtonToAllReportsAllClick(object sender, EventArgs e)
        {
            dragFromSelectedToAllOneReport(true);
        }

        private void simpleButtonDeleteScale_Click(object sender, EventArgs e)
        {
            if (listBoxControlAllScales.SelectedIndex == -1)
            {
                MessageForm.ShowWarningMsg("Для удаления необходимо выбрать шкалу");
                return;
            }
            var RemScale = (Scale) listBoxControlAllScales.SelectedItem;
            if (RemScale.TypeScale == 1)
            {
                MessageForm.ShowErrorMsg("Данную шкалу нельзя удалять");
                return;
            }
            if (MessageForm.ShowYesNoQuestionMsg("Вы уверены, что хотите удалить шкалу?")
                == DialogResult.No)
            {
                return;
            }

            if (RemScale.Tests.Count > 0)
            {
                MessageForm.ShowErrorMsg("Данная шкала используется в других тестах");
                return;
            }
            RemScale.DeleteAndFlush();
            _allScales.Remove(RemScale);
            listBoxControlAllScales.Items.Remove(RemScale);
        }

        /// <summary>
        /// обработчик события нажатия на кнопку изменения ответа на вопрос
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButtonChangeAnswer_Click(object sender, EventArgs e)
        {
            ChangeTestAnswer();
        }

        /// <summary>
        /// обработчик события отжатия клавиши с поля в таблице
        /// со стенами. Сигнализирует о том что стены изменились и их необходимо сохранить
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void gridControlStens_EditorKeyUp(object sender, KeyEventArgs e)
        {
            //ставим флажок редактирования стенов в true
            _changeSten = true;
        }


        private void checkEditNoTime_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEditNoTime.Checked)
            {
                timeEditTestLimit.Time = new DateTime(0);
                timeEditTestLimit.Enabled = false;
            }
            else
            {
                timeEditTestLimit.Enabled = true;
            }
        }

        #endregion

        #region Обработчики событий от пользовательских контролов

        private bool uc_AddAnswerEvent(Answer answer)
        {
            var testAnswer = new TestAnswer(_test, answer, (int) spinEdit1.Value);
            if (_testAnswers.Contains(testAnswer))
            {
                MessageForm.ShowWarningMsg("Ошибка добавления ответа.\nУже содержится в наборе ответов");
                return false;
            }
            testAnswer.CreateAndFlush();
            LoadTestAnswers();
            return true;
        }

        /// <summary>
        /// обработчик события получения ответа
        /// </summary>
        /// <param name="answer"></param>
        /// <returns></returns>
        private bool uc_ChangeAnswerEvent(Answer answer)
        {
            var changedTestAnswer = (TestAnswer) listBoxControlSelectedAnswers.SelectedItem;
            var testAnswer = new TestAnswer(_test, answer, (int) spinEdit1.Value);
            if (changedTestAnswer.Equals(testAnswer))
            {
                //ответы совпадают
                return true;
            }
            if (_testAnswers.Contains(testAnswer))
            {
                //данный ответ уже есть в наборе
                MessageForm.ShowWarningMsg("Ошибка добавления ответа.\nУже содердится в наборе ответов");
                return false;
            }
            changedTestAnswer.DeleteAndFlush();
            testAnswer.CreateAndFlush();
            LoadTestAnswers();
            return true;
        }

        /// <summary>
        /// метод изменения ответа на вопрос. 
        /// </summary>
        private void ChangeTestAnswer()
        {
            if (listBoxControlSelectedAnswers.SelectedIndex == -1)
            {
                MessageForm.ShowWarningMsg("Для изменения необходимо выбрать ответ");
                return;
            }
            var sel = (TestAnswer) listBoxControlSelectedAnswers.SelectedItem;
            //создаю контрол редактирования ответа
            var uc = new AddAnswer(sel.Answer, _allAnswers);
            uc.InitControl();
            uc.AddAnswerEvent += uc_ChangeAnswerEvent;
            uc.ShowInForm();
        }

        #endregion
    }
}

namespace Eureca.HumanTestingSystem.TestingPlugInStandard.Controls.CreateTest
{
    internal delegate void MasterCreateTestEventHandler(Test test);
}
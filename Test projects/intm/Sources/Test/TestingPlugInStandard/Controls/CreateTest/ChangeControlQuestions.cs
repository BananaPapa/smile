﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Castle.ActiveRecord;
using Eureca.Professional.ActiveRecords;
using Eureca.Professional.CommonClasses;
using Eureca.Professional.TestingPlugInStandard.Components;
using EurecaCorp.Professional.TestingPlugInStandard.Properties;
using log4net;
using log4net.Repository.Hierarchy;
using NHibernate.Criterion;

namespace Eureca.Professional.TestingPlugInStandard.Controls.CreateTest
{
    /// <summary>
    /// контрол конфигурирования вопросов теста
    /// </summary>
    public partial class ChangeControlQuestions : BaseTestingControl
    {
        #region поля

        /// <summary>Список всех ответов в тесте</summary>
        private readonly List<Answer> _allAnswers = new List<Answer>( );

        private static readonly ILog Log = LogManager.GetLogger(typeof (ChangeControlQuestions));

        //private readonly Test _test;
        private readonly Method _method;
        private readonly List<ControlQuestion> _questions = new List<ControlQuestion>( );
        private readonly List<HorisontalAnswer> _uControls = new List<HorisontalAnswer>( );
        private bool _change;
        private HorisontalAnswer _currentAnswer;
        private ControlQuestion _currentQuestion;

        #endregion

        #region конструктор

        public ChangeControlQuestions( Method method )
        {
            _method = method;
            InitializeComponent( );
        }

        #endregion

        #region методы

        public override void InitControl( )
        {
            base.InitControl( );
            SetDefault( );
            InsertData( );
        }

        private void SetDefault( )
        {
            CaptionControl = "Редактирование вопросов";
            exRichTextBoxQuestion.Font = new Font( "Times New Roman", 18 );
            exRichTextBoxQuestion.ForeColor = Color.Green;
        }

        private void InsertData( )
        {
            //загрузка всех ответов
            //_allAnswers.AddRange(ActiveRecordBase<Answer>.FindAll());

            //загрузка и обновление вопросов
            RefreshQuestions( );
        }

        private void InsertQuestion( int quesitonOrder )
        {
            try
            {
                if (_change && _questions.Count > 0)
                {
                    if (MessageForm.ShowYesNoQuestionMsg( "Вопрос был изменен. Сохранить?" )
                        == DialogResult.Yes)
                    {
                        if (!SaveQuestion( ))
                        {
                            return;
                        }
                    }
                    _change = false;
                }

                int MaxOrder = 1;
                if (quesitonOrder == -1)
                {
                    foreach (ControlQuestion controlQuestion in _questions)
                    {
                        if (controlQuestion.QuestionOrder >= MaxOrder)
                            MaxOrder = controlQuestion.QuestionOrder + 1;
                    }
                    if (MaxOrder == -1)
                    {
                        //нет вопросов. Это првый вопрос
                        MaxOrder = 1;
                    }
                }
                else
                {
                    foreach (ControlQuestion controlQuestion in _questions)
                    {
                        if (controlQuestion.QuestionOrder == quesitonOrder)
                        {
                            MessageForm.ShowErrorMsg( string.Format( "Вопрос с порядковым номером {0} уже есть в тесте",
                                                                   quesitonOrder ) );
                            return;
                        }
                        MaxOrder = quesitonOrder;
                    }
                }
                //создаем вопрос
                var q = new Question( new Content( "" ), null );
                q.Save( );

                var newQuestion = new ControlQuestion( _method, q, null, new Content( "" ), MaxOrder );
                newQuestion.Save( );
                _questions.Add( newQuestion );
                listBoxControlQuestions.SelectedIndex = listBoxControlQuestions.Items.Add( newQuestion );

                if (listBoxControlQuestions.ItemCount == 0)
                {
                    exRichTextBoxQuestion.Clear( );
                    splitContainerControlNavigation.Panel2.Enabled = false;
                }
                else
                {
                    splitContainerControlNavigation.Panel2.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                Log.Error( ex );
                throw;
            }
        }

        private void InsertAnswer( int answerOrder )
        {
            int MaxOrder = -1;
            if (_currentQuestion == null)
            {
                MessageForm.ShowErrorMsg( "Необходимо создать или выбрать вопрос" );
                return;
            }
            if (answerOrder == -1)
            {
                foreach (HorisontalAnswer uc in _uControls)
                {
                    if (uc.Order >= MaxOrder)
                        MaxOrder = uc.Order + 1;
                }
                if (MaxOrder == -1)
                {
                    //нет ответов. Это первый ответ
                    MaxOrder = 1;
                }
            }
            else
            {
                foreach (HorisontalAnswer uc in _uControls)
                {
                    if (uc.Order == answerOrder)
                    {
                        MessageForm.ShowErrorMsg( string.Format( "Ответ с порядковым номером {0} уже есть в тесте",
                                                               answerOrder ) );
                        return;
                    }
                }

                MaxOrder = answerOrder;
            }


            var ucr = new CreateAnswer( new ObjectWithId( MaxOrder, null ), false, null, _currentQuestion.Question,
                                       _allAnswers );
            ucr.AnswerChanged += ucr_AnswerChanged;
            ucr.InitControl( );
            ucr.ShowInForm( );
        }

        private bool SaveQuestion( )
        {
            if (!_change)
                return true;
            if (_currentQuestion == null)
            {
                MessageForm.ShowErrorMsg( "Для сохранения необходимо создать вопрос" );
                return true;
            }
            Content questionContent = GetContent( exRichTextBoxQuestion );
            if (questionContent == null)
            {
                return false;
            }
            //Content commentContent = GetContent(exRichTextBoxComment);
            //if (commentContent == null)
            //{
            //    MessageForm.ShowWarningMsg("При получении контента вопроса произошла ошибка");
            //    return false;
            //}


            _currentQuestion.Question.Content = questionContent;
            // _currentQuestion.Comment = commentContent;
            _currentQuestion.Question.Save( );
            _change = false;

            return true;
        }

        private void RefreshAnswers( )
        {
            if (_currentQuestion == null)
            {
                _uControls.Clear( );
            }
            IEnumerable<HorisontalAnswer> res =
                _uControls.OrderBy( uc => uc.Order );

            int answersCount = _uControls.Count;
            //проверка на необходимость создания контрола
            //необходимо пересоздание контрола
            tableLayoutPanel1.RowCount = answersCount;
            tableLayoutPanel1.RowStyles.Clear( );
            tableLayoutPanel1.Controls.Clear( );
            if (_currentQuestion == null) //Нет вопроса
            {
                return;
            }
            int i = 0;
            foreach (HorisontalAnswer uc in res)
            {
                tableLayoutPanel1.RowStyles.Add( new RowStyle( SizeType.Percent, 100 / answersCount ) );
                tableLayoutPanel1.Controls.Add( uc.ShowAsControl( ), 0, i );

                i++;
            }
            _currentAnswer = null;
        }


        /// <summary>
        /// метод получения контанта
        /// </summary>
        /// <returns></returns>
        private Content GetContent( ExRichTextBox exRichTextBox )
        {
            if (radioGroupFormat.SelectedIndex == 0)
            {
                string text = exRichTextBox.Text;
                if (string.IsNullOrEmpty( text.Trim( ) ))
                {
                    MessageForm.ShowErrorMsg( "Необходимо ввести ответ" );
                    return null;
                }
                if (text.Length > 255)
                {
                    //длина более чем допустимая в БД
                    if (MessageForm.ShowYesNoQuestionMsg(
                        "Длина текста вопроса более чем допустимая в БД.\nСохранить текст в формате RTF?" )
                        == DialogResult.Yes)
                    {
                        var ms_ = new MemoryStream( );
                        exRichTextBox.SaveFile( ms_, RichTextBoxStreamType.RichText );
                        var c = new Content( ms_.ToArray( ) );
                        if (ms_.Length > Consts.ContentMaxSize)
                        {
                            MessageForm.ShowErrorMsg( "Размер Вашего вопроса составляет " + ms_.Length / 1024 +
                                                     " Кбайт. \nУменьшите размер текста и изображений до " +
                                                     Consts.ContentMaxSize / 1024 + " Кбайт." );
                            return null;
                        }
                        c.Save( );
                        return c;
                    }
                    return null;
                }
                else
                {
                    var c = new Content( text );
                    c.Save( );
                    return c;
                }
            }
            else
            {
                var ms_ = new MemoryStream( );
                exRichTextBox.SaveFile( ms_, RichTextBoxStreamType.RichText );
                var c = new Content( ms_.ToArray( ) );
                if (ms_.Length > Consts.ContentMaxSize)
                {
                    MessageForm.ShowErrorMsg( "Размер Вашего вопроса составляет " + ms_.Length / 1024 +
                                             " Кбайт. \nУменьшите размер текста и изображений до " +
                                             Consts.ContentMaxSize / 1024 + " Кбайт." );
                    return null;
                }
                c.Save( );
                return c;
            }
        }

        private void RefreshQuestions( )
        {
            _questions.Clear( );
            listBoxControlQuestions.Items.Clear( );
            using (new SessionScope(FlushAction.Never))
            {
                _questions.AddRange(ActiveRecordBase<ControlQuestion>.FindAll(new Order("QuestionOrder", true),
                    Expression.Eq("Method", _method)));
           
                foreach ( ControlQuestion controlQuestion in _questions)
                {
                    listBoxControlQuestions.Items.Add( controlQuestion );
                }
            }

            if (listBoxControlQuestions.ItemCount == 0)
            {
                exRichTextBoxQuestion.Clear( );
                splitContainerControlNavigation.Panel2.Enabled = false;
            }
            else
            {
                splitContainerControlNavigation.Panel2.Enabled = true;
            }

            listBoxControlQuestions.SelectedItem = _currentQuestion;
        }


        private void SetQuestion( ControlQuestion controlQuestion )
        {
            if (_change && _currentQuestion != null)
            {
                if (MessageForm.ShowYesNoQuestionMsg( "Вопрос был изменен. Сохранить?" )
                    == DialogResult.Yes)
                {
                    if (!SaveQuestion( ))
                        return;
                }
                _change = false;
            }
            exRichTextBoxQuestion.Clear( );
            _currentQuestion = controlQuestion;
            //установка вопроса
            if (_currentQuestion.Question.Content.PlainContent == null)
            {
                var ms = new MemoryStream( _currentQuestion.Question.Content.RichContent );
                exRichTextBoxQuestion.LoadFile( ms, RichTextBoxStreamType.RichText );
                radioGroupFormat.SelectedIndex = 1;
            }
            else
            {
                exRichTextBoxQuestion.Text = _currentQuestion.Question.Content.PlainContent;
                radioGroupFormat.SelectedIndex = 0;
            }

            //установка комментария
            if (_currentQuestion.Comment.PlainContent == null)
            {
                var ms = new MemoryStream( _currentQuestion.Comment.RichContent );
                //exRichTextBoxComment.LoadFile(ms, RichTextBoxStreamType.RichText);
            }
            else
            {
                //exRichTextBoxComment.Text = _currentQuestion.Comment.PlainContent;
            }

            QuestionAnswer[] questionAnswers = null;
            
            using (new SessionScope(FlushAction.Never))
            {
                questionAnswers = ActiveRecordBase<QuestionAnswer>.FindAll(new Order("AnswerOrder", true),
                    Expression.Eq("Question",
                        _currentQuestion.
                            Question));
            }

            int answerCount = questionAnswers.Length;
            if (_uControls.Count != answerCount)
            {
                //установка ответа
                _uControls.Clear( );

                foreach (QuestionAnswer questionAnswer in questionAnswers)
                {
                    var uc = new HorisontalAnswer( new ObjectWithId( questionAnswer.AnswerOrder, questionAnswer.Answer ),
                                                  false, _currentQuestion.Question,
                                                  null, _allAnswers );

                    uc.getFocus += uc_getFocus;
                    uc.setRight += uc_setRight;
                    uc.InitControl( );
                    _uControls.Add( uc );
                    //проверка на правильность ответа
                    if (_currentQuestion.RightAnswer != null &&
                        _currentQuestion.RightAnswer.Id == questionAnswer.Answer.Id)
                    {
                        //помечаем правильный ответ
                        uc.SetRightAnswer( true );
                    }
                    else
                    {
                        //помечаем неправильный ответ
                        uc.SetRightAnswer( false );
                    }
                }
                RefreshAnswers( );
            }
            else
            {
                int i = 0;

                foreach (QuestionAnswer questionAnswer in questionAnswers)
                {
                    _uControls[i].SetAnswer( new ObjectWithId( questionAnswer.AnswerOrder, questionAnswer.Answer ),
                                            controlQuestion.Question );
                    //проверка на правильность ответа
                    if (_currentQuestion.RightAnswer != null &&
                        _currentQuestion.RightAnswer.Id == questionAnswer.Answer.Id)
                    {
                        //помечаем правильный ответ
                        _uControls[i].SetRightAnswer( true );
                    }
                    else
                    {
                        //помечаем неправильный ответ
                        _uControls[i].SetRightAnswer( false );
                    }
                    i++;
                }
            }
        }

        #endregion

        #region обработчики событий от стандартных контролов

        private void simpleButtonAddQuestion_Click( object sender, EventArgs e )
        {
            InsertQuestion( -1 );
        }

        private void listBoxControlQuestions_SelectedIndexChanged( object sender, EventArgs e )
        {
            if (listBoxControlQuestions.SelectedIndex == -1)
            {
                _currentQuestion = null;
                return;
            }
            //проверка на изменение вопроса делается внутри метода установки вопроса
            SetQuestion( (ControlQuestion)listBoxControlQuestions.SelectedItem );
            _change = false;
        }

        private void simpleButtonSaveQuestion_Click( object sender, EventArgs e )
        {
            SaveQuestion( );
            listBoxControlQuestions.SelectedItem = _currentQuestion;
            listBoxControlQuestions.Refresh( );
        }

        private void exRichTextBoxQuestion_TextChanged( object sender, EventArgs e )
        {
            _change = true;
        }

        private void simpleButtonDeleteQuestion_Click( object sender, EventArgs e )
        {
            if (listBoxControlQuestions.SelectedIndex == -1)
                return;
            if (MessageForm.ShowYesNoQuestionMsg( "Вы уверены, что хотите удалить вопрос?" )
                == DialogResult.No)
            {
                return;
            }

            _currentQuestion.Delete( );

            _change = false;
            RefreshQuestions( );
            RefreshAnswers( );
        }

        private void simpleButtonInsertQuestion_Click( object sender, EventArgs e )
        {
            InsertQuestion( (int)spinEditQuestionNumber.Value );
        }

        private void simpleButtonAddAnswer_Click( object sender, EventArgs e )
        {
            InsertAnswer( -1 );
        }

        private void simpleButtonChangeAnswer_Click( object sender, EventArgs e )
        {
            if (_currentAnswer == null)
            {
                MessageForm.ShowWarningMsg( "Выберите ответ" );
                return;
            }

            var uc = new CreateAnswer( new ObjectWithId( _currentAnswer.Order, _currentAnswer.answer ),
                                      _currentAnswer.FlagTestAnswer, null, _currentQuestion.Question, _allAnswers );
            uc.AnswerChanged += uc_AnswerChanged;
            uc.InitControl( );
            uc.ShowInForm( );
        }

        private void simpleButton5_Click( object sender, EventArgs e )
        {
            var ofd = new OpenFileDialog( );
            ofd.Filter = "Изображения  |(*.BMP;*.JPG;*.JPEG;*.bmp;*.jpg;*.jpeg)";
            if (ofd.ShowDialog( ) == DialogResult.OK)
            {
                var fInfo = new FileInfo( ofd.FileName );
                int maxLength = 102400; //100 кБайт
                if (fInfo.Length > maxLength)
                {
                    MessageForm.ShowWarningMsg(
                        "Размер изображения (" + fInfo.Length / 1024 +
                        " кБайт) превышает максимальное допустимое значение (" + maxLength / 1024 +
                        " кБайт).\nДля добавления данной картинки уменьшите ее размер." );
                    return;
                }
                exRichTextBoxQuestion.InsertImage( Image.FromFile( ofd.FileName ) );
                radioGroupFormat.SelectedIndex = 1;
                _change = true;
            }
        }

        private void radioGroupFormat_SelectedIndexChanged( object sender, EventArgs e )
        {
            //if (radioGroupFormat.SelectedIndex == 0)
            //{
            //    if (!_plain &&
            //        MessageForm.ShowYesNoQuestionMsg(
            //            "При данном режиме останется только текст. \nОставить текстовый режим форматирования?") ==
            //        DialogResult.No)
            //    {
            //        _plain = false;
            //        radioGroupFormat.SelectedIndex = 1;
            //    }
            //    else
            //        _plain = true;
            //}
            //else
            //    _plain = false;
            _change = true;
        }

        private void simpleButtonInsertAnswer_Click( object sender, EventArgs e )
        {
            InsertAnswer( (int)spinEditAnswerNumber.Value );
        }


        private void simpleButtonDeleteAnswer_Click( object sender, EventArgs e )
        {
            if (_currentAnswer == null)
            {
                MessageForm.ShowWarningMsg( "Выберите ответ" );
                return;
            }
            using (new SessionScope())
            {
                QuestionAnswer[] questionAnswers =
                    ActiveRecordBase<QuestionAnswer>.FindAll(new Order("AnswerOrder", true),
                        Expression.Eq("Question",
                            _currentQuestion.
                                Question));


                //поиск в ответах вопроса
                foreach (QuestionAnswer q in questionAnswers)
                {
                    if (q.Answer.Equals(_currentAnswer.answer) &&
                        (MessageForm.ShowYesNoQuestionMsg("Вы уверены, что хотите удалить ответ?") == DialogResult.Yes))
                    {
                        q.DeleteAndFlush();
                        _uControls.Remove(_currentAnswer);
                        _currentAnswer = null;
                        //обновление ответов
                        RefreshAnswers();
                        return;
                    }
                }
            }
        }

        public override void ParentFormFormClosing( object sender, FormClosingEventArgs e )
        {

            if (_change)
            {
                if (MessageForm.ShowYesNoQuestionMsg( "Текущий вопрос не сохранен. Сохранить?" ) == DialogResult.Yes)
                {
                    SaveQuestion( );
                }
            }

            foreach (ControlQuestion q in _questions)
            {
                QuestionAnswer[] questionAnswers = null;
                using (new SessionScope(FlushAction.Never))
                {
                    questionAnswers = ActiveRecordBase<QuestionAnswer>.FindAll(new Order("AnswerOrder", true), Expression.Eq("Question", q.Question));
                }
                if (questionAnswers.Length == 0)
                {
                    MessageForm.ShowErrorMsg( string.Format( "Для вопроса с номером {0} отсутствуют ответы",
                                                           q.QuestionOrder ) );
                    e.Cancel = true;
                    return;
                }
                if (q.RightAnswer == null)
                {
                    MessageForm.ShowErrorMsg( string.Format( "Для вопроса с номером {0} отсутствует правильный ответ",
                                                           q.QuestionOrder ) );
                    e.Cancel = true;
                    return;
                }

                bool exist = false;
                foreach (QuestionAnswer qa in questionAnswers)
                {
                    if (qa.Answer.Id == q.RightAnswer.Id)
                    {
                        exist = true;
                        break;
                    }
                }
                if (!exist)
                {
                    MessageForm.ShowErrorMsg( string.Format( "Для вопроса с номером {0} отсутствует правильный ответ",
                                                           q.QuestionOrder ) );
                    e.Cancel = true;
                    return;
                }
            }


            base.ParentFormFormClosing( sender, e );
        }

        #endregion

        #region обработчики событий от пользовательских контролов

        /// <summary>
        /// Обработчик события на добавления ответа
        /// </summary>
        /// <param name="answer"></param>
        private bool ucr_AnswerChanged(ObjectWithId answer)
        {
            using (new SessionScope(FlushAction.Never))
            {
                //поиск уже существующего такого ответа
                QuestionAnswer[] questionAnswers =
                    ActiveRecordBase<QuestionAnswer>.FindAll(new Order("AnswerOrder", true),
                        Expression.Eq("Question",
                            _currentQuestion.
                                Question));
                foreach (QuestionAnswer q in questionAnswers)
                {
                    if (q.Answer.Equals(answer.Value))
                    {
                        MessageForm.ShowErrorMsg("Данный ответ уже присутствует в наборе");
                        return false;
                    }
                }


                //добавление ответа в набор
                var questionAnswer = new QuestionAnswer(_currentQuestion.Question, (Answer) answer.Value,
                    answer.ID);
                questionAnswer.CreateAndFlush();
                var uc = new HorisontalAnswer(answer, false, _currentQuestion.Question, null, _allAnswers);
                uc.getFocus += uc_getFocus;
                uc.setRight += uc_setRight;
                uc.InitControl();
                _uControls.Add(uc);
                RefreshAnswers();
                return true;
            }
        }

        /// <summary>
        /// обработчик события на изменения ответа
        /// </summary>
        /// <param name="answer"></param>
        /// <returns></returns>
        private bool uc_AnswerChanged( ObjectWithId answer )
        {
            using (new SessionScope())
            {
                QuestionAnswer[] questionAnswers =
                    ActiveRecordBase<QuestionAnswer>.FindAll(new Order("AnswerOrder", true),
                        Expression.Eq("Question",
                            _currentQuestion.
                                Question));
                //поиск уже существующего такого ответа
                foreach (QuestionAnswer q in questionAnswers)
                {
                    if (q.Answer.Equals(answer.Value) && q.AnswerOrder != answer.ID)
                    {
                        MessageForm.ShowErrorMsg("Данный ответ уже присутствует в наборе");
                        return false;
                    }
                }


                foreach (QuestionAnswer questionAnswer in questionAnswers)
                {
                    if (questionAnswer.AnswerOrder == answer.ID)
                    {
                        if (!questionAnswer.Answer.Equals(answer.Value))
                        {
                            questionAnswer.Delete();
                            var newQuestionAnswer = new QuestionAnswer(_currentQuestion.Question, (Answer) answer.Value,
                                answer.ID);
                            newQuestionAnswer.Save();
                        }
                        //обновляем ответ
                        _currentAnswer.SetAnswer(answer, _currentQuestion.Question);

                        return true;
                    }
                }
                return true;
            }
        }

        private void uc_getFocus( HorisontalAnswer sender )
        {
            foreach (HorisontalAnswer horisontalAnswer in _uControls)
            {
                horisontalAnswer.BackgroundImage = null;
            }
            _currentAnswer = sender;
            sender.BackgroundImage = Resources.back;
        }

        private void uc_setRight( HorisontalAnswer sender )
        {
            foreach (HorisontalAnswer horisontalAnswer in _uControls)
            {
                //убираем все другие галочки
                if (sender.RightAnswer && !sender.Equals( horisontalAnswer ))
                {
                    horisontalAnswer.SetRightAnswer( false );
                }
            }
            if (sender.RightAnswer)
            {
                _currentQuestion.RightAnswer = sender.answer;
                _currentQuestion.Save( );
                sender.SetRightAnswer( true );
            }
        }

        #endregion

        private void toolStripMenuItem1_Click( object sender, EventArgs e )
        {
            exRichTextBoxQuestion.Paste( );
        }
    }
}
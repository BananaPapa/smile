﻿using System;
using System.ComponentModel;
using System.Configuration;

namespace Eureca.Professional.TestingPlugInStandard
{
    /// <summary>
    /// Контейнер настроек передачи данных.
    /// </summary>
    [Serializable]
    public class TestSettings
    {
        /// <summary>
        /// Конструктор по умолчанию. Устанавливает исходные значения.
        /// </summary>
        public TestSettings()
        {
            Random = true;
            Many = false;
            AllowUserSelectQuestion = false;
            AllowUserSkipQuestion = false;
        }


        /// <summary>
        /// Если включено, то письма удаляются с сервера после сохранения на диск.
        /// </summary>
        [
            ConfigurationProperty("Random"),
            Category("1.Ответы"),
            DisplayName("Перемешивать ответы"),
            DefaultValue(true),
            Description("Если включено, то ответы перемешиваются в тесте"),
            PropertyIndex(0)
        ]
        public bool Random { get; set; }

        /// <summary>
        /// Позволяет определить делать множественный выбор или нет
        /// </summary>
        [
            ConfigurationProperty("Many"),
            Category("1.Ответы"),
            DisplayName("Множественный выбор ответов"),
            DefaultValue(false),
            Description("Позволяет определить делать множественный выбор или нет"),
            PropertyIndex(1)
        ]
        public bool Many { get; set; }

        /// <summary>
        /// Позволяет определить, разрешать пользователям выбирать ответ
        /// </summary>
        [
            ConfigurationProperty("AllowUserSelectQuestion"),
            Category("1.Ответы"),
            DisplayName("Возможность выбирать ответ"),
            DefaultValue(false),
            Description("Позволяет определить, разрешать пользователям выбирать ответ"),
            PropertyIndex(2)
        ]
        public bool AllowUserSelectQuestion { get; set; }

        /// <summary>
        /// Позволяет определить, разрешать пользователям пропускать ответ или нет
        /// </summary>
        [
            ConfigurationProperty("AllowUserSkipQuestion"),
            Category("1.Ответы"),
            DisplayName("Возможность пропустить вопрос"),
            DefaultValue(false),
            Description("Позволяет определить, разрешать пользователям пропускать ответ или нет"),
            PropertyIndex(4)
        ]
        public bool AllowUserSkipQuestion { get; set; }


        /// <summary>
        /// получении типа
        /// </summary>
        /// <returns>Тип класса.</returns>
        public static Type ClassType()
        {
            return new TestSettings().GetType();
        }

        /// <summary>
        /// получение класса установок по xml сериализации
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        public static TestSettings GetSettings(string xml)
        {
            var ts = (TestSettings) TestDetailsSerializer.Deserialize(ClassType(), xml);
            if (ts == null)
            {
                return new TestSettings();
            }
            return ts;
        }
    }
}
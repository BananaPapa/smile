﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Text;
using System.Xml.Serialization;
using Castle.ActiveRecord;
using Eureca.Professional.ActiveRecords;

namespace Eureca.Professional.TestingPlugInStandard
{
    /// <summary>
    /// для вывода описания объекта
    /// </summary>
    public enum ObjectDescription
    {
        None,
        Method,
        Test
    } ;

    /// <summary>перечисление состояний вопросов
    /// внимание. Картинки в панели навигации берутся по этим состояниям</summary>
    public enum WorkQuestionState
    {
        /// <summary>нет состояния(еще не отвечал и не переходил на этот вопрос)</summary>
        None = -1,
        /// <summary>текущий вопрос на котором пользователь в данный момент</summary>
        Current = 0,
        /// <summary>вопрос ушел в режим ожидания(пользователь на него зашел, но не ответил)</summary>
        Wait = 1,
        /// <summary>Отвеченый вопрос</summary>
        Ok = 2,
        /// <summary>Неотвеченый вопрос(истекло время вопроса либо теста)</summary>
        Error = 3
    }

    /// <summary>Класс - контейнер</summary>
    public class WorkQuestion
    {
        /// <summary>
        /// правильный ответ
        /// только для контрольных вопросов
        /// </summary>
        public Answer RightAnswer;

        /// <summary>Порядковый номер </summary>
        public int _order;

        /// <summary>Вопрос</summary>
        public Question _question;

        /// <summary>Состояние</summary>
        public WorkQuestionState _state;

        /// <summary>Время до завершения вопроса
        /// положительное или -1 если отсутствует</summary>
        public int _time;

        /// <summary>
        /// класс контейнер для вопроса. объединение необходимых параметров для работы с вопросом
        /// </summary>
        /// <param name="question">вопрос</param>
        /// <param name="order">порядковый номер</param>
        /// <param name="state">состояние вопроса</param>
        public WorkQuestion(Question question, int order, WorkQuestionState state)
        {
            _question = question;
            _state = state;
            _order = order;
            if (question.TimeLimit != null && question.TimeLimit > 0)
            {
                _time = (int) question.TimeLimit;
            }
            else
            {
                //если в темте время не установлено, то устанавливаем в -1
                _time = -1;
            }
        }
    }

    /// <summary>
    /// класс - атрибут для представления свойств
    /// </summary>
    public class PropertyIndexAttribute : Attribute
    {
        private readonly int _index;

        public PropertyIndexAttribute(int index)
        {
            _index = index;
        }

        public int Index
        {
            get { return _index; }
        }

        [SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters")]
        public static PropertyIndexAttribute Get(Type type)
        {
            return (PropertyIndexAttribute) GetCustomAttribute(type,
                                                               typeof (PropertyIndexAttribute));
        }
    }


    /// <summary>
    /// Sign details serializer.
    /// </summary>
    public static class TestDetailsSerializer
    {
        /// <summary>
        /// Serializes object.
        /// </summary>
        /// <param name="o">object to serialize.</param>
        /// <returns>Serialized object.</returns>
        public static string Serialize(object o)
        {
            if (o == null)
            {
                return String.Empty;
            }
            else
            {
                var stream = new MemoryStream();

                try
                {
                    var serializer = new XmlSerializer(o.GetType());
                    serializer.Serialize(stream, o);

                    string xml = Encoding.UTF8.GetString(stream.GetBuffer());

                    stream.Dispose();

                    return xml;
                }
                catch (Exception)
                {
                    return String.Empty;
                }
                finally
                {
                    stream.Dispose();
                }
            }
        }

        /// <summary>
        /// Deserialized object.
        /// </summary>
        /// <param name="type">Type of object.</param>
        /// <param name="xml">Serialized object.</param>
        /// <returns>Instance of type of object.</returns>
        public static object Deserialize(Type type, string xml)
        {
            if (xml == null)
            {
                return null;
            }

            var stream = new MemoryStream(Encoding.UTF8.GetBytes(xml));

            try
            {
                var serializer = new XmlSerializer(type);

                return serializer.Deserialize(stream);
            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                stream.Dispose();
            }
        }
    }


    public class Consts
    {
        public const int ContentMaxSize = 3072000;
    }
}
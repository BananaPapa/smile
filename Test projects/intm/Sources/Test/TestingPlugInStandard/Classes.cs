﻿
using Eureca.Professional.ActiveRecords;

namespace Eureca.Professional.TestingPlugInStandard
{
    internal class ScaleCoefficient
    {
        private readonly RawPoint _coefficient;
        private readonly Scale _scale;

        internal ScaleCoefficient(Scale scale, RawPoint coefficient)
        {
            _scale = scale;
            _coefficient = coefficient;
        }

        public string ScaleName
        {
            get { return _scale.ToString(); }
        }

        public float Coefficient
        {
            get { return _coefficient.Value; }
            set { _coefficient.Value = value; }
        }

        public Scale scale
        {
            get { return _scale; }
        }

        public int ScaleId
        {
            get { return _scale.Id; }
        }

        public RawPoint rawPointsCoefficient
        {
            get { return _coefficient; }
        }
    }
}
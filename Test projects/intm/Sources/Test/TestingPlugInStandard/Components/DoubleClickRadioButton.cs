using System.Windows.Forms;

namespace Eureca.Professional.TestingPlugInStandard.Components
{
    public class DoubleClickRadioButton : RadioButton
    {
        public DoubleClickRadioButton()
        {
            SetStyle(ControlStyles.StandardClick | ControlStyles.StandardDoubleClick, true);
        }
    }
}
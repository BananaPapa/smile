﻿using System;
using System.Reflection;
using Eureca.Professional.CommonClasses;
using log4net;
using Microsoft.Office.Interop.Word;

namespace Eureca.Professional.TestingPlugInStandard
{
    /// <summary>
    /// класс для работы с word документом
    /// </summary>
    internal class WordManager
    {
        private static readonly ILog Log = LogManager.GetLogger( typeof( WordManager ) );

        #region Fields

        //используются в разных функциях
        private static object _wdSaveChanges = WdSaveOptions.wdDoNotSaveChanges;
        private object _wdFormat = WdOriginalFormat.wdOriginalDocumentFormat;
        private object _objFalse = false;
        private  Application _wa;

        #endregion

        #region Properties

        public Document WordDoc { get; set; }

        #endregion

        /// <summary>
        /// открытие существующего документа
        /// </summary>
        /// <param name="filePath"></param>
        public void Open(string filePath)
        {
            //создаем документ ворд
            _wa = new Application();
            //wa.Visible = true;
            object fileName = filePath;
            object optional = Missing.Value;
            object visible = false;
            object readOnly = false;


            WordDoc = _wa.Documents.Open(ref fileName, ref optional,
                                        ref readOnly, ref optional,
                                        ref optional, ref optional,
                                        ref optional, ref optional,
                                        ref optional, ref optional,
                                        ref optional, ref visible,
                                        ref optional, ref optional,
                                        ref optional, ref optional);
        }

        /// <summary>
        /// создание нового документа
        /// </summary>
        public void New()
        {
            //создаем документ ворд
            _wa = new Application();
            object visible = true;
            object missing = Missing.Value;

            WordDoc = _wa.Documents.Add(ref missing, ref missing, ref missing, ref visible);
        }

        /// <summary>
        /// создание документа по статическому шаблону
        /// </summary>
        /// <param name="questinoCount"></param>
        /// <param name="stenCount"></param>
        public void CreateTemplateDocument(int questinoCount, int stenCount)
        {
            object oMissing = Missing.Value;
            Object defaultTableBehavior =
                WdDefaultTableBehavior.wdWord9TableBehavior;
            Object autoFitBehavior =
                WdAutoFitBehavior.wdAutoFitWindow;
            object unit = WdUnits.wdStory;
            object extend = WdMovementType.wdMove;


            WordDoc.Select();
            _wa.Selection.Range.Text = "Общие сведения о тесте";

            _wa.Selection.EndKey(ref unit, ref extend);

            WordDoc.Tables.Add(_wa.Selection.Range, 12, 2, ref defaultTableBehavior, ref autoFitBehavior);

            //Сдвигаемся вниз в конец документа
            WordDoc.Select();
            _wa.Selection.EndKey(ref unit, ref extend);
            WordDoc.Paragraphs.Add(ref oMissing);

            WordDoc.Select();
            _wa.Selection.EndKey(ref unit, ref extend);
            WordDoc.Paragraphs.Add(ref oMissing);

            _wa.Selection.Range.Text = "Порядок оценки теста";
            //wordDoc.Select();
            //wordDoc.Paragraphs.Add(ref oMissing);
            WordDoc.Select();
            _wa.Selection.EndKey(ref unit, ref extend);
            WordDoc.Paragraphs.Add(ref oMissing);
            //Вставляем таблицу по месту курсора
            WordDoc.Tables.Add(_wa.Selection.Range, stenCount + 1, 5, ref defaultTableBehavior,
                               ref autoFitBehavior);

            WordDoc.Select();
            _wa.Selection.EndKey(ref unit, ref extend);
            WordDoc.Paragraphs.Add(ref oMissing);

            for (int i = 0; i < questinoCount; i++)
            {
                Log.Error("Создание таблицы для вопроса " + (i + 1));
                _wa.Selection.Range.Text = "Вопрос № " + (i + 1);
                WordDoc.Select();
                _wa.Selection.EndKey(ref unit, ref extend);

                WordDoc.Paragraphs.Add(ref oMissing);
                //Вставляем таблицу по месту курсора
                WordDoc.Tables.Add(_wa.Selection.Range, 2, 2, ref defaultTableBehavior,
                                   ref autoFitBehavior);

                WordDoc.Select();
                _wa.Selection.EndKey(ref unit, ref extend);
                WordDoc.Paragraphs.Add(ref oMissing);
            }
        }

        /// <summary>
        /// метод сохранения файла
        /// </summary>
        /// <param name="filePath"></param>
        public void SaveAs(object filePath)
        {
            try
            {
                object missing = Missing.Value;
                if (filePath == null)
                    throw new ArgumentNullException("filePath");
                if (WordDoc == null)
                    throw new Exception("WordDoc = null");
                WordDoc.SaveAs(ref filePath,
                               ref missing, ref missing, ref missing, ref missing, ref missing,
                               ref missing, ref missing, ref missing, ref missing, ref missing,
                               ref missing, ref missing, ref missing, ref missing, ref missing);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw;
            }
        }

        /// <summary>
        /// закрытие процесса
        /// </summary>
        public bool KillWinWord()
        {
            try
            {
                if (_wa != null)
                {
                    _wa.Quit(ref _wdSaveChanges, ref _wdFormat, ref _objFalse); //убить процесс WINWORD.EXE
                    _wa = null;
                    return true;
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return false;
            }
        }
    }
}
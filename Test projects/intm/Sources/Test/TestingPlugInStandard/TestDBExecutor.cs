﻿using System;
using Castle.ActiveRecord;
using Eureca.Professional.ActiveRecords;
using Eureca.Professional.CommonClasses;
using Eureca.Professional.CommonClasses.Data;
using log4net;
using NHibernate.Hql.Ast.ANTLR;


namespace Eureca.Professional.TestingPlugInStandard
{
    public static class TestDBExecutor
    {
        private static readonly ILog Log = LogManager.GetLogger( typeof( TestDBExecutor ) );
        public static void DropTest(Test test)
        {
            #region старое удаление
            
            //try
            //{
            //    DBExecutor.BeginTransaction();
            //    //удаление ссылок на ответы теста
            //    string querry = string.Format("delete from TestAnswers where Testid = {0}", test.Id);
            //    DBExecutor.ExecNonQuery(querry);
            //    //удаление ссылок на ответы вопросов
            //    querry = string.Format("delete from questionanswers where questionid in (select questionid from " +
            //                           "testquestions where testid = {0})", test.Id);
            //    DBExecutor.ExecNonQuery(querry);
            //    //удаление ссылок на вопросы
            //    querry = string.Format("delete from testquestions where testid = {0}", test.Id);
            //    DBExecutor.ExecNonQuery(querry);
            //    //удаление сырых баллов
            //    querry = string.Format("delete from rawpoints where testid = {0}", test.Id);
            //    DBExecutor.ExecNonQuery(querry);
            //    //удаление оценок
            //    querry = string.Format("delete from stens where testid = {0}", test.Id);
            //    DBExecutor.ExecNonQuery(querry);
            //    //удаление шкал
            //    querry = string.Format("delete from testScales where testid = {0}", test.Id);
            //    DBExecutor.ExecNonQuery(querry);
            //    //удаление прикрепленных файлов
            //    querry = string.Format("delete from testattachments where testid = {0}", test.Id);
            //    DBExecutor.ExecNonQuery(querry);
            //    //удаление отчетов
            //    querry = string.Format("delete from testreports where testid = {0}", test.Id);
            //    DBExecutor.ExecNonQuery(querry);
            //    //удаление результатов тестирования
            //    querry = string.Format("delete from testresults where testpassingid in " +
            //                           "(select testpassingid from testpassings where testid = {0})", test.Id);

            //    DBExecutor.ExecNonQuery(querry);
            //    //удаление прохождения тестов
            //    querry = string.Format("delete from testpassings where testid = {0}", test.Id);
            //    DBExecutor.ExecNonQuery(querry);
            //    //удаление теста из профилей
            //    querry = string.Format("delete from profiletests where testid = {0}", test.Id);
            //    DBExecutor.ExecNonQuery(querry);
            //    //удаление теста из таблицы тестов
            //    querry = string.Format("delete from tests where testid = {0}", test.Id);
            //    DBExecutor.ExecNonQuery(querry);

            //    DBExecutor.CommitTransaction();
            //}
            #endregion

            //test.Delete();//AndFlush();

            try
            {
                DBExecutor.BeginTransaction( );
                DBExecutor.ExecNonQuery( string.Format( "delete from tests where testid = {0}", test.Id ) );
                DBExecutor.CommitTransaction( );

            }
            catch (Exception ex)
            {
                Log.Error( ex );
                DBExecutor.RollbackTransaction( );
                throw;
            }
            try
            {
                //очистка БД
                DBExecutor.ExecNonQuery( "exec ClearDataBase" );
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw;
            }
        }
        /// <summary>
        /// метод очистки БД от лишней информации
        /// </summary>
        /// <returns></returns>
        public static int ClearDataBase()
        {
            string querry =
                string.Format("exec cleardatabase");
            return DBExecutor.ExecNonQuery(querry);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Castle.ActiveRecord;
using DevExpress.XtraEditors;
using Eureca.Professional.ActiveRecords;
using Eureca.Utility.Attributes;
using Eureca.Utility.Helper;

namespace Eureca.Professional.FillingFormsTest
{
    public class Constants
    {
       public const string PlugInName = "FillingFormTest";
       public const int ContentMaxSize = 3072000;
    }

    public static class Helper
    {
        public static RtfForm ApplyBlancChanges( int blancId, MemoryStream blancRtf, BindingListWithRemoveEvent<IComment> comments )
        {
            var newComments = new List<Comment>( );
           
            foreach (var removedItem in comments.RemovedItems)
            {
                var comment = removedItem as Comment;
                if (comment != null)
                    comment.Delete( );
            }

            foreach (var changedItem in comments.ChangedItems)
            {
                var comment = changedItem as Comment;
                if(comment.Id >0)
                    comment = Comment.Find(comment.Id);
                if (comment != null)
                    comment.UpdateAndFlush( );
            }
            RtfForm blanc = null;
            //using (new SessionScope( ))
            //{
                blanc = RtfForm.Find( blancId );
                blanc.Content = blancRtf.GetBuffer( );
               
            //}
            //blanc.UpdateAndFlush( );
            foreach (var addedItem in comments.AddedItems)
            {
                var comment = new Comment(blanc, addedItem.CommentText, addedItem.CommentType, addedItem.StartPosition,
                    addedItem.EndPosition );
                blanc.Comments.Add(comment);
            }

            //foreach (var newComment in newComments)
            //{
            //    blanc.Comments.Add( newComment );
            //}
            blanc.Update( );
            return blanc;
        }

    }

    public enum BlancCompletnessType
    {
        [Color( "#FFFFFF")]
        Unknown,
        [Color( "#FF9C9C" )]
        Unfilled,
        [Color( "#90EE90")]
        Filled,
        [Color( "#FF9C9C" )]
        Unnecessary
    }
}

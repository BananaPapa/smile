﻿namespace Eureca.Professional.FillingFormsTest.Gui
{
    partial class AssigmentBlancGrid
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if (disposing && ( components != null ))
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            this._blancsGrid = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._blancNameColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this._blancsGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // _blancsGrid
            // 
            this._blancsGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._blancsGrid.Location = new System.Drawing.Point(0, 0);
            this._blancsGrid.MainView = this.gridView1;
            this._blancsGrid.Name = "_blancsGrid";
            this._blancsGrid.Size = new System.Drawing.Size(452, 663);
            this._blancsGrid.TabIndex = 0;
            this._blancsGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._blancNameColumn});
            this.gridView1.GridControl = this._blancsGrid;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            this.gridView1.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.gridView1_RowStyle);
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.CompareStandartsForm_FocusedRowChanged);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            // 
            // _blancNameColumn
            // 
            this._blancNameColumn.Caption = "Название бланка";
            this._blancNameColumn.FieldName = "FullPath";
            this._blancNameColumn.Name = "_blancNameColumn";
            this._blancNameColumn.OptionsColumn.AllowEdit = false;
            this._blancNameColumn.OptionsColumn.ReadOnly = true;
            this._blancNameColumn.Visible = true;
            this._blancNameColumn.VisibleIndex = 0;
            // 
            // AssigmentBlancGrid
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._blancsGrid);
            this.Name = "AssigmentBlancGrid";
            this.Size = new System.Drawing.Size(452, 663);
            ((System.ComponentModel.ISupportInitialize)(this._blancsGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl _blancsGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn _blancNameColumn;
    }
}

﻿namespace Eureca.Professional.FillingFormsTest
{
    partial class CompareStandartsControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if (disposing && ( components != null ))
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CompareStandartsControl));
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.xtraTabControl = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this._assigmentBlancGrid = new Eureca.Professional.FillingFormsTest.Gui.AssigmentBlancGrid();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this._assignmentTextRichEdit = new DevExpress.XtraRichEdit.RichEditControl();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this._resultRichTextBox = new Eureca.Professional.FillingFormsTest.RtfMarkUpTextControl();
            this._standartRichTextBox = new DevExpress.XtraRichEdit.RichEditControl();
            this._callapseButton = new DevExpress.XtraEditors.CheckButton();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl)).BeginInit();
            this.xtraTabControl.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            this.SuspendLayout();
            // 
            // gridView1
            // 
            this.gridView1.Name = "gridView1";
            // 
            // gridView2
            // 
            this.gridView2.Name = "gridView2";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.xtraTabControl);
            this.splitContainerControl1.Panel1.ShowCaption = true;
            this.splitContainerControl1.Panel1.Text = "Бланки";
            this.splitContainerControl1.Panel2.Controls.Add(this.splitContainerControl2);
            this.splitContainerControl1.Panel2.Controls.Add(this._callapseButton);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(920, 656);
            this.splitContainerControl1.SplitterPosition = 287;
            this.splitContainerControl1.TabIndex = 6;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // xtraTabControl
            // 
            this.xtraTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl.Name = "xtraTabControl";
            this.xtraTabControl.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl.Size = new System.Drawing.Size(287, 656);
            this.xtraTabControl.TabIndex = 0;
            this.xtraTabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this._assigmentBlancGrid);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(282, 630);
            this.xtraTabPage1.Text = "Бланки";
            // 
            // _assigmentBlancGrid
            // 
            this._assigmentBlancGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._assigmentBlancGrid.Location = new System.Drawing.Point(0, 0);
            this._assigmentBlancGrid.Name = "_assigmentBlancGrid";
            this._assigmentBlancGrid.Size = new System.Drawing.Size(282, 630);
            this._assigmentBlancGrid.TabIndex = 1;
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this._assignmentTextRichEdit);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(281, 628);
            this.xtraTabPage2.Text = "Задание";
            // 
            // _assignmentTextRichEdit
            // 
            this._assignmentTextRichEdit.Dock = System.Windows.Forms.DockStyle.Fill;
            this._assignmentTextRichEdit.Location = new System.Drawing.Point(0, 0);
            this._assignmentTextRichEdit.Name = "_assignmentTextRichEdit";
            this._assignmentTextRichEdit.Options.Fields.UseCurrentCultureDateTimeFormat = false;
            this._assignmentTextRichEdit.Options.HorizontalRuler.Visibility = DevExpress.XtraRichEdit.RichEditRulerVisibility.Hidden;
            this._assignmentTextRichEdit.Options.MailMerge.KeepLastParagraph = false;
            this._assignmentTextRichEdit.Options.VerticalRuler.Visibility = DevExpress.XtraRichEdit.RichEditRulerVisibility.Hidden;
            this._assignmentTextRichEdit.ReadOnly = true;
            this._assignmentTextRichEdit.Size = new System.Drawing.Size(281, 628);
            this._assignmentTextRichEdit.TabIndex = 0;
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.None;
            this.splitContainerControl2.Location = new System.Drawing.Point(19, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.splitContainerControl2.Panel1.Controls.Add(this._resultRichTextBox);
            this.splitContainerControl2.Panel1.ShowCaption = true;
            this.splitContainerControl2.Panel1.Text = "Результат";
            this.splitContainerControl2.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.splitContainerControl2.Panel2.Controls.Add(this._standartRichTextBox);
            this.splitContainerControl2.Panel2.ShowCaption = true;
            this.splitContainerControl2.Panel2.Text = "Образец ";
            this.splitContainerControl2.Size = new System.Drawing.Size(608, 656);
            this.splitContainerControl2.SplitterPosition = 297;
            this.splitContainerControl2.TabIndex = 18;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // _resultRichTextBox
            // 
            this._resultRichTextBox.CommentColor = System.Drawing.Color.Empty;
            this._resultRichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this._resultRichTextBox.ExtendedFormating = true;
            this._resultRichTextBox.HideCommentsList = true;
            this._resultRichTextBox.HidePanel = true;
            this._resultRichTextBox.Location = new System.Drawing.Point(0, 0);
            this._resultRichTextBox.MarkerColor = System.Drawing.Color.Empty;
            this._resultRichTextBox.Name = "_resultRichTextBox";
            this._resultRichTextBox.ReadOnly = true;
            this._resultRichTextBox.Rtf = ((System.IO.MemoryStream)(resources.GetObject("_resultRichTextBox.Rtf")));
            this._resultRichTextBox.Size = new System.Drawing.Size(293, 633);
            this._resultRichTextBox.TabIndex = 0;
            // 
            // _standartRichTextBox
            // 
            this._standartRichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this._standartRichTextBox.Location = new System.Drawing.Point(0, 0);
            this._standartRichTextBox.Name = "_standartRichTextBox";
            this._standartRichTextBox.Options.Fields.UseCurrentCultureDateTimeFormat = false;
            this._standartRichTextBox.Options.HorizontalRuler.Visibility = DevExpress.XtraRichEdit.RichEditRulerVisibility.Hidden;
            this._standartRichTextBox.Options.MailMerge.KeepLastParagraph = false;
            this._standartRichTextBox.Options.VerticalRuler.Visibility = DevExpress.XtraRichEdit.RichEditRulerVisibility.Hidden;
            this._standartRichTextBox.ReadOnly = true;
            this._standartRichTextBox.Size = new System.Drawing.Size(301, 633);
            this._standartRichTextBox.TabIndex = 0;
            // 
            // _callapseButton
            // 
            this._callapseButton.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat;
            this._callapseButton.Dock = System.Windows.Forms.DockStyle.Left;
            this._callapseButton.Image = ((System.Drawing.Image)(resources.GetObject("_callapseButton.Image")));
            this._callapseButton.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this._callapseButton.Location = new System.Drawing.Point(0, 0);
            this._callapseButton.Margin = new System.Windows.Forms.Padding(0);
            this._callapseButton.MaximumSize = new System.Drawing.Size(19, 0);
            this._callapseButton.Name = "_callapseButton";
            this._callapseButton.Size = new System.Drawing.Size(19, 656);
            this._callapseButton.TabIndex = 16;
            this._callapseButton.ToolTip = "Свернуть";
            this._callapseButton.CheckedChanged += new System.EventHandler(this._callapseButton_CheckedChanged);
            // 
            // CompareStandartsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainerControl1);
            this.Name = "CompareStandartsControl";
            this.Size = new System.Drawing.Size(920, 656);
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl)).EndInit();
            this.xtraTabControl.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.CheckButton _callapseButton;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private Gui.AssigmentBlancGrid _assigmentBlancGrid;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraRichEdit.RichEditControl _assignmentTextRichEdit;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraRichEdit.RichEditControl _standartRichTextBox;
        private RtfMarkUpTextControl _resultRichTextBox;

    }
}
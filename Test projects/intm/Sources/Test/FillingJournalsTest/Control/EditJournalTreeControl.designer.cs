﻿namespace Eureca.Professional.FillingFormsTest.Gui
{
    partial class EditJournalTreeControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if (disposing && ( components != null ))
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            this.components = new System.ComponentModel.Container();
            this._treeContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._addSubSectionMI = new System.Windows.Forms.ToolStripMenuItem();
            this._addBlancMI = new System.Windows.Forms.ToolStripMenuItem();
            this._deleteNodeMI = new System.Windows.Forms.ToolStripMenuItem();
            this._journalTreeView = new Aga.Controls.Tree.TreeViewAdv();
            this._treeContextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // _treeContextMenu
            // 
            this._treeContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._addSubSectionMI,
            this._addBlancMI,
            this._deleteNodeMI});
            this._treeContextMenu.Name = "_treeContextMenu";
            this._treeContextMenu.Size = new System.Drawing.Size(187, 70);
            this._treeContextMenu.Opening += new System.ComponentModel.CancelEventHandler(this._treeContextMenu_Opening);
            // 
            // _addSubSectionMI
            // 
            this._addSubSectionMI.Name = "_addSubSectionMI";
            this._addSubSectionMI.Size = new System.Drawing.Size(186, 22);
            this._addSubSectionMI.Text = "Добавить подраздел";
            this._addSubSectionMI.Click += new System.EventHandler(this._addSubSectionMI_Click);
            // 
            // _addBlancMI
            // 
            this._addBlancMI.Name = "_addBlancMI";
            this._addBlancMI.Size = new System.Drawing.Size(186, 22);
            this._addBlancMI.Text = "Добавить бланк";
            this._addBlancMI.Click += new System.EventHandler(this._addBlancMI_Click);
            // 
            // _deleteNodeMI
            // 
            this._deleteNodeMI.Name = "_deleteNodeMI";
            this._deleteNodeMI.Size = new System.Drawing.Size(186, 22);
            this._deleteNodeMI.Text = "Удалить ветку";
            this._deleteNodeMI.Click += new System.EventHandler(this._deleteNodeMI_Click);
            // 
            // _journalTreeView
            // 
            this._journalTreeView.AllowDrop = true;
            this._journalTreeView.BackColor = System.Drawing.SystemColors.Window;
            this._journalTreeView.ContextMenuStrip = this._treeContextMenu;
            this._journalTreeView.DefaultToolTipProvider = null;
            this._journalTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this._journalTreeView.DragDropMarkColor = System.Drawing.Color.Black;
            this._journalTreeView.DrawPerformanceStat = false;
            this._journalTreeView.FullRowSelect = true;
            this._journalTreeView.LineColor = System.Drawing.SystemColors.ControlDark;
            this._journalTreeView.LoadOnDemand = true;
            this._journalTreeView.Location = new System.Drawing.Point(0, 0);
            this._journalTreeView.Model = null;
            this._journalTreeView.Name = "_journalTreeView";
            this._journalTreeView.SelectedNode = null;
            this._journalTreeView.Size = new System.Drawing.Size(132, 132);
            this._journalTreeView.TabIndex = 0;
            this._journalTreeView.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this._journalTreeView_ItemDrag);
            this._journalTreeView.SelectionChanged += new System.EventHandler(this._journalTreeView_SelectionChanged);
            this._journalTreeView.DragDrop += new System.Windows.Forms.DragEventHandler(this._journalTreeView_DragDrop);
            this._journalTreeView.DragOver += new System.Windows.Forms.DragEventHandler(this._journalTreeView_DragOver);
            this._journalTreeView.DoubleClick += new System.EventHandler(this._journalTreeView_DoubleClick);
            // 
            // EditJournalTreeControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._journalTreeView);
            this.Name = "EditJournalTreeControl";
            this.Size = new System.Drawing.Size(132, 132);
            this._treeContextMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Aga.Controls.Tree.TreeViewAdv _journalTreeView;
        private System.Windows.Forms.ContextMenuStrip _treeContextMenu;
        private System.Windows.Forms.ToolStripMenuItem _addSubSectionMI;
        private System.Windows.Forms.ToolStripMenuItem _addBlancMI;
        private System.Windows.Forms.ToolStripMenuItem _deleteNodeMI;


    }
}

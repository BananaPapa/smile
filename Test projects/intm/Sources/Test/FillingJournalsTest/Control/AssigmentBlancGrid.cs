﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Castle.ActiveRecord;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraPrinting.Native;
using Eureca.Professional.ActiveRecords;
using Eureca.Utility.Attributes;
using Eureca.Utility.Extensions;
using NHibernate.Criterion;

namespace Eureca.Professional.FillingFormsTest.Gui
{
    public partial class AssigmentBlancGrid : XtraUserControl
    {
        private readonly Dictionary<int, RtfForm> _standarts = new Dictionary<int, RtfForm>();
        private readonly Dictionary<int, RtfForm> _filledBlancs = new Dictionary<int, RtfForm>();
        private readonly Dictionary<int, RtfForm> _formsMerge = new Dictionary<int, RtfForm>();
        private readonly List<int> _assignmentBlancs = new List<int>(); 
        public event EventHandler<OnBlancSelectionChangedEventArgs> OnBlancSelectionChanged;

        public event EventHandler<OnBlancSelectionChangedEventArgs> OnBlancDoubleClicked;

        public TestFillingForm SelectedRow
        {
            get
            {
                GridView view = (GridView) _blancsGrid.MainView;
                var rowHandles = view.GetSelectedRows();
                if (rowHandles == null || rowHandles.Length == 0)
                    return null;

                var row = (RtfForm) view.GetRow(rowHandles[0]);
                return new TestFillingForm(GetRowType(view, rowHandles[0]), row);
            }
        }

        public AssigmentBlancGrid()
        {
            InitializeComponent();
        }

        public void LoadData(int testPassingId)
        {
            TestPassing testPass = TestPassing.Find(testPassingId);
            int questionId = 0;
            foreach (
                FormFilling formPair in
                    FillingFormsTestResult.FindAll(Restrictions.Eq("TestPassing", testPass))
                        .Select(item =>
                        {
                            questionId = item.Question.Id;
                            return item.FormFilling;
                        }).Where(filling => filling != null))
            {
                _filledBlancs.Add(formPair.Form.Id, formPair.FilledForm);
                _formsMerge.Add(formPair.Form.Id, formPair.Form);
            }

            if (questionId != 0)
                foreach (FormFilling formPair in Question.Find(questionId).Standarts)
                {
                    if(formPair.FilledForm != null)
                        _standarts.Add(formPair.Form.Id, formPair.FilledForm);
                    _assignmentBlancs.Add(formPair.Form.Id);
                    if (!_formsMerge.ContainsKey(formPair.Form.Id))
                        _formsMerge.Add(formPair.Form.Id, formPair.Form);
                }

            _blancsGrid.DataSource = _formsMerge.Values;

        }

        public Dictionary<int, RtfForm> Standarts
        {
            get { return _standarts; }
        }

        public Dictionary<int, RtfForm> FilledBlancs
        {
            get { return _filledBlancs; }
        }

        public Dictionary<int, RtfForm> FormsMerge
        {
            get { return _formsMerge; }
        }

        private void CompareStandartsForm_FocusedRowChanged(object sender,
            FocusedRowChangedEventArgs e)
        {
            var view = (GridView) sender;
            if (e.FocusedRowHandle < 0 && OnBlancSelectionChanged != null)
            {
                OnBlancSelectionChanged(this, new OnBlancSelectionChangedEventArgs(-1, BlancCompletnessType.Unknown));
                return;
            }

            RtfForm blanc = (RtfForm) view.GetRow(e.FocusedRowHandle);
            if (OnBlancSelectionChanged != null)
                OnBlancSelectionChanged(this,
                    new OnBlancSelectionChangedEventArgs(blanc.Id, GetRowType(view, e.FocusedRowHandle)));
        }

        private void gridView1_RowStyle(object sender, RowStyleEventArgs e)
        {
            var rowType = GetRowType((GridView) sender, e.RowHandle);
            var color = rowType.GetEnumAttribute<ColorAttribute>().FirstOrDefault();
            e.Appearance.BackColor2 = Color.White;
            e.Appearance.BackColor2 = color == null? Color.LightSlateGray : color.Color;
        }

        private BlancCompletnessType GetRowType(GridView view, int rowHandle)
        {
            if (rowHandle >= 0)
            {
                return GetRowType(view, (RtfForm) view.GetRow(rowHandle));
            }
            return BlancCompletnessType.Unknown;
        }

        private BlancCompletnessType GetRowType(GridView view, RtfForm blanc)
        {
            if (blanc != null)
            {
                bool isInAssignment = _assignmentBlancs.Contains( blanc.Id );
                bool isInFilled = FilledBlancs.Any((item => item.Key == blanc.Id));
                if (isInFilled && isInAssignment)
                    return BlancCompletnessType.Filled;
                if (isInFilled)
                    return BlancCompletnessType.Unnecessary;
                if (isInAssignment)
                    return BlancCompletnessType.Unfilled;
            }
            return BlancCompletnessType.Unknown;
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            GridView view = (GridView) sender;

            Point pt = view.GridControl.PointToClient(Control.MousePosition);

            GridHitInfo info = view.CalcHitInfo(pt);

            if ((info.InRow || info.InRowCell) && OnBlancDoubleClicked != null)
            {
                var row = (RtfForm) view.GetRow(info.RowHandle);

                OnBlancDoubleClicked(this, new OnBlancSelectionChangedEventArgs(row.Id, GetRowType(view, row)));
            }
        }

        private void gridView1_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView) sender;
            if (view.GetSelectedRows().Contains(e.RowHandle))
            {
                var rowType = GetRowType(view, e.RowHandle);
                var color = rowType.GetEnumAttribute<ColorAttribute>().FirstOrDefault();
                if (color != null)
                    e.Appearance.BackColor = color.Color;
            }
        }
    }

    public class TestFillingForm
    {
        public BlancCompletnessType FillingType { get; private set; }
        public RtfForm Form { get; private set; }

        public TestFillingForm(BlancCompletnessType fillingType, RtfForm form)
        {
            FillingType = fillingType;
            Form = form;
        }
    }

    public class OnBlancSelectionChangedEventArgs : EventArgs
    {
        public int BlancId { get; set; }
        public BlancCompletnessType BlancType { get; set; }

        public OnBlancSelectionChangedEventArgs(int blancId, BlancCompletnessType blancType)
        {
            BlancId = blancId;
            BlancType = blancType;
        }
    }
}

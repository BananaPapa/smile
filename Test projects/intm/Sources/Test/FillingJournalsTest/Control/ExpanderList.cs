﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

namespace Eureca.Professional.FillingFormsTest
{
    public partial class ExpanderList : XtraUserControl
    {
        private readonly Dictionary<CheckButton, CallapseControlDescriptor> _contantainersList = new Dictionary<CheckButton, CallapseControlDescriptor>( );
         List<Tuple<string, Control,Action<Control>>> _controlItems;
        private const int _titleHeight = 35;
        private int _minControlViewSize = 150;
        private bool _programmResize = false;
        private bool _isInitComplete = false;
        public int MinControlViewSize
        {
            get { return _minControlViewSize; }
            set
            {
                if (_isInitComplete)
                    throw new InvalidOperationException("Can`t change view size after init");
                 _minControlViewSize = value;
            }
        }

        public ExpanderList()
        {
            InitializeComponent();
            Resize += ExpanderList_Resize;
        }

        void ExpanderList_Resize( object sender, EventArgs e )
        {
            if (!_isInitComplete || _programmResize)
                return;
            CalculateHeight();
        }

        public void InitList( List<Tuple<string, Control,Action<Control>>>  itemsList , int openIndex)
        {
            _isInitComplete = true;
            //_tableLayoutPanel.ColumnStyles.Clear();
            _contantainersList.Clear();
            _controlItems = itemsList;
            CalculateHeight();
            _tableLayoutPanel.RowStyles.Clear();
            _tableLayoutPanel.RowCount = _tableLayoutPanel.RowStyles.Count;
            for (int i = 0 ; i < itemsList.Count ;i++)
            {
                bool isOpen = openIndex ==i || (openIndex == -1 && i == itemsList.Count -1);
                AddAutoSizeRow( isOpen);
                var panel = new Panel( ){};
                var btn = new CheckButton(){Text = itemsList[i].Item1 , Height = _titleHeight - 8 , ButtonStyle = BorderStyles.Flat,Checked = isOpen};
                btn.CheckedChanged += btn_CheckedChanged;
                Control control = itemsList[i].Item2;
               
                var desc = new CallapseControlDescriptor(btn, i, control,itemsList[i].Item3);
                btn.Dock = DockStyle.Top;
                panel.Controls.Add(btn);
                control.Dock = DockStyle.Fill;
            
                panel.Controls.Add(control);
                _tableLayoutPanel.Controls.Add(panel);
                panel.Dock = DockStyle.Fill;
                //_tableLayoutPanel.Controls.Add( new Button(){Text = "asdasdasdasd"} );
               
                _contantainersList.Add(desc.Button, desc);
                control.BringToFront( );
                if (!isOpen)
                    control.Visible = false;
            }
            //_tableLayoutPanel.Refresh();
        }

        private void CalculateHeight( )
        {
            int headersHeight = _controlItems.Count*_titleHeight;
            if (this.Height - headersHeight < _minControlViewSize)
            {
                _programmResize = true;
                this.Height = headersHeight + _minControlViewSize;
                _programmResize = false;
            }
            else
            {
            }
        }

        public void AddAutoSizeRow( bool isLast = false )
        {
            if(isLast)
                _tableLayoutPanel.RowStyles.Add(new RowStyle( SizeType.Percent, 100 ));
            else
            {
                _tableLayoutPanel.RowStyles.Add( new RowStyle( SizeType.Absolute, _titleHeight ) );
                 
            }
            _tableLayoutPanel.RowCount = _tableLayoutPanel.RowStyles.Count;  
        }

        void btn_CheckedChanged( object sender, EventArgs e )
        {
            var checkButton = (sender as CheckButton);
            bool newState = checkButton.Checked;
            if (newState)
            {
                
                foreach (var btns in _contantainersList.Keys)
                {
                    if (btns.Checked && !Object.ReferenceEquals(btns, sender))
                    {
                        btns.Checked = false;
                        var uncheckedDesc = _contantainersList[btns];
                        var onCallapseAction = uncheckedDesc.OnCallapseAction;
                        if (onCallapseAction != null)
                            onCallapseAction(uncheckedDesc.Control);
                    }
                }
                var desc = _contantainersList[checkButton];
                desc.Control.Visible = true;
                _tableLayoutPanel.RowStyles[desc.Index] = new RowStyle(SizeType.Percent, 100);
            }
            else
            {
                var desc = _contantainersList[checkButton];
                desc.Control.Visible = false;
                _tableLayoutPanel.RowStyles[desc.Index] = new RowStyle(SizeType.Absolute, _titleHeight);
            }
        }
    }

    public class CallapseControlDescriptor
    {
        private CheckButton _button;
        private int _index;
        private Control _control;
        private Action<Control> _onCallapseAction;
        
        public CallapseControlDescriptor(CheckButton button, int index, Control control,Action<Control> OnCallapse)
        {
            _button = button;
            _index = index;
            _control = control;
            _onCallapseAction = OnCallapse;
        }

        public CheckButton Button
        {
            get { return _button; }
            set { _button = value; }
        }

        public int Index
        {
            get { return _index; }
            set { _index = value; }
        }

        public Control Control
        {
            get { return _control; }
            set { _control = value; }
        }


        public Action<Control> OnCallapseAction
        {
            get { return _onCallapseAction; }
            set { _onCallapseAction = value; }
        }
        
    }
}

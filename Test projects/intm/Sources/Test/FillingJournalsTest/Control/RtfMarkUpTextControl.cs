﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Aga.Controls.Tree;
using DevExpress.Utils.Menu;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraRichEdit;
using DevExpress.XtraRichEdit.API.Native;
using Eureca.Integrator.Extensions;
using log4net;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.Office.Utils;
using Eureca.Utility.Extensions;
using Eureca.Utility.Helper;
using Eureca.Utility.Attributes;
using Eureca.Professional.ActiveRecords;
using NHibernate.Criterion;
using NHibernate.Linq;
using Eureca.Professional.FillingFormsTest.Properties;

namespace Eureca.Professional.FillingFormsTest
{

    public partial class RtfMarkUpTextControl : XtraUserControl
    {
        Dictionary<IComment, DocumentRange> _watchCommentsDic = new Dictionary<IComment, DocumentRange>( ); 

        readonly static ILog Log = LogManager.GetLogger( typeof( RtfMarkUpTextControl ) );
        const int _deltaThreshold = 8;
        
        private DXMenuItem _commentMenuItem, _selectMenuItem,_removeCommentMenuItem,_editCommentMenuItem;
        //bool _isInSelection = false;
        //bool _isMarkerEnabled = false;
        bool _readOnly = false;
        int _lastX=-1, _lastY=-1;
        IComment _lastSelectedComment = null;
        IComment _currentComment = null;
        ToolTip _commentToolTip = new ToolTip( );

        public bool TextChanged
        {
            get
            {
                return _richTextBox.Modified;
            }
        }

        public event EventHandler<ItemEventArgs<CommentLink>> OnLinkClick;
        
        public Color MarkerColor 
        {
            get
            {
                return _markCurrentBtn.SelectedColor;
            }
            set
            {
                if(_markCurrentBtn.SelectedColor == value)
                    return;
                _markCurrentBtn.SelectedColor = value;
            }
        }
        
        public Color CommentColor { get; set; }

        bool _hideCommentList = false;

        public bool HideCommentsList
        {
            get
            {
                return _hideCommentList;
                
            }
            set 
            {
                if (_hideCommentList != value)
                {
                    if (_commentGridControl.InvokeRequired)
                        _commentGridControl.BeginInvoke((Action) (() => _commentGridControl.Visible = !value));
                    else
                    {
                        _commentGridControl.Visible = !value;
                        _commentGridControl.Enabled = !value;
                    }
                    _hideCommentList = value;
                }
            }
        }

        public bool ReadOnly 
        {
            get
            {
                return _readOnly;
            }
            set
            {
                //if(_readOnly == value)
                //    return;
                if (value)
                {
                    _richTextBox.ReadOnly = true;
                    
                    _commentGridControl.UseEmbeddedNavigator = false;
                    ( _commentGridControl.MainView as GridView ).OptionsBehavior.Editable = false;
                    _richTextBox.TextChanged -=_richTextBox_TextChanged;
                }
                else
                {
                    _richTextBox.ReadOnly = false;
                    
                    _commentGridControl.UseEmbeddedNavigator = true;
                    ( _commentGridControl.MainView as GridView ).OptionsBehavior.Editable = false;
                    _richTextBox.TextChanged -= _richTextBox_TextChanged;
                    _richTextBox.TextChanged += _richTextBox_TextChanged;
                }
                _readOnly = value;
            }
        }

        private bool _hidePanel;

        public bool HidePanel
        {
            get 
            { return _hidePanel; }
            set
            {
                if(_hidePanel == value)
                    return;
                _hidePanel = value;
                _menuPanel.Visible = !value;
            }
        }
        

        private bool _extendedFormating = true;
        public  bool ExtendedFormating 
        {
            get
            {
                return _extendedFormating;
            }
            set
            {
                if(_extendedFormating == value)
                    return;
                _extendedFormating = value;
                if(_extendedFormating)
                {
                    _commentaryGroup.Visible = true;
                }
                else
                {
                    _commentaryGroup.Visible = false;
                }
            }
        }

        public BindingListWithRemoveEvent<IComment> Comments
        {
            get;
            private set;
        }

        public IEnumerable<IComment> EnumerableComments 
        {
            get
            {
                return Comments;

            }
            set
            {
                if (value != null)
                {
                    if (Comments != null)
                    {
                        Comments.Clear();
                       _watchCommentsDic.Clear();
                    }
                    foreach (var comment in value)
                    {
                        Comments.Add( comment );
                        _watchCommentsDic.Add( comment, _richTextBox.Document.CreateRange( comment.StartPosition, comment.EndPosition - comment.StartPosition ) );
                    }
                    Comments.ClearHistory();
                    EnableCommentHandling(true);
                }
            }

        }

        private void EnableCommentHandling(bool isEnable)
        {
            _richTextBox.MouseMove -= _richTextBox_MouseMove;
            _richTextBox.MouseClick -= _richTextBox_Click;
            if (isEnable)
            {
                _richTextBox.MouseClick += _richTextBox_Click;
                _richTextBox.MouseMove += _richTextBox_MouseMove;
            }

        }


        [Browsable( false ),EditorBrowsable(EditorBrowsableState.Never)]
        public MemoryStream Rtf
        {
            get
            {
                return _richTextBox.SaveRtf();
            }
            set
            {
                _richTextBox.LoadRtf(value);
            }
        }

        public RtfMarkUpTextControl():this(null, null)
        {
                
        }

        public RtfMarkUpTextControl(string rtfDocument , IEnumerable<IComment> comments )
        {
            Stopwatch watch = Stopwatch.StartNew();
          
            InitializeComponent( );
            StyleChanged += RtfMarkUpTextControl_StyleChanged;
            if(rtfDocument != null)
                _richTextBox.RtfText = rtfDocument;
            
            if(comments !=null && comments.Any())
                Comments =  new BindingListWithRemoveEvent<IComment>( comments.ToList() );
              
            else
                Comments = new BindingListWithRemoveEvent<IComment>();
            EnableCommentHandling( true );
            _commentGridControl.DataSource = Comments;
            Comments.BeforeRemove += Comments_BeforeRemove;

            InitMenuItems( );

            watch.Stop( );
            var elapsedMs = watch.ElapsedMilliseconds;
            Trace.WriteLine( "rtf mark up Constructor" + elapsedMs );
        }

        void RtfMarkUpTextControl_StyleChanged( object sender, EventArgs e )
        {
            throw new NotImplementedException( );
        }

        void _richTextBox_PopupMenuShowing( object sender, DevExpress.XtraRichEdit.PopupMenuShowingEventArgs e )
        {
            if (_readOnly)
            {
                e.Menu.Items.Clear();
            }
            List<MenuItem> items = new List<MenuItem>();
            foreach (var item in e.Menu.Items.OfType<DXMenuItem>())
            {
                if (item.Caption.Contains("..."))
                    item.Visible = false;
            }
            if (_extendedFormating)
            {
                bool isThereSelection = _richTextBox.Document.Selection.Length > 0;
                _selectMenuItem.Enabled = _commentMenuItem.Enabled = isThereSelection;
                e.Menu.Items.Add(_commentMenuItem);
                e.Menu.Items.Add(_selectMenuItem);
                bool isThereComment = _currentComment != null;
                _removeCommentMenuItem.Enabled = _editCommentMenuItem.Enabled = isThereComment;
                e.Menu.Items.Add( _removeCommentMenuItem );
                e.Menu.Items.Add( _editCommentMenuItem );
            }
        }

        void Comments_BeforeRemove( object sender, ItemEventArgs<IComment> deletedItem )
        {
            if (deletedItem == null)
                return;
            MarkUpComment(deletedItem.Item,true);
        }

        private void InitMenuItems( )
        {
          _commentMenuItem = new DXMenuItem("Добавить комментарий",(s,e)=>CommentCurrentSelection(),Resources.comment_16x16){BeginGroup = true};
          _removeCommentMenuItem = new DXMenuItem( "Удалить комментарий", ( s, e ) => RemoveComment( ), Resources.comment_remove16x16 );
          _editCommentMenuItem = new DXMenuItem( "Редактирвать", ( s, e ) => EditComment( ), Resources.new_24_128 );
          _selectMenuItem = new DXMenuItem( "Выделить", ( s, e ) => MarkCurrentSelection(),_markCurrentBtn.Glyph){BeginGroup = true};
          
        }

        private void EditComment( )
        {
            if (_currentComment == null)
                return;
            var editCommentForm = new RequestCommentForm(_currentComment);
            if (editCommentForm.ShowDialog() == DialogResult.OK)
            {
                _currentComment.CommentText = editCommentForm.Comment;
                _currentComment.CommentType = editCommentForm.CommentType;
                MarkUpComment(_currentComment);
                Comments.MarkAsChanged(_currentComment);
                _commentGridControl.DataSource = null;
                _commentGridControl.DataSource = Comments;
            }
        }

        private void RemoveComment( )
        {
            if (_currentComment == null)
                return;
            Comments.Remove( _currentComment );
            _currentComment = null;
        }

        void _richTextBox_MouseMove( object sender, MouseEventArgs e )
        {
            //Trace.WriteLine( "Mouse over:x:{0} y:{1}".FormatString( e.X, e.Y ) );
            if (_currentComment != null && _currentComment.CommentType == CommentType.Link)
                    Cursor = Cursors.Hand;
            int curX = e.X, curY = e.Y;
            if (Math.Abs( _lastX - curX ) + Math.Abs( _lastY - curY ) <= _deltaThreshold)
            {
                return;
            }
            else
            {
                CloseToolTip( );
                _currentComment = null;
                Trace.WriteLine("call close");
                Trace.WriteLine( "current comment : {0} ".FormatString( _currentComment == null ? "null" : _currentComment.CommentText ) );
            }
            _lastX = curX;
            _lastY = curY;
            var documentPos = GetDocumentPositionFromCursorCoordinates(e.Location);

            
            if (documentPos == null)
            {
                //Cursor = DefaultCursor;
                return;
            }
            int pos = documentPos.ToInt( );
            foreach (var comment in Comments)
            {
                if (comment.StartPosition<= pos && comment.EndPosition>= pos)
                {
                    if(comment.CommentType== CommentType.Link)
                    {
                        if (Cursor != Cursors.Hand)
                            Cursor = Cursors.Hand;
                    }
                    else
                    {
                        Point locationOnForm = _richTextBox.FindForm( ).PointToClient( _richTextBox.Parent.PointToScreen( _richTextBox.Location ) );
                        Trace.WriteLine( "current comment : {0} ".FormatString( _currentComment == null ? "null" : _currentComment.CommentText ) );
                        ShowToolTip( curX + locationOnForm.X, curY + locationOnForm.Y, comment.CommentText );
                    }
                    _currentComment = comment;
                    break;
                }
            }
            
        }

        private DocumentPosition GetDocumentPositionFromCursorCoordinates(Point cursorLocation)
        {
            var docPoint = ColculatePoint(cursorLocation,_richTextBox.DpiX,_richTextBox.DpiY);
            DocumentPosition documentPos = null;
            documentPos = _richTextBox.GetPositionFromPoint(docPoint);
            return documentPos;
        }

        private Point ColculatePoint(Point cursorLocation,float dpiX, float dpiY)
        {
            Point docPoint = Units.PixelsToDocuments(cursorLocation, dpiX, dpiY);
            return docPoint;
        }

        private void ShowToolTip( int curX, int curY, string comment )
        {
            _commentToolTip.Tag = true;
            Trace.WriteLine("show tooltip : '{0}'".FormatString(comment));
            _commentToolTip.Show( comment, FindForm(), curX, curY );
        }

        private void ShowToolTip(RichEditControl richTextBoxControl, IComment comment )
        {
            _commentToolTip.Tag = true;
            var commentTargetPoint =
                Units.DocumentsToPixels(
                    _richTextBox.GetBoundsFromPosition(richTextBoxControl.Document.CreatePosition(comment.StartPosition)),
                    _richTextBox.DpiX, _richTextBox.DpiY);
            var locationOnForm = _richTextBox.FindForm( ).PointToClient( _richTextBox.Parent.PointToScreen( _richTextBox.Location ) );
            
            _commentToolTip.Show( comment.CommentText, FindForm(), commentTargetPoint.X + locationOnForm.X, commentTargetPoint.Y + locationOnForm.Y );
        }

        private void CloseToolTip( )
        {
            if (_commentToolTip.Tag != null)
            {
                _commentToolTip.Tag = null; 
                _commentToolTip.RemoveAll();
                _commentToolTip.Hide( this );
            }
        }

        private void MarkCurrentSelection( )
        {
            if (_richTextBox.Document.Selection.Length == 0)
                return;
            var cp =_richTextBox.Document.BeginUpdateCharacters(_richTextBox.Document.Selection);
            cp.BackColor = MarkerColor;
            _richTextBox.Document.EndUpdateCharacters(cp);
        }

        private void CommentCurrentSelection( )
        {
            if (_richTextBox.Document.Selection.Length == 0)
                return;
            var requestCommentForm = new RequestCommentForm( );
            var res = requestCommentForm.ShowDialog(FindForm());
            if (res == DialogResult.OK && !String.IsNullOrEmpty( requestCommentForm.Comment ))
            {
                var comment =
                    new TextComment(
                        new Range<int>()
                        {
                            Minimum = _richTextBox.Document.Selection.Start.ToInt(),
                            Maximum =
                                _richTextBox.Document.Selection.Length + _richTextBox.Document.Selection.Start.ToInt()
                        },
                        requestCommentForm.Comment, requestCommentForm.CommentType);
                Comments.Add( comment );
                _watchCommentsDic.Add( comment, _richTextBox.Document.CreateRange( comment.StartPosition, comment.EndPosition - comment.StartPosition ) );
                MarkUpComment( comment );
            }
        }

        private void MarkUpComment(IComment comment , bool isInverse = false )
        {
            var cp = _richTextBox.Document.BeginUpdateCharacters(_richTextBox.Document.CreateRange(comment.StartPosition,comment.EndPosition - comment.StartPosition) );
            cp.ForeColor = isInverse? _richTextBox.Document.DefaultCharacterProperties.ForeColor : comment.CommentType.GetEnumAttribute<ColorAttribute>()[0].Color;
            _richTextBox.Document.EndUpdateCharacters(cp);
        }

        private void gridView_MouseMove( object sender, System.Windows.Forms.MouseEventArgs e )
        {
            GridView view = sender as GridView;
            if (view.RowCount == 0)
                return;

            GridHitInfo info = view.CalcHitInfo( new Point( e.X, e.Y ) );

            if (info.InRowCell)
            {
                var gridView = ( _commentGridControl.MainView as GridView );
                var currentSelectedComment = (IComment)( gridView ).GetRow( info.RowHandle );
                if (_lastSelectedComment == currentSelectedComment)
                    return;
                _lastSelectedComment = currentSelectedComment;
                //var range = _lastSelectedComment.TextRange;
                //_richTextBox.HideSelection = false;
                _richTextBox.Document.Selection = _richTextBox.Document.CreateRange(_lastSelectedComment.StartPosition, _lastSelectedComment.EndPosition - _lastSelectedComment.StartPosition);
                //_richTextBox.SelectionLength = range.Maximum - range.Minimum;
                ShowToolTip( _richTextBox, _lastSelectedComment );
            }
            else if(_lastSelectedComment != null)
            {
                //var range = _lastSelectedComment.TextRange;
                if (_richTextBox.Document.Selection.Start.ToInt( ) == _lastSelectedComment.StartPosition &&
                    _richTextBox.Document.Selection.End.ToInt( ) == _lastSelectedComment.StartPosition)
                    _richTextBox.Document.Selection = _richTextBox.Document.CreateRange(0,0);
                CloseToolTip( );
                _lastSelectedComment = null;
            }
        
        }

        private void gridView1_CustomColumnSort( object sender, DevExpress.XtraGrid.Views.Base.CustomColumnSortEventArgs e )
        {
            var val1 = ( e.RowObject1 as IComment );
            var val2 = ( e.RowObject2 as IComment );
            e.Result = val2.EndPosition.CompareTo( val1.StartPosition );
        }

        private void _richTextBox_Click( object sender, EventArgs e )
        {
            try
            {
            if(OnLinkClick != null && _currentComment!= null && _currentComment.CommentType == CommentType.Link)
                {
                    Exception ex;
                    OnLinkClick(this,new ItemEventArgs<CommentLink>(CommentLink.Deserialize(_currentComment.CommentText,out ex)));
                }
            }
            catch(Exception ex) {}
        }

        private void _richTextBox_DragDrop( object sender, DragEventArgs e )
        {
            var richEdit = sender as RichEditControl;
            if(e.Data.GetDataPresent(typeof(TreeNodeAdv[])))
            {
                var branch = (TreeNodeAdv[])e.Data.GetData( typeof( TreeNodeAdv[] ) );
                if (branch == null || branch.Length == 0)
                    return;
                var journalBranchView = (branch[0].Tag as JournalBranchView).Model;
                var pos = GetDocumentPositionFromCursorCoordinates( richEdit.PointToClient( new Point( e.X, e.Y ) ) );
                if(pos != null)
                    richEdit.Document.CaretPosition = pos;
                var startPosition = richEdit.Document.CaretPosition.ToInt();
                richEdit.Document.InsertText(richEdit.Document.CaretPosition, journalBranchView.Name + " ");
                // var endPosition = _richTextBox.Document.CaretPosition;
                if (journalBranchView.BlancForm != null)
                {
                    var commentLink = new CommentLink(journalBranchView.BlancForm.Id);
                    Exception ex;
                    var comment = new TextComment( new Range<int>( startPosition, startPosition  + journalBranchView.Name.Length), commentLink.Serialize( out ex ), CommentType.Link );
                    Comments.Add( comment );
                    _watchCommentsDic.Add( comment, richEdit.Document.CreateRange( comment.StartPosition, comment.EndPosition - comment.StartPosition ) );
                    MarkUpComment( comment);
                }
            }
            //else if (e.Data.GetDataPresent(DataFormats.StringFormat))
            //{
            //    var addedString = (string) e.Data.GetData(DataFormats.StringFormat);
            //    var pos = richEdit.GetPositionFromPoint( new PointF( e.X, e.Y ) ) ?? richEdit.Document.CaretPosition;
            //    HandleTextChanging(pos.ToInt(), addedString.Length);
            //}

            //var branch = e.Data.GetData( typeof( JournalBranchView ) );
            //Trace.WriteLine("Drag Drop " + e.Data.GetData(typeof(string)));
        }

        private static bool IsIntesct( int insertPos, Range<int> commentRange,  Range<int> insertRange, bool isInsert )
        {
            if (isInsert)
                return commentRange.ContainsValue(insertPos);
            else
                return commentRange.ContainsValue(insertRange.Minimum) ||
                       commentRange.ContainsValue(insertRange.Maximum);
            //return ( comment.EndPosition < insertPos && comment.StartPosition > insertPos ) || ( comment.EndPosition == insertPos && isInsert == false );
        }

        private void _richTextBox_DragEnter( object sender, DragEventArgs e )
        {
            e.Effect = e.Data.GetDataPresent( typeof( TreeNodeAdv[] ) ) ? DragDropEffects.Copy : DragDropEffects.None;
        }

        private void _richTextBox_DragOver( object sender, DragEventArgs e )
        {
            var richEdit = sender as RichEditControl;
            //Trace.WriteLine("Drag over:x:{0} y:{1}".FormatString(e.X, e.Y));
            var pos = GetDocumentPositionFromCursorCoordinates( richEdit.PointToClient(new Point(e.X,e.Y)));
            if (pos != null)
            {
                richEdit.Document.CaretPosition = pos;
                Trace.WriteLine(pos);
            }
        }

        private void _richTextBox_TextChanged( object sender, EventArgs e )
        {
            if(_onProgramm)
                return;
            SyncCommentsRanges();
        }

        private void SyncCommentsRanges( )
        {
            List<int> condemnedList = new List<int>( );

            for (int i =0 ; i < Comments.Count; i++)
            {
                var comment = Comments[i];
                var range = _watchCommentsDic[comment];
                var rangeStart = range.Start.ToInt();
                var rangeEnd = rangeStart + range.Length;
                if(rangeStart == rangeEnd)
                    condemnedList.Add(i);
                if (rangeStart != comment.StartPosition || rangeEnd != comment.EndPosition)
                {
                    comment.StartPosition = rangeStart;
                    comment.EndPosition = rangeEnd;
                    Comments.MarkAsChanged(comment);
                }
            }

            for (int i = condemnedList.Count-1; i >0; i--)
            {
                var comment = Comments[condemnedList[i]];
                Comments.RemoveAt( condemnedList[i] );
                _watchCommentsDic.Remove(comment);
            }
        }

        private void _createCommentaryBtn_ItemClick( object sender, ItemClickEventArgs e )
        {
            CommentCurrentSelection( );
        }

        private bool _onProgramm = false;
        public void Clear()
        {
            _onProgramm = true;
            Comments.Clear();
            _watchCommentsDic.Clear();
            _commentGridControl.RefreshDataSource();
            _richTextBox.Text = "";
            _onProgramm = false;
        }
    }

    public class TextComment : IComment
    {
        public Range<int> TextRange { get; private set; }
        
        public string CommentText { get;  set; }

        public CommentType CommentType { get;  set; }

        public TextComment( Range<int> textRange, string comment , CommentType commentType = CommentType.Info )
        {
            this.TextRange = textRange;
            this.CommentText = comment;
            this.CommentType = commentType;
        }

        #region IComment Members

        public int Id
        {
            get { return -1; }
        }

        public int StartPosition
        {
            get { return TextRange.Minimum; }
            set { TextRange.Minimum = value; }
        }

        public int EndPosition
        {
            get { return TextRange.Maximum; }
            set { TextRange.Maximum = value; }
        }
         
        public void Move( int delta )
        {
            TextRange.Minimum += delta;
            TextRange.Maximum += delta;
        } 

        #endregion
    
    }

}

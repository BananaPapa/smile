﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using System.Xml.Schema;
using Aga.Controls.Tree;
using Aga.Controls.Tree.NodeControls;
using Castle.ActiveRecord;
using Castle.DynamicProxy.Generators.Emitters.SimpleAST;
using DevExpress.CodeParser;
using DevExpress.Utils.Extensions.Helpers;
using DevExpress.Xpo.Helpers;
using DevExpress.XtraEditors;
using Eureca.Professional.ActiveRecords;
using Eureca.Professional.FillingFormsTest.Gui;
using NHibernate.Hql.Ast.ANTLR;
using MessageBox = Eureca.Integrator.Utility.Forms.MessageBox;
using log4net;
using Eureca.Utility.Extensions;
using Expression = NHibernate.Criterion.Expression;

namespace Eureca.Professional.FillingFormsTest.Gui
{

    public partial class EditJournalTreeControl : XtraUserControl ,IDisposable
    {
        private static ILog Log = LogManager.GetLogger(typeof (EditJournalTreeControl));

        public event EventHandler<JournalTreeSelectionChangedArgs> SelectedBranchChanged;


        private readonly NodeTextBox _tbName = new NodeTextBox( );
        private readonly NodeIcon _iconBox = new NodeIcon( );
        private JournalTreeContext _journalContext;

        private Journal _journal;

        private bool _isReadonly =false;

        //private bool _lazyLoad = true;

        public bool Readonly
        {
            get { return _isReadonly; }
            set
            {
                //if(_isReadonly == value)
                //    return;
                _isReadonly = value;
                if (_isReadonly)
                {
                    _journalTreeView.AllowDrop = false;
                    _tbName.EditEnabled = false;
                    
                    //_tbName.EditOnClick = false;
                }
                else
                {
                    _journalTreeView.AllowDrop = true;
                    _tbName.EditEnabled = true;
                    //_tbName.EditOnClick = true;
                }
            }
        }

        public Journal Journal
        {
            get { return _journal; }
            set
            {
                if (_journal == value)
                    return;
                _journal = value;
                Init(_journal);
            }
        }

        public EditJournalTreeControl()
        {
            InitializeComponent();
            Readonly = _isReadonly;
        }

        public void ClearSelection()
        {
            _journalTreeView.ClearSelection();
        }

        void _journalTreeView_SelectionChanged( object sender, EventArgs e )
        {
            JournalBranch journalBranch = null;
            if(_journalTreeView.SelectedNode  !=null)
                journalBranch = (_journalTreeView.SelectedNode.Tag as JournalBranchView).Model;
            SelectedBranchChanged.SafeRaise(this,new JournalTreeSelectionChangedArgs(journalBranch,_journal));
        }

        private void Init(Journal journal)
        {
            var watch = Stopwatch.StartNew( );
            // the code that you want to measure comes here
            watch.Stop( );
            
            _journalTreeView.BeginUpdate();

            _journalContext = new JournalTreeContext(journal);

            _journalTreeView.Model = _journalContext;
            //_journalTreeView.LoadOnDemand = true;
            _journalTreeView.NodeControls.Clear();

            _tbName.DataPropertyName = "Name";
            //_tbName.EditEnabled = true;
            _journalTreeView.NodeControls.Add(_tbName);

            _iconBox.DataPropertyName = "Icon";
            _journalTreeView.NodeControls.Add(_iconBox);
            _tbName.EditorShowing += _tbName_EditorShowing;


            //NodeTextBox tb = new NodeTextBox( );
            //tb.DataPropertyName = "Name";
            //_journalTreeView.NodeControls.Add( tb );

            _journalTreeView.EndUpdate();
            //_journalTreeView.FullUpdate();
            var elapsedMs = watch.ElapsedMilliseconds;
            Trace.WriteLine("время инициализации дерева:" + elapsedMs);
        }

        void _tbName_EditorShowing( object sender, CancelEventArgs e )
        {
            if (_isReadonly)
                e.Cancel = true;
        }

        private void _journalTreeView_DragOver(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof (TreeNodeAdv[])) && _journalTreeView.DropPosition.Node != null)
            {
                TreeNodeAdv[] nodes = e.Data.GetData(typeof (TreeNodeAdv[])) as TreeNodeAdv[];
                TreeNodeAdv parent = _journalTreeView.DropPosition.Node;
                if (_journalTreeView.DropPosition.Position != NodePosition.Inside)
                    parent = parent.Parent;

                foreach (TreeNodeAdv node in nodes)
                    if (!CheckNodeParent(parent, node))
                    {
                        e.Effect = DragDropEffects.None;
                        return;
                    }

                e.Effect = e.AllowedEffect;
            }
        }

        private bool CheckNodeParent(TreeNodeAdv parent, TreeNodeAdv node)
        {
            while (parent != null)
            {
                if (node == parent)
                    return false;
                else
                    parent = parent.Parent;
            }
            return true;
        }

        private void _journalTreeView_DragDrop(object sender, DragEventArgs e)
        {
            try
            {
                _journalTreeView.BeginUpdate();
                var treeNodes = (TreeNodeAdv[]) e.Data.GetData(typeof (TreeNodeAdv[]));
                List<JournalBranchView> branchs = (treeNodes).Select(item => item.Tag as JournalBranchView).ToList();
                var dropNode = _journalTreeView.DropPosition.Node.Tag as JournalBranchView;
                if (_journalTreeView.DropPosition.Position == NodePosition.Inside)
                {
                    if (_journalContext.IsLeaf(_journalContext.GetPath(dropNode)))
                    {
                        return;
                    }

                    InsertIntoNode(dropNode, branchs, 0);
                    _journalTreeView.DropPosition.Node.IsExpanded = true;
                }
                else
                {
                    var parent = dropNode.ParentView;
                    if (parent == null || _journalContext.IsLeaf(_journalContext.GetPath(parent)))
                        return;

                    var nextItem = dropNode;

                    //Расставляем по порядку старые и новые;
                    int insertIndex = -1;
                    List<JournalBranchView> parentNodes =
                        new List<JournalBranchView>(
                            _journalContext.GetChildren( _journalContext.GetPath( parent ) ).Cast<JournalBranchView>( ).Where(view => !branchs.Any(branchView => branchView.Id == view.Id)).OrderBy( view => view.Order ) );
                    insertIndex = parentNodes.IndexOf(nextItem);
                    if (_journalTreeView.DropPosition.Position == NodePosition.Before)
                        insertIndex -= 1;
                    else
                        insertIndex += 1;

                    InsertIntoNode(parent,branchs,insertIndex);
                }
                _journalTreeView.EndUpdate();
                _journalTreeView.Refresh();
            }
            catch (Exception exception)
            {
                Log.Error(exception.ToString());
            }
            finally
            {
                _journalTreeView.FullUpdate();
            }
        }

        private void _journalTreeView_ItemDrag(object sender, ItemDragEventArgs e)
        {
            if (_isReadonly)
            {
                var node = _journalTreeView.SelectedNode;
                if (_journalContext.IsLeaf(_journalContext.GetPath((node.Tag as JournalBranchView))))
                    _journalTreeView.DoDragDropSelectedNodes(DragDropEffects.Copy | DragDropEffects.Move);
            }
            else
            {
                _journalTreeView.DoDragDropSelectedNodes(DragDropEffects.Move);
            }
        }

        void InsertIntoNode(JournalBranchView dropNode, IEnumerable<JournalBranchView> dropedNodes, int insertPosition)
        {
            IEnumerable<JournalBranchView> otherDropNodeChilds =
                _journalContext.GetChildren( _journalContext.GetPath( dropNode ) ).Cast<JournalBranchView>( ).Where(allChildsBrnachview => !dropedNodes.Any(branchView => allChildsBrnachview.Id == branchView.Id)).OrderBy(view => view.Order);
            int counter = 0;
            if (insertPosition <= 0)
            {
                
                foreach (var journalBranchView in dropedNodes)
                {
                    journalBranchView.ParentView = dropNode;
                    journalBranchView.Order = counter++;
                }
                foreach (var otherDropNodeChild in otherDropNodeChilds)
                {
                    otherDropNodeChild.Order = counter++;
                }
            }
            else
            {
                bool isNewInserted = false;
                foreach (var otherDropNodeChild in otherDropNodeChilds)
                {
                    otherDropNodeChild.Order = counter++;
                    if (counter == insertPosition)
                    {
                        foreach (var journalBranchView in dropedNodes)
                        {
                            isNewInserted = true;
                            journalBranchView.ParentView = dropNode;
                            journalBranchView.Order = counter++;
                        }
                    }
                }
                if(!isNewInserted)
                    foreach (var journalBranchView in dropedNodes)
                    {
                        journalBranchView.ParentView = dropNode;
                        journalBranchView.Order = counter++;
                    }
            }
        }

        private void _treeContextMenu_Opening(object sender, CancelEventArgs e)
        {
            if (_isReadonly)
                e.Cancel = true;

            var node = _journalTreeView.GetNodeAt( _journalTreeView.PointToClient( Cursor.Position ) );
            if (node != null)
            {
                _addSubSectionMI.Enabled =
                    _addBlancMI.Enabled =
                        !_journalContext.IsLeaf(_journalContext.GetPath((node.Tag as JournalBranchView)));
            }
            else
            {
                if(_journalTreeView.AllNodes.Any())
                {
                    e.Cancel = true;
                    return;
                }
                _addSubSectionMI.Enabled =_addBlancMI.Enabled = true;
            }

        }

        private JournalBranchView AddSubSection( TreeViewAdv treeView,TreeNodeAdv parent, bool withBlanc = false )
        {
            //
            if(parent == null && treeView.AllNodes.Any())
                return null;

            RtfForm blanc = null;
            if (withBlanc)
            { 
                var editForm = new BlancEditForm(showToolPanel: true);
                var res = editForm.ShowDialog(this);
                if (res == DialogResult.OK)
                {
                    blanc = new RtfForm(editForm.EditedBlanc.GetBuffer(), FormType.Blanc , "Новый бланк");
                    blanc.CreateAndFlush();
                }
                else if (res == DialogResult.Cancel)
                {
                    return null;
                }
            }
            
            JournalBranch branch = new JournalBranch( "Новый {0}".FormatString( withBlanc ? "бланк" : "раздел" ), _journal, parent == null ? null : ( parent.Tag as JournalBranchView ).Model , blanc ) { Order = 0 };

            branch.CreateAndFlush();
            _journal.JournalBranches.Add(branch);
            var newJournalBranchView = JournalBranchView.CreateInstance(branch, _journalContext);
            if (parent != null)
            {
                InsertIntoNode(parent.Tag as JournalBranchView,new []{newJournalBranchView}, 0 );
            }
            //treeView.BeginUpdate();
            _journalContext.NotifyNodeInserted(newJournalBranchView);
            //treeView.EndUpdate();
            return newJournalBranchView;
        }

        public void OpenNode(JournalBranch branch)
        {
            var branchview = JournalBranchView.CreateInstance(branch, _journalContext);
            try
            {
                var treePath = _journalContext.GetPath(branchview);
                var childs = _journalTreeView.AllNodes;
                foreach (var node in treePath.FullPath.Cast<JournalBranchView>())
                {
                    var treeNode = childs.FirstOrDefault(adv => (adv.Tag as JournalBranchView).Id == node.Id);
                    if(treeNode != null)
                    {
                        treeNode.Expand();
                        childs = treeNode.Children;
                    }
                    if (childs == null || !childs.Any())
                    {
                        _journalTreeView.SelectedNode = treeNode;
                    }

                }
            }
            catch (Exception)
            {
            }
        }

        private void _addSubSectionMI_Click(object sender, EventArgs e)
        {
            AddNode();
        }

        private void AddNode(bool isBlanc = false)
        {
            var selectedNode = _journalTreeView.SelectedNode;
            var newNode = AddSubSection(_journalTreeView, selectedNode, isBlanc);
            try
            {
                _journalTreeView.SelectedNode = _journalTreeView.FindNodeByTag(newNode);
            }
            catch (Exception)
            {
            }
        }

        private void _addBlancMI_Click(object sender, EventArgs e)
        {
            //TreeViewAdv treeView = ( sender as ContextMenuStrip ).SourceControl as TreeViewAdv;
            AddNode(true);
        }

        private void _deleteNodeMI_Click(object sender, EventArgs e)
        {
            /// TreeViewAdv treeView = sender as TreeViewAdv;
            DeleteSelectedNode(_journalTreeView);
        }

        private void DeleteSelectedNode(TreeViewAdv treeView)
        {
            var selectedNode = treeView.SelectedNode;
            if (selectedNode != null)
            {
                var childNodesCount = selectedNode.Children.Count();
                var journalBranchView = selectedNode.Tag as JournalBranchView;
                if (
                    MessageBox.Confirm(
                        "Вы уверены, что хотите удалить раздел \"{0}\" {1}?".FormatString(journalBranchView.Name,
                            childNodesCount > 0
                                ? " и его {0} {1}.".FormatString(childNodesCount,
                                    childNodesCount.Decline("раздел", "раздела", "разделов"))
                                : ".")))
                {
                    treeView.BeginUpdate();
                    _journalContext.NotifyNodeRemoved(journalBranchView);
                    journalBranchView.Model.DeleteBranchWithReference();
                    treeView.EndUpdate();
                }
            }
        }

        

        private void _journalTreeView_DoubleClick(object sender, EventArgs e)
        {
            if (_isReadonly)
                return;
            var selectedNode = _journalTreeView.GetNodeAt( _journalTreeView.PointToClient( Cursor.Position ) ); ;
            if (selectedNode == null)
                return;
           
            var journalBranchView = selectedNode.Tag as JournalBranchView;
            if (journalBranchView.BlancForm == null)
                return;
            RtfForm blancForm = RtfForm.Find(journalBranchView.BlancForm.Id);
            if (blancForm != null)
            {
                BlancEditForm blancEditForm = new BlancEditForm( new MemoryStream( blancForm.Content ), showToolPanel: true )
                {
                    Text = blancForm.Name
                };
                DialogResult res = blancEditForm.ShowDialog( this );
                if (res == DialogResult.OK)
                {
                    blancForm.Content = blancEditForm.EditedBlanc.GetBuffer( );
                    blancForm.UpdateAndFlush( );
                }
            }
        }

        public void ExpandAll()
        {
            _journalTreeView.ExpandAll();
        }
    }

    public class JournalTreeSelectionChangedArgs : EventArgs
    {
        private JournalBranch _journalBranch;

        public JournalBranch SelectedBranch
        {
            get { return _journalBranch; }
            set { _journalBranch = value; }
        }

        private Journal _ownerJournal;

        public Journal OwnerJournal
        {
            get { return _ownerJournal; }
            set { _ownerJournal = value; }
        }
        

        public JournalTreeSelectionChangedArgs(JournalBranch journalBranch , Journal owner)
        {
            _journalBranch = journalBranch;
            _ownerJournal = owner;
        }

    }
}

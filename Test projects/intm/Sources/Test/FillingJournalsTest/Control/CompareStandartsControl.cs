﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Castle.ActiveRecord;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using Eureca.Integrator.Extensions;
using Eureca.Professional.ActiveRecords;
using Eureca.Professional.FillingFormsTest.Gui;
using Eureca.Professional.FillingFormsTest.Properties;
using NHibernate.Criterion;

namespace Eureca.Professional.FillingFormsTest
{
    public partial class CompareStandartsControl : UserControl
    {
        private Test _test;
        private Question _question;

        public CompareStandartsControl()
        {
            InitializeComponent( );
        }
        public void Init(TestPassing testPassing, Test test)
        {
            using (new SessionScope())
            {
                _test = Test.Find( test.Id );
                var results = FillingFormsTestResult.FindAll(Expression.Eq("TestPassing", testPassing));
                if(!results.Any())
                    return;
                _question = results.First().Question;
                _assignmentTextRichEdit.LoadRtf(new MemoryStream(_question.Content.RichContent));
                //TestQuestion[] questions = ActiveRecordBase<TestQuestion>.FindAll( Restrictions.Eq( "Test", _test ) );
            }
            
            _assigmentBlancGrid.OnBlancSelectionChanged += _assigmentBlancGrid_OnBlancSelectionChanged;
            _assigmentBlancGrid.LoadData(testPassing.Id);
        }

        void _assigmentBlancGrid_OnBlancSelectionChanged( object sender, Gui.OnBlancSelectionChangedEventArgs e )
        {
            using (new SessionScope())
            {
                switch (e.BlancType)
                {
                    case BlancCompletnessType.Filled:
                        var filledBlanc = RtfForm.Find( _assigmentBlancGrid.FilledBlancs[e.BlancId].Id);
                        LoadResultBlanc(filledBlanc);
                        LoadAssigmentStandart(e.BlancId);
                        splitContainerControl2.PanelVisibility = SplitPanelVisibility.Both;
                        break;
                    case BlancCompletnessType.Unfilled:
                        LoadAssigmentStandart(e.BlancId);
                        splitContainerControl2.PanelVisibility = SplitPanelVisibility.Panel2;
                        break;
                    case BlancCompletnessType.Unnecessary:
                        var rtfForm = RtfForm.Find( _assigmentBlancGrid.FilledBlancs[e.BlancId].Id );
                        LoadResultBlanc( rtfForm );
                        splitContainerControl2.PanelVisibility = SplitPanelVisibility.Panel1;
                        break;
                }
            }
        }

        private void LoadResultBlanc(RtfForm filledBlanc)
        {
            _resultRichTextBox.Clear();
            _resultRichTextBox.Rtf = new MemoryStream(filledBlanc.Content);
            _resultRichTextBox.EnumerableComments = filledBlanc.Comments.ToList();
        }

        private void LoadAssigmentStandart(int blancId)
        {
            if (_assigmentBlancGrid.Standarts.ContainsKey(blancId))
            {
                _standartRichTextBox.LoadRtf(new MemoryStream(_assigmentBlancGrid.Standarts[blancId].Content));
                splitContainerControl2.Panel2.Text = "Образец";
            }
            else
            {
                _standartRichTextBox.LoadRtf( new MemoryStream( _assigmentBlancGrid.FormsMerge[blancId].Content ) );
                splitContainerControl2.Panel2.Text = "Бланк";
            }
        }

        private void _callapseButton_CheckedChanged(object sender, EventArgs e)
        {
            CheckButton senderButton = (CheckButton) sender;
            if (senderButton.Checked)
            {
                senderButton.Image = Resources.right;
                senderButton.ToolTip = "Развернуть список бланков";
                splitContainerControl1.PanelVisibility = SplitPanelVisibility.Panel2;
            }
            else
            {
                senderButton.Image = Resources.left;
                senderButton.ToolTip = "Свернуть список бланков";
                splitContainerControl1.PanelVisibility = SplitPanelVisibility.Both;
            }
        }
    }
}

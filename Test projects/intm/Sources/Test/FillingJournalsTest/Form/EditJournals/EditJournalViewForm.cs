﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Eureca.Integrator.Common;
using Eureca.Professional.ActiveRecords;
using Eureca.Utility.Helper;
using MessageBox = Eureca.Integrator.Utility.Forms.MessageBox;

namespace Eureca.Professional.FillingFormsTest.Gui
{
    public partial class EditJournalViewForm : PreloadMarqueeForm
    {
        private const string formHeaderText = "Редактирование представления";

        private MemoryStream _originalView;

        public BindingListWithRemoveEvent<IComment> Comments
        {
            get
            {
                return _journalViewRtfMarkUp.Comments;
            }
        }

        public MemoryStream ViewBlanc
        {
            get
            {
                if (DialogResult == DialogResult.Cancel)
                    return _originalView;
                return _journalViewRtfMarkUp.Rtf;
            }
        }

        private string _viewName;
        public string ViewName 
        {
            get
            {
                return _viewName;
            }
            set
            {
                if(_viewName == value)
                    return;
                _viewName = value;
                Text = string.Format("{0}: {1}", formHeaderText, _viewName);
            }
        }

        public EditJournalViewForm(Journal journal, MemoryStream view = null, string name = null , IEnumerable<Comment> comments = null )
        {
            StartMarquee( "Создание формы редактирования..." );
            InitializeComponent( );
            Closing += EditJournalViewForm_Closing;

            _originalView = view;
            _viewNameTextEdit.DataBindings.Add("Text", this, "ViewName",false,DataSourceUpdateMode.OnPropertyChanged);
            //if (name != null)
            //    _viewNameTextEdit.Text = name;
            ViewName = name ?? "Новое представление";
            if (journal == null)
                throw new ArgumentNullException("journal");
            _journalTreeView.Readonly = true;

            if (comments != null)
                _journalViewRtfMarkUp.EnumerableComments = comments;
            if(view != null)
                _journalViewRtfMarkUp.Rtf = view;


            _journalTreeView.Journal = journal;
            _journalTreeView.ExpandAll();
            CloseMarquee();
        }

        void EditJournalViewForm_Closing( object sender, CancelEventArgs e )
        {
            if (DialogResult == DialogResult.OK)
            {
                if (!Verify())
                {
                    e.Cancel = true;
                }
            }
            else
            {
                if (_journalViewRtfMarkUp.TextChanged)
                {
                    if (!ConfirmClosing())
                        e.Cancel = true;
                }
                else
                    DialogResult = DialogResult.Cancel;
            }
        }

        private bool Verify()
        {
            StringBuilder errors = new StringBuilder();
            if (_journalViewRtfMarkUp.Comments.Count == 0)
            {
                errors.AppendLine("Представление не содержит ни одной ссылки на бланк.");
            }
            if (string.IsNullOrEmpty(ViewName))
            {
                errors.AppendLine("Введите название представления");
            }
            if (errors.Length > 0)
            {
                errors.Insert(0, "Представление содержит следующие ошибки:\n");
                MessageBox.ShowValidationError(errors.ToString());
                return false;
            }
            return true;
        }

        private void _okBtn_Click( object sender, EventArgs e )
        {

            DialogResult = DialogResult.OK;
        }

        private void _cancelBtn_Click( object sender, EventArgs e )
        {
           DialogResult = DialogResult.Cancel;
        }

        private bool ConfirmClosing( )
        {
            var res = MessageBox.ConfirmOrCancel( "Изменения будут утеряны. Сохранить изменения?" );
            if (res.HasValue)
            {
                if (res.Value)
                {
                    if (Verify( ))
                    DialogResult = DialogResult.OK;
                }
                else
                {
                    DialogResult = DialogResult.Cancel;    
                }
                return true;
            }
            return false;
        }

    }
}

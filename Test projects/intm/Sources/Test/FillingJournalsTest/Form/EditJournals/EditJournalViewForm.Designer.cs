﻿using Eureca.Professional.FillingFormsTest.Gui;

namespace Eureca.Professional.FillingFormsTest.Gui
{
    partial class EditJournalViewForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if (disposing && ( components != null ))
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditJournalViewForm));
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this._saveBtn = new DevExpress.XtraEditors.SimpleButton();
            this._cancelBtn = new DevExpress.XtraEditors.SimpleButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this._viewNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this._journalTreeView = new Eureca.Professional.FillingFormsTest.Gui.EditJournalTreeControl();
            this._journalViewRtfMarkUp = new Eureca.Professional.FillingFormsTest.RtfMarkUpTextControl();
            this.flowLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._viewNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.Controls.Add(this._saveBtn);
            this.flowLayoutPanel1.Controls.Add(this._cancelBtn);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 747);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1342, 29);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // _saveBtn
            // 
            this._saveBtn.Location = new System.Drawing.Point(1264, 3);
            this._saveBtn.Name = "_saveBtn";
            this._saveBtn.Size = new System.Drawing.Size(75, 23);
            this._saveBtn.TabIndex = 0;
            this._saveBtn.Text = "Ок";
            this._saveBtn.Click += new System.EventHandler(this._okBtn_Click);
            // 
            // _cancelBtn
            // 
            this._cancelBtn.Location = new System.Drawing.Point(1183, 3);
            this._cancelBtn.Name = "_cancelBtn";
            this._cancelBtn.Size = new System.Drawing.Size(75, 23);
            this._cancelBtn.TabIndex = 1;
            this._cancelBtn.Text = "Отмена";
            this._cancelBtn.Click += new System.EventHandler(this._cancelBtn_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this._viewNameTextEdit);
            this.panel1.Controls.Add(this.labelControl1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1342, 35);
            this.panel1.TabIndex = 2;
            // 
            // _viewNameTextEdit
            // 
            this._viewNameTextEdit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._viewNameTextEdit.Location = new System.Drawing.Point(145, 9);
            this._viewNameTextEdit.Name = "_viewNameTextEdit";
            this._viewNameTextEdit.Size = new System.Drawing.Size(1185, 20);
            this._viewNameTextEdit.TabIndex = 1;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(12, 12);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(133, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Название представления:";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.IsSplitterFixed = true;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 35);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.splitContainerControl1.Panel1.Controls.Add(this._journalTreeView);
            this.splitContainerControl1.Panel1.ShowCaption = true;
            this.splitContainerControl1.Panel1.Text = "Журнал";
            this.splitContainerControl1.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.splitContainerControl1.Panel2.Controls.Add(this._journalViewRtfMarkUp);
            this.splitContainerControl1.Panel2.ShowCaption = true;
            this.splitContainerControl1.Panel2.Text = "Представление";
            this.splitContainerControl1.Size = new System.Drawing.Size(1342, 712);
            this.splitContainerControl1.SplitterPosition = 347;
            this.splitContainerControl1.TabIndex = 3;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // _journalTreeView
            // 
            this._journalTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this._journalTreeView.Journal = null;
            this._journalTreeView.Location = new System.Drawing.Point(0, 0);
            this._journalTreeView.Name = "_journalTreeView";
            this._journalTreeView.Readonly = true;
            this._journalTreeView.Size = new System.Drawing.Size(343, 689);
            this._journalTreeView.TabIndex = 0;
            // 
            // _journalViewRtfMarkUp
            // 
            this._journalViewRtfMarkUp.AllowDrop = true;
            this._journalViewRtfMarkUp.CommentColor = System.Drawing.Color.Empty;
            this._journalViewRtfMarkUp.Dock = System.Windows.Forms.DockStyle.Fill;
            this._journalViewRtfMarkUp.ExtendedFormating = false;
            this._journalViewRtfMarkUp.HideCommentsList = true;
            this._journalViewRtfMarkUp.HidePanel = false;
            this._journalViewRtfMarkUp.Location = new System.Drawing.Point(0, 0);
            this._journalViewRtfMarkUp.MarkerColor = System.Drawing.Color.Gray;
            this._journalViewRtfMarkUp.Name = "_journalViewRtfMarkUp";
            this._journalViewRtfMarkUp.ReadOnly = false;
            this._journalViewRtfMarkUp.Rtf = ((System.IO.MemoryStream)(resources.GetObject("_journalViewRtfMarkUp.Rtf")));
            this._journalViewRtfMarkUp.Size = new System.Drawing.Size(985, 689);
            this._journalViewRtfMarkUp.TabIndex = 0;
            // 
            // EditJournalViewForm
            // 
            this.ClientSize = new System.Drawing.Size(1342, 776);
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Name = "EditJournalViewForm";
            this.ShowIcon = false;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.flowLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._viewNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton _saveBtn;
        private DevExpress.XtraEditors.SimpleButton _cancelBtn;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private EditJournalTreeControl _journalTreeView;
        private RtfMarkUpTextControl _journalViewRtfMarkUp;
        private DevExpress.XtraEditors.TextEdit _viewNameTextEdit;
    }
}
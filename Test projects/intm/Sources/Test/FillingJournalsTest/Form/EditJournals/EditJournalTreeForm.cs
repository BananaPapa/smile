﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Eureca.Professional.ActiveRecords;
using Eureca.Utility.Extensions;
using MessageBox = Eureca.Integrator.Utility.Forms.MessageBox;

namespace Eureca.Professional.FillingFormsTest.Gui
{
    public partial class EditJournalTreeForm : XtraForm
    {
        private const string formName = "Редактирование журнала";
        private Journal _journal;
        private string _journalName;
        private AutoResetEvent _waitFormCreation = new AutoResetEvent(false);

        public EditJournalTreeForm( Journal journal )
        {
            if (journal == null)
                throw new ArgumentNullException( "journal" );
            _journal = journal;

            Task.Factory.StartNew( ( ) => Eureca.Integrator.Utility.Forms.MarqueeProgressForm.Show( "Создание формы редактирования",
                   ( form ) => { _waitFormCreation.WaitOne( ); }, this.Owner ) );
            InitializeComponent( );
            _waitFormCreation.Set( );
            Load += EditJournalTreeForm_Load;
        }

        void EditJournalTreeForm_Load( object sender, EventArgs e )
        {
            JournalName = _journal.Name;
            //SetFormName(_journal.Name);
            _editJournalTreeControl.Journal = _journal;
            _editJournalTreeControl.ExpandAll( );
            _journalNameTB.DataBindings.Add( "Text", this, "JournalName", false, DataSourceUpdateMode.OnPropertyChanged );
        }

        public IEnumerable<string> IllegalNames { get; set; }

        public string JournalName
        {
            get
            {
                return _journalName;
            }
            set
            {
                if (_journalName == value)
                    return;
                _journalName = value;
                Text = "{0}: {1}".FormatString( formName, value );
            } 
        }

        public Journal Journal
        {
            get { return _journal; }
        }

        private void _okBtn_Click( object sender, EventArgs e )
        {
            if (!ValidateJournal()) return;
            _journal = Journal.Find(Journal.Id);
            _journal.Name = _journalName;
            _journal.Update();
            DialogResult = DialogResult.OK;
        }

        private bool ValidateJournal()
        {
            StringBuilder sb = new StringBuilder();
            _journalName = _journalName.Trim();
            if (string.IsNullOrWhiteSpace(_journalName))
                sb.AppendLine("Необходимо заполнить название журнала");
            if (IllegalNames != null && IllegalNames.Contains(_journalName))
                sb.AppendLine("Данное имя уже используется.");
            if (!_journal.JournalBranches.Any())
            {
                sb.AppendLine("Журнал должен содержать хотя бы один подраздел");
            }
            if (sb.Length > 0)
            {
                sb.Insert(0, "Журнал содержит следующие ошибки:\n");
                MessageBox.ShowValidationError(sb.ToString());
                return false;
            }
            return true;
        }

        private void _cancelBtn_Click( object sender, EventArgs e )
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}

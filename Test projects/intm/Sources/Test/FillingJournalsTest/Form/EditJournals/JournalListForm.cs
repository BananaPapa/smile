﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Castle.ActiveRecord;
using DevExpress.XtraEditors;
using Eureca.Integrator.Extensions;
using Eureca.Professional.ActiveRecords;
using Eureca.Utility.Extensions;
using DialogResult = System.Windows.Forms.DialogResult;
using NHibernate.Criterion;
using MessageBox = Eureca.Integrator.Utility.Forms.MessageBox;

namespace Eureca.Professional.FillingFormsTest.Gui
{
    public partial class JournalListForm : XtraForm, IDisposable
    {
        private BindingList<Journal> _journals = new BindingList<Journal>();
        private BindingList<JournalView> _journalsViews = new BindingList<JournalView>(); 

        public JournalListForm( )
        {
            InitializeComponent( );

            Load += JournalListForm_Load;
            Closing += JournalListForm_Closing;
        }

        void JournalListForm_Load( object sender, EventArgs e )
        {
            FillJournals();

            _journalsViewsList.DataSource = _journalsViews;
            _journalsViewsList.DisplayMember = "ViewForm.Name";
            _journalsList.DataSource = _journals;
            _journalsList.DisplayMember = "Name";
          
        }

        private void FillJournals(Journal desiredSelection = null)
        {
            _journals.Clear();
            int desiredSelectionIndex = -1;
            Journal[] journals = null;
            using (new SessionScope(FlushAction.Never))
            {
                journals = Journal.FindAll();
            }
            for (int i = 0 ; i < journals.Length ; i++)
            {
                _journals.Add(journals[i]);
                if (desiredSelection!=null && journals[i].Id == desiredSelection.Id)
                    desiredSelectionIndex = i;
            }
            if (desiredSelectionIndex >0)
            {
                _journalsList.SelectedIndex = desiredSelectionIndex;
            }
        }

        void JournalListForm_Closing( object sender, CancelEventArgs e )
        {
            Journal firstfailed = null;
            StringBuilder errors = new StringBuilder();
            foreach (var journal in _journals)
            {
                var error = CheckJournal(journal);
                if (!String.IsNullOrEmpty(error))
                {
                    if (firstfailed == null)
                        firstfailed = journal;
                    errors.AppendLine("Журнал \'{0}\' содержит следующие ошибки:".FormatString(journal.Name));
                    errors.AppendLine(error);
                }
            }
            if (errors.Length > 0)
            {
                MessageBox.ShowValidationError(errors.ToString());
                _journalsList.SelectedItem = firstfailed;
                e.Cancel = true;
            }
            DialogResult = DialogResult.OK;
        }

        string CheckJournal(Journal journal)
        {
            string errors = "";
            Journal syncJournal = null;
            IEnumerable<JournalView> views = null;
            using (new SessionScope(FlushAction.Never))
            {
                syncJournal = Journal.Find( journal.Id );
                views = syncJournal.JournalViews.ToList();
            }
            if (views == null || !views.Any())
                errors += "отсутствуют представления";
            else if (!views.Any(view => view.IsDefault))
                errors += "не выбрано представление по умолчанию";
            return errors;
        }

        private void _journalList_SelectedIndexChanged( object sender, EventArgs e )
        {
            FillViews((Journal)_journalsList.SelectedValue);
        }

        private void FillViews( Journal journal , JournalView desiredViewSelection = null )
        {
            _journalsViews.Clear();
            int counter = 0, desiredSelectionIndex = -1;

            JournalView[] journalViews = null;
            using (new SessionScope(FlushAction.Never))
            {
                journalViews = JournalView.FindAll(Expression.Eq("Journal", journal));
            }
            foreach (var journalView in journalViews)
            {
                _journalsViews.Add(journalView);
                if (desiredViewSelection != null && journalView.Id == desiredViewSelection.Id)
                    desiredSelectionIndex = counter;
                counter++;
            }
            if (desiredSelectionIndex >0)
                _journalsViewsList.SelectedIndex = desiredSelectionIndex;

            var defaultView = _journalsViews.FirstOrDefault(view => view.IsDefault);
            if (defaultView != null)
                for(int i =0 ; i < _journalsViews.Count;i++)
                {
                    var item = _journalsViews[i];
                    if(item == defaultView)
                    {
                        _journalsViewsList.SetItemChecked(i, true);
                        break;
                    }
                }
        }

        private void _delJournalBtn_Click( object sender, EventArgs e )
        {
            var journal = _journalsList.SelectedValue as Journal;
            if (journal == null)
                return;
            if(MessageBox.Confirm("Вы уверены, что хотите удалить журнал и все связанные с ним бланки?"))
            {
                journal.DeleteJournalWithReferences();
                FillJournals( );
            }
        }

        private void _deleteViewBtn_Click( object sender, EventArgs e )
        {
            var journalView = _journalsViewsList.SelectedValue as JournalView;
            if (journalView == null)
                return;
            if (MessageBox.Confirm( "Вы уверены, что хотите удалить представление?" ))
                using (new SessionScope(FlushAction.Never) )
                {
                    journalView.DeleteWithReferences();
                }
            FillViews( _journalsList.SelectedValue as Journal);
            if (_journalsViews.Count == 1)
            {
                _journalsViewsList.SetItemChecked( 0, true );
                var remainingView = _journalsViews[0];
                if (!remainingView.IsDefault)
                {
                    if (remainingView.Id > 0)
                        remainingView = JournalView.Find(remainingView.Id);
                    remainingView.IsDefault = true;
                    remainingView.Save();
                }
            }
        }

        private void _changeViewBtn_Click( object sender, EventArgs e )
        {
            var journal = _journalsList.SelectedValue as Journal;
            var journalView = _journalsViewsList.SelectedValue as JournalView;
            if (journal == null || journalView == null)
                return;
            using (var tran = new TransactionScope( OnDispose.Commit ))
            {
                var viewForm = journalView.ViewForm;
                IEnumerable < Comment > comments = null;
                viewForm = RtfForm.Find(viewForm.Id);
                if (viewForm.Comments.Any())
                    comments = viewForm.Comments.ToList( );

                var editJournalViewForm = new EditJournalViewForm(journal, new MemoryStream(viewForm.Content),
                    viewForm.Name, comments);

                //editJournalViewForm.TopMost = true;
                var res = editJournalViewForm.ShowDialog( this );
                if (res == DialogResult.OK)
                {
                    viewForm = FillingFormsTest.Helper.ApplyBlancChanges(viewForm.Id,editJournalViewForm.ViewBlanc,editJournalViewForm.Comments);
                    viewForm.Name = editJournalViewForm.ViewName;
                    viewForm.UpdateAndFlush();
                }
                else
                    tran.VoteRollBack( );
            }
            FillViews( journal,journalView);
        }


        private void _changeJournalBtn_Click( object sender, EventArgs e )
        {
            var journal = _journalsList.SelectedValue as Journal;
            if (journal == null)
                return;
            using (new SessionScope())
            {
                journal = Journal.Find(journal.Id);
                var form = new EditJournalTreeForm(journal)
                {
                    IllegalNames = _journals.Where(jrnl => jrnl.Id != journal.Id).Select(jrnl => jrnl.Name)
                };
                form.ShowDialog();
                journal = (form as EditJournalTreeForm).Journal;
            }
            
            journal.UpdateAndFlush();
            FillJournals( journal );
        }

        private void _addJournalBrn_Click( object sender, EventArgs e )
        {
            Journal journal = null;
            JournalBranch branch = null;

            using (new SessionScope())
            {
                journal = new Journal() {Name = "Новый журнал"};
                journal.CreateAndFlush();
                branch = new JournalBranch("Новый раздел", journal);
                branch.CreateAndFlush();
                var form = new EditJournalTreeForm(journal) {IllegalNames = _journals.Select(jrnl => jrnl.Name)};
                form.ShowDialog();
                journal = (form as EditJournalTreeForm).Journal;
            }

            journal.UpdateAndFlush( );
            FillJournals(journal);
        }

        private void _addViewBtn_Click( object sender, EventArgs e )
        {
             var journal = _journalsList.SelectedValue as Journal;
            if (journal == null)
                return;
            JournalView journalView =null;
            using (new SessionScope())
            {
                journal = Journal.Find(journal.Id);
                var editJournalViewForm = new EditJournalViewForm(journal);
                //editJournalViewForm.TopMost = true;
                var res = editJournalViewForm.ShowDialog(this);
                if (res == DialogResult.OK)
                {
                    var viewBlanc = new RtfForm(editJournalViewForm.ViewBlanc.GetBuffer(), FormType.BlancJournal,editJournalViewForm.ViewName);
                    viewBlanc.CreateAndFlush( );
                    foreach (var comment in editJournalViewForm.Comments)
                    {
                        var commentEntity = new Comment( viewBlanc, comment.CommentText, comment.CommentType, comment.StartPosition,
                            comment.EndPosition );
                        //commentEntity.CreateAndFlush( );
                        viewBlanc.Comments.Add( commentEntity );
                    }
                    viewBlanc.UpdateAndFlush();
                    journalView = new JournalView(viewBlanc, journal);
                    journalView.CreateAndFlush();
                    journal.JournalViews.Add(journalView);
                }
            }
            FillViews( journal, journalView );
            if (_journalsViews.Count == 1)
            {
                _journalsViewsList.SetItemChecked(0, true);
                var lastJournalView = _journalsViews[0];
                lastJournalView.IsDefault = true;
                lastJournalView.UpdateAndFlush( );
            }
        }

        private bool _programmChangeFlag = false;

        private void _journalsViewsList_ItemChecking( object sender, DevExpress.XtraEditors.Controls.ItemCheckingEventArgs e )
        {
            if(_programmChangeFlag)
                return;
            if(e.NewValue == CheckState.Unchecked)
            {
                e.Cancel = true;
                return;
            }
            _programmChangeFlag = true;
            var changedList = new List<Tuple<bool,JournalView>>();
            var viewListItems = _journalsViewsList.GetItems<JournalView>(_journalsViews.Count);
            var checkedItem = viewListItems[e.Index];
            for(int i = 0 ; i <viewListItems.Count; i ++)
            {
                var journalsView = viewListItems[i];
                if (journalsView != checkedItem)
                {
                    if (journalsView.IsDefault)
                    {
                        changedList.Add( new Tuple<bool, JournalView>( false, journalsView ) );
                    }
                    _journalsViewsList.SetItemChecked(i,false);
                }
            }
            if (!checkedItem.IsDefault)
            {
                changedList.Add( new Tuple<bool, JournalView>( true, checkedItem ) );
            }
            _journalsViewsList.SetItemChecked( e.Index, true );
            foreach (var journalView in changedList)
            {
                var syncView = JournalView.Find(journalView.Item2.Id);
                syncView.IsDefault = journalView.Item1;
                syncView.UpdateAndFlush();
            }
            _programmChangeFlag = false;

        }
    
    }
}

﻿using System.Windows.Forms;
using DevExpress.XtraRichEdit.Commands;

namespace Eureca.Professional.FillingFormsTest.Gui
{
    partial class JournalListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if (disposing && ( components != null ))
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this._addJournalBrn = new DevExpress.XtraEditors.SimpleButton();
            this._delJournalBtn = new DevExpress.XtraEditors.SimpleButton();
            this._changeJournalBtn = new DevExpress.XtraEditors.SimpleButton();
            this._journalsList = new DevExpress.XtraEditors.ListBoxControl();
            this._journalsViewsList = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this._addViewBtn = new DevExpress.XtraEditors.SimpleButton();
            this._deleteViewBtn = new DevExpress.XtraEditors.SimpleButton();
            this._changeViewBtn = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._journalsList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._journalsViewsList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.IsSplitterFixed = true;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.splitContainerControl1.Panel1.Controls.Add(this.groupControl2);
            this.splitContainerControl1.Panel1.Controls.Add(this._journalsList);
            this.splitContainerControl1.Panel1.ShowCaption = true;
            this.splitContainerControl1.Panel1.Text = "Журналы";
            this.splitContainerControl1.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.splitContainerControl1.Panel2.Controls.Add(this._journalsViewsList);
            this.splitContainerControl1.Panel2.Controls.Add(this.groupControl1);
            this.splitContainerControl1.Panel2.ShowCaption = true;
            this.splitContainerControl1.Panel2.Text = "Представления";
            this.splitContainerControl1.Size = new System.Drawing.Size(819, 632);
            this.splitContainerControl1.SplitterPosition = 401;
            this.splitContainerControl1.TabIndex = 0;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // groupControl2
            // 
            this.groupControl2.AutoSize = true;
            this.groupControl2.Controls.Add(this.tableLayoutPanel2);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupControl2.Location = new System.Drawing.Point(0, 553);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(397, 56);
            this.groupControl2.TabIndex = 3;
            this.groupControl2.Text = "Управление журналами";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.AutoSize = true;
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.Controls.Add(this._addJournalBrn, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this._delJournalBtn, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this._changeJournalBtn, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(2, 21);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(393, 33);
            this.tableLayoutPanel2.TabIndex = 3;
            // 
            // _addJournalBrn
            // 
            this._addJournalBrn.Dock = System.Windows.Forms.DockStyle.Fill;
            this._addJournalBrn.Location = new System.Drawing.Point(267, 5);
            this._addJournalBrn.Margin = new System.Windows.Forms.Padding(5);
            this._addJournalBrn.Name = "_addJournalBrn";
            this._addJournalBrn.Size = new System.Drawing.Size(121, 23);
            this._addJournalBrn.TabIndex = 1;
            this._addJournalBrn.Text = "Добавить";
            this._addJournalBrn.Click += new System.EventHandler(this._addJournalBrn_Click);
            // 
            // _delJournalBtn
            // 
            this._delJournalBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this._delJournalBtn.Location = new System.Drawing.Point(5, 5);
            this._delJournalBtn.Margin = new System.Windows.Forms.Padding(5);
            this._delJournalBtn.Name = "_delJournalBtn";
            this._delJournalBtn.Size = new System.Drawing.Size(121, 23);
            this._delJournalBtn.TabIndex = 0;
            this._delJournalBtn.Text = "Удалить";
            this._delJournalBtn.Click += new System.EventHandler(this._delJournalBtn_Click);
            // 
            // _changeJournalBtn
            // 
            this._changeJournalBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this._changeJournalBtn.Location = new System.Drawing.Point(136, 5);
            this._changeJournalBtn.Margin = new System.Windows.Forms.Padding(5);
            this._changeJournalBtn.Name = "_changeJournalBtn";
            this._changeJournalBtn.Size = new System.Drawing.Size(121, 23);
            this._changeJournalBtn.TabIndex = 2;
            this._changeJournalBtn.Text = "Изменить";
            this._changeJournalBtn.Click += new System.EventHandler(this._changeJournalBtn_Click);
            // 
            // _journalsList
            // 
            this._journalsList.Dock = System.Windows.Forms.DockStyle.Fill;
            this._journalsList.Location = new System.Drawing.Point(0, 0);
            this._journalsList.Name = "_journalsList";
            this._journalsList.Size = new System.Drawing.Size(397, 609);
            this._journalsList.TabIndex = 2;
            this._journalsList.SelectedIndexChanged += new System.EventHandler(this._journalList_SelectedIndexChanged);
            // 
            // _journalsViewsList
            // 
            this._journalsViewsList.Dock = System.Windows.Forms.DockStyle.Fill;
            this._journalsViewsList.Location = new System.Drawing.Point(0, 0);
            this._journalsViewsList.Name = "_journalsViewsList";
            this._journalsViewsList.Size = new System.Drawing.Size(408, 553);
            this._journalsViewsList.TabIndex = 5;
            this._journalsViewsList.ItemChecking += new DevExpress.XtraEditors.Controls.ItemCheckingEventHandler(this._journalsViewsList_ItemChecking);
            // 
            // groupControl1
            // 
            this.groupControl1.AutoSize = true;
            this.groupControl1.Controls.Add(this.tableLayoutPanel1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupControl1.Location = new System.Drawing.Point(0, 553);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(408, 56);
            this.groupControl1.TabIndex = 4;
            this.groupControl1.Text = "Управление представлениями";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Controls.Add(this._addViewBtn, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this._deleteViewBtn, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this._changeViewBtn, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(2, 21);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(404, 33);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // _addViewBtn
            // 
            this._addViewBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this._addViewBtn.Location = new System.Drawing.Point(273, 5);
            this._addViewBtn.Margin = new System.Windows.Forms.Padding(5);
            this._addViewBtn.Name = "_addViewBtn";
            this._addViewBtn.Size = new System.Drawing.Size(126, 23);
            this._addViewBtn.TabIndex = 6;
            this._addViewBtn.Text = "Добавить";
            this._addViewBtn.Click += new System.EventHandler(this._addViewBtn_Click);
            // 
            // _deleteViewBtn
            // 
            this._deleteViewBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this._deleteViewBtn.Location = new System.Drawing.Point(5, 5);
            this._deleteViewBtn.Margin = new System.Windows.Forms.Padding(5);
            this._deleteViewBtn.Name = "_deleteViewBtn";
            this._deleteViewBtn.Size = new System.Drawing.Size(124, 23);
            this._deleteViewBtn.TabIndex = 7;
            this._deleteViewBtn.Text = "Удалить";
            this._deleteViewBtn.Click += new System.EventHandler(this._deleteViewBtn_Click);
            // 
            // _changeViewBtn
            // 
            this._changeViewBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this._changeViewBtn.Location = new System.Drawing.Point(139, 5);
            this._changeViewBtn.Margin = new System.Windows.Forms.Padding(5);
            this._changeViewBtn.Name = "_changeViewBtn";
            this._changeViewBtn.Size = new System.Drawing.Size(124, 23);
            this._changeViewBtn.TabIndex = 8;
            this._changeViewBtn.Text = "Изменить";
            this._changeViewBtn.Click += new System.EventHandler(this._changeViewBtn_Click);
            // 
            // JournalListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(819, 632);
            this.Controls.Add(this.splitContainerControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "JournalListForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Журналы и представления ";
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._journalsList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._journalsViewsList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.ListBoxControl _journalsList;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.SimpleButton _changeJournalBtn;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton _addViewBtn;
        private DevExpress.XtraEditors.SimpleButton _changeViewBtn;
        private DevExpress.XtraEditors.SimpleButton _deleteViewBtn;
        private DevExpress.XtraEditors.SimpleButton _addJournalBrn;
        private DevExpress.XtraEditors.SimpleButton _delJournalBtn;
        private DevExpress.XtraEditors.CheckedListBoxControl _journalsViewsList;
    }
}
﻿namespace Eureca.Professional.FillingFormsTest.Gui
{
    partial class EditJournalTreeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if (disposing && ( components != null ))
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this._okBtn = new DevExpress.XtraEditors.SimpleButton();
            this._cancelBtn = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this._journalNameTB = new System.Windows.Forms.TextBox();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this._editJournalTreeControl = new Eureca.Professional.FillingFormsTest.Gui.EditJournalTreeControl();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.Controls.Add(this._okBtn);
            this.flowLayoutPanel1.Controls.Add(this._cancelBtn);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 723);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Padding = new System.Windows.Forms.Padding(10);
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1238, 49);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // _okBtn
            // 
            this._okBtn.Location = new System.Drawing.Point(1140, 13);
            this._okBtn.Name = "_okBtn";
            this._okBtn.Size = new System.Drawing.Size(75, 23);
            this._okBtn.TabIndex = 1;
            this._okBtn.Text = "Ок";
            this._okBtn.Click += new System.EventHandler(this._okBtn_Click);
            // 
            // _cancelBtn
            // 
            this._cancelBtn.Location = new System.Drawing.Point(1059, 13);
            this._cancelBtn.Name = "_cancelBtn";
            this._cancelBtn.Size = new System.Drawing.Size(75, 23);
            this._cancelBtn.TabIndex = 0;
            this._cancelBtn.Text = "Отмена";
            this._cancelBtn.Visible = false;
            this._cancelBtn.Click += new System.EventHandler(this._cancelBtn_Click);
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(9, 11);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(10, 3, 10, 3);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(52, 13);
            this.labelControl2.TabIndex = 8;
            this.labelControl2.Text = "Название:";
            // 
            // _journalNameTB
            // 
            this._journalNameTB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._journalNameTB.Location = new System.Drawing.Point(9, 30);
            this._journalNameTB.Margin = new System.Windows.Forms.Padding(10, 3, 10, 3);
            this._journalNameTB.Name = "_journalNameTB";
            this._journalNameTB.Size = new System.Drawing.Size(1210, 21);
            this._journalNameTB.TabIndex = 9;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(9, 57);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(10, 3, 10, 3);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(53, 13);
            this.labelControl1.TabIndex = 7;
            this.labelControl1.Text = "Иерархия:";
            // 
            // _editJournalTreeControl
            // 
            this._editJournalTreeControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._editJournalTreeControl.Journal = null;
            this._editJournalTreeControl.Location = new System.Drawing.Point(9, 76);
            this._editJournalTreeControl.Margin = new System.Windows.Forms.Padding(10, 3, 10, 3);
            this._editJournalTreeControl.Name = "_editJournalTreeControl";
            this._editJournalTreeControl.Readonly = false;
            this._editJournalTreeControl.Size = new System.Drawing.Size(1210, 641);
            this._editJournalTreeControl.TabIndex = 6;
            // 
            // EditJournalTreeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1238, 772);
            this.ControlBox = false;
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this._journalNameTB);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this._editJournalTreeControl);
            this.Controls.Add(this.flowLayoutPanel1);
            this.MinimumSize = new System.Drawing.Size(1210, 766);
            this.Name = "EditJournalTreeForm";
            this.ShowIcon = false;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton _cancelBtn;
        private DevExpress.XtraEditors.SimpleButton _okBtn;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private System.Windows.Forms.TextBox _journalNameTB;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private Gui.EditJournalTreeControl _editJournalTreeControl;

    }
}
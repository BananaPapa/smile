﻿using Eureca.Utility.Helper;

namespace Eureca.Professional.FillingFormsTest.Gui
{
    partial class FillingBlancsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if (disposing && ( components != null ))
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FillingBlancsForm));
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this._assigmentRichEdit = new DevExpress.XtraRichEdit.RichEditControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this._journalsListBox = new DevExpress.XtraEditors.ListBoxControl();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this._filledBlancs = new DevExpress.XtraGrid.GridControl();
            this._filledBlancsView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._blancName = new DevExpress.XtraGrid.Columns.GridColumn();
            this._journalViewRtfExtControl = new Eureca.Professional.FillingFormsTest.RtfMarkUpTextControl();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._journalsListBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._filledBlancs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._filledBlancsView)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.splitContainerControl1.Panel1.Controls.Add(this.tableLayoutPanel1);
            this.splitContainerControl1.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.splitContainerControl1.Panel2.Controls.Add(this._journalViewRtfExtControl);
            this.splitContainerControl1.Panel2.ShowCaption = true;
            this.splitContainerControl1.Panel2.Text = "Журнал";
            this.splitContainerControl1.Size = new System.Drawing.Size(1329, 810);
            this.splitContainerControl1.SplitterPosition = 709;
            this.splitContainerControl1.TabIndex = 1;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.groupControl1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.groupControl2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.groupControl3, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(705, 806);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this._assigmentRichEdit);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(3, 3);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(699, 316);
            this.groupControl1.TabIndex = 3;
            this.groupControl1.Text = "Задание";
            // 
            // _assigmentRichEdit
            // 
            this._assigmentRichEdit.Dock = System.Windows.Forms.DockStyle.Fill;
            this._assigmentRichEdit.Location = new System.Drawing.Point(2, 21);
            this._assigmentRichEdit.Name = "_assigmentRichEdit";
            this._assigmentRichEdit.Options.Fields.UseCurrentCultureDateTimeFormat = false;
            this._assigmentRichEdit.Options.HorizontalRuler.Visibility = DevExpress.XtraRichEdit.RichEditRulerVisibility.Hidden;
            this._assigmentRichEdit.Options.MailMerge.KeepLastParagraph = false;
            this._assigmentRichEdit.Options.VerticalRuler.Visibility = DevExpress.XtraRichEdit.RichEditRulerVisibility.Hidden;
            this._assigmentRichEdit.ReadOnly = true;
            this._assigmentRichEdit.Size = new System.Drawing.Size(695, 293);
            this._assigmentRichEdit.TabIndex = 1;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this._journalsListBox);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl2.Location = new System.Drawing.Point(3, 325);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(699, 155);
            this.groupControl2.TabIndex = 4;
            this.groupControl2.Text = "Журналы";
            // 
            // _journalsListBox
            // 
            this._journalsListBox.DisplayMember = "BlancName";
            this._journalsListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this._journalsListBox.Location = new System.Drawing.Point(2, 21);
            this._journalsListBox.Name = "_journalsListBox";
            this._journalsListBox.Size = new System.Drawing.Size(695, 132);
            this._journalsListBox.TabIndex = 2;
            this._journalsListBox.SelectedValueChanged += new System.EventHandler(this._journalsList_SelectedValueChanged);
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this._filledBlancs);
            this.groupControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl3.Location = new System.Drawing.Point(3, 486);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(699, 317);
            this.groupControl3.TabIndex = 5;
            this.groupControl3.Text = "Заполненные бланки";
            // 
            // _filledBlancs
            // 
            this._filledBlancs.Dock = System.Windows.Forms.DockStyle.Fill;
            this._filledBlancs.Location = new System.Drawing.Point(2, 21);
            this._filledBlancs.MainView = this._filledBlancsView;
            this._filledBlancs.Name = "_filledBlancs";
            this._filledBlancs.Size = new System.Drawing.Size(695, 294);
            this._filledBlancs.TabIndex = 3;
            this._filledBlancs.UseEmbeddedNavigator = true;
            this._filledBlancs.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this._filledBlancsView});
            // 
            // _filledBlancsView
            // 
            this._filledBlancsView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._blancName});
            this._filledBlancsView.GridControl = this._filledBlancs;
            this._filledBlancsView.Name = "_filledBlancsView";
            this._filledBlancsView.OptionsView.ShowGroupPanel = false;
            this._filledBlancsView.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            // 
            // _blancName
            // 
            this._blancName.Caption = "Заполненные бланки";
            this._blancName.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this._blancName.FieldName = "BlancName";
            this._blancName.Name = "_blancName";
            this._blancName.OptionsColumn.AllowEdit = false;
            this._blancName.OptionsColumn.ReadOnly = true;
            this._blancName.Visible = true;
            this._blancName.VisibleIndex = 0;
            // 
            // _journalViewRtfExtControl
            // 
            this._journalViewRtfExtControl.CommentColor = System.Drawing.Color.Empty;
            this._journalViewRtfExtControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._journalViewRtfExtControl.ExtendedFormating = false;
            this._journalViewRtfExtControl.HideCommentsList = true;
            this._journalViewRtfExtControl.HidePanel = true;
            this._journalViewRtfExtControl.Location = new System.Drawing.Point(0, 0);
            this._journalViewRtfExtControl.MarkerColor = System.Drawing.Color.Empty;
            this._journalViewRtfExtControl.Name = "_journalViewRtfExtControl";
            this._journalViewRtfExtControl.ReadOnly = true;
            this._journalViewRtfExtControl.Rtf = ((System.IO.MemoryStream)(resources.GetObject("_journalViewRtfExtControl.Rtf")));
            this._journalViewRtfExtControl.Size = new System.Drawing.Size(610, 787);
            this._journalViewRtfExtControl.TabIndex = 0;
            this._journalViewRtfExtControl.OnLinkClick += new System.EventHandler<ItemEventArgs<Eureca.Professional.ActiveRecords.CommentLink>>( this.OnCommentLinkClick );
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.Controls.Add(this.simpleButton1);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 810);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1329, 23);
            this.flowLayoutPanel1.TabIndex = 2;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(1254, 0);
            this.simpleButton1.Margin = new System.Windows.Forms.Padding(0);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 1;
            this.simpleButton1.Text = "Завершить";
            this.simpleButton1.Click += new System.EventHandler(this._endTestButton_Click);
            // 
            // FillingBlancsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1329, 833);
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Name = "FillingBlancsForm";
            this.ShowIcon = false;
            this.Text = "FillingBlancsForm";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._journalsListBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._filledBlancs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._filledBlancsView)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraRichEdit.RichEditControl _assigmentRichEdit;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.ListBoxControl _journalsListBox;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraGrid.GridControl _filledBlancs;
        private DevExpress.XtraGrid.Views.Grid.GridView _filledBlancsView;
        private RtfMarkUpTextControl _journalViewRtfExtControl;
        private DevExpress.XtraGrid.Columns.GridColumn _blancName;

    }
}
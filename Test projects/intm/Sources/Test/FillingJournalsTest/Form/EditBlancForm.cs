﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Eureca.Integrator.Common;
using Eureca.Integrator.Extensions;
using Eureca.Professional.FillingFormsTest.Properties;
using MessageBox = Eureca.Integrator.Utility.Forms.MessageBox; 

namespace Eureca.Professional.FillingFormsTest.Gui
{
    public partial class BlancEditForm : PreloadMarqueeForm
    {
        MemoryStream _template,_original;
        private bool _isSplitContainerHorisontal = true;
        public MemoryStream EditedBlanc 
        {
            get
            {
                if (DialogResult == DialogResult.Cancel)
                    return _original;
                return _documentEditor.SaveRtf( );
            }
            set
            {
                _documentEditor.LoadRtf( value );
            }
        }


        internal BlancEditForm( MemoryStream blanc = null, MemoryStream template = null, MemoryStream aditionalInformation = null, bool showToolPanel = false )
        {
            StartMarquee( "Создание формы редактирования..." );
            InitializeComponent(  );
            Closing += BlancEditForm_Closing;

            _ribbonControl.Visible = showToolPanel;
            if (blanc != null)
            {
                _original = blanc;
                EditedBlanc = blanc;
            }
            if (template != null)
            {
                _template = template;
                _abortBtn.Visible = true;
            }
            if (aditionalInformation != null)
                _aditionalInformation.LoadRtf( aditionalInformation );
            else
            {
                _callapseButton.Visible = false;
                _splitContainerControl.PanelVisibility = SplitPanelVisibility.Panel1;
                _verticalCheckedBtn.Visible = false;
                _horizontalCheckBtn.Visible = false;
            }
            CloseMarquee();
        }

        void BlancEditForm_Closing( object sender, CancelEventArgs e )
        {
            if (DialogResult == DialogResult.Cancel && _documentEditor.Modified && !ConfirmClosing( ))
                e.Cancel = true;
        }

        private bool ConfirmClosing()
        {
            var res = MessageBox.ConfirmOrCancel("Изменения будут утеряны. Сохранить изменения?");
            if (res.HasValue)
            {
                DialogResult = res.Value ? DialogResult.OK : DialogResult.Cancel;
                return true;
            }
            else
            {
                return false;
            }
        }

        public static DialogResult Edit( ref MemoryStream blanc, MemoryStream template = null, MemoryStream aditionalInformation = null, string title = null, bool showToolPanel = false,System.Windows.Forms.Form owner = null )
        {
            var form = new BlancEditForm( blanc, template, aditionalInformation, showToolPanel ) { Text = title ?? "Редактирование документа" };
            var res =  form.ShowDialog(owner);
            blanc = form.EditedBlanc;
            return res;
        }

        private void _saveBatton_Click( object sender, EventArgs e )
        {
            DialogResult = DialogResult.OK;
        }

        private void _cancelBtn_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void simpleButton1_Click( object sender, EventArgs e )
        {
            if (MessageBox.Confirm("Текущее заполнение бланка будет сброшено к исходному состоянию. Вы уверены?"))
            {
                _documentEditor.LoadRtf(new MemoryStream(_template.GetBuffer()));
            }
        }

        private void _callapseButton_CheckedChanged( object sender, EventArgs e )
        {
            CheckButton senderButton = (CheckButton)sender;
            RefreshCallapseButton(senderButton);
        }

        private void RefreshCallapseButton(CheckButton senderButton)
        {
            if (senderButton.Checked)
            {
                senderButton.Image = _isSplitContainerHorisontal ? Resources.left : Resources.up;
                senderButton.ToolTip = "Развернуть задание";
                _splitContainerControl.PanelVisibility = SplitPanelVisibility.Panel1;
            }
            else
            {
                senderButton.Image = _isSplitContainerHorisontal ? Resources.right : Resources.down;
                senderButton.ToolTip = "Свернуть задание";
                _splitContainerControl.PanelVisibility = SplitPanelVisibility.Both;
            }
        }

        private void switchOrientation_CheckedChanged( object sender, EventArgs e )
        {
            _isSplitContainerHorisontal = _splitContainerControl.Horizontal = _verticalCheckedBtn.Checked;
            if (_isSplitContainerHorisontal)
            {
                _callapseButton.Dock = DockStyle.Right;
                _callapseButton.MaximumSize = new Size(18,0);
            }
            else
            {
                _callapseButton.Dock = DockStyle.Bottom;
                _callapseButton.MaximumSize = new Size( 0, 18 );
            }
            _documentEditor.BringToFront();
            RefreshCallapseButton(_callapseButton);
        }
    }
}

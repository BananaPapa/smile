﻿namespace Eureca.Professional.FillingFormsTest.Gui
{
    partial class EditAssigmentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if (disposing && ( components != null ))
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this._assignmentsRichEdit = new DevExpress.XtraRichEdit.RichEditControl();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this._blancsGrid = new DevExpress.XtraGrid.GridControl();
            this._blancsView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._blancGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._standartsGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._repositoryItemStandartCheckEdit = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this._journalViewsGrid = new DevExpress.XtraGrid.GridControl();
            this.journalViews = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._journalColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this._journalViewColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.journalsViewItemLookUpEdit = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this._okBtn = new DevExpress.XtraEditors.SimpleButton();
            this._cancelBtn = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._blancsGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._blancsView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._repositoryItemStandartCheckEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._journalViewsGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.journalViews)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.journalsViewItemLookUpEdit)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.splitContainerControl1.Panel1.Controls.Add(this._assignmentsRichEdit);
            this.splitContainerControl1.Panel1.ShowCaption = true;
            this.splitContainerControl1.Panel1.Text = "Задание";
            this.splitContainerControl1.Panel2.Controls.Add(this.splitContainerControl2);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1174, 757);
            this.splitContainerControl1.SplitterPosition = 823;
            this.splitContainerControl1.TabIndex = 0;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // _assignmentsRichEdit
            // 
            this._assignmentsRichEdit.Dock = System.Windows.Forms.DockStyle.Fill;
            this._assignmentsRichEdit.Location = new System.Drawing.Point(0, 0);
            this._assignmentsRichEdit.Name = "_assignmentsRichEdit";
            this._assignmentsRichEdit.Options.Fields.UseCurrentCultureDateTimeFormat = false;
            this._assignmentsRichEdit.Options.HorizontalRuler.Visibility = DevExpress.XtraRichEdit.RichEditRulerVisibility.Hidden;
            this._assignmentsRichEdit.Options.MailMerge.KeepLastParagraph = false;
            this._assignmentsRichEdit.Options.VerticalRuler.Visibility = DevExpress.XtraRichEdit.RichEditRulerVisibility.Hidden;
            this._assignmentsRichEdit.Size = new System.Drawing.Size(819, 734);
            this._assignmentsRichEdit.TabIndex = 0;
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Horizontal = false;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.splitContainerControl2.Panel1.Controls.Add(this._blancsGrid);
            this.splitContainerControl2.Panel1.ShowCaption = true;
            this.splitContainerControl2.Panel1.Text = "Бланки";
            this.splitContainerControl2.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.splitContainerControl2.Panel2.Controls.Add(this._journalViewsGrid);
            this.splitContainerControl2.Panel2.ShowCaption = true;
            this.splitContainerControl2.Panel2.Text = "Представления";
            this.splitContainerControl2.Size = new System.Drawing.Size(346, 757);
            this.splitContainerControl2.SplitterPosition = 385;
            this.splitContainerControl2.TabIndex = 2;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // _blancsGrid
            // 
            this._blancsGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._blancsGrid.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this._blancsGrid.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this._blancsGrid.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this._blancsGrid.Location = new System.Drawing.Point(0, 0);
            this._blancsGrid.MainView = this._blancsView;
            this._blancsGrid.Name = "_blancsGrid";
            this._blancsGrid.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this._repositoryItemStandartCheckEdit});
            this._blancsGrid.Size = new System.Drawing.Size(342, 362);
            this._blancsGrid.TabIndex = 3;
            this._blancsGrid.UseEmbeddedNavigator = true;
            this._blancsGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this._blancsView});
            // 
            // _blancsView
            // 
            this._blancsView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._blancGridColumn,
            this._standartsGridColumn});
            this._blancsView.GridControl = this._blancsGrid;
            this._blancsView.Name = "_blancsView";
            this._blancsView.OptionsNavigation.AutoFocusNewRow = true;
            this._blancsView.OptionsView.ShowGroupPanel = false;
            this._blancsView.DoubleClick += new System.EventHandler(this._blancsView_DoubleClick);
            // 
            // _blancGridColumn
            // 
            this._blancGridColumn.Caption = "Бланк";
            this._blancGridColumn.FieldName = "Form.FullPath";
            this._blancGridColumn.Name = "_blancGridColumn";
            this._blancGridColumn.OptionsColumn.AllowEdit = false;
            this._blancGridColumn.OptionsColumn.ReadOnly = true;
            this._blancGridColumn.Visible = true;
            this._blancGridColumn.VisibleIndex = 0;
            this._blancGridColumn.Width = 160;
            // 
            // _standartsGridColumn
            // 
            this._standartsGridColumn.Caption = "Образец";
            this._standartsGridColumn.ColumnEdit = this._repositoryItemStandartCheckEdit;
            this._standartsGridColumn.FieldName = "IsFillingPresent";
            this._standartsGridColumn.Name = "_standartsGridColumn";
            this._standartsGridColumn.OptionsColumn.AllowEdit = false;
            this._standartsGridColumn.OptionsColumn.ReadOnly = true;
            this._standartsGridColumn.Visible = true;
            this._standartsGridColumn.VisibleIndex = 1;
            this._standartsGridColumn.Width = 50;
            // 
            // _repositoryItemStandartCheckEdit
            // 
            this._repositoryItemStandartCheckEdit.AutoHeight = false;
            this._repositoryItemStandartCheckEdit.Caption = "Check";
            this._repositoryItemStandartCheckEdit.Name = "_repositoryItemStandartCheckEdit";
            this._repositoryItemStandartCheckEdit.ReadOnly = true;
            // 
            // _journalViewsGrid
            // 
            this._journalViewsGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._journalViewsGrid.Location = new System.Drawing.Point(0, 0);
            this._journalViewsGrid.MainView = this.journalViews;
            this._journalViewsGrid.Name = "_journalViewsGrid";
            this._journalViewsGrid.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.journalsViewItemLookUpEdit});
            this._journalViewsGrid.Size = new System.Drawing.Size(342, 344);
            this._journalViewsGrid.TabIndex = 1;
            this._journalViewsGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.journalViews});
            // 
            // journalViews
            // 
            this.journalViews.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._journalColumn,
            this._journalViewColumn});
            this.journalViews.GridControl = this._journalViewsGrid;
            this.journalViews.Name = "journalViews";
            this.journalViews.OptionsView.ShowGroupPanel = false;
            // 
            // _journalColumn
            // 
            this._journalColumn.Caption = "Журнал";
            this._journalColumn.FieldName = "Journal.Name";
            this._journalColumn.Name = "_journalColumn";
            this._journalColumn.OptionsColumn.ReadOnly = true;
            this._journalColumn.Visible = true;
            this._journalColumn.VisibleIndex = 0;
            // 
            // _journalViewColumn
            // 
            this._journalViewColumn.Caption = "Представление";
            this._journalViewColumn.FieldName = "ViewForm.Name";
            this._journalViewColumn.Name = "_journalViewColumn";
            this._journalViewColumn.OptionsColumn.ReadOnly = true;
            this._journalViewColumn.Visible = true;
            this._journalViewColumn.VisibleIndex = 1;
            // 
            // journalsViewItemLookUpEdit
            // 
            this.journalsViewItemLookUpEdit.AutoHeight = false;
            this.journalsViewItemLookUpEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.journalsViewItemLookUpEdit.Name = "journalsViewItemLookUpEdit";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.Controls.Add(this._okBtn);
            this.flowLayoutPanel1.Controls.Add(this._cancelBtn);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 757);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1174, 29);
            this.flowLayoutPanel1.TabIndex = 1;
            // 
            // _okBtn
            // 
            this._okBtn.Location = new System.Drawing.Point(1096, 3);
            this._okBtn.Name = "_okBtn";
            this._okBtn.Size = new System.Drawing.Size(75, 23);
            this._okBtn.TabIndex = 0;
            this._okBtn.Text = "Ок";
            this._okBtn.Click += new System.EventHandler(this._okBtn_Click);
            // 
            // _cancelBtn
            // 
            this._cancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._cancelBtn.Location = new System.Drawing.Point(1015, 3);
            this._cancelBtn.Name = "_cancelBtn";
            this._cancelBtn.Size = new System.Drawing.Size(75, 23);
            this._cancelBtn.TabIndex = 1;
            this._cancelBtn.Text = "Отмена";
            this._cancelBtn.Visible = false;
            // 
            // EditAssigmentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1174, 786);
            this.ControlBox = false;
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Name = "EditAssigmentForm";
            this.ShowIcon = false;
            this.Text = "Редактирование задания";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._blancsGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._blancsView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._repositoryItemStandartCheckEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._journalViewsGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.journalViews)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.journalsViewItemLookUpEdit)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraRichEdit.RichEditControl _assignmentsRichEdit;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraGrid.GridControl _journalViewsGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView journalViews;
        private DevExpress.XtraGrid.Columns.GridColumn _journalColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _journalViewColumn;
        private DevExpress.XtraGrid.GridControl _blancsGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView _blancsView;
        private DevExpress.XtraGrid.Columns.GridColumn _blancGridColumn;
        private DevExpress.XtraGrid.Columns.GridColumn _standartsGridColumn;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton _okBtn;
        private DevExpress.XtraEditors.SimpleButton _cancelBtn;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit journalsViewItemLookUpEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit _repositoryItemStandartCheckEdit;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Castle.ActiveRecord;
using DevExpress.CodeParser;
using DevExpress.XtraEditors;
using Eureca.Integrator.Extensions;
using Eureca.Professional.ActiveRecords;
using Eureca.Professional.FillingFormsTest.Gui;
using Eureca.Utility.Extensions;
using NHibernate.Criterion;
using Expression = NHibernate.Criterion.Expression;
using MessageBox = Eureca.Integrator.Utility.Forms.MessageBox;

namespace Eureca.Professional.FillingFormsTest.Gui
{
    public partial class FormFillingTestAssigmentsListForm : XtraForm , IDisposable
    {
        private const int questionTitleLength = 80;
        private const string _formTitle = "Редактирование заданий";
        private readonly Test _test;
        private readonly BindingList<Tuple<string,TestQuestion>> _questions = new BindingList<Tuple<string, TestQuestion>>();
        private RichTextBox _richTextBoxControl = new RichTextBox();
        public FormFillingTestAssigmentsListForm( Test test )
        {
            if(test.Type.PlugInFileName != Constants.PlugInName)
                throw new InvalidOperationException("Неверный тест");
            InitializeComponent( );
            _test = Test.Find( test.Id );
            Text = "{0}: {1}".FormatString( _formTitle, _test.Title );
            FillQuestions();
            _questionListBox.DataSource = _questions;
        }

        void FillQuestions()
        {
            _questions.Clear();
            TestQuestion[] testQuestions = null;
            using (new SessionScope(FlushAction.Never))
            {
                testQuestions = TestQuestion.FindAll( Order.Asc( "QuestionOrder" ), Expression.Eq( "Test", _test ) );
            }

            foreach (var question in testQuestions)
            {
                _questions.Add( new Tuple<string, TestQuestion>( GetQuestionTitle(question.Question), question ) );
            }
            if(_questions.Count == 0)
                _assigmentTextRichEdit.Text = "";
        }

        private string GetQuestionTitle(Question question)
        {
            byte[] richContent = null;
            using (new SessionScope(FlushAction.Never))
            {
                question = Question.Find(question.Id);
                richContent = question.Content.RichContent;
            }
            if (richContent != null && richContent.Length>0)
                _richTextBoxControl.LoadRtf(new MemoryStream(richContent));
            var decodeString = _richTextBoxControl.Text.TrimStart(new []{'\t',' '});
            return decodeString.Substring(0, Math.Min(questionTitleLength, decodeString.Length));
        }

        private void _deleteQuestionBtn_Click( object sender, EventArgs e )
        {
            var selectedItem = _questionListBox.SelectedItem;
            if(selectedItem == null || !MessageBox.Confirm("Вы уверены, что хотите удалить задание?"))
                return;

            using (new SessionScope(FlushAction.Never))
            {
                var testQuestion = (selectedItem as Tuple<string, TestQuestion>).Item2;
                testQuestion = TestQuestion.FindOne(Expression.And(Expression.Eq("Test", testQuestion.Test),
                        Expression.Eq("Question", testQuestion.Question)));
                
                if (testQuestion.Question.Id > 0)
                {
                    foreach (
                        var questionJournal in
                            QuestionJournals.FindAll(Expression.Eq("Question", testQuestion.Question)))
                    {
                        questionJournal.DeleteAndFlush();
                    }
                    testQuestion.Question.DeleteWithReferences(false,testQuestion.Test);

                }
                else
                {
                    testQuestion.DeleteAndFlush( );
                }

            }
            FillQuestions();
        }

        private void _changeQuestionBtn_Click( object sender, EventArgs e )
        {
            var selectedItem = _questionListBox.SelectedItem as Tuple<string, TestQuestion>;
            if (selectedItem == null)
                return;
            var editAssigmentForm = new EditAssigmentForm( selectedItem.Item2.Question );
            editAssigmentForm.ShowDialog();
            FillQuestions();
            _questionListBox.SelectedIndex = -1;
            _questionListBox.SelectedIndex = _questions.IndexOf(selectedItem);
        }

        private void _addQuerstionBtn_Click(object sender, EventArgs e)
        {
            Question newQuestion = null;
            var question = new Question( new Content( new byte[0] ) );  
            question.CreateAndFlush();
            EditAssigmentForm editAssigmentForm = new EditAssigmentForm( question, true );
            editAssigmentForm.ShowDialog();
            newQuestion = (editAssigmentForm as EditAssigmentForm).Question;
            newQuestion.Update();//AndFlush();

            var testQuestion = new TestQuestion( _test, newQuestion,
                _questions.Select( tuple => tuple.Item2.QuestionOrder ).DefaultIfEmpty( -1 ).Max( ) + 1 );
            testQuestion.CreateAndFlush( );
            var item = new Tuple<string, TestQuestion>( GetQuestionTitle( newQuestion ), testQuestion );
            _questions.Add( item );
            _questionListBox.SelectedItem = item;
        }

        private void _okButton_Click( object sender, EventArgs e )
        {
            if (_questions.Count == 0)
            {
                MessageBox.ShowValidationError("Тест должен иметь как минимум одно задание.");
                return;
            }

            DialogResult = DialogResult.OK;
        }

        private void _cancelButton_Click( object sender, EventArgs e )
        {
            DialogResult = DialogResult.Cancel;
        }


        #region IDisposable Members

        void IDisposable.Dispose( )
        {
            if (_richTextBoxControl != null)
            {
                _richTextBoxControl.Dispose();
                _richTextBoxControl = null;
            }
        }

        #endregion

        private void _questionListBox_SelectedValueChanged( object sender, EventArgs e )
        {
            var listBox = sender as ListBox;
            UpdateAssignmentText(listBox);
        }

        private void UpdateAssignmentText(ListBox listBox)
        {
            var selectedItem = listBox.SelectedItem as Tuple<string, TestQuestion>;
            if (selectedItem == null)
            {
                _assigmentTextRichEdit.Text = "";
                return;
            }
            byte[] assigmnetText = null;
            using (new SessionScope(FlushAction.Never))
            {
                var currentQuestion = Question.Find((selectedItem.Item2.Question).Id);
                assigmnetText = currentQuestion.Content.RichContent;
            }
            _assigmentTextRichEdit.LoadRtf(new MemoryStream(assigmnetText));
        }
    }
}

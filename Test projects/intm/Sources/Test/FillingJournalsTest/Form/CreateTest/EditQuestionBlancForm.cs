﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Castle.ActiveRecord;
using DevExpress.CodeParser;
using DevExpress.XtraEditors;
using Eureca.Integrator.Extensions;
using Eureca.Professional.ActiveRecords;
using Expression = NHibernate.Criterion.Expression;
using MessageBox = Eureca.Integrator.Utility.Forms.MessageBox;

namespace Eureca.Professional.FillingFormsTest.Gui
{
    public partial class EditQuestionBlancForm : XtraForm
    {
        private const string createStandartBtnName = "Создать образец", editStandartBtnName = "Изменить образец";
        private const string nonSelectedBlancName = "Бланк не выбран";
        private const string newBlancFormName = "Добавление бланка", editBlancFormName = "Редактирование бланка";
        private FormFilling _currentFilling;
        public FormFilling CurrentFilling
        {
            get { return _currentFilling; }
        }

        private FormFilling SyncFilling
        {
            get
            {
                if (_currentFilling.Id <= 0)
                    return _currentFilling;
                if (SessionScope.Current != null)
                    return FormFilling.Find(_currentFilling.Id);
                else
                    using (new SessionScope(FlushAction.Never))
                    {
                        return FormFilling.Find( _currentFilling.Id );
                    }
            }               
        }

        public Journal BlancOwner
        {
            get;
            private set;
        }

        public EditQuestionBlancForm( FormFilling formFilling )
        {
            InitializeComponent( );

            _currentFilling = formFilling;
            _blancNameLabel.Text = nonSelectedBlancName;
            Text = formFilling.Form == null ? newBlancFormName : editBlancFormName;
            if (formFilling.Form != null)
            {
                LoadBlanc(formFilling.Form);
            }
            if (formFilling.FilledForm != null)
            {
                FillStandartTab( new MemoryStream( formFilling.FilledForm.Content ) ); 
            }
            else
            {
                _standartTabPage.PageEnabled = false;
            }
            _editStandartBtn.Text = editStandartBtnName;
           
            InitExpander();
        }

        void InitExpander()
        {
            //using (new SessionScope())
            //{
                var branch = JournalBranch.FindAll(Expression.Eq("BlancForm", _currentFilling.Form)).FirstOrDefault();
                Journal interestedJournal = null;
                if (branch != null)
                    interestedJournal = branch.Journal;
                var journals = Journal.FindAll();
                var controlsList = new List<Tuple<string, Control,Action<Control>>>( journals.Length );
                int journalIndex = -1;
                for (int i=0; i < journals.Length ; i++)
                {
                    var journal = journals[i];
                    bool isCurrent = interestedJournal != null && interestedJournal.Id == journal.Id;
                    var treeViewEdit = new EditJournalTreeControl(){ Journal = journal,Readonly = true };
                    treeViewEdit.ExpandAll();
                    if (isCurrent)
                    {
                        journalIndex = i;
                        treeViewEdit.OpenNode(branch);
                        BlancOwner = journal;
                    }
                    treeViewEdit.SelectedBranchChanged += treeViewEdit_SelectedBranchChanged;
                    controlsList.Add( new Tuple<string, Control, Action<Control>>( journal.Name, treeViewEdit, ( c ) => ( c as EditJournalTreeControl ).ClearSelection( ) ) );
                }
                _treeExpander.MinControlViewSize = 300;
                _treeExpander.InitList( controlsList , journalIndex);
                splitContainerControl1.Panel1.AutoScrollMinSize = new Size( 50, _treeExpander.Size.Height - 2 );
            //}
        }

        void treeViewEdit_SelectedBranchChanged( object sender, JournalTreeSelectionChangedArgs e )
        {
            if ((e.SelectedBranch == null || e.SelectedBranch.BlancForm == null))
            {
                DropBlanc();
                return;
            }
            var blancForm = e.SelectedBranch.BlancForm;
            BlancOwner = e.OwnerJournal;
            if (blancForm != CurrentFilling.FilledForm)
            {
                if (_currentFilling.FilledForm != null)
                    DeleteStandart();
                LoadBlanc(blancForm);
            }
        }

        private void DropBlanc()
        {
            if (_currentFilling.FilledForm != null)
            {
                if(!MessageBox.Confirm("Удалить текущий образец?"))
                    return;
                DeleteStandart();
            }

            _currentFilling.Form = null;
            _blancNameLabel.Text = nonSelectedBlancName;
            _blancRichEdit.RtfText = "";
        }

        private void _deleteStandartBtn_Click( object sender, EventArgs e )
        {
            DeleteStandart();
        }

        private void _editStandartBtn_Click( object sender, EventArgs e )
        {
            if(_currentFilling.Form == null)
                return;
            if (_currentFilling.FilledForm == null)
            {
                FillStandartTab(null);
                (sender as SimpleButton).Name = editStandartBtnName ;
                _tabControl.SelectedTabPage = _standartTabPage;
            }
        }

        private void LoadBlanc(RtfForm blanc)
        {
            _blancNameLabel.Text = blanc.Name;
            _currentFilling.Form = blanc;
            _blancRichEdit.LoadRtf( new MemoryStream(blanc.Content));
            _editStandartBtn.Enabled = true;
        }

        void FillStandartTab(MemoryStream stream = null)
        {
            _standartTabPage.PageEnabled = true;
            if(stream != null)
            {
                _standartRichEdit.LoadRtf(stream);
            }
            else
            {
                _currentFilling = SyncFilling;
                var buffer = _blancRichEdit.SaveRtf().GetBuffer();
                _standartRichEdit.LoadRtf( new MemoryStream(buffer));
                var filledForm = new RtfForm( buffer, FormType.Standart );
                filledForm.CreateAndFlush();
                _currentFilling.FilledForm = filledForm;
               // _currentFilling.UpdateAndFlush();
            }
            _deleteStandartBtn.Enabled = true;
        }

        void DeleteStandart()
        {
            _standartRichEdit.RtfText = "";
            if (_currentFilling.FilledForm != null)
            {
                //_currentFilling = SyncFilling;
                var orpanForm = _currentFilling.FilledForm;
                _currentFilling.FilledForm = null;
                //_currentFilling.UpdateAndFlush();
                orpanForm.Delete( );
            }
            _deleteStandartBtn.Enabled = false;
             _tabControl.SelectedTabPage  = _blancTabPage;
            _standartTabPage.PageEnabled = false;
        }


        private void _okBtn_Click( object sender, EventArgs e )
        {
            if (_currentFilling.Form == null)
            {
                MessageBox.ShowValidationError("Необходимо выбрать бланк.");
                return;
            }
            //_currentFilling = SyncFilling;
            if (_currentFilling.FilledForm != null)
            {
                _currentFilling.FilledForm.Content = _standartRichEdit.SaveRtf().GetBuffer();
                _currentFilling.FilledForm.SaveAndFlush();
            }
            if(_currentFilling.Id <=0)
                _currentFilling.CreateAndFlush();
            else
                _currentFilling.UpdateAndFlush( );
            DialogResult = DialogResult.OK;
        }

        private void _cancelBtn_Click( object sender, EventArgs e )
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Castle.ActiveRecord;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using Eureca.Integrator.Extensions;
using Eureca.Professional.ActiveRecords;
using Expression = NHibernate.Criterion.Expression;
using MessageBox = Eureca.Integrator.Utility.Forms.MessageBox;

namespace Eureca.Professional.FillingFormsTest.Gui
{
    public partial class EditAssigmentForm : XtraForm
    {
        private Question _question;
        readonly BindingList<FormFilling> _questionFormsList = new BindingList<FormFilling>();
        readonly BindingList<QuestionJournals>  _journalViewsList = new BindingList<QuestionJournals>();
        readonly List<Tuple<Journal, List<RtfForm>>> _refWatcher = new List<Tuple<Journal, List<RtfForm>>>( );
        private Question SyncQuestion
        {
            get
            {
                if(_question.Id>0)
                    return Question.Find(Question.Id);
                return _question;
            }
        }

        public Question Question
        {
            get { return _question; }
        }

        public EditAssigmentForm( Question question , bool isNew = false )
        {
            if(question == null)
                throw new ArgumentNullException("question");

            _question = question;
            InitializeComponent( );

            if(!isNew)
            {
                using (new SessionScope())
                {
                    _question = Question.Find(Question.Id);
                    foreach (var formFilling in Question.Standarts)
                    {
                        _questionFormsList.Add(formFilling);
                    }
                    foreach (var questionJournals in QuestionJournals.FindAll(Expression.Eq("Question", Question)))
                    {
                        _journalViewsList.Add(questionJournals);
                    }
                    _assignmentsRichEdit.LoadRtf(new MemoryStream(_question.Content.RichContent));
                }

                FillRefWatcher();
            }

            _journalViewsGrid.DataSource = _journalViewsList;
            _blancsGrid.DataSource = _questionFormsList;

            //_journalViewsGrid.EmbeddedNavigator.ButtonClick += _journalView_EmbeddedNavigator_ButtonClick;
            _blancsGrid.EmbeddedNavigator.ButtonClick += _blancsGrid_EmbeddedNavigator_ButtonClick;
        }

        private void FillRefWatcher()
        {
            foreach (var journalView in _journalViewsList)
            {
                _refWatcher.Add( new Tuple<Journal, List<RtfForm>>( journalView.Journal, new List<RtfForm>( ) ) );
            }
            using (new SessionScope())
            {
                foreach (var formFilling in _questionFormsList)
                {
                    var form = formFilling.Form;
                    var branch = JournalBranch.FindOne(Expression.Eq("BlancForm",form));
                    if(branch == null)
                        throw new InvalidOperationException("Неизвестный бланк");
                    var journal = branch.Journal;
                    var tuple = _refWatcher.Where(item => item.Item1.Id == journal.Id);
                    if (tuple.Any())
                    {
                        tuple.First().Item2.Add(form);
                    }
                    else
                    {
                        _refWatcher.Add(new Tuple<Journal,List<RtfForm>>(journal,new List<RtfForm>(new []{form})));
                    }
                }    
            }
        }

        private void RemoveJournalViewFromWatcher( RtfForm blanc)
        {
            Journal condemned = null;
            foreach (var journal in _refWatcher)
            {
                var forms = journal.Item2;
                var seekingForm = blanc;
                int formIndex = -1;
                for (int i = 0; i < forms.Count; i++)
                {
                    if (forms[i].Id != seekingForm.Id) continue;
                    formIndex = i;
                    break;
                }
                if (formIndex < 0) continue;
                forms.RemoveAt(formIndex);
                if (forms.Count != 0) continue;
                condemned = journal.Item1;
                break;
            }
            if (condemned != null)
            {
                _refWatcher.RemoveAll(item=>item.Item1.Id == condemned.Id);
                var unnecessaryViews = _journalViewsList.Where(view => view.Journal.Id == condemned.Id).ToArray();
                for(int i=0;i<unnecessaryViews.Length ; i++ )
                {
                    var unnecessaryView = unnecessaryViews[i];
                    _journalViewsList.Remove(unnecessaryView);
                    QuestionJournals.Find( unnecessaryView.Id).DeleteAndFlush( );
                }
            }
        }

        private void AddJournalViewToWathcer(RtfForm blanc , Journal journal)
        {
            var tuple = _refWatcher.Where( item => item.Item1.Id == journal.Id );
            if (tuple.Any( ))
            {
                tuple.First( ).Item2.Add( blanc );
            }
            else
            {
                _refWatcher.Add( new Tuple<Journal, List<RtfForm>>( journal, new List<RtfForm>( new[] { blanc } ) ) );
                
                JournalView defView = null;
                using (new SessionScope (FlushAction.Never))
                {
                    journal = Journal.Find(journal.Id);
                    defView = journal.JournalViews.Single(view => view.IsDefault);
                }
                var questionJournals = new QuestionJournals(Question, defView, journal);
                questionJournals.SaveAndFlush();
                _journalViewsList.Add(questionJournals);
            }
        }

        private void _blancsGrid_EmbeddedNavigator_ButtonClick( object sender, NavigatorButtonClickEventArgs e )
        {
            //int index = -1;
            var selectedRow = (FormFilling)_blancsView.GetSelectedItem();
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Append:
                    EditFormFilling();
                    e.Handled = true;
                    break;
                case NavigatorButtonType.Edit:
                    if (selectedRow != null)
                    {
                        EditFormFilling(selectedRow);
                    }
                    e.Handled = true;
                    break;
                case NavigatorButtonType.Remove:
                    if (selectedRow != null)
                    {
                        e.Handled = true;
                        DeleteFormFilling( _blancsView.GetSelectedRows()[0], selectedRow );
                    }
                    break;
            }
        }

        private void DeleteFormFilling(int index, FormFilling selectedRow)
        {
            _questionFormsList.RemoveAt(index);
            using (new SessionScope())
            {
                _question = SyncQuestion;
                FormFilling syncFormFiling = null;
                syncFormFiling = FormFilling.Find(selectedRow.Id);
                _question.Standarts.Remove( syncFormFiling );
                _question.UpdateAndFlush( );
            }
            RemoveJournalViewFromWatcher( selectedRow.Form );
            
        }


        void EditFormFilling(FormFilling formFilling = null)
        {
            bool isNew = formFilling == null;
            var oldform = isNew ? null : formFilling.Form;
            EditQuestionBlancForm editQuestionBlancForm = null;
            bool res = false;
            using (var tran = new TransactionScope())
            {

                if(isNew)
                    formFilling = new FormFilling();
                editQuestionBlancForm = new EditQuestionBlancForm( formFilling );
                var dialogResult = editQuestionBlancForm.ShowDialog( );
                if (dialogResult == DialogResult.OK)
                {
                    formFilling = editQuestionBlancForm.CurrentFilling;
                    if (_questionFormsList.Any(
                        filling => (isNew || filling.Form.Id != oldform.Id) && filling.Form.Id == formFilling.Form.Id))
                    {
                        MessageBox.ShowValidationError("Такой бланк уже добавлен. Операция будет отменена.");
                        tran.VoteRollBack();
                        return;
                    }
                }
                else
                {
                    tran.VoteRollBack( );
                    return;
                }
            }
            formFilling = editQuestionBlancForm.CurrentFilling;
            if(isNew)
            {
                _question = SyncQuestion;
                _question.Standarts.Add( formFilling );
                _question.SaveAndFlush( );
                _questionFormsList.Add( formFilling );
                AddJournalViewToWathcer(formFilling.Form,editQuestionBlancForm.BlancOwner);
            }
            else if(formFilling.Form != oldform)
            {
                RemoveJournalViewFromWatcher(oldform);
                AddJournalViewToWathcer( formFilling.Form, editQuestionBlancForm.BlancOwner );
            }
        }

        private void _okBtn_Click( object sender, EventArgs e )
        {
            if (ValidateAssignment()) return;

            using (new SessionScope(FlushAction.Never))
            {
                _question = SyncQuestion;
                if (_question.Content == null)
                {
                    _question.Content = new Content(_assignmentsRichEdit.SaveRtf().GetBuffer());
                    _question.Content.CreateAndFlush();
                }
                else
                {
                    var questionContent = Content.Find(_question.Content.Id);
                    questionContent.RichContent = _assignmentsRichEdit.SaveRtf().GetBuffer();
                    _question.Content.SaveAndFlush( );

                }
                _question.SaveAndFlush();
            }
            DialogResult = DialogResult.OK;
        }

        private bool ValidateAssignment()
        {
            StringBuilder errors = new StringBuilder();
            if (String.IsNullOrEmpty(_assignmentsRichEdit.Text))
                errors.AppendLine("Задание не может быть пустым.");

            if (_questionFormsList.Count == 0)
                errors.AppendLine( "Необходимо добавить хотя бы один бланк." );

            var assignmentRtfStream = _assignmentsRichEdit.SaveRtf();
            if (assignmentRtfStream.Length > Constants.ContentMaxSize)
            {
                errors.AppendLine( "Размер Вашего задания составляет " + assignmentRtfStream.Length / 1024 +
                                         " Кбайт. \nУменьшите размер текста и изображений до " +
                                         Constants.ContentMaxSize / 1024 + " Кбайт." );
            }

            if (errors.Length > 0)
            {
                errors.Insert(0, "Задание содержит следующие ошибки:\n");
                MessageBox.ShowValidationError(errors.ToString());
                return true;
            }

            

            return false;
        }


        private void _blancsView_DoubleClick( object sender, EventArgs e )
        {
             Point pt = _blancsView.GridControl.PointToClient(Control.MousePosition);


             GridHitInfo info = _blancsView.CalcHitInfo( pt );

            if (info.InRow || info.InRowCell)
            {
                var clickedRow = _blancsView.GetRow(info.RowHandle) as FormFilling;

                if (clickedRow != null)
                {
                    var index = _questionFormsList.IndexOf(clickedRow);
                    
                    EditFormFilling(clickedRow);

                    _questionFormsList.RemoveAt( index );
                    using (new SessionScope(FlushAction.Never))
                    {
                        _questionFormsList.Insert(index,FormFilling.Find(clickedRow.Id));
                    }

                }
            }
        }
    }
}

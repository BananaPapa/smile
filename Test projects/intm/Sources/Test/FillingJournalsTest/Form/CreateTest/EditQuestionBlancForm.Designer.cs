﻿namespace Eureca.Professional.FillingFormsTest.Gui
{
    partial class EditQuestionBlancForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if (disposing && ( components != null ))
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this._treeExpander = new Eureca.Professional.FillingFormsTest.ExpanderList();
            this._tabControl = new DevExpress.XtraTab.XtraTabControl();
            this._standartTabPage = new DevExpress.XtraTab.XtraTabPage();
            this._standartRichEdit = new DevExpress.XtraRichEdit.RichEditControl();
            this._blancTabPage = new DevExpress.XtraTab.XtraTabPage();
            this._blancRichEdit = new DevExpress.XtraRichEdit.RichEditControl();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this._blancNameLabel = new DevExpress.XtraEditors.LabelControl();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this._editStandartBtn = new DevExpress.XtraEditors.SimpleButton();
            this._deleteStandartBtn = new DevExpress.XtraEditors.SimpleButton();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._tabControl)).BeginInit();
            this._tabControl.SuspendLayout();
            this._standartTabPage.SuspendLayout();
            this._blancTabPage.SuspendLayout();
            this.flowLayoutPanel3.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.Controls.Add(this.simpleButton2);
            this.flowLayoutPanel1.Controls.Add(this.simpleButton3);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 591);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1014, 29);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(936, 3);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 0;
            this.simpleButton2.Text = "Ок";
            this.simpleButton2.Click += new System.EventHandler(this._okBtn_Click);
            // 
            // simpleButton3
            // 
            this.simpleButton3.Location = new System.Drawing.Point(855, 3);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(75, 23);
            this.simpleButton3.TabIndex = 1;
            this.simpleButton3.Text = "Отмена";
            this.simpleButton3.Click += new System.EventHandler(this._cancelBtn_Click);
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.AutoScroll = true;
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.splitContainerControl1.Panel1.Controls.Add(this._treeExpander);
            this.splitContainerControl1.Panel1.ShowCaption = true;
            this.splitContainerControl1.Panel1.Text = "Журналы";
            this.splitContainerControl1.Panel2.Controls.Add(this._tabControl);
            this.splitContainerControl1.Panel2.Controls.Add(this.flowLayoutPanel3);
            this.splitContainerControl1.Panel2.Controls.Add(this.flowLayoutPanel2);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1014, 591);
            this.splitContainerControl1.SplitterPosition = 207;
            this.splitContainerControl1.TabIndex = 1;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // _treeExpander
            // 
            this._treeExpander.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._treeExpander.Location = new System.Drawing.Point(0, 1);
            this._treeExpander.MinControlViewSize = 150;
            this._treeExpander.Name = "_treeExpander";
            this._treeExpander.Size = new System.Drawing.Size(204, 567);
            this._treeExpander.TabIndex = 0;
            // 
            // _tabControl
            // 
            this._tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tabControl.Location = new System.Drawing.Point(0, 22);
            this._tabControl.Name = "_tabControl";
            this._tabControl.SelectedTabPage = this._standartTabPage;
            this._tabControl.Size = new System.Drawing.Size(801, 540);
            this._tabControl.TabIndex = 3;
            this._tabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this._blancTabPage,
            this._standartTabPage});
            // 
            // _standartTabPage
            // 
            this._standartTabPage.Controls.Add(this._standartRichEdit);
            this._standartTabPage.Name = "_standartTabPage";
            this._standartTabPage.Size = new System.Drawing.Size(796, 514);
            this._standartTabPage.Text = "Образец";
            // 
            // _standartRichEdit
            // 
            this._standartRichEdit.Dock = System.Windows.Forms.DockStyle.Fill;
            this._standartRichEdit.Location = new System.Drawing.Point(0, 0);
            this._standartRichEdit.Name = "_standartRichEdit";
            this._standartRichEdit.Options.Fields.UseCurrentCultureDateTimeFormat = false;
            this._standartRichEdit.Options.HorizontalRuler.Visibility = DevExpress.XtraRichEdit.RichEditRulerVisibility.Hidden;
            this._standartRichEdit.Options.MailMerge.KeepLastParagraph = false;
            this._standartRichEdit.Options.VerticalRuler.Visibility = DevExpress.XtraRichEdit.RichEditRulerVisibility.Hidden;
            this._standartRichEdit.Size = new System.Drawing.Size(796, 514);
            this._standartRichEdit.TabIndex = 0;
            // 
            // _blancTabPage
            // 
            this._blancTabPage.Controls.Add(this._blancRichEdit);
            this._blancTabPage.Name = "_blancTabPage";
            this._blancTabPage.Size = new System.Drawing.Size(796, 512);
            this._blancTabPage.Text = "Бланк";
            // 
            // _blancRichEdit
            // 
            this._blancRichEdit.Dock = System.Windows.Forms.DockStyle.Fill;
            this._blancRichEdit.Location = new System.Drawing.Point(0, 0);
            this._blancRichEdit.Name = "_blancRichEdit";
            this._blancRichEdit.Options.Fields.UseCurrentCultureDateTimeFormat = false;
            this._blancRichEdit.Options.HorizontalRuler.Visibility = DevExpress.XtraRichEdit.RichEditRulerVisibility.Hidden;
            this._blancRichEdit.Options.MailMerge.KeepLastParagraph = false;
            this._blancRichEdit.Options.VerticalRuler.Visibility = DevExpress.XtraRichEdit.RichEditRulerVisibility.Hidden;
            this._blancRichEdit.ReadOnly = true;
            this._blancRichEdit.Size = new System.Drawing.Size(796, 512);
            this._blancRichEdit.TabIndex = 0;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.AutoSize = true;
            this.flowLayoutPanel3.Controls.Add(this._blancNameLabel);
            this.flowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(801, 22);
            this.flowLayoutPanel3.TabIndex = 2;
            // 
            // _blancNameLabel
            // 
            this._blancNameLabel.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this._blancNameLabel.Location = new System.Drawing.Point(20, 3);
            this._blancNameLabel.Margin = new System.Windows.Forms.Padding(20, 3, 3, 3);
            this._blancNameLabel.Name = "_blancNameLabel";
            this._blancNameLabel.Size = new System.Drawing.Size(0, 16);
            this._blancNameLabel.TabIndex = 0;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.AutoSize = true;
            this.flowLayoutPanel2.Controls.Add(this._editStandartBtn);
            this.flowLayoutPanel2.Controls.Add(this._deleteStandartBtn);
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.flowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(0, 562);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(801, 29);
            this.flowLayoutPanel2.TabIndex = 1;
            // 
            // _editStandartBtn
            // 
            this._editStandartBtn.Enabled = false;
            this._editStandartBtn.Location = new System.Drawing.Point(674, 3);
            this._editStandartBtn.Name = "_editStandartBtn";
            this._editStandartBtn.Size = new System.Drawing.Size(124, 23);
            this._editStandartBtn.TabIndex = 0;
            this._editStandartBtn.Click += new System.EventHandler(this._editStandartBtn_Click);
            // 
            // _deleteStandartBtn
            // 
            this._deleteStandartBtn.Enabled = false;
            this._deleteStandartBtn.Location = new System.Drawing.Point(550, 3);
            this._deleteStandartBtn.Name = "_deleteStandartBtn";
            this._deleteStandartBtn.Size = new System.Drawing.Size(118, 23);
            this._deleteStandartBtn.TabIndex = 1;
            this._deleteStandartBtn.Text = "Удалить образец";
            this._deleteStandartBtn.Click += new System.EventHandler(this._deleteStandartBtn_Click);
            // 
            // EditQuestionBlancForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1014, 620);
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Name = "EditQuestionBlancForm";
            this.ShowIcon = false;
            this.Text = "Добавление бланка";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._tabControl)).EndInit();
            this._tabControl.ResumeLayout(false);
            this._standartTabPage.ResumeLayout(false);
            this._blancTabPage.ResumeLayout(false);
            this.flowLayoutPanel3.ResumeLayout(false);
            this.flowLayoutPanel3.PerformLayout();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private ExpanderList _treeExpander;
        private DevExpress.XtraTab.XtraTabControl _tabControl;
        private DevExpress.XtraTab.XtraTabPage _standartTabPage;
        private DevExpress.XtraRichEdit.RichEditControl _standartRichEdit;
        private DevExpress.XtraTab.XtraTabPage _blancTabPage;
        private DevExpress.XtraRichEdit.RichEditControl _blancRichEdit;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private DevExpress.XtraEditors.LabelControl _blancNameLabel;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private DevExpress.XtraEditors.SimpleButton _editStandartBtn;
        private DevExpress.XtraEditors.SimpleButton _deleteStandartBtn;

    }
}
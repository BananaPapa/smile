﻿namespace Eureca.Professional.FillingFormsTest.Gui
{
    partial class FormFillingTestAssigmentsListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if (disposing && ( components != null ))
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this._questionListBox = new System.Windows.Forms.ListBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this._addQuerstionBtn = new DevExpress.XtraEditors.SimpleButton();
            this._changeQuestionBtn = new DevExpress.XtraEditors.SimpleButton();
            this._deleteQuestionBtn = new DevExpress.XtraEditors.SimpleButton();
            this._assigmentTextRichEdit = new DevExpress.XtraRichEdit.RichEditControl();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this._okButton = new DevExpress.XtraEditors.SimpleButton();
            this._cancelButton = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this._questionListBox);
            this.splitContainerControl1.Panel1.Controls.Add(this.tableLayoutPanel1);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this._assigmentTextRichEdit);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1090, 746);
            this.splitContainerControl1.SplitterPosition = 290;
            this.splitContainerControl1.TabIndex = 0;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // _questionListBox
            // 
            this._questionListBox.DisplayMember = "Item1";
            this._questionListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this._questionListBox.FormattingEnabled = true;
            this._questionListBox.Location = new System.Drawing.Point(0, 0);
            this._questionListBox.Name = "_questionListBox";
            this._questionListBox.Size = new System.Drawing.Size(290, 717);
            this._questionListBox.TabIndex = 1;
            this._questionListBox.SelectedValueChanged += new System.EventHandler(this._questionListBox_SelectedValueChanged);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Controls.Add(this._addQuerstionBtn, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this._changeQuestionBtn, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this._deleteQuestionBtn, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 717);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(290, 29);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // _addQuerstionBtn
            // 
            this._addQuerstionBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this._addQuerstionBtn.Location = new System.Drawing.Point(195, 3);
            this._addQuerstionBtn.Name = "_addQuerstionBtn";
            this._addQuerstionBtn.Size = new System.Drawing.Size(92, 23);
            this._addQuerstionBtn.TabIndex = 0;
            this._addQuerstionBtn.Text = "Добавить";
            this._addQuerstionBtn.Click += new System.EventHandler(this._addQuerstionBtn_Click);
            // 
            // _changeQuestionBtn
            // 
            this._changeQuestionBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this._changeQuestionBtn.Location = new System.Drawing.Point(99, 3);
            this._changeQuestionBtn.Name = "_changeQuestionBtn";
            this._changeQuestionBtn.Size = new System.Drawing.Size(90, 23);
            this._changeQuestionBtn.TabIndex = 1;
            this._changeQuestionBtn.Text = "Изменить";
            this._changeQuestionBtn.Click += new System.EventHandler(this._changeQuestionBtn_Click);
            // 
            // _deleteQuestionBtn
            // 
            this._deleteQuestionBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this._deleteQuestionBtn.Location = new System.Drawing.Point(3, 3);
            this._deleteQuestionBtn.Name = "_deleteQuestionBtn";
            this._deleteQuestionBtn.Size = new System.Drawing.Size(90, 23);
            this._deleteQuestionBtn.TabIndex = 2;
            this._deleteQuestionBtn.Text = "Удалить";
            this._deleteQuestionBtn.Click += new System.EventHandler(this._deleteQuestionBtn_Click);
            // 
            // _assigmentTextRichEdit
            // 
            this._assigmentTextRichEdit.Dock = System.Windows.Forms.DockStyle.Fill;
            this._assigmentTextRichEdit.Location = new System.Drawing.Point(0, 0);
            this._assigmentTextRichEdit.Name = "_assigmentTextRichEdit";
            this._assigmentTextRichEdit.Options.Fields.UseCurrentCultureDateTimeFormat = false;
            this._assigmentTextRichEdit.Options.HorizontalRuler.Visibility = DevExpress.XtraRichEdit.RichEditRulerVisibility.Hidden;
            this._assigmentTextRichEdit.Options.MailMerge.KeepLastParagraph = false;
            this._assigmentTextRichEdit.Options.VerticalRuler.Visibility = DevExpress.XtraRichEdit.RichEditRulerVisibility.Hidden;
            this._assigmentTextRichEdit.ReadOnly = true;
            this._assigmentTextRichEdit.Size = new System.Drawing.Size(794, 746);
            this._assigmentTextRichEdit.TabIndex = 0;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.Controls.Add(this._okButton);
            this.flowLayoutPanel1.Controls.Add(this._cancelButton);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 746);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1090, 29);
            this.flowLayoutPanel1.TabIndex = 1;
            // 
            // _okButton
            // 
            this._okButton.Location = new System.Drawing.Point(1012, 3);
            this._okButton.Name = "_okButton";
            this._okButton.Size = new System.Drawing.Size(75, 23);
            this._okButton.TabIndex = 0;
            this._okButton.Text = "Принять";
            this._okButton.Click += new System.EventHandler(this._okButton_Click);
            // 
            // _cancelButton
            // 
            this._cancelButton.Location = new System.Drawing.Point(931, 3);
            this._cancelButton.Name = "_cancelButton";
            this._cancelButton.Size = new System.Drawing.Size(75, 23);
            this._cancelButton.TabIndex = 1;
            this._cancelButton.Text = "Отмена";
            this._cancelButton.Visible = false;
            this._cancelButton.Click += new System.EventHandler(this._cancelButton_Click);
            // 
            // FormFillingTestAssigmentsListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1090, 775);
            this.ControlBox = false;
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Name = "FormFillingTestAssigmentsListForm";
            this.ShowIcon = false;
            this.Text = "FormFillingTestAssigmentsListForm";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton _okButton;
        private DevExpress.XtraEditors.SimpleButton _cancelButton;
        private System.Windows.Forms.ListBox _questionListBox;
        private DevExpress.XtraRichEdit.RichEditControl _assigmentTextRichEdit;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton _addQuerstionBtn;
        private DevExpress.XtraEditors.SimpleButton _changeQuestionBtn;
        private DevExpress.XtraEditors.SimpleButton _deleteQuestionBtn;
    }
}
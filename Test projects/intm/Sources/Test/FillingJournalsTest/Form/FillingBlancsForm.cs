﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Threading;
using System.Windows.Forms;
using Castle.ActiveRecord;
using DevExpress.CodeParser;
using DevExpress.XtraBars.Docking;
using DevExpress.XtraCharts.Wizard;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using Eureca.Integrator.Common;
using Eureca.Integrator.Extensions;
using Eureca.Professional.ActiveRecords;
using Eureca.Professional.FillingFormsTest.Gui;
using Eureca.Utility.Extensions;
using Eureca.Utility.Helper;
using log4net;
using NHibernate.Cfg.MappingSchema;
using NHibernate.Criterion;
using Expression = NHibernate.Criterion.Expression;
using MessageBox = Eureca.Integrator.Utility.Forms.MessageBox;

//using NHibernate.Linq;

namespace Eureca.Professional.FillingFormsTest.Gui
{
    public partial class FillingBlancsForm : PreloadMarqueeForm
    {
        private ILog Log = LogManager.GetLogger(typeof (FillingBlancsForm));
        private AssigmentDescriptor _currentAssigment;
        private readonly Test _test;
        private readonly BlancPassingTestStorageManager _blancManager;
        private readonly TestPassing _testPassing;

        public IEnumerable<FormFillingContainer> FilledForms
        {
            get
            {
                return _currentAssigment.FilledBlancs.Values.ToList();
            }            
        }

        public FillingBlancsForm(TestPassing passing)
        {
            StartMarquee("Подготовка формы прохождения теста...");
            using (new SessionScope( ))
            {
                _test = Test.Find( ( passing.Test.Id ) );
            }
            if (_test.Type.PlugInFileName != Constants.PlugInName)
                throw new InvalidOperationException( "Wrong test type." );
            _blancManager = new BlancPassingTestStorageManager(passing);
            _testPassing = passing;
            InitializeComponent();
            _currentAssigment = _blancManager.GetRandomAssigment();
            Init(_currentAssigment);
            _filledBlancs.EmbeddedNavigator.ButtonClick += EmbeddedNavigator_ButtonClick;
            CloseMarquee();
            Closing += FillingBlancsForm_Closing;
        }

        void FillingBlancsForm_Closing( object sender, System.ComponentModel.CancelEventArgs e )
        {
            if (DialogResult != DialogResult.OK && !MessageBox.Confirm( "Вы уверены, что хотите завершить тест?" ))
                e.Cancel = true;
        }

        void EmbeddedNavigator_ButtonClick( object sender, NavigatorButtonClickEventArgs e )
        {
            var navigatorButtonType = e.Button.ButtonType;  
            if (navigatorButtonType == NavigatorButtonType.Append)
            {
                e.Handled = true;
                return;
            }
            else if (navigatorButtonType == NavigatorButtonType.Edit)
            {
                e.Handled = true;
                var selectedItem = _filledBlancsView.GetSelectedItem() as FormFillingContainer;
                if (selectedItem == null) 
                    return;
                EditBlanc(selectedItem.BlancId);
            }
            else if (navigatorButtonType == NavigatorButtonType.Remove)
            {
                e.Handled = true;
                var selectedItem = _filledBlancsView.GetSelectedItem( ) as FormFillingContainer;
                if (selectedItem == null)
                    return;
                _currentAssigment.FilledBlancs.Remove(selectedItem.BlancId);
            }
            RefreshBlancGrid( );
        }

        private void SaveResult()
        {
            if(!_currentAssigment.IsSaved)
                _blancManager.SaveAssigmentPass(_testPassing, _currentAssigment);
        }

        private void Init(AssigmentDescriptor descriptor)
        {
            Text = _test.Title;
            _assigmentRichEdit.LoadRtf(descriptor.Assigment);
            _journalsListBox.DataSource = descriptor.JournalsViews;
            RefreshBlancGrid();
        }

        private void RefreshBlancGrid()
        {
            _filledBlancs.DataSource = FilledForms;
        }

        private void OnCommentLinkClick( object sender, ItemEventArgs<CommentLink> link )
        {
            int rtfFormIndex = link.Item.TargetBlancId;
            EditBlanc(rtfFormIndex);
        }

        private void EditBlanc(int rtfFormIndex)
        {
            using (new SessionScope())
            {
                RtfForm blanc = RtfForm.Find(rtfFormIndex);

                if (blanc == null)
                    throw new InvalidOperationException("Неизвестный бланк");

                var template = new MemoryStream(blanc.Content, 0, blanc.Content.Length, false, true);
                FormFillingContainer filledForm = null;
                if (_currentAssigment.FilledBlancs.ContainsKey(blanc.Id))
                    filledForm = _currentAssigment.FilledBlancs[blanc.Id];
                //Если этот бланк уже заполнялся то берём результат предыдущего заполнения
                MemoryStream sourceBlanc = filledForm != null
                    ? filledForm.Content
                    : template;

                var editBlanc = new MemoryStream(sourceBlanc.GetBuffer());

                if (BlancEditForm.Edit( ref editBlanc, template, new MemoryStream( _currentAssigment.Assigment.GetBuffer( ) ),owner:this ) == DialogResult.OK)
                {
                    _currentAssigment.FilledBlancs[blanc.Id] = new FormFillingContainer(blanc.Id,editBlanc, filledForm == null? blanc.FullPath : filledForm.BlancName);
                    RefreshBlancGrid( );
                }
            }
        }

        private void _endTestButton_Click( object sender, EventArgs e )
        {
            if (!MessageBox.Confirm( "Вы уверены, что хотите завершить тест?" ))
                return;
            SaveResult( );
            DialogResult = DialogResult.OK;
        }

        private void gridView1_DoubleClick( object sender, EventArgs e )
        {
             GridHitInfo info = _filledBlancsView.CalcHitInfo(_filledBlancsView.GridControl.PointToClient(Control.MousePosition));

            if (info.InRow || info.InRowCell)
            {
                var clickedItem = _filledBlancsView.GetRow(info.RowHandle) as FormFillingContainer;
                if(clickedItem!= null)
                    EditBlanc(clickedItem.BlancId);
            }
        }

        private void _journalsList_SelectedValueChanged( object sender, EventArgs e )
        {
            var listBoxControl = sender as ListBoxControl;
            var selectedValue = listBoxControl.SelectedValue as NamedFormWithCommentContainer;
            _journalViewRtfExtControl.Clear();
            if(selectedValue != null)
            {
                _journalViewRtfExtControl.Clear();
                _journalViewRtfExtControl.Rtf = new MemoryStream( selectedValue.Content.GetBuffer());
                _journalViewRtfExtControl.EnumerableComments = selectedValue.Comments;
            }
        }
    }

    public class BlancPassingTestStorageManager
    {
        private readonly List<Question> _questions = new List<Question>();
        private int _currentIndex;
        private readonly Test _test;
        private readonly TestPassing _testPassing;
        //private bool _noMoreQuestion;
        private Random rand = new Random((int)((long)Thread.CurrentThread.ManagedThreadId ^ DateTime.Now.Ticks ^ (long)DateTime.Now.Day));

        public int QuestionsCount
        {
            get { return _questions.Count; }
        }

        public int CurrentIndex
        {
            get { return _currentIndex; }
        }


        public BlancPassingTestStorageManager(TestPassing testPassing)
        {
            _testPassing = testPassing;
            using (new SessionScope())
            {
                _test = Test.Find(testPassing.Test.Id);
            }
            FillQuestions();
        }


        private void FillQuestions()
        {
            using (new SessionScope())
            {
                Test test = Test.Find(_test.Id);
                TestQuestion[] questions = ActiveRecordBase<TestQuestion>.FindAll(Restrictions.Eq("Test", test));
                _questions.AddRange( questions.Select(question => question.Question) );
            }
        }

        public AssigmentDescriptor GetNextAssigment(int currentIndex = -1)
        {
            if (currentIndex == -1)
                currentIndex = _currentIndex;
            //if (_noMoreQuestion)
            //    return null;
            AssigmentDescriptor assigmentDescriptor;
            MemoryStream assigment;
            var journalViews = new List<NamedFormWithCommentContainer>();
            using (new SessionScope())
            {
                var question = _questions[currentIndex];

                var questionJournalses = QuestionJournals.FindAll(Expression.Eq("Question", question));
                foreach (var questionJournals in questionJournalses)
                {
                    journalViews.Add(new NamedFormWithCommentContainer(questionJournals.Journal.Name,
                        questionJournals.ViewForm.ViewForm.ContentStream,
                        questionJournals.ViewForm.ViewForm.Comments.ToList()));
                }
                var rtfBytes = Question.Find(question.Id).Content.RichContent;
                assigment = new MemoryStream(rtfBytes,0,rtfBytes.Length,true,true);
            }

            assigmentDescriptor = new AssigmentDescriptor(assigment, journalViews);
            _currentIndex = currentIndex;

            return assigmentDescriptor;
        }

        public AssigmentDescriptor GetRandomAssigment()
        {
            int randIndex = rand.Next(QuestionsCount);
            return GetNextAssigment( randIndex );
        }



        public void SaveAssigmentPass( TestPassing testPassing,AssigmentDescriptor assigmentPass )
        {
            if (assigmentPass.IsSaved)
                return;

                List<FormFilling> completedForms = new List<FormFilling>( );
                if (assigmentPass.FilledBlancs.Any())
                {
                    foreach (var keyValuePair in assigmentPass.FilledBlancs)
                    {
                        var form = new RtfForm(keyValuePair.Value.Content.GetBuffer(), FormType.Answer);
                        form.Save();
                        var filledForm = new FormFilling(RtfForm.Find(keyValuePair.Key), form);
                        filledForm.Save();
                        completedForms.Add(filledForm);
                    }
                }
                else
                {
                    completedForms.Add(null);
                }
                foreach (var completedForm in completedForms)
                {
                    var res = new FillingFormsTestResult( _testPassing, _questions[_currentIndex], completedForm );
                    res.Save( );
                }

            assigmentPass.IsSaved = true;
        }
    }

    public class AssigmentDescriptor
    {
        private MemoryStream _assigment;
        private List<NamedFormWithCommentContainer> _journalsViews = new List<NamedFormWithCommentContainer>( );
        private readonly Dictionary<int, FormFillingContainer> _filledBlancs = new Dictionary<int, FormFillingContainer>( );

        public List<NamedFormWithCommentContainer> JournalsViews
        {
            get { return _journalsViews; }
            set { _journalsViews = value; }
        }

        public Dictionary<int, FormFillingContainer> FilledBlancs
        {
            get { return _filledBlancs; }
        }

        public AssigmentDescriptor( MemoryStream assigment, IEnumerable<NamedFormWithCommentContainer> journalsViews )
        {
            _assigment = assigment;
            if (journalsViews != null)
                _journalsViews.AddRange(journalsViews);
        }

        public bool IsSaved { get; set; }

        public MemoryStream Assigment
        {
            get { return _assigment; }
        }
    }

    public class NamedFormWithCommentContainer:BaseBlanc
    {

        private IEnumerable<IComment> _comments;

        public IEnumerable<IComment> Comments
        {
            get { return _comments; }
            set { _comments = value; }
        }

        public NamedFormWithCommentContainer(string formName, MemoryStream content, IEnumerable<IComment> comments):base(content,formName)
        {
            _comments = comments;
        }
    }

    public class FormFillingContainer : BaseBlanc
    {
        private int _blancId;

        public int BlancId
        {
            get { return _blancId; }
            set { _blancId = value; }
        }

        public FormFillingContainer(int blancId, MemoryStream content, string blancName):base(content,blancName)
        {
            _blancId = blancId;
        }
    }

    public class BaseBlanc
    {
        private MemoryStream _content;

        public MemoryStream Content
        {
            get { return _content; }
            set { _content = value; }
        }

        private string _blancName;

        public string BlancName
        {
            get { return _blancName; }
            set { _blancName = value; }
        }

        public BaseBlanc(MemoryStream content, string blancName)
        {
            _content = content;
            _blancName = blancName;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Castle.ActiveRecord;
using DevExpress.XtraBars.Objects;
using DevExpress.XtraEditors;
using Eureca.Professional.ActiveRecords;
using Eureca.Utility.Extensions;
using NHibernate.Criterion;

namespace Eureca.Professional.FillingFormsTest.Gui
{
    public partial class ShowResultForm : XtraForm
    {
        private TestPassing _testPassing;
        private Test _test;
        public ShowResultForm( TestPassing testPassing )
        {
            
            _testPassing = testPassing;
            using (new SessionScope())
            {
                _test = Test.Find(testPassing.Test.Id);
            }
            if(_test.Type.PlugInFileName != Constants.PlugInName)
                throw  new InvalidOperationException("Неверный тест");
            InitializeComponent( );
            Load += NonSupervisedResultForm_Load;
            if (testPassing.IsSupervise)
            {
                vStatistic statistic = null;
                using (new SessionScope())
                {
                    statistic = vStatistic.FindOne(Expression.Eq("TestPassingId", testPassing.Id));
                }
                Text = "Оценка: {0}.".FormatString(statistic.Valution == 5? "Зачёт" : "Незачёт") ;
            }
            else
            {
                Text = "Сравнение заполнения";
            }
        }

        void NonSupervisedResultForm_Load( object sender, EventArgs e )
        {
            _compareStandartsControl.Init(_testPassing,_test);
        }
    }
}

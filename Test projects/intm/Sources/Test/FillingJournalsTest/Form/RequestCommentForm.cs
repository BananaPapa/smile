﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.Data.PLinq.Helpers;
using DevExpress.XtraEditors;
using Eureca.Professional.ActiveRecords;
using Eureca.Utility.Extensions;

namespace Eureca.Professional.FillingFormsTest
{
    public partial class RequestCommentForm : XtraForm
    {
        public string Comment
        {
            get;
            set;
        }

        CommentType _commentType;

        public CommentType CommentType 
        {
            get
            {
                return _commentType;
            }
            set
            {
                _commentType = value;
            }
        }

        public RequestCommentForm( )
        {
            InitializeComponent( );

            Load += RequestCommentForm_Load;
        }

        public RequestCommentForm( IComment comment )
        {
            InitializeComponent( );
            Comment = comment.CommentText;
            CommentType = comment.CommentType;
            Load += RequestCommentForm_Load;
        }

        private void InitTextEdit()
        {
            _commentTextEdit.DataBindings.Add(new Binding("Text", this, "Comment"));
            _commentTextEdit.Focus();
        }

        void RequestCommentForm_Load( object sender, EventArgs e )
        {
            InitTextEdit( );
            InitComentTypeSelector( );
            KeyDown += RequestCommentForm_KeyDown;
            this.Icon = SystemIcons.Question;
        }

        private void InitComentTypeSelector()
        {
            _commentTypeSelector.DataSource = Enum.GetValues(typeof (CommentType)).Cast<CommentType>().Where(item =>
            {
                var attr = item.GetEnumAttribute<BrowsableAttribute>();
                if (attr != null && attr.Length > 0)
                    return attr[0].Browsable;
                return true;
            })
                .Cast<Enum>().Select(value => new
                {
                    (Attribute.GetCustomAttribute(value.GetType().GetField(value.ToString()), typeof (DescriptionAttribute)) as
                        DescriptionAttribute).Description,
                    value
                })
                .OrderBy(item => item.value)
                .ToList();
            _commentTypeSelector.DisplayMember = "Description";
            _commentTypeSelector.ValueMember = "Value";
            _commentTypeSelector.DataBindings.Add(new Binding("SelectedValue", this, "CommentType", false,
                DataSourceUpdateMode.OnPropertyChanged));
        }

        void RequestCommentForm_KeyDown( object sender, KeyEventArgs e )
        {
            if (e.KeyCode  == Keys.Enter )
                DialogResult = System.Windows.Forms.DialogResult.OK;
            else if (e.KeyCode == Keys.Escape)
                DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

    }
}

﻿namespace Eureca.Professional.FillingFormsTest.Gui
{
    partial class BlancTestResultPreviewForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if (disposing && ( components != null ))
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this._verifyCurrent = new DevExpress.XtraEditors.SimpleButton();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this._assigmentRichEditControl = new DevExpress.XtraRichEdit.RichEditControl();
            this._assigmentBlancGrid = new Eureca.Professional.FillingFormsTest.Gui.AssigmentBlancGrid();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.Controls.Add(this.simpleButton1);
            this.flowLayoutPanel1.Controls.Add(this.simpleButton2);
            this.flowLayoutPanel1.Controls.Add(this._verifyCurrent);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 565);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(686, 29);
            this.flowLayoutPanel1.TabIndex = 1;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(608, 3);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 0;
            this.simpleButton1.Text = "Зачёт";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(527, 3);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 1;
            this.simpleButton2.Text = "Незачёт";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // _verifyCurrent
            // 
            this._verifyCurrent.Enabled = false;
            this._verifyCurrent.Location = new System.Drawing.Point(402, 3);
            this._verifyCurrent.Name = "_verifyCurrent";
            this._verifyCurrent.Size = new System.Drawing.Size(119, 23);
            this._verifyCurrent.TabIndex = 2;
            this._verifyCurrent.Text = "Проверить текущий";
            this._verifyCurrent.Click += new System.EventHandler(this._verifyCurrent_Click);
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.splitContainerControl1.Panel1.Controls.Add(this._assigmentRichEditControl);
            this.splitContainerControl1.Panel1.ShowCaption = true;
            this.splitContainerControl1.Panel1.Text = "Задание ";
            this.splitContainerControl1.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.splitContainerControl1.Panel2.Controls.Add(this._assigmentBlancGrid);
            this.splitContainerControl1.Panel2.ShowCaption = true;
            this.splitContainerControl1.Panel2.Text = "Заполненные бланки";
            this.splitContainerControl1.Size = new System.Drawing.Size(686, 565);
            this.splitContainerControl1.SplitterPosition = 314;
            this.splitContainerControl1.TabIndex = 2;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // _assigmentRichEditControl
            // 
            this._assigmentRichEditControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._assigmentRichEditControl.Location = new System.Drawing.Point(0, 0);
            this._assigmentRichEditControl.LookAndFeel.UseDefaultLookAndFeel = false;
            this._assigmentRichEditControl.Name = "_assigmentRichEditControl";
            this._assigmentRichEditControl.Options.Fields.UseCurrentCultureDateTimeFormat = false;
            this._assigmentRichEditControl.Options.HorizontalRuler.Visibility = DevExpress.XtraRichEdit.RichEditRulerVisibility.Hidden;
            this._assigmentRichEditControl.Options.MailMerge.KeepLastParagraph = false;
            this._assigmentRichEditControl.Options.VerticalRuler.Visibility = DevExpress.XtraRichEdit.RichEditRulerVisibility.Hidden;
            this._assigmentRichEditControl.ReadOnly = true;
            this._assigmentRichEditControl.Size = new System.Drawing.Size(362, 542);
            this._assigmentRichEditControl.TabIndex = 0;
            // 
            // _assigmentBlancGrid
            // 
            this._assigmentBlancGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._assigmentBlancGrid.Location = new System.Drawing.Point(0, 0);
            this._assigmentBlancGrid.Name = "_assigmentBlancGrid";
            this._assigmentBlancGrid.Size = new System.Drawing.Size(310, 542);
            this._assigmentBlancGrid.TabIndex = 0;
            this._assigmentBlancGrid.OnBlancSelectionChanged += new System.EventHandler<Eureca.Professional.FillingFormsTest.Gui.OnBlancSelectionChangedEventArgs>(this._assigmentBlancGrid_OnBlancSelectionChanged);
            this._assigmentBlancGrid.OnBlancDoubleClicked += new System.EventHandler<Eureca.Professional.FillingFormsTest.Gui.OnBlancSelectionChangedEventArgs>(this._assigmentBlancGrid_OnBlancDoubleClicked);
            // 
            // BlancTestResultPreviewForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(686, 594);
            this.ControlBox = false;
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Name = "BlancTestResultPreviewForm";
            this.ShowIcon = false;
            this.Text = "BlancTestResultPreviewForm";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private Gui.AssigmentBlancGrid _assigmentBlancGrid;
        private DevExpress.XtraRichEdit.RichEditControl _assigmentRichEditControl;
        private DevExpress.XtraEditors.SimpleButton _verifyCurrent;

    }
}
﻿namespace Eureca.Professional.FillingFormsTest.Gui
{
    partial class BlancVerificationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if (disposing && ( components != null ))
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BlancVerificationForm));
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this._okButton = new DevExpress.XtraEditors.SimpleButton();
            this._saveButton = new DevExpress.XtraEditors.SimpleButton();
            this._cancelButton = new DevExpress.XtraEditors.SimpleButton();
            this._splitContainerControl = new DevExpress.XtraEditors.SplitContainerControl();
            this._callapseButton = new DevExpress.XtraEditors.CheckButton();
            this._standartRichEditControl = new DevExpress.XtraRichEdit.RichEditControl();
            this._rtfMarkUpTextControl = new Eureca.Professional.FillingFormsTest.RtfMarkUpTextControl();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._splitContainerControl)).BeginInit();
            this._splitContainerControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.Controls.Add(this._okButton);
            this.flowLayoutPanel1.Controls.Add(this._saveButton);
            this.flowLayoutPanel1.Controls.Add(this._cancelButton);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 695);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1451, 29);
            this.flowLayoutPanel1.TabIndex = 1;
            // 
            // _okButton
            // 
            this._okButton.Location = new System.Drawing.Point(1373, 3);
            this._okButton.Name = "_okButton";
            this._okButton.Size = new System.Drawing.Size(75, 23);
            this._okButton.TabIndex = 0;
            this._okButton.Text = "Ок";
            this._okButton.Visible = false;
            this._okButton.Click += new System.EventHandler(this._cancelButton_Click);
            // 
            // _saveButton
            // 
            this._saveButton.Location = new System.Drawing.Point(1292, 3);
            this._saveButton.Name = "_saveButton";
            this._saveButton.Size = new System.Drawing.Size(75, 23);
            this._saveButton.TabIndex = 0;
            this._saveButton.Text = "Сохранить";
            this._saveButton.Click += new System.EventHandler(this._saveButton_Click);
            // 
            // _cancelButton
            // 
            this._cancelButton.Location = new System.Drawing.Point(1211, 3);
            this._cancelButton.Name = "_cancelButton";
            this._cancelButton.Size = new System.Drawing.Size(75, 23);
            this._cancelButton.TabIndex = 1;
            this._cancelButton.Text = "Отмена";
            this._cancelButton.Click += new System.EventHandler(this._cancelButton_Click);
            // 
            // _splitContainerControl
            // 
            this._splitContainerControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._splitContainerControl.Location = new System.Drawing.Point(0, 0);
            this._splitContainerControl.Name = "_splitContainerControl";
            this._splitContainerControl.Panel1.Controls.Add(this._rtfMarkUpTextControl);
            this._splitContainerControl.Panel1.Controls.Add(this._callapseButton);
            this._splitContainerControl.Panel1.Text = "Panel1";
            this._splitContainerControl.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this._splitContainerControl.Panel2.Controls.Add(this._standartRichEditControl);
            this._splitContainerControl.Panel2.ShowCaption = true;
            this._splitContainerControl.Panel2.Text = "Образец";
            this._splitContainerControl.Size = new System.Drawing.Size(1451, 695);
            this._splitContainerControl.SplitterPosition = 1099;
            this._splitContainerControl.TabIndex = 2;
            this._splitContainerControl.Text = "_splitContainerControl";
            // 
            // _callapseButton
            // 
            this._callapseButton.Dock = System.Windows.Forms.DockStyle.Right;
            this._callapseButton.Image = global::Eureca.Professional.FillingFormsTest.Properties.Resources.right;
            this._callapseButton.Location = new System.Drawing.Point(1079, 0);
            this._callapseButton.Name = "_callapseButton";
            this._callapseButton.Size = new System.Drawing.Size(20, 695);
            this._callapseButton.TabIndex = 3;
            this._callapseButton.CheckedChanged += new System.EventHandler(this._callapseButton_CheckedChanged);
            // 
            // _standartRichEditControl
            // 
            this._standartRichEditControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._standartRichEditControl.Location = new System.Drawing.Point(0, 0);
            this._standartRichEditControl.Name = "_standartRichEditControl";
            this._standartRichEditControl.Options.Fields.UseCurrentCultureDateTimeFormat = false;
            this._standartRichEditControl.Options.HorizontalRuler.Visibility = DevExpress.XtraRichEdit.RichEditRulerVisibility.Hidden;
            this._standartRichEditControl.Options.MailMerge.KeepLastParagraph = false;
            this._standartRichEditControl.Options.VerticalRuler.Visibility = DevExpress.XtraRichEdit.RichEditRulerVisibility.Hidden;
            this._standartRichEditControl.ReadOnly = true;
            this._standartRichEditControl.Size = new System.Drawing.Size(342, 672);
            this._standartRichEditControl.TabIndex = 0;
            this._standartRichEditControl.TabStop = false;
            // 
            // _rtfMarkUpTextControl
            // 
            this._rtfMarkUpTextControl.CommentColor = System.Drawing.Color.Empty;
            this._rtfMarkUpTextControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._rtfMarkUpTextControl.ExtendedFormating = true;
            this._rtfMarkUpTextControl.HideCommentsList = true;
            this._rtfMarkUpTextControl.HidePanel = false;
            this._rtfMarkUpTextControl.Location = new System.Drawing.Point(0, 0);
            this._rtfMarkUpTextControl.LookAndFeel.SkinName = "Office 2010 Silver";
            this._rtfMarkUpTextControl.LookAndFeel.UseDefaultLookAndFeel = false;
            this._rtfMarkUpTextControl.MarkerColor = System.Drawing.Color.Empty;
            this._rtfMarkUpTextControl.Name = "_rtfMarkUpTextControl";
            this._rtfMarkUpTextControl.ReadOnly = false;
            this._rtfMarkUpTextControl.Rtf = ((System.IO.MemoryStream)(resources.GetObject("_rtfMarkUpTextControl.Rtf")));
            this._rtfMarkUpTextControl.Size = new System.Drawing.Size(1079, 695);
            this._rtfMarkUpTextControl.TabIndex = 4;
            // 
            // BlancVerificationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1451, 724);
            this.ControlBox = false;
            this.Controls.Add(this._splitContainerControl);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Name = "BlancVerificationForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Проверка бланка";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._splitContainerControl)).EndInit();
            this._splitContainerControl.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SplitContainerControl _splitContainerControl;
        private DevExpress.XtraEditors.SimpleButton _okButton;
        private DevExpress.XtraEditors.SimpleButton _saveButton;
        private DevExpress.XtraEditors.SimpleButton _cancelButton;
        private DevExpress.XtraRichEdit.RichEditControl _standartRichEditControl;
        private RtfMarkUpTextControl _rtfMarkUpTextControl;
        private DevExpress.XtraEditors.CheckButton _callapseButton;
    }
}
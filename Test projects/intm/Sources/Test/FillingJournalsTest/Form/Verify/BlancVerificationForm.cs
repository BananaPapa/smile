﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.CodeParser;
using DevExpress.XtraEditors;
using Eureca.Integrator.Common;
using Eureca.Integrator.Extensions;
using Eureca.Professional.ActiveRecords;
using Eureca.Professional.FillingFormsTest.Properties;
using Eureca.Utility.Helper;
using Comment = Eureca.Professional.ActiveRecords.Comment  ;

namespace Eureca.Professional.FillingFormsTest.Gui
{
    public partial class BlancVerificationForm : PreloadMarqueeForm
    {
        private const string formName = "Проверка бланка";
        private readonly MemoryStream _original ;

        public MemoryStream FilledBlanc
        {
            get
            {
                if(DialogResult == DialogResult.Cancel)
                    return _original;
                return _rtfMarkUpTextControl.Rtf;
            }
            set
            {
                _rtfMarkUpTextControl.Rtf = value; 
            }
        }

        public BindingListWithRemoveEvent<IComment> Comments 
        {
            get
            {
               return _rtfMarkUpTextControl.Comments;
            }
        }


        public BlancVerificationForm( MemoryStream blanc = null , MemoryStream standart = null , IEnumerable<Comment> comments = null , string blancName = null)
        {
            StartMarquee("Создание формы редактирования...");
            InitializeComponent( );
            Text = formName + (blancName == null ? "." : (": " + blancName));
            if (comments != null && comments.Any())
                _rtfMarkUpTextControl.EnumerableComments = comments;
            
            if(blanc!=null)
            {
                _rtfMarkUpTextControl.Rtf = new MemoryStream( blanc.GetBuffer( ) );
                _original = blanc;
            }
            else
            {
                _splitContainerControl.PanelVisibility = SplitPanelVisibility.Panel2;
                _splitContainerControl.Panel2.Text = "Пример";
                Text = "Бланк не был заполнен.";
            }
            
            _saveButton.Visible = blanc != null;
            _cancelButton.Visible = blanc != null;
            _okButton.Visible = blanc == null;
            
            if (standart != null)
                _standartRichEditControl.LoadRtf(standart);
            else
                _callapseButton.Checked = true;
            CloseMarquee( );
        }

        private void _callapseButton_CheckedChanged( object sender, EventArgs e )
        {
            CheckButton senderButton = (CheckButton)sender;
            if (senderButton.Checked)
            {
                senderButton.Image = Resources.left;
                senderButton.ToolTip = "Развернуть образец";
                _splitContainerControl.PanelVisibility = SplitPanelVisibility.Panel1;
            }
            else
            {
                senderButton.Image = Resources.right;
                senderButton.ToolTip = "Свернуть образец";
                _splitContainerControl.PanelVisibility = SplitPanelVisibility.Both;
            }
        }

        private void _cancelButton_Click( object sender, EventArgs e )
        {
            DialogResult = DialogResult.Cancel;
        }

        private void _saveButton_Click( object sender, EventArgs e )
        {
            DialogResult = DialogResult.OK;
        }

    }
}

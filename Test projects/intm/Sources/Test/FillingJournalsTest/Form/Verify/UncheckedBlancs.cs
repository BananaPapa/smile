﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using Eureca.Professional.ActiveRecords;

namespace Eureca.Professional.FillingFormsTest.Gui
{
    public partial class UncheckedBlancs : XtraForm
    {
        public UncheckedBlancs( )
        {
            if (!ProfessionalDataContext.IsInit)
                throw new InvalidOperationException( "Контекст БД профессионала не инициализирован. Необходимо вызвать Init." );
            InitializeComponent( );
            _uncheckedBlancList.DataSource = vUnverifiedAssignments.FindAll();
        }

        private void _uncheckedBlancView_DoubleClick( object sender, EventArgs e )
        {
            GridView view = (GridView)sender;

            Point pt = view.GridControl.PointToClient( Control.MousePosition );

            GridHitInfo info = view.CalcHitInfo( pt );

            if (info.InRow || info.InRowCell)
            {
                var row = (vUnverifiedAssignments)view.GetRow( info.RowHandle );

                VerifyTestPassing( row.TestPassingsId );
            }
        }

        private void VerifyTestPassing(int testPassingId)
        {
            var blancTestResult = new BlancTestResultPreviewForm(testPassingId);
            if (blancTestResult.ShowDialog() == DialogResult.OK)
            {
                _uncheckedBlancList.DataSource = vUnverifiedAssignments.FindAll();
                if (_uncheckedBlancList.MainView.RowCount == 0)
                    DialogResult = DialogResult.OK;
            }
        }

        private void _okButton_Click( object sender, EventArgs e )
        {
            DialogResult = DialogResult.OK;
        }

        private void _verifyCurrentButton_Click( object sender, EventArgs e )
        {
            GridView view = (GridView)_uncheckedBlancList.MainView;
            var rowHandles = view.GetSelectedRows( );
            if (rowHandles == null || rowHandles.Length == 0)
                return;
            var row = (vUnverifiedAssignments)view.GetRow( rowHandles[0] );
            if(row == null)
                return;
            VerifyTestPassing( row.TestPassingsId );
        }
    }
}

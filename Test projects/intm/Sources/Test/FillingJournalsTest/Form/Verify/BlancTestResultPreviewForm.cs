﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Castle.ActiveRecord;
using DevExpress.LookAndFeel;
using DevExpress.XtraEditors;
using DevExpress.XtraPrinting;
using Eureca.Integrator.Extensions;
using Eureca.Professional.ActiveRecords;
using Eureca.Utility.Extensions;
using Eureca.Utility.Helper;
using log4net;
using NHibernate.Criterion;

namespace Eureca.Professional.FillingFormsTest.Gui
{
    public partial class BlancTestResultPreviewForm : XtraForm , IDisposable
    {
        private ILog Log = LogManager.GetLogger(typeof (BlancTestResultPreviewForm));
        private readonly int _testPassingId;
        //private MemoryStream _assigment;
        
        public BlancTestResultPreviewForm( int TestPassingId )
        {
            _testPassingId = TestPassingId;
            InitializeComponent( );
            _assigmentBlancGrid.OnBlancDoubleClicked += _assigmentBlancGrid_OnBlancDoubleClicked;
            try
            {
                InitForm( _testPassingId );
            }
            catch (Exception exception)
            {
                Log.Error(exception);
                throw ;
            }
        }

        void _assigmentBlancGrid_OnBlancDoubleClicked( object sender, Gui.OnBlancSelectionChangedEventArgs e )
        {
            VerifyBlanc( e.BlancId);
        }

        private void VerifyBlanc( int blancId )
        {
            IEnumerable<Comment> comments = null;
            RtfForm filledBlanc = null;
            if (_assigmentBlancGrid.FilledBlancs.ContainsKey(blancId))
                using (new SessionScope())
                {
                    filledBlanc = _assigmentBlancGrid.FilledBlancs[blancId];
                    var filledBlancEntity = RtfForm.Find(filledBlanc.Id);
                    comments = filledBlancEntity.Comments.ToList();
                }
            var additionalForm = _assigmentBlancGrid.Standarts.ContainsKey(blancId) &&
                                 _assigmentBlancGrid.Standarts[blancId] != null
                ? _assigmentBlancGrid.Standarts[blancId].ContentStream
                : _assigmentBlancGrid.FormsMerge[blancId].ContentStream;
            var verificationForm = new BlancVerificationForm(filledBlanc == null ? null : filledBlanc.ContentStream, comments: comments,
                standart: additionalForm );
            verificationForm.LookAndFeel.UseDefaultLookAndFeel = false;
            verificationForm.LookAndFeel.Style = LookAndFeelStyle.Skin;
            verificationForm.LookAndFeel.SkinName = "DevExpress Style";
            if (verificationForm.ShowDialog( this ) == DialogResult.OK && filledBlanc != null)
                using (new SessionScope(FlushAction.Never))
                {
                    filledBlanc = FillingFormsTest.Helper.ApplyBlancChanges(filledBlanc.Id, verificationForm.FilledBlanc,
                        verificationForm.Comments);
                    filledBlanc.UpdateAndFlush();
                    SessionScope.Current.Flush();
                }
        }

        void _assigmentBlancGrid_OnBlancSelectionChanged( object sender, Gui.OnBlancSelectionChangedEventArgs e )
        {
            _verifyCurrent.Enabled = e.BlancId > 0;
        }

        public void InitForm(int testPassingId)
        {
            
            byte[] _assigmentBytes = null;
            using (new SessionScope())
            {
                _assigmentBlancGrid.LoadData( testPassingId );
                var testPassing = ActiveRecords.TestPassing.Find(testPassingId);
                var test = testPassing.Test;
                var user = testPassing.Probationer;
                Text = "Результат выполнения теста \'{0}\' обучаемым {1}".FormatString(test.Title, user);
                var testResults = FillingFormsTestResult.FindAll( Expression.Eq( "TestPassing", testPassing ) );
                if (testResults.Any())
                {
                    _assigmentBytes = testResults.First( ).Question.Content.RichContent;
                }
            }
            if (_assigmentBytes != null)
                _assigmentRichEditControl.LoadRtf(new MemoryStream(_assigmentBytes));
        }

        private void _verifyCurrent_Click( object sender, EventArgs e )
        {
            var selectedBlanc = _assigmentBlancGrid.SelectedRow;
            if (selectedBlanc != null && selectedBlanc.FillingType != BlancCompletnessType.Unknown)
                VerifyBlanc( selectedBlanc.Form.Id);
        }

        private void simpleButton1_Click( object sender, EventArgs e )
        {
            var testPassing = TestPassing.Find(_testPassingId);
            ProfessionalDataContext.UpdateStatistic(testPassing,true);
            DialogResult = DialogResult.OK;
        }

        private void simpleButton2_Click( object sender, EventArgs e )
        {
            var testPassing = TestPassing.Find( _testPassingId );
            ProfessionalDataContext.UpdateStatistic( testPassing, false );
            DialogResult = DialogResult.OK;
        }
    }
}

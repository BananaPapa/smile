﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.ActiveRecord;
using Eureca.Professional.ActiveRecords;
using Eureca.Professional.FillingFormsTest.Properties;

namespace Eureca.Professional.FillingFormsTest
{

    public class JournalBranchView : IJournalBranch
    {
        public static JournalBranchView Empty = new JournalBranchView( null );

        private static ConcurrentDictionary<int, JournalBranchView> _views = new ConcurrentDictionary<int, JournalBranchView>( );

        public bool IsExpanded { get; set; }


        private JournalBranch _model;

        private JournalTreeContext _context;

        public JournalTreeContext Context
        {
            get
            {
                if (_context == null)
                    _context = FindContext( );
                return _context;
            }
            set
            {
                _context = value;
            }
        }

        #region IJournalBranch Members

        public JournalBranch Model { get { return _model; } }

        public JournalBranch SyncModel 
        {
            get
            {
                if (_model.Id>0)
                    return JournalBranch.Find( _model.Id);
                return _model;
            }
        }


        public int Id
        {
            get { return _model.Id; }
        }

        public string Name
        {
            get
            {
                return _model.Name;
            }
            set
            {
                if (_model.Name == value || string.IsNullOrEmpty(value))
                    return;
                _model = SyncModel;
                _model.Name = value;
               
                if(_model.BlancForm !=null)
                {
                    var blancForm = RtfForm.Find( _model.BlancForm.Id );
                    blancForm.Name = value;
                    blancForm.Update( );
                }
                _model.Update();
                Context.NotifyNodeChanged( this );
            }
        }

        public Journal Journal
        {
            get { return _model.Journal; }
        }

        public RtfForm BlancForm
        {
            get
            {
                return _model.BlancForm;
            }
            set
            {
                _model = SyncModel;
                if (_model.BlancForm.Id == value.Id)
                    return;
                _model.BlancForm = value;
                _model.Update();
                Context.NotifyNodeChanged( this );
            }
        }

        public JournalBranch Parent
        {
            get
            {
                return _model.Parent;
            }
            set
            {
                _model = SyncModel;
                if (_model.Id == value.Id)
                    return;
                Context.NotifyNodeRemoved( this );
                _model.Parent = value;
                _model.Update();
                Context.NotifyNodeInserted(this);
            }
        }

        public JournalBranchView ParentView
        {
            get
            {
                var parent = Parent;
                if (parent == null)
                    return null;
                JournalBranchView view = null;
                if (_views.TryGetValue( parent.Id, out view ))
                    return view;
                else
                {
                    return CreateInstance(parent, _context);
                }
                //throw new InvalidOperationException( "failed to get value from view Dictionary" );
            }
            set
            {
                if (Parent.Id == value._model.Id)
                    return;
                Parent = value._model;
            }
        }

        public int Order
        {
            get
            {
                return _model.Order;
            }
            set
            {
                _model = SyncModel;
                if (_model.Order == value)
                    return;
                Context.NotifyNodeRemoved( this );
                _model.Order = value;
                _model.UpdateAndFlush();
                Context.NotifyNodeInserted( this );
            }
        }

        #endregion

        public void AddChild( JournalBranchView branch )
        {
            if (branch.Model == null)
            {
                throw new InvalidOperationException( "Uregistered branch" );
            }

            branch.ParentView = this;
        }

        JournalBranchView( JournalBranch model )
        {
            _model = model;
            if (model != null && !_views.ContainsKey( _model.Id ) && !_views.TryAdd( _model.Id, this ))
                throw new InvalidOperationException( "failed to add view in view Dictionary" );
        }

        public static JournalBranchView CreateInstance( JournalBranch model, JournalTreeContext context )
        {
            JournalBranchView viewModel = null;
            if (_views.ContainsKey( model.Id ))
                _views.TryGetValue( model.Id, out viewModel );
            else
                viewModel = new JournalBranchView( model ) { Context = context };
            return viewModel;
        }

        private JournalTreeContext FindContext( )
        {
            JournalBranchView node = this;
            while (node != null)
            {
                if (node.Context != null)
                    return node.Context;
                node = node.ParentView;
            }
            return null;
        }

        public override string ToString( )
        {
            JournalBranch par = _model;
            List<string> parts = new List<string>( );
            do
            {
                parts.Add( par.Name );
                par = par.Parent;
            } while (par != null);

            return _model.Name;
        }

        public Image Icon
        {
            get
            {
                return BlancForm == null ? Resources.folder_generic_closed : Resources.Form;
            }
        }
    }

}

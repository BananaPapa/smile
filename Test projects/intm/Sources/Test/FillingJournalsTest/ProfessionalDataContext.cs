﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.VisualStyles;
using Castle.ActiveRecord;
using Castle.ActiveRecord.Framework.Config;
using DevExpress.CodeParser;
using Eureca.Professional.ActiveRecords;
using Eureca.Utility.Extensions;
using Comment = Eureca.Professional.ActiveRecords.Comment;
using Event = Eureca.Professional.ActiveRecords.Event;
using Method = Eureca.Professional.ActiveRecords.Method;

namespace Eureca.Professional.FillingFormsTest
{
    public static class ProfessionalDataContext
    {
        private static string _connnectionString = null;

        public static bool IsInit
        { get; internal set; }

        public static bool InitActiveRecord(string professionalConnectionString)
        {
            if(ActiveRecordStarter.IsInitialized)
            {
                IsInit = true;
                return true;
            }
            try
            {
                var properties = new Dictionary<string, string>();
                properties.Add(NHibernate.Cfg.Environment.ConnectionDriver, "NHibernate.Driver.SqlClientDriver");
                properties.Add(NHibernate.Cfg.Environment.Dialect, "NHibernate.Dialect.MsSql2008Dialect");
                properties.Add(NHibernate.Cfg.Environment.ConnectionProvider,
                    "NHibernate.Connection.DriverConnectionProvider");
                properties.Add(NHibernate.Cfg.Environment.ConnectionString, professionalConnectionString);
                properties.Add(NHibernate.Cfg.Environment.ProxyFactoryFactoryClass,
                    "NHibernate.ByteCode.Castle.ProxyFactoryFactory, NHibernate.ByteCode.Castle");

                var source = new InPlaceConfigurationSource();

                source.Add(typeof (ActiveRecordBase), properties);
                //source.Debug = true;
                //source.
                
                ActiveRecordStarter.Initialize(source,
                    typeof (Test),
                    typeof (TestType),
                    typeof (Method),
                    typeof (Content),
                    typeof (Answer),
                    typeof (Question),
                    typeof (TestAnswer),
                    typeof (TestQuestion),
                    typeof (QuestionAnswer),
                    typeof (ControlQuestion),
                    typeof (MemorizationInfo),
                    typeof (Probationer),
                    typeof (Profile),
                    typeof (ProfilePassing),
                    typeof (RawPoint),
                    typeof (Scale),
                    typeof (Sten),
                    typeof (TestPassing),
                    typeof (TestResult),
                    typeof (TestType),
                    typeof (TimeInterval),
                    typeof (User),
                    typeof (EventType),
                    typeof (Event),
                    typeof (ArmApplication),
                    typeof (Report),
                    typeof (ReportField),
                    typeof (SystemProperties),
                    typeof (SuperviseAdresses),
                    typeof (Comment),
                    typeof (RtfForm),
                    typeof (FormFilling),
                    typeof (Journal),
                    typeof (JournalView),
                    typeof (JournalBranch),
                    typeof (FormFilling),
                    typeof (FillingFormsTestResult),
                    typeof (QuestionJournals),
                    typeof (vStatistic),
                    typeof (vUnverifiedAssignments));
                //проверка на инициализацию именно этой БД
                ActiveRecordBase<vUnverifiedAssignments>.FindAll();
                IsInit = true;
                _connnectionString = professionalConnectionString;
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static int UpdateStatistic(TestPassing tp, bool isPass, bool isVerified = true)
        {
            if (!IsInit)
                throw new Exception("Отсутствует подключение к базе, модуль ЗЖУ.");
            int res = 0; // isPass ? 0 : 0;
            int perc = 0; // isPass ? 0 : 0;
            int valution = isPass ? 5 : 0;
            string query =
                string.Format(("update statistic " +
                               "set [Result] = {0}, [Perc] = {1} , [Valuation] = {2}, [Verified] = {3} " +
                               "where TestPassingId = {4}"
                               ).FormatString(res, perc, valution,isVerified? 1 :0, tp.Id));

        // "(testpassingid,userid,useruniqueid,stime,etime, " +
                              //"issupervise,maxeff,timeduration,result,perc,valuation,testid,verified)" +

                              //"select testpassingid,userid,UserUniqueId,sTime,eTime, " +
                              //"IsSupervise,ISNULL(MaxEff,100),TimeDuration,Result,Perc,Valuation,testid,{1} from vrawStatistic " +
                              //"where testpassingid = {0}", tp.Id, isVerified ? 1 : 0 );
            SqlConnection sqlCon = new SqlConnection(_connnectionString);

            try
            {
                sqlCon.Open();
                var command = sqlCon.CreateCommand();
                command.CommandText = query;
                return command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                return 0;
            }
            finally
            {
                if (sqlCon != null && sqlCon.State != ConnectionState.Closed)
                {
                    sqlCon.Close();
                }
            }
            //DBExecutor.ExecNonQuery( query );
        }

    }
}

﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.VisualStyles;
using Aga.Controls.Tree;
using Castle.ActiveRecord;
using DevExpress.XtraSplashScreen;
using Eureca.Professional.ActiveRecords;
using NHibernate.Criterion;
using NHibernate.Hql.Ast.ANTLR;

namespace Eureca.Professional.FillingFormsTest
{
    public class JournalTreeContext : TreeModelBase
    {

        private readonly Journal _owner;
        private JournalBranchView _root;
        //private BindingList<JournalBranch> _table;

        //public BindingList<JournalBranch> Table
        //{
        //    get { return _table; }
        //}
        
        public JournalTreeContext(Journal journal)
        {
            try
            {
                _owner = journal;
                //_root = JournalBranchView.Empty;
                //using (new SessionScope( ))
                //{
                    var _original = JournalBranch.FindOne( Expression.IsNull( "Parent" ), Expression.Eq( "Journal", _owner ) );
                    if(_original != null)
                    {
                        _root = JournalBranchView.CreateInstance( _original , this );
                        _root.Context = this;
                    }
                //}

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public override System.Collections.IEnumerable GetChildren( TreePath treePath )
        {
            List<JournalBranchView> items = new List<JournalBranchView>( );

            if (treePath.IsEmpty( ))
            {
                items.Add( _root );
            }
            else
            {
                JournalBranch n = (treePath.LastNode as JournalBranchView).SyncModel;
                var journalBranchViews = JournalBranch.FindAll(Order.Asc("Order"),Expression.Eq("Parent",n)).Select(
                    item => 
                    {
                        return JournalBranchView.CreateInstance( item , this );
                    }
                    );  
                items.AddRange( journalBranchViews);
            }
            return items;
        }

        public override bool IsLeaf( TreePath treePath )
        {
            if (treePath.IsEmpty())
                return false;
            var branch = treePath.LastNode as JournalBranchView;
            if(treePath == null)
                throw new ArgumentException("Некоректный элемент дерева ");
            return branch == null || branch.BlancForm != null;
        }

        public void AddChild(JournalBranch parent, string name, MemoryStream content = null)
        {
            RtfForm newForm = null;
            if(content!=null)
                newForm= new RtfForm(content.GetBuffer(),FormType.Blanc);
            JournalBranch newBranch = new JournalBranch(name,_owner,parent,newForm);
            newBranch.CreateAndFlush();
            //Table.Add(newBranch);
        }

        public void NotifyNodeInserted(JournalBranchView newNode )
        {
            TreePath parentTreePath = GetPath(newNode.ParentView);
//            OnNodesInserted( new TreeModelEventArgs( parentTreePath, new[] { GetChildrenIndex( parentTreePath, newNode ) }, new[] { newNode } ) );
            OnNodesInserted( new TreeModelEventArgs( parentTreePath, new []{newNode.Order}, new[] { newNode } ) );
        }

        public void NotifyNodeRemoved(JournalBranchView node)
        {
            TreePath parentTreePath = GetPath( node.ParentView );
            OnNodesRemoved( new TreeModelEventArgs( parentTreePath,null, new[] { node } ) );
//            OnNodesRemoved( new TreeModelEventArgs( parentTreePath, new[] { GetChildrenIndex( parentTreePath, node ) }, new[] { node } ) );
        }

        public void NotifyNodeChanged(JournalBranchView node )
        {
            TreePath parentTreePath = GetPath( node.ParentView );
            OnNodesChanged( new TreeModelEventArgs( parentTreePath, null, new[] { node } ) );
            //OnNodesChanged( new TreeModelEventArgs( parentTreePath, new[] { GetChildrenIndex( parentTreePath, node ) }, new[] { node } ) );
        }

        int GetChildrenIndex(TreePath parentTreePath, JournalBranchView journalView)
        {
           return (new List<JournalBranchView>(GetChildren(parentTreePath).Cast<JournalBranchView>())).IndexOf(journalView);
        }

        public TreePath GetPath( JournalBranchView node )
        {
            if(node == null)
                return TreePath.Empty;
            if (node == _root)
                return new TreePath(_root);
            else
            {
                Stack<object> stack = new Stack<object>( );
                while (node != null)
                {
                    stack.Push( node );
                    node = node.ParentView;
                }
                return new TreePath( stack.ToArray( ) );
            }
        }
    }

}

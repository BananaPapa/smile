﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Castle.ActiveRecord;
using DevExpress.CodeParser;
using Eureca.Integrator.Extensions;
using Eureca.Professional.ActiveRecords;
using Eureca.Professional.FillingFormsTest.Gui;
using TestPlugins;
using Expression = NHibernate.Criterion.Expression;

namespace Eureca.Professional.FillingFormsTest
{

    public class JournalModuleManager : IJournalModuleManager
    {
        static List<Form> showedForms = new List<Form>();

        public void EditJournals( Form owner )
        {
            var form = new JournalListForm();
            form.ShowDialog( owner );
            //ActiveRecordHelper.ShowInSessionScope(() => new JournalListForm(), null,owner);
        }

        public void ShowUnverifiedTests( Form owner )
        {
            var sameForms = showedForms.OfType<UncheckedBlancs>( );
            if (sameForms.Any())
            {
                sameForms.First().Focus();
            }
            else
            {
                var form = new UncheckedBlancs( );
                form.ShowDialog( owner );
                showedForms.Remove(form);
            }
        }

        public void EditTestQuestions( ActiveRecords.Test test, Form owner )
        {
            var formFillingTestAssigmentsListForm = new FormFillingTestAssigmentsListForm( test );
            formFillingTestAssigmentsListForm.ShowDialog(owner);
        }

        public void ShowTestResult( ActiveRecords.TestPassing testPassing, Form owner )
        {
            var form = new ShowResultForm( testPassing );
            form.ShowDialog( owner );
        }

        public bool PerformTest( ActiveRecords.TestPassing testPassing, Form owner )
        {
            var form = new FillingBlancsForm(testPassing);
            return form.ShowDialog( owner ) == DialogResult.OK;
        }

        public void ShowResults( ActiveRecords.TestPassing testPassing, Form owner )
        {
            var form = new ShowResultForm( testPassing );
            form.ShowDialog( owner );
        }

        public bool InitDataBase( string connectionString )
        {
            return !ProfessionalDataContext.InitActiveRecord(connectionString);
        }

        public bool CheckTest( ActiveRecords.Test test, out string error )
        {
            error = null;
            using (new SessionScope(FlushAction.Never))
            {
                var testQuestions = TestQuestion.FindAll(Expression.Eq("Test", test));
                var isThereQuestions = testQuestions.Any( );
                if (isThereQuestions)
                    error = "В тесте отсутствуют задания, обратитесь к преподавателю.";
                return isThereQuestions;
            }
        }

    }
}

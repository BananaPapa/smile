﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eureca.Integrator.Utility
{
    public enum IntegratorDb
    {
        [Description("Curriculum")]
        Curriculum,
        [Description("Professional")]
        Professional
    }
}

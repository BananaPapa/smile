﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eureca.Utility
{
    public interface IEntityEditor<T>
    {
        bool Validate(T entity );

        void SetValidationFunc(Func<T, bool> validator);

    }
}

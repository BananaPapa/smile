﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eureca.Utility.Attributes
{
    public class ColorAttribute:Attribute
    {
        Color _color;
        Brush _brush;
        public Color Color 
        {
            get { return _color; } 
        }

        public Brush Brush 
        { 
            get 
            {
                if (_brush == null)
                    _brush = new SolidBrush( _color );
                return _brush;
            } 
        }
        public ColorAttribute(byte r, byte g, byte b, byte a = 255)
        {
            _color = Color.FromArgb( a, r, g, b );
        }

        public ColorAttribute( string argbString )
        {
            _color = System.Drawing.ColorTranslator.FromHtml( argbString );
        }
    }
}

using System;
using System.Collections.Generic;

namespace Eureca.Utility.Extensions
{
    /// <summary>
    /// System.Collections.Generic.IEnumerable extension methods.
    /// </summary>
    public static class IEnumerableExtensions
    {
        /// <summary>
        /// Performs the specified action on each element of the System.Collections.Generic.IEnumerable.
        /// </summary>
        /// <typeparam name="T">Type of element.</typeparam>
        /// <param name="enumerable">Enumerable source.</param>
        /// <param name="action">The delegate to perform on each element.</param>
        public static void ForEach<T>(this IEnumerable<T> enumerable, Action<T> action)
        {
            if (enumerable == null)
            {
                throw new ArgumentNullException("enumerable");
            }

            if (action == null)
            {
                throw new ArgumentNullException("action");
            }

            foreach (T item in enumerable)
            {
                action(item);
            }
        }
    }
}
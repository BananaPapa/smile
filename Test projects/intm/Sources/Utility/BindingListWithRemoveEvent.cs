﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eureca.Utility.Helper
{
    public interface IListChangesHistory<T>
    {
        IEnumerable<T> AddedItems { get; }
        IEnumerable<T> RemovedItems { get; }
        IEnumerable<T> ChangedItems { get; }
    }

    public class BindingListWithRemoveEvent<T> : BindingList<T>, IListChangesHistory<T>
    {
        private readonly List<T> _addedItems = new List<T>(),
            _removedItems = new List<T>(),
            _changedItems = new List<T>();

        public bool CollectionChanged
        {
            get
            {
                return _addedItems.Count + _removedItems.Count + _changedItems.Count > 0;
            }
        }

        public IEnumerable<T> AddedItems
        {
            get { return _addedItems; }
        }

        public IEnumerable<T> RemovedItems
        {
            get { return _removedItems; }
        }

        public IEnumerable<T> ChangedItems
        {
            get { return _changedItems; }
        }

        protected override void RemoveItem( int itemIndex )
        {
            //itemIndex = index of item which is going to be removed
            //get item from binding list at itemIndex position
            T deletedItem = this.Items[itemIndex];

            if (BeforeRemove != null)
            {
                //raise event containing item which is going to be removed
                BeforeRemove( this, new ItemEventArgs<T>(deletedItem));
            }

            //remove item from list
            base.RemoveItem( itemIndex );
            RegisterDeleteItem(deletedItem);
        }

        private void RegisterDeleteItem(T deletedItem)
        {
            if (_addedItems.Contains(deletedItem))
            {
                _addedItems.Remove(deletedItem);
                if (_changedItems.Contains( deletedItem ))
                    _changedItems.Remove( deletedItem );
                return;
            }
            if (_changedItems.Contains( deletedItem ))
                _changedItems.Remove( deletedItem );
            if (!_removedItems.Contains( deletedItem ))
                _removedItems.Add(deletedItem);
        }

        private void RegisterAddedItem(T addedItem)
        {
            if (_removedItems.Contains(addedItem))
            {
                _removedItems.Remove(addedItem);
                if (!_changedItems.Contains( addedItem ))
                    _changedItems.Add(addedItem);
            }
            else
            {
                if (!_addedItems.Contains( addedItem ))
                    _addedItems.Add( addedItem );    
            }
            
        }

        private void RegisterChangedItem(T changedItem)
        {
            if (_addedItems.Contains( changedItem ))
                return;
            if (!_changedItems.Contains( changedItem ))
                _changedItems.Add( changedItem );
        }

        public event EventHandler<ItemEventArgs<T>> BeforeRemove;

        public BindingListWithRemoveEvent() : base()
        {
            this.ListChanged += BindingListWithRemoveEvent_ListChanged;
        }

        public BindingListWithRemoveEvent(IList<T> items) : base(items)
        {
            this.ListChanged+=BindingListWithRemoveEvent_ListChanged;
        }

        public void MarkAsChanged(T item)
        {
            if (!this.Contains( item ))
                throw new InvalidOperationException( "Данный элемент отсутствует в коллекции" );
            RegisterChangedItem(item);
        }

        public void ClearHistory()
        {
            _addedItems.Clear();
            _removedItems.Clear();
            _changedItems.Clear();
        }

        void BindingListWithRemoveEvent_ListChanged( object sender, ListChangedEventArgs e )
        {
            switch (e.ListChangedType)
            {
                case ListChangedType.Reset:
                    break;
                case ListChangedType.ItemAdded:
                    RegisterAddedItem(this[e.NewIndex]);
                    break;
                case ListChangedType.ItemDeleted:
                    break;
                case ListChangedType.ItemMoved:
                    break;
                case ListChangedType.ItemChanged:
                    RegisterChangedItem(this[e.NewIndex]);
                    break;
                case ListChangedType.PropertyDescriptorAdded:
                    break;
                case ListChangedType.PropertyDescriptorDeleted:
                    break;
                case ListChangedType.PropertyDescriptorChanged:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }



    public class ItemEventArgs<T>:EventArgs
    {
        public T Item { get; private set; }

        public ItemEventArgs(T item)
        {
            Item = item;
        }
    }
}

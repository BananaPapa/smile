﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Threading;

namespace Eureca.Utility.Extensions
{
    public static class ExceptionExtensions
    {
        /// <summary>
        ///     Gets error text recursively including all inner exceptions from the current <paramref name="exception" />.
        /// </summary>
        /// <param name="exception">
        ///     Instance of <see cref="T:System.Exception" />.
        /// </param>
        /// <param name="detailed">Whether to show detailed information about the exception.</param>
        /// <returns>Error text including inner exceptions (recursively).</returns>
        public static string GetErrorText(this Exception exception, bool detailed = true)
        {
            var aggregateException = exception as AggregateException;
            if (aggregateException != null)
            {
                return String.Join(Environment.NewLine + Environment.NewLine,
                    (from e in aggregateException.InnerExceptions select GetErrorText(e, detailed)).
                        ToArray());
            }

            string error = String.Empty;
            Exception ex = exception;

            while (ex != null)
            {
                if (!String.IsNullOrEmpty(error)) error += Environment.NewLine;
                error += detailed ? ex.ToString() : ex.Message;

                ex = ex.InnerException;
            }
            return error;
        }

        public static string FormatString( this string value, params object[] parms )
        {
            return String.Format( value, parms );
        }

        public static string Decline(this int num, string nominative, string singular, string plural)
        {
            if (num > 10 && ((num % 100) / 10) == 1) return plural;

            switch (num % 10)
                {
                    case 1:
                    return nominative;
                    case 2:
                    case 3:
                    case 4:
                    return singular;
                    default: // case 0, 5-9
                    return plural;
                }
            }

        public static string GetEnumDescription(this Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes =
                (DescriptionAttribute[])fi.GetCustomAttributes(
                typeof(DescriptionAttribute),
                false);

            if (attributes != null &&
                attributes.Length > 0)
                return attributes[0].Description;
            else
                return value.ToString();
        }

        public static T[] GetEnumAttribute<T>( this Enum value ) where T:Attribute
        {
            var type = value.GetType();
            var memInfo = type.GetMember( value.ToString( ) );
            var attributes = memInfo[0].GetCustomAttributes( typeof( T ),
                false );
            return (T[])attributes;

        }

        public static void CallInReaderLock( this ReaderWriterLock readerWriterLock, Action action, int lockDuration = -1 )
        {
            bool isLock = false;
            try
            {
                readerWriterLock.AcquireReaderLock( lockDuration );
                isLock = true;
                action( );
            }
            finally
            {
                if (isLock && readerWriterLock.IsReaderLockHeld)
                    readerWriterLock.ReleaseReaderLock( );
            }
        }

        public static void CallInWriterLock( this ReaderWriterLock readerWriterLock, Action action,int lockDuration =-1 )
        {
            bool isLock = false;
            try
            {
                readerWriterLock.AcquireReaderLock( lockDuration );
                isLock = true;
                action( );
            }
            finally
            {
                if (isLock && readerWriterLock.IsReaderLockHeld)
                    readerWriterLock.ReleaseReaderLock( );
            }
        }
    }
}
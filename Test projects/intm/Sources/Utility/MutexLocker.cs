﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Eureca.Utility.Synchronization
{
    public class MutexLocker : IDisposable
    {
        Mutex _mutex;

        public MutexLocker(string mutexName)
        {
            try
            {
                _mutex = new Mutex(false, mutexName);
            }
            catch
            {
                _mutex = null;
            }

            if (_mutex != null)
            {
                _mutex.WaitOne();
            }
        }

        public void Dispose()
        {
            if (_mutex != null)
            {
                _mutex.ReleaseMutex();
                _mutex.Dispose();
            }
        }
    }
}

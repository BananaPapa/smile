﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Eureca.Utility.Helpers
{
   /// <summary>
    /// Provides a static utility object of methods and properties to interact with enumerated types. 
    /// </summary>
    public static class EnumHelper
    {
       /// <summary>
       /// Gets the <see cref="DescriptionAttribute"/> of an <see cref="Enum"/> type value.
       /// </summary>
       /// <param name="value">The <see cref="Enum"/> type value.</param>
      /// <returns>A string containing the text of the <see cref="DescriptionAttribute"/>.</returns>
      public static string GetDescription(Enum value)
      {
         if (value == null)
         {
            throw new ArgumentNullException("value");
         }
    
         string description = value.ToString();
         FieldInfo fieldInfo = value.GetType().GetField(description);
         DescriptionAttribute[] attributes = (DescriptionAttribute[])fieldInfo.GetCustomAttributes( typeof( DescriptionAttribute ), false );
    
         if (attributes != null && attributes.Length > 0)
         {
            description = attributes[0].Description;
         }
         return description;
      }



      public static T[] GetAttribute<T>( Enum value ) where  T : Attribute
      {
          if (value == null)
          {
              throw new ArgumentNullException( "value" );
          }

          //string description = value.ToString( );
          FieldInfo fieldInfo = value.GetType( ).GetField( value.ToString( ) );
          T[] attributes = (T[])fieldInfo.GetCustomAttributes( typeof( T ), false );

          //if (attributes != null && attributes.Length > 0)
          //{
          //    description = attributes[0].Description;
          //}
          return attributes;
      }

      /// <summary>
      /// Converts the <see cref="Enum"/> type to an <see cref="IList"/> compatible object.
      /// </summary>
      /// <param name="type">The <see cref="Enum"/> type.</param>
      /// <returns>An <see cref="IList"/> containing the enumerated type value and description.</returns>
      public static IList ToList(Type type)
      {
         if (type == null)
         {
            throw new ArgumentNullException("type");
         }
    
         ArrayList list = new ArrayList();
         Array enumValues = Enum.GetValues(type);
    
         foreach (Enum value in enumValues)
         {
            list.Add(new KeyValuePair<Enum, string>(value, GetDescription(value)));
         }
    
         return list;
      } 
   }
}

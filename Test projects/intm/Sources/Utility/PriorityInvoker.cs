﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Eureca.Utility.Extensions;

namespace Eureca.Utility
{
    public class PriorityInvoker<T>
    {
        private SortedList<int, List<T>> _listenersQueue = new SortedList<int,List<T>>();
        private ReaderWriterLock _readerWriterLock = new ReaderWriterLock();

        public PriorityInvoker()
        {
            
        }

        /// <summary>
        /// Add listener in invoke queue
        /// </summary>
        /// <param name="order">order >= 0 ,inviker starts from zero element </param>
        /// <param name="invokeObject"></param>
        public void Addlistener(int order, T invokeObject)
        {
            _readerWriterLock.CallInWriterLock(
                () =>
                {
                    if (_listenersQueue.ContainsKey(order))
                    {
                        _listenersQueue[order].Add(invokeObject);
                    }
                    else
                    {
                        _listenersQueue[order] = new List<T>() {invokeObject};
                    }
                }
                , -1);
        }

        public bool Removelistener( T invokeObject, int order = -1 )
        {
            bool res = false;
            _readerWriterLock.CallInWriterLock(
                () =>
                {
                    if (order != -1)
                    {
                        if (_listenersQueue.ContainsKey(order))
                        {
                            var invokeObjects = _listenersQueue[order];
                            if (invokeObjects.Contains(invokeObject))
                            {
                                invokeObjects.Remove(invokeObject);
                                res = true;
                            }
                        }
                        res = false;
                    }
                    else
                    {
                        foreach (var invokerObjects in _listenersQueue.Values)
                        {
                            if (invokerObjects.Contains(invokeObject))
                            {
                                invokerObjects.Remove(invokeObject);
                                res = true;
                            }
                        }
                    }
                }, -1);
            return res;
        }

        public void Invoke(Action<T> action )
        {
           _readerWriterLock.CallInReaderLock(
                () =>
                {
                    foreach (var pair in _listenersQueue)
                    {
                       var res = Parallel.ForEach(pair.Value, action);
                    }
                },-1
                );
        }

        public void ClearListeners()
        {
            _listenersQueue.Clear();
        }
    }
}

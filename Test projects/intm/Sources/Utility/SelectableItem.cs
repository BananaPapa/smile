﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Eureca.Utility
{
    public class SelectableItem<T1>
    {
        public bool IsSelect { get; set; }

        public T1 Item { get; private set; }

        public SelectableItem(T1 item, bool isSelect)
        {
            IsSelect = isSelect;
            Item = item;
        }
    }
}

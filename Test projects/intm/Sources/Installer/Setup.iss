;#define use_dotnetfx45
;#define use_sql2012express
;#define use_KB976932


#define MyAppName "����������"
#define MyAppVersion "1.0.1.1"
#define MyAppPublisher "Eureca"
#define MyAppURL "http://eureca.ru"

#define CurriculumExeName "Curriculum.exe"
#define ProTeacherExeName "ProfessionalTeacher.exe"
#define ProAdministratorExeName "ProfessionalAdministration.exe"
#define PropProbationerExeName "ProfessionalProbationer.exe"



#define CurriculumAppFolder "Curriculum"
#define ProTeacherAppFolder "ProfessionalTeacher"
#define ProAdministratorAppFolder "ProfessionalAdministration"
#define PropProbationerAppFolder "ProfessionalProbationer"
#define UpdateServiceAppFolder "UpdateService"
#define UpdateFolder "DbUpdates"

#define CurriculumRuName "��� �������������"
#define ProTeacherRuName "��� ���������������"
#define ProAdministratorRuName "��� ��������������"
#define PropProbationerRuName "��� ���������� ����������"

#define LogServiceName "IntegratorLogService"
#define LogServiceExe "LogService.exe"

#define SourcesFolder "..\Binary"
#define IconFolder "\Icons"

#define AdministrationServiceName "AdministrationService"
#define AdministrationServiceExe "ProfessionalAdministration.exe"

#define UpdateServiceName "EurecaUpdateService"
#define UpdateServiceExe "Eureca.UpdateService.exe"

[Setup]
AppId={{DFE1128C-80D9-4BD1-8010-80F285A6BDD0}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
AppPublisher={#MyAppPublisher}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppUpdatesURL={#MyAppURL}
DefaultDirName={sd}\Integrator
DefaultGroupName={#MyAppName}
OutputBaseFilename=setup
OutputDir=Output\{#MyAppVersion}
Compression=lzma
SolidCompression=true
ArchitecturesAllowed=x86 x64
ArchitecturesInstallIn64BitMode=x64
DirExistsWarning=no


[CustomMessages]
ru.notenoughdiskspace=��� ��������� ������������ ����� �� �����

win_sp_title=Windows %1 Service Pack %2

[Languages]
Name: ru; MessagesFile: compiler:Languages\Russian.isl
Name: en; MessagesFile: compiler:Default.isl


[Tasks]
Name: desktopicon; Description: {cm:CreateDesktopIcon}; GroupDescription: {cm:AdditionalIcons}
Name: quicklaunchicon; Description: {cm:CreateQuickLaunchIcon}; GroupDescription: {cm:AdditionalIcons}; OnlyBelowVersion: 0,6.1


[Files]
Source: {#SourcesFolder}\{#CurriculumAppFolder}\{#CurriculumExeName}; DestDir: {app}\{#CurriculumAppFolder}; Flags: ignoreversion
Source: {#SourcesFolder}\{#CurriculumAppFolder}\*; DestDir: {app}\{#CurriculumAppFolder}; Flags: ignoreversion recursesubdirs createallsubdirs

Source: {#SourcesFolder}\{#ProAdministratorAppFolder}\{#ProAdministratorExeName}; DestDir: {app}\{#ProAdministratorAppFolder}; Flags: ignoreversion
Source: {#SourcesFolder}\{#ProAdministratorAppFolder}\*; DestDir: {app}\{#ProAdministratorAppFolder}; Flags: ignoreversion recursesubdirs createallsubdirs

Source: {#SourcesFolder}\{#PropProbationerAppFolder}\{#PropProbationerExeName}; DestDir: {app}\{#PropProbationerAppFolder}; Flags: ignoreversion
Source: {#SourcesFolder}\{#PropProbationerAppFolder}\*; DestDir: {app}\{#PropProbationerAppFolder}; Flags: ignoreversion recursesubdirs createallsubdirs

Source: {#SourcesFolder}\{#ProTeacherAppFolder}\{#ProTeacherExeName}; DestDir: {app}\{#ProTeacherAppFolder}; Flags: ignoreversion
Source: {#SourcesFolder}\{#ProTeacherAppFolder}\*; DestDir: {app}\{#ProTeacherAppFolder}; Flags: ignoreversion recursesubdirs createallsubdirs

Source: {#SourcesFolder}\{#UpdateServiceAppFolder}\{#UpdateServiceExe}; DestDir: {app}\{#UpdateServiceAppFolder}; Flags: ignoreversion
Source: {#SourcesFolder}\{#UpdateServiceAppFolder}\*; DestDir: {app}\{#UpdateServiceAppFolder}; Flags: ignoreversion recursesubdirs createallsubdirs

Source: {#SourcesFolder}\{#UpdateFolder}\*; DestDir: {app}\{#UpdateFolder}; Flags: ignoreversion recursesubdirs createallsubdirs


Source: {#SourcesFolder}\Test\KeyboardSimulator\KeyboardSimulator.exe; DestDir: {app}\KeyboardSimulator; Flags: ignoreversion
Source: {#SourcesFolder}\Test\KeyboardSimulator\*; DestDir: {app}\KeyboardSimulator; Flags: ignoreversion recursesubdirs createallsubdirs

Source: ..\..\Outsource\db\Curriculum.sql; DestDir: {app}\db; Flags: ignoreversion recursesubdirs createallsubdirs
Source: ..\..\Outsource\db\Professional.sql; DestDir: {app}\db; Flags: ignoreversion recursesubdirs createallsubdirs

Source: {#SourcesFolder}\{#ProAdministratorAppFolder}\*; DestDir: {app}\{#ProAdministratorAppFolder}; Flags: ignoreversion recursesubdirs createallsubdirs
Source: {#SourcesFolder}\{#UpdateServiceAppFolder}\*; DestDir: {app}\{#UpdateServiceAppFolder}; Flags: ignoreversion recursesubdirs createallsubdirs
Source: {#SourcesFolder}\{#UpdateServiceAppFolder}\{#UpdateServiceExe}; DestDir: {app}\{#UpdateServiceAppFolder}; Flags: ignoreversion recursesubdirs createallsubdirs; AfterInstall : InstallWindowsServices

;�������� ����������
;Source: "..\Binary\PluginTestApplication\*"; DestDir: "{app}\PluginTestApplication"; Flags: ignoreversion recursesubdirs createallsubdirs

[Icons]
Name: {group}\{#CurriculumRuName}; Filename: {app}\{#CurriculumAppFolder}\{#CurriculumExeName}; Languages: 
Name: {commondesktop}\{#CurriculumRuName}; Filename: {app}\{#CurriculumAppFolder}\{#CurriculumExeName}; Languages: 
Name: {group}\{#PropProbationerRuName}; Filename: {app}\{#PropProbationerAppFolder}\{#PropProbationerExeName}
Name: {commondesktop}\{#PropProbationerRuName}; Filename: {app}\{#PropProbationerAppFolder}\{#PropProbationerExeName}
Name: {group}\{#ProAdministratorRuName}; Filename: {app}\{#ProAdministratorAppFolder}\{#ProAdministratorExeName}; Tasks: 
Name: {commondesktop}\{#ProAdministratorRuName}; Filename: {app}\{#ProAdministratorAppFolder}\{#ProAdministratorExeName}
Name: {group}\{#ProTeacherRuName}; Filename: {app}\{#ProTeacherAppFolder}\{#ProTeacherExeName}
Name: {commondesktop}\{#ProTeacherRuName}; Filename: {app}\{#ProTeacherAppFolder}\{#ProTeacherExeName}

[Run]
Filename: {app}\{#CurriculumAppFolder}\{#CurriculumExeName}; Description: "��������� ���������� ""{#CurriculumRuName}""."; Flags: nowait postinstall skipifsilent unchecked
Filename: {app}\{#ProAdministratorAppFolder}\{#ProAdministratorExeName}; Description: "��������� ���������� ""{#ProAdministratorRuName}""."; Flags: nowait postinstall skipifsilent unchecked
Filename: {app}\{#ProTeacherAppFolder}\{#ProTeacherExeName}; Description: "��������� ���������� ""{#ProTeacherRuName}""."; Flags: nowait postinstall skipifsilent unchecked
Filename: {app}\{#PropProbationerAppFolder}\{#PropProbationerExeName}; Description: "��������� ���������� ""{#PropProbationerRuName}""."; Flags: nowait postinstall skipifsilent unchecked

#include "scripts\products.iss"
#include "scripts\common.iss"

#include "scripts\products\stringversion.iss"
#include "scripts\products\winversion.iss"
#include "scripts\products\fileversion.iss"
#include "scripts\products\dotnetfxversion.iss"

#include "scripts\winservices\winservice.iss"

#include "scripts\forms\uninstallform.iss"
#include "scripts\forms\winservicelogonform.iss"

#ifdef use_dotnetfx45
#include "scripts\products\dotnetfx45full.iss"
#endif


#ifdef use_sql2012express
#include "scripts\products\sql2012express.iss"
#endif

#ifdef use_KB976932
#include "scripts\products\KB976932.iss"
#endif

[Code]


Const
constHost = 'ftp://dimadiv:1@dimadiv-server/update/';

var
servicesAdminRights,onlyInstallRumble: Boolean;
host : String;


procedure InitializeWizard();
var
  winServiceLogonPageId : Integer;
begin
  //���� ������� ����� ���������� ��-��� ������������
 if(servicesAdminRights = true) then
 begin
  winServiceLogonPageId := WinServiceLogonPageCreatePage(wpLicense);
  WizardForm.KeyPreview := True;
  _firstInstall := false;
  _initialWindowTitle := WizardForm.Caption;
 end;
end;



function InitializeSetup(): Boolean;
var
	version: string;
begin
  //��������� ��������
  servicesAdminRights := false;
  onlyInstallRumble   := false;

	initwinversion();
  //����� �������� �������������� �������
  host := GetCommandLineParam('/host');

  //����, ����������� ��� ������� ����� ���������� � ������� ��������������(��������� ��� Win 8 ������� �����)
  if(GetCommandLineParam('/servicesAdminRights') = 'true') then
  servicesAdminRights := true ;

  //����, ����������� ��� ������������ ����� ���������� ������ ���, ��� �������������� �������
  if(GetCommandLineParam('/onlyInstallRumble') = 'true') then
    onlyInstallRumble := true;

  //���� ������� ����� �������� �������������� ������� �� ������, �� ��������� �� ���������
  if(host = '') then
    host:=constHost;

  //���� �������� ��������� ������ ��� �� ������, ���� ������ �� "true, �� ������������� ��� �������������� ������"
  if(onlyInstallRumble <> true) then
  begin

    //MsgBox(host, mbInformation, MB_OK);
    #ifdef use_dotnetfx45
    dotnetfx45full(host);
    #endif

    #ifdef use_KB976932
    KB976932(host);
    #endif

    #ifdef use_sql2012express
    sql2012express(host);
    #endif
  end;


Result := true;
end;






//��������� �������� ��������� �������
procedure WaitUntilWindowsServiceStop(serviceName: String);
begin
   if (IsServiceInstalled(serviceName) = true) then
   repeat
      Sleep(200);
   until IsServiceRunning(serviceName) <> true;
end;

//��������� �������� �������� �������
procedure WaitUntilWindowsServiceRemoved(serviceName: String);
begin
  repeat
     Sleep(200);
  until IsServiceInstalled(serviceName) <> true;
end;

//��������� �������� �������
procedure RemoveWindowsService(serviceName: String);
begin
   if (IsServiceInstalled(serviceName) = true) then
      begin
	if (IsServiceRunning(serviceName) = true) then
	begin
	   StopService(serviceName)
	   WaitUntilWindowsServiceStop(serviceName);
        end;
        RemoveService(serviceName);
        WaitUntilWindowsServiceRemoved(serviceName);
      end;
end;

//�������� ��������
procedure RemoveWindowsServices();
begin
    RemoveWindowsService(ExpandConstant('{#AdministrationServiceName}'));
    RemoveWindowsService(ExpandConstant('{#UpdateServiceName}'));
end;


//��������� ������� �������
procedure StartWindowsService(serviceName: String);
begin
   if (IsServiceInstalled(serviceName) = true) then
   begin
    if ( not IsServiceRunning(serviceName) = true) then
    begin
	   StartService(serviceName)
	  end;
   end;
end;

//��������� ������� ��������
procedure StartWindowsServices();
begin
    StartWindowsService(ExpandConstant('{#AdministrationServiceName}'));
    StartWindowsService(ExpandConstant('{#UpdateServiceName}'));

end;



//��������� ��������� ��������
procedure InstallWindowsServices();
var
	serviceInstalled : Boolean;
	serviceUserName, serviceUserPassword : String;
begin
  serviceUserName := 'NT AUTHORITY\System'
  if (servicesAdminRights = true) then
	begin
	   serviceUserName := WinServiceLogonFormUserName();
	   serviceUserPassword := WinServiceLogonFormUserPassword();
     //MsgBox(serviceUserName, mbInformation, MB_OK);
     //MsgBox(serviceUserPassword, mbInformation, MB_OK);
	end;

  serviceInstalled := InstallServiceByUser(ExpandConstant('{app}\{#ProAdministratorAppFolder}\{#AdministrationServiceExe}'),ExpandConstant('{#AdministrationServiceName}'),ExpandConstant('{#AdministrationServiceName}'),'This service is responsible for administrationg probationers results and lessons.',SERVICE_WIN32_OWN_PROCESS,SERVICE_AUTO_START, serviceUserName, serviceUserPassword);
	if (serviceInstalled = false) then
	begin
		MsgBox(ExpandConstant('������ ��������� ������� {#AdministrationServiceName}. �������� ����� � ������ ������������ ��������. ����������� ����� ��������!'), mbError, MB_OK);
		WizardForm.Close();
    exit;
	end;
	 serviceInstalled := InstallServiceByUser(ExpandConstant('{app}\{#UpdateServiceAppFolder}\{#UpdateServiceExe}'),ExpandConstant('{#UpdateServiceName}'),ExpandConstant('{#UpdateServiceName}'),'This service is responsible for updating eureca products.',SERVICE_WIN32_OWN_PROCESS,SERVICE_AUTO_START, serviceUserName, serviceUserPassword);
	if (serviceInstalled = false) then
	begin
		MsgBox(ExpandConstant('������ ��������� ������� {#UpdateServiceName}. �������� ����� � ������ ������������ ��������. ����������� ����� ��������!'), mbError, MB_OK);
		WizardForm.Close();
    exit;
	end;
end;

//������� ��������� �������� ����
procedure CurStepChanged(CurStep: TSetupStep);
begin
  case CurStep of
     ssInstall:
     begin
       RemoveWindowsServices();
     end;
     ssDone:
     begin
       StartWindowsServices();
     end;
  end;
end;

//������� �������� �� ��������� ��������
function NextButtonClick(CurPageID: Integer): Boolean;
begin
  Result := True;

  // Component install page
  if CurPageID = wpReady then begin
		if downloadMemo <> '' then begin
			//change isxdl language only if it is not english because isxdl default language is already english
			if (ActiveLanguage() <> 'en') then begin
				ExtractTemporaryFile(CustomMessage('isxdl_langfile'));
				isxdl_SetOption('language', ExpandConstant('{tmp}{\}') + CustomMessage('isxdl_langfile'));
			end;
			if isxdl_DownloadFiles(StrToInt(ExpandConstant('{wizardhwnd}'))) = 0 then
				Result := false;
		end;
	end;
end;


procedure CurUninstallStepChanged(CurUninstallStep: TUninstallStep);
begin
 case CurUninstallStep of
   usUninstall:
     begin
       RemoveWindowsServices();
       Sleep(200);
     end;
end;





end;

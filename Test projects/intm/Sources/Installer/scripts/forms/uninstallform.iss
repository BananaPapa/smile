#define UninstallFormCaption = "������������� - ������-���"
#define UninstallTaskTitleLabelCaption = "������������� �������� ������������ � ������ ��������"
#define UninstallCheckBoxDeleteUserSettingsCaption = "������� ��������� ������������";
#define UninstallCheckBoxDeleteClientLogsCaption = "������� ����� ��������";
#define UninstallButtonFinishCaption = "��������� >";

[Code]

var
  _uninstallForm : TSetupForm;

  _panelTitle: TPanel;
  _labelTitle: TLabel;
  _checkBoxUserSettings: TCheckBox;
  _checkBoxClientLogs: TCheckBox;
  _buttonFinish: TButton;
  _isCloseByButton : Boolean;

procedure OnFinishButtonClick(Sender: TObject);
begin
  _isCloseByButton := True;
  _uninstallForm.Close;
end;

procedure OnUninstallFormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  if (_isCloseByButton = False) then
     CanClose := False;

  _isCloseByButton := False;
end;

function CreateUninstallAdditionalFilesForm() : TSetupForm;
begin
  _uninstallForm := CreateCustomForm();
  with _uninstallForm do
  begin
    Caption := '{#UninstallFormCaption}';
    Width := ScaleX(380);
    Height := ScaleY(235);
    OnCloseQuery := @OnUninstallFormCloseQuery;
  end;

  //panelTitle
  _panelTitle := TPanel.Create(_uninstallForm);
  with _panelTitle do
  begin
    Parent := _uninstallForm;
    Left := 0;
    Top := 0;
    Width := _uninstallForm.Width;
    Height := ScaleY(58);
    Color := clWhite;
    TabOrder := 2;
  end;
  
   //labelTitle
  _labelTitle := TLabel.Create(_panelTitle);
  with _labelTitle do
  begin
    Parent := _panelTitle;
    Left := ScaleX(8);
    Top := ScaleY(8);
    Width := ScaleX(410);
    Height := ScaleY(16);
    Caption := '{#UninstallTaskTitleLabelCaption}';
    Font.Color := clBlack;
    Font.Height := ScaleY(-11);
    Font.Name := 'Tahoma';
    Font.Style := [fsBold];
    WordWrap := True;
  end;
  
  //checkBoxUserSettings
  _checkBoxUserSettings := TCheckBox.Create(_uninstallForm);
  with _checkBoxUserSettings do
  begin
    Parent := _uninstallForm;
    Left := ScaleX(16);
    Top := ScaleY(72);
    Width := ScaleX(241);
    Height := ScaleY(17);
    Caption := '{#UninstallCheckBoxDeleteUserSettingsCaption}';
    Font.Color := -16777208;
    Font.Height := ScaleY(-13);
    Font.Name := 'Tahoma';
    TabOrder := 0;
  end;
  
  //checkBoxClientLogs
  _checkBoxClientLogs := TCheckBox.Create(_uninstallForm);
  with _checkBoxClientLogs do
  begin
    Parent := _uninstallForm;
    Left := ScaleX(16);
    Top := ScaleY(104);
    Width := ScaleX(273);
    Height := ScaleY(17);
    Caption := '{#UninstallCheckBoxDeleteClientLogsCaption}';
    Font.Color := -16777208;
    Font.Height := ScaleY(-13);
    Font.Name := 'Tahoma';
    TabOrder := 1;
  end;
  
  //buttonFinish
  _buttonFinish := TButton.Create(_uninstallForm);
  with _buttonFinish do
  begin
    Parent := _uninstallForm;
    Left := ScaleX(265);
    Top := ScaleY(168);
    Width := ScaleX(99);
    Height := ScaleY(28);
    Caption := '{#UninstallButtonFinishCaption}';
    TabOrder := 3;
    OnClick := @OnFinishButtonClick    
  end;
  
  Result := _uninstallForm;
end;

procedure ShowUninstallAdditionalFilesForm();
begin
   _uninstallForm := CreateUninstallAdditionalFilesForm();

   _uninstallForm.Center;
   _uninstallForm.ShowModal();
end;

function NeedToDeleteUserSettings() : Boolean;
begin
  Result := _checkBoxUserSettings.Checked;
end;

function NeedToDeleteClientLogs() : Boolean;
begin
  Result := _checkBoxClientLogs.Checked;
end;







[CustomMessages]
WinServiceLogonFormCaption=��������� ������ ��������� ���������
WinServiceLogonFormDescription=������� ��������� ����������� ��� ������ ��������� ���������
WinServiceLogonFormLblAuthTypeCaption =��� �����������:
WinServiceLogonFormChkWinServiceLocalSystemAuthCaption =��������� ������� ������
WinServiceLogonFormChkWinServiceUserAuthCaption =���������������� ������� ������
WinServiceLogonFormLblWinServiceUserCaption =������������:
WinServiceLogonFormLblWinServicePasswordCaption =������:
WinServiceLogonFormLblSignalRPortCaption =���� ��������������:

[Code]
var
  _lblWinServiceAuthType: TLabel;
  _lblWinServiceUser: TLabel;
  _lblWinServicePassword: TLabel;
  _chkWinServiceLocalSystemAuth: TRadioButton;
  _chkWinServiceUserAuth: TRadioButton;
  _txtWinServiceAuthUsername: TEdit;
  _txtWinServiceAuthPassword: TPasswordEdit;

 var
  WinServiceLogonPage: TWizardPage;

function WinServiceLogonFormIsLocalSystemAccount() : Boolean;
begin
   Result := _chkWinServiceLocalSystemAuth.Checked;
end;

function WinServiceLogonFormUserName() : String;
begin
    if(WinServiceLogonFormIsLocalSystemAccount() = true) then
    begin
      Result := 'NT AUTHORITY\System';
    end
    else
    begin
      Result := '.\' + _txtWinServiceAuthUsername.Text;
    end;
end;

function WinServiceLogonFormUserPassword() : String;
begin
   Result := _txtWinServiceAuthPassword.Text;
end;


function IsNextButtonEnabled() : Boolean;
begin
  
   if (_chkWinServiceLocalSystemAuth.Checked) then
   begin
      Result := True
      exit;
   end
   else
   begin
      if (_txtWinServiceAuthUsername.Text = '') or (_txtWinServiceAuthPassword.Text = '') then
      begin
         Result := false
	 exit;
      end;
   end;
   Result := true;
end;



procedure WinServiceLogonFormAuthOnChange (Sender: TObject);
begin
  if _chkWinServiceUserAuth.Checked then
  begin
    _lblWinServiceUser.Enabled := true;
    _lblWinServicePassword.Enabled := true;
    _txtWinServiceAuthUsername.Enabled := true;
    _txtWinServiceAuthPassword.Enabled := true;
  end
  Else
  begin
    _lblWinServiceUser.Enabled := false;
    _lblWinServicePassword.Enabled := false;
    _txtWinServiceAuthUsername.Enabled := false;
    _txtWinServiceAuthPassword.Enabled := false;
  end;
  WizardForm.NextButton.Enabled := IsNextButtonEnabled();
end;

procedure WinServiceLogonFormUserNameOrPasswordOnChange (Sender: TObject);
begin
	WizardForm.NextButton.Enabled := IsNextButtonEnabled();
end;

function WinServiceLogonPageCreatePage(PreviousPageId: Integer): Integer;
begin

  WinServiceLogonPage := CreateCustomPage(
    PreviousPageId,
    ExpandConstant('{cm:WinServiceLogonFormCaption}'),
    ExpandConstant('{cm:WinServiceLogonFormDescription}')
  );

  { _lblWinServiceAuthType }
  _lblWinServiceAuthType := TLabel.Create(WinServiceLogonPage);
  with _lblWinServiceAuthType do
  begin
    Parent := WinServiceLogonPage.Surface;
    Caption := ExpandConstant('{cm:WinServiceLogonFormLblAuthTypeCaption}');
    Left := ScaleX(24);
    Top := ScaleY(8);
    Enabled := True;
  end;

  { _chkWinServiceLocalSystemAuth }
  _chkWinServiceLocalSystemAuth := TRadioButton.Create(WinServiceLogonPage);
  with _chkWinServiceLocalSystemAuth do
  begin
    Parent := WinServiceLogonPage.Surface;
    Caption := ExpandConstant('{cm:WinServiceLogonFormChkWinServiceLocalSystemAuthCaption}');
    Left := ScaleX(24);
    Top := ScaleY(24);
    Width := ScaleX(177);
    Height := ScaleY(17);
    Checked := True;
    TabOrder := 1;
    TabStop := True;
    OnClick := @WinServiceLogonFormAuthOnChange;
  end;

  { _chkWinServiceUserAuth }
  _chkWinServiceUserAuth := TRadioButton.Create(WinServiceLogonPage);
  with _chkWinServiceUserAuth do
  begin
    Parent := WinServiceLogonPage.Surface;
    Caption := ExpandConstant('{cm:WinServiceLogonFormChkWinServiceUserAuthCaption}');
    Left := ScaleX(24);
    Top := ScaleY(44);
    Width := ScaleX(195);
    Height := ScaleY(17);
    TabOrder := 2;
    Enabled := True;
    OnClick := @WinServiceLogonFormAuthOnChange;
  end;

  { _lblWinServiceUser }
  _lblWinServiceUser := TLabel.Create(WinServiceLogonPage);
  with _lblWinServiceUser do
  begin
    Parent := WinServiceLogonPage.Surface;
    Caption := ExpandConstant('{cm:WinServiceLogonFormLblWinServiceUserCaption}');
    Left := ScaleX(49);
    Top := ScaleY(64);
    Width := ScaleX(75);
    Height := ScaleY(13);
    Enabled := False;
  end;

  { _lblWinServicePassword }
  _lblWinServicePassword := TLabel.Create(WinServiceLogonPage);
  with _lblWinServicePassword do
  begin
    Parent := WinServiceLogonPage.Surface;
    Caption := ExpandConstant('{cm:WinServiceLogonFormLblWinServicePasswordCaption}');
    Left := ScaleX(49);
    Top := ScaleY(88);
    Width := ScaleX(53);
    Height := ScaleY(13);
    Enabled := False;
  end;

  { _txtWinServiceAuthUsername }
  _txtWinServiceAuthUsername := TEdit.Create(WinServiceLogonPage);
  with _txtWinServiceAuthUsername do
  begin
    Parent := WinServiceLogonPage.Surface;
    Left := ScaleX(144);
    Top := ScaleY(62);
    Width := ScaleX(241);
    Height := ScaleY(21);
    Enabled := False;
    TabOrder := 4;
    OnChange := @WinServiceLogonFormUserNameOrPasswordOnChange;
  end;

  { _txtWinServiceAuthPassword }
  _txtWinServiceAuthPassword := TPasswordEdit.Create(WinServiceLogonPage);
  with _txtWinServiceAuthPassword do
  begin
    Parent := WinServiceLogonPage.Surface;
    Left := ScaleX(144);
    Top := ScaleY(86);
    Width := ScaleX(241);
    Height := ScaleY(21);
    Enabled := False;
    TabOrder := 5;
    OnChange := @WinServiceLogonFormUserNameOrPasswordOnChange;
  end;

    

  Result := WinServiceLogonPage.ID;
end;
[CustomMessages]
CustomForm_Caption=����������� � ���� ������
CustomForm_Description=������� ����������, ����������� ��� ����������� � ������� ��� ������
CustomForm_lblServer_Caption0=����� �������:
CustomForm_lblAuthType_Caption0=��� �����������:
CustomForm_lblUser_Caption0=������������:
CustomForm_lblPassword_Caption0=������:
CustomForm_lblDatabase_Caption0=���� ������:
CustomForm_chkSQLAuth_Caption0=����������� SQL Server
CustomForm_chkWindowsAuth_Caption0=����������� Windows
//CustomForm_lstVersion_Line0=2008 R2
//CustomForm_lstVersion_Line1=2012

[Code]
const
  adCmdUnspecified = $FFFFFFFF;
  adCmdUnknown = $00000008;
  adCmdText = $00000001;
  adCmdTable = $00000002;
  adCmdStoredProc = $00000004;
  adCmdFile = $00000100;
  adCmdTableDirect = $00000200;
  adOptionUnspecified = $FFFFFFFF;
  adAsyncExecute = $00000010;
  adAsyncFetch = $00000020;
  adAsyncFetchNonBlocking = $00000040;
  adExecuteNoRecords = $00000080;
  adExecuteStream = $00000400;
  adExecuteRecord = $00000800;

var
  //lstVersion: TComboBox;
  lblServer: TLabel;
  lblAuthType: TLabel;
  lblUser: TLabel;
  lblPassword: TLabel;
  lblDatabase: TLabel;
  chkSQLAuth: TRadioButton;
  txtServer: TEdit;
  chkWindowsAuth: TRadioButton;
  txtUsername: TEdit;
  txtPassword: TPasswordEdit;
  lstDatabase: TComboBox;
  txtDatabase: TEdit;
  bIsNextEnabled: Boolean;

 var
  Page: TWizardPage;

// Used to generate error code by sql script errors
procedure ExitProcess(exitCode:integer);
  external 'ExitProcess@kernel32.dll stdcall';

function GetDbConnectionServerName() : String;
begin
   Result := txtServer.Text;
end;

function GetDbConnectionIntegratedSecurity() : Boolean;
begin
   Result := chkWindowsAuth.Checked;
end;

function GetDbConnectionUserName() : String;
begin
   Result := txtUsername.Text;
end;

function GetDbConnectionPassword() : String;
begin
   Result := txtPassword.Text;
end;

function GetDbConnectionDatabaseName() : String;
begin
   if (_firstInstall) then
      Result := txtDatabase.Text
   else
      Result := lstDatabase.Text;
end;

procedure DbConnectionPageInitFirstInstall();
begin
   if (_firstInstall = true) then
   begin
      lstDatabase.Visible := false;
      txtDatabase.Visible := true;
      txtDatabase.Text := lstDatabase.Text;
      if (Length(txtDatabase.Text) > 0) then
         WizardForm.NextButton.Enabled := true
      else
         WizardForm.NextButton.Enabled := false
   end
   else 
   begin
      lstDatabase.Visible := true;
      txtDatabase.Visible := false;
      lstDatabase.Text := txtDatabase.Text;
      WizardForm.NextButton.Enabled := false;
   end;

end;

// enable/disable child text boxes & functions when text has been entered into Server textbox. Makes no sense to populate child items unless a value exists for server.
Procedure ServerOnChange (Sender: TObject);
begin                
  lstDatabase.Items.Clear;
  lstDatabase.Text := '';
  bIsNextEnabled := False;
  WizardForm.NextButton.Enabled := bIsNextEnabled;
  if (Length(txtServer.Text) > 0) then
  begin
    lblAuthType.Enabled := True;
    lblDatabase.Enabled := True;
    lblUser.Enabled := True;
    lblPassword.Enabled := True;
    txtUserName.Enabled := True;
    txtPassword.Enabled := True;
    lstDatabase.Enabled := True;
    txtDatabase.Enabled := True;
    chkWindowsAuth.Enabled := True;
    chkSQLAuth.Enabled := True;
  end
  else
  begin
    lblAuthType.Enabled := False;
    lblDatabase.Enabled := False;
    lblUser.Enabled := False;
    lblPassword.Enabled := False;
    txtUserName.Enabled := False;
    txtPassword.Enabled := False;
    lstDatabase.Enabled := False; 
    txtDatabase.Enabled := False;
    chkWindowsAuth.Enabled := False;
    chkSQLAuth.Enabled := False;
  end;

  if (_firstInstall) and (Length(txtDatabase.Text) > 0) and (Length(txtServer.Text) > 0) then
      WizardForm.NextButton.Enabled := true

end;

// enable/disable user/pass text boxes depending on selected auth type. A user/pass is only required for SQL Auth
procedure  AuthOnChange (Sender: TObject);
begin
  if chkSQLAuth.Checked then
  begin
    lblUser.Enabled := true;
    lblPassword.Enabled := true;
    txtUsername.Enabled := true;
    txtPassword.Enabled := true;
  end
  Else
  begin
    lblUser.Enabled := false;
    lblPassword.Enabled := false;
    txtUsername.Enabled := false;
    txtPassword.Enabled := false;
  end
end;

// Enable next button once a database name has been entered.
Procedure DatabaseOnChange (Sender: TObject);
begin
  if (Length(lstDatabase.Text) > 0) and (lstDatabase.Enabled) and (_firstInstall <> true) then
  begin
    bIsNextEnabled := True;
    WizardForm.NextButton.Enabled := bIsNextEnabled;  
  end
  else
  if (_firstInstall) and (Length(txtDatabase.Text) > 0) then
  begin
      WizardForm.NextButton.Enabled := true;
  end
  else
  begin
    bIsNextEnabled := False;
    WizardForm.NextButton.Enabled := bIsNextEnabled;  
  end;

end;

// Retrieve a list of databases accessible on the server with the credentials specified.
// This list is shown in the database dropdown list
procedure RetrieveDatabaseList(Sender: TObject);
var  
  ADOCommand: Variant;
  ADORecordset: Variant;
  ADOConnection: Variant;  
begin
  lstDatabase.Items.Clear;
  try
    // create the ADO connection object
    ADOConnection := CreateOleObject('ADODB.Connection');
    // build a connection string; for more information, search for ADO
    // connection string on the Internet 
    ADOConnection.ConnectionString := 
      'Provider=SQLOLEDB;' +               // provider
      'Data Source=' + txtServer.Text + ';' +   // server name
      'Application Name=' + '{#SetupSetting("AppName")}' + ' DB List;'
    if chkWindowsAuth.Checked then
      ADOConnection.ConnectionString := ADOConnection.ConnectionString +
      'Integrated Security=SSPI;'         // Windows Auth
    else
      ADOConnection.ConnectionString := ADOConnection.ConnectionString +
      'User Id=' + txtUsername.Text + ';' +              // user name
      'Password=' + txtPassword.Text + ';';                   // password
    // open the connection by the assigned ConnectionString
    ADOConnection.Open;
    try
      // create the ADO command object
      ADOCommand := CreateOleObject('ADODB.Command');
      // assign the currently opened connection to ADO command object
      ADOCommand.ActiveConnection := ADOConnection;
      // assign text of a command to be issued against a provider
      ADOCommand.CommandText := 'SELECT name FROM master.dbo.sysdatabases WHERE HAS_DBACCESS(name) = 1 ORDER BY name';
      // this property setting means, that you're going to execute the 
      // CommandText text command; it does the same, like if you would
      // use only adCmdText flag in the Execute statement
      ADOCommand.CommandType := adCmdText;
      // this will execute the command and return dataset
      ADORecordset := ADOCommand.Execute;
      // get values from a dataset using 0 based indexed field access;
      // notice, that you can't directly concatenate constant strings 
      // with Variant data values
      while not ADORecordset.eof do 
      begin
       lstDatabase.Items.Add(ADORecordset.Fields(0));
       ADORecordset.MoveNext;
      end;

    finally
      ADOConnection.Close;
    end;
  except
    MsgBox(GetExceptionMessage, mbError, MB_OK);
  end;
end;

{ CustomForm_NextkButtonClick }
// try to connect to supplied db. Dont need to catch errors/close conn on error because a failed connection is never opened.
function CustomForm_NextButtonClick(Page: TWizardPage): Boolean;
var  
  ADOConnection: Variant;  
  InitialCatalog: String;
begin
   if (_firstInstall) then
      InitialCatalog := ''
   else
      InitialCatalog := lstDatabase.Text;
  try
    // create the ADO connection object
    ADOConnection := CreateOleObject('ADODB.Connection');
    // build a connection string; for more information, search for ADO
    // connection string on the Internet 
    ADOConnection.ConnectionString := 
      'Provider=SQLOLEDB;' +               // provider
      'Data Source=' + txtServer.Text + ';' +   // server name
      'Initial Catalog=' + InitialCatalog + ';' +   // server name
      'Application Name=' + '{#SetupSetting("AppName")}' + ' Execute SQL;' ;     
    if chkWindowsAuth.Checked then
      ADOConnection.ConnectionString := ADOConnection.ConnectionString +
      'Integrated Security=SSPI;'         // Windows Auth
    else
      ADOConnection.ConnectionString := ADOConnection.ConnectionString +
      'User Id=' + txtUsername.Text + ';' +              // user name
      'Password=' + txtPassword.Text + ';';                   // password
    // open the connection by the assigned ConnectionString
    ADOConnection.Open;
    ADOConnection.Close;
    Result := True;
  except
    MsgBox(GetExceptionMessage, mbError, MB_OK);
  end;
end;

{ CustomForm_CreatePage }
function CustomForm_CreatePage(PreviousPageId: Integer): Integer;
begin
  Page := CreateCustomPage(
    PreviousPageId,
    ExpandConstant('{cm:CustomForm_Caption}'),
    ExpandConstant('{cm:CustomForm_Description}')
  );

  { lblServer }
  lblServer := TLabel.Create(Page);
  with lblServer do
  begin
    Parent := Page.Surface;
    Caption := ExpandConstant('{cm:CustomForm_lblServer_Caption0}');
    Left := ScaleX(24);
    Top := ScaleY(8);
    Width := ScaleX(85);
    Height := ScaleY(13);
    Enabled := True;
  end;

  { txtServer }
  txtServer := TEdit.Create(Page);
  with txtServer do
  begin
    Parent := Page.Surface;
    Left := ScaleX(130);
    Top := ScaleY(6);
    Width := ScaleX(255);
    Height := ScaleY(21);
    TabOrder := 1;
    Enabled := True;
    OnChange := @ServerOnChange;
  end;

  { lblAuthType }
  lblAuthType := TLabel.Create(Page);
  with lblAuthType do
  begin
    Parent := Page.Surface;
    Caption := ExpandConstant('{cm:CustomForm_lblAuthType_Caption0}');
    Left := ScaleX(24);
    Top := ScaleY(38);
    Width := ScaleX(90);
    Height := ScaleY(13);
    Enabled := False;
  end;

  { chkWindowsAuth }
  chkWindowsAuth := TRadioButton.Create(Page);
  with chkWindowsAuth do
  begin
    Parent := Page.Surface;
    Caption := ExpandConstant('{cm:CustomForm_chkWindowsAuth_Caption0}');
    Left := ScaleX(24);
    Top := ScaleY(54);
    Width := ScaleX(177);
    Height := ScaleY(17);
    Checked := True;
    TabOrder := 2;
    TabStop := True;
    OnClick := @AuthOnChange;
    Enabled := False;
  end;

  { chkSQLAuth }
  chkSQLAuth := TRadioButton.Create(Page);
  with chkSQLAuth do
  begin
    Parent := Page.Surface;
    Caption := ExpandConstant('{cm:CustomForm_chkSQLAuth_Caption0}');
    Left := ScaleX(24);
    Top := ScaleY(74);
    Width := ScaleX(185);
    Height := ScaleY(17);
    TabOrder := 3;
    OnClick := @AuthOnChange;
    Enabled := False;
  end;

  { lblUser }
  lblUser := TLabel.Create(Page);
  with lblUser do
  begin
    Parent := Page.Surface;
    Caption := ExpandConstant('{cm:CustomForm_lblUser_Caption0}');
    Left := ScaleX(46);
    Top := ScaleY(94);
    Width := ScaleX(74);
    Height := ScaleY(13);
    Enabled := False;
  end;

  { lblPassword }
  lblPassword := TLabel.Create(Page);
  with lblPassword do
  begin
    Parent := Page.Surface;
    Caption := ExpandConstant('{cm:CustomForm_lblPassword_Caption0}');
    Left := ScaleX(46);
    Top := ScaleY(118);
    Width := ScaleX(74);
    Height := ScaleY(13);
    Enabled := False;
  end;

  { txtUsername }
  txtUsername := TEdit.Create(Page);
  with txtUsername do
  begin
    Parent := Page.Surface;
    Left := ScaleX(130);
    Top := ScaleY(92);
    Width := ScaleX(257);
    Height := ScaleY(21);
    Enabled := False;
    TabOrder := 4;
  end;

  { txtPassword }
  txtPassword := TPasswordEdit.Create(Page);
  with txtPassword do
  begin
    Parent := Page.Surface;
    Left := ScaleX(130);
    Top := ScaleY(116);
    Width := ScaleX(257);
    Height := ScaleY(21);
    Enabled := False;
    TabOrder := 5;
  end;

   { lblDatabase }
  lblDatabase := TLabel.Create(Page);
  with lblDatabase do
  begin
    Parent := Page.Surface;
    Caption := ExpandConstant('{cm:CustomForm_lblDatabase_Caption0}');
    Left := ScaleX(26);
    Top := ScaleY(152);
    Width := ScaleX(70);
    Height := ScaleY(13);
    Enabled := False;
  end;

  { lstDatabase }
  lstDatabase := TComboBox.Create(Page);
  with lstDatabase do
  begin
    Parent := Page.Surface;
    Left := ScaleX(130);
    Top := ScaleY(150);
    Width := ScaleX(257);
    Height := ScaleY(21);
    Enabled := False;
    TabOrder := 6;    
    OnDropDown:= @RetrieveDatabaseList;
    OnChange:= @DatabaseOnChange;
  end;

  txtDatabase := TEdit.Create(Page);
  with txtDatabase do
  begin
    Parent := Page.Surface;
    Left := ScaleX(130);
    Top := ScaleY(150);
    Width := ScaleX(257);
    Height := ScaleY(21);
    Enabled := True;
    Visible:= False;
    TabOrder := 7;    
    OnChange:= @DatabaseOnChange;
  end;

  with Page do
  begin
    OnNextButtonClick := @CustomForm_NextButtonClick;
  end;

  Result := Page.ID;
end;
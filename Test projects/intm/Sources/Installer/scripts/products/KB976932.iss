// required by .NET Framework 2.0 Service Pack 1 on Windows 2000 Service Pack 2-4
// http://www.microsoft.com/technet/security/bulletin/ms04-011.mspx
// http://www.microsoft.com/downloads/details.aspx?FamilyId=0692C27E-F63A-414C-B3EB-D2342FBB6C00

[CustomMessages]
en.kbKB976932_title=Windows SP 1 (KB976932)
;de.kb835732_title=Windows 2000 Sicherheitsupdate (KB835732)

en.KB976932_size=500 MB
;de.kb835732_size=6,8 MB


[Code]

procedure KB976932(host:String);
var 
fileName,url,regKey,KB976932_url,KB976932_url_x64:string;

begin
  
  KB976932_url := host + 'Common/Win7Sp1/windows6.1-KB976932-X86.exe'
  KB976932_url_x64 := host + 'Common/Win7Sp1/windows6.1-KB976932-X64.exe'

  url := KB976932_url; 
  fileName := 'windows6.1-KB976932-X86.exe'; 
  regKey     := 'SOFTWARE\Microsoft\Windows\CurrentVersion\Component Based Servicing\PackageIndex\Windows7SP1-KB976933~31bf3856ad364e35~x86~~0.0.0.0'         
    if IsWin64 then begin
    url :=  KB976932_url_x64;
    fileName := 'windows6.1-KB976932-X64.exe'; 
    regKey     := 'SOFTWARE\Microsoft\Windows\CurrentVersion\Component Based Servicing\PackageIndex\Windows7SP1-KB976933~31bf3856ad364e35~amd64~~0.0.0.0'         
    end;
	if (exactwinversion(6, 1) and (minwinspversion(6, 1, 0) and maxwinspversion(6, 1, 0))) then begin
		if (not RegKeyExists(HKLM, regKey)) then
    begin
        AddProduct(fileName,
          '/quiet /nodialog /norestart',
          CustomMessage('kbKB976932_title'),
          CustomMessage('KB976932_size'),
          url,
          false, true);
    end;
	end;
end;
[CustomMessages]
sql2012expressr2_title=SQL Server 2012 Express

en.sql2012expressr2_size=112 MB

en.sql2012expressr2_size_x64=153.5 MB

[Code]
procedure sql2012express(host:String);
var
	version,url,fileName,sql2012expressr2_url,sql2012expressr2_url_x64: string;

begin
  sql2012expressr2_url := host +'Common/SQL/2012/SQLServerExpress/SQLEXPR32_x86_RUS.exe'
  sql2012expressr2_url_x64 := host + 'Common/SQL/2012/SQLServerExpress/SQLEXPR_x64_RUS.exe'
  url := sql2012expressr2_url; 
  fileName := 'SQLEXPR32_x86_RUS.exe';          
  if IsWin64 then begin
    url :=  sql2012expressr2_url_x64;
    fileName := 'SQLEXPR_x64_RUS.exe';          
    end;
	RegQueryStringValue(HKLM, 'SOFTWARE\Microsoft\Microsoft SQL Server\SQLEXPRESS2012\MSSQLServer\CurrentVersion', 'CurrentVersion', version);
  if ( Length(version) = 0 ) or  (compareversion(version, '11.0') < 0) then begin
		if (not isIA64()) then
			AddProduct(fileName,
				' /ACTION=Install /INSTANCENAME=SQLEXPRESS2012 /INSTANCEID=SQLEXPRESS2012 /QS /HIDECONSOLE /INDICATEPROGRESS="False" /IAcceptSQLServerLicenseTerms /SQLSVCACCOUNT="NT AUTHORITY\NETWORK SERVICE" /SQLSYSADMINACCOUNTS="builtin\administrators" /SKIPRULES="RebootRequiredCheck"',
				CustomMessage('sql2012expressr2_title'),
				CustomMessage('sql2012expressr2_size' + GetArchitectureString()),
        url,
				false, false);
	end;
end;

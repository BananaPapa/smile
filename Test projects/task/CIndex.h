#pragma once
#include <fstream>
#include <math.h>
#include "CTree.h"
using namespace std;

class CIndex
{
private:
	int		count;
public:
	CIndex();
	CTree* m_pT;
	~CIndex();
	int		SearchByKey(char*);
	void	AddIndex(char*, int);
	bool	DeleteIndex(char*);
	int		GetOffset(char*);
	int		GetIndexCount();
	bool	LoadIndexes(char*);
	bool	SaveIndexes(char*);
};
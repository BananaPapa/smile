#pragma once
#include "CListItem.h"
#include <iostream>
#include <stdio.h>
#include <fstream>
#include <string>
using namespace std;

class CTree
{
public:
	CListItem* root;
	CListItem* work;
	int count;

	CTree();
	~CTree();

	bool add(CListItem**,int,char*);

	CListItem* getUzel(CListItem**);
	int getcount(){return count;};
	CListItem** GetRoot(){return &root;};
	int find(CListItem**,char*);
	bool del(CListItem**,char*);
	void StartOut(CListItem**);
	void ViewAsTree(CListItem**,string );
	void PrintTreeinFile(ofstream*,CListItem**);
};
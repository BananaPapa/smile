#include <iostream>
#include <string>
#include <stdio.h>
#include <clocale>
#include "CMenu.h"
using namespace std;

CMenu::CMenu()
{
	BinFile = new CBinaryFile();
	setlocale(LC_ALL,"Russian");

	__asm{
		nop
	}
}

CMenu::~CMenu()
{
	delete BinFile;
}

void CMenu::Execute()
{
	int choice;
	do {
		cout<<endl;
		cout<<"  1 - ������� ����"<<endl;
		cout<<"  2 - ��������� ����"<<endl;
		cout<<"  3 - ������ �����"<<endl;
		cout<<"  4 - �������� ������"<<endl;
		cout<<"  5 - ������� ������"<<endl;
		cout<<"  6 - ������������� ������"<<endl;
		cout<<"  7 - ����� (���������������� �������)"<<endl;
		cout<<"  8 - �������� �����"<<endl;
		cout<<"  9 - �������� �������"<<endl;
		cout<<"  10 - ����������"<<endl;
		cout<<endl;
		cout<<"  0 - �����"<<endl;
		cout<<endl;
		cout<<"  ��� �����:  ";
		scanf("%i", &choice);
		switch(choice){
			case 1: open();
				break;
			case 2: save();
				break;
			case 3: compres();
				break;
			case 4: add();
				break;
			case 5: del();
				break;
			case 6: modify();
				break;
			case 7: search();
				break;
			case 8: binarysearch();
				break;
			case 9: showall();
				break;
			case 10:	sort();
				break;
			case 0:
				return;
			default:
				cout<<"\t��� ������ ��������!";
		}
		cout<<endl;
	} while (true);
}

void CMenu::open(){
	BinFile->Close();

	cout<<"\t��� �����: ";
	char filename[20];
	cin>>filename;
	
	if(BinFile->Open(filename))
		cout<<"\t���� ��������!"<<endl;
	else
		cout<<"\t���� �� ��������!"<<endl;
}

void CMenu::save()
{
	if(!BinFile->IsOpen()) {
		cout<<"\t���� �� ������! ��������(��������) ����"<<endl;
		return;
	}
	if(BinFile->SaveIndexes())
		cout<<"\t��������� ���� ��������!"<<endl;
	else
		cout<<"\t��������� ���� �� ��������!"<<endl;
}

void CMenu::compres()
{
	system(compress);
	if(!BinFile->IsOpen())
		cout << "\t���� �� ������! ��������(��������) ����" << endl;
}
void CMenu::add()
{
	if(!BinFile->IsOpen()) {
		cout<<"\t���� �� ������! ��������(��������) ����"<<endl;
		return;
	}

	cout<<"\t���: ";
	char Name[20];
	cin>>Name;
	cout<<"\t���������: ";
	float Money(0.0f);
	cin>>Money;
	cout<<"\t��� ��������: ";
	short Year(0);
	cin>>Year;
	cout<<"\t���������� �������� � �������: ";
	long long Oper(0);
	cin>>Oper;

	CTech Object;
	Object.SetMoney(Money);
	Object.SetOper(Oper);
	Object.SetYear(Year);
	Object.SetName(Name);

	if(!BinFile->AddObject(Object))
		cout<<"\t����� ������ ��� ����������!"<<endl;
}

void CMenu::del()
{
	if(!BinFile->IsOpen()) {
		cout<<"\t���� �� ������! ��������(��������) ����"<<endl;
		return;
	}

	cout<<"\t���� (��������): ";
	char Key[20];
	cin>>Key;
	BinFile->Index->DeleteIndex(Key);
}

void CMenu::modify()
{
	if(!BinFile->IsOpen()) {
		cout<<"\t���� �� ������! ��������(��������) ����"<<endl;
		return;
	}

	cout<<"\t���� (��������): ";
	char Key[20];
	cin>>Key;
	if(BinFile->Index->SearchByKey(Key) < 0) {
		cout<<"\t������ �� �������!"<<endl;
		return;
	}

	cout<<"\t���: ";
	char Name[20];
	cin>>Name;
	cout<<"\t���������: ";
	float Money(0.0f);
	cin>>Money;
	cout<<"\t��� ��������: ";
	short Year(0);
	cin>>Year;
	cout<<"\t���������� �������� � �������: ";
	long long Oper(0);
	cin>>Oper;

	CTech Object;
	Object.SetMoney(Money);
	Object.SetOper(Oper);
	Object.SetYear(Year);
	Object.SetName(Name);


	if(BinFile->ModifyObject(Object, Key))
		cout<<"\t������ ���������������!"<<endl;
	else
		cout<<"\t������ �� ���������������!"<<endl;
}

void CMenu::search()
{
	if(!BinFile->IsOpen()) {
		cout<<"\t���� �� ������! ��������(��������) ����"<<endl;
		return;
	}

	cout<<"\t���� (��������): ";
	char Key[20];
	cin>>Key;
	if(BinFile->Index->SearchByKey(Key) < 0) {
		cout<<"\t������ �� �������!"<<endl;
		return;
	}

	CTech Obj = BinFile->LoadObject(Key);
	cout<<"\t��������: "<<Obj.GetName()<<endl;
	cout<<"\t���������: "<<Obj.GetMoney()<<endl;
	cout<<"\t���: "<<Obj.GetYear()<<endl;
	cout<<"\t���������� �������� � �������: "<<Obj.GetOper()<<endl;

}

void CMenu::binarysearch()
{
	/*if(!BinFile->IsOpen())
	{
		cout<<"\t���� �� ������! ��������(��������) ����"<<endl;
		return;
	}

	cout<<"\t���� (��������): ";
	char Key[20];
	cin>>Key;
	
	int i=BinFile->Index->BinarySearch(Key);
	if(i<0)
	{
		cout<<"\t������ �� �������!"<<endl;
		return;
	}
	CTech Obj=BinFile->LoadObject(Key);
	cout<<"\t��������: "<<Obj.GetName()<<endl;
	cout<<"\t���������: "<<Obj.GetMoney()<<endl;
	cout<<"\t���: "<<Obj.GetYear()<<endl;
	cout<<"\t���������� �������� � �������: "<<Obj.GetOper()<<endl;*/

}

void CMenu::showall()
{
	BinFile->Index->m_pT->StartOut(BinFile->Index->m_pT->GetRoot());
}
void CMenu::sort()
{
	//BinFile->Index->QuickSortByName(BinFile->Index->Indexes,0,BinFile->Index->GetIndexCount()-1);
}
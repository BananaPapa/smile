#include "CTree.h"
#include <string>

using namespace std;
CTree::CTree()
{
	root = 0;
	work = 0;
	count = 0;
}
CTree::~CTree() { }


// ���������� ������ �������� � ������
bool CTree::add(CListItem** obj, int offset, char *key)
{
	if((*obj) == NULL) {
		(*obj)= new CListItem();
		(*obj)->ItemKey->SetItemKey(key, offset);
		count++;
		return true;
	} else {
		if((strcmp((*obj)->ItemKey->Key, key)) > 0) {
			add(&(*obj)->right,offset,key);
		} else {
			add(&(*obj)->left, offset, key);
		}
	}
}
bool CTree::del(CListItem** obj, char* key)
{
  CListItem *p,*p2;

  if(!(*obj)) return false; /* ������� �� ������� */

  if((strcmp((*obj)->ItemKey->Key,key))==0) 
  { /* �������� ����� */
    /* ��� �������� ������ ������ */
	  if(((*obj)->left==NULL)&&((*obj)->right==NULL))
	  {			
		  delete (*obj);
		  return true;
	  }
    /* ��� ���� ���� �� ����������� ������ */
	  else 
		  if((*obj)->left == NULL) 
		  {
			  p = (*obj)->right;
			  delete (*obj);
			  (*obj)=p;
			  return true;
	      }
    else if((*obj)->right == NULL) {
      p = (*obj)->left;
	  delete (*obj);
	  (*obj)=p;
      return true;
    }
    /* ��� ���� ��� ��������� */
    else {
      p2 = (*obj)->right;
      p = (*obj)->right;
      while(p->left) p = p->left;
      p->left = (*obj)->left;
	  delete (*obj);
      (*obj)=p2;
      return true;
    }
  }
  if((strcmp((*obj)->ItemKey->Key,key))>0) del(&(*obj)->right, key);
  else del(&(*obj)->left, key);
  return true;
}

int CTree::find(CListItem** obj, char *key)
{
	if(!(*obj)){
		return -1;
	} else {
		if((strcmp((*obj)->ItemKey->Key,key))==0) {
			return (*obj)->ItemKey->Position;
		} else {
			if((strcmp((*obj)->ItemKey->Key,key))>0) {
				find(&(*obj)->right,key);
			} else {
				if((strcmp((*obj)->ItemKey->Key,key))<0)
					find(&(*obj)->left,key);
			}
		}
	}
}
void CTree::StartOut(CListItem** obj)
{
	if((*obj)) {
		ViewAsTree(&(*obj), "");
	} else {
		cout << "Nothing to print!\n"; 
	}
}

void CTree::ViewAsTree(CListItem** obj,string str)
{
	if(*obj) {
		ViewAsTree(&(*obj)->right, str + "\t");
		cout << str << (*obj)->ItemKey->Key << "  " << (*obj)->ItemKey->Position << endl;
		ViewAsTree(&(*obj)->left, str + "\t");
	}
}

CListItem* CTree::getUzel(CListItem **obj)
{
	if(((*obj)->left) == 0 && ((*obj)->right) == 0) {
		return (*obj);
	} else {
		getUzel(&(*obj)->left);
		getUzel(&(*obj)->right);
	}
}
void CTree::PrintTreeinFile(ofstream* out, CListItem** obj)
{
	if((*obj)) {
		PrintTreeinFile(out,&(*obj)->left);
		(*out) << (*obj)->ItemKey->Key << "  " << (*obj)->ItemKey->Position << endl;
		PrintTreeinFile(out,&(*obj)->right);
	}
	return;
}
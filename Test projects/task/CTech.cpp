#include "CTech.h"
#include <string>

CTech::CTech() : m_money(0),m_year(0),m_oper(0),m_name(new char[1])
{
	*m_name=0;
}

CTech::CTech(char *name, float money, short year, long long oper) : m_money(money),m_year(year),m_oper(oper)
{
	strcpy_s(m_name,strlen(name)+1,name);
}

CTech::CTech(CTech &Tech) : m_money(Tech.m_money),m_year(Tech.m_year),m_oper(Tech.m_oper),m_name(new char[strlen(Tech.m_name)+1])
{
	strcpy_s(m_name,strlen(Tech.m_name)+1,Tech.m_name);
}

void CTech::SetMoney(float money)
{
	m_money=money;
}

void CTech::SetName(const char *name)
{
	if(m_name!='\0') {
		delete [] m_name;
	}
	m_name=new char[strlen(name)+1];
	strcpy_s(m_name,strlen(name)+1,name);
}

void CTech::SetYear(short year)
{
	m_year=year;
}

void CTech::SetOper(long long oper)
{
	m_oper=oper;
}

CTech::~CTech()
{
	delete [] m_name;
}

bool CTech::CheckName(char *name)
{
	return !strcmp(m_name,name);
}
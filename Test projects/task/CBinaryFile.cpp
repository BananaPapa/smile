#include "CBinaryFile.h"
#include <iostream>
using namespace std;

CBinaryFile::CBinaryFile()
{
	Index = new CIndex;
	opened = false;
}

CBinaryFile::~CBinaryFile()
{
	Close();
}

// ������ ����. ������
int CBinaryFile::GetBufferSize(CTech Object)
{
	return (sizeof(Object.GetMoney()) + sizeof(Object.GetYear()) + 
			sizeof(Object.GetOper()) + sizeof(short) + strlen(Object.GetName()));
}

// ��������� ����. ������ ��� ������ � ����
char* CBinaryFile::GetBuffer(CTech Object)
{
	float money = Object.GetMoney();
	short year = Object.GetYear();
	long long oper = Object.GetOper();
	short len = (short)strlen(Object.GetName()); // ����� ������ � ������

	int bufSize = GetBufferSize(Object);
	char* pBuf = new char [bufSize];

	int cIdx = 0;
	memcpy_s(&pBuf[cIdx], bufSize - cIdx, &len, sizeof(len)); // �������� ����� ������
	cIdx += sizeof(len);
	strcpy(&pBuf[cIdx], Object.GetName()); // �������� ���
	cIdx += len;
	// ����������
	memcpy_s(&pBuf[cIdx], bufSize - cIdx, &money, sizeof(money)); // money
	cIdx += sizeof(money);
	memcpy_s(&pBuf[cIdx], bufSize - cIdx, &year, sizeof(year)); // year
	cIdx += sizeof(year);
	memcpy_s(&pBuf[cIdx], bufSize - cIdx, &oper, sizeof(oper)); // oper

	return pBuf;
}

// ����������  ����. ������ ������ �������� � ����
bool CBinaryFile::AddObject(CTech Object)
{
	// ��������, ����� ������ ��� ����������
	if(Index->SearchByKey(Object.GetName()) > -1)
		return false;


	char* pBuffer = GetBuffer(Object);
	int size = GetBufferSize(Object);
	_lseek(hFile, 0, SEEK_END);
	int offset = _tell(hFile);	// ���� � �����, � �������� ���������� ������� ������
	_write(hFile, pBuffer, size);
	Index->AddIndex(Object.GetName(), offset);

	return true;
}

bool CBinaryFile::ModifyObject(CTech Object, char* Key)
{
	if(Index->DeleteIndex(Key)) {
		AddObject(Object);
		return true;
	}

	return false;
}

// ���������� ������ �� ������� �� �����
CTech CBinaryFile::LoadObject(char *key)
{
	int offset = Index->GetOffset(key);
	CTech Object;
	if(offset >= 0) {
		int idx = offset;
		short year, len;
		long long oper;
		float money;
		char* name;

		_lseek(hFile, idx, SEEK_SET);
		_read(hFile, reinterpret_cast<char*>(&len), sizeof(len)); // ��������� �� ����� ����� ������
		idx += sizeof(len);
		name = new char[len];

		_lseek(hFile, idx, SEEK_SET); 
		_read(hFile, name, (int)len); // ��������� ��������
		name[len] = '\0';
		idx += len;

		// ��������� 
		_lseek(hFile, idx, SEEK_SET);
		_read(hFile, reinterpret_cast<char*>(&money), sizeof(money)); // money
		idx += sizeof(money);

		_lseek(hFile, idx, SEEK_SET);
		_read(hFile, reinterpret_cast<char*>(&year), sizeof(year)); // year
		idx += sizeof(year);

		_lseek(hFile, idx, SEEK_SET);
		_read(hFile, reinterpret_cast<char*>(&oper), sizeof(oper)); // oper
		idx += sizeof(oper);

		Object.SetMoney(money);
		Object.SetName(name);
		Object.SetOper(oper);
		Object.SetYear(year);
	}
	return Object;
}

bool CBinaryFile::IsOpen()
{
	return opened;
}

void CBinaryFile::Close()
{
	if (opened)
	{
		_close(hFile);
		opened = false;
	}
}

bool CBinaryFile::Open(char* _filename)
{
	filename = new char[strlen(_filename)+4];
	strcpy(filename, _filename);
	strcat(filename, ".bin");

	idx_filename = new char[strlen(_filename)+4];
	strcpy(idx_filename, _filename);
	strcat(idx_filename, ".idx");
	Index->LoadIndexes(idx_filename);

	opened = ! _sopen_s(&hFile, filename, _O_RDWR | _O_CREAT, _SH_DENYNO, _S_IREAD | _S_IWRITE );
	return opened;
}

bool CBinaryFile::SaveIndexes()
{
	return Index->SaveIndexes(idx_filename);
}
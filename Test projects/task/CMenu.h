#define compress "compression.bat"
#include "CBinaryFile.h"
#include "CIndex.h"
#pragma once

class CMenu
{
	void compres();
	void add();
	void del();
	void search();
	void modify();
	void open();
	void save();
	void binarysearch();
	void showall();
	void sort();

public:
	CBinaryFile* BinFile;
	CMenu();
	~CMenu();
	void Execute();
};
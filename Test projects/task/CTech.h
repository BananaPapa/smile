#pragma once
class CTech
{
	char *m_name;
	float m_money;
	short m_year;
	long long m_oper;

public:
	CTech();
	CTech(char*,float,short,long long);
	CTech(CTech&);
	void SetName(const char*);
	void SetMoney(float);
	void SetYear(short);
	void SetOper(long long);
	char* GetName(){return m_name;}
	float GetMoney(){return m_money;}
	short GetYear(){return m_year;}
	long long GetOper(){return m_oper;}
	bool CheckName(char*);
	~CTech();
};
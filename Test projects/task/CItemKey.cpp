#include "CItemKey.h"
#include <string>

CItemKey::CItemKey ()
{
	Key = new char [100];
	*Key = '\0';
	Position = 0l;
}

CItemKey::~CItemKey ()
{
	delete [] Key;
}

CItemKey::CItemKey (char* key, int position)
{
	Key = new char [ strlen(key) + 1 ];
	strcpy (Key, key);
	Position = position;
}

void CItemKey::SetItemKey (char* key, int position)
{
	Key = new char [ strlen(key) + 1 ];
	strcpy (Key, key);
	Position = position;
}
#pragma once
#include "CListItem.h"
class CList
{
private:
	int count;
	CListItem* head;
	CListItem* list;
public:
	CList();
	~CList();
	int getcount(){return count;};
	char* getlist();
	bool add(int,char*);
	bool del(char*);
	int find(char*);
};
#include "CTech.h"
#include <fstream>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <io.h>
#include <stdio.h>

#include "CIndex.h"
#include <cerrno>
#include <iostream>
#include <memory.h>
#include <string.h>

using namespace std;
#pragma once
class CBinaryFile
{
	char	*filename, *idx_filename;
	int		hFile;
	bool	opened;

	char*	GetBuffer(CTech);
	int		GetBufferSize(CTech);

public:
	CBinaryFile();
	~CBinaryFile();
	CIndex	*Index;
	CTech	LoadObject(char*);
	bool	AddObject(CTech);
	bool	Open(char*);
	bool	ModifyObject(CTech, char*);
	bool	IsOpen();
	void	Close();
	bool	SaveIndexes();
};
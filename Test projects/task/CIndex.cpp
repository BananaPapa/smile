#include "CIndex.h"
#include <stdio.h>
#include <string.h>

CIndex::CIndex()
{
	count=0;
	m_pT = new CTree();
}

CIndex::~CIndex() { }

int CIndex::SearchByKey(char* key)
{
	int rez = m_pT->find(m_pT->GetRoot(), key);
	if(rez != -1)
		return rez;
	
	return -1;
}

void CIndex::AddIndex(char *key, int offset)
{
	m_pT->add(m_pT->GetRoot(), offset, key);
}

bool CIndex::DeleteIndex(char* Key)
{
	if(m_pT->del(m_pT->GetRoot(), Key))
		return true;
	
	return false;
}

int CIndex::GetOffset(char *key)
{
	return m_pT->find(m_pT->GetRoot(),key);
}

int CIndex::GetIndexCount()
{
	return m_pT->getcount();
}

bool CIndex::LoadIndexes(char* filename)
{
	ifstream fIn;
	fIn.open(filename);
	if(!fIn.is_open())
		return false;
	int j(0);
	fIn>>j;
	for(int i = 0; i < j; i++) {
		char temp[20];
		fIn>>temp;
		int offset;
		fIn>>offset;
		AddIndex(temp, offset);
	}
	fIn.close();
	return true;
}

bool CIndex::SaveIndexes(char* filename)
{
	int cou=m_pT->getcount();
	int i(0);
	char * key= new char[80];
	ofstream fOut;
	fOut.open(filename);
	if(!fOut.is_open())
		return false;
	fOut<<cou<<endl;
	m_pT->PrintTreeinFile(&fOut,m_pT->GetRoot());
	fOut.close();
	return true;
}